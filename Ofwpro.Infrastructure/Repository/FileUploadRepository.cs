﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;

namespace Ofwpro.Infrastructure.Repository
{
    public class FileUploadRepository : IFileUploadRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public FileUploadRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<FileUpload>> Get(long fileId)
        {
            return await unitOfWork.Context.FileUploads.Where(x => x.FileId == (fileId > 0 ? fileId : x.FileId)).ToListAsync();
        }
        public async Task<IEnumerable<FileUpload>> GetByModule(long masterId, string module)
        {
            return await unitOfWork.Context.FileUploads.Where(x => x.MasterIdf == masterId && x.Module == module).ToListAsync();
        }
        public long Save(FileUpload fileUpload)
        {
            if (fileUpload.FileId == 0)
            {
                fileUpload.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<FileUpload>().Add(fileUpload);
            }
            else
            {
                fileUpload.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(fileUpload).State = EntityState.Modified;
                unitOfWork.Context.FileUploads.Update(fileUpload);
            }

            unitOfWork.Commit();
            return fileUpload.FileId;
        }
        public bool Delete(long fileId)
        {
            FileUpload fileUpload = unitOfWork.Context.FileUploads.Where(x => x.FileId == fileId).LastOrDefault();

            if (fileUpload != null)
            {
                unitOfWork.Context.FileUploads.Remove(fileUpload);
                return true;
            }

            return false;
        }
        public bool DeleteByModule(long masterId, string module)
        {
            var fileUploads = unitOfWork.Context.FileUploads.Where(x => x.MasterIdf == masterId && x.Module == module).ToList();

            if (fileUploads.Any())
            {
                unitOfWork.Context.FileUploads.RemoveRange(fileUploads);
                return true;
            }

            return false;
        }
    }
}
