﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class SaveMessageMobileRequestDTO
    {
        public string token { get; set; }
        public long recruiter_posted_jobs_and_applicants_id_enc { get; set; }
        public string user_type { get; set; }
        public string message { get; set; }
        public string os { get; set; }
    }
}
