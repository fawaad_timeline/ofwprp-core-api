﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class SpecializationRequestDTO
    {
        public long Id { get; set; }
        public string SpecializationNameEn { get; set; }
        public string SpecializationNameAr { get; set; }
        public string Lan { get; set; }

        public bool IsActive { get; set; }
    }
    public class SpecializationRequestPaggingDTO
    {
        public int paging { get; set; }
        public int offset { get; set; }
    }
}
