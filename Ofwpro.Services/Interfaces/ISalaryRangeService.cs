﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface ISalaryRangeService
    {
        Task<Tuple<IEnumerable<SalaryRange>, int>> GetAll(int offset, int paging);
        SalaryRange GetById(long id);
        long Add(SalaryRange salaryRange);
        long Update(SalaryRange salaryRange);
        void Delete(long id);
        bool IsExists(string name);
    }
}
