﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class PagesRepository: IPagesRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public PagesRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Pages>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Pages pagesObj)
        {
            var predicate = PredicateBuilder.New<Pages>(true);
            var predicateInner = PredicateBuilder.New<Pages>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                predicateInner = predicateInner.Or(i => i.ContactUs.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.AboutUs.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (pagesObj.PagesId > 0)
            {
                predicate = predicate.And(i => i.PagesId == pagesObj.PagesId);
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(pagesObj.ContactUs))
            {
                predicate = predicate.And(i => i.ContactUs.Contains(pagesObj.ContactUs));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(pagesObj.AboutUs))
            {
                predicate = predicate.And(i => i.AboutUs.Contains(pagesObj.AboutUs));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(pagesObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == pagesObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.Pages.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.Pages.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.Pages.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.Pages.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<Pages> GetById(long pagesId)
        {
            return unitOfWork.Context.Pages.Where(x => !x.IsDeleted && x.PagesId == pagesId).FirstOrDefaultAsync();
        }

        public long Save(Pages pagesObj)
        {
            if (pagesObj.PagesId <= 0)
            {
                pagesObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<Pages>().Add(pagesObj);
            }
            else
            {
                pagesObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(pagesObj).State = EntityState.Modified;
                unitOfWork.Context.Pages.Update(pagesObj);
            }

            unitOfWork.Commit();
            return pagesObj.PagesId;
        }


        public bool Activate(long pagesId, bool isActive)
        {
            Pages pagesObj = unitOfWork.Context.Pages.Where(x => x.PagesId == pagesId).ToList().LastOrDefault();
            if (pagesObj != null)
            {
                pagesObj.IsActive = isActive;
                pagesObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(pagesObj).State = EntityState.Modified;
                unitOfWork.Context.Pages.Update(pagesObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long pagesId)
        {
            var pagesObj = unitOfWork.Context.Pages.Where(x => !x.IsDeleted && x.PagesId == pagesId).ToList().LastOrDefault();

            if (pagesObj != null)
            {
                pagesObj.UpdatedOn = DateTime.Now;
                pagesObj.IsDeleted = true;
                unitOfWork.Context.Entry(pagesObj).State = EntityState.Modified;
                unitOfWork.Context.Pages.Update(pagesObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool IsExist(string ContactUs, long pagesId)
        {
            var predicate = PredicateBuilder.New<Pages>(true);

            predicate = predicate.And(i => i.ContactUs == ContactUs);
            if (pagesId > 0)
            {
                predicate = predicate.And(i => i.PagesId != pagesId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.Pages.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public async Task<IEnumerable<Pages>> GetAll()
        {
            return await unitOfWork.Context.Pages.Where(x => !x.IsDeleted).ToListAsync();
        }
    }
}
