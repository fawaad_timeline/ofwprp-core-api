﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models
{
    public class MessageData
    {
        public DateTime added_date { get; set; }
        public string message_from { get; set; }
        public long id { get; set; }
        public string user_type { get; set; }
        public string message { get; set; }
    }
}
