﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class MessageAgainstJob
    {
        protected long messageAgainstJobId, recruiterPostedJobsAndApplicantsId, userId;
        protected string userType, message, createdBy, updatedBy;
        protected DateTime createdOn, updatedOn;
        protected bool isRead, isActive, isDeleted;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long MessageAgainstJobId { get => messageAgainstJobId; set => messageAgainstJobId = value; }
        public long RecruiterPostedJobsAndApplicantsId { get => recruiterPostedJobsAndApplicantsId; set => recruiterPostedJobsAndApplicantsId = value; }
        public long UserId { get => userId; set => userId = value; }
        public string UserType { get => userType; set => userType = value; }
        public string Message { get => message; set => message = value; }
        public bool IsRead { get => isRead; set => isRead = value; }

        public bool IsActive { get => isActive; set => isActive = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }
    }
}
