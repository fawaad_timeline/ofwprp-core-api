﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class RecruiterPostedJobsMobileResponseDTO
    {
        public long recruiter_posted_jobs_id_enc { get; set; }
        public string reference_no { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string country_name { get; set; }
        public string city { get; set; }
        public string salary_range { get; set; }
        public DateTime date_posted { get; set; }
        public int total_applicats { get; set; }
    }
}
