﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class JobSeekerWorkExperienceService: IJobSeekerWorkExperienceService
    {
        private readonly IJobSeekerWorkExperienceRepository jobSeekerWorkExperienceRepository;

        public JobSeekerWorkExperienceService(IJobSeekerWorkExperienceRepository jobSeekerWorkExperienceRepository)
        {
            this.jobSeekerWorkExperienceRepository = jobSeekerWorkExperienceRepository;
        }

        public async Task<IEnumerable<JobSeekerWorkExperience>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerWorkExperience jobSeekerWorkExperienceObj)
        {
            return await jobSeekerWorkExperienceRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, jobSeekerWorkExperienceObj);
        }

        public async Task<JobSeekerWorkExperience> GetById(long jobSeekerWorkExperienceId)
        {
            return await jobSeekerWorkExperienceRepository.GetById(jobSeekerWorkExperienceId);
        }

        public long Save(JobSeekerWorkExperience jobSeekerWorkExperienceObj)
        {
            return jobSeekerWorkExperienceRepository.Save(jobSeekerWorkExperienceObj);
        }

        public bool Activate(long jobSeekerWorkExperienceId, bool isActive)
        {
            return jobSeekerWorkExperienceRepository.Activate(jobSeekerWorkExperienceId, isActive);
        }

        public bool Delete(long jobSeekerWorkExperienceId)
        {
            return jobSeekerWorkExperienceRepository.Delete(jobSeekerWorkExperienceId);
        }

        public Task<JobSeekerWorkExperience> GetByJobSeekerId(long jobSeekerId)
        {
            return jobSeekerWorkExperienceRepository.GetByJobSeekerId(jobSeekerId);
        }

    }
}
