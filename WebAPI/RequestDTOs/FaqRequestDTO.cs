﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class FaqRequestDTO
    {
        public long Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
       
        public bool IsActive { get; set; }
    }
}
