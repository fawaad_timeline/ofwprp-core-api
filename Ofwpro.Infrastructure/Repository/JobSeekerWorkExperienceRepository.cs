﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class JobSeekerWorkExperienceRepository: IJobSeekerWorkExperienceRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public JobSeekerWorkExperienceRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<JobSeekerWorkExperience>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerWorkExperience jobSeekerWorkExperienceObj)
        {
            var predicate = PredicateBuilder.New<JobSeekerWorkExperience>(true);
            var predicateInner = PredicateBuilder.New<JobSeekerWorkExperience>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                //predicateInner = predicateInner.Or(i => i.JobSeekerWorkExperienceNameEn.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.JobSeekerWorkExperienceNameAr.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (jobSeekerWorkExperienceObj.JobSeekerWorkExperienceId > 0)
            {
                predicate = predicate.And(i => i.JobSeekerWorkExperienceId == jobSeekerWorkExperienceObj.JobSeekerWorkExperienceId);
                isPradicate = true;
            }
            //if (!string.IsNullOrEmpty(jobSeekerWorkExperienceObj.JobSeekerWorkExperienceNameEn))
            //{
            //    predicate = predicate.And(i => i.JobSeekerWorkExperienceNameEn.Contains(jobSeekerWorkExperienceObj.JobSeekerWorkExperienceNameEn));
            //    isPradicate = true;
            //}
            //if (!string.IsNullOrEmpty(jobSeekerWorkExperienceObj.JobSeekerWorkExperienceNameAr))
            //{
            //    predicate = predicate.And(i => i.JobSeekerWorkExperienceNameAr.Contains(jobSeekerWorkExperienceObj.JobSeekerWorkExperienceNameAr));
            //    isPradicate = true;
            //}
            if (!string.IsNullOrEmpty(jobSeekerWorkExperienceObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == jobSeekerWorkExperienceObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.JobSeekerWorkExperience.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.JobSeekerWorkExperience.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.JobSeekerWorkExperience.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.JobSeekerWorkExperience.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<JobSeekerWorkExperience> GetById(long jobSeekerWorkExperienceId)
        {
            return unitOfWork.Context.JobSeekerWorkExperience.Where(x => !x.IsDeleted && x.JobSeekerWorkExperienceId == jobSeekerWorkExperienceId).FirstOrDefaultAsync();
        }

        public long Save(JobSeekerWorkExperience jobSeekerWorkExperienceObj)
        {
            if (jobSeekerWorkExperienceObj.JobSeekerWorkExperienceId <= 0)
            {
                jobSeekerWorkExperienceObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<JobSeekerWorkExperience>().Add(jobSeekerWorkExperienceObj);
            }
            else
            {
                jobSeekerWorkExperienceObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(jobSeekerWorkExperienceObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerWorkExperience.Update(jobSeekerWorkExperienceObj);
            }

            unitOfWork.Commit();
            unitOfWork.Context.Entry(jobSeekerWorkExperienceObj).State = EntityState.Detached;
            return jobSeekerWorkExperienceObj.JobSeekerWorkExperienceId;
        }


        public bool Activate(long jobSeekerWorkExperienceId, bool isActive)
        {
            JobSeekerWorkExperience jobSeekerWorkExperienceObj = unitOfWork.Context.JobSeekerWorkExperience.Where(x => x.JobSeekerWorkExperienceId == jobSeekerWorkExperienceId).ToList().LastOrDefault();
            if (jobSeekerWorkExperienceObj != null)
            {
                jobSeekerWorkExperienceObj.IsActive = isActive;
                jobSeekerWorkExperienceObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(jobSeekerWorkExperienceObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerWorkExperience.Update(jobSeekerWorkExperienceObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long jobSeekerWorkExperienceId)
        {
            var jobSeekerWorkExperienceObj = unitOfWork.Context.JobSeekerWorkExperience.Where(x => !x.IsDeleted && x.JobSeekerWorkExperienceId == jobSeekerWorkExperienceId).ToList().LastOrDefault();

            if (jobSeekerWorkExperienceObj != null)
            {
                jobSeekerWorkExperienceObj.UpdatedOn = DateTime.Now;
                jobSeekerWorkExperienceObj.IsDeleted = true;
                unitOfWork.Context.Entry(jobSeekerWorkExperienceObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerWorkExperience.Update(jobSeekerWorkExperienceObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }


        public Task<JobSeekerWorkExperience> GetByJobSeekerId(long jobSeekerId)
        {
            var result = unitOfWork.Context.JobSeekerWorkExperience.Where(x => !x.IsDeleted && x.JobSeekerId == jobSeekerId).OrderBy(x => x.JobSeekerWorkExperienceId).LastOrDefaultAsync();

            return result;
        }

    }
}
