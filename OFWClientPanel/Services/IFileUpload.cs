﻿using BlazorInputFile;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Services
{
    public interface IFileUpload
    {
        Task<bool> Attachments(IFileListEntry file, string UserType, string documentType, long id);
        Task<long> UploadPicture(IFileListEntry file, string UserType, string documentType, long id);
    }
}
