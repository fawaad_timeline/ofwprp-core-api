﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.ResponseDTO;

namespace WebAPI.Helpers
{
    public class ModelStateErrorHelper
    {
        public ResponseDTO<string> ErrorResponse = new ResponseDTO<string>();
        public ModelStateErrorHelper(List<string> Errors)
        {
            ErrorResponse.StatusCode = 400;
            ErrorResponse.Message = string.Join(" ,", Errors);
        }
    }
}
