﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class TestimonialRepository: ITestimonialRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public TestimonialRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Testimonial>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Testimonial testimonialObj)
        {
            var predicate = PredicateBuilder.New<Testimonial>(true);
            var predicateInner = PredicateBuilder.New<Testimonial>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                predicateInner = predicateInner.Or(i => i.FullName.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.Designation.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (testimonialObj.TestimonialId > 0)
            {
                predicate = predicate.And(i => i.TestimonialId == testimonialObj.TestimonialId);
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(testimonialObj.FullName))
            {
                predicate = predicate.And(i => i.FullName.Contains(testimonialObj.FullName));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(testimonialObj.Designation))
            {
                predicate = predicate.And(i => i.Designation.Contains(testimonialObj.Designation));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(testimonialObj.TestimonialText))
            {
                predicate = predicate.And(i => i.TestimonialText.Contains(testimonialObj.TestimonialText));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(testimonialObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == testimonialObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.Testimonial.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.Testimonial.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.Testimonial.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.Testimonial.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<Testimonial> GetById(long testimonialId)
        {
            return unitOfWork.Context.Testimonial.Where(x => !x.IsDeleted && x.TestimonialId == testimonialId).FirstOrDefaultAsync();
        }

        public long Save(Testimonial testimonialObj)
        {
            if (testimonialObj.TestimonialId <= 0)
            {
                testimonialObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<Testimonial>().Add(testimonialObj);
            }
            else
            {
                testimonialObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(testimonialObj).State = EntityState.Modified;
                unitOfWork.Context.Testimonial.Update(testimonialObj);
            }

            unitOfWork.Commit();
            return testimonialObj.TestimonialId;
        }


        public bool Activate(long testimonialId, bool isActive)
        {
            Testimonial testimonialObj = unitOfWork.Context.Testimonial.Where(x => x.TestimonialId == testimonialId).ToList().LastOrDefault();
            if (testimonialObj != null)
            {
                testimonialObj.IsActive = isActive;
                testimonialObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(testimonialObj).State = EntityState.Modified;
                unitOfWork.Context.Testimonial.Update(testimonialObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long testimonialId)
        {
            var testimonialObj = unitOfWork.Context.Testimonial.Where(x => !x.IsDeleted && x.TestimonialId == testimonialId).ToList().LastOrDefault();

            if (testimonialObj != null)
            {
                testimonialObj.UpdatedOn = DateTime.Now;
                testimonialObj.IsDeleted = true;
                unitOfWork.Context.Entry(testimonialObj).State = EntityState.Modified;
                unitOfWork.Context.Testimonial.Update(testimonialObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool IsExist(string FullName, long testimonialId)
        {
            var predicate = PredicateBuilder.New<Testimonial>(true);

            predicate = predicate.And(i => i.FullName == FullName);
            if (testimonialId > 0)
            {
                predicate = predicate.And(i => i.TestimonialId != testimonialId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.Testimonial.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public async Task<IEnumerable<Testimonial>> GetAll()
        {
            return await unitOfWork.Context.Testimonial.Where(x => !x.IsDeleted).ToListAsync();
        }
    }
}
