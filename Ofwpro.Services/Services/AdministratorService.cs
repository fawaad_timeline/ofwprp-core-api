﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;

namespace Ofwpro.Services.Services
{
    public class AdministratorService : IAdministratorService
    {
        private readonly IAdministratorRepository administratorRepository;

        public AdministratorService(IAdministratorRepository administratorRepository)
        {
            this.administratorRepository = administratorRepository;
        }

        public async Task<IEnumerable<Administrator>> Get(long administratorId) => await administratorRepository.Get(administratorId);

        public long Save(Administrator administrator) => administratorRepository.Save(administrator);

        public bool Activate(long administratorId, bool isActive) => administratorRepository.Activate(administratorId, isActive);

        public bool Delete(long administratorId) => administratorRepository.Delete(administratorId);

        public bool IsExist(string email, long administratorId) => administratorRepository.IsExist(email, administratorId);
        public bool Validate(string email, string password) => administratorRepository.Validate(email, password);
        public Task<Administrator> GetByEmail(string email) => administratorRepository.GetByEmail(email);
        public Task<Administrator> GetByAdministratorIdToken(string administratorIdToken) => administratorRepository.GetByAdministratorIdToken(administratorIdToken);

        public Task<IEnumerable<Administrator>> GetAll() => administratorRepository.GetAll();

    }
}
