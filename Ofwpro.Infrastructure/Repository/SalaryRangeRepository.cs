﻿using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class SalaryRangeRepository :ISalaryRangeRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public SalaryRangeRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public long Add(SalaryRange salaryRange)
        {
            
            _unitOfWork.Context.Set<SalaryRange>().Add(salaryRange);
            _unitOfWork.Commit();
            return salaryRange.Id;
        }

        public void Delete(long id)
        {
            var salaryRange = _unitOfWork.Context.SalaryRange.Where(x => x.Id==id).FirstOrDefault();
            if (salaryRange != null)
            {
                _unitOfWork.Context.SalaryRange.Remove(salaryRange);
                _unitOfWork.Commit();
            }
        }

        public async Task<Tuple<IEnumerable<SalaryRange>, int>> GetAll(int offset, int paging)
        {

            int recordsCount = _unitOfWork.Context.SalaryRange.Count();
            var listData = await _unitOfWork.Context.SalaryRange.OrderBy(x => x.Id).Skip(offset * paging).Take(paging).ToListAsync();

            return new Tuple<IEnumerable<SalaryRange>, int>(listData, recordsCount);
        }
        public SalaryRange GetById(long id)
        {
            var salaryRangeList = _unitOfWork.Context.SalaryRange.Where(x => x.Id == id).FirstOrDefault();
            return salaryRangeList;
        }

        public bool IsExists(string name)
        {
            bool IsExists = false;
            var salaryRanges = _unitOfWork.Context.SalaryRange.Where(x => x.Name == name);
            if (salaryRanges.Any())
                IsExists = true;
            else
                IsExists = false;
            return IsExists;
        }

        public long Update(SalaryRange salaryRange)
        {
            _unitOfWork.Context.Entry(salaryRange).State = EntityState.Modified;
            _unitOfWork.Context.SalaryRange.Update(salaryRange);
            _unitOfWork.Commit();
            return salaryRange.Id;
        }
    }
}
