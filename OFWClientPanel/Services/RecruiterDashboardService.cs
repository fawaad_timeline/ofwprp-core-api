﻿using Microsoft.Extensions.Configuration;
using OFWClientPanel.Data.Models.Recruiter;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Services
{
    public class RecruiterDashboardService:IRecruiterDashboardService
    {
        private readonly IRecruiterCompanyService recruiterCompanyService;
        private readonly IRecruiterAttachmentService recruiterAttachmentService;
        private readonly IConfiguration config;

        public RecruiterDashboardService(IRecruiterCompanyService recruiterCompanyService,
                                         IRecruiterAttachmentService recruiterAttachmentService, IConfiguration config)
        {
            this.recruiterCompanyService = recruiterCompanyService;
            this.recruiterAttachmentService = recruiterAttachmentService;
            this.config = config;
        }

        public async Task<CompanyDetails> GetCompanyDetails(long RecruiterId)
        {
            CompanyDetails recruiterCompany = new CompanyDetails();

            try
            {
                string hostUrl = config["Utility:ClientBaseURL"].ToString();
                string uploadFolderName = config["UploadFolders:UploadFolder"];

                var recruiterCompanyObj = await recruiterCompanyService.GetByRecruiterId(RecruiterId);
                var recruitmentAttachments = await recruiterAttachmentService.GetByDocumentType(RecruiterId, "company_logo");

                if (recruiterCompanyObj != null)
                {
                    recruiterCompany.RecruiterCompanyId = recruiterCompanyObj.RecruiterCompanyId;
                    recruiterCompany.CompanyName = recruiterCompanyObj.CompanyName;
                    recruiterCompany.PhoneNumber = recruiterCompanyObj.PhoneNumber;
                    recruiterCompany.Description = recruiterCompanyObj.Description;
                    recruiterCompany.OtherInformation = recruiterCompanyObj.AboutCompany;
                    recruiterCompany.Address = recruiterCompanyObj.Address;
                }

                if (recruitmentAttachments != null)
                {
                    string UploadedFrom = "";
                    if (recruitmentAttachments.UploadedFrom != null) UploadedFrom = recruitmentAttachments.UploadedFrom;

                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();

                    recruiterCompany.CompanyLogoURL = hostUrl + uploadFolderName + "/" + recruitmentAttachments.FileNameUpdated;
                }
            }
            catch (Exception ex) { }

            return recruiterCompany;
        }
    }
}
