﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models
{
    public class RecruiterRegistration
    {
        //[StringLength(8, ErrorMessage = "National Number too long (8 character limit).")]

        [Required(ErrorMessage = "Business Registration/License Number is required")]
        public string NationalNumber { get; set; }

        public string Title { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        [Required]
        public string CompanyName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }


        [Required]
        public string Gender { get; set; }

        [Required]
        public string Mobile { get; set; }
        [Required]
        public string CompanyPhone { get; set; }
        public string CountryNo { get; set; }

        public string CountryCode { get; set; }
        public string UserType { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string ProfilePictureURL { get; set; }
    }
}
