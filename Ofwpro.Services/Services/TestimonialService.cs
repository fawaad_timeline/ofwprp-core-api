﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class TestimonialService: ITestimonialService
    {
        private readonly ITestimonialRepository testimonialRepository;

        public TestimonialService(ITestimonialRepository testimonialRepository)
        {
            this.testimonialRepository = testimonialRepository;
        }

        public async Task<IEnumerable<Testimonial>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Testimonial testimonialObj)
        {
            return await testimonialRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, testimonialObj);
        }

        public async Task<Testimonial> GetById(long testimonialId)
        {
            return await testimonialRepository.GetById(testimonialId);
        }

        public long Save(Testimonial testimonialObj)
        {
            return testimonialRepository.Save(testimonialObj);
        }

        public bool Activate(long testimonialId, bool isActive)
        {
            return testimonialRepository.Activate(testimonialId, isActive);
        }

        public bool Delete(long testimonialId)
        {
            return testimonialRepository.Delete(testimonialId);
        }

        public bool IsExist(string testimonialName, long TestimonialId)
        {
            return testimonialRepository.IsExist(testimonialName, TestimonialId);
        }
        public async Task<IEnumerable<Testimonial>> GetAll()
        {
            return await testimonialRepository.GetAll();
        }
    }
}
