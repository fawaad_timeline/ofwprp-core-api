﻿
using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JobSeekerPersonalInformationResponseDTO
    {     
        public long tbl_job_seeker_personal_information_id { get; set; }
        public long tbl_job_seeker_id { get; set; }
        public long nationality { get; set; }
        public long tbl_marital_status_id { get; set; }
        public string home_phone { get; set; }
        public string mobileCode { get; set; }
        public string is_driving_license { get; set; }
        public string current_location { get; set; }
        public string visa_status { get; set; }

        public CountryDetails countryDetails { get; set; }
        public MaritalStatus maritalStatus { get; set; }

        public DateTime added_date { get; set; }
        public string is_active { get; set; }
    }
}
