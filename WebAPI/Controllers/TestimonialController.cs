﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Ofwpro.Core;
using Ofwpro.Services.Interfaces;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class TestimonialController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IUserService userService;
        private readonly IFileUploadService fileUploadService;
        private readonly IUserResolverService userResolverService;
        private readonly ITestimonialService testimonialService;
        private readonly IAdministratorService administratorService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public TestimonialController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IUserService userService, IFileUploadService fileUploadService, IUserResolverService userResolverService, ITestimonialService testimonialService, IAdministratorService administratorService, IMapper mapper, IConfiguration config)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.userService = userService;
            this.fileUploadService = fileUploadService;
            this.userResolverService = userResolverService;
            this.testimonialService = testimonialService;
            this.administratorService = administratorService;
            this.mapper = mapper;
            this.config = config;
        }

        [HttpGet]
        [Route("en/testimonial/AllTestimonial")]
        public async Task<object> All()
        {
            Testimonial TestimonialObj = new Testimonial();
            var testimonialList = await testimonialService.All(0, 9999, "CreatedOn", "ASC", "", TestimonialObj);
            var TestimonialList = mapper.Map<IEnumerable<TestimonialResponseDTO>>(testimonialList);
            if (TestimonialList.Any())
            {
                return TestimonialList;
            }
            else
            {
            }
            return "No data found.";
        }

        [HttpGet("en/testimonial/GetTestimonial")]
        public async Task<IActionResult> GetTestimonial(GetRequestDTO model)
        {
            long id = model.Id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            isSuccess = true;
            StatusCode = 200;
            Message = "Success";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.TestimonialManagement.ToString());
                bool hasRight = true;

                // Check rights
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Get details
                if (isSuccess == true)
                {
                    var testimonialObj = await testimonialService.GetById(id);
                    long sessionId = id;

                    if (testimonialObj != null)
                    {
                        TestimonialResponseDTO TestimonialResponseDTOObj = mapper.Map<TestimonialResponseDTO>(testimonialObj);
                        response.Data = TestimonialResponseDTOObj;
                    }
                    else
                    {
                        StatusCode = 404;
                        isSuccess = false;
                        Message = "No data found.";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        [HttpPost("en/testimonial/SaveTestimonial")]
        public async Task<IActionResult> SaveTestimonial([FromBody] TestimonialRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            Testimonial testimonialObjOld = null;
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been saved successfully";
            string ExceptionMessage = "";

            string lan = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.TestimonialManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                if (isSuccess == true && model.Id > 0)
                {
                    testimonialObjOld = await testimonialService.GetById(model.Id);
                    if (testimonialObjOld == null)
                    {
                        StatusCode = 404;
                        isSuccess = false;
                        Message = "No data found";
                        ExceptionMessage = "";
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(model.FullName))
                    {
                        StatusCode = 400;
                        isSuccess = false;
                        Message = "Please provide full name.";
                        ExceptionMessage = "";
                    }
                }

                // Check duplication
                if (isSuccess == true)
                {
                    isExist = testimonialService.IsExist(model.FullName, model.Id);

                    if (isExist)
                    {
                        StatusCode = 409;
                        isSuccess = false;
                        Message = "Same name already exists.";
                        ExceptionMessage = "";
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    Testimonial testimonialObj = mapper.Map<Testimonial>(model);
                    if (model.Id > 0)
                    {
                        if (testimonialObjOld != null)
                        {
                            testimonialObj.CreatedOn = testimonialObjOld.CreatedOn;
                            testimonialObj.CreatedBy = testimonialObjOld.CreatedBy;
                            testimonialObj.IsDeleted = testimonialObjOld.IsDeleted;
                            testimonialObj.UpdatedBy = currentUser.Id;
                        }
                    }
                    else
                    {
                        testimonialObj.CreatedOn = DateTime.Now;
                        testimonialObj.IsDeleted = false;
                        if (string.IsNullOrEmpty(testimonialObj.CreatedBy))
                        {
                            if (currentUser != null)
                            {
                                testimonialObj.CreatedBy = currentUser.Id;
                            }
                        }
                    }

                    // Save testimonial                   
                    long sessionId = testimonialService.Save(testimonialObj);

                    if (sessionId < 0)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be saved.";
                        ExceptionMessage = "";
                    }
                    else
                    {
                        // Retrieve testimonial
                        testimonialObj = await testimonialService.GetById(sessionId);
                        TestimonialResponseDTO TestimonialResponseDTOObj = mapper.Map<TestimonialResponseDTO>(testimonialObj);
                        Data = TestimonialResponseDTOObj;
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while saving data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        [HttpPut("en/testimonial/ActivateTestimonial")]
        public async Task<IActionResult> ActivateTestimonial(ActivateRequestDTO model)
        {
            long id = model.id; bool active = model.active;
            ResponseDTO<object> response = new ResponseDTO<object>();
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been updated successfully";
            string ExceptionMessage = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.TestimonialManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                Testimonial testimonialObj = await testimonialService.GetById(id);
                if (testimonialObj == null)
                {
                    StatusCode = 404;
                    isSuccess = false;
                    Message = "No data found";
                    ExceptionMessage = "";
                }

                // Activate/Deactivate
                if (isSuccess == true)
                {
                    bool result = testimonialService.Activate(id, active);
                    if (result == false)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be updated";
                        ExceptionMessage = "";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Information could not be updated";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        [HttpDelete("en/testimonial/DeleteTestimonial")]
        public async Task<IActionResult> DeleteTestimonial(DeleteRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been deleted successfully";
            string ExceptionMessage = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.TestimonialManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                Testimonial testimonialObj = await testimonialService.GetById(id);
                if (testimonialObj == null)
                {
                    StatusCode = 404;
                    isSuccess = false;
                    Message = "No data found";
                    ExceptionMessage = "";
                }

                // Delete
                if (isSuccess == true)
                {
                    bool result = testimonialService.Delete(id);
                    if (result == false)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be deleted";
                        ExceptionMessage = "";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Information could not be deleted";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }
    }
}