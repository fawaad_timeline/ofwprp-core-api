﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class MessageAgainstJobResponseDTO
    {
        public string code { get; set; }
        public string MSG { get; set; }
        public List<Dictionary<string, string>> messages { get; set; }
    }
}
