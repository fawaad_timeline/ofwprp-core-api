﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Services.DTOs;
using Ofwpro.Services.Interfaces;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;

namespace WebAPI.Controllers
{
    [EnableCors("AllowOrigin")]
    public class NewsController : ControllerBase
    {
        private readonly IFileUploadService fileUploadService;
        private readonly IUserResolverService userResolverService;
        private readonly INewsService newsService;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IMapper mapper;
        private readonly IConfiguration config;
        public NewsController(IFileUploadService fileUploadService, IUserResolverService userResolverService, INewsService newsService, IWebHostEnvironment hostingEnvironment, IMapper mapper, IConfiguration config)
        {
            this.fileUploadService = fileUploadService;
            this.userResolverService = userResolverService;
            this.newsService = newsService;
            this.hostingEnvironment = hostingEnvironment;
            this.mapper = mapper;
            this.config = config;
        }

        [HttpPost]
        [Route("/en/news/all")]
        public async Task<ResponseDTO<SearchResponseDTO>> All([FromBody]NewsSearchDTO model)
        {
            int StatusCode = 200;
            bool isSuccess = true;
            string Message = "Information retrieved successfully";
            string ExceptionMessage = "";
            string BaseURL = config["Utility:APIBaseURL"].ToString();
            string InitiativeFolderName = config["UploadFolders:InitiativeFiles"];
            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode=200 };

            int offset = model.offset;
            int paging = model.paging == 0 ? Convert.ToInt32(config["GeneralConfig:Paging"]) : model.paging;
            string orderByColumn = model.orderByColumn == null ? "CreatedOn" : model.orderByColumn;
            string orderBy = model.orderBy == null ? "DESC" : model.orderBy;

            if (string.IsNullOrEmpty(orderByColumn) || orderByColumn.ToLower() == "string") { orderByColumn = "CreatedOn"; }
            if (string.IsNullOrEmpty(orderBy) || orderBy.ToLower() == "string") { orderBy = "Desc"; }

            News newsObj = mapper.Map<News>(model);
            IEnumerable<News> sellsList = await newsService.All(offset, paging, orderByColumn, orderBy, model.searchTerm, model.searchNewsDate, newsObj);
            if (!sellsList.Any())
            {
                StatusCode = 404;
                isSuccess = true;
                Message = "No records found";
                ExceptionMessage = "";
            }
            else
            {
                int totalRecords = sellsList.Count();
                IEnumerable<News> DocumentsList = sellsList.Skip(paging * offset).Take(paging).ToList();

                IEnumerable<NewsResponseDTO> newsList = mapper.Map<IEnumerable<NewsResponseDTO>>(DocumentsList);
                foreach (var _newsList in newsList)
                {
                    _newsList.Icon = BaseURL + InitiativeFolderName + "/" + _newsList.Icon;
                    _newsList.NewsDateStr = _newsList.NewsDate.ToString("yyyy-MM-ddThh:mm");
                }
                SearchResponseDTO SearchResponseDTOObj = new SearchResponseDTO();
                SearchResponseDTOObj.Records = newsList;
                SearchResponseDTOObj.offset = offset;
                SearchResponseDTOObj.paging = paging;
                SearchResponseDTOObj.totalRecords = totalRecords;

                response.Data = SearchResponseDTOObj;
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;
            return response;
        }
        [HttpGet]
        [Route("/en/news/get/{id}")]
        public async Task<IActionResult> Get(long id)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            isSuccess = true;
            StatusCode = 200;
            Message = "Success";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.SellManagement.ToString());
                bool hasRight = true;

                // Check rights
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Get details
                if (isSuccess == true)
                {
                    var newsObj = await newsService.GetById(id);
                    long newsId = id;

                    if (newsObj != null)
                    {
                        NewsResponseDTO SellResponseDTOObj = mapper.Map<NewsResponseDTO>(newsObj);
                        SellResponseDTOObj.NewsDateStr = SellResponseDTOObj.NewsDate.ToString("yyyy-MM-ddThh:mm");
                        //NewsFiles _sellFilesObj = new NewsFiles();
                        //_sellFilesObj.IsActive = true;
                        //_sellFilesObj.IsDeleted = false;

                        //_sellFilesObj.NewsId = newsId;
                        //var _newsFilesList = await newsFilesService.All(0, 9999, "CreatedOn", "DESC", "", _sellFilesObj);
                        //List<FileResponseDTO> FileResponseDTOList = new List<FileResponseDTO>();
                        //if (_newsFilesList.Any())
                        //{
                        //    foreach (NewsFiles sellFileObj in _newsFilesList)
                        //    {
                        //        IEnumerable<FileUpload> fileObjList = await fileUploadService.Get(sellFileObj.FilesId);
                        //        if (fileObjList.Any())
                        //        {
                        //            FileUpload fileObj = fileObjList.FirstOrDefault();
                        //            FileResponseDTO FileResponseDTOObj = mapper.Map<FileResponseDTO>(fileObj);
                        //            FileResponseDTOList.Add(FileResponseDTOObj);
                        //        }
                        //    }

                        //SellResponseDTOObj.FilesList = FileResponseDTOList;
                        //}

                        response.Data = SellResponseDTOObj;
                    }
                    else
                    {
                        StatusCode = 404;
                        isSuccess = false;
                        Message = "No data found.";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }
        [HttpPost]
        [Route("en/news/save")]
        public async Task<IActionResult> Post([FromBody] NewsRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            News newsObjOld = null;
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been saved successfully";
            string ExceptionMessage = "";

            string lan = "";
            if (!string.IsNullOrEmpty(model.Lan)) { lan = "en"; }
            if (!lan.Equals("en") && !lan.Equals("ar")) { lan = "en"; }
            model.Lan = lan;

            //List<NewsFiles> NewsFilesList = mapper.Map<List<NewsFiles>>(model.NewsFilesList);

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();
                bool hasRight = true;
                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }
                // Check existance
                if (isSuccess == true && model.Id > 0)
                {
                    newsObjOld = await newsService.GetById(model.Id);
                    if (newsObjOld == null)
                    {
                        StatusCode = 404;
                        isSuccess = false;
                        Message = "No data found";
                        ExceptionMessage = "";
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(model.NewsTitle))
                    {
                        StatusCode = 400;
                        isSuccess = false;
                        Message = "Please provide news title.";
                        ExceptionMessage = "";
                    }
                    else if (string.IsNullOrEmpty(model.NewsDescription))
                    {
                        StatusCode = 400;
                        isSuccess = false;
                        Message = "Please provide news details.";
                        ExceptionMessage = "";
                    }
                }


                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    News newsObj = mapper.Map<News>(model);
                    if (model.Id > 0)
                    {
                        if (newsObjOld != null)
                        {
                            newsObj.CreatedOn = newsObjOld.CreatedOn;
                            newsObj.CreatedBy = newsObjOld.CreatedBy;
                            newsObj.IsDeleted = newsObjOld.IsDeleted;
                            newsObj.UpdatedBy = currentUser?.Id;
                        }
                    }
                    else
                    {
                        newsObj.CreatedOn = DateTime.Now;
                        newsObj.NewsDate = model.NewsDate;
                        newsObj.IsDeleted = false;
                        newsObj.IsActive = true;
                        newsObj.Icon = model.Icon;

                        if (string.IsNullOrEmpty(newsObj.CreatedBy))
                        {
                            if (currentUser != null)
                            {
                                newsObj.CreatedBy = currentUser.Id;
                            }
                        }
                    }

                    // Save sell                   
                    long newsId = newsService.Save(newsObj);

                    if (newsId < 0)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be saved.";
                        ExceptionMessage = "";
                    }
                    else
                    {
                        
                        newsObj = await newsService.GetById(newsId);
                        Data = newsObj;
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while saving data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }
        
        [Route("en/news/delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(long id)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been deleted successfully";
            string ExceptionMessage = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.SellManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                News newsObj = await newsService.GetById(id);
                if (newsObj == null)
                {
                    StatusCode = 404;
                    isSuccess = false;
                    Message = "No data found";
                    ExceptionMessage = "";
                }

                // Delete
                if (isSuccess == true)
                {
                    bool result = newsService.Delete(id);
                    if (result == false)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be deleted";
                        ExceptionMessage = "";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Information could not be deleted";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

    }
}
