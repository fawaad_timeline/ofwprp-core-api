﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class RecruiterPostedJobsAndApplicantsResponseDTO
    {
        public long recruiter_posted_jobs_and_applicants_id_enc { get; set; }
        public long recruiter_posted_jobs_id_enc { get; set; }
        public long job_seeker_id_enc { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string title { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string total_message_against_job { get; set; }
        public string total_message_against_job_unread { get; set; }
        public string comments { get; set; }
        public string selected_for_interview { get; set; }
        public string selected_for_job { get; set; }
        public string logo_url { get; set; }
    }
}
