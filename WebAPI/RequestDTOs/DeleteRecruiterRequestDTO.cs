﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class DeleteRecruiterRequestDTO
    {
        public string token { get; set; }
        public long recruiter_id { get; set; }
    }
}
