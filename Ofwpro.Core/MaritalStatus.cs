﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ofwpro.Core
{
    public class MaritalStatus
    {
		protected long _Id;
		protected string _Rng;
		protected string _Name;
		
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id
		{
			get => _Id;
			set => _Id = value;
		}
		public string Rng
		{
			get => _Rng;
			set => _Rng = value;
		}
		public string Name
		{
			get => _Name;
			set => _Name = value;
		}
	}
}
