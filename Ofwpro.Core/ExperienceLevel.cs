﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ofwpro.Core
{
    public class ExperienceLevel
    {
		protected int _Id;
		protected string _Name;

		public int Id
		{
			get => _Id;
			set => _Id = value;
		}
		public string Name
		{
			get => _Name;
			set => _Name = value;
		}
	}
}
