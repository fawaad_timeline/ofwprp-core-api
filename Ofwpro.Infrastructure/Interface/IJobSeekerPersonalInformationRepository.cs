﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IJobSeekerPersonalInformationRepository
    {
        Task<IEnumerable<JobSeekerPersonalInformation>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerPersonalInformation jobSeekerPersonalInformationObj);
        Task<JobSeekerPersonalInformation> GetById(long jobSeekerPersonalInformationId);
        long Save(JobSeekerPersonalInformation jobSeekerPersonalInformationObj);
        bool Activate(long jobSeekerPersonalInformationId, bool isActive);
        bool Delete(long jobSeekerPersonalInformationId);
        Task<JobSeekerPersonalInformation> GetByJobSeekerId(long jobSeekerId);
    }
}
