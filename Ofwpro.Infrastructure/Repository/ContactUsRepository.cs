﻿using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ofwpro.Infrastructure.Repository
{
    public class ContactUsRepository:IContactUsRepository
    {
        public readonly IUnitOfWork unitOfWork;

        public ContactUsRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public long Save(ContactUs contactUs)
        {
            if (contactUs.ContactUsId <= 0)
            {
                contactUs.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<ContactUs>().Add(contactUs);
            }
            else
            {
                contactUs.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(contactUs).State = EntityState.Modified;
                unitOfWork.Context.ContactUs.Update(contactUs);
            }

            unitOfWork.Commit();
            unitOfWork.Context.Entry(contactUs).State = EntityState.Detached;
            return contactUs.ContactUsId;
        }
        public List<ContactUs> GetAll()
        {
            var contactUsList = unitOfWork.Context.ContactUs.ToList();
            return contactUsList;
        }
    }
}
