﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class JobSeekerSaveWorkExperienceRequestDTO
    {
        public string token { get; set; }
        public string experience_level { get; set; }

        public string company1 { get; set; }
        public string job_title1 { get; set; }
        public long country_id_enc1 { get; set; }
        public string city1 { get; set; }
        public string time_period_from1 { get; set; }
        public string time_period_to1 { get; set; }
        public string reason_for_leaving1 { get; set; }

        public string company2 { get; set; }
        public string job_title2 { get; set; }
        public long country_id_enc2 { get; set; }
        public string city2 { get; set; }
        public string time_period_from2 { get; set; }
        public string time_period_to2 { get; set; }
        public string reason_for_leaving2 { get; set; }
        
        public string company3 { get; set; }
        public string job_title3 { get; set; }
        public long country_id_enc3 { get; set; }
        public string city3 { get; set; }
        public string time_period_from3 { get; set; }
        public string time_period_to3 { get; set; }
        public string reason_for_leaving3 { get; set; }
    }
}
