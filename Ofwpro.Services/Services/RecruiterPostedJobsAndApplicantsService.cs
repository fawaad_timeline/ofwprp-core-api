﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class RecruiterPostedJobsAndApplicantsService: IRecruiterPostedJobsAndApplicantsService
    {
        private readonly IRecruiterPostedJobsAndApplicantsRepository recruiterPostedJobsAndApplicantsRepository;

        public RecruiterPostedJobsAndApplicantsService(IRecruiterPostedJobsAndApplicantsRepository recruiterPostedJobsAndApplicantsRepository)
        {
            this.recruiterPostedJobsAndApplicantsRepository = recruiterPostedJobsAndApplicantsRepository;
        }

        public async Task<IEnumerable<RecruiterPostedJobsAndApplicants>> All(string orderByColumn, string orderBy, string searchTerm, RecruiterPostedJobsAndApplicants recruiterPostedJobsAndApplicantsObj)
        {
            return await recruiterPostedJobsAndApplicantsRepository.All(orderByColumn, orderBy, searchTerm, recruiterPostedJobsAndApplicantsObj);
        }

        public async Task<RecruiterPostedJobsAndApplicants> GetById(long recruiterPostedJobsAndApplicantsId)
        {
            return await recruiterPostedJobsAndApplicantsRepository.GetById(recruiterPostedJobsAndApplicantsId);
        }

        public long Save(RecruiterPostedJobsAndApplicants recruiterPostedJobsAndApplicantsObj)
        {
            return recruiterPostedJobsAndApplicantsRepository.Save(recruiterPostedJobsAndApplicantsObj);
        }

        public bool Activate(long recruiterPostedJobsAndApplicantsId, bool isActive)
        {
            return recruiterPostedJobsAndApplicantsRepository.Activate(recruiterPostedJobsAndApplicantsId, isActive);
        }

        public bool Delete(long recruiterPostedJobsAndApplicantsId)
        {
            return recruiterPostedJobsAndApplicantsRepository.Delete(recruiterPostedJobsAndApplicantsId);
        }       

        public Task<RecruiterPostedJobsAndApplicants> GetByRecruiterPostedJobsId(long recruiterPostedJobsId)
        {
            return recruiterPostedJobsAndApplicantsRepository.GetByRecruiterPostedJobsId(recruiterPostedJobsId);
        }

        public Task<List<RecruiterPostedJobsAndApplicants>> GetAllByRecruiterPostedJobsId(long recruiterPostedJobsId)
        {
            return recruiterPostedJobsAndApplicantsRepository.GetAllByRecruiterPostedJobsId(recruiterPostedJobsId);
        }

        public bool IsExist(long jobSeekerId, long recruiterPostedJobsId)
        {
            return recruiterPostedJobsAndApplicantsRepository.IsExist(jobSeekerId, recruiterPostedJobsId);
        }

        public int totalApplicants(long recruiterPostedJobsId)
        {
            return recruiterPostedJobsAndApplicantsRepository.totalApplicants(recruiterPostedJobsId);
        }

        public async Task<IEnumerable<RecruiterPostedJobsAndApplicants>> GetByJobSeeker(long jobSeekerId)
        {
            return await recruiterPostedJobsAndApplicantsRepository.GetByJobSeeker(jobSeekerId);
        }
    }
}
