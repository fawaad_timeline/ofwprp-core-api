﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Ofwpro.Core;
using Ofwpro.Services.Interfaces;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class SpecializationController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IUserService userService;
        private readonly IFileUploadService fileUploadService;
        private readonly IUserResolverService userResolverService;
        private readonly ISpecializationService specializationService;
        private readonly IAdministratorService administratorService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public SpecializationController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IUserService userService, IFileUploadService fileUploadService, IUserResolverService userResolverService, ISpecializationService specializationService, IAdministratorService administratorService, IMapper mapper, IConfiguration config)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.userService = userService;
            this.fileUploadService = fileUploadService;
            this.userResolverService = userResolverService;
            this.specializationService = specializationService;
            this.administratorService = administratorService;
            this.mapper = mapper;
            this.config = config;
        }

        [HttpGet]
        [Route("en/admin/specialization/ajax_all_specializations_mobile")]
        public async Task<object> All()
        {
            Specialization SpecializationObj = new Specialization();
            var specializationList = await specializationService.All(0, 9999, "SpecializationId", "ASC", "", SpecializationObj);
            var SpecializationList = mapper.Map<IEnumerable<SpecializationResponseDTO>>(specializationList.Item1);
            if (SpecializationList.Any())
            {
                return SpecializationList;
            }
            else
            {
            }
            return "No data found.";
        }
        [HttpPost]
        [Route("en/admin/specialization/ajax_all_specializations_admin")]
        public async Task<IActionResult> ajax_all_specializations_admin([FromBody] SpecializationRequestPaggingDTO model)
        {
            int StatusCode = 200;
            bool isSuccess = true;
            int recordsCount = 0;
            string Message = "Information retrieved successfully";
            string ExceptionMessage = "";
            string BaseURL = config["Utility:APIBaseURL"].ToString();
            string InitiativeFolderName = config["UploadFolders:InitiativeFiles"];
            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode=200 };

            int offset = model.offset;
            int paging = model.paging == 0 ? Convert.ToInt32(config["GeneralConfig:Paging"]) : model.paging;
            Specialization SpecializationObj = new Specialization();
            var tuple = await specializationService.All(offset, paging, "SpecializationId", "ASC", "", SpecializationObj);
            var specializationList = tuple.Item1;
            recordsCount = tuple.Item2;
            var SpecializationList = mapper.Map<IEnumerable<SpecializationResponseDTO>>(specializationList);
            if (!SpecializationList.Any())
            {
                response.StatusCode =  404;
                response.Data =  null;
                response.Message = "no record found.";
            }
            else
            {
                SearchResponseDTO SearchResponseDTOObj = new SearchResponseDTO();
                SearchResponseDTOObj.Records = SpecializationList;
                SearchResponseDTOObj.offset = offset;
                SearchResponseDTOObj.paging = paging;
                SearchResponseDTOObj.totalRecords = recordsCount;

                response.Data = SearchResponseDTOObj;
            }
            return ResponseHelper<SearchResponseDTO>.GenerateResponse(response);
        }
        [HttpGet("Get")]
        public async Task<IActionResult> Get(GetSpecializationRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            isSuccess = true;
            StatusCode = 200;
            Message = "Success";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.SpecializationManagement.ToString());
                bool hasRight = true;

                // Check rights
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Get details
                if (isSuccess == true)
                {
                    var specializationObj = await specializationService.GetById(id);
                    long sessionId = id;

                    if (specializationObj != null)
                    {
                        SpecializationResponseDTO SpecializationResponseDTOObj = mapper.Map<SpecializationResponseDTO>(specializationObj);
                        response.Data = SpecializationResponseDTOObj;
                    }
                    else
                    {
                        StatusCode = 404;
                        isSuccess = false;
                        Message = "No data found.";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        [HttpPost("Save")]
        public async Task<IActionResult> Post([FromBody] SpecializationRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            Specialization specializationObjOld = null;
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been saved successfully";
            string ExceptionMessage = "";

            string lan = "";
            if (!string.IsNullOrEmpty(model.Lan)) { lan = "en"; }
            if (!lan.Equals("en") && !lan.Equals("ar")) { lan = "en"; }
            model.Lan = lan;

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.SpecializationManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                if (isSuccess == true && model.Id > 0)
                {
                    specializationObjOld = await specializationService.GetById(model.Id);
                    if (specializationObjOld == null)
                    {
                        StatusCode = 404;
                        isSuccess = false;
                        Message = "No data found";
                        ExceptionMessage = "";
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(model.SpecializationNameEn))
                    {
                        StatusCode = 400;
                        isSuccess = false;
                        Message = "Please provide specialization name [En].";
                        ExceptionMessage = "";
                    }                  
                }
                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    Specialization specializationObj = mapper.Map<Specialization>(model);
                    if (model.Id > 0)
                    {
                        if (specializationObjOld != null)
                        {
                            specializationObj.CreatedOn = specializationObjOld.CreatedOn;
                            specializationObj.CreatedBy = specializationObjOld.CreatedBy;
                            specializationObj.IsDeleted = specializationObjOld.IsDeleted;
                            //specializationObj.UpdatedBy = currentUser.Id;
                        }
                    }
                    else
                    {
                        specializationObj.CreatedOn = DateTime.Now;
                        specializationObj.IsDeleted = false;
                        //if (string.IsNullOrEmpty(specializationObj.CreatedBy))
                        //{
                        //    if (currentUser != null)
                        //    {
                        //        specializationObj.CreatedBy = currentUser.Id;
                        //    }
                        //}
                    }

                    // Save specialization                   
                    long sessionId = specializationService.Save(specializationObj);

                    if (sessionId < 0)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be saved.";
                        ExceptionMessage = "";
                    }
                    else
                    {
                        // Retrieve specialization
                        specializationObj = await specializationService.GetById(sessionId);
                        SpecializationResponseDTO SpecializationResponseDTOObj = mapper.Map<SpecializationResponseDTO>(specializationObj);
                        Data = SpecializationResponseDTOObj;
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while saving data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        [HttpPut("Activate")]
        public async Task<IActionResult> Put(ActivateSpecializationRequestDTO model)
        {
            long id = model.id;  bool active = model.active;
            ResponseDTO<object> response = new ResponseDTO<object>();
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been updated successfully";
            string ExceptionMessage = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.SpecializationManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                Specialization specializationObj = await specializationService.GetById(id);
                if (specializationObj == null)
                {
                    StatusCode = 404;
                    isSuccess = false;
                    Message = "No data found";
                    ExceptionMessage = "";
                }

                // Activate/Deactivate
                if (isSuccess == true)
                {
                    bool result = specializationService.Activate(id, active);
                    if (result == false)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be updated";
                        ExceptionMessage = "";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Information could not be updated";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        [HttpDelete("Delete")]
        public async Task<IActionResult> Delete(DeleteSpecializationRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been deleted successfully";
            string ExceptionMessage = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.SpecializationManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                Specialization specializationObj = await specializationService.GetById(id);
                if (specializationObj == null)
                {
                    StatusCode = 404;
                    isSuccess = false;
                    Message = "No data found";
                    ExceptionMessage = "";
                }

                // Delete
                if (isSuccess == true)
                {
                    bool result = specializationService.Delete(id);
                    if (result == false)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be deleted";
                        ExceptionMessage = "";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Information could not be deleted";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }
    }
}