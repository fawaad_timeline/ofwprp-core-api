﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IWebSettingsRepository
    {
        public Task<WebSettings> All(int settingId);
        public Task<WebSettings> GetById(long WebSettingsId);
        public long AddUpdate(WebSettings WebSettingsObj);
        public bool Delete(long WebSettingsId);
        public Task<IEnumerable<WebSettings>> GetAll();
        public Task<WebSettings> GetFirstRecord();
    }

}
