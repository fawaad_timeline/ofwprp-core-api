﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class DeleteEducationLevelRequestDTO
    {
        public int id { get; set; }
    }
}
