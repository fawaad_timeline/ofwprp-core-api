﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class SaveChatRecruiter2RequestDTO
    {
        public string token { get; set; }
        public long job_seeker_id_enc { get; set; }
        public string message { get; set; }
        public string os { get; set; }
    }
}
