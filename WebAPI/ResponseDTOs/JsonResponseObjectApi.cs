﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JsonResponseObjectApi
    {
        public int total_messages_unread;
        public long job_seeker_id_enc;
        public string sender_name;
        public long recruiter_posted_jobs_and_applicants_id_enc;
        public long recruiter_id_enc;

        public DateTime added_date { get; set; }
        public string message_from { get; set; }
        public long id  { get; set; }
        public string user_type { get; set; }
        public string message { get; set; }
    }
}
