﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class RecruiterPostedJobsRepository: IRecruiterPostedJobsRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public RecruiterPostedJobsRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<RecruiterPostedJobs>> All(string orderByColumn, string orderBy, string searchTerm, RecruiterPostedJobs recruiterPostedJobsObj)
        {
            var predicate = PredicateBuilder.New<RecruiterPostedJobs>(true);
            var predicateInner = PredicateBuilder.New<RecruiterPostedJobs>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                //predicateInner = predicateInner.Or(i => i.Title.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.City.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.CompanyType.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.EmploymentType==searchTerm);
            }

            // Dynamic Conditions
            if (recruiterPostedJobsObj.RecruiterId > 0)
            {
                predicate = predicate.And(i => i.RecruiterId == recruiterPostedJobsObj.RecruiterId);
                isPradicate = true;
            }
            if (recruiterPostedJobsObj.RecruiterPostedJobsId > 0)
            {
                predicate = predicate.And(i => i.RecruiterPostedJobsId == recruiterPostedJobsObj.RecruiterPostedJobsId);
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(recruiterPostedJobsObj.Title))
            {
                predicate = predicate.And(i => i.Title.Contains(recruiterPostedJobsObj.Title));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(recruiterPostedJobsObj.City))
            {
                predicate = predicate.And(i => i.City.Contains(recruiterPostedJobsObj.City));
                isPradicate = true;
            }
            if (recruiterPostedJobsObj.SalaryRangeId > 0)
            {
                predicate = predicate.And(i => i.SalaryRangeId == recruiterPostedJobsObj.SalaryRangeId);
                isPradicate = true;
            }
            if (recruiterPostedJobsObj.CompanyType > 0)
            {
                predicate = predicate.And(i => i.CompanyType == recruiterPostedJobsObj.CompanyType);
                isPradicate = true;
            }
            if (recruiterPostedJobsObj.EmploymentType>0)
            {
                predicate = predicate.And(i => i.EmploymentType ==recruiterPostedJobsObj.EmploymentType);
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(recruiterPostedJobsObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == recruiterPostedJobsObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            predicate = predicate.And(i => i.IsActive == true);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.RecruiterPostedJobs.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.RecruiterPostedJobs.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.RecruiterPostedJobs.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.RecruiterPostedJobs.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public async Task<IEnumerable<RecruiterPostedJobs>> GetAll()
        {
            var result = unitOfWork.Context.RecruiterPostedJobs.Where(x => !x.IsDeleted).OrderBy(x => x.Title);
            return await result.ToListAsync();
        }
        public Task<RecruiterPostedJobs> GetById(long recruiterPostedJobsId)
        {
            return unitOfWork.Context.RecruiterPostedJobs.Where(x => !x.IsDeleted && x.RecruiterPostedJobsId == recruiterPostedJobsId).FirstOrDefaultAsync();
        }

        public long Save(RecruiterPostedJobs recruiterPostedJobsObj)
        {
            if (recruiterPostedJobsObj.RecruiterPostedJobsId <= 0)
            {
                recruiterPostedJobsObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<RecruiterPostedJobs>().Add(recruiterPostedJobsObj);
            }
            else
            {
                recruiterPostedJobsObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(recruiterPostedJobsObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterPostedJobs.Update(recruiterPostedJobsObj);
            }

            unitOfWork.Commit();
            return recruiterPostedJobsObj.RecruiterPostedJobsId;
        }


        public bool Activate(long recruiterPostedJobsId, bool isActive)
        {
            RecruiterPostedJobs recruiterPostedJobsObj = unitOfWork.Context.RecruiterPostedJobs.Where(x => x.RecruiterPostedJobsId == recruiterPostedJobsId).ToList().LastOrDefault();
            if (recruiterPostedJobsObj != null)
            {
                recruiterPostedJobsObj.IsActive = isActive;
                recruiterPostedJobsObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(recruiterPostedJobsObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterPostedJobs.Update(recruiterPostedJobsObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long recruiterPostedJobsId)
        {
            var recruiterPostedJobsObj = unitOfWork.Context.RecruiterPostedJobs.Where(x => !x.IsDeleted && x.RecruiterPostedJobsId == recruiterPostedJobsId).ToList().LastOrDefault();

            if (recruiterPostedJobsObj != null)
            {
                recruiterPostedJobsObj.UpdatedOn = DateTime.Now;
                recruiterPostedJobsObj.IsDeleted = true;
                unitOfWork.Context.Entry(recruiterPostedJobsObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterPostedJobs.Update(recruiterPostedJobsObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public Task<RecruiterPostedJobs> GetByRecruiterId(long recruiterId)
        {
            return unitOfWork.Context.RecruiterPostedJobs.Where(x => !x.IsDeleted && x.RecruiterId == recruiterId).FirstOrDefaultAsync();
        }

        public bool IsExist(string referenceNo)
        {
            var predicate = PredicateBuilder.New<RecruiterPostedJobs>(true);

            predicate = predicate.And(i => i.ReferenceNo == referenceNo);            
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.RecruiterPostedJobs.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public bool IsExistRecruiterPostedJobsId(long recruiterPostedJobsId)
        {
            var predicate = PredicateBuilder.New<RecruiterPostedJobs>(true);

            predicate = predicate.And(i => i.RecruiterPostedJobsId == recruiterPostedJobsId);
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.RecruiterPostedJobs.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public bool RemoveRange(List<RecruiterPostedJobs> recruiterPostedJobs)
        {
            try
            {
                recruiterPostedJobs.ForEach(x => { x.UpdatedOn = DateTime.Now; x.IsDeleted = true; });

                unitOfWork.Context.Set<RecruiterPostedJobs>().UpdateRange(recruiterPostedJobs);
                unitOfWork.Commit();

                return true;
            }
            catch (Exception ex) { }

            return false;
        }

        public async Task<IEnumerable<RecruiterPostedJobs>> Filter(string searchText, string searchLocation, List<string> employmentType)
        {
            //var recruiterCompany = unitOfWork.Context.RecruiterCompany.Where(x => x.IsActive && !x.IsDeleted).ToList();

            var recruiter = unitOfWork.Context.Recruiter.Where(x => x.IsActive && !x.IsDeleted).ToList();

           // recruiter = recruiter.Where(x => recruiterCompany.Any(r => r.RecruiterId == x.RecruiterId)).ToList();

            var recruitedPostedJobs = await unitOfWork.Context.RecruiterPostedJobs.Where(x => !x.IsDeleted).ToListAsync();

            recruitedPostedJobs = recruitedPostedJobs.Where(x => recruiter.Any(r => r.RecruiterId == x.RecruiterId)).OrderByDescending(x => (x.UpdatedOn.Date > x.CreatedOn.Date ? x.UpdatedOn : x.CreatedOn)).ToList();

           
          

            List<RecruiterCompany> recruiterCompanies = new List<RecruiterCompany>();
            List<RecruiterPostedJobs> filteredLocationPostedJobs = new List<RecruiterPostedJobs>();
            List<RecruiterPostedJobs> filteredEmployeeTypePostedJobs = new List<RecruiterPostedJobs>();

            //if (searchText.Trim().ToLower() != "")
            //{
            //    foreach (var job in recruitedPostedJobs)
            //    {
            //        recruiterCompany = recruiterCompany.Where(x => x.RecruiterId == job.RecruiterId).ToList();
            //        if (recruiterCompany.Any())
            //        {
            //            recruiterCompany = recruiterCompany.Where(x => x.CompanyName!=null && x.CompanyName.Trim().ToLower().Contains(searchText)).ToList();

            //            if (recruiterCompany.Count() > 0)
            //            {
            //                foreach (var company in recruiterCompany)
            //                {
            //                    recruiterCompanies.Add(company);
            //                }
            //            }
            //        }
            //    }
            //    recruitedPostedJobs = recruitedPostedJobs.Where(x => x.Title.Trim().ToLower().Contains(searchText) || (recruiterCompanies.Any(r => r.RecruiterId == x.RecruiterId))).ToList();
            //}

            if (searchText.Trim().ToLower() != "")
            {
                recruitedPostedJobs = recruitedPostedJobs.Where(x => x.Title.Trim().ToLower().Contains(searchText)).ToList();
            }


            if (searchLocation.ToLower().Trim() != "")
            {
                var countryDetails = await unitOfWork.Context.CountryDetails.ToListAsync();
                var countries = countryDetails.Where(c => c.Name.Trim().ToLower().Contains(searchLocation.ToLower().Trim())).ToList();
                recruitedPostedJobs = recruitedPostedJobs.Where(x => (countries.Any(r => r.Id == x.CountryId))).ToList();
            }


            if (employmentType.Count() > 0)
            {
                var employeeTypes = await unitOfWork.Context.EmployeeType.ToListAsync();
                var employementTypes = employeeTypes.Where(c => employmentType.Any(et => et.Trim().ToLower() == c.Name.Trim().ToLower())).ToList();

                recruitedPostedJobs = recruitedPostedJobs.Where(x => employementTypes.Any(et => et.Id == x.EmploymentType)).ToList();
            }

            return recruitedPostedJobs;
        }
        public async Task<IEnumerable<RecruiterPostedJobs>> GetTopEightLatestPostedJobs()
        {
            try
            {
               // var recruitedCompany = unitOfWork.Context.RecruiterCompany.Where(x => x.IsActive && !x.IsDeleted).ToList();

                var recruiter = unitOfWork.Context.Recruiter.Where(x => x.IsActive && !x.IsDeleted).ToList();

               // recruiter = recruiter.Where(x => recruitedCompany.Any(r => r.RecruiterId == x.RecruiterId)).ToList();

                var jobs= unitOfWork.Context.RecruiterPostedJobs.Where(x =>!x.IsDeleted).ToList();

                return jobs.Where(x=>recruiter.Any(r => r.RecruiterId == x.RecruiterId)).OrderByDescending(x => (x.UpdatedOn.Date > x.CreatedOn.Date ? x.UpdatedOn : x.CreatedOn)).Take(8).ToList();

            }
            catch (Exception ex)
            {

            }

            return null;
        }
        public long NoOfJobs()
        {
            //var recruiterCompany = unitOfWork.Context.RecruiterCompany.Where(x => x.IsActive && !x.IsDeleted).ToList();

            var recruiter = unitOfWork.Context.Recruiter.Where(x => x.IsActive && !x.IsDeleted).ToList();

            //recruiter = recruiter.Where(x => recruiterCompany.Any(r => r.RecruiterId == x.RecruiterId)).ToList();

            var recruitedPostedJobs = unitOfWork.Context.RecruiterPostedJobs.Where(x => !x.IsDeleted).ToList();

            return recruitedPostedJobs.Where(x => recruiter.Any(r => r.RecruiterId == x.RecruiterId)).OrderByDescending(x => (x.UpdatedOn.Date > x.CreatedOn.Date ? x.UpdatedOn : x.CreatedOn)).Count();
        }

        public async Task<IEnumerable<RecruiterPostedJobs>> PostedJobsByRecruiterId(long recruiterId)
        {
            return await unitOfWork.Context.RecruiterPostedJobs.Where(x => !x.IsDeleted && x.RecruiterId == recruiterId).ToListAsync();
        }
        public async Task<bool> DeleteJobsByRecruiterId(long recruiterId)
        {
            try
            {
                var list = await unitOfWork.Context.RecruiterPostedJobs.Where(x => !x.IsDeleted && x.RecruiterId == recruiterId).ToListAsync();
                foreach (var item in list)
                {
                    item.UpdatedOn = DateTime.Now;
                    item.IsDeleted = true;
                    unitOfWork.Context.Entry(item).State = EntityState.Modified;
                    unitOfWork.Context.RecruiterPostedJobs.Update(item);
                    unitOfWork.Commit();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
