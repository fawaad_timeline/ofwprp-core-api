﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ofwpro.Core;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IFileUploadRepository
    {
        Task<IEnumerable<FileUpload>> Get(long fileId);
        Task<IEnumerable<FileUpload>> GetByModule(long masterId, string module);
        long Save(FileUpload fileUpload);
        bool Delete(long fileId);
        bool DeleteByModule(long masterId, string module);
    }
}
