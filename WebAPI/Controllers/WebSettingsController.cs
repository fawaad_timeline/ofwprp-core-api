﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Ofwpro.Core;
using Ofwpro.Services.Interfaces;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class WebSettingsController : ControllerBase
    {
        private readonly IUserResolverService userResolverService;
        private readonly IWebSettingsService WebSettingsService;
        private readonly IConfiguration config;

        public WebSettingsController(IUserResolverService userResolverService, IWebSettingsService WebSettingsService, IConfiguration config)
        {
            this.userResolverService = userResolverService;
            this.WebSettingsService = WebSettingsService;
            this.config = config;
        }

        [HttpGet]
        [Route("en/WebSettings/AllWebSettings")]
        public async Task<object> All()
        {
            int webSettingId = 1;
            WebSettings WebSettingsObj = new WebSettings();
            var WebSettingsList = await WebSettingsService.All(webSettingId);
            if (WebSettingsList != null)
            {
                return WebSettingsList;
            }
            else
            {
            }
            return "No data found.";
        }

        [HttpGet("en/WebSettings/GetWebSettings")]
        public async Task<IActionResult> GetWebSettings()
        {
            int id = 1;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            isSuccess = true;
            StatusCode = 200;
            Message = "Success";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();
                bool hasRight = true;
                // Check rights
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Get details
                if (isSuccess == true)
                {
                    var WebSettingsObj = await WebSettingsService.GetById(id);
                    long sessionId = id;

                    if (WebSettingsObj != null)
                    {
                        response.Data = WebSettingsObj;
                    }
                    else
                    {
                        StatusCode = 404;
                        isSuccess = false;
                        Message = "No data found.";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        [HttpPost("en/WebSettings/AddUpdateWebSettings")]
        public async Task<IActionResult> AddUpdateWebSettings([FromBody] WebSettingsRequestDTO model)
        {
            model.Id = 1;
            ResponseDTO<object> response = new ResponseDTO<object>();
            WebSettings WebSettingsObjOld = new WebSettings();
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been saved successfully";
            string ExceptionMessage = "";

            string lan = "";

            try
            {
                bool hasRight = true;
                // Check existance
                if (isSuccess == true && model.Id > 0)
                {
                    WebSettingsObjOld = await WebSettingsService.GetById(model.Id);
                    if (WebSettingsObjOld == null)
                    {
                        WebSettingsObjOld = new WebSettings();
                        WebSettingsObjOld.Id = 0;
                        WebSettingsObjOld.AboutHtml = model.AboutHtml;
                        WebSettingsObjOld.PrivacyPolicyHtml = model.PrivacyPolicyHtml;
                        WebSettingsObjOld.TermsAndConditionsHtml = model.TermsAndConditionsHtml;
                        // Save WebSettings                   
                        long sessionId = WebSettingsService.AddUpdate(WebSettingsObjOld);
                    }
                }
                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    if (!String.IsNullOrEmpty(model.AboutHtml))
                    {
                        WebSettingsObjOld.AboutHtml = model.AboutHtml;
                    }
                    if (!String.IsNullOrEmpty(model.PrivacyPolicyHtml))
                    {
                        WebSettingsObjOld.PrivacyPolicyHtml = model.PrivacyPolicyHtml;
                    }
                    if (!String.IsNullOrEmpty(model.TermsAndConditionsHtml))
                    {
                        WebSettingsObjOld.TermsAndConditionsHtml = model.TermsAndConditionsHtml;
                    }
                    // Save WebSettings                   
                    long sessionId = WebSettingsService.AddUpdate(WebSettingsObjOld);

                    if (sessionId < 0)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be saved.";
                        ExceptionMessage = "";
                    }
                    else
                    {
                        Data = WebSettingsObjOld;
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while saving data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

     
        //[HttpDelete("en/WebSettings/DeleteWebSettings")]
        //public async Task<IActionResult> DeleteWebSettings(DeleteRequestDTO model)
        //{
        //    long id = model.id;
        //    ResponseDTO<object> response = new ResponseDTO<object>();
        //    bool isExist = false;

        //    int StatusCode = 200;
        //    bool isSuccess = true;
        //    object Data = null;
        //    string Message = "Information has been deleted successfully";
        //    string ExceptionMessage = "";

        //    try
        //    {
        //        ApplicationUser currentUser = await userResolverService.GetCurrentUser();

        //        //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.WebSettingsManagement.ToString());
        //        bool hasRight = true;

        //        // Check righs
        //        if (!hasRight)
        //        {
        //            StatusCode = 403;
        //            isSuccess = false;
        //            Message = "Forbidden! You are not authorized to perform this task.";
        //        }

        //        // Check existance
        //        WebSettings WebSettingsObj = await WebSettingsService.GetById(id);
        //        if (WebSettingsObj == null)
        //        {
        //            StatusCode = 404;
        //            isSuccess = false;
        //            Message = "No data found";
        //            ExceptionMessage = "";
        //        }

        //        // Delete
        //        if (isSuccess == true)
        //        {
        //            bool result = WebSettingsService.Delete(id);
        //            if (result == false)
        //            {
        //                StatusCode = 500;
        //                isSuccess = false;
        //                Message = "Information could not be deleted";
        //                ExceptionMessage = "";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        StatusCode = 500;
        //        Message = "Information could not be deleted";
        //        ExceptionMessage = ex.Message.ToString();
        //    }

        //    response.StatusCode = StatusCode;
        //    response.IsSuccess = isSuccess;
        //    response.Data = Data;
        //    response.Message = Message;
        //    response.ExceptionMessage = ExceptionMessage;

        //    return ResponseHelper<object>.GenerateResponse(response);
        //}
    }
}