﻿using BlazorInputFile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ofwpro.Core;
using Ofwpro.Services.DTOs;

namespace OFWClientPanel.Services
{
    public interface INotificationService
    {
        Task<object> SendNotificationToJobSeekerAsync(string deviceToken, string title, object data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj);
        Task<object> SendNotificationToRecruiterAsync(string deviceToken, string title, object data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj);
        Task<object> SendPusher(string deviceToken, string title, object data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj, SaveChatJobSeekerRequestDTO saveChatJobSeekerRequestDTO);
        Task<object> SendPusherJs(string deviceToken, string title, object data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj, SaveChatRecruiterRequestDTO saveChatRecruiterRequestDTO);
    }
}
