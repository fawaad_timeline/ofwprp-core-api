﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JobSeekerAttachmentResponseDTO
    {
        public long Id { get; set; }
        public string DocumentType { get; set; }
        public string FileNameOriginal { get; set; }
        public string FileNameUpdated { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }        
    }
}
