﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class JobSeekerSavePersonalInformationRequestDTO
    {
        public string token { get; set; }
        public string country_id_enc { get; set; }
        public string marital_status_id_enc { get; set; }
        public string home_phone { get; set; }
        public string mobileCode { get; set; }
        public string is_driving_license { get; set; }
        public string current_location { get; set; }
        public string visa_status { get; set; }
    }
}
