﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class DeleteSpecializationRequestDTO
    {
        public long id { get; set; }
    }
}
