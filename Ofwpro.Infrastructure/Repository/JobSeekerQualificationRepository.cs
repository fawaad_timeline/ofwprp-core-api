﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class JobSeekerQualificationRepository : IJobSeekerQualificationRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public JobSeekerQualificationRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<JobSeekerQualification>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerQualification jobSeekerQualificationObj)
        {
            var predicate = PredicateBuilder.New<JobSeekerQualification>(true);
            var predicateInner = PredicateBuilder.New<JobSeekerQualification>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                //predicateInner = predicateInner.Or(i => i.JobSeekerQualificationNameEn.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.JobSeekerQualificationNameAr.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (jobSeekerQualificationObj.JobSeekerQualificationId > 0)
            {
                predicate = predicate.And(i => i.JobSeekerQualificationId == jobSeekerQualificationObj.JobSeekerQualificationId);
                isPradicate = true;
            }
            //if (!string.IsNullOrEmpty(jobSeekerQualificationObj.JobSeekerQualificationNameEn))
            //{
            //    predicate = predicate.And(i => i.JobSeekerQualificationNameEn.Contains(jobSeekerQualificationObj.JobSeekerQualificationNameEn));
            //    isPradicate = true;
            //}
            //if (!string.IsNullOrEmpty(jobSeekerQualificationObj.JobSeekerQualificationNameAr))
            //{
            //    predicate = predicate.And(i => i.JobSeekerQualificationNameAr.Contains(jobSeekerQualificationObj.JobSeekerQualificationNameAr));
            //    isPradicate = true;
            //}
            if (!string.IsNullOrEmpty(jobSeekerQualificationObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == jobSeekerQualificationObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.JobSeekerQualification.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.JobSeekerQualification.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.JobSeekerQualification.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.JobSeekerQualification.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<JobSeekerQualification> GetById(long jobSeekerQualificationId)
        {
            return unitOfWork.Context.JobSeekerQualification.Where(x => !x.IsDeleted && x.JobSeekerQualificationId == jobSeekerQualificationId).FirstOrDefaultAsync();
        }

        public long Save(JobSeekerQualification jobSeekerQualificationObj)
        {
            if (jobSeekerQualificationObj.JobSeekerQualificationId <= 0)
            {
                jobSeekerQualificationObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<JobSeekerQualification>().Add(jobSeekerQualificationObj);
            }
            else
            {
                jobSeekerQualificationObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(jobSeekerQualificationObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerQualification.Update(jobSeekerQualificationObj);
            }

            unitOfWork.Commit();
            unitOfWork.Context.Entry(jobSeekerQualificationObj).State = EntityState.Detached;
            return jobSeekerQualificationObj.JobSeekerQualificationId;
        }


        public bool Activate(long jobSeekerQualificationId, bool isActive)
        {
            JobSeekerQualification jobSeekerQualificationObj = unitOfWork.Context.JobSeekerQualification.Where(x => x.JobSeekerQualificationId == jobSeekerQualificationId).ToList().LastOrDefault();
            if (jobSeekerQualificationObj != null)
            {
                jobSeekerQualificationObj.IsActive = isActive;
                jobSeekerQualificationObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(jobSeekerQualificationObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerQualification.Update(jobSeekerQualificationObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long jobSeekerQualificationId)
        {
            var jobSeekerQualificationObj = unitOfWork.Context.JobSeekerQualification.Where(x => !x.IsDeleted && x.JobSeekerQualificationId == jobSeekerQualificationId).ToList().LastOrDefault();

            if (jobSeekerQualificationObj != null)
            {
                jobSeekerQualificationObj.UpdatedOn = DateTime.Now;
                jobSeekerQualificationObj.IsDeleted = true;
                unitOfWork.Context.Entry(jobSeekerQualificationObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerQualification.Update(jobSeekerQualificationObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }


        public Task<JobSeekerQualification> GetByJobSeekerId(long jobSeekerId)
        {
            return unitOfWork.Context.JobSeekerQualification.Where(x => !x.IsDeleted && x.JobSeekerId == jobSeekerId).FirstOrDefaultAsync();
        }

    }
}
