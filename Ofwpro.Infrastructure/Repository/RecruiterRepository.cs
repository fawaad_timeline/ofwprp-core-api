﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class RecruiterRepository: IRecruiterRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public RecruiterRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Recruiter>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Recruiter recruiterObj)
        {
            var predicate = PredicateBuilder.New<Recruiter>(true);
            var predicateInner = PredicateBuilder.New<Recruiter>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
            }

            // Dynamic Conditions
            if (recruiterObj.RecruiterId > 0)
            {
                predicate = predicate.And(i => i.RecruiterId == recruiterObj.RecruiterId);
                isPradicate = true;
            }
            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.Recruiter.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.Recruiter.Where(predicate).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.Recruiter.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.Recruiter.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<Recruiter> GetById(long recruiterId)
        {
            return unitOfWork.Context.Recruiter.Where(x => !x.IsDeleted && x.RecruiterId == recruiterId).FirstOrDefaultAsync();
        }

        public long Save(Recruiter recruiterObj)
        {
            if (recruiterObj.RecruiterId <= 0)
            {
                recruiterObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<Recruiter>().Add(recruiterObj);
            }
            else
            {
                recruiterObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(recruiterObj).State = EntityState.Modified;
                unitOfWork.Context.Recruiter.Update(recruiterObj);
            }

            unitOfWork.Commit();
            return recruiterObj.RecruiterId;
        }


        public bool Activate(long recruiterId, bool isActive)
        {
            Recruiter recruiterObj = unitOfWork.Context.Recruiter.Where(x => x.RecruiterId == recruiterId).ToList().LastOrDefault();
            if (recruiterObj != null)
            {
                recruiterObj.IsActive = isActive;
                recruiterObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(recruiterObj).State = EntityState.Modified;
                unitOfWork.Context.Recruiter.Update(recruiterObj);
                unitOfWork.Commit();
                unitOfWork.Context.Entry(recruiterObj).State = EntityState.Detached;
                return true;
            }
            return false;
        }


        public bool Delete(long recruiterId)
        {
            var recruiterObj = unitOfWork.Context.Recruiter.Where(x => !x.IsDeleted && x.RecruiterId == recruiterId).ToList().LastOrDefault();

            if (recruiterObj != null)
            {
                recruiterObj.UpdatedOn = DateTime.Now;
                recruiterObj.IsDeleted = true;
                unitOfWork.Context.Entry(recruiterObj).State = EntityState.Modified;
                unitOfWork.Context.Recruiter.Update(recruiterObj);
                unitOfWork.Commit();
                unitOfWork.Context.Entry(recruiterObj).State = EntityState.Detached;
                return true;
            }

            return false;
        }

        public bool IsExist(string email, long recruiterId)
        {
            var predicate = PredicateBuilder.New<Recruiter>(true);

            predicate = predicate.And(i => i.Email == email);
            if (recruiterId > 0)
            {
                predicate = predicate.And(i => i.RecruiterId != recruiterId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.Recruiter.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }
        
        public bool Validate(string email, string password)
        {
            var predicate = PredicateBuilder.New<Recruiter>(true);

            predicate = predicate.And(i => i.Email == email);
            predicate = predicate.And(i => i.Password == password);
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.Recruiter.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }
        public bool IsUserActivated(string email, string password)
        {
            var predicate = PredicateBuilder.New<Recruiter>(true);

            predicate = predicate.And(i => i.Email == email);
            predicate = predicate.And(i => i.Password == password);
            predicate = predicate.And(i => i.IsActive == true);

            var result = this.unitOfWork.Context.Recruiter.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public Task<Recruiter> GetByEmail(string email)
        {
            return unitOfWork.Context.Recruiter.Where(x => !x.IsDeleted && x.Email == email).FirstOrDefaultAsync();
        }

        public Task<Recruiter> GetByRecruiterIdToken(string recruiterIdToken)
        {
            return unitOfWork.Context.Recruiter.Where(x => !x.IsDeleted && x.RecruiterIdToken == recruiterIdToken).FirstOrDefaultAsync();
        }

        public Task<Recruiter> GetByActivationToken(string accActivationToken)
        {
            return unitOfWork.Context.Recruiter.Where(x => !x.IsDeleted && x.AccActivationToken == accActivationToken).FirstOrDefaultAsync();
        }
        public long NoOfCompanies()
        {
            return unitOfWork.Context.Recruiter.Where(x => !x.IsDeleted).Count();
        }
    }
}
