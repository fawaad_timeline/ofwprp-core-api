﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ofwpro.Core
{
    public class WebsiteVisitorCounter
    {
        protected long websiteVisitorCounterId, visitorCount;
        protected string visitorId, createdBy, updatedBy;
        protected DateTime visitedDate, createdOn, updatedOn;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long WebsiteVisitorCounterId { get => websiteVisitorCounterId; set => websiteVisitorCounterId = value; }
        public long VisitorCount { get => visitorCount; set => visitorCount = value; }
        public string VisitorId { get => visitorId; set => visitorId = value; }
        public DateTime VisitedDate { get => visitedDate; set => visitedDate = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }
    }
}
