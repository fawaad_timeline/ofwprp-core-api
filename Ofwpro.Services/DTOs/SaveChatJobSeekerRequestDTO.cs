﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ofwpro.Services.DTOs
{
    public class SaveChatJobSeekerRequestDTO
    {
        public string message_from { get; set; }
        public string sender_name { get; set; }
        public long job_seeker_id_enc { get; set; }
        public long recruiter_id_enc { get; set; }
        public int total_messages_unread { get; set; }
        public string message { get; set; }
        public DateTime added_date { get; set; }
        public string country_no { get; set; }
        public string country_code { get; set; }
        //public string user_type { get; set; }        
        public string email { get; set; }
        public string password { get; set; }
    }
}
