﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
	public class CountryDetailsService : ICountryDetailsService
	{
		private readonly ICountryDetailRepository _ICountryDetailRepository;
		public CountryDetailsService(ICountryDetailRepository iCountryDetailRepository)
		{
			_ICountryDetailRepository = iCountryDetailRepository;
		}

		public async Task<IEnumerable<CountryDetails>> GetAll()
		{
			return await _ICountryDetailRepository.GetAll();
		}

		public CountryDetails GetById(long id)
		{
			return _ICountryDetailRepository.GetById(id);
		}

		public CountryDetails GetByCode(string code)
		{
			return _ICountryDetailRepository.GetByCode(code);
		}
	}
}
