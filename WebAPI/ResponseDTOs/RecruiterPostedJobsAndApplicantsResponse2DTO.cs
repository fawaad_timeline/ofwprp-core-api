﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class RecruiterPostedJobsAndApplicantsResponse2DTO
    {
        public long recruiter_posted_jobs_and_applicants_id_enc { get; set; }
        public long recruiter_posted_jobs_id_enc { get; set; }
        public string reference_no { get; set; }
        public string title { get; set; }
        public string country_name { get; set; }
        public string city { get; set; }
        public DateTime date { get; set; }
        public string total_message_against_job { get; set; }
        public string total_message_against_job_unread { get; set; }
        public string logo_url { get; set; }
        public string is_active { get; set; }
    }
}
