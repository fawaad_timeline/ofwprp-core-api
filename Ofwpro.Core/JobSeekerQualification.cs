﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class JobSeekerQualification
    {
        protected long jobSeekerQualificationId, jobSeekerId, educationLevelId, specializationId;
        protected string nameOfDegree, schoolUniversity, createdBy, updatedBy;
        protected DateTime graduationDate, createdOn, updatedOn;
        protected bool isActive, isDeleted;

        //protected string detailedAddress, originalAddress, englishMasterLevel, frenchMasterLevel, otherLanguages;
        //protected DateTime yearOfStudying;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long JobSeekerQualificationId { get => jobSeekerQualificationId; set => jobSeekerQualificationId = value; }
        public long JobSeekerId { get => jobSeekerId; set => jobSeekerId = value; }
        public string NameOfDegree { get => nameOfDegree; set => nameOfDegree = value; }
        public long EducationLevelId { get => educationLevelId; set => educationLevelId = value; }
        public long SpecializationId { get => specializationId; set => specializationId = value; }
        public string SchoolUniversity { get => schoolUniversity; set => schoolUniversity = value; }
        public DateTime GraduationDate { get => graduationDate; set => graduationDate = value; }

        public bool IsActive { get => isActive; set => isActive = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }

        //public string DetailedAddress { get => detailedAddress; set => detailedAddress = value; }
        //public string OriginalAddress { get => originalAddress; set => originalAddress = value; }
        //public string EnglishMasterLevel { get => englishMasterLevel; set => englishMasterLevel = value; }
        //public string FrenchMasterLevel { get => frenchMasterLevel; set => frenchMasterLevel = value; }
        //public string OtherLanguages { get => otherLanguages; set => otherLanguages = value; }
        //public DateTime YearOfStudying { get => yearOfStudying; set => yearOfStudying = value; }

    }
}
