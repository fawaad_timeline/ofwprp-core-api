﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class AdminUserListRequestDTO
    {
        //public string national_number { get; set; }
        public string Token { get; set; }
        public int offset { get; set; }
        public int pagination { get; set; }       
    }
}
