﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class WebsiteVisitorCounterService : IWebsiteVisitorCounterService
    {
        private readonly IWebsiteVisitorCounterRepository websiteVisitorCounterRepository;

        public WebsiteVisitorCounterService(IWebsiteVisitorCounterRepository websiteVisitorCounterRepository)
        {
            this.websiteVisitorCounterRepository = websiteVisitorCounterRepository;
        }
        public async Task<IEnumerable<WebsiteVisitorCounter>> GetAll()
        {
            return await websiteVisitorCounterRepository.GetAll();
        }
        public Task<WebsiteVisitorCounter> GetByVisitorId(string visitorId)
        {
            return websiteVisitorCounterRepository.GetByVisitorId(visitorId);
        }
        public long Save(WebsiteVisitorCounter websiteVisitorCounter)
        {
            return websiteVisitorCounterRepository.Save(websiteVisitorCounter);
        }
    }
}
