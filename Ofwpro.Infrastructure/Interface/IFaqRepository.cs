﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IFaqRepository
    {
        Task<IEnumerable<Faq>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Faq faqObj);
        Task<Faq> GetById(long faqId);
        long Save(Faq faqObj);
        bool Activate(long faqId, bool isActive);
        bool Delete(long faqId);
        bool IsExist(string faqName, long FaqId);

        Task<IEnumerable<Faq>> GetAll();
    }
}
