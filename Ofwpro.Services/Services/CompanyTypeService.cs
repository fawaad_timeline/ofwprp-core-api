﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class CompanyTypeService : ICompanyTypeService
    {
		private readonly ICompanyTypeRepository _ICompanyTypeRepository;
		public CompanyTypeService(ICompanyTypeRepository iCompanyTypeRepository)
		{
			_ICompanyTypeRepository = iCompanyTypeRepository;
		}

        public long Add(CompanyType companyType)
        {
            return _ICompanyTypeRepository.Add(companyType);
        }

        public void Delete(long id)
        {
            _ICompanyTypeRepository.Delete(id);
        }

        public async Task<IEnumerable<CompanyType>> GetAll()
		{
			return await _ICompanyTypeRepository.GetAll();
		}

		public CompanyType GetById(long id)
		{
			return _ICompanyTypeRepository.GetById(id);
		}

        public bool IsExists(string name)
        {
            return _ICompanyTypeRepository.IsExists(name);
        }

        public long Update(CompanyType companyType)
        {
            return _ICompanyTypeRepository.Update(companyType);
        }
    }
}
