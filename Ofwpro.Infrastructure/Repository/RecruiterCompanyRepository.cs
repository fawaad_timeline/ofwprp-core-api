﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class RecruiterCompanyRepository: IRecruiterCompanyRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public RecruiterCompanyRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<RecruiterCompany>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, RecruiterCompany recruiterCompanyObj)
        {
            var predicate = PredicateBuilder.New<RecruiterCompany>(true);
            var predicateInner = PredicateBuilder.New<RecruiterCompany>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                //predicateInner = predicateInner.Or(i => i.RecruiterCompanyNameEn.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.RecruiterCompanyNameAr.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (recruiterCompanyObj.RecruiterCompanyId > 0)
            {
                predicate = predicate.And(i => i.RecruiterCompanyId == recruiterCompanyObj.RecruiterCompanyId);
                isPradicate = true;
            }
            //if (!string.IsNullOrEmpty(recruiterCompanyObj.RecruiterCompanyNameEn))
            //{
            //    predicate = predicate.And(i => i.RecruiterCompanyNameEn.Contains(recruiterCompanyObj.RecruiterCompanyNameEn));
            //    isPradicate = true;
            //}
            //if (!string.IsNullOrEmpty(recruiterCompanyObj.RecruiterCompanyNameAr))
            //{
            //    predicate = predicate.And(i => i.RecruiterCompanyNameAr.Contains(recruiterCompanyObj.RecruiterCompanyNameAr));
            //    isPradicate = true;
            //}
            if (!string.IsNullOrEmpty(recruiterCompanyObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == recruiterCompanyObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.RecruiterCompany.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.RecruiterCompany.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.RecruiterCompany.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.RecruiterCompany.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<RecruiterCompany> GetById(long recruiterCompanyId)
        {
            return unitOfWork.Context.RecruiterCompany.Where(x => !x.IsDeleted && x.RecruiterCompanyId == recruiterCompanyId).FirstOrDefaultAsync();
        }

        public long Save(RecruiterCompany recruiterCompanyObj)
        {
            if (recruiterCompanyObj.RecruiterCompanyId <= 0)
            {
                recruiterCompanyObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<RecruiterCompany>().Add(recruiterCompanyObj);
            }
            else
            {
                recruiterCompanyObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(recruiterCompanyObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterCompany.Update(recruiterCompanyObj);
            }

            unitOfWork.Commit();
            return recruiterCompanyObj.RecruiterCompanyId;
        }


        public bool Activate(long recruiterCompanyId, bool isActive)
        {
            RecruiterCompany recruiterCompanyObj = unitOfWork.Context.RecruiterCompany.Where(x => x.RecruiterCompanyId == recruiterCompanyId).ToList().LastOrDefault();
            if (recruiterCompanyObj != null)
            {
                recruiterCompanyObj.IsActive = isActive;
                recruiterCompanyObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(recruiterCompanyObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterCompany.Update(recruiterCompanyObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long recruiterCompanyId)
        {
            var recruiterCompanyObj = unitOfWork.Context.RecruiterCompany.Where(x => !x.IsDeleted && x.RecruiterCompanyId == recruiterCompanyId).ToList().LastOrDefault();

            if (recruiterCompanyObj != null)
            {
                recruiterCompanyObj.UpdatedOn = DateTime.Now;
                recruiterCompanyObj.IsDeleted = true;
                unitOfWork.Context.Entry(recruiterCompanyObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterCompany.Update(recruiterCompanyObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public Task<RecruiterCompany> GetByRecruiterId(long recruiterId)
        {
            var data = unitOfWork.Context.RecruiterCompany.Where(x => !x.IsDeleted && x.RecruiterId == recruiterId).FirstOrDefaultAsync();
            return data;
        }

    }
}
