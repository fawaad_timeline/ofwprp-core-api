﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IRecruiterPostedJobsRepository
    {
        Task<IEnumerable<RecruiterPostedJobs>> All(string orderByColumn, string orderBy, string searchTerm, RecruiterPostedJobs recruiterPostedJobsObj);
        Task<RecruiterPostedJobs> GetById(long recruiterPostedJobsId);
        long Save(RecruiterPostedJobs recruiterPostedJobsObj);
        bool Activate(long recruiterPostedJobsId, bool isActive);
        bool Delete(long recruiterPostedJobsId);
        Task<RecruiterPostedJobs> GetByRecruiterId(long recruiterId);
        bool IsExist(string referenceNo);
        bool IsExistRecruiterPostedJobsId(long recruiterPostedJobsId);

        bool RemoveRange(List<RecruiterPostedJobs> recruiterPostedJobs);
        Task<IEnumerable<RecruiterPostedJobs>> Filter(string searchText, string searchLocation, List<string> employeeType);
        Task<IEnumerable<RecruiterPostedJobs>> GetTopEightLatestPostedJobs();
        long NoOfJobs();
        Task<IEnumerable<RecruiterPostedJobs>> PostedJobsByRecruiterId(long recruiterId);
        Task<bool> DeleteJobsByRecruiterId(long recruiterId);
    }
}
