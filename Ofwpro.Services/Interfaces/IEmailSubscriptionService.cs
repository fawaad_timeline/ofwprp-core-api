﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ofwpro.Services.Interfaces
{
    public interface IEmailSubscriptionService
    {
        long Save(EmailSubscription emailSubscription);
    }
}
