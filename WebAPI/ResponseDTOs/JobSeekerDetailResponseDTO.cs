﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JobSeekerDetailResponseDTO
    {
        public long tbl_job_seeker_id { get; set; }
        public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public string mobile { get; set; }
        public string country_no { get; set; }
        public string country_code { get; set; }
        public string email { get; set; }
        public string last_login { get; set; }
        public DateTime added_date { get; set; }
        public string apple_device_token { get; set; }
        public string android_device_token { get; set; }
    }
}
