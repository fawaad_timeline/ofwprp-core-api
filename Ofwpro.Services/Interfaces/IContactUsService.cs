﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ofwpro.Services.Interfaces
{
    public interface IContactUsService
    {
        long Save(ContactUs contactUs);
        List<ContactUs> GetAll();
    }
}
