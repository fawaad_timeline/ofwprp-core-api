﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface IEducationLevelService
    {
        Task<Tuple<IEnumerable<EducationLevels>,int>> GetAll(int offset,int paging);
        EducationLevels GetById(long id);
        long Add(EducationLevels educationLevel);
        long Update(EducationLevels educationLevel);
        void Delete(long id);
        bool IsExists(string name);
    }
}
