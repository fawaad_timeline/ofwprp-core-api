﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ofwpro.Services.DTOs
{
    public class NewsSearchDTO
    {
        public int offset { get; set; }
        public int paging { get; set; }
        public string orderByColumn { get; set; }
        public string orderBy { get; set; }
        public string searchTerm { get; set; }

        public long NewsId { get; set; }
        public string NewsTitle { get; set; }
        public string NewsDescription { get; set; }
        public DateTime NewsDate { get; set; }
        public bool searchNewsDate { get; set; }

        public string Lan { get; set; }
    }
}
