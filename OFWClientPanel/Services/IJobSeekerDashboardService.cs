﻿using OFWClientPanel.Data.Models.JobSeeker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Services
{
    public interface IJobSeekerDashboardService
    {
        Task<PersonalInformation> GetJobSeekerPersonalInformation(long JobSeekerId);
        Task<Qualification> GetJobSeekerQulification(long JobSeekerId);
        Task<WorkExperience> GetJobSeekerWorkExperience(long JobSeekerId);
        Task<Attachments> GetJobSeekerAttachments(long JobSeekerId);
    }
}
