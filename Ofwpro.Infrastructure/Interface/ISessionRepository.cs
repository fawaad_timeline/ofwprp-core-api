﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface ISessionRepository
    {
        Task<IEnumerable<Session>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Session sessionObj);
        Task<Session> GetById(long sessionId);
        long Save(Session sessionObj);
        bool Activate(long sessionId, bool isActive);
        bool Delete(long sessionId);
        bool IsExist(string sessionName, long SessionId);
        public bool DeleteSessionBasedOnUser(long jobSeekerId, string userType);
        Task<Session> GetByToken(string token);
        bool DeleteByToken(string token);
        Task<Session> GetByUserId(long userId);
    }
}
