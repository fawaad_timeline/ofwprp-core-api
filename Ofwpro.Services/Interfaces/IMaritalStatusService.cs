﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface IMaritalStatusService
    {
        Task<IEnumerable<MaritalStatus>> GetAll();
        MaritalStatus GetById(long id);
        long Save(MaritalStatus data);
        bool Delete(long id);
    }
}
