﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class RecruiterPostedJobsAndApplicants
    {
        protected long recruiterPostedJobsAndApplicantsId, recruiterPostedJobsId, jobSeekerId;
        protected string comments, selectedForJob, selectedForInterview, createdBy, updatedBy;
        protected DateTime createdOn, updatedOn;
        protected bool isActive, isDeleted;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long RecruiterPostedJobsAndApplicantsId { get => recruiterPostedJobsAndApplicantsId; set => recruiterPostedJobsAndApplicantsId = value; }
        public long RecruiterPostedJobsId { get => recruiterPostedJobsId; set => recruiterPostedJobsId = value; }        
        public long JobSeekerId { get => jobSeekerId; set => jobSeekerId = value; }
        public string Comments { get => comments; set => comments = value; }
        public string SelectedForJob { get => selectedForJob; set => selectedForJob = value; }
        public string SelectedForInterview { get => selectedForInterview; set => selectedForInterview = value; }
        public bool IsActive { get => isActive; set => isActive = value; }

        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }
    }
}
