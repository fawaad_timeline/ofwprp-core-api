﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JobSeekerWorkExperienceDetailsResponseDTO
    {
        public List<JobSeekerWorkExperienceDetailsArrResponseDTO> work_experience { get; set; }
    }
}
