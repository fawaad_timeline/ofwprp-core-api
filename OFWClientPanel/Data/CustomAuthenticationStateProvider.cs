﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Ofwpro.Core;

namespace OFWClientPanel.Data
{
    public class CustomAuthenticationStateProvider : AuthenticationStateProvider
    {
        private ILocalStorageService localStorageService;

        public CustomAuthenticationStateProvider(ILocalStorageService localStorageService)
        {
            this.localStorageService = localStorageService;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var emailAddress = await localStorageService.GetItemAsync<string>("EmailAddress");
            ClaimsIdentity identity;

            if (emailAddress != null)
            {
               identity = new ClaimsIdentity(new[]
                   {
                       new Claim(ClaimTypes.Name, emailAddress),
                   }, "apiauth_type");
            }

            else
            {
                identity = new ClaimsIdentity();
            }
            var user = new ClaimsPrincipal(identity);

            return await Task.FromResult(new AuthenticationState(user));
        }

        public async Task MarkJobSeekerAsAuthenticated(JobSeeker jobSeeker)
        {
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, jobSeeker.Email),
            }, "apiauth_type");

            var user = new ClaimsPrincipal(identity);

            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(user)));
        }

        public async Task MarkRecruiterAsAuthenticated(Recruiter recruiter)
        {
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, recruiter.Email),
            }, "apiauth_type");

            var user = new ClaimsPrincipal(identity);

            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(user)));
        }

        public async Task MarkUserAsLoggedOut()
        {
            //await localStorageService.ClearAsync(); 
            await localStorageService.RemoveItemAsync("JobSeekerDetails");
            await localStorageService.RemoveItemAsync("EmailAddress");
            await localStorageService.RemoveItemAsync("UserType");
            await localStorageService.RemoveItemAsync("ProfilePictureURL");
            await localStorageService.RemoveItemAsync("RecruiterDetails");
            await localStorageService.RemoveItemAsync("CompanyName");
            await localStorageService.RemoveItemAsync("CompanyPhoneNumber");
            await localStorageService.RemoveItemAsync("CompanyLogoURL");

            var identity = new ClaimsIdentity();

            var user = new ClaimsPrincipal(identity);

            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(user)));
        }
    }
}
