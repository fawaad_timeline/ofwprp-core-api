﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class RecruiterAllRequestAdminDTO
    {
        public string pagination { get; set; }
        public string offset { get; set; }
        public string q { get; set; }
    }
}
