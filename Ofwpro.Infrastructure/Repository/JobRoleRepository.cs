﻿using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class JobRoleRepository : IJobRoleRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public JobRoleRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public long Add(JobRoles jobRole)
        {
            
            _unitOfWork.Context.Set<JobRoles>().Add(jobRole);
            _unitOfWork.Commit();
            return jobRole.Id;
        }

        public void Delete(long id)
        {
            var jobRole = _unitOfWork.Context.JobRoles.Where(x => x.Id == id).FirstOrDefault();
            if (jobRole != null)
            {
                _unitOfWork.Context.JobRoles.Remove(jobRole);
                _unitOfWork.Commit();
            }
        }

        public async Task<Tuple<IEnumerable<JobRoles>,int>> GetAll(int offset, int paging)
        {
            var count = await _unitOfWork.Context.JobRoles.CountAsync();
            var result = _unitOfWork.Context.JobRoles.OrderBy(x => x.Id);
            var dataList = await result.Skip(offset*paging).Take(paging).ToListAsync();
            return new Tuple<IEnumerable<JobRoles>, int>(dataList, count);
        }
        public JobRoles GetById(long id)
        {
            var maritalStatusList = _unitOfWork.Context.JobRoles.Where(x => x.Id == id).FirstOrDefault();
            return maritalStatusList;
        }

        public bool IsExists(string name)
        {
            bool IsExists = false;
            var jobRoles = _unitOfWork.Context.JobRoles.Where(x => x.Name == name);
            if (jobRoles.Any())
                IsExists = true;
            else
                IsExists = false;
            return IsExists;
        }

        public long Update(JobRoles jobRole)
        {
            _unitOfWork.Context.Entry(jobRole).State = EntityState.Modified;
            _unitOfWork.Context.JobRoles.Update(jobRole);
            _unitOfWork.Commit();
            return jobRole.Id;
        }
    }
}
