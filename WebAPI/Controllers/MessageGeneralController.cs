﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using MimeKit;
using MailKit.Net.Smtp;
using PusherServer;
using Ofwpro.Services.Interfaces;
using Ofwpro.Core;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class MessageGeneralController : ControllerBase
    {
        private readonly IFileUploadService fileUploadService;
        private readonly IJobSeekerService jobSeekerService;
        private readonly IAdministratorService administratorService;
        private readonly ISessionService sessionService;
        private readonly IJobSeekerPersonalInformationService jobSeekerPersonalInformationService;
        private readonly IJobSeekerQualificationService jobSeekerQualificationService;
        private readonly IJobSeekerWorkExperienceService jobSeekerWorkExperienceService;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IJobSeekerAttachmentService jobSeekerAttachmentService;
        private readonly IRecruiterPostedJobsService recruiterPostedJobsService;
        private readonly IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService;
        private readonly ICountryDetailsService countryDetailsService;
        private readonly IRecruiterAttachmentService recruiterAttachmentService;
        private readonly IForgotPasswordService forgotPasswordService;
        private readonly IMessageGeneralService messageGeneralService;
        private readonly IRecruiterService recruiterService;
        private readonly IRecruiterCompanyService recruiterCompanyService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public MessageGeneralController(IFileUploadService fileUploadService, IJobSeekerService jobSeekerService, IAdministratorService administratorService, ISessionService sessionService, IJobSeekerPersonalInformationService jobSeekerPersonalInformationService, IJobSeekerQualificationService jobSeekerQualificationService, IJobSeekerWorkExperienceService jobSeekerWorkExperienceService, IWebHostEnvironment hostingEnvironment, IJobSeekerAttachmentService jobSeekerAttachmentService, IRecruiterPostedJobsService recruiterPostedJobsService, IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService, ICountryDetailsService countryDetailsService, IRecruiterAttachmentService recruiterAttachmentService, IForgotPasswordService forgotPasswordService, IMessageGeneralService messageGeneralService, IRecruiterService recruiterService, IRecruiterCompanyService recruiterCompanyService, IMapper mapper, IConfiguration config)
        {
            this.fileUploadService = fileUploadService;
            this.jobSeekerService = jobSeekerService;
            this.administratorService = administratorService;
            this.sessionService = sessionService;
            this.jobSeekerPersonalInformationService = jobSeekerPersonalInformationService;
            this.jobSeekerQualificationService = jobSeekerQualificationService;
            this.jobSeekerWorkExperienceService = jobSeekerWorkExperienceService;
            this.hostingEnvironment = hostingEnvironment;
            this.jobSeekerAttachmentService = jobSeekerAttachmentService;
            this.recruiterPostedJobsService = recruiterPostedJobsService;
            this.recruiterPostedJobsAndApplicantsService = recruiterPostedJobsAndApplicantsService;
            this.countryDetailsService = countryDetailsService;
            this.recruiterAttachmentService = recruiterAttachmentService;
            this.forgotPasswordService = forgotPasswordService;
            this.messageGeneralService = messageGeneralService;
            this.recruiterService = recruiterService;
            this.recruiterCompanyService = recruiterCompanyService;
            this.mapper = mapper;
            this.config = config;
        }

        [HttpPost]
        [Route("en/message_general/ajax_all_messages_mobile")]
        public async Task<object> ajax_all_messages_mobile([FromBody] AllMessagesMobileRequestDTO model)
        {
            string token = model.token; long job_seeker_id_enc = model.job_seeker_id_enc;  long recruiter_id_enc = model.recruiter_id_enc;
            string lan = "en";

            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode=200 };
            List<RecruiterPostedJobsAndApplicantsResponse2DTO> RecruiterPostedJobsAndApplicantsResponse2DTOList = new List<RecruiterPostedJobsAndApplicantsResponse2DTO>();

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            JsonArrListResponseDTO JsonArrListResponseDTOObj = new JsonArrListResponseDTO();
            Dictionary<string, object> JsonResponseDict = new Dictionary<string, object>();

            string hostUrl = config["Utility:APIBaseURL"];
            bool isSuccess = true;

            Session sessionObj = null;

            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }
            }

            long jobSeekerId = 0;
            long recruiterId = 0;
            if (sessionObj.UserType.Equals("job_seeker"))
            {
                jobSeekerId = sessionObj.UserId;
            }
            else
            {
                recruiterId = sessionObj.UserId;
            }

            //if (jobSeekerId <= 0)
            //{
            //    jobSeekerId = job_seeker_id_enc;
            //}
            //if (recruiterId <= 0)
            //{
            //    recruiterId = recruiter_id_enc;
            //}

            string userNameJobSeekerTo = "";
            string userNameJobSeeker = "";
            string userNameRecruiter = "";

            if (jobSeekerId <=0 && sessionObj.UserType.Equals("recruiter"))
            {
                IEnumerable<MessageGeneral> messageGeneralList = await messageGeneralService.GetAllMessagesGeneral("CreatedOn", "DESC", "", true, 0, recruiterId);

                List<MessageGeneralRecruiterMobileResponseDTO> messageGeneralRecruiterMobileResponseDTOList = new List<MessageGeneralRecruiterMobileResponseDTO>();

               
                foreach (MessageGeneral messageGeneralObj in messageGeneralList)
                {
                    userNameJobSeekerTo = "";
                    userNameJobSeeker = "";
                    userNameRecruiter = "";
                    MessageGeneralRecruiterMobileResponseDTO MessageGeneralRecruiterMobileResponseDTOObj = new MessageGeneralRecruiterMobileResponseDTO();

                    /*MESSAGE TO*/
                    JobSeeker jobSeekerObjTo = await jobSeekerService.GetById(messageGeneralObj.JobSeekerId);
                    if (jobSeekerObjTo != null) {
                        userNameJobSeekerTo = jobSeekerObjTo.FirstName + " " + jobSeekerObjTo.LastName;
                    }

                    /*MESSAGE FROM: It can be from job seeker or recruiter*/
                    if (messageGeneralObj.UserType.Equals("job_seeker"))
                    {
                        JobSeeker jobSeekerObj = await jobSeekerService.GetById(messageGeneralObj.JobSeekerId);
                        if (jobSeekerObj!= null)
                        {
                            userNameJobSeeker = jobSeekerObj.FirstName + " " + jobSeekerObj.LastName;
                        }
                    }

                    if (messageGeneralObj.UserType.Equals("recruiter"))
                    {
                        RecruiterCompany recruiterCompanyObj = await recruiterCompanyService.GetByRecruiterId(messageGeneralObj.UserId);
                        if (recruiterCompanyObj != null)
                        {
                            userNameRecruiter = recruiterCompanyObj.CompanyName;
                        }
                    }

                    MessageGeneralRecruiterMobileResponseDTOObj.from_icon = hostUrl + "/images/icons/icon_recruiter.png";
                    MessageGeneralRecruiterMobileResponseDTOObj.from = userNameRecruiter;
                    MessageGeneralRecruiterMobileResponseDTOObj.to_icon = hostUrl + "/images/icons/icon_job_seeker.png";
                    MessageGeneralRecruiterMobileResponseDTOObj.to = userNameJobSeekerTo;
                    MessageGeneralRecruiterMobileResponseDTOObj.date = Convert.ToString(messageGeneralObj.CreatedOn);
                    MessageGeneralRecruiterMobileResponseDTOObj.message = messageGeneralObj.Message;
                    messageGeneralRecruiterMobileResponseDTOList.Add(MessageGeneralRecruiterMobileResponseDTOObj);
                }


                JsonResponseDict.Add("code", 1);
                JsonResponseDict.Add("MSG", "Success");
                JsonResponseDict.Add("data", messageGeneralRecruiterMobileResponseDTOList);
                return JsonResponseDict;

            } // if (jobSeekerId <=0 && sessionObj.UserType.Equals("recruiter"))




            IEnumerable<MessageGeneral> messageGeneralList_ = await messageGeneralService.GetAllMessagesGeneral("CreatedOn", "DESC", "", true, 0, jobSeekerId);

            List<MessageGeneralRecruiterMobileResponseDTO> messageGeneralRecruiterMobileResponseDTOList_ = new List<MessageGeneralRecruiterMobileResponseDTO>();
            
            foreach (MessageGeneral messageGeneralObj in messageGeneralList_)
            {
                userNameJobSeekerTo = "";
                userNameJobSeeker = "";
                userNameRecruiter = "";
                MessageGeneralRecruiterMobileResponseDTO MessageGeneralRecruiterMobileResponseDTOObj = new MessageGeneralRecruiterMobileResponseDTO();

                /*MESSAGE TO*/
                JobSeeker jobSeekerObjTo = await jobSeekerService.GetById(messageGeneralObj.JobSeekerId);
                if (jobSeekerObjTo != null)
                {
                    userNameJobSeekerTo = jobSeekerObjTo.FirstName + " " + jobSeekerObjTo.LastName;
                }                            

                RecruiterCompany recruiterCompanyObj = await recruiterCompanyService.GetByRecruiterId(messageGeneralObj.UserId);
                if (recruiterCompanyObj != null)
                {
                    userNameRecruiter = recruiterCompanyObj.CompanyName;
                }

                MessageGeneralRecruiterMobileResponseDTOObj.from_icon = hostUrl + "/images/icons/icon_recruiter.png";
                MessageGeneralRecruiterMobileResponseDTOObj.from = userNameRecruiter;
                MessageGeneralRecruiterMobileResponseDTOObj.to_icon = hostUrl + "/images/icons/icon_job_seeker.png";
                MessageGeneralRecruiterMobileResponseDTOObj.to = userNameJobSeekerTo;
                MessageGeneralRecruiterMobileResponseDTOObj.date = Convert.ToString(messageGeneralObj.CreatedOn);
                MessageGeneralRecruiterMobileResponseDTOObj.message = messageGeneralObj.Message;
                messageGeneralRecruiterMobileResponseDTOList_.Add(MessageGeneralRecruiterMobileResponseDTOObj);
            }

            JsonResponseDict = new Dictionary<string, object>();
            JsonResponseDict.Add("code", 1);
            JsonResponseDict.Add("MSG", "Success");
            JsonResponseDict.Add("data", messageGeneralRecruiterMobileResponseDTOList_);
            return JsonResponseDict;

        }
    }
}