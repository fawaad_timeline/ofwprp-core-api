﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ofwpro.Core
{
    public class CountryDetails
    {
        protected long _Id;
        protected string _Name, _Code;

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id
		{
			get => _Id;
			set => _Id = value;
		}
		public string Name
		{
			get => _Name;
			set => _Name = value;
		}
		public string Code
		{
			get => _Code;
			set => _Code = value;
		}
	}
}
