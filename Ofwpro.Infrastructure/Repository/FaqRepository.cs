﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class FaqRepository: IFaqRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public FaqRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Faq>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Faq faqObj)
        {
            var predicate = PredicateBuilder.New<Faq>(true);
            var predicateInner = PredicateBuilder.New<Faq>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                predicateInner = predicateInner.Or(i => i.Question.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.Answer.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (faqObj.FaqId > 0)
            {
                predicate = predicate.And(i => i.FaqId == faqObj.FaqId);
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(faqObj.Question))
            {
                predicate = predicate.And(i => i.Question.Contains(faqObj.Question));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(faqObj.Answer))
            {
                predicate = predicate.And(i => i.Answer.Contains(faqObj.Answer));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(faqObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == faqObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.Faq.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.Faq.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.Faq.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.Faq.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<Faq> GetById(long faqId)
        {
            return unitOfWork.Context.Faq.Where(x => !x.IsDeleted && x.FaqId == faqId).FirstOrDefaultAsync();
        }

        public long Save(Faq faqObj)
        {
            if (faqObj.FaqId <= 0)
            {
                faqObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<Faq>().Add(faqObj);
            }
            else
            {
                faqObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(faqObj).State = EntityState.Modified;
                unitOfWork.Context.Faq.Update(faqObj);
            }

            unitOfWork.Commit();
            return faqObj.FaqId;
        }


        public bool Activate(long faqId, bool isActive)
        {
            Faq faqObj = unitOfWork.Context.Faq.Where(x => x.FaqId == faqId).ToList().LastOrDefault();
            if (faqObj != null)
            {
                faqObj.IsActive = isActive;
                faqObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(faqObj).State = EntityState.Modified;
                unitOfWork.Context.Faq.Update(faqObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long faqId)
        {
            var faqObj = unitOfWork.Context.Faq.Where(x => !x.IsDeleted && x.FaqId == faqId).ToList().LastOrDefault();

            if (faqObj != null)
            {
                faqObj.UpdatedOn = DateTime.Now;
                faqObj.IsDeleted = true;
                unitOfWork.Context.Entry(faqObj).State = EntityState.Modified;
                unitOfWork.Context.Faq.Update(faqObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool IsExist(string Question, long faqId)
        {
            var predicate = PredicateBuilder.New<Faq>(true);

            predicate = predicate.And(i => i.Question == Question);
            if (faqId > 0)
            {
                predicate = predicate.And(i => i.FaqId != faqId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.Faq.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public async Task<IEnumerable<Faq>> GetAll()
        {
            return await unitOfWork.Context.Faq.Where(x => !x.IsDeleted).ToListAsync();
        }
    }
}
