﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class MaritalStatusRequestDTO
    {
        public long Id { get; set; }
        public string Rng { get; set; }
        public string Name { get; set; }
    }
}
