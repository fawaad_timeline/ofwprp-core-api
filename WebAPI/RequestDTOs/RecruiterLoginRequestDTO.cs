﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class RecruiterLoginRequestDTO
    {
        public string email { get; set; }
        public string password { get; set; }       
    }
}
