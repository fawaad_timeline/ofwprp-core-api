﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ofwpro.Services.Services
{
    public class EmailSubscriptionService: IEmailSubscriptionService
    {
        private readonly IEmailSubscriptionRepository emailSubscriptionRepository;

        public EmailSubscriptionService(IEmailSubscriptionRepository emailSubscriptionRepository)
        {
            this.emailSubscriptionRepository = emailSubscriptionRepository;
        }

        public long Save(EmailSubscription emailSubscription)
        {
            return emailSubscriptionRepository.Save(emailSubscription);
        }
    }
}
