﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class JobSeekerPersonalInformationService: IJobSeekerPersonalInformationService
    {
        private readonly IJobSeekerPersonalInformationRepository jobSeekerPersonalInformationRepository;

        public JobSeekerPersonalInformationService(IJobSeekerPersonalInformationRepository jobSeekerPersonalInformationRepository)
        {
            this.jobSeekerPersonalInformationRepository = jobSeekerPersonalInformationRepository;
        }

        public async Task<IEnumerable<JobSeekerPersonalInformation>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerPersonalInformation jobSeekerPersonalInformationObj)
        {
            return await jobSeekerPersonalInformationRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, jobSeekerPersonalInformationObj);
        }

        public async Task<JobSeekerPersonalInformation> GetById(long jobSeekerPersonalInformationId)
        {
            return await jobSeekerPersonalInformationRepository.GetById(jobSeekerPersonalInformationId);
        }

        public long Save(JobSeekerPersonalInformation jobSeekerPersonalInformationObj)
        {
            return jobSeekerPersonalInformationRepository.Save(jobSeekerPersonalInformationObj);
        }

        public bool Activate(long jobSeekerPersonalInformationId, bool isActive)
        {
            return jobSeekerPersonalInformationRepository.Activate(jobSeekerPersonalInformationId, isActive);
        }

        public bool Delete(long jobSeekerPersonalInformationId)
        {
            return jobSeekerPersonalInformationRepository.Delete(jobSeekerPersonalInformationId);
        }

        public Task<JobSeekerPersonalInformation> GetByJobSeekerId(long jobSeekerId)
        {
            return jobSeekerPersonalInformationRepository.GetByJobSeekerId(jobSeekerId);
        }

    }
}
