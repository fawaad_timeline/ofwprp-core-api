﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class AttachmentJobSeekerAdminResponseDTO
    {
        
        public string resume { get; set; }
        public string id_card { get; set; }
        public string passport { get; set; }
        public string video_resume { get; set; }
        public string picture { get; set; }

    }
}
