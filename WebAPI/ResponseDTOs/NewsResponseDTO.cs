﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class NewsResponseDTO
    {
        public long Id { get; set; }
        public string NewsTitle { get; set; }
        public string NewsDescription { get; set; }
        public DateTime NewsDate { get; set; }
        public string NewsDateStr { get; set; }
        public string Lan { get; set; }
        public string Icon { get; set; }

        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        //public List<FileResponseDTO> FilesList { get; set; }
    }
}
