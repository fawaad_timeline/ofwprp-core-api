﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class SalaryRangeService : ISalaryRangeService
    {
		private readonly ISalaryRangeRepository _ISalaryRangeRepository;
		public SalaryRangeService(ISalaryRangeRepository iSalaryRangeRepository)
		{
			_ISalaryRangeRepository = iSalaryRangeRepository;
		}

        public long Add(SalaryRange salaryRange)
        {
            return _ISalaryRangeRepository.Add(salaryRange);
        }

        public void Delete(long id)
        {
            _ISalaryRangeRepository.Delete(id);
        }

        public async Task<Tuple<IEnumerable<SalaryRange>,int>> GetAll(int offset,int paging)
		{
			return await _ISalaryRangeRepository.GetAll(offset, paging);
		}

		public SalaryRange GetById(long id)
		{
			return _ISalaryRangeRepository.GetById(id);
		}

        public bool IsExists(string name)
        {
            return _ISalaryRangeRepository.IsExists(name);
        }

        public long Update(SalaryRange salaryRange)
        {
            return _ISalaryRangeRepository.Update(salaryRange);
        }
    }
}
