﻿using Microsoft.Extensions.Configuration;
using OFWClientPanel.Data.Models.JobSeeker;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Services
{
    public class JobSeekerDashboardService:IJobSeekerDashboardService
    {
        private readonly IJobSeekerWorkExperienceService jobSeekerWorkExperienceService;
        private readonly IJobSeekerQualificationService jobSeekerQualificationService;
        private readonly IJobSeekerPersonalInformationService jobSeekerPersonalInformationService;
        private readonly IJobSeekerAttachmentService jobSeekerAttachmentService;
        private readonly IConfiguration config;

        public JobSeekerDashboardService(
        IJobSeekerWorkExperienceService jobSeekerWorkExperienceService,
        IJobSeekerQualificationService jobSeekerQualificationService,
        IJobSeekerPersonalInformationService jobSeekerPersonalInformationService,
        IJobSeekerAttachmentService jobSeekerAttachmentService,
        IConfiguration config)
            {
                this.jobSeekerWorkExperienceService = jobSeekerWorkExperienceService;
                this.jobSeekerQualificationService = jobSeekerQualificationService;
                this.jobSeekerPersonalInformationService = jobSeekerPersonalInformationService;
                this.jobSeekerAttachmentService = jobSeekerAttachmentService;
                this.config = config;
            }

        public async Task<PersonalInformation> GetJobSeekerPersonalInformation(long JobSeekerId)
        {
            PersonalInformation personalInformation = new PersonalInformation();

            try 
            {
                var personalInfoObj = await jobSeekerPersonalInformationService.GetByJobSeekerId(JobSeekerId);

                if (personalInfoObj != null)
                {
                    personalInformation.PersonalInformationId = personalInfoObj.JobSeekerPersonalInformationId;
                    personalInformation.CountryId = personalInfoObj.CountryId.ToString();
                    personalInformation.PhoneNumber = personalInfoObj.HomePhone;
                    personalInformation.CurrentLocation = personalInfoObj.CurrentLocation;
                    personalInformation.HasDrivingLicense = personalInfoObj.IsDrivingLicense;
                    personalInformation.MaritalStatusId = personalInfoObj.MaritalStatusId.ToString();
                    personalInformation.VisaStatus = personalInfoObj.VisaStatus;
                    personalInformation.NumberOfChildren = personalInfoObj.NumberOfChildren.ToString();
                    personalInformation.DisabilityDescription = personalInfoObj.DisabilityDescription;
                    personalInformation.NeedCompanion = personalInfoObj.NeedCompanion;
                    personalInformation.PeopleOfDisability = personalInfoObj.PeopleOfDisability;
                    personalInformation.DisabilityCardNumber = personalInfoObj.DisabilityCardNumber;
                    personalInformation.DateOfHavingDisabilityCard = personalInfoObj.DateOfHavingDisabilityCard;
                    personalInformation.City = personalInfoObj.City;
                    personalInformation.PlaceOfBirthId = personalInfoObj.PlaceOfBirthId.ToString();
                    personalInformation.IdentificationPapers = personalInfoObj.IdentificationPapers;
                    personalInformation.OriginalAddress = personalInfoObj.OriginalAddress;
                    personalInformation.CurrentCountryId = personalInfoObj.CurrentCountryId.ToString();
                    personalInformation.ExpectedHomeAddress = personalInfoObj.ExpectedHomeAddress;
                    personalInformation.ReturningHomeExpectedDate = personalInfoObj.ReturningHomeExpectedDate;
                }
            }
            catch(Exception ex) { }

            return personalInformation;
        }

        public async Task<Qualification> GetJobSeekerQulification(long JobSeekerId)
        {
            Qualification qualification = new Qualification();

            try 
            {
                var qualificationObj = await jobSeekerQualificationService.GetByJobSeekerId(JobSeekerId);

                if (qualificationObj != null)
                {
                    qualification.QualificationId = qualificationObj.JobSeekerQualificationId;
                    qualification.NameOfDegree = qualificationObj.NameOfDegree;
                    qualification.EducationLevelId = qualificationObj.EducationLevelId.ToString();
                    qualification.SpecializationId = qualificationObj.SpecializationId.ToString();
                    qualification.SchoolOrUniversity = qualificationObj.SchoolUniversity;
                    qualification.GraduationDate = qualificationObj.GraduationDate;
                }
            }
            catch (Exception ex) { }

            return qualification;
        }
        public async Task<WorkExperience> GetJobSeekerWorkExperience(long JobSeekerId)
        {
            WorkExperience workExperience = new WorkExperience();

            try
            {
                var workExperienceObj = await jobSeekerWorkExperienceService.GetByJobSeekerId(JobSeekerId);


                if (workExperienceObj != null)
                {
                    workExperience.WorkExperienceId = workExperienceObj.JobSeekerWorkExperienceId;
                    workExperience.ExperienceLevel = workExperienceObj.ExperienceLevel;

                    workExperience.Company1 = workExperienceObj.Company1;
                    workExperience.JobTitle1 = workExperienceObj.JobTitle1;
                    workExperience.City1 = workExperienceObj.City1;
                    workExperience.CountryId1 = workExperienceObj.CountryId1.ToString();
                    workExperience.TimePeriodFrom1 = Convert.ToDateTime(workExperienceObj.TimePeriodFrom1);
                    workExperience.TimePeriodTo1 = Convert.ToDateTime(workExperienceObj.TimePeriodTo1);
                    workExperience.ReasonForLeaving1 = workExperienceObj.ReasonForLeaving1;

                    workExperience.Company2 = workExperienceObj.Company2;
                    workExperience.JobTitle2 = workExperienceObj.JobTitle2;
                    workExperience.City2 = workExperienceObj.City2;
                    workExperience.CountryId2 = workExperienceObj.CountryId2.ToString();
                    workExperience.TimePeriodFrom2 = Convert.ToDateTime(workExperienceObj.TimePeriodFrom2);
                    workExperience.TimePeriodTo2 = Convert.ToDateTime(workExperienceObj.TimePeriodTo2);
                    workExperience.ReasonForLeaving2 = workExperienceObj.ReasonForLeaving2;

                    workExperience.Company3 = workExperienceObj.Company3;
                    workExperience.JobTitle3 = workExperienceObj.JobTitle3;
                    workExperience.City3 = workExperienceObj.City3;
                    workExperience.CountryId3 = workExperienceObj.CountryId3.ToString();
                    workExperience.TimePeriodFrom3 = Convert.ToDateTime(workExperienceObj.TimePeriodFrom3);
                    workExperience.TimePeriodTo3 = Convert.ToDateTime(workExperienceObj.TimePeriodTo3);
                    workExperience.ReasonForLeaving3 = workExperienceObj.ReasonForLeaving3;
                }
            }
            catch (Exception ex) { }

            return workExperience;
        }

        public async Task<Attachments> GetJobSeekerAttachments(long JobSeekerId)
        {
            Attachments attachments = new Attachments();

            try 
            {
                string hostUrl = config["Utility:ClientBaseURL"].ToString();
                string uploadFolderName = config["UploadFolders:UploadFolder"];

                var attachmetObj = await jobSeekerAttachmentService.GetByJobSeekerId(JobSeekerId);

                if (attachmetObj.Any())
                {
                    foreach (var attachment in attachmetObj)
                    {
                        string UploadedFrom = "";
                        if (attachment.UploadedFrom != null) UploadedFrom = attachment.UploadedFrom;

                        if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                        else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();

                        if (attachment.DocumentType == "resume")
                        {
                            attachments.ResumeURL = hostUrl + uploadFolderName + "/" + attachment.FileNameUpdated;
                            attachments.ResumeFileType = attachment.FileType;
                            attachments.ResumeDocumentId = attachment.JobSeekerAttachmentId;
                        }

                        else if (attachment.DocumentType == "passport")
                        {
                            attachments.PassportURL = hostUrl + uploadFolderName + "/" + attachment.FileNameUpdated;
                            attachments.PassportFileType = attachment.FileType;
                            attachments.PassportDocumentId = attachment.JobSeekerAttachmentId;
                        }

                        else if (attachment.DocumentType == "id_card")
                        {
                            attachments.IDCardURL = hostUrl + uploadFolderName + "/" + attachment.FileNameUpdated;
                            attachments.IDCardFileType = attachment.FileType;
                            attachments.IDCardDocumentId = attachment.JobSeekerAttachmentId;
                        }

                        else if (attachment.DocumentType == "picture")
                        {
                            attachments.PictureURL = hostUrl + uploadFolderName + "/" + attachment.FileNameUpdated;
                            attachments.PictureFileType = attachment.FileType;
                            attachments.PictureId = attachment.JobSeekerAttachmentId;
                        }

                        else if (attachment.DocumentType == "video_resume")
                        {
                            attachments.VideoURL = hostUrl + uploadFolderName + "/" + attachment.FileNameUpdated;
                            attachments.VideoFileType = attachment.FileType;
                            attachments.VideoId = attachment.JobSeekerAttachmentId;
                        }
                    }
                }
            }
            catch (Exception ex) { }

            return attachments;
        }
    }
}
