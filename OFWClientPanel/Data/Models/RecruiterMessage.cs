﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models
{
    public class RecruiterMessage
    {
        public long JobSeekerId { get; set; }
        public long RecruiterId { get; set; }
        public string UserType { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
        public bool Checked { get; set; }

        public string Firstname { get; set; }
        public string LastName { get; set; }
        public long TotalMessages { get; set; }
        public long TotalUnreadMessages { get; set; }

        public string JobSeekerProfile { get; set; }
    }
}
