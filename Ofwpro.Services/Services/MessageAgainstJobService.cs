﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class MessageAgainstJobService: IMessageAgainstJobService
    {
        private readonly IMessageAgainstJobRepository messageAgainstJobRepository;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public MessageAgainstJobService(IMessageAgainstJobRepository messageAgainstJobRepository, IMapper mapper, IConfiguration config)
        {
            this.messageAgainstJobRepository = messageAgainstJobRepository;
            this.mapper = mapper;
            this.config = config;
        }

        public async Task<IEnumerable<MessageAgainstJob>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, MessageAgainstJob messageGeneralObj)
        {
            return await messageAgainstJobRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, messageGeneralObj);
        }

        public async Task<MessageAgainstJob> GetById(long messageGeneralId)
        {
            return await messageAgainstJobRepository.GetById(messageGeneralId);
        }

        public long Save(MessageAgainstJob messageGeneralObj)
        {
            return messageAgainstJobRepository.Save(messageGeneralObj);
        }

        public bool Activate(long messageGeneralId, bool isActive)
        {
            return messageAgainstJobRepository.Activate(messageGeneralId, isActive);
        }

        public bool Delete(long messageGeneralId)
        {
            return messageAgainstJobRepository.Delete(messageGeneralId);
        }

        public int GetTotalMessagesAgainstJobUnRead(long recruiterPostedJobsAndApplicantsId, string userType)
        {
            return messageAgainstJobRepository.GetTotalMessagesAgainstJobUnRead(recruiterPostedJobsAndApplicantsId, userType);
        }

        public Task<IEnumerable<MessageAgainstJob>> GetAllMessagesAgainstJob(string orderByColumn, string orderBy, string searchTerm, bool isActive, long recruiterPostedJobsAndApplicantsId)
        {
            return messageAgainstJobRepository.GetAllMessagesAgainstJob(orderByColumn, orderBy, searchTerm, isActive, recruiterPostedJobsAndApplicantsId);
        }

        public bool MarkMessageRead(long recruiterPostedJobsAndApplicantsId, string userType)
        {
            return messageAgainstJobRepository.MarkMessageRead(recruiterPostedJobsAndApplicantsId, userType);
        }

        public int GetTotalMessagesAgainstJob(long recruiterPostedJobsAndApplicantsId)
        {
            return messageAgainstJobRepository.GetTotalMessagesAgainstJob(recruiterPostedJobsAndApplicantsId);
        }
    }
}
