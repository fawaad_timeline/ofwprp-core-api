﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IExperienceLevelRepository
    {
        Task<IEnumerable<ExperienceLevel>> GetAll();
        Task<Tuple<IEnumerable<ExperienceLevel>, int>> GetAll(int offset, int paging);
        ExperienceLevel GetById(int id);
        int Add(ExperienceLevel experienceLevel);
        int Update(ExperienceLevel experienceLevel);
        void Delete(int id);
        bool IsExists(string name);
        Task<int> GetAllCount();
    }
}
