﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class PagesResponseDTO
    {
        public long Id { get; set; }
        public string ContactUs { get; set; }
        public string AboutUs { get; set; }
        public string IsActive { get; set; }
    }
}
