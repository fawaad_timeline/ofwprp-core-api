﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class FaqService: IFaqService
    {
        private readonly IFaqRepository faqRepository;

        public FaqService(IFaqRepository faqRepository)
        {
            this.faqRepository = faqRepository;
        }

        public async Task<IEnumerable<Faq>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Faq faqObj)
        {
            return await faqRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, faqObj);
        }

        public async Task<Faq> GetById(long faqId)
        {
            return await faqRepository.GetById(faqId);
        }

        public long Save(Faq faqObj)
        {
            return faqRepository.Save(faqObj);
        }

        public bool Activate(long faqId, bool isActive)
        {
            return faqRepository.Activate(faqId, isActive);
        }

        public bool Delete(long faqId)
        {
            return faqRepository.Delete(faqId);
        }

        public bool IsExist(string faqName, long FaqId)
        {
            return faqRepository.IsExist(faqName, FaqId);
        }
        public Task<IEnumerable<Faq>> GetAll()
        {
            return faqRepository.GetAll();
        }
    }
}
