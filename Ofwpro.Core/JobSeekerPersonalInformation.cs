﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class JobSeekerPersonalInformation
    {
        protected long jobSeekerPersonalInformationId, jobSeekerId, countryId, maritalStatusId;
        protected string  mobileCode, homePhone, currentLocation, visaStatus, isDrivingLicense, createdBy, updatedBy;
        protected DateTime dob, createdOn, updatedOn;
        protected bool isActive, isDeleted;


        protected long placeOfBirthId, numberOfChildren, currentCountryId;
        protected string peopleOfDisability, needCompanion, originalAddress, expectedHomeAddress, disabilityDescription, disabilityCardNumber, city, identificationPapers;        
        protected DateTime dateOfHavingDisabilityCard, returningHomeExpectedDate;


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long JobSeekerPersonalInformationId { get => jobSeekerPersonalInformationId; set => jobSeekerPersonalInformationId = value; }
        public long JobSeekerId { get => jobSeekerId; set => jobSeekerId = value; }
        public long CountryId { get => countryId; set => countryId = value; }
        public long MaritalStatusId { get => maritalStatusId; set => maritalStatusId = value; }
        public string HomePhone { get => homePhone; set => homePhone = value; }
        public string MobileCode { get => mobileCode; set => mobileCode = value; }
        public string IsDrivingLicense { get => isDrivingLicense; set => isDrivingLicense = value; }
        public string CurrentLocation { get => currentLocation; set => currentLocation = value; }
        public string VisaStatus { get => visaStatus; set => visaStatus = value; }


        public string PeopleOfDisability { get => peopleOfDisability; set => peopleOfDisability = value; }
        public string NeedCompanion { get => needCompanion; set => needCompanion = value; }
        public long PlaceOfBirthId { get => placeOfBirthId; set => placeOfBirthId = value; }
        public string OriginalAddress { get => originalAddress; set => originalAddress = value; }
        public string ExpectedHomeAddress { get => expectedHomeAddress; set => expectedHomeAddress = value; }
        public string DisabilityDescription { get => disabilityDescription; set => disabilityDescription = value; }
        public string DisabilityCardNumber { get => disabilityCardNumber; set => disabilityCardNumber = value; }
        public string City { get => city; set => city = value; }
        public string IdentificationPapers { get => identificationPapers; set => identificationPapers = value; }
        public DateTime DateOfHavingDisabilityCard { get => dateOfHavingDisabilityCard; set => dateOfHavingDisabilityCard = value; }
        public DateTime ReturningHomeExpectedDate { get => returningHomeExpectedDate; set => returningHomeExpectedDate = value; }
        public long NumberOfChildren { get => numberOfChildren; set => numberOfChildren = value; }
        public long CurrentCountryId { get => currentCountryId; set => currentCountryId = value; }


        public bool IsActive { get => isActive; set => isActive = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }

        

        //public long CurrentCountry { get => currentCountry; set => currentCountry = value; }
        //public string ObigationService { get => obigationService; set => obigationService = value; }
        //public string NoOfChildren { get => noOfChildren; set => noOfChildren = value; }
        //public string MartyrsFamily { get => martyrsFamily; set => martyrsFamily = value; }
        //public string PeopleOfDisability { get => peopleOfDisability; set => peopleOfDisability = value; }
        //public string DisabilityDescription { get => disabilityDescription; set => disabilityDescription = value; }
        //public string NoOfDisabilityCard { get => noOfDisabilityCard; set => noOfDisabilityCard = value; }
        //public string AddressOfLiving { get => addressOfLiving; set => addressOfLiving = value; }
        //public string PlaceOfBirth { get => placeOfBirth; set => placeOfBirth = value; }
        //public string IdentificationPapers { get => identificationPapers; set => identificationPapers = value; }
        //public string OriginalAddress { get => originalAddress; set => originalAddress = value; }
        //public string ExpectedAddress { get => expectedAddress; set => expectedAddress = value; }
        //public bool NeedCompassion { get => needCompassion; set => needCompassion = value; }
        //public DateTime DateOfDisabilityCard { get => dateOfDisabilityCard; set => dateOfDisabilityCard = value; }
        //public DateTime ExpectedDate { get => expectedDate; set => expectedDate = value; }
    }
}
