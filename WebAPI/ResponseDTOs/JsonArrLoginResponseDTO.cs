﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JsonArrLoginResponseDTO
    {
        public int totalRecords { get; set; }
        public string code { get; set; }
        public string MSG { get; set; }
        public object data { get; set; }
    }
}
