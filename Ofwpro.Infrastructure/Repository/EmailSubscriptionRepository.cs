﻿using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ofwpro.Infrastructure.Repository
{
    public class EmailSubscriptionRepository : IEmailSubscriptionRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public EmailSubscriptionRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public long Save(EmailSubscription emailSubscription)
        {
            if (emailSubscription.EmailSubscriptionId <= 0)
            {
                emailSubscription.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<EmailSubscription>().Add(emailSubscription);
            }
            else
            {
                emailSubscription.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(emailSubscription).State = EntityState.Modified;
                unitOfWork.Context.EmailSubscription.Update(emailSubscription);
            }

            unitOfWork.Commit();
            unitOfWork.Context.Entry(emailSubscription).State = EntityState.Detached;
            return emailSubscription.EmailSubscriptionId;
        }
    }
}
