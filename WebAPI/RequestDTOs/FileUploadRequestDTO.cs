﻿namespace WebAPI.RequestDTOs
{
    public class FileUploadRequestDTO
    {
        public long Id { get; set; }
        public long MasterIdf { get; set; }
        public string Module { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
        public string Type { get; set; }
        public string OriginalName { get; set; }
        public string OtherDetails { get; set; }
    }
}
