﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class RecruiterPostedJobsDetailsResponseDTO
    {
        public long recruiter_posted_jobs_id_enc { get; set; }
        public long recruiter_id_enc { get; set; }
        public string reference_no { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string role_name { get; set; }     
        public string country_name { get; set; }
        public string city { get; set; }
        public string employment_type { get; set; }
        public string company_type { get; set; }
        public string salary_range { get; set; }
        public string experience_level { get; set; }
        public string logo { get; set; }
        public bool has_applied { get; set; }
    }
}
