﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class GetEducationLevelRequestDTO
    {
        public int id { get; set; }
    }
    public class EducationLevelRequestDTO
    {
        public int offset { get; set; }
        public int paging { get; set; }
    }
}
