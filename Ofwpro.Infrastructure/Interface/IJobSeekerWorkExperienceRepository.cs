﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IJobSeekerWorkExperienceRepository
    {
        Task<IEnumerable<JobSeekerWorkExperience>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerWorkExperience jobSeekerWorkExperienceObj);
        Task<JobSeekerWorkExperience> GetById(long jobSeekerWorkExperienceId);
        long Save(JobSeekerWorkExperience jobSeekerWorkExperienceObj);
        bool Activate(long jobSeekerWorkExperienceId, bool isActive);
        bool Delete(long jobSeekerWorkExperienceId);
        Task<JobSeekerWorkExperience> GetByJobSeekerId(long jobSeekerId);
    }
}
