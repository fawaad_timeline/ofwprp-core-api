﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class EmployeeTypeService : IEmployeeTypeService
    {
		private readonly IEmployeeTypeRepository _IEmployeeTypeRepository;
		public EmployeeTypeService(IEmployeeTypeRepository iEmployeeTypeRepository)
		{
			_IEmployeeTypeRepository = iEmployeeTypeRepository;
		}

        public int Add(EmployeeType employeeType)
        {
            return _IEmployeeTypeRepository.Add(employeeType);
        }

        public void Delete(int id)
        {
            _IEmployeeTypeRepository.Delete(id);
        }

        public async Task<Tuple<IEnumerable<EmployeeType>, int>> GetAll(int offset,int paging)
		{
			return await _IEmployeeTypeRepository.GetAll(offset, paging);
		}

		public EmployeeType GetById(int id)
		{
			return _IEmployeeTypeRepository.GetById(id);
		}

        public bool IsExists(string name)
        {
            return _IEmployeeTypeRepository.IsExists(name);
        }

        public int Update(EmployeeType employeeType)
        {
            return _IEmployeeTypeRepository.Update(employeeType);
        }
    }
}
