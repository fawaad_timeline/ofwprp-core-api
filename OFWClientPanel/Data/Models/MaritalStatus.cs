﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models
{
    public class MaritalStatus
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
