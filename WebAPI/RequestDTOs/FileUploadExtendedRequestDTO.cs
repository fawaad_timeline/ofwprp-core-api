﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class FileUploadExtendedRequestDTO
    {
        public string FileTitle { get; set; }
        public IFormFile FileToUpload { get; set; }
    }
}
