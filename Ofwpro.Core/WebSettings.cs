﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Ofwpro.Core
{
    public class WebSettings
    {
        [Key]
        public int Id { get; set; }
        public string AboutHtml { get; set; }
        public string PrivacyPolicyHtml { get; set; }
        public string TermsAndConditionsHtml { get; set; }
    }
}
