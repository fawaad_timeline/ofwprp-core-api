﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IMessageGeneralRepository
    {
        Task<IEnumerable<MessageGeneral>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, MessageGeneral messageGeneralObj);
        Task<MessageGeneral> GetById(long messageGeneralId);
        long Save(MessageGeneral messageGeneralObj);
        bool Activate(long messageGeneralId, bool isActive);
        bool Delete(long messageGeneralId);
        int TotalMessagesUnreadRecruiter(long jobSeekerId, long recruiterId);
        int TotalMessagesUnreadJobSeeker(long jobSeekerId, long recruiterId);
        bool MarkMessageReadJobSeeker(long jobSeekerId, long recruiterId);
        bool MarkMessageReadRecruiter(long jobSeekerId, long recruiterId);
        IEnumerable<long> GetChatGroupsJobSeeker(long jobSeekerId);
        IEnumerable<long> GetChatGroupsRecruiter(long recruiterId);
        int GetTotalMessages(long jobSeekerId, long recruiterId);
        MessageGeneral GetLastMessages(long jobSeekerId, long recruiterId);
        List<MessageGeneral> GetAllMessages(long jobSeekerId, long recruiterId, int offset, int paging);
        Task<IEnumerable<MessageGeneral>> GetAllMessagesGeneral(string orderByColumn, string orderBy, string searchTerm, bool isActive, long jobSeekerId, long recruiterId);
        bool DeleteMessagesOfJobSeeker(long jobSeekerId, long recruiterId, string userType);
        bool DeleteMessagesOfRecruiter(long jobSeekerId, long recruiterId, string userType);
    }
}
