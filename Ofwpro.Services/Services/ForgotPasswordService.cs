﻿
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class ForgotPasswordService: IForgotPasswordService
    {
        private readonly IForgotPasswordRepository forgotPasswordRepository;

        public ForgotPasswordService(IForgotPasswordRepository forgotPasswordRepository)
        {
            this.forgotPasswordRepository = forgotPasswordRepository;
        }

        public async Task<IEnumerable<ForgotPassword>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, ForgotPassword forgotPasswordObj)
        {
            return await forgotPasswordRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, forgotPasswordObj);
        }

        public async Task<ForgotPassword> GetById(long forgotPasswordId)
        {
            return await forgotPasswordRepository.GetById(forgotPasswordId);
        }

        public long Save(ForgotPassword forgotPasswordObj)
        {
            return forgotPasswordRepository.Save(forgotPasswordObj);
        }

        public bool Activate(long forgotPasswordId, bool isActive)
        {
            return forgotPasswordRepository.Activate(forgotPasswordId, isActive);
        }

        public bool Delete(long forgotPasswordId)
        {
            return forgotPasswordRepository.Delete(forgotPasswordId);
        }

        public bool IsExist(string email, long forgotPasswordId)
        {
            return forgotPasswordRepository.IsExist(email, forgotPasswordId);
        }

        public bool Validate(string email, string password)
        {
            return forgotPasswordRepository.Validate(email, password);
        }

        public Task<ForgotPassword> GetByUserId(long userId)
        {
            return forgotPasswordRepository.GetByUserId(userId);
        }

        public Task<ForgotPassword> GetByUserToken(string token)
        {
            return forgotPasswordRepository.GetByUserToken(token);
        }
    }
}
