﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface IJobSeekerAttachmentService
    {
        Task<IEnumerable<JobSeekerAttachment>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerAttachment jobSeekerAttachmentObj);
        Task<JobSeekerAttachment> GetById(long jobSeekerAttachmentId);
        long Save(JobSeekerAttachment jobSeekerAttachmentObj);
        bool Activate(long jobSeekerAttachmentId, bool isActive);
        bool Delete(long jobSeekerId, string documentType);
        bool IsExist(string jobSeekerAttachmentName, long JobSeekerAttachmentId);
        bool IsExistDocumentType(long jobSeekerId, string documentType);
        Task<JobSeekerAttachment> GetByDocumentType(string documentType);
        Task<JobSeekerAttachment> GetByDocumentType(long jobSeekerId, string documentType);
        Task<IEnumerable<JobSeekerAttachment>> GetByJobSeekerId(long jobSeekerId);
        Task<JobSeekerAttachment> GetByJobSeekerAndDocumentType(long jobSeekerId, string documentType);
        bool DeleteById(long jobSeekerAttachmentId);
    }
}
