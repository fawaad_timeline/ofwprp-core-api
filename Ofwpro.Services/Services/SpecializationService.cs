﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class SpecializationService: ISpecializationService
    {
        private readonly ISpecializationRepository specializationRepository;

        public SpecializationService(ISpecializationRepository specializationRepository)
        {
            this.specializationRepository = specializationRepository;
        }

        public async Task<Tuple<IEnumerable<Specialization>,int>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Specialization specializationObj)
        {
            return await specializationRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, specializationObj);
        }

        public async Task<Specialization> GetById(long specializationId)
        {
            return await specializationRepository.GetById(specializationId);
        }

        public long Save(Specialization specializationObj)
        {
            return specializationRepository.Save(specializationObj);
        }

        public bool Activate(long specializationId, bool isActive)
        {
            return specializationRepository.Activate(specializationId, isActive);
        }

        public bool Delete(long specializationId)
        {
            return specializationRepository.Delete(specializationId);
        }

        public bool IsExist(string specializationName, long SpecializationId)
        {
            return specializationRepository.IsExist(specializationName, SpecializationId);
        }

        public async Task<IEnumerable<Specialization>> GetAll()
        {
            return await specializationRepository.GetAll();
        }
    }
}
