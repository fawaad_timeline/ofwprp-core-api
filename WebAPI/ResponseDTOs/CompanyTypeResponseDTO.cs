﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class CompanyTypeResponseDTO
    {
		public long Id
		{
			get;
			set;
		}
		public string Name
		{
			get;
			set;
		}
	}
}
