﻿using BlazorInputFile;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using OFWClientPanel.Helpers;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace OFWClientPanel.Services
{
    public class FileUpload : IFileUpload
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IConfiguration configuration;
        private readonly IFileUploadService fileUploadService;
        private readonly IJobSeekerAttachmentService jobSeekerAttachmentService;
        private readonly IRecruiterAttachmentService recruiterAttachmentService;

        public FileUpload(IWebHostEnvironment environment,
                          IConfiguration configuration,
                          IFileUploadService fileUploadService,
                          IJobSeekerAttachmentService jobSeekerAttachmentService,
                          IRecruiterAttachmentService recruiterAttachmentService)
        {
            _environment = environment;
            this.configuration = configuration;
            this.fileUploadService = fileUploadService;
            this.jobSeekerAttachmentService = jobSeekerAttachmentService;
            this.recruiterAttachmentService = recruiterAttachmentService;
        }

        public async Task<bool> Attachments(IFileListEntry file,string UserType, string DocumentType, long Id)
        {
            bool IsSuccess = false;
            object Data = null;
            string Message = "";
            dynamic uploadedFile = null;

            try
            {
                string folderName = configuration["UploadFolders:UploadFolder"];
                if (string.IsNullOrWhiteSpace(_environment.WebRootPath))
                {
                    _environment.WebRootPath = Path.Combine(string.Empty, "wwwroot");
                }
                string webRootPath = _environment.ContentRootPath + Path.DirectorySeparatorChar.ToString() + "wwwroot" + Path.DirectorySeparatorChar.ToString();
                string contentRootPath = _environment.ContentRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Data.Length > 0)
                {
                    string fileNameUpdated = "";
                    string fileNameOriginal = "";
                    string fileType = "";
                    int fileSize = 0;

                    string fileName = file.Name; //ContentDispositionHeaderValue.Parse(file.Name).FileName.Trim('"');
                    string fullPath = Path.Combine(newPath, string.Empty);
                    fileNameOriginal = Path.GetFileName(file.Name);
                    string fileExt = fileNameOriginal.Substring(fileNameOriginal.LastIndexOf('.'), fileNameOriginal.Length - fileNameOriginal.LastIndexOf('.'));
                    fileNameUpdated = Guid.NewGuid().ToString() + fileExt;
                    if (file.Type == null)
                    {
                        fileType = UtilityHelper.GetContentType(file.Name);
                    }
                    else
                    {
                        fileType = file.Type.Split('/')[0].ToString();
                    }
                    if (fileType == "application") fileType = "document";
                    fileSize = Int32.Parse(file.Data.Length.ToString());
                    string savedFileName = Path.Combine(fullPath, fileNameUpdated);
                    using (var stream = new FileStream(savedFileName, FileMode.Create))
                    {
                        await file.Data.CopyToAsync(stream);
                    }

                    if(UserType == "Job Seeker")
                    {
                        jobSeekerAttachmentService.Delete(Id, DocumentType);

                        Ofwpro.Core.JobSeekerAttachment obItem = new Ofwpro.Core.JobSeekerAttachment
                        {
                            JobSeekerAttachmentId = 0,
                            JobSeekerId = Id,
                            DocumentType = DocumentType,
                            FileNameUpdated = fileNameUpdated,
                            FileNameOriginal = fileNameOriginal,
                            FileSize = fileSize.ToString(),
                            FileType = fileType,
                            IsActive = true,
                            IsDeleted = false,
                            CreatedOn = DateTime.Now,
                            CreatedBy = Id.ToString(),
                            UploadedFrom = "Client"
                        };

                        long newItemId = jobSeekerAttachmentService.Save(obItem);

                        uploadedFile = await jobSeekerAttachmentService.GetById(newItemId);
                    }

                    else if (UserType == "Recruiter")
                    {
                        recruiterAttachmentService.Delete(Id, DocumentType);

                        Ofwpro.Core.RecruiterAttachment obItem = new Ofwpro.Core.RecruiterAttachment
                        {
                            RecruiterAttachmentId = 0,
                            RecruiterId = Id,
                            DocumentType = DocumentType,
                            FileNameUpdated = fileNameUpdated,
                            FileNameOriginal = fileNameOriginal,
                            FileSize = fileSize.ToString(),
                            FileType = fileType,
                            IsActive = true,
                            IsDeleted = false,
                            CreatedOn = DateTime.Now,
                            CreatedBy = Id.ToString(),
                            UploadedFrom = "Client"
                        };

                        long newItemId = recruiterAttachmentService.Save(obItem);

                        uploadedFile = await recruiterAttachmentService.GetById(newItemId);
                    }

                    if (uploadedFile != null)
                    {
                        Data = uploadedFile;
                        Message = "File uploaded successfully.";
                        IsSuccess = true;

                    }
                    else
                    {
                        Message = "Failed while uploading";
                        IsSuccess = false;
                    }
                }
                else
                {
                    Message = "File not found";
                    IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                IsSuccess = false;
            }

            return IsSuccess;
        }


        public async Task<long> UploadPicture(IFileListEntry file, string UserType, string DocumentType, long Id)
        {
            bool IsSuccess = false;
            object Data = null;
            string Message = "";
            dynamic uploadedFile = null;

            long uploadedFileId = 0;

            try
            {
                string folderName = configuration["UploadFolders:UploadFolder"];
                if (string.IsNullOrWhiteSpace(_environment.WebRootPath))
                {
                    _environment.WebRootPath = Path.Combine(string.Empty, "wwwroot");
                }
                string webRootPath = _environment.ContentRootPath + Path.DirectorySeparatorChar.ToString() + "wwwroot" + Path.DirectorySeparatorChar.ToString();
                string contentRootPath = _environment.ContentRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Data.Length > 0)
                {
                    string fileNameUpdated = "";
                    string fileNameOriginal = "";
                    string fileType = "";
                    int fileSize = 0;

                    string fileName = file.Name; //ContentDispositionHeaderValue.Parse(file.Name).FileName.Trim('"');
                    string fullPath = Path.Combine(newPath, string.Empty);
                    fileNameOriginal = Path.GetFileName(file.Name);
                    string fileExt = fileNameOriginal.Substring(fileNameOriginal.LastIndexOf('.'), fileNameOriginal.Length - fileNameOriginal.LastIndexOf('.'));
                    fileNameUpdated = Guid.NewGuid().ToString() + fileExt;
                    if (file.Type == null)
                    {
                        fileType = UtilityHelper.GetContentType(file.Name);
                    }
                    else
                    {
                        fileType = file.Type.Split('/')[0].ToString();
                    }
                    if (fileType == "application") fileType = "document";
                    fileSize = Int32.Parse(file.Data.Length.ToString());
                    string savedFileName = Path.Combine(fullPath, fileNameUpdated);
                    using (var stream = new FileStream(savedFileName, FileMode.Create))
                    {
                        await file.Data.CopyToAsync(stream);
                    }

                    if (UserType == "Job Seeker")
                    {
                        jobSeekerAttachmentService.Delete(Id, DocumentType);

                        Ofwpro.Core.JobSeekerAttachment obItem = new Ofwpro.Core.JobSeekerAttachment
                        {
                            JobSeekerAttachmentId = 0,
                            JobSeekerId = Id,
                            DocumentType = DocumentType,
                            FileNameUpdated = fileNameUpdated,
                            FileNameOriginal = fileNameOriginal,
                            FileSize = fileSize.ToString(),
                            FileType = fileType,
                            IsActive = true,
                            IsDeleted = false,
                            CreatedOn = DateTime.Now,
                            CreatedBy = Id.ToString(),
                            UploadedFrom = "Client"
                        };

                        uploadedFileId = jobSeekerAttachmentService.Save(obItem);

                        uploadedFile = await jobSeekerAttachmentService.GetById(uploadedFileId);
                    }

                    else if (UserType == "Recruiter")
                    {
                        recruiterAttachmentService.Delete(Id, DocumentType);

                        Ofwpro.Core.RecruiterAttachment obItem = new Ofwpro.Core.RecruiterAttachment
                        {
                            RecruiterAttachmentId = 0,
                            RecruiterId = Id,
                            DocumentType = DocumentType,
                            FileNameUpdated = fileNameUpdated,
                            FileNameOriginal = fileNameOriginal,
                            FileSize = fileSize.ToString(),
                            FileType = fileType,
                            IsActive = true,
                            IsDeleted = false,
                            CreatedOn = DateTime.Now,
                            CreatedBy = Id.ToString(),
                            UploadedFrom = "Client"
                        };

                        uploadedFileId = recruiterAttachmentService.Save(obItem);

                        uploadedFile = await recruiterAttachmentService.GetById(uploadedFileId);
                    }

                    if (uploadedFile != null)
                    {
                        Data = uploadedFile;
                        Message = "File uploaded successfully.";
                        IsSuccess = true;

                    }
                    else
                    {
                        Message = "Failed while uploading";
                        IsSuccess = false;
                    }
                }
                else
                {
                    Message = "File not found";
                    IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                IsSuccess = false;
            }

            return uploadedFileId;
        }


    }
}
