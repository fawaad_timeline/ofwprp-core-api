﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models.JobSeeker
{
    public class WorkExperience
    {
        public long WorkExperienceId { get; set; }
        public string ExperienceLevel { get; set; }

        public string Company1 { get; set; }
        public string JobTitle1 { get; set; }
        public string CountryId1 { get; set; }
        public string City1 { get; set; }
        public DateTime? TimePeriodFrom1 { get; set; }
        public DateTime? TimePeriodTo1 { get; set; }
        public string ReasonForLeaving1 { get; set; }
        public string ExperienceLevel1 { get; set; }
        public string RolesAndResponsibilities1 { get; set; }

        public string Company2 { get; set; }
        public string JobTitle2 { get; set; }
        public string CountryId2 { get; set; }
        public string City2 { get; set; }
        public DateTime? TimePeriodFrom2 { get; set; }
        public DateTime? TimePeriodTo2 { get; set; }
        public string ReasonForLeaving2 { get; set; }
        public string ExperienceLevel2 { get; set; }
        public string RolesAndResponsibilities2 { get; set; }

        public string Company3 { get; set; }
        public string JobTitle3 { get; set; }
        public string CountryId3 { get; set; }
        public string City3 { get; set; }
        public DateTime? TimePeriodFrom3 { get; set; }
        public DateTime? TimePeriodTo3 { get; set; }
        public string ReasonForLeaving3 { get; set; }
        public string ExperienceLevel3 { get; set; }
        public string RolesAndResponsibilities3 { get; set; }
    }
}
