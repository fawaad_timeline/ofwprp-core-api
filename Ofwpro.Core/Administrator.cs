﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class Administrator
    {
        protected long administratorId;
        protected string administratorIdToken, firstName, lastName, email, password, contactNo, role, uploadedFileId, accActivationToken, createdBy, updatedBy;
        protected DateTime? createdOn, updatedOn;
        protected bool isActive, isDeleted;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long AdministratorId { get => administratorId; set => administratorId = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string AdministratorIdToken { get => administratorIdToken; set => administratorIdToken = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Email { get => email; set => email = value; }
        public string Password { get => password; set => password = value; }
        public string AccActivationToken { get => accActivationToken; set => accActivationToken = value; }
        public string ContactNo { get => contactNo; set => contactNo = value; }
        public string Role { get => role; set => role = value; }
        public string UploadedFileId { get => uploadedFileId; set => uploadedFileId = value; }

        public bool IsActive { get => isActive; set => isActive = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public DateTime? CreatedOn { get => createdOn; set => createdOn = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime? UpdatedOn { get => updatedOn; set => updatedOn = value; }
    }
}
