﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface INewsRepository
    {
        Task<IEnumerable<News>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, bool searchNewsDate, News newsObj);
        Task<IEnumerable<News>> GetAll();

        Task<IEnumerable<News>> GetCurrentNews();
        
        Task<News> GetById(long newsId);
        long Save(News newsObj);
        bool Activate(long newsId, bool isActive);
        bool Delete(long newsId);
        bool IsExist(string newsTitle, long NewsId);
    }
}
