﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class MarkMessageReadGenefalRequestDTO
    {
        public string token { get; set; }
        public long user_id_enc_param { get; set; }
    }
}
