﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JsonObjResponseDTO
    {
        public string code { get; set; }
        public string MSG { get; set; }
        public JsonResponseObjectApi data { get; set; }
    }
}
