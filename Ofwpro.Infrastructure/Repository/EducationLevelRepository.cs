﻿using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class EducationLevelRepository : IEducationLevelRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public EducationLevelRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public long Add(EducationLevels educationLevel)
        {
            _unitOfWork.Context.Set<EducationLevels>().Add(educationLevel);
            _unitOfWork.Commit();
            return educationLevel.Id;
        }

        public void Delete(long id)
        {
            var educationLevel = _unitOfWork.Context.EducationLevels.Where(x => x.Id == id).FirstOrDefault();
            if (educationLevel != null)
            {
                _unitOfWork.Context.EducationLevels.Remove(educationLevel);
                _unitOfWork.Commit();
            }
        }

        public async Task<Tuple<IEnumerable<EducationLevels>, int>> GetAll(int offset, int paging)
        {
            int recordsCount = _unitOfWork.Context.EducationLevels.Count();
            var result = _unitOfWork.Context.EducationLevels.OrderBy(x => x.Id).Skip(offset*paging).Take(paging);
            var dataList = await result.ToListAsync();
            return new Tuple<IEnumerable<EducationLevels>, int>(dataList, recordsCount);
        }
        public EducationLevels GetById(long id)
        {
            var educationLevel = _unitOfWork.Context.EducationLevels.Where(x => x.Id == id).FirstOrDefault();
            return educationLevel;
        }

        public bool IsExists(string name)
        {
            bool IsExists = false;
            var educationLevels = _unitOfWork.Context.EducationLevels.Where(x => x.Name == name);
            if (educationLevels.Any())
                IsExists = true;
            else
                IsExists = false;
            return IsExists;
        }

        public long Update(EducationLevels educationLevel)
        {
            _unitOfWork.Context.Entry(educationLevel).State = EntityState.Modified;
            _unitOfWork.Context.EducationLevels.Update(educationLevel);
            _unitOfWork.Commit();
            return educationLevel.Id;
        }
    }
}
