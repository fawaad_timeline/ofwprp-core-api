﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class RecruiterCompanyService: IRecruiterCompanyService
    {
        private readonly IRecruiterCompanyRepository recruiterCompanyRepository;

        public RecruiterCompanyService(IRecruiterCompanyRepository recruiterCompanyRepository)
        {
            this.recruiterCompanyRepository = recruiterCompanyRepository;
        }

        public async Task<IEnumerable<RecruiterCompany>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, RecruiterCompany recruiterCompanyObj)
        {
            return await recruiterCompanyRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, recruiterCompanyObj);
        }

        public async Task<RecruiterCompany> GetById(long recruiterCompanyId)
        {
            return await recruiterCompanyRepository.GetById(recruiterCompanyId);
        }

        public long Save(RecruiterCompany recruiterCompanyObj)
        {
            return recruiterCompanyRepository.Save(recruiterCompanyObj);
        }

        public bool Activate(long recruiterCompanyId, bool isActive)
        {
            return recruiterCompanyRepository.Activate(recruiterCompanyId, isActive);
        }

        public bool Delete(long recruiterCompanyId)
        {
            return recruiterCompanyRepository.Delete(recruiterCompanyId);
        }       

        public Task<RecruiterCompany> GetByRecruiterId(long recruiterId)
        {
            return recruiterCompanyRepository.GetByRecruiterId(recruiterId);
        }
    }
}
