﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface IJobSeekerService
    {
        Task<IEnumerable<JobSeeker>> All(string orderByColumn, string orderBy, string searchTerm, JobSeeker jobSeekerObj,bool? isApproved=null);
        Task<JobSeeker> GetById(long jobSeekerId);
        long Save(JobSeeker jobSeekerObj);
        bool Activate(long jobSeekerId, bool isActive);
        bool Delete(long jobSeekerId);
        bool IsExist(string email, long jobSeekerId);

        bool Validate(string email, string password);
        Task<JobSeeker> GetByEmail(string email);
        Task<JobSeeker> GetByJobSeekerIdToken(string jobSeekerIdToken);

        Task<JobSeeker> GetByActivationToken(string accActivationToken);
        Task<IEnumerable<JobSeeker>> GetAll();
        Task<IEnumerable<JobSeeker>> GetTopEightLatestJobSeekers();
        Task<IEnumerable<JobSeeker>> Filter(string searchText, string searchLocation, string experienceLevel);
        Task<IEnumerable<JobSeeker>> GetByPostedJob(long postedJobId);
        long NoOfCVs();

    }
}
