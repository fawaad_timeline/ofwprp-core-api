﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class PagesService: IPagesService
    {
        private readonly IPagesRepository pagesRepository;

        public PagesService(IPagesRepository pagesRepository)
        {
            this.pagesRepository = pagesRepository;
        }

        public async Task<IEnumerable<Pages>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Pages pagesObj)
        {
            return await pagesRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, pagesObj);
        }

        public async Task<Pages> GetById(long pagesId)
        {
            return await pagesRepository.GetById(pagesId);
        }

        public long Save(Pages pagesObj)
        {
            return pagesRepository.Save(pagesObj);
        }

        public bool Activate(long pagesId, bool isActive)
        {
            return pagesRepository.Activate(pagesId, isActive);
        }

        public bool Delete(long pagesId)
        {
            return pagesRepository.Delete(pagesId);
        }

        public bool IsExist(string pagesName, long PagesId)
        {
            return pagesRepository.IsExist(pagesName, PagesId);
        }
        public Task<IEnumerable<Pages>> GetAll()
        {
            return pagesRepository.GetAll();
        }
    }
}
