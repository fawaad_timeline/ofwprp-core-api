﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class WebSettingsService: IWebSettingsService
    {
        private readonly IWebSettingsRepository WebSettingsRepository;

        public WebSettingsService(IWebSettingsRepository WebSettingsRepository)
        {
            this.WebSettingsRepository = WebSettingsRepository;
        }

        public async Task<WebSettings> All(int settingId)
        {
            return await WebSettingsRepository.All(settingId);
        }

        public async Task<WebSettings> GetById(long WebSettingsId)
        {
            return await WebSettingsRepository.GetById(WebSettingsId);
        }

        public long AddUpdate(WebSettings WebSettingsObj)
        {
            return WebSettingsRepository.AddUpdate(WebSettingsObj);
        }

        public bool Delete(long WebSettingsId)
        {
            return WebSettingsRepository.Delete(WebSettingsId);
        }

        public Task<IEnumerable<WebSettings>> GetAll()
        {
            return WebSettingsRepository.GetAll();
        }
        public Task<WebSettings> GetFirstRecord()
        {
            return WebSettingsRepository.GetFirstRecord();
        }
    }
}
