﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class ExperienceLevelService : IExperienceLevelService
	{
		private readonly IExperienceLevelRepository _IExperienceLevelRepository;
		public ExperienceLevelService(IExperienceLevelRepository iExperienceLevelRepository)
		{
			_IExperienceLevelRepository = iExperienceLevelRepository;
		}

        public int Add(ExperienceLevel experienceLevel)
        {
            return _IExperienceLevelRepository.Add(experienceLevel);
        }

        public void Delete(int id)
        {
            _IExperienceLevelRepository.Delete(id);
        }

        public async Task<IEnumerable<ExperienceLevel>> GetAll()
		{
			return await _IExperienceLevelRepository.GetAll();
		}

        public async Task<Tuple<IEnumerable<ExperienceLevel>, int>> GetAll(int offset, int paging)
        {
            return await _IExperienceLevelRepository.GetAll(offset,paging);
        }

        public async Task<IEnumerable<ExperienceLevel>> GetAll(int? offset=0, int? paging=9999)
		{
			return await _IExperienceLevelRepository.GetAll();
		}
        public async Task<int> GetAllCount()
        {
            return await _IExperienceLevelRepository.GetAllCount();
        }
        public ExperienceLevel GetById(int id)
		{
			return _IExperienceLevelRepository.GetById(id);
		}

        public bool IsExists(string name)
        {
            return _IExperienceLevelRepository.IsExists(name);
        }

        public int Update(ExperienceLevel experienceLevel)
        {
            return _IExperienceLevelRepository.Update(experienceLevel);
        }
    }
}
