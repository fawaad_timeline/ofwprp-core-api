﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class JobSeekerService: IJobSeekerService
    {
        private readonly IJobSeekerRepository jobSeekerRepository;

        public JobSeekerService(IJobSeekerRepository jobSeekerRepository)
        {
            this.jobSeekerRepository = jobSeekerRepository;
        }

        public async Task<IEnumerable<JobSeeker>> All(string orderByColumn, string orderBy, string searchTerm, JobSeeker jobSeekerObj,bool? isApproved = null)
        {
            return await jobSeekerRepository.All(orderByColumn, orderBy, searchTerm, jobSeekerObj, isApproved);
        }

        public async Task<JobSeeker> GetById(long jobSeekerId)
        {
            return await jobSeekerRepository.GetById(jobSeekerId);
        }

        public long Save(JobSeeker jobSeekerObj)
        {
            return jobSeekerRepository.Save(jobSeekerObj);
        }

        public bool Activate(long jobSeekerId, bool isActive)
        {
            return jobSeekerRepository.Activate(jobSeekerId, isActive);
        }

        public bool Delete(long jobSeekerId)
        {
            return jobSeekerRepository.Delete(jobSeekerId);
        }

        public bool IsExist(string email, long jobSeekerId)
        {
            return jobSeekerRepository.IsExist(email, jobSeekerId);
        }

        public bool Validate(string email, string password)
        {
            return jobSeekerRepository.Validate(email, password);
        }

        public Task<JobSeeker> GetByEmail(string email)
        {
            return jobSeekerRepository.GetByEmail(email);
        }

        public Task<JobSeeker> GetByJobSeekerIdToken(string jobSeekerIdToken)
        {
            return jobSeekerRepository.GetByJobSeekerIdToken(jobSeekerIdToken);
        }

        public Task<JobSeeker> GetByActivationToken(string accActivationToken)
        {
            return jobSeekerRepository.GetByActivationToken(accActivationToken);
        }
        public async Task<IEnumerable<JobSeeker>> GetAll()
        {
            return await jobSeekerRepository.GetAll();
        }
       public async Task<IEnumerable<JobSeeker>> GetTopEightLatestJobSeekers()
        {
            return await jobSeekerRepository.GetTopEightLatestJobSeekers();
        }
        public async Task<IEnumerable<JobSeeker>> Filter(string searchText, string searchLocation, string experienceLevel)
        {
            return await jobSeekerRepository.Filter(searchText, searchLocation, experienceLevel);
        }

        public async Task<IEnumerable<JobSeeker>> GetByPostedJob(long postedJobId)
        {
            return await jobSeekerRepository.GetByPostedJob(postedJobId);
        }

        public long NoOfCVs()
        {
            return jobSeekerRepository.NoOfCVs();
        }

    }
}
