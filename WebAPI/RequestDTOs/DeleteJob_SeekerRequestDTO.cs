﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class DeleteJob_SeekerRequestDTO
    {
        public long id { get; set; }
        public string token { get; set; }
        public bool isActive { get; set; }
    }
}
