﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using MimeKit;
using MailKit.Net.Smtp;
using PusherServer;
using Ofwpro.Services.Interfaces;
using Ofwpro.Core;
using Ofwpro.Services.DTOs;
using WebAPI.RequestDTOs;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class MessageGeneralMobileController : ControllerBase
    {
        private readonly IFileUploadService fileUploadService;
        private readonly IJobSeekerService jobSeekerService;
        private readonly IAdministratorService administratorService;
        private readonly ISessionService sessionService;
        private readonly IJobSeekerPersonalInformationService jobSeekerPersonalInformationService;
        private readonly IJobSeekerQualificationService jobSeekerQualificationService;
        private readonly IJobSeekerWorkExperienceService jobSeekerWorkExperienceService;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IJobSeekerAttachmentService jobSeekerAttachmentService;
        private readonly IRecruiterPostedJobsService recruiterPostedJobsService;
        private readonly IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService;
        private readonly ICountryDetailsService countryDetailsService;
        private readonly IRecruiterAttachmentService recruiterAttachmentService;
        private readonly IForgotPasswordService forgotPasswordService;
        private readonly IMessageGeneralService messageGeneralService;
        private readonly IMessageAgainstJobService messageAgainstJobService;

        private readonly IRecruiterService recruiterService;
        private readonly INotificationService notificationService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public MessageGeneralMobileController(IFileUploadService fileUploadService, IJobSeekerService jobSeekerService, IAdministratorService administratorService, ISessionService sessionService, IJobSeekerPersonalInformationService jobSeekerPersonalInformationService, IJobSeekerQualificationService jobSeekerQualificationService, IJobSeekerWorkExperienceService jobSeekerWorkExperienceService, IWebHostEnvironment hostingEnvironment, IJobSeekerAttachmentService jobSeekerAttachmentService, IRecruiterPostedJobsService recruiterPostedJobsService, IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService, ICountryDetailsService countryDetailsService, IRecruiterAttachmentService recruiterAttachmentService, IForgotPasswordService forgotPasswordService, IMessageGeneralService messageGeneralService, IMessageAgainstJobService messageAgainstJobService, IRecruiterService recruiterService, INotificationService notificationService, IMapper mapper, IConfiguration config)
        {
            this.fileUploadService = fileUploadService;
            this.jobSeekerService = jobSeekerService;
            this.administratorService = administratorService;
            this.sessionService = sessionService;
            this.jobSeekerPersonalInformationService = jobSeekerPersonalInformationService;
            this.jobSeekerQualificationService = jobSeekerQualificationService;
            this.jobSeekerWorkExperienceService = jobSeekerWorkExperienceService;
            this.hostingEnvironment = hostingEnvironment;
            this.jobSeekerAttachmentService = jobSeekerAttachmentService;
            this.recruiterPostedJobsService = recruiterPostedJobsService;
            this.recruiterPostedJobsAndApplicantsService = recruiterPostedJobsAndApplicantsService;
            this.countryDetailsService = countryDetailsService;
            this.recruiterAttachmentService = recruiterAttachmentService;
            this.forgotPasswordService = forgotPasswordService;
            this.messageGeneralService = messageGeneralService;
            this.recruiterService = recruiterService;
            this.notificationService = notificationService;
            this.messageAgainstJobService = messageAgainstJobService;
            this.mapper = mapper;
            this.config = config;
        }

        [HttpPost]
        [Route("en/message_general_mobile/save_chat_job_seeker")]
        public async Task<JsonObjResponseDTO> save_chat_job_seeker([FromBody] SaveChatJobSeeker2RequestDTO model)
        {
            string token = model.token; long recruiter_id_enc = model.recruiter_id_enc; string message = model.message; string os = model.os;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeeker jobSeekerObj = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonObjResponseDTO JsonObjResponseDTOObj = new JsonObjResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonObjResponseDTOObj.code = "2";
                        JsonObjResponseDTOObj.MSG = "Invalid token";
                        return JsonObjResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonObjResponseDTOObj.code = "2";
                            JsonObjResponseDTOObj.MSG = "Invalid token";
                            return JsonObjResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                    {
                        JsonObjResponseDTOObj.code = "2";
                        JsonObjResponseDTOObj.MSG = "Invalid token";
                        return JsonObjResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    DateTime todayDateTime = DateTime.Now;

                    jobSeekerObj = await jobSeekerService.GetById(sessionObj.UserId);
                    MessageGeneral messageGeneralObj = new MessageGeneral();

                    messageGeneralObj.JobSeekerId = jobSeekerObj.JobSeekerId;
                    messageGeneralObj.UserId = recruiter_id_enc;
                    messageGeneralObj.UserType = "job_seeker";
                    messageGeneralObj.Message = message;
                    messageGeneralObj.CreatedOn = todayDateTime;
                    messageGeneralObj.CreatedBy = Convert.ToString(sessionObj.UserId);
                    messageGeneralObj.UpdatedOn = todayDateTime;
                    messageGeneralObj.UpdatedBy = loggedInUserId;
                    messageGeneralObj.IsDeleted = false;
                    messageGeneralObj.IsActive = true;

                    long gernalMassageId = messageGeneralService.Save(messageGeneralObj);

                    int totalMessageUnread = messageGeneralService.TotalMessagesUnreadRecruiter(jobSeekerObj.JobSeekerId, recruiter_id_enc);

                    SaveChatJobSeekerRequestDTO saveChatJobSeekerRequestDTO = new SaveChatJobSeekerRequestDTO();
                    saveChatJobSeekerRequestDTO.message_from = "job_seeker";
                    saveChatJobSeekerRequestDTO.sender_name = jobSeekerObj.FirstName + " " + jobSeekerObj.LastName;
                    saveChatJobSeekerRequestDTO.job_seeker_id_enc = jobSeekerObj.JobSeekerId;
                    saveChatJobSeekerRequestDTO.recruiter_id_enc = recruiter_id_enc;
                    saveChatJobSeekerRequestDTO.total_messages_unread = totalMessageUnread;
                    saveChatJobSeekerRequestDTO.message = message;
                    saveChatJobSeekerRequestDTO.added_date = todayDateTime;

                    Recruiter recruiterObj_ = await recruiterService.GetById(recruiter_id_enc);
                    Session sessionRecruiterObj = await sessionService.GetByUserId(recruiter_id_enc);

                    JsonResponseObjectApi apidataresponse = new JsonResponseObjectApi();
                    apidataresponse.added_date = todayDateTime;
                    apidataresponse.id = 0;
                    apidataresponse.message = message;
                    apidataresponse.message_from = recruiterObj_.FirstName + " " + recruiterObj_.LastName;
                    apidataresponse.user_type = "recruiter";

                    object data = new object();
                    //if (model.os == "IOS")
                    //{
                    //    await notificationService.SendNotificationToRecruiterAsync(recruiterObj_.AppleDeviceToken, jobSeekerObj.FirstName + " " + jobSeekerObj.LastName, apidataresponse, totalMessageUnread, message, os, sessionRecruiterObj, jobSeekerObj, recruiterObj_);
                    //}
                    //else if (model.os == "android")
                    //{
                    //    await notificationService.SendNotificationToRecruiterAsync(recruiterObj_.AndroidDeviceToken, jobSeekerObj.FirstName + " " + jobSeekerObj.LastName, apidataresponse, totalMessageUnread, message, os, sessionRecruiterObj, jobSeekerObj, recruiterObj_);
                    //}
                    //$ch = curl_init(HOST_URL."/".LAN_SEL."/notifications/send_notification_to_recruiter");
                    //curl_setopt($ch, CURLOPT_POST, count($params));
                    //curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                    //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    if (model.os == "IOS")
                    {
                        await notificationService.SendPusher(recruiterObj_.AppleDeviceToken, jobSeekerObj.FirstName + " " + jobSeekerObj.LastName, apidataresponse, totalMessageUnread, message, os, sessionRecruiterObj, jobSeekerObj, recruiterObj_, saveChatJobSeekerRequestDTO);
                    }
                    else
                    {
                        await notificationService.SendPusher(recruiterObj_.AndroidDeviceToken, jobSeekerObj.FirstName + " " + jobSeekerObj.LastName, apidataresponse, totalMessageUnread, message, os, sessionRecruiterObj, jobSeekerObj, recruiterObj_, saveChatJobSeekerRequestDTO);
                    }

                    JsonObjResponseDTOObj.code = "1";
                    JsonObjResponseDTOObj.MSG = "Chat messages sent successfully";
                    JsonResponseObjectApi _response = new JsonResponseObjectApi();
                    _response.added_date = todayDateTime;
                    _response.id = gernalMassageId;
                    _response.message = message;
                    _response.message_from = jobSeekerObj.FirstName + " " + jobSeekerObj.LastName;
                    _response.user_type = jobSeekerObj.UserType;
                    JsonObjResponseDTOObj.data = _response;
                }
            }
            catch (Exception ex)
            {
                JsonObjResponseDTOObj.code = "0";
                JsonObjResponseDTOObj.MSG = "Information could not be saved";
                return JsonObjResponseDTOObj;
            }

            return JsonObjResponseDTOObj;
        }


        [HttpPost]
        [Route("en/message_general_mobile/save_chat_recruiter")]
        public async Task<JsonObjResponseDTO> save_chat_recruiter([FromBody] SaveChatRecruiter2RequestDTO model)
        {
            string token = model.token; long job_seeker_id_enc = model.job_seeker_id_enc; string message = model.message; string os = model.os;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            Recruiter recruiterObj = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonObjResponseDTO JsonObjResponseDTOObj = new JsonObjResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonObjResponseDTOObj.code = "2";
                        JsonObjResponseDTOObj.MSG = "Invalid token";
                        return JsonObjResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonObjResponseDTOObj.code = "2";
                            JsonObjResponseDTOObj.MSG = "Invalid token";
                            return JsonObjResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("job_seeker"))
                    {
                        JsonObjResponseDTOObj.code = "2";
                        JsonObjResponseDTOObj.MSG = "Invalid token";
                        return JsonObjResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    DateTime todayDateTime = DateTime.Now;

                    recruiterObj = await recruiterService.GetById(sessionObj.UserId);
                    MessageGeneral messageGeneralObj = new MessageGeneral();

                    messageGeneralObj.JobSeekerId = job_seeker_id_enc;
                    messageGeneralObj.UserId = recruiterObj.RecruiterId;
                    messageGeneralObj.UserType = "recruiter";
                    messageGeneralObj.Message = message;
                    messageGeneralObj.CreatedOn = todayDateTime;
                    messageGeneralObj.CreatedBy = Convert.ToString(sessionObj.UserId);
                    messageGeneralObj.UpdatedOn = todayDateTime;
                    messageGeneralObj.UpdatedBy = loggedInUserId;
                    messageGeneralObj.IsDeleted = false;
                    messageGeneralObj.IsActive = true;

                    long gernalMassageId = messageGeneralService.Save(messageGeneralObj);

                    int totalMessageUnread = messageGeneralService.TotalMessagesUnreadJobSeeker(job_seeker_id_enc, recruiterObj.RecruiterId);
                    string isNew = "N";
                    if (totalMessageUnread == 1) { isNew = "Y"; }

                    SaveChatRecruiterRequestDTO saveChatRecruiterRequestDTO = new SaveChatRecruiterRequestDTO();
                    saveChatRecruiterRequestDTO.message_from = "recruiter";
                    saveChatRecruiterRequestDTO.sender_name = recruiterObj.CompanyName;
                    saveChatRecruiterRequestDTO.job_seeker_id_enc = job_seeker_id_enc;
                    saveChatRecruiterRequestDTO.tbl_recruiter_id = recruiterObj.RecruiterId;
                    saveChatRecruiterRequestDTO.recruiter_id_enc = recruiterObj.RecruiterId;
                    saveChatRecruiterRequestDTO.total_messages_unread = totalMessageUnread;
                    saveChatRecruiterRequestDTO.is_new = isNew;
                    saveChatRecruiterRequestDTO.message = message;
                    saveChatRecruiterRequestDTO.added_date = todayDateTime;
                    object data = new object();
                    JobSeeker jobSeekerObj_ = await jobSeekerService.GetById(job_seeker_id_enc);
                    Session sessionJobSeekerObj = await sessionService.GetByUserId(job_seeker_id_enc);


                    object results = null;
                    JsonResponseObjectApi apidataresponse = new JsonResponseObjectApi();
                    apidataresponse.added_date = todayDateTime;
                    apidataresponse.id = 0;
                    apidataresponse.message = message;
                    apidataresponse.message_from = jobSeekerObj_.FirstName + " " + jobSeekerObj_.LastName;
                    apidataresponse.user_type = "jobSeeker";

                    //if (model.os == "IOS")
                    //{
                    //    results = await notificationService.SendNotificationToJobSeekerAsync(jobSeekerObj_.AppleDeviceToken, recruiterObj.FirstName +" "+ recruiterObj.LastName, apidataresponse, totalMessageUnread, message, os, sessionJobSeekerObj, jobSeekerObj_, recruiterObj);
                    //}
                    //else if (model.os == "android")
                    //{
                    //     results = await notificationService.SendNotificationToJobSeekerAsync(jobSeekerObj_.AndroidDeviceToken, recruiterObj.FirstName + " " + recruiterObj.LastName, apidataresponse, totalMessageUnread, message, os, sessionJobSeekerObj, jobSeekerObj_, recruiterObj);
                    //}

                    if (model.os == "IOS")
                    {
                        await notificationService.SendPusherJs(jobSeekerObj_.AppleDeviceToken, recruiterObj.FirstName + " " + recruiterObj.LastName, apidataresponse, totalMessageUnread, message, os, sessionJobSeekerObj, jobSeekerObj_, recruiterObj, saveChatRecruiterRequestDTO);
                    }
                    else
                    {
                        await notificationService.SendPusherJs(jobSeekerObj_.AndroidDeviceToken, recruiterObj.FirstName + " " + recruiterObj.LastName, apidataresponse, totalMessageUnread, message, os, sessionJobSeekerObj, jobSeekerObj_, recruiterObj, saveChatRecruiterRequestDTO);
                    }

                    JsonObjResponseDTOObj.code = "1";
                    JsonObjResponseDTOObj.MSG = "Chat messages sent successfully";
                    JsonResponseObjectApi _response = new JsonResponseObjectApi();

                    _response.id = gernalMassageId;
                    _response.added_date = todayDateTime;
                    _response.message = message;
                    _response.message_from = recruiterObj.FirstName + " " + recruiterObj.LastName;
                    _response.user_type = recruiterObj.UserType;
                    JsonObjResponseDTOObj.data = _response;
                    return JsonObjResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonObjResponseDTOObj.code = "0";
                JsonObjResponseDTOObj.MSG = "Information could not be saved";
                return JsonObjResponseDTOObj;
            }

            return JsonObjResponseDTOObj;
        }



        [HttpPost]
        [Route("en/message_general_mobile/mark_message_read_general")]
        public async Task<JsonArrResponseDTO> mark_message_read_general([FromBody] MarkMessageReadGenefalRequestDTO model)
        {
            string token = model.token; long user_id_enc_param = model.user_id_enc_param;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            Recruiter recruiterObj = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    long jobSeekerId = 0;
                    long recruiterId = 0;
                    if (sessionObj.UserType.Equals("job_seeker"))
                    {
                        jobSeekerId = sessionObj.UserId;
                        recruiterId = user_id_enc_param;
                    }
                    else
                    {
                        jobSeekerId = user_id_enc_param;
                        recruiterId = sessionObj.UserId;
                    }

                    if (sessionObj.UserType.Equals("job_seeker"))
                    {
                        messageGeneralService.MarkMessageReadJobSeeker(jobSeekerId, recruiterId);
                    }
                    else
                    {
                        messageGeneralService.MarkMessageReadRecruiter(jobSeekerId, recruiterId);
                    }

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Success";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }



        [HttpPost]
        [Route("en/message_general_mobile/get_chat_groups_job_seeker")]
        public async Task<object> get_chat_groups_job_seeker([FromBody] GetChatGroupJobSeekerRequestDTO model)
        {

            string token = model.token;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            List<object> data = new List<object>();
            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            JsonArrListResponseDTO JsonArrListResponseDTOObj = new JsonArrListResponseDTO();

            string hostUrl = config["Utility:APIBaseURL"];

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    //Dictionary<string, object> response = new Dictionary<string, object>();

                    string jobSeekerPicture = "";
                    string recruiterPicture = "";
                    long jobSeekerId = sessionObj.UserId;
                    IEnumerable<long> recuterIdList = messageGeneralService.GetChatGroupsJobSeeker(jobSeekerId);
                    foreach (long recuterId_ in recuterIdList)
                    {
                        // Job Seeker
                        long recuterId = recuterId_;
                        Recruiter recruiterObj = await recruiterService.GetById(recuterId);
                        RecruiterAttachment recruiterAttachmentObj = new RecruiterAttachment();
                        recruiterAttachmentObj.IsActive = true;
                        recruiterAttachmentObj.DocumentType = "picture";
                        recruiterAttachmentObj.RecruiterId = recuterId;
                        IEnumerable<RecruiterAttachment> recurtmentAttachmentList = await recruiterAttachmentService.All(0, 9999999, "CreatedOn", "ASC", "", recruiterAttachmentObj);
                        string UploadedFrom = String.Empty;
                        if (recurtmentAttachmentList != null && recurtmentAttachmentList.Count() > 0) UploadedFrom = recurtmentAttachmentList.FirstOrDefault().UploadedFrom;
                        if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                        else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                        if (recurtmentAttachmentList.Any())
                        {
                            recruiterPicture = hostUrl + "/Uploads/" + recurtmentAttachmentList.FirstOrDefault().FileNameUpdated;
                        }
                        else
                        {
                            if (recruiterObj.Gender.Equals("male"))
                            {
                                recruiterPicture = hostUrl + "/assets/images/default_male.png";
                            }
                            else
                            {
                                recruiterPicture = hostUrl + "/assets/images/default_female.png";
                            }
                        }

                        // Recruiter
                        JobSeeker jobSeekerObj = await jobSeekerService.GetById(jobSeekerId);
                        JobSeekerAttachment jobSeekerAttachmentObj = new JobSeekerAttachment();
                        jobSeekerAttachmentObj.IsActive = true;
                        jobSeekerAttachmentObj.DocumentType = "picture";
                        jobSeekerAttachmentObj.JobSeekerId = jobSeekerId;
                        IEnumerable<JobSeekerAttachment> jobSeekerAttachmentList = await jobSeekerAttachmentService.All(0, 9999999, "CreatedOn", "ASC", "", jobSeekerAttachmentObj);
                        UploadedFrom = String.Empty;
                        if (jobSeekerAttachmentList != null && jobSeekerAttachmentList.Count() > 0) UploadedFrom = jobSeekerAttachmentList.FirstOrDefault().UploadedFrom;
                        if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                        else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                        if (jobSeekerAttachmentList.Any())
                        {
                            jobSeekerPicture = hostUrl + "/Uploads/" + jobSeekerAttachmentList.FirstOrDefault().FileNameUpdated;
                        }
                        else
                        {
                            if (jobSeekerObj.Gender.Equals("male"))
                            {
                                jobSeekerPicture = hostUrl + "/assets/images/default_male.png";
                            }
                            else
                            {
                                jobSeekerPicture = hostUrl + "/assets/images/default_female.png";
                            }
                        }

                        int totalMessages = messageGeneralService.GetTotalMessages(jobSeekerId, recuterId);
                        int totalMessagesUnread = messageGeneralService.TotalMessagesUnreadJobSeeker(jobSeekerId, recuterId);

                        MessageGeneral lastMessage = messageGeneralService.GetLastMessages(jobSeekerId, recuterId);

                        Dictionary<string, object> jobSeekerDict = new Dictionary<string, object>();
                        jobSeekerDict.Add("job_seeker_id_enc", jobSeekerId);
                        jobSeekerDict.Add("job_seeker_full_name", jobSeekerObj?.FirstName + " " + jobSeekerObj?.LastName);
                        jobSeekerDict.Add("picture", jobSeekerPicture);

                        Dictionary<string, object> recruiterDict = new Dictionary<string, object>();
                        recruiterDict.Add("recruiter_id_enc", recuterId);
                        recruiterDict.Add("company_name", recruiterObj?.CompanyName);
                        recruiterDict.Add("logo", recruiterPicture);

                        Dictionary<string, object> lastMessateDict = new Dictionary<string, object>();
                        lastMessateDict.Add("date", lastMessage?.CreatedOn);
                        lastMessateDict.Add("message", lastMessage?.Message);
                        lastMessateDict.Add("message_from", lastMessage?.UserType);
                        lastMessateDict.Add("is_read", lastMessage?.IsRead??false == true ? "Y" : "N");

                        Dictionary<string, object> responseData = new Dictionary<string, object>();
                        responseData.Add("job_title", "");
                        responseData.Add("total_messages", totalMessages);
                        responseData.Add("total_messages_unread", totalMessagesUnread);
                        responseData.Add("job_seeker", jobSeekerDict);
                        responseData.Add("recruiter", recruiterDict);
                        responseData.Add("last_message", lastMessateDict);

                        data.Add(responseData);
                    }

                    JsonArrListResponseDTOObj.code = "1";
                    JsonArrListResponseDTOObj.MSG = "Chat Messages.";
                    JsonArrListResponseDTOObj.data = data;
                    return JsonArrListResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be retrieved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }



        [HttpPost]
        [Route("en/message_general_mobile/get_chat_groups_recruiter")]
        public async Task<object> get_chat_groups_recruiter([FromBody] GetChatGroupRecruiterRequestDTO model)
        {
            string token = model.token;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            List<object> data = new List<object>();
            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            JsonArrListResponseDTO JsonArrListResponseDTOObj = new JsonArrListResponseDTO();

            string hostUrl = config["Utility:APIBaseURL"];

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("job_seeker"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    //Dictionary<string, object> response = new Dictionary<string, object>();

                    string jobSeekerPicture = "";
                    string recruiterPicture = "";
                    long recruiterId = sessionObj.UserId;
                    IEnumerable<long> jobseekerIdList = messageGeneralService.GetChatGroupsRecruiter(recruiterId);
                    foreach (long jobSeekerId_ in jobseekerIdList)
                    {
                        // Job Seeker
                        long jobSeekerId = jobSeekerId_;
                        JobSeeker jobSeekerObj = await jobSeekerService.GetById(jobSeekerId);
                        JobSeekerAttachment jobSeekerAttachmentObj = new JobSeekerAttachment();
                        jobSeekerAttachmentObj.IsActive = true;
                        jobSeekerAttachmentObj.DocumentType = "picture";
                        jobSeekerAttachmentObj.JobSeekerId = jobSeekerId;
                        IEnumerable<JobSeekerAttachment> jobSeekerAttachmentList = await jobSeekerAttachmentService.All(0, 9999999, "CreatedOn", "ASC", "", jobSeekerAttachmentObj);
                        string UploadedFrom = String.Empty;
                        if (jobSeekerAttachmentList?.Count() > 0) UploadedFrom = jobSeekerAttachmentList.FirstOrDefault().UploadedFrom;
                        if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                        else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                        if (jobSeekerAttachmentList.Any())
                        {
                            jobSeekerPicture = hostUrl + "/Uploads/" + jobSeekerAttachmentList.FirstOrDefault().FileNameUpdated;
                        }
                        else
                        {
                            if (jobSeekerObj.Gender.Equals("male"))
                            {
                                jobSeekerPicture = hostUrl + "/assets/images/default_male.png";
                            }
                            else
                            {
                                jobSeekerPicture = hostUrl + "/assets/images/default_female.png";
                            }
                        }

                        // Recruiter
                        Recruiter recruiterObj = await recruiterService.GetById(recruiterId);
                        RecruiterAttachment recruiterAttachmentObj = new RecruiterAttachment();
                        recruiterAttachmentObj.IsActive = true;
                        recruiterAttachmentObj.DocumentType = "logo";
                        recruiterAttachmentObj.RecruiterId = recruiterId;
                        IEnumerable<RecruiterAttachment> recruiterAttachmentList = await recruiterAttachmentService.All(0, 9999999, "CreatedOn", "ASC", "", recruiterAttachmentObj);
                         UploadedFrom = String.Empty;
                        if (recruiterAttachmentList != null && recruiterAttachmentList.Count() > 0) UploadedFrom = recruiterAttachmentList.FirstOrDefault().UploadedFrom;
                        if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                        else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                        if (recruiterAttachmentList.Any())
                        {
                            recruiterPicture = hostUrl + "/Uploads/" + recruiterAttachmentList.FirstOrDefault().FileNameUpdated;
                        }
                        else
                        {
                            recruiterPicture = hostUrl + "/assets/images/default_logo.png";
                        }

                        int totalMessages = messageGeneralService.GetTotalMessages(jobSeekerId, recruiterId);
                        int totalMessagesUnread = messageGeneralService.TotalMessagesUnreadRecruiter(jobSeekerId, recruiterId);

                        MessageGeneral lastMessage = messageGeneralService.GetLastMessages(jobSeekerId, recruiterId);

                        Dictionary<string, object> jobSeekerDict = new Dictionary<string, object>();
                        jobSeekerDict.Add("job_seeker_id_enc", jobSeekerId);
                        jobSeekerDict.Add("job_seeker_full_name", jobSeekerObj?.FirstName + " " + jobSeekerObj?.LastName);
                        jobSeekerDict.Add("picture", jobSeekerPicture);

                        Dictionary<string, object> recruiterDict = new Dictionary<string, object>();
                        recruiterDict.Add("recruiter_id_enc", recruiterId);
                        recruiterDict.Add("company_name", recruiterObj?.CompanyName);
                        recruiterDict.Add("logo", recruiterPicture);

                        Dictionary<string, object> lastMessateDict = new Dictionary<string, object>();
                        lastMessateDict.Add("date", lastMessage?.CreatedOn);
                        lastMessateDict.Add("message", lastMessage?.Message);
                        lastMessateDict.Add("message_from", lastMessage?.UserType);
                        lastMessateDict.Add("is_read", lastMessage?.IsRead == true ? "Y" : "N");

                        Dictionary<string, object> responseData = new Dictionary<string, object>();
                        responseData.Add("job_title", "");
                        responseData.Add("total_messages", totalMessages);
                        responseData.Add("total_messages_unread", totalMessagesUnread);
                        responseData.Add("job_seeker", jobSeekerDict);
                        responseData.Add("recruiter", recruiterDict);
                        responseData.Add("last_message", lastMessateDict);

                        data.Add(responseData);
                    }

                    JsonArrListResponseDTOObj.code = "1";
                    JsonArrListResponseDTOObj.MSG = "Chat Messages.";
                    JsonArrListResponseDTOObj.data = data;
                    return JsonArrListResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be retrieved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/message_general_mobile/get_all_messages_job_seeker")]
        public async Task<object> get_all_messages_job_seeker([FromBody] AllMessageJobSeekerRequestDTO model)
        {
            string token = model.token; long recruiter_id_enc = model.recruiter_id_enc; int offset = model.offset; int pagination = model.pagination;

            string lan = "en";

            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode=200 };
            List<RecruiterPostedJobsAndApplicantsResponse2DTO> RecruiterPostedJobsAndApplicantsResponse2DTOList = new List<RecruiterPostedJobsAndApplicantsResponse2DTO>();

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            string hostUrl = config["Utility:APIBaseURL"];
            bool isSuccess = true;

            Session sessionObj = null;

            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }

            if (offset < 0) { offset = 0; }
            if (pagination <= 0) { pagination = 50; }

            List<MessageGeneral> messageGeneralList = messageGeneralService.GetAllMessages(sessionObj.UserId, recruiter_id_enc, offset, pagination);
            List<AllMessagesResponseDTO> allMessagesResponseDTOList = mapper.Map<List<AllMessagesResponseDTO>>(messageGeneralList);

            allMessagesResponseDTOList.ForEach(x => x.message_from = x.user_type);

            //$rs_all_messages = array_reverse($rs_all_messages);
            allMessagesResponseDTOList.Reverse();

            messageGeneralService.MarkMessageReadJobSeeker(sessionObj.UserId, recruiter_id_enc);

            Dictionary<string, object> JsonResponseDict = new Dictionary<string, object>();
            JsonResponseDict.Add("code", 1);
            JsonResponseDict.Add("MSG", "Chat Messages.");
            JsonResponseDict.Add("messages", allMessagesResponseDTOList);
            return JsonResponseDict;
        }


        [HttpPost]
        [Route("en/message_general_mobile/get_all_messages_recruiter")]
        public async Task<object> get_all_messages_recruiter([FromBody] AllMessageRecruiterRequestDTO model)
        {
            string token = model.token; long job_seeker_id_enc = model.job_seeker_id_enc; int offset = model.offset; int pagination = model.pagination;
            string lan = "en";

            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode=200 };
            List<RecruiterPostedJobsAndApplicantsResponse2DTO> RecruiterPostedJobsAndApplicantsResponse2DTOList = new List<RecruiterPostedJobsAndApplicantsResponse2DTO>();

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            string hostUrl = config["Utility:APIBaseURL"];
            bool isSuccess = true;

            Session sessionObj = null;

            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && sessionObj.UserType.Equals("job_seeker"))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }

            if (offset < 0) { offset = 0; }
            if (pagination <= 0) { pagination = 50; }

            List<MessageGeneral> messageGeneralList = messageGeneralService.GetAllMessages(job_seeker_id_enc, sessionObj.UserId, offset, pagination);
            List<AllMessagesResponseDTO> allMessagesResponseDTOList = mapper.Map<List<AllMessagesResponseDTO>>(messageGeneralList);

            allMessagesResponseDTOList.ForEach(x => x.message_from = x.user_type);

            //$rs_all_messages = array_reverse($rs_all_messages);
            allMessagesResponseDTOList.Reverse();

            messageGeneralService.MarkMessageReadRecruiter(job_seeker_id_enc, sessionObj.UserId);

            Dictionary<string, object> JsonResponseDict = new Dictionary<string, object>();
            JsonResponseDict.Add("code", 1);
            JsonResponseDict.Add("MSG", "Chat Messages.");
            JsonResponseDict.Add("messages", allMessagesResponseDTOList);
            return JsonResponseDict;
        }
    }
}