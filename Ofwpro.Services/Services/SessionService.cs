﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class SessionService: ISessionService
    {
        private readonly ISessionRepository sessionRepository;

        public SessionService(ISessionRepository sessionRepository)
        {
            this.sessionRepository = sessionRepository;
        }

        public async Task<IEnumerable<Session>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Session sessionObj)
        {
            return await sessionRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, sessionObj);
        }

        public async Task<Session> GetById(long sessionId)
        {
            return await sessionRepository.GetById(sessionId);
        }

        public long Save(Session sessionObj)
        {
            return sessionRepository.Save(sessionObj);
        }

        public bool Activate(long sessionId, bool isActive)
        {
            return sessionRepository.Activate(sessionId, isActive);
        }

        public bool Delete(long sessionId)
        {
            return sessionRepository.Delete(sessionId);
        }

        public bool IsExist(string sessionName, long SessionId)
        {
            return sessionRepository.IsExist(sessionName, SessionId);
        }

        public bool DeleteSessionBasedOnUser(long jobSeekerId, string userType)
        {
            return sessionRepository.DeleteSessionBasedOnUser(jobSeekerId, userType);
        }

        public Task<Session> GetByToken(string token)
        {
            return sessionRepository.GetByToken(token);
        }

        public bool DeleteByToken(string token)
        {
            return sessionRepository.DeleteByToken(token);
        }
        public Task<Session> GetByUserId(long userId)
        {
            return sessionRepository.GetByUserId(userId);
        }
    }
}
