﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class JobSeekerSaveQualificationAdminRequestDTO
    {
        public long Id { get; set; }
        public string Token { get; set; }
        public long education_level_id_enc { get; set; }
        public long specialization_id_enc { get; set; }
        public string school_university  { get; set; }
        public string name_of_degree { get; set; }
        public DateTime graduation_date { get; set; }

        //public string DetailedAddress { get; set; }
        //public string OriginalAddress { get; set; }
        //public string EnglishMasterLevel { get; set; }
        //public string FrenchMasterLevel { get; set; }
        //public string OtherLanguages { get; set; }
        //public DateTime YearOfStudying { get; set; }
    }
}
