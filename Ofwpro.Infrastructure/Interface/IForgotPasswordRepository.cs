﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IForgotPasswordRepository
    {
        Task<IEnumerable<ForgotPassword>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, ForgotPassword forgotPasswordObj);
        Task<ForgotPassword> GetById(long forgotPasswordId);
        long Save(ForgotPassword forgotPasswordObj);
        bool Activate(long forgotPasswordId, bool isActive);
        bool Delete(long forgotPasswordId);
        bool IsExist(string email, long forgotPasswordId);
        bool Validate(string email, string password);
        Task<ForgotPassword> GetByUserId(long userId);
        Task<ForgotPassword> GetByUserToken(string token);
    }
}
