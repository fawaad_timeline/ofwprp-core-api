﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class JobSeekerAllRequestAdminDTO
    {
        public string offset { get; set; }
        public string pagination { get; set; }
        public string has_video { get; set; }
        public string q { get; set; }
        public bool? isApproved { get; set; }
        public bool? isAttachment { get; set; }
    }
}
