﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class MessageGeneralRepository: IMessageGeneralRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public MessageGeneralRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<MessageGeneral>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, MessageGeneral messageGeneralObj)
        {
            var predicate = PredicateBuilder.New<MessageGeneral>(true);
            var predicateInner = PredicateBuilder.New<MessageGeneral>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                //predicateInner = predicateInner.Or(i => i.MessageGeneralNameEn.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.MessageGeneralNameAr.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (messageGeneralObj.MessageGeneralId > 0)
            {
                predicate = predicate.And(i => i.MessageGeneralId == messageGeneralObj.MessageGeneralId);
                isPradicate = true;
            }
            //if (!string.IsNullOrEmpty(messageGeneralObj.MessageGeneralNameEn))
            //{
            //    predicate = predicate.And(i => i.MessageGeneralNameEn.Contains(messageGeneralObj.MessageGeneralNameEn));
            //    isPradicate = true;
            //}
            //if (!string.IsNullOrEmpty(messageGeneralObj.MessageGeneralNameAr))
            //{
            //    predicate = predicate.And(i => i.MessageGeneralNameAr.Contains(messageGeneralObj.MessageGeneralNameAr));
            //    isPradicate = true;
            //}
            if (!string.IsNullOrEmpty(messageGeneralObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == messageGeneralObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.MessageGeneral.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.MessageGeneral.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.MessageGeneral.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.MessageGeneral.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<MessageGeneral> GetById(long messageGeneralId)
        {
            return unitOfWork.Context.MessageGeneral.Where(x => !x.IsDeleted && x.MessageGeneralId == messageGeneralId).FirstOrDefaultAsync();
        }

        public long Save(MessageGeneral messageGeneralObj)
        {
            if (messageGeneralObj.MessageGeneralId <= 0)
            {
                messageGeneralObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<MessageGeneral>().Add(messageGeneralObj);
            }
            else
            {
                messageGeneralObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(messageGeneralObj).State = EntityState.Modified;
                unitOfWork.Context.MessageGeneral.Update(messageGeneralObj);
            }

            unitOfWork.Commit();
            unitOfWork.Context.Entry(messageGeneralObj).State = EntityState.Detached;
            return messageGeneralObj.MessageGeneralId;
        }


        public bool Activate(long messageGeneralId, bool isActive)
        {
            MessageGeneral messageGeneralObj = unitOfWork.Context.MessageGeneral.Where(x => x.MessageGeneralId == messageGeneralId).ToList().LastOrDefault();
            if (messageGeneralObj != null)
            {
                messageGeneralObj.IsActive = isActive;
                messageGeneralObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(messageGeneralObj).State = EntityState.Modified;
                unitOfWork.Context.MessageGeneral.Update(messageGeneralObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long messageGeneralId)
        {
            var messageGeneralObj = unitOfWork.Context.MessageGeneral.Where(x => !x.IsDeleted && x.MessageGeneralId == messageGeneralId).ToList().LastOrDefault();

            if (messageGeneralObj != null)
            {
                messageGeneralObj.UpdatedOn = DateTime.Now;
                messageGeneralObj.IsDeleted = true;
                unitOfWork.Context.Entry(messageGeneralObj).State = EntityState.Modified;
                unitOfWork.Context.MessageGeneral.Update(messageGeneralObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }


        public int TotalMessagesUnreadRecruiter(long jobSeekerId, long recruiterId)
        {
            return unitOfWork.Context.MessageGeneral.Where(x => !x.IsDeleted &&!x.IsDeletedRecruiter && x.IsRead == false && x.UserType.Equals("job_seeker") && x.JobSeekerId == jobSeekerId && x.UserId == recruiterId).ToList().Count();
        }

        public int TotalMessagesUnreadJobSeeker(long jobSeekerId, long recruiterId)
        {
            return unitOfWork.Context.MessageGeneral.Where(x => !x.IsDeleted && !x.IsDeletedJobSeeker && x.IsRead == false && x.UserType.Equals("recruiter") && x.JobSeekerId == jobSeekerId && x.UserId == recruiterId).ToList().Count();
        }


        public bool MarkMessageReadJobSeeker(long jobSeekerId, long recruiterId)
        {
            List<MessageGeneral> list = unitOfWork.Context.MessageGeneral.Where(x => x.UserType.Equals("recruiter") && x.JobSeekerId == jobSeekerId && x.UserId == recruiterId).ToList();
            list.ForEach(x => x.IsRead = true);
            unitOfWork.Context.MessageGeneral.UpdateRange(list);
            unitOfWork.Commit();
            list.ForEach(x => unitOfWork.Context.Entry(x).State = EntityState.Detached);
            return true;
        }

        public bool MarkMessageReadRecruiter(long jobSeekerId, long recruiterId)
        {
            List<MessageGeneral> list = unitOfWork.Context.MessageGeneral.Where(x => x.UserType.Equals("job_seeker") && x.JobSeekerId == jobSeekerId && x.UserId == recruiterId).ToList();
            list.ForEach(x => x.IsRead = true);
            unitOfWork.Context.MessageGeneral.UpdateRange(list);
            unitOfWork.Commit();
            list.ForEach(x => unitOfWork.Context.Entry(x).State = EntityState.Detached);
            return true;
        }


        public IEnumerable<long> GetChatGroupsJobSeeker(long jobSeekerId)
        {
            List<MessageGeneral> list =  unitOfWork.Context.MessageGeneral.Where(x => !x.IsDeleted && !x.IsDeletedJobSeeker && x.JobSeekerId == jobSeekerId).ToList();
            IEnumerable<long> list2 = list.Select(x => x.UserId).Distinct();
            return list2;
        }

        public IEnumerable<long> GetChatGroupsRecruiter(long recruiterId)
        {
            List<MessageGeneral> list = unitOfWork.Context.MessageGeneral.Where(x => !x.IsDeleted && !x.IsDeletedRecruiter && x.UserId == recruiterId).ToList();
            IEnumerable<long> list2 = list.Select(x => x.JobSeekerId).Distinct();
            return list2;
        }
        

        public int GetTotalMessages(long jobSeekerId, long recruiterId)
        {
            return unitOfWork.Context.MessageGeneral.Where(x => !x.IsDeleted && x.UserType.Equals("job_seeker") && x.JobSeekerId == jobSeekerId && x.UserId == recruiterId).ToList().Count();
        }

        public MessageGeneral GetLastMessages(long jobSeekerId, long recruiterId)
        {
            return unitOfWork.Context.MessageGeneral.Where(x => !x.IsDeleted && x.JobSeekerId == jobSeekerId && x.UserId == recruiterId).OrderByDescending(x=>x.CreatedOn).ToList().FirstOrDefault();
        }

        public List<MessageGeneral> GetAllMessages(long jobSeekerId, long recruiterId, int offset, int paging)
        {
            List<MessageGeneral> list = unitOfWork.Context.MessageGeneral.Where(x => !x.IsDeleted && x.IsActive ==true && x.JobSeekerId == jobSeekerId && x.UserId == recruiterId).OrderByDescending(x => x.MessageGeneralId).Skip(offset).Take(paging).ToList();
            return list;
        }

        public async Task<IEnumerable<MessageGeneral>> GetAllMessagesGeneral(string orderByColumn, string orderBy, string searchTerm, bool isActive, long jobSeekerId, long recruiterId)
        {
            var predicate = PredicateBuilder.New<MessageGeneral>(true);
            var predicateInner = PredicateBuilder.New<MessageGeneral>(false);
            bool isPradicate = false;
            
            // Dynamic Conditions
            
            if (jobSeekerId > 0)
            {
                predicate = predicate.And(i => i.JobSeekerId == jobSeekerId);
                isPradicate = true;
            }

            if (recruiterId > 0)
            {
                predicate = predicate.And(i => i.UserId == recruiterId);
                predicate = predicate.And(i => i.UserType.Equals("recruiter"));
                isPradicate = true;
            }

            if (isActive == true)
            {
                predicate = predicate.And(i => i.IsActive == true);
            }else
            {
                predicate = predicate.And(i => i.IsActive == false);
            }

            if (!string.IsNullOrEmpty(searchTerm))
            {
                predicate = predicate.And(i => i.Message.Contains(searchTerm));

                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;


            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("desc"))
                {
                    return await this.unitOfWork.Context.MessageGeneral.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.MessageGeneral.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("desc"))
                {
                    return await this.unitOfWork.Context.MessageGeneral.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.MessageGeneral.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public bool DeleteMessagesOfJobSeeker(long jobSeekerId, long recruiterId, string userType)
        {
            var messageGeneralObj = unitOfWork.Context.MessageGeneral.Where(x => !x.IsDeleted && !x.IsDeletedJobSeeker && x.JobSeekerId == jobSeekerId && x.UserId == recruiterId).ToList();

            if (messageGeneralObj.Any())
            {
                foreach (var message in messageGeneralObj)
                {
                    message.UpdatedOn = DateTime.Now;
                    //message.IsDeleted = true;
                    message.IsDeletedJobSeeker = true;
                    unitOfWork.Context.Entry(message).State = EntityState.Modified;
                    unitOfWork.Context.MessageGeneral.Update(message);
                }
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool DeleteMessagesOfRecruiter(long jobSeekerId, long recruiterId, string userType)
        {
            var messageGeneralObj = unitOfWork.Context.MessageGeneral.Where(x => !x.IsDeleted && !x.IsDeletedRecruiter && x.JobSeekerId == jobSeekerId && x.UserId == recruiterId).ToList();

            if (messageGeneralObj.Any())
            {
                foreach(var message in messageGeneralObj)
                {
                    message.UpdatedOn = DateTime.Now;
                    //message.IsDeleted = true;
                    message.IsDeletedRecruiter = true;
                    unitOfWork.Context.Entry(message).State = EntityState.Modified;
                    unitOfWork.Context.MessageGeneral.Update(message);
                }
                unitOfWork.Commit();
                return true;
            }

            return false;
        }
    }
}
