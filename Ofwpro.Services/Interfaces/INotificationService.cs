﻿using Ofwpro.Core;
using Ofwpro.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface INotificationService
    {
        Task<object> SendNotificationToJobSeekerAsync(string deviceToken, string title, object data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj);
        Task<object> SendNotificationToRecruiterAsync(string deviceToken, string title, object data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj);
        Task<object> SendPusher(string deviceToken, string title, object data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj, SaveChatJobSeekerRequestDTO saveChatJobSeekerRequestDTO);
        Task<object> SendPusherJs(string deviceToken, string title, object data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj, SaveChatRecruiterRequestDTO saveChatRecruiterRequestDTO);
    }
}
