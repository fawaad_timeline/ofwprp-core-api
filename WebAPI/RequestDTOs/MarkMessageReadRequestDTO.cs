﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class MarkMessageReadRequestDTO
    {
        public string token { get; set; }
        public long recruiter_posted_jobs_and_applicants_id_enc { get; set; }
    }
}
