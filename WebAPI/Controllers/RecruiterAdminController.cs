﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using MimeKit;
using MailKit.Net.Smtp;
using PusherServer;
using Ofwpro.Services.Interfaces;
using Ofwpro.Core;
using System.Linq.Expressions;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class RecruiterAdmin : ControllerBase
    {
        private readonly IFileUploadService fileUploadService;
        private readonly IRecruiterService jobSeekerService;
        private readonly IAdministratorService administratorService;
        private readonly ISessionService sessionService;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IRecruiterAttachmentService jobSeekerAttachmentService;
        private readonly IRecruiterPostedJobsService recruiterPostedJobsService;
        private readonly IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService;
        private readonly ICountryDetailsService countryDetailsService;
        private readonly IRecruiterAttachmentService recruiterAttachmentService;
        private readonly IMessageAgainstJobService messageAgainstJobService;
        private readonly IForgotPasswordService forgotPasswordService;
        private readonly IRecruiterService recruiterService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public RecruiterAdmin(IFileUploadService fileUploadService, IRecruiterService jobSeekerService, IAdministratorService administratorService, ISessionService sessionService, IWebHostEnvironment hostingEnvironment, IRecruiterAttachmentService jobSeekerAttachmentService, IRecruiterPostedJobsService recruiterPostedJobsService, IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService, ICountryDetailsService countryDetailsService, IRecruiterAttachmentService recruiterAttachmentService, IMessageAgainstJobService messageAgainstJobService, IForgotPasswordService forgotPasswordService, IRecruiterService recruiterService, IMapper mapper, IConfiguration config)
        {
            this.fileUploadService = fileUploadService;
            this.jobSeekerService = jobSeekerService;
            this.administratorService = administratorService;
            this.sessionService = sessionService;
            this.hostingEnvironment = hostingEnvironment;
            this.jobSeekerAttachmentService = jobSeekerAttachmentService;
            this.recruiterPostedJobsService = recruiterPostedJobsService;
            this.recruiterPostedJobsAndApplicantsService = recruiterPostedJobsAndApplicantsService;
            this.countryDetailsService = countryDetailsService;
            this.recruiterAttachmentService = recruiterAttachmentService;
            this.messageAgainstJobService = messageAgainstJobService;
            this.forgotPasswordService = forgotPasswordService;
            this.recruiterService = recruiterService;
            this.mapper = mapper;
            this.config = config;
        }


        [HttpGet]
        [Route("en/recruiter/ajax_all_recruiters_admin")]
        public async Task<object> ajax_all_recruiters_admin(RecruiterAllAdminListRequestDTO model)
        {
            string lan = "en";
            Session sessionObj = null;
            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            int StatusCode = 200;
            bool isSuccess = true;
            string Message = "Information retrieved successfully";
            string ExceptionMessage = "";
            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode = 200 };
            List<RecruiterListingResponseDTO> RecruiterListingResponseDTOList = null;

            // Validate Token

            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(model.token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(model.token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && !sessionObj.UserType.Equals("admin"))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }

            if (isSuccess == true)
            {
                int offset = 0;
                int paging = 9999;
                string orderByColumn = "";
                string orderBy = "ASC";

                if (string.IsNullOrEmpty(orderByColumn) || orderByColumn.ToLower() == "string") { orderByColumn = "CreatedOn"; }
                if (string.IsNullOrEmpty(orderBy) || orderBy.ToLower() == "string") { orderBy = "Desc"; }

                Recruiter recruiterObj = new Recruiter();
                IEnumerable<Recruiter> recruiterList = await recruiterService.All(offset, paging, orderByColumn, orderBy, "", recruiterObj);
                if (!recruiterList.Any())
                {
                    StatusCode = 404;
                    isSuccess = true;
                    Message = "No records found";
                    ExceptionMessage = "";
                }
                else
                {
                    int totalRecords = recruiterList.Count();
                    IEnumerable<Recruiter> RecruiterList = recruiterList.Skip(paging * offset).Take(paging).ToList();
                    RecruiterListingResponseDTOList = mapper.Map<List<RecruiterListingResponseDTO>>(RecruiterList);
                }
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;
            return RecruiterListingResponseDTOList;
        }


        [HttpPost]
        [Route("en/admin_user/activedeactive_recruiter")]
        public async Task<JsonArrResponseDTO> activedeactive_recruiter([FromBody] ActivateRecruiterRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("job_seeker"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }
                if (isSuccess == true)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(model.recruiter_id)) || model.recruiter_id <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "[recruiter id] is blank.";
                        return JsonArrResponseDTOObj;
                    }
                }
                //get the object 
                //Recruiter recruiterObj = new Recruiter();
                //recruiterObj.RecruiterId = ;
                var objectRecruiter = await recruiterService.GetById(model.recruiter_id);
                //var  = objectRecruiterIE.FirstOrDefault();
                if (objectRecruiter != null)
                {
                    // Save
                    if (isSuccess == true)
                    {
                        recruiterService.Activate(model.recruiter_id, model.IsActive);
                        JsonArrResponseDTOObj.code = "1";
                        if (model.IsActive)
                        {
                            JsonArrResponseDTOObj.MSG = "Recruiter Active successfully.";
                        }
                        else
                        {
                            var recruiterData = await recruiterService.GetById(model.recruiter_id);
                            recruiterData.AndroidDeviceToken = "";
                            recruiterData.AppleDeviceToken = "";
                            var Id = recruiterService.Save(recruiterData);
                            JsonArrResponseDTOObj.MSG = "Recruiter Deactive successfully.";
                        }
                        return JsonArrResponseDTOObj;
                    }
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be deactivate";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/admin_user/delete_recruiter")]
        public async Task<JsonArrResponseDTO> delete_recruiter([FromBody] DeleteRecruiterRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (isSuccess == true)
                    {
                        if (string.IsNullOrWhiteSpace(model.token))
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }

                        if (isSuccess == true)
                        {
                            sessionObj = await sessionService.GetByToken(model.token);
                            if (sessionObj == null)
                            {
                                JsonArrResponseDTOObj.code = "2";
                                JsonArrResponseDTOObj.MSG = "Invalid token";
                                return JsonArrResponseDTOObj;
                            }
                        }

                        if (isSuccess == true && !sessionObj.UserType.Equals("administrator"))
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }
                }

                if (isSuccess == true)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(model.recruiter_id)) || model.recruiter_id <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "[recruiter id] is blank.";
                        return JsonArrResponseDTOObj;
                    }
                }

                //get the object 
                Recruiter recruiterObj = await recruiterService.GetById(model.recruiter_id);

                if (recruiterObj != null)
                {
                    // Save
                    if (isSuccess == true)
                    {
                        recruiterObj.AndroidDeviceToken = "";
                        recruiterObj.AppleDeviceToken = "";
                        recruiterObj.IsDeleted = true;
                        var id = recruiterService.Save(recruiterObj);
                        bool isDeleted = await recruiterPostedJobsService.DeleteJobsByRecruiterId(model.recruiter_id);
                        var recruiterToken = await sessionService.GetByUserId(model.recruiter_id);
                        if (recruiterToken != null)
                        {
                            sessionService.DeleteByToken(recruiterToken.Token);
                        }
                        //recruiterService.Delete(model.recruiter_id);

                        JsonArrResponseDTOObj.code = "1";
                        JsonArrResponseDTOObj.MSG = "Recruiter deleted successfully.";
                        return JsonArrResponseDTOObj;
                    }
                }
                else
                {
                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Recruiter does not exist.";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be deleted";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

    }
}
