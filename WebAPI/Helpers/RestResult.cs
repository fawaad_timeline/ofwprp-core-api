﻿namespace WebAPI.Helpers
{
    internal class RestResult<T>
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }
}