﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using System.Linq.Expressions;
using Ofwpro.Core;
using Ofwpro.Services.Interfaces;

namespace WebAPI.Controllers
{
    [EnableCors("AllowOrigin")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdministratorController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUserService userService;
        private readonly IAdministratorService administratorService;
        private readonly IUserResolverService userResolverService;
        private readonly IFileUploadService fileUploadService;
        private readonly ICountryDetailsService _ICountryDetailsService;
        private readonly ISalaryRangeService _ISalaryRangeService;
        private readonly IMaritalStatusService _IMaritalStatusService;
        private readonly IEducationLevelService _IEducationLevelService;
        private readonly IJobRoleService _IJobRoleService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;
        private readonly IWebHostEnvironment hostingEnvironment;

        public AdministratorController(UserManager<ApplicationUser> userManager,
                                        IUserService userService,
                                       IAdministratorService administratorService,
                                       IUserResolverService userResolverService, IFileUploadService fileUploadService, ICountryDetailsService countryDetailsService,
                                       ISalaryRangeService salaryRangeService, IMaritalStatusService IMaritalStatusService,
                                       IEducationLevelService IEducationLevelService, IJobRoleService IJobRoleService,
                                       IMapper mapper, IConfiguration config, IWebHostEnvironment hostingEnvironment)
        {
            this.userManager = userManager;
            this.userService = userService;
            this.administratorService = administratorService;
            this.userResolverService = userResolverService;
            this.fileUploadService = fileUploadService;
            _ICountryDetailsService = countryDetailsService;
            _ISalaryRangeService = salaryRangeService;
            _IMaritalStatusService = IMaritalStatusService;
            _IEducationLevelService = IEducationLevelService;
            _IJobRoleService = IJobRoleService;
            this.mapper = mapper;
            this.config = config;
            this.hostingEnvironment = hostingEnvironment;
        }

        //Get: api/Countrylist
        [HttpGet]
        [Route("en/admin/country/ajax_all_countries_mobile")]
        public async Task<object> ajax_all_countries_mobile()
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var countryList = await _ICountryDetailsService.GetAll();
                var CountryList = mapper.Map<IEnumerable<CountryResponseDTO>>(countryList);
                if (CountryList.Any())
                {
                    return CountryList;
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }

            return "No data found.";
        }


        //Get: api/SalaryRangeList
        [HttpGet]
        [Route("en/admin/salary_range/ajax_all_salary_range_mobile")]
        public async Task<Object> ajax_all_salary_range_mobile()
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var salaryRanges = await _ISalaryRangeService.GetAll(0, 99999);
                var salaryRangeList = mapper.Map<IEnumerable<SalaryRangeResponseDTO>>(salaryRanges.Item1);
                if (salaryRangeList.Any())
                {
                    return salaryRangeList;
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }
            return "No data found.";
        }
        //Get: api/SalaryRangeList
        [HttpPost]
        [Route("en/admin/salary_range/ajax_all_salary_range_admin")]
        public async Task<Object> ajax_all_salary_range_admin([FromBody] SalaryRangePaggingRequestDTO model)
        {
            int recordsCount = 0;
            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode=200 };

            int offset = model.offset;
            int paging = model.paging == 0 ? Convert.ToInt32(config["GeneralConfig:Paging"]) : model.paging;
            Specialization SpecializationObj = new Specialization();
            try
            {
                var salaryRanges = await _ISalaryRangeService.GetAll(offset, paging);
                recordsCount = salaryRanges.Item2;
                var salaryRangeList = mapper.Map<IEnumerable<SalaryRangeResponseDTO>>(salaryRanges.Item1);
                if (!salaryRangeList.Any())
                {
                    response.StatusCode = 404;
                    response.Data = null;
                    response.Message = "no record found.";
                }
                else
                {
                    SearchResponseDTO SearchResponseDTOObj = new SearchResponseDTO();
                    SearchResponseDTOObj.Records = salaryRangeList;
                    SearchResponseDTOObj.offset = offset;
                    SearchResponseDTOObj.paging = paging;
                    SearchResponseDTOObj.totalRecords = recordsCount;

                    response.Data = SearchResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = 500;
                response.Data = null;
                response.Message = "Failed while fetching data.";
                response.ExceptionMessage = ex.Message.ToString();
            }
            return ResponseHelper<SearchResponseDTO>.GenerateResponse(response);
        }

        //Get: api/SalaryRange/5
        [HttpGet]
        [Route("en/admin/salary_range/ajax_get_salary_range_mobile")]
        public IActionResult ajax_get_salary_range_mobile(SalaryRangeRequestDTO model)
        {
            long id = model.id;

            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var result = _ISalaryRangeService.GetById(id);
                var salaryRange = mapper.Map<SalaryRangeResponseDTO>(result);
                if (salaryRange != null)
                {
                    isSuccess = true;
                    StatusCode = 200;
                    Data = salaryRange;
                    Message = "Success";
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //Post: api/SalaryRange
        [HttpPost]
        [Route("en/admin/salary_range/ajax_add_salary_range_mobile")]
        public IActionResult ajax_add_salary_range_mobile([FromBody] SalaryRangeResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            // map dto to entity
            var salaryRange = mapper.Map<SalaryRange>(model);
            try
            {
                if (!_ISalaryRangeService.IsExists(model.Name))
                {
                    long newSalaryRange = _ISalaryRangeService.Add(salaryRange);
                    Data = null;
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Salary range created successfully";
                }
                else
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Salary range already exists.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while adding data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //PUT: api/SalaryRange/5
        [HttpPut]
        [Route("en/admin/salary_range/ajax_update_salary_range_mobile")]
        public IActionResult ajax_update_salary_range_mobile(int id, [FromBody] SalaryRangeResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            // map dto to entity
            var salaryRange = mapper.Map<SalaryRange>(model);

            try
            {
                salaryRange.Id = id;
                if (id != model.Id)
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Invalid salary range.";
                }
                else
                {
                    long newId = _ISalaryRangeService.Update(salaryRange);
                    Data = null;
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Salary range updated successfully";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while updating data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //Get: api/GetMaritalStatusList
        [HttpDelete]
        [Route("en/admin/marital_status/ajax_delete_marital_status")]
        public async Task<object> ajax_delete_marital_status(long id)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                bool maritalStatuses = _IMaritalStatusService.Delete(id);
                if (maritalStatuses)
                {
                    StatusCode = 200;
                    return "Marital status removed successfully";
                }
                else
                {
                    StatusCode = 404;
                    Message = "No record found to delete.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }

            return "No data found.";
        }

        //Get: api/GetMaritalStatusList
        [HttpGet]
        [Route("en/admin/marital_status/ajax_all_marital_status_mobile")]
        public async Task<object> ajax_all_marital_status_mobile()
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var maritalStatuses = await _IMaritalStatusService.GetAll();
                var maritalStatusList = mapper.Map<IEnumerable<MaritalStatusResponseDTO>>(maritalStatuses);
                if (maritalStatusList.Any())
                {
                    return maritalStatusList;
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }

            return "No data found.";
        }

        [HttpPost("en/admin/marital_status/ajax_save_marital_status")]
        public async Task<IActionResult> ajax_save_marital_status([FromBody] MaritalStatusRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            MaritalStatus maritalStatus = null;
            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been saved successfully";
            string ExceptionMessage = "";

            string lan = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.FaqManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                if (isSuccess == true && model.Id > 0)
                {
                    maritalStatus = _IMaritalStatusService.GetById(model.Id);
                    if (maritalStatus == null)
                    {
                        StatusCode = 404;
                        isSuccess = false;
                        Message = "No data found";
                        ExceptionMessage = "";
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(model.Name))
                    {
                        StatusCode = 400;
                        isSuccess = false;
                        Message = "Please provide question.";
                        ExceptionMessage = "";
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    MaritalStatus maritalStatusUpdated = new MaritalStatus();
                    maritalStatusUpdated.Id = model.Id;
                    maritalStatusUpdated.Name = model.Name;
                    maritalStatusUpdated.Rng = model.Rng;

                    // Save faq                   
                    long sessionId = _IMaritalStatusService.Save(maritalStatusUpdated);

                    if (sessionId < 0)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be saved.";
                        ExceptionMessage = "";
                    }
                    else
                    {
                        maritalStatusUpdated.Id = sessionId;
                        Data = maritalStatusUpdated;
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while saving data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        //Get: api/GetEducationLevelList
        [HttpGet]
        [Route("en/admin/education_level/ajax_all_education_levels_mobile")]
        public async Task<object> ajax_all_education_levels_mobile()
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var educationLevelsTuple = await _IEducationLevelService.GetAll(0,9999);
                var educationLevels = educationLevelsTuple.Item1;
                var educationLevelList = mapper.Map<IEnumerable<EducationLevelResponseDTO>>(educationLevels);
                if (educationLevelList.Any())
                {
                    return educationLevelList;
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }
            return "No data found.";
        }

        //Get: api/GetEducationLevelList
        [HttpPost]
        [Route("en/admin/education_level/ajax_all_education_levels_admin")]
        public async Task<object> ajax_all_education_levels_admin([FromBody] EducationLevelRequestDTO model)
        {
            int recordsCount = 0;
            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode=200 };

            int offset = model.offset;
            int paging = model.paging == 0 ? Convert.ToInt32(config["GeneralConfig:Paging"]) : model.paging;
            try
            {
                var educationLevelsTuple = await _IEducationLevelService.GetAll(offset,paging);
                var educationLevels = educationLevelsTuple.Item1;
                recordsCount = educationLevelsTuple.Item2;
                var educationLevelList = mapper.Map<IEnumerable<EducationLevelResponseDTO>>(educationLevels);
                if (!educationLevelList.Any())
                {
                    response.StatusCode = 404;
                    response.Data = null;
                    response.Message = "no record found.";
                }
                else
                {
                    SearchResponseDTO SearchResponseDTOObj = new SearchResponseDTO();
                    SearchResponseDTOObj.Records = educationLevelList;
                    SearchResponseDTOObj.offset = offset;
                    SearchResponseDTOObj.paging = paging;
                    SearchResponseDTOObj.totalRecords = recordsCount;

                    response.Data = SearchResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = 500;
                response.Data = null;
                response.Message = "Failed while fetching data.";
                response.ExceptionMessage = ex.Message.ToString();
            }
            return ResponseHelper<SearchResponseDTO>.GenerateResponse(response);
        }

        //Get: api/GetJobRoleList
        [HttpGet]
        [Route("en/admin/roles_job/ajax_all_roles_job_mobile")]
        public async Task<object> ajax_all_roles_job_mobile()
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var jobRoles = await _IJobRoleService.GetAll(0, 99999);
                var jobRoleList = mapper.Map<IEnumerable<JobRoleResponseDTO>>(jobRoles.Item1);
                if (jobRoleList.Any())
                {
                    return jobRoleList;
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }

            return "No data found.";
        }
        //Get: api/GetJobRoleList
        [HttpPost]
        [Route("en/admin/roles_job/ajax_all_roles_job_admin")]
        public async Task<object> ajax_all_roles_job_admin([FromBody] JobRolePagingResponseDTO model)
        {
            int recordsCount = 0;
            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode=200 };

            int offset = model.offset;
            int paging = model.paging == 0 ? Convert.ToInt32(config["GeneralConfig:Paging"]) : model.paging;
            try
            {
                var jobRoles = await _IJobRoleService.GetAll(offset, paging);
                recordsCount = jobRoles.Item2;
                var jobRoleList = mapper.Map<IEnumerable<JobRoleResponseDTO>>(jobRoles.Item1);
                if (!jobRoleList.Any())
                {
                    response.StatusCode = 404;
                    response.Data = null;
                    response.Message = "no record found.";
                }
                else
                {
                    SearchResponseDTO SearchResponseDTOObj = new SearchResponseDTO();
                    SearchResponseDTOObj.Records = jobRoleList;
                    SearchResponseDTOObj.offset = offset;
                    SearchResponseDTOObj.paging = paging;
                    SearchResponseDTOObj.totalRecords = recordsCount;

                    response.Data = SearchResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = 500;
                response.Data = null;
                response.Message = "Failed while fetching data.";
                response.ExceptionMessage = ex.Message.ToString();
            }

            return ResponseHelper<SearchResponseDTO>.GenerateResponse(response);
        }
        // GET: api/Administrator
        public async Task<IActionResult> Get(GetAdminRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";


            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();
                var IsSuperAdminRole = await userManager.IsInRoleAsync(currentUser, config["SystemRole:SuperAdminRole"]);

                if (IsSuperAdminRole)
                {
                    var administrators = await administratorService.Get(id);

                    if (administrators.Any())
                    {
                        var adminstratorResponse = mapper.Map<IEnumerable<AdministratorResponseDTO>>(administrators);

                        string PictureURL = string.Empty;
                        string BaseURL = config["Utility:APIBaseURL"].ToString();
                        string uploadFolderName = config["UploadFolders:UploadFolder"];

                        foreach (var admin in adminstratorResponse)
                        {
                            PictureURL = string.Empty;

                            var applicationUser = await userService.GetByEmail(admin.Email);

                            if (applicationUser != null)
                            {
                                var fileUploads = await fileUploadService.GetByModule(admin.Id, FileUploadEnum.AdminProfilePicture.ToString());

                                if (fileUploads.Any()) PictureURL = uploadFolderName + "/" + fileUploads.LastOrDefault().Name;

                                if (string.IsNullOrEmpty(PictureURL))
                                {
                                    string defaultProfilePicture = config["UploadFolders:DefaultProfilePicture"];
                                    PictureURL = BaseURL + uploadFolderName + "/" + defaultProfilePicture;
                                }
                                else PictureURL = BaseURL + PictureURL;

                                admin.Username = applicationUser.UserName;
                                admin.Role = (await userService.GetRole(applicationUser)).LastOrDefault().ToString();
                                admin.ProfilePictureURL = PictureURL;

                                if (!string.IsNullOrEmpty(admin.CreatedBy)) admin.CreatedBy = await userService.GetFullName(admin.CreatedBy);
                                if (!string.IsNullOrEmpty(admin.UpdatedBy)) admin.UpdatedBy = await userService.GetFullName(admin.UpdatedBy);

                            }

                        }

                        isSuccess = true;
                        StatusCode = 200;
                        Data = adminstratorResponse;
                        Message = "Success";
                    }
                    else
                    {
                        StatusCode = 404;
                        Message = "No data found.";
                    }
                }
                else
                {
                    StatusCode = 403;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        /*
               // PUT: api/Administrator/5
               [HttpPut("Activate")]
               public async Task<IActionResult> Activate(ActivateAdminRequestDTO model)
               {
                   long id = model.id;  bool active = model.active;
                   ResponseDTO<object> response = new ResponseDTO<object>();

                   int StatusCode = 0;
                   bool isSuccess = false;
                   object Data = null;
                   string Message = "";
                   string ExceptionMessage = "";

                   try
                   {
                       ApplicationUser currentUser = await userResolverService.GetCurrentUser();
                       var IsSuperAdminRole = await userManager.IsInRoleAsync(currentUser, config["SystemRole:SuperAdmin"]);

                       if (IsSuperAdminRole)
                       {
                           var admins = await administratorService.Get(id);

                           if (admins.Any())
                           {
                               var admin = admins.LastOrDefault();

                               bool activation = administratorService.Activate(admin.AdministratorId, active);

                               if (activation)
                               {
                                   isSuccess = true;
                                   StatusCode = 200;
                                   Message = active ? "Activated successfully." : "De-activated successfully.";
                               }
                               else
                               {
                                   StatusCode = 500;
                                   Message = "Failed while updating.";
                               }
                           }
                           else
                           {
                               StatusCode = 404;
                               Message = "Data not found.";
                           }
                       }
                       else
                       {
                           StatusCode = 403;
                           Message = "Forbidden! You are not authorized to perform this task.";
                       }
                   }
                   catch (Exception ex)
                   {
                       StatusCode = 500;
                       Message = "Failed while fetching data.";
                       ExceptionMessage = ex.Message.ToString();
                   }

                   response.StatusCode = StatusCode;
                   response.IsSuccess = isSuccess;
                   response.Data = Data;
                   response.Message = Message;
                   response.ExceptionMessage = ExceptionMessage;

                   return ResponseHelper<object>.GenerateResponse(response);
               }
               */
        // DELETE: api/ApiWithActions/5
        public async Task<IActionResult> Delete(DeleteAdminRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();
                var IsSuperAdminRole = await userManager.IsInRoleAsync(currentUser, config["SystemRole:SuperAdmin"]);

                if (IsSuperAdminRole)
                {
                    var admins = await administratorService.Get(id);

                    if (admins.Any())
                    {
                        var admin = admins.LastOrDefault();

                        bool deleted = administratorService.Delete(admin.AdministratorId);

                        if (deleted)
                        {
                            isSuccess = true;
                            StatusCode = 200;
                            Message = "Deleted successfully.";
                        }
                        else
                        {
                            StatusCode = 500;
                            Message = "Failed while updating.";
                        }
                    }
                    else
                    {
                        StatusCode = 404;
                        Message = "Data not found.";
                    }
                }
                else
                {
                    StatusCode = 403;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        // DELETE: api/SalaryRange/5
        [HttpDelete]
        [Route("en/admin/salary_range/ajax_delete_salary_range_mobile")]
        public IActionResult DeleteSalaryRange(DeleteSalaryRangeRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            try
            {
                _ISalaryRangeService.Delete(id);

                isSuccess = true;
                StatusCode = 200;
                Message = "Deleted successfully.";
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while deleting data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //Post: api/EducationLevels
        [HttpPost]
        [Route("en/admin/education_level/ajax_add_education_levels_mobile")]
        public IActionResult AddEducationLevel([FromBody] EducationLevelResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            // map dto to entity
            var educationLevel = mapper.Map<EducationLevels>(model);
            try
            {
                if (!_IEducationLevelService.IsExists(model.Name))
                {
                    long newEducationLevel = _IEducationLevelService.Add(educationLevel);
                    Data = null;
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Education level created successfully";
                }
                else
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Education level already exists.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while adding data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }
        //PUT: api/EducationLevels
        [HttpPut]
        [Route("en/admin/education_level/ajax_update_education_levels_mobile")]
        public IActionResult UpdateEducationLevel(int id, [FromBody] EducationLevelResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            // map dto to entity
            var educationLevel = mapper.Map<EducationLevels>(model);
            try
            {
                educationLevel.Id = id;
                if (id != model.Id)
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Invalid education level type.";
                }
                else
                {
                    long newId = _IEducationLevelService.Update(educationLevel);
                    Data = null;
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Education level updated successfully";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while adding data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //Get: api/EducationLevel/5
        [HttpGet]
        [Route("en/admin/education_level/ajax_get_education_levels_mobile")]
        public IActionResult GetEducationLevel(GetEducationLevelRequestDTO model)
        {
            int id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var result = _IEducationLevelService.GetById(id);
                var educationLevel = mapper.Map<EducationLevels>(result);
                if (educationLevel != null)
                {
                    isSuccess = true;
                    StatusCode = 200;
                    Data = educationLevel;
                    Message = "Success";
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        // DELETE: api/EducationLevel/5
        [HttpDelete]
        [Route("en/admin/education_level/ajax_delete_education_levels_mobile")]
        public IActionResult DeleteEducationLevel(DeleteEducationLevelRequestDTO model)
        {
            int id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            try
            {
                _IEducationLevelService.Delete(id);

                isSuccess = true;
                StatusCode = 200;
                Message = "Deleted successfully.";
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while deleting data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //Post: api/JobRole
        [HttpPost]
        [Route("en/admin/roles_job/ajax_add_roles_job_mobile")]
        public IActionResult AddJobRole([FromBody] JobRoleResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            // map dto to entity
            var jobRole = mapper.Map<JobRoles>(model);
            try
            {
                if (!_IJobRoleService.IsExists(model.Name))
                {
                    long newJobRole = _IJobRoleService.Add(jobRole);
                    Data = null;
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Job role created successfully";
                }
                else
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Job role already exists.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while adding data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //PUT: api/JobRole/5
        [HttpPut]
        [Route("en/admin/roles_job/ajax_update_roles_job_mobile")]
        public IActionResult UpdateJobRole(long id, [FromBody] JobRoleResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            // map dto to entity
            var jobRole = mapper.Map<JobRoles>(model);

            try
            {
                jobRole.Id = id;
                if (id != model.Id)
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Invalid job role.";
                }
                else
                {
                    long newId = _IJobRoleService.Update(jobRole);
                    Data = null;
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Job role updated successfully";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while updating data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //Get: api/JobRole/5
        [HttpGet]
        [Route("en/admin/roles_job/ajax_get_roles_job_mobile")]
        public IActionResult GetJobRole(GetJobRoleRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var result = _IJobRoleService.GetById(id);
                var jobRole = mapper.Map<JobRoleResponseDTO>(result);
                if (jobRole != null)
                {
                    isSuccess = true;
                    StatusCode = 200;
                    Data = jobRole;
                    Message = "Success";
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        // DELETE: api/SalaryRange/5
        [HttpDelete]
        [Route("en/admin/roles_job/ajax_delete_roles_job_mobile")]
        public IActionResult DeleteJobRole(DeleteobRoleRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            try
            {
                _IJobRoleService.Delete(id);

                isSuccess = true;
                StatusCode = 200;
                Message = "Deleted successfully.";
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while deleting data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }
    }
}
