﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using MimeKit;
using MailKit.Net.Smtp;
using PusherServer;
using Ofwpro.Services.Interfaces;
using Ofwpro.Core;
using System.Linq.Expressions;
using System.Text;
using Ofwpro.Services.Services;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class JobSeekerController : ControllerBase
    {
        private readonly IFileUploadService fileUploadService;
        private readonly IJobSeekerService jobSeekerService;
        private readonly IAdministratorService administratorService;
        private readonly ISessionService sessionService;
        private readonly IJobSeekerPersonalInformationService jobSeekerPersonalInformationService;
        private readonly IJobSeekerQualificationService jobSeekerQualificationService;
        private readonly IJobSeekerWorkExperienceService jobSeekerWorkExperienceService;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IJobSeekerAttachmentService jobSeekerAttachmentService;
        private readonly IRecruiterPostedJobsService recruiterPostedJobsService;
        private readonly IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService;
        private readonly ICountryDetailsService countryDetailsService;
        private readonly IRecruiterAttachmentService recruiterAttachmentService;
        private readonly IMessageAgainstJobService messageAgainstJobService;
        private readonly IForgotPasswordService forgotPasswordService;
        private readonly IMaritalStatusService maritalStatusService;
        private readonly ISpecializationService specializationService;
        private readonly IEducationLevelService educationalLevelService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public JobSeekerController(ISpecializationService specializationService, IEducationLevelService educationalLevelService, IFileUploadService fileUploadService, IJobSeekerService jobSeekerService, IAdministratorService administratorService, ISessionService sessionService, IJobSeekerPersonalInformationService jobSeekerPersonalInformationService, IJobSeekerQualificationService jobSeekerQualificationService, IJobSeekerWorkExperienceService jobSeekerWorkExperienceService, IWebHostEnvironment hostingEnvironment, IJobSeekerAttachmentService jobSeekerAttachmentService, IRecruiterPostedJobsService recruiterPostedJobsService, IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService, ICountryDetailsService countryDetailsService, IRecruiterAttachmentService recruiterAttachmentService, IMessageAgainstJobService messageAgainstJobService, IForgotPasswordService forgotPasswordService, IMaritalStatusService maritalStatusService, IMapper mapper, IConfiguration config)
        {
            this.specializationService = specializationService;
            this.educationalLevelService = educationalLevelService;
            this.fileUploadService = fileUploadService;
            this.jobSeekerService = jobSeekerService;
            this.administratorService = administratorService;
            this.sessionService = sessionService;
            this.jobSeekerPersonalInformationService = jobSeekerPersonalInformationService;
            this.jobSeekerQualificationService = jobSeekerQualificationService;
            this.jobSeekerWorkExperienceService = jobSeekerWorkExperienceService;
            this.hostingEnvironment = hostingEnvironment;
            this.jobSeekerAttachmentService = jobSeekerAttachmentService;
            this.recruiterPostedJobsService = recruiterPostedJobsService;
            this.recruiterPostedJobsAndApplicantsService = recruiterPostedJobsAndApplicantsService;
            this.countryDetailsService = countryDetailsService;
            this.recruiterAttachmentService = recruiterAttachmentService;
            this.messageAgainstJobService = messageAgainstJobService;
            this.forgotPasswordService = forgotPasswordService;
            this.maritalStatusService = maritalStatusService;
            this.mapper = mapper;
            this.config = config;
        }

        [HttpPost]
        [Route("en/job_seeker/ajax_all_job_seekers")]
        public async Task<object> ajax_all_job_seekers([FromBody] JobSeekerAllRequestDTO model)
        {
            string lan = "en";

            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode = 200 };
            List<JobSeekerListingResponseDTO> JobSeekerListingResponseDTOList = null;

            int offset = Convert.ToInt32(model.offset);
            int paging = Convert.ToInt32(model.pagination);
            string orderByColumn = "CreatedOn";
            string orderBy = "DESC";

            JobSeeker jobSeekerObj = new JobSeeker();
            jobSeekerObj.IsActive = true;

            string hostUrl = config["Utility:APIBaseURL"];

            IEnumerable<JobSeeker> jobSeekerList = await jobSeekerService.All(orderByColumn, orderBy, model.q, jobSeekerObj);
            if (!jobSeekerList.Any())
            {

            }
            else
            {
                if (!string.IsNullOrWhiteSpace(model.has_video))
                {
                    if (model.has_video.Equals("Y"))
                    {
                        JobSeekerAttachment jobSeekerAttachmentObj = new JobSeekerAttachment();
                        jobSeekerAttachmentObj.IsActive = true;
                        jobSeekerAttachmentObj.DocumentType = "video_resume";
                        IEnumerable<JobSeekerAttachment> jobSeekerAttachmentList = await jobSeekerAttachmentService.All(0, 9999999, "CreatedOn", "ASC", "", jobSeekerAttachmentObj);
                        List<long> jobSeekerIdExemptList = jobSeekerAttachmentList.Select(x => x.JobSeekerId).ToList();
                        if (!jobSeekerIdExemptList.Any()) { jobSeekerIdExemptList.Add(9999999999999); }
                        jobSeekerList = jobSeekerList.Where(x => jobSeekerIdExemptList.Contains(x.JobSeekerId));
                    }
                }

                int totalRecords = jobSeekerList.Count();
                IEnumerable<JobSeeker> JobSeekerList = jobSeekerList.Skip(paging * offset).Take(paging).ToList();
                JobSeekerListingResponseDTOList = mapper.Map<List<JobSeekerListingResponseDTO>>(JobSeekerList);
                foreach (JobSeekerListingResponseDTO JobSeekerListingResponseDTOObj in JobSeekerListingResponseDTOList)
                {
                    bool isExist = jobSeekerAttachmentService.IsExistDocumentType(JobSeekerListingResponseDTOObj.job_seeker_id_enc, "video_resume");
                    JobSeekerListingResponseDTOObj.has_video = "N";
                    if (isExist == true)
                    {
                        JobSeekerListingResponseDTOObj.has_video = "Y";
                    }
                    JobSeekerAttachment jobSeekerAttachmentFilter = new JobSeekerAttachment();
                    jobSeekerAttachmentFilter.JobSeekerId = JobSeekerListingResponseDTOObj.job_seeker_id_enc;
                    jobSeekerAttachmentFilter.FileType = "picture";
                    jobSeekerAttachmentFilter.DocumentType = "picture";
                    var filterdObjectdata = await jobSeekerAttachmentService.All(0, 9999999, "CreatedOn", "ASC", "", jobSeekerAttachmentFilter);

                    JobSeekerAttachment jobSeekerAttachmentObj = filterdObjectdata.ToList().FirstOrDefault();
                    if (jobSeekerAttachmentObj != null)
                    {
                        JobSeekerListingResponseDTOObj.picture = hostUrl + "/Uploads/" + jobSeekerAttachmentObj.FileNameUpdated;
                    }
                    else
                    {
                        if (JobSeekerListingResponseDTOObj.gender.Equals("male"))
                        {
                            JobSeekerListingResponseDTOObj.picture = hostUrl + "/assets/images/default_male.png";
                        }
                        else
                        {
                            JobSeekerListingResponseDTOObj.picture = hostUrl + "/assets/images/default_female.png";
                        }
                    }

                    var appliedJobs = await recruiterPostedJobsAndApplicantsService.GetByJobSeeker(JobSeekerListingResponseDTOObj.job_seeker_id_enc);

                    JobSeekerListingResponseDTOObj.applied_jobs = appliedJobs.Count();

                }
            }

            JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
            JsonArrLoginResponseDTOObj.code = "1";
            JsonArrLoginResponseDTOObj.MSG = "success";
            JsonArrLoginResponseDTOObj.data = JobSeekerListingResponseDTOList;
            return JsonArrLoginResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/ajax_register_job_seeker_mobile")]

        public async Task<JsonArrResponseDTO> ajax_register_job_seeker_mobile([FromBody] JobSeekerRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeeker jobSeekerObjOld = null;
            bool isSuccess = true;
            bool isExist = false;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(model.title))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Title is blank. Please enter title.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.first_name))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "First Name is blank. Please enter First Name.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.last_name))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Last Name is blank. Please enter Last Name.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(model.dob)))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Date of Birth is blank. Please enter Date of Birth.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.gender))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Gender is blank. Please enter Gender.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (!model.gender.Equals("male") && !model.gender.Equals("female"))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Gender can only be male or female.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.mobile))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Mobile is blank. Please enter Mobile.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.email))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Email is blank. Please enter Email.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (!UtilityHelper.IsValidEmail(model.email))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Invalid Email.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Validate Duplidate
                if (isSuccess == true)
                {
                    isExist = jobSeekerService.IsExist(model.email, 0);
                    if (isExist == true)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Email already exists. Please use a different email.";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    if (string.IsNullOrEmpty(model.password))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Password is blank. Please enter Password.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    string token = UtilityHelper.rand();

                    string accActivationToken = UtilityHelper.rand();
                    model.password = UtilityHelper.sha1(model.password);

                    JobSeeker jobSeekerObj = mapper.Map<JobSeeker>(model);

                    jobSeekerObj.JobSeekerIdToken = token;
                    jobSeekerObj.CreatedOn = DateTime.Now;
                    jobSeekerObj.IsDeleted = false;
                    jobSeekerObj.IsActive = false;
                    jobSeekerObj.IsPictureApproved = false;
                    jobSeekerObj.IsVideoApproved = false;
                    jobSeekerObj.AccActivationToken = accActivationToken;

                    if (string.IsNullOrEmpty(jobSeekerObj.CreatedBy))
                    {
                        if (loggedInUserId != null)
                        {
                            jobSeekerObj.CreatedBy = loggedInUserId;
                        }
                    }

                    long jobSeekerId = jobSeekerService.Save(jobSeekerObj);

                    // Email Code Goes Here

                    string hostUrl = config["Utility:APIBaseURL"];
                    string activationLink = hostUrl + "/en/job_seeker/activate_job_seeker?token=" + token;
                    string projectName = config["Utility:ProjectName"];

                    #region Email Template 
                    string emailText = "<style type='text/css'> .ReadMsgBody { width: 100% !important;} .ExternalClass {width: 100% !important;} #like { color:#000000; } #follow { color:#000000; } </style> <table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' style='height:100% !important; margin:0; padding:0; width:100% !important; background-color:#FFFFFF'> <tr> <td align='center' valign='top'><table width='600' border='0' align='left' cellpadding='0' cellspacing='0'> <tr> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='580' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='580' align='left' valign='top' style='border-collapse:collapse;'><font face='Verdana, Arial, Helvetica, sans-serif' size='2'> <font size='5'>D</font><strong>ear " + model.first_name + " " + model.last_name + ",</strong></font></td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td height='12' width='10'></td> <td height='12' width='580'></td> <td height='12' width='10'></td> </tr> <tr> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>Thanks for registering with us. To complete your registration, you will need to confirm that you received this email by clicking the link below:</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'></td> <td width='10' height='12'></td> </tr> <tr> <td height='12'></td> <td height='12' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#237DAF;'>" + activationLink + "</td> <td height='12'></td> </tr> <tr> <td height='12'></td> <td height='12'></td> <td height='12'></td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'></td> <td width='10' height='12'></td> </tr> <tr> <td height='12'></td> <td height='12' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>If clicking the link does not work, just copy and paste the entire link into your browser.</td> <td height='12'></td> </tr> <tr> <td align='left' valign='top'></td> <td align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>&nbsp;</td> <td align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>We assure you our best services.&nbsp;</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td align='left' valign='top'></td> <td align='left' valign='top' style='border-collapse:collapse; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:22px; font-weight:normal; color:#000000;'>&nbsp;</td> <td align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:22px; font-weight:normal; color:#000000;'><font face='Verdana, Arial, Helvetica, sans-serif' size='2'> <font size='5'>T</font><strong>hanks</strong></font></td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'><a href='" + hostUrl + "' target='_blank' alias='" + hostUrl + "' style='color:#237DAF; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-decoration:none'><span style='color:#237DAF;' title='" + hostUrl + "'>" + hostUrl + "</span></a></td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'></td> <td width='10' height='12'></td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'><hr ></td> <td width='10' height='12'></td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='center' valign='top' style='border-collapse:collapse; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; font-weight:normal; color:#000000;'>This mail is sent to you because you registered at " + projectName + ". Its not a part of spam mails.</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>&nbsp;</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> </table></td> </tr> </table>";
                    #endregion

                    string emailSubject = "Activate Account at " + projectName;

                    string emailFromName = config["EmailConfig:EmailFromName"];
                    string emailFromEmail = config["EmailConfig:EmailFromEmail"];
                    string emailFromPassword = config["EmailConfig:EmailFromPassword"];
                    string emailToName = jobSeekerObj.FirstName + " " + jobSeekerObj.LastName;
                    string emailToEmail = jobSeekerObj.Email;

                    MimeMessage message = new MimeKit.MimeMessage();
                    var bodyBuilder = new BodyBuilder();
                    MailboxAddress from = new MailboxAddress(emailFromEmail, emailFromEmail);
                    message.From.Add(from);
                    MailboxAddress to = null;
                    to = new MailboxAddress(emailToName, emailToEmail);
                    message.To.Add(to);
                    message.Subject = emailSubject;
                    bodyBuilder.HtmlBody = emailText;
                    message.Body = bodyBuilder.ToMessageBody();

                    SmtpClient client = new SmtpClient();
                    int port = Convert.ToInt32(config["EmailConfig:EmailPort"]);
                    bool EmailEnableSSL = Convert.ToBoolean(config["EmailConfig:EmailEnableSSL"]);
                    string SMTP = config["EmailConfig:EmailSMTP"];
                    client.Connect(SMTP, port, EmailEnableSSL);

                    client.Authenticate(emailFromEmail, emailFromPassword);
                    client.Send(message);
                    client.Disconnect(true);
                    client.Dispose();

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "User registered successfully. Please activate your account by following the link in your email.";
                    return JsonArrResponseDTOObj;

                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/job_seeker/ajax_validate_and_login_mobile")]
        public async Task<object> ajax_validate_and_login_mobile([FromBody] JobSeekerLoginRequestDTO model)
        {
            string loggedInUserId = "";
            bool isSuccess = true;
            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.email) || string.IsNullOrWhiteSpace(model.password))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Parameters Missing";
                        return JsonArrResponseDTOObj;
                    }
                }

                string password = "";
                if (isSuccess == true)
                {
                    password = UtilityHelper.Encrypt(model.password);
                    bool validate = jobSeekerService.Validate(model.email, password);
                    if (validate == false)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Invalid Credentials";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    JobSeeker jobSeekerObj = await jobSeekerService.GetByEmail(model.email);
                    HttpContext.Session.SetString("jobSeekerIdSess", Convert.ToString(jobSeekerObj.JobSeekerId));
                    HttpContext.Session.SetString("firstNameSess", jobSeekerObj.FirstName);
                    HttpContext.Session.SetString("lastNameSess", jobSeekerObj.LastName);
                    HttpContext.Session.SetString("title", jobSeekerObj.Title);
                    HttpContext.Session.SetString("userTypeSess", "job_seeker");

                    sessionService.DeleteSessionBasedOnUser(jobSeekerObj.JobSeekerId, "job_seeker");

                    string token = UtilityHelper.rand();
                    Session sessionObj = new Session();
                    sessionObj.IsActive = true;
                    sessionObj.SessionId = 0;
                    sessionObj.Token = token;
                    sessionObj.UserId = jobSeekerObj.JobSeekerId;
                    sessionObj.UserType = "job_seeker";
                    sessionService.Save(sessionObj);

                    // Get picture
                    string picture = "";


                    // Get picture
                    string hostUrl = config["Utility:APIBaseURL"];

                    JobSeekerAttachment jobSeekerAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerObj.JobSeekerId, "picture");
                    string fileNameUpdated = "", imgPath = "";
                    string UploadedFrom = "";
                    if (jobSeekerAttachmentObj != null) UploadedFrom = jobSeekerAttachmentObj?.UploadedFrom;
                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (jobSeekerAttachmentObj != null) { fileNameUpdated = jobSeekerAttachmentObj?.FileNameUpdated; }
                    imgPath = hostUrl + "/assets/images/default_logo.png";
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { imgPath = hostUrl + "/Uploads/" + fileNameUpdated; }
                    picture = imgPath;


                    LoginResponseDTO loginResponseDTOObj = new LoginResponseDTO();
                    loginResponseDTOObj.token = token;
                    loginResponseDTOObj.id = jobSeekerObj.JobSeekerId;
                    loginResponseDTOObj.first_name = jobSeekerObj.FirstName;
                    loginResponseDTOObj.last_name = jobSeekerObj.LastName;
                    loginResponseDTOObj.title = jobSeekerObj.Title;
                    loginResponseDTOObj.logo_url = picture;

                    JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
                    JsonArrLoginResponseDTOObj.code = "1";
                    JsonArrLoginResponseDTOObj.MSG = "Login Successful";
                    JsonArrLoginResponseDTOObj.data = loginResponseDTOObj;
                    return JsonArrLoginResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "There was an error in processing the request.";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/ajax_register_save_changes_mobile")]
        public async Task<JsonArrResponseDTO> ajax_register_save_changes_mobile([FromBody] JobSeekerSaveRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeeker jobSeekerObjOld = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }
                }

                //if (isSuccess == true)
                //{
                //    long administratorId = sessionObj.UserId;
                //    IEnumerable<Administrator> administratorList = await administratorService.Get(administratorId);
                //    if (administratorList.Any())
                //    {
                //        Administrator administratorObj = administratorList.FirstOrDefault();
                //        string role = administratorObj.Role;
                //        if (!role.Equals("super-admin"))
                //        {
                //            JsonArrResponseDTOObj.code = "0";
                //            JsonArrResponseDTOObj.MSG = "Not authorized.";
                //            return JsonArrResponseDTOObj;
                //        }
                //    }
                //    else
                //    {
                //        JsonArrResponseDTOObj.code = "0";
                //        JsonArrResponseDTOObj.MSG = "Invalid user";
                //        return JsonArrResponseDTOObj;
                //    }
                //}

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(model.title))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Title is blank. Please enter title.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.first_name))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "First Name is blank. Please enter First Name.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.last_name))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Last Name is blank. Please enter Last Name.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(model.dob)))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Date of Birth is blank. Please enter Date of Birth.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.gender))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Gender is blank. Please enter Gender.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (!model.gender.Equals("male") && !model.gender.Equals("female"))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Gender can only be male or female.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.mobile))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Mobile is blank. Please enter Mobile.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    jobSeekerObjOld = await jobSeekerService.GetById(sessionObj.UserId);

                    JobSeeker jobSeekerObj = null;
                    jobSeekerObj = jobSeekerObjOld;
                    jobSeekerObj.Title = model.title;
                    jobSeekerObj.FirstName = model.first_name;
                    jobSeekerObj.LastName = model.last_name;
                    jobSeekerObj.Dob = model.dob;
                    jobSeekerObj.Gender = model.gender;
                    jobSeekerObj.Mobile = model.mobile;
                    jobSeekerObj.CountryNo = model.country_no;
                    jobSeekerObj.CountryCode = model.country_code;

                    if (!string.IsNullOrEmpty(model.password))
                    {
                        jobSeekerObj.Password = UtilityHelper.sha1(model.password);
                    }
                    jobSeekerObjOld.UpdatedOn = DateTime.Now;
                    jobSeekerObjOld.UpdatedBy = loggedInUserId;

                    long jobSeekerId = jobSeekerService.Save(jobSeekerObj);
                    // Email Code Goes Here

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Changes saved successfully.";
                    return JsonArrResponseDTOObj;

                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/ajax_save_personal_information_mobile")]
        public async Task<JsonArrResponseDTO> ajax_save_personal_information_mobile([FromBody] JobSeekerSavePersonalInformationRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeekerPersonalInformation jobSeekerPersonalInformationObjOld = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(Convert.ToString(model.country_id_enc)) || Convert.ToInt64(model.country_id_enc) <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Nationality is blank. Please select Nationality.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(model.marital_status_id_enc)) || Convert.ToInt64(model.marital_status_id_enc) <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Marital Status is blank. Please select Marital Status.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    jobSeekerPersonalInformationObjOld = await jobSeekerPersonalInformationService.GetByJobSeekerId(sessionObj.UserId);
                    if (jobSeekerPersonalInformationObjOld != null)
                    {
                        jobSeekerPersonalInformationObjOld.CountryId = Convert.ToInt64(model.country_id_enc);
                        jobSeekerPersonalInformationObjOld.MaritalStatusId = Convert.ToInt64(model.marital_status_id_enc);
                        jobSeekerPersonalInformationObjOld.HomePhone = model.home_phone;
                        jobSeekerPersonalInformationObjOld.MobileCode = model.mobileCode;
                        jobSeekerPersonalInformationObjOld.IsDrivingLicense = model.is_driving_license;
                        jobSeekerPersonalInformationObjOld.CurrentLocation = model.current_location;
                        jobSeekerPersonalInformationObjOld.VisaStatus = model.visa_status;

                        jobSeekerPersonalInformationObjOld.UpdatedOn = DateTime.Now;
                        jobSeekerPersonalInformationObjOld.UpdatedBy = loggedInUserId;

                        jobSeekerPersonalInformationService.Save(jobSeekerPersonalInformationObjOld);
                    }
                    else
                    {
                        JobSeekerPersonalInformation jobSeekerPersonalInformationObj = mapper.Map<JobSeekerPersonalInformation>(model);
                        jobSeekerPersonalInformationObj.JobSeekerPersonalInformationId = 0;
                        jobSeekerPersonalInformationObj.JobSeekerId = sessionObj.UserId;
                        jobSeekerPersonalInformationObj.CreatedOn = DateTime.Now;
                        jobSeekerPersonalInformationObj.CreatedBy = loggedInUserId;
                        jobSeekerPersonalInformationObj.IsDeleted = false;
                        jobSeekerPersonalInformationObj.IsActive = true;
                        jobSeekerPersonalInformationService.Save(jobSeekerPersonalInformationObj);
                    }

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Changes saved successfully.";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/ajax_save_qualification_mobile")]
        public async Task<JsonArrResponseDTO> ajax_save_qualification_mobile([FromBody] JobSeekerSaveQualificationRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeekerQualification jobSeekerQualificationObjOld = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(Convert.ToString(model.education_level_id_enc)) || model.education_level_id_enc <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Education Level is blank. Please select Education Level.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(model.school_university)))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "School/University is blank. Please enter School/University.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(model.graduation_date)))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Graduation Date is blank. Please enter Graduation Date.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    jobSeekerQualificationObjOld = await jobSeekerQualificationService.GetByJobSeekerId(sessionObj.UserId);
                    if (jobSeekerQualificationObjOld != null)
                    {
                        jobSeekerQualificationObjOld.EducationLevelId = model.education_level_id_enc;
                        jobSeekerQualificationObjOld.SpecializationId = model.specialization_id_enc;
                        jobSeekerQualificationObjOld.SchoolUniversity = model.school_university;
                        jobSeekerQualificationObjOld.GraduationDate = model.graduation_date;
                        jobSeekerQualificationObjOld.NameOfDegree = model.name_of_degree;

                        jobSeekerQualificationObjOld.UpdatedOn = DateTime.Now;
                        jobSeekerQualificationObjOld.UpdatedBy = loggedInUserId;

                        jobSeekerQualificationService.Save(jobSeekerQualificationObjOld);
                    }
                    else
                    {
                        JobSeekerQualification jobSeekerQualificationObj = mapper.Map<JobSeekerQualification>(model);
                        jobSeekerQualificationObj.JobSeekerQualificationId = 0;
                        jobSeekerQualificationObj.JobSeekerId = sessionObj.UserId;
                        jobSeekerQualificationObj.CreatedOn = DateTime.Now;
                        jobSeekerQualificationObj.CreatedBy = loggedInUserId;
                        jobSeekerQualificationObj.IsDeleted = false;
                        jobSeekerQualificationObj.IsActive = true;
                        jobSeekerQualificationService.Save(jobSeekerQualificationObj);
                    }

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Changes saved successfully.";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }



        [HttpPost]
        [Route("en/job_seeker/ajax_save_work_experience_mobile")]
        public async Task<JsonArrResponseDTO> ajax_save_work_experience_mobile([FromBody] JobSeekerSaveWorkExperienceRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeekerWorkExperience jobSeekerWorkExperienceObjOld = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    jobSeekerWorkExperienceObjOld = await jobSeekerWorkExperienceService.GetByJobSeekerId(sessionObj.UserId);
                    if (jobSeekerWorkExperienceObjOld != null)
                    {
                        jobSeekerWorkExperienceObjOld.ExperienceLevel = model.experience_level;
                        if (String.IsNullOrEmpty(model.company1) && String.IsNullOrEmpty(model.job_title1)
                            && model.country_id_enc1 <= 0 && String.IsNullOrEmpty(model.city1) && String.IsNullOrEmpty(model.time_period_from1) &&
                            String.IsNullOrEmpty(model.time_period_to1))
                        {
                            jobSeekerWorkExperienceObjOld.Company1 = "";
                            jobSeekerWorkExperienceObjOld.JobTitle1 = "";
                            jobSeekerWorkExperienceObjOld.CountryId1 = 0;
                            jobSeekerWorkExperienceObjOld.City1 = "";
                            jobSeekerWorkExperienceObjOld.TimePeriodFrom1 = "";
                            jobSeekerWorkExperienceObjOld.TimePeriodTo1 = "";
                            jobSeekerWorkExperienceObjOld.ReasonForLeaving1 = "";
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(model.company1) || String.IsNullOrEmpty(model.job_title1)
                            || model.country_id_enc1 <= 0 || String.IsNullOrEmpty(model.city1) || String.IsNullOrEmpty(model.time_period_from1) ||
                            String.IsNullOrEmpty(model.time_period_to1))
                            {
                                JsonArrResponseDTOObj.code = "0";
                                JsonArrResponseDTOObj.MSG = "Required fields must not be empty";
                                return JsonArrResponseDTOObj;
                            }
                            else
                            {
                                jobSeekerWorkExperienceObjOld.Company1 = model.company1;
                                jobSeekerWorkExperienceObjOld.JobTitle1 = model.job_title1;
                                jobSeekerWorkExperienceObjOld.CountryId1 = model.country_id_enc1;
                                jobSeekerWorkExperienceObjOld.City1 = model.city1;
                                jobSeekerWorkExperienceObjOld.TimePeriodFrom1 = model.time_period_from1;
                                jobSeekerWorkExperienceObjOld.TimePeriodTo1 = model.time_period_to1;
                                jobSeekerWorkExperienceObjOld.ReasonForLeaving1 = model.reason_for_leaving1;
                            }
                        }
                        if (String.IsNullOrEmpty(model.company2) && String.IsNullOrEmpty(model.job_title2)
                            && model.country_id_enc2 <= 0 && String.IsNullOrEmpty(model.city2) && String.IsNullOrEmpty(model.time_period_from2) &&
                            String.IsNullOrEmpty(model.time_period_to2))
                        {
                            jobSeekerWorkExperienceObjOld.Company2 = "";
                            jobSeekerWorkExperienceObjOld.JobTitle2 = "";
                            jobSeekerWorkExperienceObjOld.CountryId2 = 0;
                            jobSeekerWorkExperienceObjOld.City2 = "";
                            jobSeekerWorkExperienceObjOld.TimePeriodFrom2 = "";
                            jobSeekerWorkExperienceObjOld.TimePeriodTo2 = "";
                            jobSeekerWorkExperienceObjOld.ReasonForLeaving2 = "";
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(model.company2) || String.IsNullOrEmpty(model.job_title2)
                            || model.country_id_enc2 <= 0 || String.IsNullOrEmpty(model.city2) || String.IsNullOrEmpty(model.time_period_from2) ||
                            String.IsNullOrEmpty(model.time_period_to2))
                            {
                                JsonArrResponseDTOObj.code = "0";
                                JsonArrResponseDTOObj.MSG = "Required fields must not be empty";
                                return JsonArrResponseDTOObj;
                            }
                            else
                            {
                                jobSeekerWorkExperienceObjOld.Company2 = model.company2;
                                jobSeekerWorkExperienceObjOld.JobTitle2 = model.job_title2;
                                jobSeekerWorkExperienceObjOld.CountryId2 = model.country_id_enc2;
                                jobSeekerWorkExperienceObjOld.City2 = model.city2;
                                jobSeekerWorkExperienceObjOld.TimePeriodFrom2 = model.time_period_from2;
                                jobSeekerWorkExperienceObjOld.TimePeriodTo2 = model.time_period_to2;
                                jobSeekerWorkExperienceObjOld.ReasonForLeaving2 = model.reason_for_leaving2;
                            }
                        }

                        if (String.IsNullOrEmpty(model.company3) && String.IsNullOrEmpty(model.job_title3)
                            && model.country_id_enc3 <= 0 && String.IsNullOrEmpty(model.city3) && String.IsNullOrEmpty(model.time_period_from3) &&
                            String.IsNullOrEmpty(model.time_period_to3))
                        {
                            jobSeekerWorkExperienceObjOld.Company3 = "";
                            jobSeekerWorkExperienceObjOld.JobTitle3 = "";
                            jobSeekerWorkExperienceObjOld.CountryId3 = 0;
                            jobSeekerWorkExperienceObjOld.City3 = "";
                            jobSeekerWorkExperienceObjOld.TimePeriodFrom3 = "";
                            jobSeekerWorkExperienceObjOld.TimePeriodTo3 = "";
                            jobSeekerWorkExperienceObjOld.ReasonForLeaving3 = "";
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(model.company3) || String.IsNullOrEmpty(model.job_title3)
                            || model.country_id_enc3 <= 0 || String.IsNullOrEmpty(model.city3) || String.IsNullOrEmpty(model.time_period_from3) ||
                            String.IsNullOrEmpty(model.time_period_to3))
                            {
                                JsonArrResponseDTOObj.code = "0";
                                JsonArrResponseDTOObj.MSG = "Required fields must not be empty";
                                return JsonArrResponseDTOObj;
                            }
                            else
                            {
                                jobSeekerWorkExperienceObjOld.Company3 = model.company3;
                                jobSeekerWorkExperienceObjOld.JobTitle3 = model.job_title3;
                                jobSeekerWorkExperienceObjOld.CountryId3 = model.country_id_enc3;
                                jobSeekerWorkExperienceObjOld.City3 = model.city3;
                                jobSeekerWorkExperienceObjOld.TimePeriodFrom3 = model.time_period_from3;
                                jobSeekerWorkExperienceObjOld.TimePeriodTo3 = model.time_period_to3;
                                jobSeekerWorkExperienceObjOld.ReasonForLeaving3 = model.reason_for_leaving3;
                            }
                        }

                        jobSeekerWorkExperienceObjOld.UpdatedOn = DateTime.Now;
                        jobSeekerWorkExperienceObjOld.UpdatedBy = loggedInUserId;

                        jobSeekerWorkExperienceService.Save(jobSeekerWorkExperienceObjOld);
                    }
                    else
                    {
                        JobSeekerWorkExperience jobSeekerWorkExperienceObj = mapper.Map<JobSeekerWorkExperience>(model);
                        jobSeekerWorkExperienceObj.JobSeekerWorkExperienceId = 0;
                        jobSeekerWorkExperienceObj.JobSeekerId = sessionObj.UserId;
                        jobSeekerWorkExperienceObj.CreatedOn = DateTime.Now;
                        jobSeekerWorkExperienceObj.CreatedBy = loggedInUserId;
                        jobSeekerWorkExperienceObj.IsDeleted = false;
                        jobSeekerWorkExperienceObj.IsActive = true;
                        jobSeekerWorkExperienceService.Save(jobSeekerWorkExperienceObj);
                    }

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Changes saved successfully.";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost, DisableRequestSizeLimit]
        [Consumes("multipart/form-data")]
        [Route("en/job_seeker/upload_file_mobile")]
        public async Task<object> upload_file_mobile([FromForm] AppsUploadRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";
            Session sessionObj = null;

            int StatusCode = 0;
            bool isSuccess = true;
            string Message = "";
            string ExceptionMessage = "";

            string FileTypeConfig = "";
            string FileSizeConfig = "";

            FileTypeConfig = config["FileConfig:FileTypeConfig"];
            FileSizeConfig = config["FileConfig:FileSizeConfig"];
            string hostUrl = config["Utility:APIBaseURL"];

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            //ApplicationUser currentUser = await userResolverService.GetCurrentUser();
            string userId = "";

            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(model.token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(model.token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }

            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(model.document_type))
                {
                    JsonArrResponseDTOObj.code = "0";
                    JsonArrResponseDTOObj.MSG = "[document_type] is blank. Please enter [document_type].";
                    return JsonArrResponseDTOObj;
                }
                else if (!model.document_type.Equals("resume") && !model.document_type.Equals("video_resume") && !model.document_type.Equals("passport") && !model.document_type.Equals("id_card") && !model.document_type.Equals("picture"))
                {
                    JsonArrResponseDTOObj.code = "0";
                    JsonArrResponseDTOObj.MSG = "Invalid document type.";
                    return JsonArrResponseDTOObj;
                }
            }

            try
            {
                var file = model.myfile;

                string webRootPath = hostingEnvironment.WebRootPath;
                string contentRootPath = hostingEnvironment.ContentRootPath;

                //string newPath = Path.Combine(webRootPath, "assets");
                string newPath = Path.Combine(webRootPath, "Uploads");

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }

                if (isSuccess == true)
                {
                    if (file.Length > 0)
                    {
                        string fileNameUpdated = "";
                        string fileNameOriginal = "";
                        string fileType = "";
                        int fileSize = 0;

                        string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                        string fullPath = Path.Combine(newPath, string.Empty);
                        fileNameOriginal = Path.GetFileName(file.FileName);
                        string fileExt = fileNameOriginal.Substring(fileNameOriginal.LastIndexOf('.'), fileNameOriginal.Length - fileNameOriginal.LastIndexOf('.'));
                        string fileExtension = Path.GetExtension(fileNameOriginal);

                        fileNameUpdated = Guid.NewGuid().ToString() + fileExt;
                        if (file.ContentType == null)
                        {
                            fileType = UtilityHelper.GetContentType(file.FileName);
                        }
                        else
                        {
                            fileType = file.ContentType.Split('/')[0].ToString();
                        }
                        if (fileType == "application") fileType = "document";
                        fileSize = Int32.Parse(file.Length.ToString());

                        if (isSuccess == true && fileSize > Convert.ToInt32(FileSizeConfig))
                        {
                            JsonArrResponseDTOObj.code = "0";
                            JsonArrResponseDTOObj.MSG = "File size cannot be greater than " + Convert.ToInt32(FileSizeConfig) + " KB";
                            return JsonArrResponseDTOObj;
                        }

                        if (model.document_type.Equals("video_resume") && !fileExtension.Contains("mp4"))
                        {
                            JsonArrResponseDTOObj.code = "0";
                            JsonArrResponseDTOObj.MSG = "Only .mp4 files are allowed.";
                            return JsonArrResponseDTOObj;
                        }

                        if (isSuccess == true)
                        {
                            string savedFileName = Path.Combine(fullPath, fileNameUpdated);
                            using (var stream = new FileStream(savedFileName, FileMode.Create))
                            {
                                file.CopyTo(stream);
                            }

                            jobSeekerAttachmentService.Delete(sessionObj.UserId, model.document_type);

                            JobSeekerAttachment jobSeekerAttachmentObj = new JobSeekerAttachment();
                            jobSeekerAttachmentObj.JobSeekerAttachmentId = 0;
                            jobSeekerAttachmentObj.JobSeekerId = sessionObj.UserId;
                            jobSeekerAttachmentObj.DocumentType = model.document_type;
                            jobSeekerAttachmentObj.FileNameOriginal = fileNameOriginal;
                            jobSeekerAttachmentObj.FileNameUpdated = fileNameUpdated;
                            jobSeekerAttachmentObj.FileType = fileType;
                            jobSeekerAttachmentObj.FileSize = Convert.ToString(fileSize);
                            jobSeekerAttachmentObj.UploadedFrom = "Admin";
                            jobSeekerAttachmentObj.IsActive = true;
                            jobSeekerAttachmentObj.IsDeleted = false;
                            jobSeekerAttachmentObj.CreatedOn = DateTime.Now;
                            jobSeekerAttachmentObj.CreatedBy = loggedInUserId;
                            jobSeekerAttachmentService.Save(jobSeekerAttachmentObj);
                            /// un Approve Job Seekers IsPictureApproved and IsVideoApproved
                            var data = await jobSeekerService.GetById(sessionObj.UserId);
                            if (model.document_type == "video_resume")
                            {
                                data.IsApproved = false;
                                data.IsPictureApproved = false;
                            }
                            if (model.document_type == "picture")
                            {
                                data.IsApproved = false;
                                data.IsVideoApproved = false;
                            }
                            jobSeekerService.Save(data);
                            JsonArrUploadResponseDTO jsonArrUploadResponseDTOObj = new JsonArrUploadResponseDTO();
                            jsonArrUploadResponseDTOObj.code = "1";
                            jsonArrUploadResponseDTOObj.MSG = "File uploaded successfully.";
                            //// TODO: Update upload files path http://ofwpro.com/Uploads/306477b2-ea2f-4e4c-9038-edce6db9415b.png
                            jsonArrUploadResponseDTOObj.path = hostUrl + "/Uploads/" + fileNameUpdated;
                            return jsonArrUploadResponseDTOObj;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "There was an error while uploading the file.";
                return JsonArrResponseDTOObj;
            }

            JsonArrResponseDTOObj.code = "0";
            JsonArrResponseDTOObj.MSG = "There was an error while uploading the file.";
            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/ajax_apply_job_mobile")]
        public async Task<JsonArrResponseDTO> ajax_apply_job_mobile([FromBody] RecruiterPostedJobsAndApplicantsSaveRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Validate
                if (isSuccess == true)
                {
                    if (sessionObj.UserId <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "[job_seeker_id_enc] is blank. Please enter [job_seeker_id_enc].";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrWhiteSpace(Convert.ToString(model.tbl_recruiter_posted_jobs_id)) || model.tbl_recruiter_posted_jobs_id <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "[recruiter_posted_jobs_id_enc] is blank. Please enter [recruiter_posted_jobs_id_enc].";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Check existance
                if (isSuccess == true)
                {
                    isExist = recruiterPostedJobsService.IsExistRecruiterPostedJobsId(model.tbl_recruiter_posted_jobs_id);
                    if (isExist == false)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Job is no longer available.";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        isExist = recruiterPostedJobsAndApplicantsService.IsExist(sessionObj.UserId, model.tbl_recruiter_posted_jobs_id);
                        if (isExist == true)
                        {
                            JsonArrResponseDTOObj.code = "0";
                            JsonArrResponseDTOObj.MSG = "Job already applied.";
                            return JsonArrResponseDTOObj;
                        }

                    }
                }

                // Create
                if (isSuccess == true)
                {
                    // Create
                    RecruiterPostedJobsAndApplicants recruiterPostedJobsAndApplicantsObj = new RecruiterPostedJobsAndApplicants();
                    recruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsAndApplicantsId = 0;
                    recruiterPostedJobsAndApplicantsObj.JobSeekerId = sessionObj.UserId;
                    recruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsId = model.tbl_recruiter_posted_jobs_id;

                    recruiterPostedJobsAndApplicantsObj.CreatedOn = DateTime.Now;
                    recruiterPostedJobsAndApplicantsObj.IsDeleted = false;
                    recruiterPostedJobsAndApplicantsObj.IsActive = true;
                    recruiterPostedJobsAndApplicantsService.Save(recruiterPostedJobsAndApplicantsObj);

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Application submitted successfully.";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        // activationLink = hostUrl + "/en/job_seeker/activate_job_seeker?token=" + token;
        [HttpGet]
        [Route("en/job_seeker/activate_job_seeker")]
        public async Task<object> activate_job_seeker(ActivateJobSeekerRequestDTO model)
        {
            string token = model.token;
            ResponseDTO<object> response = new ResponseDTO<object>();
            bool isSuccess = true;
            string content = "";
            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            try
            {
                // Validate Token
                if (!string.IsNullOrEmpty(token))
                {
                    JobSeeker jobSeekerObj = await jobSeekerService.GetByJobSeekerIdToken(token);
                    if (jobSeekerObj == null)
                    {
                        content = "<!DOCTYPE html><html><head><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'></head><body><div class=' w-100 col-lg-6 mt-5 border border-secondary shadow-lg p-3 mb-5 bg-white rounded' style='text-align:center; margin:auto'> <img class='mt-3' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAYAAABS3GwHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAGGdSURBVHja7P15lGxXceeLf2LvfU5m1nDnq6sBEBKTEEPbmMGIGSQxaAADpodfv+e3+vlng7Hb7jbzPIhBA6PNZMDYYLf7Lfu932+9td6vMUhoQBJtjGkza5aYQcMdqyozz9k74vfHOZmVVZVVmVnDrSu3DisXuvfmOXnOPhGxI74R8Q0xM4Ydh9/0hnPnv3fX2e72nz9sqiSkXIqjqRtI6lo+09XOe+CY8DDnhv/D8r/WpX8S1dW+CWBmblsep5YLEVnxd6MOMTbtnkvvYl4UeX7Gg+60Jzzmm3svu+RLw39z2c0deuXrX77wf/39y7Ij8ztS070o5p7kwKshgEMwBOQBBdi4tMgagu9WCP2gIli9/jJEyCZ9NcMEdOitLRXWia437PobFaG1fkcUJGvSPNjmCN0rUzN09r/8or/d/fmPfH5VBbjnCb9xefc7/+Ox6eTWC4LMYGaoGUbCi8M5h6hQpEj2gPhu8HBj/p2u+efe+9sM0znJrj5KeBXblOus956zFInZDGWCPCTKvAM/OXpl9rgnffuUb/1//niFAtzzrN982+Fvf+NxM7v3vFzmheTioioBooaZ4BCcc0Qe2AG2ThkcEFeatMEXr7K62qiBk3UJzjAXZpTgbvYOshnKmgkccpGmCjPJMW/KVMhZOHLsi81/dfo/H/jHq9/YX7sjb7v8me2vff2pzb17Xu7bXWSpjwrmMPHgHOrkf17hF138rGZzl3xnqVCbOEwCJqE+3y27jtafOCxWwFT6n2FnL/6UDBWYwY+u9ogiK4RfbOnHaWXhex+TSuDTwGet89cr/IP3P+qICNPR4/F0gsNngYgRppsvmPv2XY//5Rvecm5/B/jxI8//hJv7+StNBDBEs1X8z//Jj0GhnijGdCus4koh0FX3hI0ADus5V2ztezWzkRZ+M639ep9j+TmOynDEGClnd3z+zJ98/bfCwl/9f08Ot975cDu1hbkc6yygzYCPD8j7MCu8ER9/qSDopr/wzRD8oS6OLLuebL3Qb8bziwjo4o5hAiqCb+SEn/7w1CN/8oWzQ/n1bz7Zpu3cII55NWbI6KYH/PvNC2xHBbXDX7psg9Cv6nYMeQJ3ggr9CgUAfO3WRVUUIziPeTl3/ivXPze0D96zp6TAnBBK0OBwopjJA/K8YWVYHcEZ5uLIcRSc1YS1/9p18xChrb7/YetnvdhjWWxjVDvCgnWYOXxwV0hlkecCKQohOLrdAu/dA/K7Yes/Cr5ME+wam28tJ0Fshj3piWTth99j9VDJquxarPOGDsGS0lBDLBJaeDqSkfmcTmqTe0+hiYB/QKYncnF0rLOPd0A7juAPva5szMIvjyE2+gzLz5MxzxEREoYMuETEhCODWGaBaKF0jtIZeaGId3h169+PHzjG9qXHtf5b4dsrxmpoto64sxWokGyt8q7v+SuoV7zDIahqXckAZRnpAjtFCEnQgKCpQJ0Hc4iLk++L/1MFsqMt/nIfX46j8Pd83iVBoYx/vVEuzqQCv64n0A0CAiIVaGdGJo5CjeTBicd1umREJIkLD9jm9Qj/eqz98XVzlgv/8SxeNLN1exAbBQRWuEBI3xUyQGOxZC0eUIBNEvztwO1HujhrmODNrmWcqI5o2e60Fc/ff0ZZjAW8QSzKJcoVHhD6jbk467VYG33hq8KYveseJw92vRna47UjiQhJleAcliJWxgcUYLte/FZau+Pdn7FRNGerq+lFBGdLXUBJiiVFZXFXfEABJrD6awe1x1dIJylP2K5anO0Q/OW7tNbBtFlCy4gzBef7XuG/YAVwGxL8tYrR5DgIzigXZ6vLjTdVadWOO6reM1KqCsGhpWJFuSI//8AOMGZtzongimzGtWXALdhKF+d4W/uh91EH231FsFRlguV+oQCrtAQur7O3xRZCG5m91BHCsX7sernA6AT70fJ7FYM05AEGemDWjVutJvibiWJthuC7CR3TlaXPVflD5gWKktju1s8vVbn3ia8Aqzz2unu917b0bhNe/EYdsVFC6rbAxdnMgH4z70k34f4ruFWxRf6AFWt7P3aBNlZvL1tcejxK8Nestz+BUZztDGrXvP8hnkFwoNHQmKq+gDopxkDHW7i/C/2kdmMjmcaNCM1aQe39sd5+OwR/1ftf0X66qJyWEhrjEoP3L2QHWFv43TYL/jiNJpt1ra2+/80sT9is+3eD733FPwugqAJRQVfKRrr/KcCkpchaL5xbFxqyJcmqgYI0GfGkJ3q9/bZYe9F+cmusa6SExYSz2v2RlWwX4f4j8JPV26+nHG2rXnq/LmeVGvkTMqA9oay9rli/4efKklXXMoKmqiBuld+4/7pAQxgaVi6eG9sib7aLs+S6srFrb3W9/WYp/0aUem03Z6V/zzJVXSL85kCUlBLetFYAG/oitkEBNqnefgAOHf7ytk74Ycx6+3WWJ9iErAu6nuJrXX92ezNilbUC2srNsVXuSvrvd8mOJT1FSJVxXOgQ1Ej1+gjSv6RsHwq0eb3G212Mtt319rbB87fDxRkNHQ8GtLLGNXph7HIjqH3lVl3E/2WNpw3bJ/g60RkP1Nv/Cwxq++9Z19xdlwq+rJChXr2/WF3yoKn6/57lX+MIx1fo7z8v/v5cb3//EPy1M/OL57mJnDQxJZZx1V1ukC/ohA6CH6i339jzy/HV0yGCmNb1+ysFf7TnIFKRNgtGGROpjBUZ1nIwYUhccZwV4IF6+61WGjFODP9+maUd73y3ykKvLjcq4KzeNWKqKU8MEQdLihs3PRP8QL39A9Z+7UK08a8hEwt+/7drxjetSx5MFY+gYxrR47AD6Ja9+BPRzdnqevsTwdpvbmzihkqKW1P4F10ZkQreVK1wfzHD4Sra9oEguB8ML88E9yJuVyNLIh61Euklmjax3n5wmsl66u11gv0nIrgs4Bc6xJbHBJrzJdbIqoWi+jtlEUGofsMgeJyusyFkzNKKUSOCetfpK9SApd8Max8deFukDFcqIeqta5EZs9HR0RLvAx0q2WgVxoKU5M6v8d6X+9pLMfteomq57LgJPQ7nBFGtLH9ZIinh61+u1mr0Kq2+A0xcd69jCf7xsjRBlXyuSzmVw3wHm2kgWU6Kig8NklVIQTIDM7xUk28qc2Jb7uKMuv6g4G/FEbR3bSP2LKkTKq9CmCqEDhHLckIJuSXavqQQmMozYlyLP1/6b7v3G0v/WSeMDVeXHO0pQNI6EJ4sMRiWamkc2CE2h9/esXkVmJMoUNEARWkmI0xNowsFbUtkec68JJpWlx07tzg1RRbJU0eNGDp+LsLWBNkBQWVlzGNSBbCl8yQ8hRmGkntPwyuqkSKVffLZ1YR19ecfFwiR0XGEVkGvlRFRRcRhE659GLrgtV+1nps/XtZ+lFC0SgjmKGOkmxI7JNBtCix0aeSCWkWb4ZyAc6SUlgRVtg1Cv1E6wInvVUAHFL2XSFIzAgpBaERjSgKdoo13wnTI6cQS87JC8Ida+7HdpKUKNDqGVixVkKfVrpuzChWaZAKlAzcmD+jKmurBj5jVAcjkwr8VREntmGh7R5k5TJSFZqLZjXR2eEJvuzQqXz9q36LZhPDdpty/2kR8/JtxRIwkSzlzJFWCJCKkPJE5wVJiQUoIDskCR7XEsnwTnl/GdnX6169dUzGQpJXwp4hH6vG9k9+HWynS41p+xSyt++VPMvBsPdfO82b1UstEyzk0Jha8cqe2+WkGKfe4PNSWz/rTL3uD37b8/ntCr9uTRLAB4e8jSr04KDhcYdxJm6PTGU0LmBlFJnSdVRQj/ecXRvdqjOPju6EwanVvipgOyFxCY6pcVeshPbUCMJkBC/0fGUPoNzq4bTPpANfknBcoURpRKWYDoasEF7i77DBfGu0yQrNkdz5FyzuIaSm8uIw6Yyug2+2GL0W0pwG4GgQwgdIZbRK/bAiHOol7XZujWYOzUgs52kaanoZ4ipFGYv2BbXWfaQmfUIXUDUy6LCJYVeufaqFXq+BOsfFRRrfqzSwb97l8zOYkYengaM5x7MFmuB6hSKRWxkwXEsZ37Bg/63Z44l9ewtlveCU/OXgPx2KX6KopIoNbbIUGba6bM8m5bosEf/AeXBUt9iFDqRWgrZG5VHAowpP+66U86o/+Fw7de5gf+4L7ZhwhKm2nq6CEg2/Y1rD0rj9+d9Xr6FJZ67lmZopqQjX2p1maQKrr/ns7+tg7QCLhfBXpixhmqX4BbplVXLqYIpO/JrdOa9+z+Etck+hxVhJbHl/UpbFqla9alnQbLUIZKbznR8VR2hb59c99hH0Xv4xwEaR2l39+34c4/cA+HtxokHXadLynQZNjdGlZr05xoMG6Hg/qgq+23zED2kkt/mYYiFIMb5DVW5n1Kiw14UVQSQh5BQV7CFqCBO5ykGLBr//Fn3HghedzygsNbcP3rvgID997MiHLmE6J6CIdVWYkpxSjoyVTPqOrSk5ASau/eXPIwCxk0+GIn9auTK+e36lhpaJFOSDsvfWVda2ff92jn/DEdMcPL7Q86ycPBGUtJRrlY21KRpXh7s4A5kBqZchcm9J7vARKV71oMo/vJmZ8zq3cwz0i/OqffYwH/cbFOImUMbLnvHOYLpRbvvRFmA7sCrtJZiRXMJWalERUagqNuonCifRZxtZag+2qtx/8fdcrBtPeQGtQqeBdh1CqkKuSvMNZgfkmt0vBgnZ40l//OfsuOg8r2kgQ9pz3DMJC4kdXXsPMbI6WhgVHiODNYaUSskCJVVPZXcQvpqSGWrcezrZ6Y8zieU5cNfElVZCn1a7P+gSrRvmsIH/w6df51579hCfqbXddKHkO1vN4dGIvddODWVn8DLt2lIQVJY3mFL7uo3CpmgaiUXGNjFviL7k7wVM+86ecevFFdMURfMA7cCXsPPdZ+BJ+9KWrKKY9u3QKA4rUxoWwRNm1V3Ne3dAKnG4j9Uyb6dv3LVstItYTfAFcdf8C+NAgOkee2pif5laZ57AWPOFzn+TAxS9CTfABUhkJ0mTX+U+jPXcfP7nmRvyOWXYUBpnHDGLmcDhCCQs+MeWzNWL72j0aIS+DBkZEMFVSETFNKyz+RhQgyAApWuU7HR/B32gxmvceR6DodJFGAzodfJaRCZgXbu3ex2FzPOUzf8KBl7wYH6GZKQkhWIAcUoSHXfImFOO297wfOUU4Pc3Ski5trV6SOOknVxKKF9dHi7ZE8TcrttBUvcleC2A9NNpZ5XKU2sVTYvkOflDeS8d5nvr5T7H3ohdhUcmcgWSEkKEdxTcCv3r5JWRlxvc/9mnC3t0cWFBKUwKO0juC92QizGmiOYTUYBIQoHJ7en9QrExV4susKnbbpPULff92Qjhu0ycWTni9UEJXgDxjpkjcMyPMdEtSgh/5gkMxcs7n/oyTXnYBdEq6zUAjRZwXlIwukPsCi45HXvJmnHPc/J5LkQOJk7VZCYtWEJx3Duc8ilXBlrHt9faj1svMqtIGV+1Ykqr77onltDO6THGTHqHrA4//7Ic46cIXgWlfgcx5khrSqqm6YoPHfvhdaCvjh5f/KW7fSezpGAuZMVVA8kJWCHlwQ3iFbOIq3wrRsQrvj2WNCm3uioflVtmOk9BvtN4+1edPl8Ixl5gtYAcZ3/Qd7iuNZ3zmT9h/0fNZUGg0PQ1LmM+rQjwHLQUkgERSSjzsXW8klZFbPvBBZP+pHNCAU7AyQkxI8IiTmmWYTX8Rkwq8G2UDe8IPSJ1v8BIwJyhGJ0XuyjocTZGnfv5TnHThi+hom6a2IIugeaXoviowQ40jouwsA49+35vRcoEffeAvOXryTs7seopY4K1BcB5Jkdh/aeuz1VWlq1VgQ13m3FvzxOYpQtgqSz/WmM4NnO8QyBypMJoq5M7zT1nBsXsjT/vC+9n5ipeS6NKyKk+IVBaQAmiUdJzRjDn4QEpdvLY4+31vJVP49gf+FL9nJztDk4Y6NJWIVgGJGHhxx8312cjvOOsJi6s8IC9EjALlzoZQapcn/sUn2XPRC8EiTWvSySKhMFIOmUa8gEbBOWGneWIGWTfnsVdcgrgpfnb5p7nv5F3sKRzqKteqK4lsnXOmB583pYQWJZbSlpmbsNwqs4VViONa/LEUwyK5ZZQorTzje/EwB48pT/vrD7D7N19M3lViIyAk0MCCBHKB0CgRzWgKFCGSp4CGFjlAWXLmpa/DAnz/0g/w0N0nscc3EecW64PUcJ4lIN92CP6oaiVNhjnBekUeTkgCbUssaEksuzz+c5/g5Be/EIsG4rEQaVpGzKGhEF2o66WqXbN0iouR1MgJZYPHXPYmTDvc/sHPEPfvYV870XVKyypFWxnIrU16vrQA0Eix2p19HdTrIEK0SeLp33j2r/1acesdF0qzWW8zjopNRdaxZQ0HvkzqlrT6H1XGuLr6yudrCAkl02rrrvoWEkXeIisNnwVuKg5yuCg55zMfZtcrXk4jesgFhwOphn5n0lt+378PX5dChT487RA8e5/7TLLC+MGXribMNNkrOVnRYSFA5lqURDJxfSRIRPBS+dpqVg1nHlgPW4fAuyVAmCIVuV/9UbRGpMQEkQBWfUsNxALiFMXjXCBaIrcS5zJ+YJE5KXjo3/0VDzv3ORVtoK8Ce7FqbazSl2q96puQWgid8zig60vymHPyC55HPNbmx1d9lWwqsMtyikLJnCOqkofK7epYSeYdmhLBHCY1dclA7VU/6eUcttBBtMpQs2wNN7QbLEeBdNiL2IT9Zi3akHG4L0UMGoFyvo1kntILJKHjjCzPybslTZfx3XQ393nPkz79SU56yQVAWZc1Z+tYm0U6joe+548RMf7p/ZcS9+/jEY09NMoO4tq0rEkh5aJi19yTvs4gm9om8Nvrmvbfmaspv6uy7yXWUYwyCU2UrihBIsk3uc3aRLo8+a8+y4GnPxWCozTFiauaYXrJTYtVfLTG0dCMGBQrSx57xZsovHL7hz5O2ruT06TBQqqSVb6jlBLJGxmmSgtP26V+xt3M+nkV5xyoEosCZ8cHZQtm5gYTNyrjZWzXou9b66bHLVMtpMC3hcZ0C19CVxWvhldfYdOtJt/u/ox7u3DO5z7G/pdcyJwZ02Q4HzdgIOqxOlE4411vpBTh+5ddjuz2PLzcQcSxYAt4ly1aq57l73VXmVUJuUmEfgW//erRzyBgoVhl9euGFiyRgBByohNC5wg0dnKbm+dg0eHJf/lp9l3wfDCl1Ij4fPCmKuUaI8tvRuXlZw3KwnjCpW9DrOT2D36KbNce9miO5I5uUoyMkBQrlaM+0nRZ5aLVmfUe+C5maFnld44XuBw2Kqhj+6wTXi8XD5lDF0riVI6fK9CWp6WOhUy4pbiPe/A89bMf5cBLXowYtIJVOSoLGyqoERFcyKGAs971JlDj5ve/H04SHppmmY5dOlolUMRXrlC0RDKtXK5J10jGqZVfVm/fq4Pp4SGiOF38nZKCoBFp7OQH6SAd85zzhT9j38UVzh+d4WrhT2UiD75O+CnjFFMKilrl6mWZoB3jVy97NyYZP7zikxT7dvOgY5GOKDmO0gOZo4VnThIz4ola+QmV5TdSWWIx4utu3uOmAIPW202YC9sqoiifhMIpLvM0u5GDM9CIifkU+aEvuK/T5alf+DSn/sYF0C7ptDKamsAppYR1OEBLjy6QhS4ues665M3ghO9eehm6P/EgbeK0hhdj6pcX9OqVnFtGybEufvuVOPoSB0kV84tXcckI/SpWoemhrTm3cIyO8zz+zz/MgYsuqJJJCAz09A4KfC/JN+p1Ri8EhVTfgm/kpAi/eulbCOq464MfQ/bv5bRjxrFGolV6BE80peH8IptbDSxYiqS6s8tX0MXxU4AlzdZjdEMdj+EOyQuiQjBYsIJWKezSjH8MbQ6X8PTP/gknXVjj/C1P0yLJZVWj9yYoYzOB+QxzEYup2gnKxE0f/CD5vlM4SQOSDCmrunQJvl+2sWrl6AT89ksrblc+UG/nURJOq8I3qbH/5CBG405fMIfy5L/4OAcuuoCCLnnMIU+kUvHegwjOV8oQU8SH8YhCFKGQLnlyJJ9hCi5ALOHxl78do81Pr/hLuifPcGbb0y1LxEHDB1KMxNpQOCAWBamMOCpgwjh+2fUhibBK+0fV22+WoK/24oWqUEsiNJOH4PhmXnDsYOLpX7iUna94GWpdGrga7VG8gRbgGyVscA8ofUmWMvCBrnRpaIuz3vdWMhP++QN/gts5y668RW4OKVOfrrViVpDhSauJ+O1lJEhgMtC87wR1QuGMjig/yj2Flvzan3+KvRe/EEzJtUEnV7KSJYKeYsRnAR9C9atjeAF5CWSe6I2gEAGPIlkG0XHW5e/Aa5M7P/gpdp60i11FoMgFi4mO1woJMiPVTG6iVf2ZiqBWsXYcj8MNCt6wsufNTIT1u5DGaDaxuklFVdFWxi06x73zHZ7215ez6xUvoVEajoCvE1yFBZKAa5Sg2YYXJrMMfES6DnyL5IFUcvqlr+WsN7yaHx+9j2OpQIMjYpU/W/cUBLdYUD6ks2DZZ4jwr1Un3/uYVi5YvWuLCBFhwZSjsaQsj/Erf/4JDrz4hUiqmmXVFTTNYVmo3DWLleHI3DKYcfQWWmYlxICSEUkEU8QcIQlFiDSKKR75gTdy+h//r9xy3z3cPSvk0ZhzialUwa4pJWJR4tQIdVOjWjyuWfYwKPh9ZoBxqvVGWbMRu0T/33Up+0Hv751zuJjjGp7byp/zE0089y8/xb6XXARFgrxCpnvGchHLyDano0TqDbIBzd49+4A3OPvdb8XEc9N7P8KZJ+3j5CzDt+fp5oGWa7Jg3YqbUqlxbKE0RQWChGoOsOmqzgWiJF/FGM5kybr01skhlOJwDpIqXpXgcu6yElyHM/72bzjtGedALEnegzic5X2yKAHEheH7zRjyl5FBqNfd+cU190JOgLxL1m3xuMsvR6zFnR/8IHbSAU7rTNGWLloWlJZoqJKRKGol9uoppeIs2hYUaLPyAOO7OsOVJkVhptnlq/ZLGuU053/iQ8z8xvMrffF+S7qmJskTPPLdr2E6Cv906aW0T97Lw/0eXHuOwi+QuxYxFUv6opxzeKmCv5TSKoHKolK4uLg+vXXpDcp2QIGRR6OjniARzXPusHli6vLEv/gcpyzD+VNKeOdrxo/ROP+G3y8ZsVFiER53+TsI3nPLRz5C3KEc6AZmFpQCJWF0qRJjmQqjOoyPCwy6kcHRYwfOq7Av9F52aOZ8r/NzPIHHfu4K9rz05fQT1J5tO3pKkEfHGe96PV1nfO+DH4BdnkfIFNEcbe3QkoDW7ZaqEAy8LO60MiLp5fttitWXE7Y4CkoE74wUMvLuApLPciuHua+Y5ylf+BwnX/yCFTi/1JnqcXH+Da+TOjKFGBJJHWde+jbKtMAPP/xxfOsAe0Vp1A1YpaOK4+rw19vmtc2ORBtf++gnPCneducF0mz2F6iK8beW338t4TczbuUoB6d28YRPfpB9v3FxlfL3hlgk1XTY26kEyXmcOvad90zoRH749/8N3T3Ffpum1V1AyUBcVcRQsy70Ss6rkgtdEQcsCc7MY85hIqRaUAbLI4ooeOngG01+kO5jTh1P+ctPc+AlF1SzcQHnM6TG+UON+PRw/q32s1NNMuZU6Xojx3Pg+c+m3Sn52Tf/B+2G0iwht7pdsw6PeolY27qXt6QUwm0mdMkmBNC9cw4vzHHGM5/Og156IY0y4J1gJEwjaVscoGVBIJBCQUqJR1/yZh79pjfx4/vu5rbsMAsur4i2aq6dTFxVJixCMh2LhilhVXBNj7bElvQh7MgEtYyb9AhtHP/qcx/hlBe/CK+pjgd8XUQGYRnOf7ySTOpKzGW0NODKArGMx73pP7PzwC46GRS+tvS93gtsBQfscUOBeoIvMDEx1EbyBEtXbBHWO23HAX7yf/8/fP8t70UaStTKKoo0yHTb5Z9mCV5ycAkSPPrdb+KR//k/8aN77+WXU0JqBsxXjSEStWfWMWdr9DMNsCY4WRIneK2K0XpVnV0tuUsic5rxpL/4Mx7ykhdT0Kk0M8vQWNGKYPRx/qTpuCHsTrs4y0CgSAlcTtE+xtdf8Tvcfc99nDIvNFO9D8rik/t69ziuMUDPwtgACGDHQ+BZigQNPvdpHcNmG3z/0o/gmoEz3vpHOJ3CGUQP2TYrgGUlkjKcz0l0CKnJ4973NjITvv+BjyE7ZtgZGhXsGEvUrP9ig1VcNqvYoWUGSfrEXYgSqXh7fpQJpZb82l/8GfsuOr8qD4+BTsNolhVzxXKc33k/Ns6/YU/DVSFbSaTpjfs6c9z0kt/hZ9d/kX8lJzPdXiDWRXxikKTKDXg4rjvA0kyw2kQh+GYRwApLKQnFACl5UJrB7/bc+vYrMFUe8fY341TqhsbGtiqAkFH4SugIFVCqMXHm+9+AiHDzBz6IzOyh6XOcOso6/+EV3IAyrLbgLhnaQ3nVUBKqQscl2iRSNH7lLz7OgYuei8UILmChag6KWWVJTWOFPg3g/GZ2fHD2okHKFR8d2pnjjpf/Fseu+yqPCLvZMX+EiCdilYuG0XGQUBrJHe8g+IlPirfefoFrNQfs/3BWiIkEvm4ml+WxhVk/9hvsHVjudjmtuG12kaOzTe788nXQPcaec8/BdRrEEHEVfFCV0DqhAEKE5PS4BMm9fgI3EGB5HHuf90ysKLnl6uthKmNHCkynRAdDkscyXw/5FMRcVfciPSYHIYriLWLiKwJkqbBynOfW4DhGh4f+3Rc483nPrprcva+48ql8if6wIVkMm/u9BSKbYv2TFTgRCqp1zwCKRPTgSKBVqUYoDnL9b/429115LQ+dmmX3sUiSBkqPzrzqcPAmOBNsKwPgIUFw2DJLryuzypM8meaeUCiawakdIdu9hx9f9heQch71vtcSug1iQyufkapmSCSCF3z02zL+b0me4JLX0cLzT5ddhu7axcPKXTSsQBsKhZD36oac4utYQVOV3Q2ilHjyMhK9IxCJocmddOlql1//wp+zf4P1/BtWfgkQHbnVjrsoKReCOlJyWAZF9yDfuuh3+cU11/H41h7csQU6uccXc7ht3sHX3n/ZOPnrkvPXM3Cik2hK3h+AdqAwpvc2ufPSj3Pb298PDcXXg9WCVoGhj0opie42zr7s7WS+EB76jtfzuNe/nl8cPcpt2TGCNvCa0ZF2pSyuqt6UpFhSTBRzVZFY8DllFsi0g/fT3Ok6HLaCX/nzT7DvwudDnlGarqznN44Lzt+pt5qUVf47VMaoSIkyAz36c771klfx8+uv43GtaXa0S6bUYUVkahm79HYe/rWP/rW+C2S9IQM2eUvkCr/eVuUFGzPI9LhUkdwGoGh69ndBZpvcduVXsW7B3uf9Os5CjxULdRmhbuvbTqBURDDvMXXsO/cZ0In89MtXUewM7Cwb7ALmxWqUSCHFqsfXOcxXtUVljIh2CflObtJDLABP/sInOeXFF5KiomLbi/PX8KqhlOKqXTg5fACOHOF//LtX8+Mrr+GsRouZhS6FRkR89Zxq2Ha9oRXMcEsUwNaMASZxkza6/GWoWuW8GZp5GmXV/DFrjtQK/OQr1xFN2f/MJyMEiOCkmnriVTenJnoDRxvIXERLZe/zno0k486rrkRnHc3S1Z2LA+hbr8c3KSSl5SH5BrfLAh3neNznPsypF1+M1L6z827pbIa6AE/6sdfWPr9TEIs4MRBH3ps4076Pr7/i1Rz+0pd5RD7LbCehZuzEca8rafoGTv1xLXleUwFec9YTnpJuveNF0mz0d4BxFGC1gFbYHIQtUyOJ0lSPc47C6rE5WcaO5PGtwE1fvBJnyv7nnlP5vK6i/a5oULZ3a83KXmeZ4syx9zlPh3bkR9d8FZ1pstcyfKqSPylUuU8fq9Ji5wKFKT8MyhGLPOnzn+DUiy+mnRbINEBQUqm4utCuJ/wxxUVF2OIFEIVU18C5stoO2p1DfPOi3+Wea6/kIdPTNEplWj3JIke9cUAz5rWL+WZ/kPb27wBnPeHJiwpgIxWgb+m3WIGjwB7JOSolSY2meYIJZexSOmM6eqb2TnPzF7+CqrLvOU9BoxLFKjdom5PFyUdc6cELKhFS4KRzn05oF9x81XW08pwA+H6dT1UMpl6Yz4Xbc88CHX71c5/gwMUvQjAyy+hkCoURsqzfypVixHlX0bfU72ajycyRKKeLZOopkuEzQRfu459f/CqO3HA9p0212HnMKE1JVk2dCRJIZuQETBLbtQGsUIDXPfrXnhhvvf2CcRTgeA54yMwzR6QhrgpqnYAqKRgNBScZrQKa0zl3/v3VuCic9LxnEmKgCEXNTryNCmBASLjS0/UecYaLiR3nnYPvFHzvxmuZdoEZl1UKoErmA/NB+Inv0E1tfuWzn+TUl1yIRqsCW58IFtDgK2oci4gD56Vf3tAHPbf4BXkUTa4ixj12jBtf+rvce91VnNLI2XskYhaYsoSjqJKFKqhkVNFCFyOcGArwmrOe8JR42x0v8q1mXaFZF2oN1moNcXO2+jCxukVC8KmCVasqSqv9/JLojVnLkOmMO666Bivm2XnuOeS9PEGqQfo0kCdIULpyyxXES81LVCOyDsGcw+HZ/9xnIbHNTV++kWxmhl1dwbyRfM5tvmTO5nnY3/4NZ577bMS05u0RsLpdUGRVnH+zrJNqrIPbARKwaCRnFc4fA2WARvso//1lv82xr3yJh85MseMoOJnCKEgIRqiICjB6+e9tE/4VCvDQ64b2A2yz+zwWe4JmAV8olsMpHU/YU+UJTHMe8Z7XkHcbxEYFUZdS4dUiEfNWtzpub57gMW97C7nl3PzeS8n2HmDngufndpCjyfHUv/zCttfzRxfIi8oTsFwqRQwQ1BEVfIDYPcjXL/49fnbNNfyrqT3YsQ4x87jiKNtfrDLeEVw9RKFfstsnrd1u8tcl9mhl3NGtEIXSIFriQMdxbHfgzvd9jJAlHvWOt+BUMQnkCilUeYLCG+a3r5CipwRmnoe//XW0tc3t7/swzd07KMuc53/2o0xfeD7I9tbz11U6WG6olXh1iHe0rSQLGfHw3Xz3X/8hd3/1Sh4zNcP0giIpp526NEOgHe8X8o9/3VlPeHJ56+0vcq1mPXVPaksr22rtR9XLW/BIggLFYZRNz74uyI6c2668Drol+5/3NMR8hVg5Q10gs3BC5AmiFPhuxr7znkPUkoNXf4Nf/9vPMHvRBZRxAcRvK84PgnhBrRqdVIrDmSMXgSOH+fq//0N+cuWVnN3ImZnvUiYjUHGxllpU0PSJeCx3gXrMcMd7XOdGB86VXrAYcerQEMg6CTM4pRuIs7PcetlHSLnw2Le+BsiQwnBBiA5CUvDbCxN5gABawuPe8TYe9a9fTnjk6QDkWXNJQ9J6eHs2/n5ANOJxRO/ob0ALh7nx372aw1+6mrPzaXYsFJQm7CLwSzrM1AEvxv3iCCvdjeNzuHXU+y0yJwh5VykwpsxQq4qynAiWeU6NDfKd8L13XUGmyqPe8XosVc3yHlBx295SI7jKLasjzMYjzqb0JY4SiVnF9+PcEt6epKn/31uex4A6U10NEs+CY644yrd+4/c4eO2VnDk1TbOTCGR0LPGLEDkQM46mDuJbkMr7jwIM+t09x0M21ZrYEAGY5NyV4lqKsEcaHKSLTwUNqzhtuu0uKYP9ZY475SS+/d4PU4jw2Le/jhQNDYHctr+jLJZGZhBzJaYuWWiRlRlIAg+O7a3nLySSa6BtJS3nYe4g33nZ73P4azfwsNYUu46WzHmYw+EQWuqYI9Egw1LifhICLBYM9Ef+bFICZbOK6VbbnxrqOaglDfGUuSPmoE4hE5oKTgJ7jwkP37mTOy75MLe+/QOEEMgLR+m33zpViax5lJKmtBCMmFUlzx2xVXl7jlc9f07V0J/5DOaOce3LXsl913yZ08XYcbQkIkwnIddISzKCKskLhSSUgvvLESqGYamIhWvnT2Spi6K4MS31CBRn8dt10d3KrqjFTLNfRhqrDEJBySkZFRdnI1b/ngScOZJAKLuUmWd/mqK71/HdSz9IYW3OetdbyDoZqRnxMdQ+kVJ4SDhaJWhWrotefTIXCMima16duoG8Mv00e0mtYbw9m2SgSiIBR6rfdK5AmYgNIaCgFatFNj/Hjb/5Kuau+gqPmGkxfSSSfANJZZ0f8HTr4kmfrHZu3Ylh3deUwIorLCwX4t76TvIQ6+G3Xy78a9OF65jO0+I9l40MV0TI4CHzntauPfz0fZ9DUs4j3vM68m5G2agIZaOr8gRJIgTDldn9BcZev49vAVLtAwfAKalR1fNbdGhe4/wvfiU/vfZqHj+9A5nrUGYBV7bhREV5xpTAXjGeWw7PrdvFEV36WVNIHWYy8FlNgZTxmuNWkhBqUdJwOZlW5cYnzytTezLueO+fcus73wsNxaWIGWTKQD+B0f0XLvzVDlAtbQoVlLyknj8HDh3kWxe/ml/ccCWPmc6YXSiZTg4tEzO+cb973h4l53KJmkiNN5ff3q2iKDqGsI/j3AmpLIkeggjdhuPhHfj5vhluee/HcGXgrEteNzDu1EghrwJT+ZevAEkgy6jGooojJiGYkQehOHqYb/y//pAfX30Vj2412HW0S7SA0MShtFOHE32LHEY9M5jd0kqe3bpbIjfKbz9a+Icr0dj36utp8skoco8vlGjGqaUnTc8szRNIDl3FZ/6EyRNsuQukgFVTIJ1UQwBVBGvfyzf+9R9w6Mtf5uzmNDvmImo5O8j5uUSmQkBLu18Kf//fZNBOiuhy16fq312FsWxM/H4Uv/1oqz+Z8Fe/t3idvKjGgTbwJISibjonZDy4dGQ7hO+98/LFPIHPsDpPYNvKO3d8Dq9goRrkoWWELNDpHOI7F72Sg9ddw5lTDfKukpGxQMkvvbI/BY6VbYJvVFUDJ7DQ64DU6OoYqC5mggcttaxX6Mfnt9+oqyM2qGS64te6DvbQ5D7r4KLSsoCI0Gl30ODYV2TYyfv59ns+tDJPgONf+pF8xGugo4lm5ijb9/DdF7+aYzfewEOnp9hxNNKWxEItEzlGh8gUOTEZ6QQX/LFh0NWsqYwUfFsi6Cv47Ye6OTrk/Mmt/dIdZvg9esu4ly4t8XR9NUjOxYRlQmaKJ2fvEYUdVZ4gT45Hvev1UDpiVhL+hcNAXiAlqsk2R4/yjZe+inu/ejWntVrMHkkUEphSRai6zLopoi5QqpGOK4P/xoV/2C7Q83pCxQRdoTlejKhWNVmMaIy3tbKpfcHXVZQGVGRgXNCyaw0JkFf2HK+t604SuTmSQUhSjRJ14FMFgKXUhkbGyWWD7g74zhUfpuu7nPX2N5K3G8RWSajLJ0iJGDyRihLRsnjiFnv1UZ6SDE/E0QFagO8mUkNwKJICRVCa3UN87RW/w5Grr+Zhs7PMHok4ycDKOptbEXgK1dzmukZ0y+/fDTOZMqal78mOuRXnD46lUCCsLqwypquzPr/ejVAgMd1QB9pyhVn+5zLzhK5izcBDup58egc/efdn8EXW7yfo5wm8J+vxDgVD7gd5goCH6AgKM1n1Pqwh+OQok5BlIJ3D3HjR7/Kzq6/jcdN70PmKtyecALw9OkZgO0r4x9k53HIBc8bQQW4V5i9rCL8yGrsfIH9d89F1U2gX1yrHaJjgEayIWEqc2oZde5v88NJP8IN3VHkCiWWVJ7DFPEESuV/kCaJVvD2WV6NPsSpOKk2xXNAjP+c7F7+au6+7jrOnp5lpR3ZEBycQb8+wkVrjZoaWi2mfel968i7gJIUlEyJtLWsvk+jrsu+78XReN4drdKwgUMCJUnhPA0+RGWfMwS/2THHLpZ+gkRo86t2v7VcH9vIE3kDl+DcMTXqIVBFeiWJOCBrwEbKg2KHDXP9bf8QvrrmaxzRazMx1KNVokNMAilJhm3uqx3Z3Vpzo+uK6UglkefNojxxXJ3BzdA0vbZjD4obf5KCPrxtjhFzPbqGawDm8Kd1MaJTV3x1YEIrpWW5+/4fQhuPRb/pPYBmuFPBQCmTJqijyRA5yE6hEcoFUOUTVsOr2Qf7p3/wBB6+5irOzWWYXEsmEnTjuocu0b5GSwTbgPOsW+pFukqy4dn8SUU9Il7oKMoY/vwEYUwatvR43oR88mqlim3BRMROKEMhwaHA8uGtkszN8/11X4GPkUW9/HZBj0mugOPFhUgGSq/mGCoXc0eke4tsXv5J7r/sKj8yrev6mOY6Z8Ytg7I85R1IHF6bxMW2L4G/WubZKWc/yXsdQh0c4qwaVmcqSUUnri91Xd3OGDcc73sIPcMwZJ0mDQ76LGIQyggqxLKHp2VdkyP49fPs9H6ILPO4dryeWioZAQ/12ewgjj46PNDVQpESeQVq4lx+8+PdYuP4GTp+eYechZUESx8QQE5rmmRejZRkpFceloWsjGP5KwZeRyjQo/NUkGsPpklm1NubtrJxzu0T4zfXH+fTH+mjqk+RWLrStGiQPQ4g22l+w/MhDg1+UbfKyGjidvCCZQCvgi0iQwO5DysN37OSu936Um95yGSEEGsmT/Inf7tGEanp15tH5eW58ye9xzzVXcnLw7D5U0hFHZkbQkpYTsmSYhKoTzTontPCv5uKMu5PYAKlAWAof9XRkrSBvjO1fFNO0jr2rWgaVzbX2w3YPtYKmcyRXz6S1ah5XUMB7ulIQvOe0dpP5nYFvX/FRnHM86m2vrdkmFodUn5gwUCDminYXuOEV/xtHr/kSp02fytSRDuIiuWrl+tKgi1SDwDVWJLdsHAXaEI6/Cpy5JNc6Yt2V4f0AVS6gMvsiE2dzxs/QbtTFGeTQ2UzB769v1H5zyWBQ1B9ckSK5Qsct8LBuYHbnfr77/o9QpoLHXvL27SN3HTsKhnB0jm9c/O84cv03OKN1KjvnDxOAjjkSi0NETLWu2BJE6uTRBtd+3Tj+MjRnI/HBKCVLy8cBr27NVmL3YrrkU7k4afHP6xDQQRdnq/MAvSdSWMEkrRg+NGiHAvGejhOOtQ/SaDWY/tWz++XSJ6z1B4qyIM4Gdj/pV0k+kkIJETouYVZVf/YG8fkqz1sVAtY74Wa5ORvB8W3F+PpqbrKtY937/QD1p2fAQvUSdWJrv1nWeatcnJE7bD2Ttif8VWwyEA/FiDNPCC1uzdt0ypyn/tdPsOeFL0TuBzAoeU5AeeTl78byFt+54jJO2znDGXNNDIdaQTKrRlkNlKUYVbmD32CeY72Q5mLCan2/r2vsODqEMjWM7ddvdhC0TYK/qN+y6G4Z1ZzagUtk3ui6aW5hnmMFPPO/fpap85+DxPuB8Pdc0DKQssSj3vMmkhduu+y9uB37OPVQbx5ZImnNMSrVNEpByPokQMdX6McTcFdLrE70W724cjn4HoY1pq/ODK3LQKftPTZKrtXDsZxV9fG9hYquwtBv9gc5kjV53mf/hMaLnk2jFMiMDonmCV4MlyloCEAkll0e89bXkoJy8zsvx+2c5cC8hyS1y1pnt6XaCb2OnwZbq/lkcqGXsa+pI67dE3hnS0GV5d8Lg0GHWS8FnvrWsCf4so3CulnXGlpcp9YPhs0JXW8Uzvhxrmg38PS/+Si7n38BdAtiwwgayO4H7QJKxFnAxYBlJRSBR7/1zXhv3P7Oj+LynJnkmCo9ThPRrE8bOI713ywYs7qOTPRkgx6LbVAwAzYsrelG1ttvRFgHZwJvh/D3fP7euotVwl+KUYrRFqUTlSf9lz/j5PNeAKURG3lFJaLge61jJ/DhXCClRMw8DWtVZFuaeMyb34SbV+748Ecw16QpvkdTUOHjImiajBt2GNQ5aijiqoK7ai/JMphiAvFZaxcIUhehiSWcGdGo6LdHjUhaBaYcR0DHEf7BgHQ9KqhecMmW+PZLymBFgQw1SM7ISJjz3OkBujz87/6ak59xDlhJyjwBIViAik3kRJf/Cgn19RQEqW7YmcMMznrPW+kGxx2XfgybbnDKUaWFcUQSWRKcqwdu9MpVnPSayHFWsftHZOi7GbbWa0QpQ/5a1wWlribgy31/6V+n1/y6Tss6rN5+M92cSUhRhq5j1L4SmVRzm02qKlATKJMjU0VF8ZR0fcZdGZS0+ZUvfJZH9/j56yFwqgN3YvcX4r/VDc9j3/V6Hv+aP+An8/fxiz1CKdOEBOKNpFVuoBcVmirUf1bvKLzb0LsZx3pvVPhtzN9edyS37SjOiPOd1sk0WbmgCoSQ07FI0wq60uKHWcEhLXni5/+M/Re+AGwpP3/9Ixwvfv6tVAIzI3QdZ77z9cRg3HTZFaTZGR5ytEmRCjou0rQANeYuKeIwFEHxlGJr9gOt5tev119fDdIced4Y/xbur4Lvlp2/sqtY+unu1J9dvBh7FKnAU1KEae4M8xxNxlP/y6fZ+6IXgELEcLXwpzKRB1/HzIrczykjRAQaHqfwiHe+mRgjP7zsg7B7F6cdbrBXjWO9ZJFUM8gyrWBSS1qjwDK2cK8HKVq3soy4pixTnnB/E/xJzrcBmnpfo7hSu0AzQVigyV1Zh2MGT/yrj3PSi15An+dogIZ8O/j5t/poAy1XzVE7+z1vw0S47YoPIntm2X+f4c1RWsIUcI7SWeWeG9UYDNkiAZbJfPxRrs7ymGQwGbaCG3SrhX4zrusmEP7eb7l6IVz94tQJ7aT8tOU4rJGn/vWn2f/C59OlS6PMIKvm8Pp6KkuPkz+miA+BfwlHK9UW0SfEMh59yVtxwG2Xfxi3czcHjpUErSbBm0GSaoSrq9dxGFRqG9gaV8CqoxRsHdcdFP7UK4VYTTBPBNx+GCI0yZWWb7PJQeGh9MYPfUZJmyf/1afZ/cLzETEaZTWHN+sqPl/0/Xv8/D6E48bPv9VH6UuyIoNcSRQEy3nMu99GXgrf+/An8Lln2hyN6HApoWr1zGOQZa2r4wq+jimkm6VE43zPDRPWzazz2ci1xGyJ8E96TbfsoZNA4YwOitk8/+qzH+ekC16A1BVSliWa6qGRr8rPX3m/93/euEBGzCMkRyl51ThfJM689A2c+ZpX8TPpcMQb5gWH4A2CODIcfkABVgiduaGMDNst/CqroECq6qweW5/MEDk+jelr5RHcGFBe6aqUfc+96T2YQyq/zyulesCRRGloifiMHwUo4xxn/u3f8KBnnAOxJHkP4hDL+lojrMLPz2ZZf13SwJ0A1xt+kSJssatV9cxX8xGaVXCD5VVn4OMueQsO5dYPfAqmAjtjZEoTpYFGj2aOXk+QGJgXSle9u5ASTiH58YLkSX37jbhOg7xAhqEO3TQ8b1JrPyyPICOEf/DIBoR/qVgZSYxuFPKkqBkZkcI1apy/y6/99fbj/IZj3sp+/5GnpikXsG2KMwbzBI9+zxv4lf/0e9zZPsTRXQHcDhbU8Lnhyiprrq7Kr6gqErWqp3JVX/U4MKZu0JBMGjcM2422LQ/QO9+t06g6HUB6ZCVXRZZldFKkZV0KWvyoUXAwFTz5rz7N/gufD8a24vySjOAzVBXn6tIT8dseX/R2Zl8EHnLJGykyzy2XXUa5YyenHJmiGwvMRcR5tFYYp+C1QsYSRnIbqy8eN4m1lvCbLDWOg/kgGZCUsN2Cv5EXpVhf+PuKVLtDURNOC8owzZ3ZAnNqnPPXn2b3hS/AEkTR7cX5DXyPhzoaEqodCxzqFL+NyTYRIeVVnuBR73gj3RS584oP4HfsYt9czpQZCwpJFvsJxNVjh6h5WFfR4vXCmeP49yN3CAZdoB414hYJvlvl/M2SrcrVWdT0/keqBFjLwbw0uT20WTD41b/6GPsueCFSD37GbS/Ob0GQVA3FQ6AoCnKX13MN3LbXGnWAaYkkFR73nrcSxHHT5Vfgdu6gcximVFAS0ao1E7dYO+RkMQGzXh9/0phh5A4x9HsjeoKPR7JqI/7fYuBb17HXOH8SmC8jP5nOOBwjT/urT7P/whdSWJe8DJAnUsm24vySqjzDvHbJfIb3AVBUIUfY7nTzdFnV/eATYp6z3/1mVJVbPvgRTtm5k3w+YUlwNX16wlCpuFS9CVE21lAzqW8/bnn2YiA8xAVanORi6yJqs3Xi9es93MCjODMiSmFG4eCHLU+iy1P++jPseMH5gJKnBp08kheGzxarWbYF5zdHAhoukFB8WylajswFJLLtM+gsK5GU4QySdPHa4LGXvBlBueWKTyJZYCp4WhEkxSqx5KTaBJIO7ZqbxM2ZJECeXPiHeCongrWfaCyeDuQJ1FBVoia6KTKfCpIu8Ct//gn2X/CCKnFjDkKkmRyWZ9uP83toEwlRSHcf5K6//L/JcUTSCVFrXZIhPkJ0RBqUUlWEPua9b+DM1/wuv7Q28z20LFWl5yKC1C7kZvr4myH8q53jqgKZKvhyfXx6vHr9wc8g77MbR5B1daaGwYDW6SLiU9GXSAWfS4mVCdqJslNSlG2Cwk+bOT9rGmf8n/+FU19wLiGW+FCrvoX+7C+hahrp/eISytRNkP8uZR2pwELv9XeVVHnOJOsyYwEt5vnn//W3+Nar/j0/eNelZNExL5CsgFQLTipIaDXZsYTucdhf856DkEEDyMRh3mOW8/j3vJ2H/8f/yC9UuXvGkxy0rHKBNAoxc0sYxkWkiguAKEbpbFUiq9Xg0UGGiepjS6Y+6go/39UEWHW7j/TafhYTdUmWbbSbOfVpFIYhfnEw9pJWxcFEV53UclTpd5dSVZeuRkeVBokuDgmJZmxw84xjznd52l98hv09nN8UV+P8fXjTYhUIb+HRwEPpEIGpIJglUgNC8hV4HgLdhXv5+m/8Hgev+WdOOvBgvv/299Mk44w3/0dIOWVuBIwkFSu1SYTM0SjdtswnGExePvz9b6QRMr552WWk3TOcemQa32mTNROp8EQX64Zr6fcSiAhBXCXgQzwGnag5Xta1QyzfkdaNAm3USKYBOLQn9CLSx26D9fgbtWrRU0OSVnX+QO48bVVC6JLbDLfsUuZSyZNOEJw/pgphiKFa7oARcBSaIPP4Y0f41stfxZHrr+ORYTfTv+zidrb41iXvI8bIGe96AxJLxFXCHx0E04qTKHPbFiL0lCCLjjPe+Tqig+9fdjk263jQ0SaxKIi+xJAKURsU9rqOyDF+CcRmllIPC+2CrToNcmvgyzTE2vcE37QKvqsFTiSsGuhQu1lWu0AigmrCU5Axy83TBUdEeNp//QwnveD5dA28bS/On7wQfEW5Ls6hFnAKeabEw4e59t/9LgvX3cipzjHVOUoZHA85kuF2N/mn919O15Sz3v2aRUzClCg5warxr9sZKIhINVsswlnvfCMpJW7/wAewPbs4cDhnWrrM1bGZF8GcI9VUVFLT0KiTiYVYR35/jakwA8omA5XC/o8f8binxltvf75rtHA2EL5sYlA8mGJ3tT/oneB6N6OGpYSpYimiqWKZM6tq8x2VO9RDfdSMZhDEmtzRihzOHb/++Y9z0gsvABLBKmjT9SFSQ+ref3F1o8wWB7reQLUkc+DFV4zwDmLnPv7x5b/H/Je+wqmtBifNlyy4gmkLLJhnZwfcTsfPvnw96hz7n/VkRD0+VUmzBASq4dbbeRQG3pWQYP95zyYVyo++8hVsV8DNa8U1pLXxqqax9PsznMjwCS6Dn2WCb6z1/ZWjeG117a3ksSjwZz74K/6PH/G4p6bb7ni+a7QqoWNzaLKk/qHex1XMk/3/mWr1iUqKlW9vvQUzMKrBzdTx62BwbCIkFX66I+OXvuQ5n/80+y68gNJ1cSlDvJKi1oGX9IU/prioCFusABKrZJcTwQqDTJjrHuTbF/4uh669igft3MmDjxQUlATL6TqHE4ezyFQX8pmcm6+8CjD2P+fXq8I8AXEK4re9HtUnSE7AJxye/c9+BrJQ8rOrryfbOVsN4kZwJkgvsJUBAVwuxKuggqsKfn/+tIwNvVfWv1JGLbpkZzykUgC77Y7nu7zVnyO1Odvk4qc/AaYWck0JUsKS9hWB2lJIT3nqxuvKWltdMyaUXigyxw9bOYeakad97tPsetHzCc7wZERvEK1KaNULnWLEeYc418f5t5rXs3AlmVZjRX0GsX0f37nolczfcAOnTbWYPdRmQQTzGcEcRe7IopKhqPfkhTK9d5qbv/gVVIV9z/l1NCWiCEEd2z3KuHBKFh3iPckKnHn2P+9ZhLkud173NbIA4hxBHJhWeFhP8NVW8LGOcndshaKsLfjLaRD7cYarFNCKolKA1zz8cU9Lt91xPnkTUd3EBNAiya2poTGRUsJSQlNNvGU9Ha7+N6jTvSmRFYRW8dUk52gHaAc42Iic85lPsvfFFxBVCeIRiTgVNPiq9dFiNfLVS7+4ofd7W25CxXDJkTJBjh7jH176Sg5f9xVObjaYOlpA5gkpoHhUElMx0XaVgpuUmDmaC4mZmRY/vep6YjT2Pu9Z5OpIrsDJ9iYLPIJ5RcpqiqaJ4ZKx4/lPIy10+MUNN+C9r+DTVDGOOu9wzkHUVRVg0eq7pT6RDPeRVotgezDpCrhVlinAGx7++HPad952fpY1Kk5/AbGBUWKyRjA7YObd4NZmhisixAQxYbHy76WP/FTCbsu02wY4XFQSDihCXjE00CF3DQ42mtzVKDjrb/8Lp5/7bJwp4h3OVY5Sz8ev0CU3KPKbjPMngjlMoI2RIVAoyScUJRSBdm405w/y31/+/2buqmt40MwsraMlTnKwWO9uWmPSFUOzMxBz5BgWPM0CdLbBndd+lVjOsee555B1GpRZiU++etCYiM5RACFBdLGf1dlSHaeam+brd2pSZZNOet6zoPDcddWVNGdymmUThzI3VSBRyS0jukVqSqlzPrWHhOHWjAmGCb4uyxGsGpf2ZKwoyB92+pX+dQ9//DndO24/P2RNMOsTxVkval/FVQji6uaTOhObKkFPZcTSACdPH0Va/N/yBxj2nLkElAxPSYaCm+aW2ZJugif/H5/hoc96BmSeEqv8Y9XqXmvLv9VQZzAHsXpxwQlYIgWthmurI2XgOoe58eWv5p6rruUhUzO4o23IPVnsYKsAmb0X3xXDpSqhM9OONHfN8PMvfZ3ULdj1gqeQd3PKrAqMSxwZ1RxjcQmfsm1xkQZlZc9znoDXwJ3XfpXQMmZiE9c1gjOSDxUTHUvHl9qg8E9wjJ0/qHcd51ylAGc+5Cq3dPFl6X8714cdBx/OzCBGrCz7H2KEGHGqiCbUNjbrt2vgKYla4i3npzuUY5Jz1t99jAPnvQDyjNJ0Jc5vHB+cv85kx1BRqAiGx9OxRAwQjt3D11/2ag5++UpOn26yp63sIsOVibwuvluZ3Vy8flMygoR6Oruy71CH2Z2BH132Sb7/lncvmWOc11lNXyYUobuN3lGfdkZaPOJdr+Gs1/4BPz82z30zRjNr0kbwYiuUXh2orC38g4O5VNbXVFOV0Q8YslV9vAF3xrTO1urAwAldFPDesIFFP971u+6HP4iNRGEikYb3NJo7uVU6zEXh1/7mozz4hRdWzGVmJx7Ob9AMSnnkMDf8+9/nni9ezcOmp9h1pMMCRiYZDYP2KjHIoAEqxRCLpDwnV2GeyEMOG4f2THHb5Z+iKVM86t2v7cdSmJKyxgkxx1hEaCbAHGe96y2Y5tx6+RV0drbYdyRDNULNSdqfzmPDk1XrNaG2BrIkLB2Qof2ElNU/qYZp6gu72KICDIrwEl9wSfS+tvAPS7otr9dukViwjLtCh6NOeMZffor9F7yIblnQ8Pm21/PnBklLctcLYCB60M5BvvGK3+fgVVfysNYO9h5LdJxjFuGIlkizhe8aSBp4USvvtkwlWchAIwvOMaWB0kpm7+tw2u6Z/hzjs9/8n0EDLlXVHSfKHGNztVsbHWdf8jpM29x5+Sco9+wiHC4qBbCauJbBCe4Dw0smdHdGNsYPrHRP3vzrH/a4pxW333a+czkaI2aKJasCWO1Z+gqmdP0AVgZw3clszWAcsCTjPOD/KpDT4KbZgrnQ4Omf/xj7L7oACkVycCmRkp0AOL/hxGGFoZmw0DnIty78XQ5dezVnTM/SaEe8GvMWKTLHjtDAuh0Kl+HEBpCNlceUC5QoLlaszRo8mJAc7OiCnwnc8uVrCKrse+5TwQVwlZsrbvtZK1RqV7SuZDzpec+mPd/m5mu+Qmu61S95cTZIWLsYCy4vlVhrCqTJ6A1PB+MAEazbJXvY6V/2r33Y2U/v3HbreSJ5BVHWNTfLLXYPRzH6ofrQAH15vDBaIQa2upq0ChF+Np3T2THF0z7zEfZdeCEpRXwmWHJIEJzz24zzd8k0X8T5O/fx7Yteydz1X+NBUw2mj3TpOkO9kKtQeocrEuYDDRk9gKItymyshnl7BCkSHkHNSAFahTC1Z4of/P3VqCkHnvNUYhmJ7sTIE7iUMBxRFB8jahl7zn86Mwg//od/Ius5y/2WvgHsXkYL/SjBV4YP8+3DoN0u+cNOv9L1Cs7QRajSrWGxqxJUq0sTlmVoMdS04pgZ4fYwJOPc94Gd8BN3hNlnPJedF11Ax5SQPF3AO0NrpGA76/m9+KpyPDj06Bz/ePGrOXjdtRxoOFqHS0wyppKRlx18EFoFtPMGyZQu7ZH+Q5DAIReZwlOS0FBVc6cMcq0Gms/eV3Lm7Cx3vfej3PzWy8myjEbyFL7Y9h0AZ3QdZMmDb+C0Q8MCp//Rq2jsOYXCVwV+vRqengeQ3Hh+/yh3Z5gBHHaO/+PTH/WsdNcPn+ek0Z8Q7Nbw4vtb1BANWw51jhLIFefXPqFTI2s0OPa97zI3P8eDznsmSCKYYeKrngDA40i9Mou6dr7KPEdwGzOB3bqCMyF0gMyAMpJ8VS/lS6Xwnnx+juv/7W9x+KorOb25h9ljEckiZlUNurqAUvUweKug2sFOVB2yliYVCO3FUfYa52tD0vOZvSViEJqFw2ZyfnjNV9HuHHvOPYesW024zLTqN67u21ECIUJ0JRvtOjYtEBHaQJZ6tf7UHWQgUhKoOuxSgpgF/MGD3PDi/0D7zu9zUuEIdYWAifWtv7A+q2+DpQejhN8JVpQ0Tz/9mhOW53v/scRev5ObL/1TvvmGN2M+gGZQJ4yq+nitOXy0r51zGPiNFws3SFA4fAlTKCYFMQ/4GJBodLMM6R7k+pf979zz367loa3d5HPzdBtCtxzNK7QRfn2AlHuCQkTZd7TkwTt38qNLP8dNb7qc2OjSKnK6dbIp+YBXCEQIkRA33kwgLkBHaZn0MmGolaARb6m3ghDBZdA5+FNueNn/zqGv3cBubS2ByHUdlIbDklyDH13NhC+Lj05YBXAGpy50ePjuWX76kc9x52vfw3wG0gFSSVcSksCLY54EXnFJmbLNcX9iqqxnzKppKGKBQKSwRAwed+Qw33j5Kznylb/nzJlZds0rLVOs7NLKdm7Muq6BgPQ+rjRCcoQacp4+vIDfAT+67JPc8tb3Qq74Ok/gqVwLH5UEFJvQTFDgoBEoBDqxhJTIpVYGqZikw3xJkSudw7/glt/8I+75h+s4bdcMzaKYCMcfFgv0Yk1bhuszLLk25NxeZap/zRmPfkZ5113Pc5L3vXUH2z4HXbyj0A472h5p5vzgxuuZPdJl94ueAUkJLmNBjEyrrHEhhnc1uljl5DemAE7wXolWgAREpUp2BcUOz/ONf/+H3PPF/8ZDplvsOKYsqCLBCAlK84vwxhpJnaFb+xC2s6HBYObRGNHg8SK0vbKvK4TdLW676jpcJ7LvvKf1XZ2Egg94repk3AbjJJ8WzWdeux2FCKnOBglCCh6bu5dvvux3ufv6r7K/NUPWKen6iDc/2gisEeT23OAlRAwD3x+MAQarT0VqBLNb0HjoQ75ywiqAWSSXKTyJBkarNcVN115LWcxx8nnPo2OJlnnMC6KV4ZmXVOHymwADektoguA8XgwxQZ2j7BzhH3/ztzn8pS9yemuWXfNCV4QZyZgjYc284s0cUo+yxN8fUeuiqyAgvat2JBFCVQISHbTwxJRodhN+R4OfXHkdJrD/WU+pTW2V3ExQ+d6bsEZG3abqhFiXxmRJkFRP2zx2N9958av45VevYXbHFHsWMrxvUFIOrVVaS+hXRXVWiwv6fR9Ld4VeSbR2C/IzHnLlCasAVWmz0rGC6eRpdRWb8fz4+m/SmZvnwec9DZKR8IivAu1chGTVNu83CoNGIKtq2a1UNDjmuwf51kW/w6Frrua0mQaNrtCwJqWWlGK0XI4VBWXu6p6G1V/aWkK/ollkyO0FIJqRJUA8yVfwSXKOWfO4VuCmK68hmLL/OU/FuVAlpzYpT1DWqIVzVqFz4vAJurGEXMh+/nO++W9/n59efyWn7drD3oUG82W3QrPwDOJ2NobQjx0Q93D+gTZMlgl/BYMWNM54yFUnrAIUCJ4OSI6TjEjBSamJb0zx46uvQtvKvvOfhLeAmKPtKmEQJ3jchisBCoGg0DUlhEBsH+TbL/5tjt1wPae1Ztl5JLLgoSDRVK2YEEpBM08eU583c6h1k9HWftT6Jx+YiTAfKpZgXxqZOGIq6VqiVTqm9k5x0xevRlKlBCkmCgeZbnx9PL3RqkpCyNRhDopguHt/wT/8L3/Ez6+9llNnp5D5Ek2Kb+UURLxV7qSNcHHWyimt2lE2wq2S2l3ToqD50BNYAZrME/0sQR2Jbg0GekLRprl3B9//yg34hcT+88+hcI6mVttuMF/BcZuR6k0BCw6OHeEfXvo7HLzuBk5uNJk+1iHYFMEUR0EIDkpjoRnwMZFbInq3poszKQKyQgDVMy+JqSxnwSlW5740CE0TIo58ITEz2+RnV90AhXLS855Bpp62L8g2CINGASdVyYmXgJRQhki4725ufMV/5p4bv8yp07tpdowyJDSruvicwpQp5Rq7UI/echimv6qgy+oJ2b7fP5gIO9EVQMkQq8DCXlClKA6h1Y40d03xk6uuYaFInPrcZ2CWyMxQ50g9xjirXpRQBcd1lx6mkSjVHKxePX9AkEJJ3jASvsiImRLa9/K1l/82x669ngdPTTF9uEvupildUZeBeKIJ5gRvVUNRXCMHoatUf47c5pd9klOcq1y+UPdN68AQOHERDY6pQkizOT+67nqsM8fuc8+h0Vk7T1C6hNO4Ns5PWVFKpQAG3UxpHDzKtS/9Dxz9x+t5cJitSlYwnFVJw14PePJuTRx/MLjtz3mrXbdF9GfYjro8UpAlRqZfyeaE2C2ZOvP0L99vB14dOBopp/dw02UfJWmbJ7zvPSStPMugisNhTuvhnnUVk8A8xrQLVWIrVmveClU9f8yren41h+XQ6d7Hd1/2+9xz9Y080rVIh9vE6SZufg785Fj6Ruj+Js4TaMUk0XHCSUcird2z3H7pnxNp8JhLXkOraNDNEzlCclVOARfBC1n0EKTC+RuhT0BR4fwVoYERSE5wmlDv6R78Jd98+e9w7L9/k/3TM1ipqxIjrIbjj3r+Jd8ZORHGrfoOBv/lhN0BRvroZpyaBNkR+Pn1/4TOtdl13rPIIoiPFbFWql7WAonMCRKNzFWKENVwJsSsonb1VMV1RTLIHHH+l3z3JX/Iz6/+Cg9vtZjuKjs0q3ahYJi5JU3bo7jrbQKW5HHH/ay1W2Q+o5kcqNGWRCMp3R2eQ1d+nU55jJPPezYSK3ZqT1Vk58uS5IXSORTBe0chUMaSYDWpXt1kJ+pxhbKQC8WRn3HTS1/NPTdezyk7W7iFtPog+EEqnBrHt1V89dVigNXzJPX1ZHWGiD4U2i2ZOuMhV95vdwBBOSTznHQ4kGZyvveBj+Cd45HvexOUiSxkdERpJphyGVGUkIHEqgozecH7yqqJBKwerJ1nRjx8mP/xb36fn1x3A49qTdM6skApjhgCZVnScmGJEK5nUsmkgj8uB35/B7BEMiXmGbkF5iRy0pyxsG+K2y77JE1r8ahL/njRx7C4pJ8gS4vtjr0GnqKW6lDvqWVT4cghvvkbv8PRr93ISdN7kYV5iiCE5FcI7yCznA30m4yV6d2EEUu9XWGw2u1+tQMMWjgTpWEtzBk7ouKmmvzwq1/laPsgp51/Hh1TmniSrwIvByxItROYq8Z9mpZkIng8ooI66fP2/PLqq3iM38HMfMl8MHaEjKNakrucMoSql3dZQDappV8rsFstVhgXOuxKwoUAphQIUzjKlGi2lbCjyU+vvA4TYd8znwxaYffOCSWQqdW++Oo4fyck9OhRvnPxf+Ce629kdvcudrYVL9NEOvS46wYteTIbG8fv6yXry5MsXT+3qHAs5gGmTuQ8wFrJEQWc5ESJiJYkPLNdRXY2+NnV/8h8t81Dzn06JKOs8wQOIROhpMoThCR1PT9YVMgcc92DfOuiup5/dgfFQoeggldl3sMe12A+RUw8fiABP+plrEfwJ8mODsPLsxCwZORlRbOiWSBTTxIjT5BN5fzgyq/g6zyB+FAl0KUi3YqyNs4ffvlL/vnf/D5333g1p+3Zy66FjIOdeRAjuLDEfVEqMrNJcPxVrf2Qfx++m7pVIbieAjTPPIEVwIYI/eBLNoRo82jWoqUZBR32pgbM7OCXX/4KqZ3Yd/6TCAScObpiBAXnpOLhcSWZZpQKPhNi+z6+ffErOXbD1zh5pkXjSEGyhA+umodlRoqJVshILvWzubZJE1B0woaPUUmipNBUYb4hNNSRdRN4TxTFnNAohdbeKW7++6sxVU56zlPQqJRSQcle1sb5v/Zv/pBfXPslTp7ZiSx0KcuSrDlLzAocjRqzWyr4a+VC1qztHztPsij0a7lTIkIqClon9A6wTPiXL1STNlGmaaRAx3VAApqMqW6B3zPNLVfdiGvXeQJxNEwoveHVE12FYVcszQJ93p5r2N9qMH20wKXAdPCodusmG09sNChTl8y66DrZpVcv553M9x31fpxktC0y5QNzrioPzxAKSXgzFE/eTuyYmeKnV30V6Rr7z30mIQU6vm5ZXAPnP/Tfr+KUmZMIZUERqoEYvhr5QSjbRFl9UuRQRGgD02GGXWRtBfBo0aV1IgTBNm4rW+/me1T7NPEo0Wm/4MtqUvrdhwts1zTf/sgVUC7w6A+8l7ZW9TJdZzRIaBmwDMLcPdz4ildx5NrrODVv0Doa0ayJk4KuAVI13qszXCrAeeKQJNKkAyB0ZNZyeGO7Ll8IG6AZHAjujETwnkKNBr6OCxRf/zdSUARPo2PM7pziO5d/CPMlD3/nG2i2G8SWgSmx5jX1DdD77uPrL/195v/hak7Nd0JZoHiy5DGzqvzZSsranVoL8hw+G0CXWfPJYc5RwEOlfEqd2ti+QTyTCv545w/w0hw1wvRJ3PSxz9AN8LjL3gul4J1B4dAc4sLdfO03fp+fffWrPCqfRtodXLNJuXAMFxobytZuvCpb1l6TCX5gmHKW4siioB72HlPyHbPc8b7PUHSFs9//eswaEIVAgWUNjh4+xndf+tvc/c83cmD6ZKxYWBXDX1fiUyZ/ho08/3hqdlwzv+OPSFr57mXF9qepZNfRglN3TPHjD32Cm//o7cSsJEQl5Y5w7G6++eI/4L7rr+MR09M0usrulNEtC2Yb+dj16quiGuvE8YfFFWutSQ/lWGr91yjCq48WGTnV4JASZXquIOwN/OiKT3HTmy8hMyNpSZE1aB+8l+9c8K+5+x+/xoFsN63OsUVLvszHH9W8PvT5ZFAce5NcRj/DOO/F1vh9284dQDdsUWXNf28Gx5xXTrkXZPdO7vyzj9NuJJ5w6Tvxx45y47/9A+659loe0mwwfWie6D3zWUYoI/PBr16EtU6rPymOryPs1XpQp8H7SBLxCpZ7vBOOSZeTjzrm981w8wc+TUtyHnzJW7FDh/n2S/83Dn7rHzh5dh/+WIfoqpbPcZR9UmNgG44DRoMNg6/2uCiAbUEJwKjvHbKq1uXYVJNdxzr4ZsZPP/g5cmmwcPsPufv/+XsetmMvM0cLFjLPTtfg3tQmm24SOoJKHFuo1jPlcFLBtwmaa8a5jwKl1cwgRjrB0YoZ3VTSOqjs2TnF997/SeaJHLnpVn557XU86KS9pLmjuKyBxCbqdNXnGkx4rTdxtV4ffy3BHzSbutUxwHpG24y+5viU2vtSYI5AiAUL1uHA3C46uzNu/uif0mo0OH3HDrJOQZk5vMLdvsusBLQdKV0YyzecVPA3U+g3ai1bEkhmuKgVmVhwCA063phdUBp7dnHHBz9EK29x2r4DMD9PLp7CHEF0zedaLvyT+vc6hrLLmjmANXaYZSjccdkBhvVsrjZHWWxYrfdkXPIA7SCoJbwlvGsw5+C0ox38TEaunt3zxn25kiWlIRVW3lWl0cjJO11i8Jvi4qzXGNgmCs2wa7ZTyS7LOJIZmTNcNxFcBk4pAnQ797Fr18k02m1s/hgNpihzILUx3xw5QGg95SCTWP1J465V1kPd8RT+UURHyxG+oQ9mvTGXVYizmiAVYnSzLkbGbDmNWIe5cIx9bc/sUaNjwnQSgpXkCs0ykBpN5ssOkuumoTq2yRRF1VO7Dd9Hi4xjQWngKZwRc1BvJG+EMrInzbDj0DwdFwg2g3Ng2iU3twyu3HxkZiPPNilhbljRMT/iznpLn9Zh1WSgsGMYh7uNQ+BfL76OCIKdQTPmGIlOaKMieG1RYjXJT+8t5LQ9QCQrAQmUusGKzAmDfhti+WyEWdkolUj09cwxEbKaxaU0RZJg4mlLlfNoFQkNiQ5UyUBXTXpcdT3MrfrEtgEff5TF16HBrvRn3VWs0FbPh3aIN5zkk7tAW6v7siVWVmVrLdBGhH891lE3iLys5a9v+LrL44NVUKzNdHUmRYnGokffihe8WvJqnIXVTRD6DT/nBFZ/9Jq4ifuANyL4g7CuriL4k5ZjjG9c3MTvZVKLP/SepSJL6GWepZ7H44cpgNbtboNc7av6WZsgTOsJbMfS7nVmFDdD6De0Hlts8XXi97OxXVU3OKTENqmP2npzl1aLASaxtpsj+MfHKq9X4DfPxx8zUbTOlztxKckYLYfrXedJ3Ekb4uxOksAbtSb9jjPWzkeMdIFsE+tfbMzpdLqJLs56fPzNDWwnvz/dYlLrzRT61VydcRNdk+L34wi+LHs34/BehOUX0ppLcqPW3mT84FY3KKDjCv0kL3qjxXqj789NjOqMLjuQsX37zTUwbtPLFcY6nEwMnbhlK7+uRJgbSwDWsSJiQ89fb4/tZli5E8HHH1f4B4PdQVqRYYGwrRMHs7ERHR3re+u1+qOoHceNQIKIqGJ4J6RoAzwsy0Tc3FIUYc3tVCa2mKuet4kdV6O/v1IBVw8e3QZdHN0Uge8L+3KBH4pM2dhKaL33b26daI4buhYbMUSDJLdjKevyuKcevD45DGpuTOu3eTi+bvOYK+XEO5Za8JUveTJWBV3bTprbMAozbrnCuMJvWyATweq61hUwp7kxrdLaN6abiARtFNkZfY5MsFuNf1/DXrBMSAuyAiQYct6KWc4jr6sr4Mq1hllNFJhu0MAtt/Zb1aIbRtmCzcTxx8KrZXOFfrOSV5sh9MO2ZxtBILUkaTVCqHtb/HpcwY0mrzYrYzupm7NhBdABl6hXrDasInOUtV8vJLhZPv5GXCfdJB9/IvdglV1B13BxYDSU2Q9uRwq+G/oc67XyG30Pg1NbjucRxtkyV7P263EVjmeJwkjlm/D7WyH0vftYzog8yoXpFXeNpeBjdV1tbnHaJDHNdgj+yCB4NVRmvcK/EShz6zuuVrIqTIborMMQrDMeWHSNbIOC79b8joy5DuvF8LcqqN00BVgvjm/rPH8zA9vNRHS2yjKNYkteLY2/ZPLJUPBGhrpO4yvI8HJm2yTB39COYZsrFwChdJlOk9HxRnSeoqvsc475ZQswXjHV+MmrDRelDaBUNqQ/VVcPZNbl40/surnhAqwDK7Vm1nak76/L7sstOW8Yjj/o5rh6JvPgNQb3lo0GtSOJsDbRBV7VrTTDe4+liqpd6nciQFKPuKTOWXSHaVOmgk6Ck7ImR7UY62bsOGVRh5sDXfwME41NvLl18QINFe6BAQ9rCP84ArO088kN2wrGyt/ohCXZ63F1tvPok3DVwzV68dZ0gkKTC+aJs5IRnSdog0NxHsszfLJ1BYrrQXYmrjdfBanazD5b3dRiQBmhGKsrz+qW102we7l1C/5G4cztOlx1E33uIufcEh6jMiRmJGgI0zsX2nQQncGVBWWA1joE/3jg97YWJ/9YvzNp6+HmCP0wwV8Pn84kJcnjojq2Ce940O07kY7lO20a+O8jruCU1kwnhIc+7K7ogJBDOcesNSljAeI2hsJs1Mcf96WM9V03tsCv9z56C66ruEIbIZAav6Vza1gV7m+C31v3wWk0PeF39fy2mTLgHn7GbS684OlfzNLUlaIJyQJJFXWyqp+/Vtf9WoOM17rWKKrA5QK/GoXiIpS59DOKYm8997H8nmyZ8JsZWo8CNRnuHoyiEhx2T8Ofo3rOkc/B6nSBw35z6O/3vt8bSH4CCv8S/98WsS0vQnAOjZFG2bhy17lPv9LtOe+ZKr/y6/+cugdRPGVTiKxP8McR+rF3kAn4Mav+2tX5Mdcj9KMEf7kS9ha7/9kgjr8ewV/L2tuIZ1t+jdUEf63ZvSfq0XOFXD1vNHUKsqc94caTz3tuxQv0oEtf83qjc6WEFqGIBDbGPLxRn3p8AKe28BO/DzfWfYxa1HGEYfn3qrmVtup5KpDGWtXJC9Q2KkTjCv6JUEnrVhlV6wzKbvHFh7/xP3yov4rZ+c/WU9992VsX2j+90sICLuQkDHPSRxwyAp56UgqKiVXcPvXHln2WcxSrLP2MEvxRbNHV3KfxgumVH8Vwa37Eqg8DHxPf/4hK/7PC5RJHlWMMmPn+BwJi9Uf8ogUf+GDVQPDlK7h4767+jGf1bciObEPWhDV2cJOVO5wOeUeD70q3+WPaxrLK98+7SpmB9565e49e+ah3/Md3n3TByw4DyGCUfM/lH3/C/Os+eOkxd5/mbur8FIwUamFLWo/OMUQMUr4mErNap08f6zYdDgGOadF0gk6joVZJ0kTnr0YgtjpbhowVOPaEabhbx9KE33oK/VZBznQMi7+RHXK7D02ObhB2SU47HoOjx/5/ITXcI9/5+ref+qY//PriexyCS99x4Sv+yP7+xueXsR0i5bmVJTEcHieCWaonQK2OUIyqJVHTEe7U2i3NaQLnaz2IyWDX29DaexuhMGajr7/szpb+KRvhWox6fr/qt3oWfS2BH0V/6E7wQdJ5aLFQzuEy+6LM7jy651nPvfaJ/9cXPr5C0dd6UT//P/5un9z8w0e2urEZHdpRdV6dy1Vc9DGuJQBJhhuZ3vdc/YJX+3VxSTdLAYYrxXjNcCqTu7TGGJVQMrxmp2dQBgVM108ls253XG3tNLKI6ImsAC5qsFajs+uhZ9y199+++O7Vvvf/HwA9D+v/tNjSsAAAAABJRU5ErkJggg==' height='120px' width='120px'><h1 class=' mt-5 text-wrap'>Invalid Token</h1><p class='mt-3' style='font-size: 19px;' class='text-nowrap bd-highlight'>Please check your email or contact administrator</p></div></body></html>";
                    }
                    else
                    {
                        jobSeekerObj.IsActive = true;
                        jobSeekerService.Save(jobSeekerObj);
                        content = "<!DOCTYPE html><html><head><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'></head><body><div class=' w-100 col-lg-6 mt-5 border border-secondary shadow-lg p-3 mb-5 bg-white rounded' style=' text-align:center; margin:auto'> <img class='mt-3' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAYAAABS3GwHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAGCOSURBVHja7L15vKxXVeb/XXvv962qc86dws1AIEAI8ygQSJhnERB/ODbdbSuI0oCIoCLKpAiIqK0IiuAA2g5td2uDzIQhBMIoYxgyMAkmJGS+956h6n33Xuv3x1tVp4a3xnPuzU1I5VOf3HtPnaq39rv22ms961nPEjOj7vHXF7/iQVdfd+VJ11535UlF2lzxmVPvnaoaquoEr9z8OOYPFTAzxFK4UX8Rc5X9iLplfj2hTkQm2mCQdrOM+w4f2HuLq2+3//bffOo9f+Mzda+T0Q3wt5/9o3t99JvveYJaJ3dBXp5lHnOCqpKSgTm8D6gUN1vjUXw4pLKTrsFD9f/e3RKOD/8zyYHO8w27W3rSVp/uCLorISK116Pspek32dosKXzxhw3v9MxTHv+e5579ig9P3AC/d87TfvrCw594YLbv9s9udgpUtVp0cXiXISKYAWqYSzdb6VE1rDR0U1WGb/Dojb/xGP7MM27O/SNTry1KxGKi1RBSmdN2nnZ55evOyO74td/74bf96dgG+N0PPP+HLz7yb/dvtVov852IOt9fZDOrjl2xgQt0N1vpMTAwHbnPJl3jV7uJbABd8rdkovev9kcg2SZOGqTk8b6kIbC5kX7nwAknf++PHvv/3tC34n+5+C9u/5XrPvjovSsHXtbZNDKfDxn+tsdxQOg+3c3PKU+xxdcoYf2nyrjxVx6LY278PTsYfO7U6J3pDOMfXhuT4WfPGZul/rN6P+0GSB2EgKVI5iOo0THFr8jLvnvNN8543eee8+T+Bvj8JR94TKtx0vOKeIhGM1DGeLML3qnRyGKeLXF8hpS7G+psG73KImu5+LWZSeUsahy59/5XPv+1z96vvwEu3/jaHfNsD6IG1kHl5vDmaD/SyH/Hm9HvjqdnyCsv6kB6z+H36Hl9GzsZR5+Vwft+uG7dneTEc/nmpbd+28VvuY2/84+ecLdvbV54d0QeGrSB2hbetzBuRjmPhtFb97+btqfvGb4taPRA77nA9cnIj5wNJskD/xdQTagqMSt+YI9f+4g7FK86qaPhRWZbaAqEkJGOwxt0U/D2x2uIc/QQnfkNf1qYs4jx1wIHA7+vqiRTXHJc177qYChTkccYaTTWiDFimqGa8HKz4e7U6OfJEcTcDWb4NwR6Myuun/faxObb2H2gyFW/kKyC9l0SEikES9DIPI4MCZFSS7xltcfQzY8FDEuO0+u6ESfaM41+oFYi1v2/KIoiJuCsqm05CM6j3gjBg1NH2e6QMkfIDEoH3FzoOloGdiy9f911VYaxwx06C+Wq/W5u6ASZtGYm48ZeG+cPhDuDxl/9WUFclQukgfCH6rurRaKDYPgYZQsXVjGLUDZASm4udC0Y4ixgTzeU4fc/v1/T0blClMr4dA4Dn/Ve2r+2EiE38DESvUcyXzEMNOKc4BRMfD+U6VFDEoZ4hw7UQgbpIr3rNROcE1Ky/p9NDWeGWSI6aBV5EW425SWNS47T69qlGH7Y4y4e79sMJKchQiGKeU/uPO1oRDFy8QR1JE3gtzesiVRev5cU9z19Pdi0fcJpNw+Q2vW5eQPcSGPoo389unQSO9cplCJBIGWeTYMsCbk5koNOSjgviHRtW7thS+/vczqgwVDPzLa5bQM8ops3wKTbL3pcJ7U3hNFPjufrjH76e0VTcsmJBpaUhgrJGcmUFo5NEbwYdJPZZLqUkxCpOEOqEdXxAtrNG+BGhpocnevSY35dzmcUajSikJkQtaTjDO9yIFCmDs4FBBtKbsXAi+vToef9fj3vrzK8dW/OdG9EG3J3sPu652KxfQ9lmUREm/aePZpCZkJpioiQgtH2yh7JcdbgM+UGnaREUxwe51zfk1drMf9x3Psd7dInRtGv79sTQAdgvJu+t9+topUbuLb5YfI6/D6lROYc0UESxftA2wL/3ol8JcJtsu3Pq+J2xcRXDAeb/XkV1GlD3r+fUNvNG+C4Lgwd1evrbfxpMKYomBtKdCuvu9g1TStcRQchBDZjQTNBwwe+kLb4UjRW7QA+28CJA7OKpIlhXfDfI8QFP0+13gncaDeAmJtKOa52en0RSEVv4gavOJtAOe4bvpuC6rj+Rtm+Lht7/aDB9f7cK1CVeSArIpIUfBXCRAyli/CUTazcRJ2Sh1UuLLf4/HqL/WsCsU3LFGclKoJ5QfD47ntHSWPGPsonMgScYGJEjZQWEREyEQb3wo02B5jFtx81KDMjkY47499dMtqx49vXGX/vc1UgKyI+GeVKxmYubKVUJbXmKItIpIMFxwmxxSUx8eH1Qzz77k/iZ2/9o1xTXNEvfO3k+gfXdxAOnY1l3YSSxh4T86Zr+Dcc336Qdz/2/hrZXBHKsiSPQgiBrVRSirDSXK1aFAvHxSJ8cGOdnzntCfzumc/nO+1LOej2bie9arWf0fP4k42/m2CnRLQ4FBUMJsI3qQ0w2GCioset0e+u4S+O4Eyq0k66tkk8nGmfId7RiQmftpPRPQm8GIe2NlihwUUYH1pf58m3fjR/88jXAMoV5QbeDC9u7JSZtenGv1Pq8//7J8DI0ehuSoZ/vJ9GN1y4uPt8ezfJK/c2kgqNUsjzHE0l4pStLGEpciCscIkZH97c5IdPeTBvffjvY9KhsMR6WqfhUv/9+5IwMl/1edJ368OhMqyoEW7MRn/TRnGOb779aI6h2FCeHE3J8waFJppRiQ3ooJwga3xNEh9ZL3jMyQ/jnx/xB+A6SOnwTrl281r2Zb5/0ijbShiLrmkyHUN/bvR1gJv59vM+3JRrk9oNtgjffuYG8g6NCRWjbAVip+CA7OEq83zwyNU85BYP5G2PeS0iRRWWZBmdjSMcKg5zkvcTTybFahPkXg/AYI6TUhqK/XsNMoMdj+FGZ/hzJnh1lGM38G+7lR8cv3z7yWtmNf5jUb797DhcUYymC3RSyR5p8c1Med81l/OYk8/krY/5Q1pSomQkJ4SoXJo2uM4ie2kObdCKDUptGaIf4tjIvwn9Aph0lQ1vVCfA0eDb72ZSPFwJdTXH7M749m6GZOZgCFLHt59Us5I5mdzJjRjUSEJqJqgzMgHMKLo/b5lDNNFpZqR2JDlhTTzXieND3zvM/W/zIP7loX/ASlghJkfwAAX4nOs3vkfUyCqKikd9Rf/shVfS2wy6TZIDcM71DR7nESdsxSOYs/619wxf1I7/HODGwbd3Mz3g/DG2zoyx54nxZ52SYjv9zsNHRBaNFAzFkSdBvbAZhCwZvh1xPqdhbb7rW7z/+uu574mn846H/QF5I+86it5nVN59PbZBq3qBmdF3+trXOBkSBavH9hWz+YGHcHwa1/fD9Rw7Buaihj8owjvp4TGSU8Q8Qar+WoBMoe2ElZix4kuuaQc+tH49t9p/O/7p4X/KwcYKWADpNapo/4S/pn09aCR3K1V3IgxJyPQ2Rc8BjH0vt3iifINtgJv59rM8/Ay+vemOjN6mSIfM1WwC4DxBPQpEUbJoBAz1gnMlhzTnvcU6J+Yn875H/SmnrZyCqpAEMtzACVBxlK/sXI/TEu/Aks28lMHkFieIGIpW6iYDitrT8rEbrA5wPOjRHLvrWrxodazWa9n37TihESvzKSXhNeGkUlxoRaHIHe+4fotG8yDv+KE3cFp+EoZUSp9juVj1PlcW15Jj+DmbX5RuW6TbpkqrJlKaX9rz5oaYXQ95dgu/16WuSWw+r7/T79vD6KvKrGFiOPEkyThs8OHr24S8ybsf9lrutnY6BZCn1I3jC3DN/lGi/Q1wiKZLeIUo9d+n7uTqGz/b9BfXfc9ZaNxR2wA38+2XMfrd49uPvv80ZGiZR5YgBcElw2OYcyTLuCYKF1rkSLmfcx73W9znhNMhQp5FcAGLBS7L+6vmB9bw+vIImRPcnEnLKMktaSR2WZ9zb+SjHUrctI1/ydCml/9IvfEvYvgzK7ZYLTN2xw9RfKrkRUrnaERPio5/j4mLNtr838e/hPsevB+YJ2WABpJEUhaQ5EY2YxXLb8U2wQke2a47yPB32f63bX6PiKCkAdx/OO4f7Cab+wSYxbd3dKt1A69R0e+TXtopfPsJvmWUby9oX72s+lkafu0Mvv20z57VLzuPhxRzKIkQHFZGJCoubxCl4vSrzylSB7NIUzMiwsfiOpcUnv/1sN/lh056cIXVu66Xd+B75uZ7oQ94gZwE5rkqHmGVBjrC9++tnXRT4upnrj+0xTDUykp82KwSxDIbu/e9XhA/cO6EycfmrBlNaShFr6Azu8kb/vZmXybEqQlLZHZIM8q3X9bwFw3ys6K6x9F7xOeYCDFGBAipQ0YDJyVWFnwx38Ml7UP80/1fyY/f7sF9Dz9vOGIGRdHG9Sa/1N43HXYiYjuONsJuGJaK3oS5OcvF99McyKLsy6lh5lFa945LrHhHEkcpQq6GJMGJ0HANJEXaeaSpDT4fA5++9jJef79X8p/u8iRisqqpbIbxD+palVayUWzScn6ue2BmldqbGUkr3k8dLWLWybfQBhgKcW70ymi7b/g7YWDOzXEfJKTt4B7M+jynhrgMQ5CUyKOgPqIOSmIlLFsmvlAWfKq9zsvv+3yec68fp50geJl9aTJ8+Vta0C622O9cd61m1DmkYjQplfGnJYcHunkNvx9fHafx/Y2Vbz8NZpwUNh2L79pMgiQgKTkOzZQii0SJpNhhxYSv0eLf2tfzy3f4cV5292eQgKZXgs3nYFwvphNhSzu0U0GzywSduX+6RbCUUp/d2Ut2510fM3NhLm9vx6fRHx/Q5c6ub5InnsW3341NO+0RvceSIeaQDDZdQmNilQYNAt8oHeetX8lT7vAEXvvAF5MsVqriWaKgIKe5wHdXNtMWJQWZzJfLVGpxPeQnTUV66mDTiSGQdZlJx6toyPFzCs2iHtvcxl77HZfgtSzyWbM2QEcM7x25do0tGqsx4KXJv5M493CHH7/NA3nzQ15FLISQO4pMgYx8CSr4ZtEhWSSYn2+Nuno/aRAa7a6Xl/nHiLtljmq7AXptj4+awuDozsnXpUv21Zpsl/eXrQIv0jM7PQRSsqT4qGhX4WFVG1xbGu/bvJoH3OoM/uzR/4MWGT4XSI5MtZotN4fouG3HMihQpqKSPrdBOvnkCF11W/Bq1KtPOwlGEaOgqk5FURLmEqrVVI1pH35s9O3TjFTFLRjG6EwDnBSCLMq393OEjcnVeH1bzNh3JTmX+vf16tiUjMwiPhO8wuWu5H1bkTvtOYN3P/xNNDTDfJXMVqJVGQGBfI7rV4c5SNoh0KBtHdpEmt2gRMT6zezbPJ9uU4sXOmXR2z9j65Fqm5S2p+/1+oLVHYdcoG3PtzO+/azXHvd8+6Mc4/eVk0c6qXp/LQVyt8lGaJCVkczt4aNbBfsbGe9+3BtoZBne+7m87jQMdMi4R6gNo3+uKr5Kisv0g2vtyX0T7gc4fvn2u/F9dx3/VxuCJUMy1puOAygbusLbNzeIkvHOR72B2/pT6YXqMmeyOeuRUlm5PcdE6kavGyzGOBMCrm9VTd3r2kaawk3H4BczepsxDHwWGW2ZJPN4hJFlAOgbNNzSOfa2PVsUfFIdV4hy7qP/kPsduAtlKglkY99pUaO37rwurHcCpFq4czu0SdsFL1lsbQfpEJD6PdXh+83wb4jrO54Mf+hadDvkGTVkcw688fl24OL2lbz1YX/GQ04+iw4dcpdhPXWGGqNfZCMMXc+EdaoGXCipK3KlogyW2iY2/9f+uwMEc4Iix24DHA/U42GvrLse7uwm3/5orX//+l1lQkPhhXRRmW4z7sdim4uKw7zpAa/gyac/nK3CyPMGaMQNlpBsxKMvcxKJQHcoHjXr2PP+iTRV+HgOwHPoEW7qRj9smDcevv1OcfxRo58mg9I3WoGIYWpcphkXH17nt+71PH7hrj9OwtEKkc2OYyUPM5Pr+fLgrmyigPcZiGA2TIbrzfZKmqbSP8byhmkh7gDM7I5v498Z377OAxjz2+kNyrffhfWfKl47ODTOjArBr2ZxRU2ce+i7PPsez+Al93smKbm+Hv9KoyTJcPI8uJGWPQEqaZNt8drBmD2l1Be56iNGI2jOoh7RTLZRoDro6egZvE49kub3+LP59sPQ3mJ8+/6VyOIhznx8+/k9+lgbYF27YNcQ5+kXMAH1Sig8pQguF9jcInMrbOSOf77ucp58xn/lf9z/5yAqKTgaycA5jFB9hmyHUssdcYoaeGlgAt4FMhMKFUprI9JANVJYRCWiPlUwKR4vUoP1O4ZxqcQkdTwZGLQRzMzthFm4uLfbqeHPibgcBb79scTsp22IUex+7usf+JmPDm+Ka2R02h2aecahEHnbdyMPvt3ZvPkhz8KnFuui3f7ansdQVBx+x196uJCZ+1B9Tn+qY6pOpV7FV6yfs6SpNtU7Obal5Ea7w3YlB7ip8+0H3+NocO6XfU8ZUU2WJT9H84xOp4PbbNNazTlStnnnuuc2J57G/3roqzmQbgHBkQk0BpTrxMBVUms7z3P6g4BhNTTJCMTuKVaYklJENfa7vKqDp9sJ5t3coafVdYd1B3mEuosyOxYecHf5RLvOtz8OktjR95tHsGre941FBx88KQctOpzbDuyRPbzrUa/itHBLjkikIRUZjmRYVsGUYh7ZjcxRqiRYunnUamiRS0aHEktKWVY05zo1vjoUaGygx8hrKpBChpLgsRzgWB/9u3Ejl722up7eZUloxwTCtPmuf95HjqO0khMs412F57A5znn4K7hL8/ZshcQeAiikFElZFQQFqbpYSuiWwXbv+62FFo2syeG0hUo1InUbnnVD31Fs/sLX9o2dQIcWGY4lhitmu/049gWr45VvvxMIs+765/u8AfTEZexNxgdLzzc2N/n7R7ySs0+9PwbkKHQKyAI+dL1uSuAyolTyh7uKH5pjxeW0Qov1jStJXvszARDXj/37tjnPWw5b9Vj8r9120qEkuKefXn2Q3MCGf9Pm20+CLRnBMnbn/cfXvyEln02rfGX9Kv7kIS/jv5z+WEjQkYqRmRoBhyLR8M6By8Ag9mgQbpfsQqq4PiOj0WhxzeGCaIKIW8jRDYU4M8CAwUMgHD3Pv3Ojn3Q9vY06zSCnhjgDzRPHRXhTg+x4ZEchzoSwu+84vhkTn2pfxm/e9Rd57u2fBDFRhIym5tAdgZSkki8vu/27waARMowS2YUgyKqO4/7VhRAodNj59j15f4HGZRUHjX+7Z33CEI1+r8HABjCVbfGWIWM8mnx7Nx0GnFCp7YcEO+Tbi02HH6dBpYvAmBN/TyejOCJCEsfyIaOSR6XdDJgmmppI6jDXokHBFep4/+FD/MjpT+aVZ/93KDM2MlhVUGc4S4iEboI4EO+73vXuQgbQbXwxHKKKieO0xgEuZAOzEzAKwCFOhkad9vSo4nhGPRQXuIl0iW141FSmHWQ759tPe9ZNLJyl77Is7bh2IWRyaDPP58wa0znpfeavXOpSht/7vZR7fFspcbSlWXVbuSNcr/t4d/tKHnTCA/mnhz8PpVJuW42pMv5CKOTYaSbLwIl+oLkHUen2BLtaCLOu8j7qPIZUo8ciiWF2wQ64QDtVO949Ls7RQK4WqdQuEuPvfkmh/j6UmshyT+b24cojiAelwbs3ruQ2q/fgnx79CsT2ERTQEkLFjpSski4/Jpbfi0RFQTynrZ6MqlKI4E1INjjedBuinydFnRbSS5dzVLMBZqX3u8e35zjk20/kzSxjtWp19/vY5VWZEcuMrH0VsbnCmhrv3IIi5PzDQ17OafmtKARwChiFVIKB0osQjrLuU7+4NbBMt1u5JaZCIULLenBnlw4t23mfTQhxFkqYFzsBbubbL5zUHmUUZ+ajbUgzIckT3BHO39zDZcUh/ulRf8YDTrgn7QBNNcwlSh+QLuEhxkjwjqM9OmKQgWrdcthp+UmAY8OgRdfbT6AzLGP8c8mi7GTRb8x8+52GPLNk+Y51iNnwK5TFJpYLF3VW+KIe4U/u+wKefMoDSA4aZhQukZuQp0rBtkyKBQfx2HWLSxexMeC05i3xWZNDmjgo21Vv3bXTvnfiyAjuOFQLW87wK4MZlg2ZV7R0lpzHYLK5W/Iog5/ZQ4RmJbYTE1q1uZLnRd9/J49NK2jmgUuLFh8/1OHX7/jTPPdu/5UYAuIMFSEvKxWQnlpzFpQcJR6rA9WGnc0pqyfTWtnLepkqlunIoi4K1Q8KLNiERV/unJuDbz/vNPJFhjMfy1BnFsIzi2+/K3nEwsfktgNqZCVXt3POOXwlTz79Ubzqfs9FMRrmcMQq9M8aiM8oohFdt1dWDc2OxQrrUC1GgQONvTRaK2xpGnHHw5vAzYPSVbOT+rz/SZvDmfUQ822jnvUBVluwShWyYwkj1erNiIHX7aezyR6y16CxrOGPevhp3nhwGMPg38f49mq4ZNWsWZvP00/eSDrjOX03RTxeI3lyiItEhBZCx0WClZRxD+9fv4q7nHAmb3zkixGyalC4JhJZn9AmQJ5JpedDA1w2j6zPLjh/B6WClLioKI5A5IzWHbguHsYUnHicCSQbaoKpr/K6oacNOmPc2LMayiHqlvWKg4Y6xsSz+maT3sUPPieGFbvszZfdNL0QZ/T6Fz01dvsRpKAMqxR5m1SukMfIdU5paMB8xoeKQ2TN03jHQ1/IyewB6aq2OY/asVf2Gw8ijNTwoBlbQYESxHH6yilsSEa0SvZQBczN6jRzQ+H2pHnOdQoWrs5QRrug6m7sJEMdNaJ5i0qzChzLhjBLG6Rux/bHS1w/dHkuENIGKe2FvETyjDw5MgKf6Gxx+UaTv3/4S7nDgXug6vAK6hmu7N6Aj46DmCA5pWUNylhhr2fvvzuUBarDrY9jBj/w1C5NfPA5KcIftdsweqz06aYLZt5L6duPJEO7EgbvJOZWm/v6jyV0XPv9o+JpIBqJrmBLPfvNc0GZ8dUjBa99yG/yuBPP4ojAilQIT4ajTJHM3fB6aA0rQLIqHJHIisu5Km5w3rXnQb6CmlWEOLGhvKsy6nnEm6eTKRcSxlpW3/7GwrevM/yd8u2PegjhXeVGm5tstRMHA/x7aHL+tdfwwns9lV+8848DyppV87e8C0SM4Cr5kxv8q6RAEcCspGEZ7/vOefzc536Ha6/4DndfW6XP0u8KWKkZs8duuIVtNtT7rvm58TdWvv0sb388Gz9AIkfdBpGMfY3A1YXx7kOHeOIZj+EVD3gWzhxWdpA8kMTj2yBN2e74u4G/T+k9lpTkM371317FG778lzhtcPaJJ3NivIotayxgyItRpwdeP3wW9mYD7FYIcjzx7YeP0d3YYDdMItkvuJWbpJUWsrlFzNb44Pomd95zO/7xgS8hWBNLhuYtfNX6QGqC7yRSLvgbcAP0rj9T42udy3jauc/nE5d+ktutnMJ9fWSr/C6H3Sq5ujG4fTi216U8/uhmGeoH6DNFJ8ijDBrlrBDhuAt1dDhudHMqKSy0wdhRWrNQGBrEEbY6OJp8YP0wnXyVv//BV7K3sVZBpEF76AIplViu+EbWVV64YY0f4B3fPZennPsytuIVPHDlIAebJWUSvB7Ax6Jriw4jDc0Dtj7vX6au8iBdYpsdKgMNX9X/g4k5UwHnELqa6/jRnrLxI3iH+vY7DXdm8e0Nh6bEisCWSyjKijqSCBFBeyxDmZQ46cLefze/vksVM1M7JanRgpgwKWkZbImgmceVkQtSybc7nr9/2As4s3UPTMDZVlUVqISQCT6r3aQ7C8HA9eLygXjdVBHnUClQ8optilbsCnPgIq/80pt56ad+j1vnLc5o7eOAJayMJKk6ccT8wH3x2+tqDFR0Z/cEbxu6DH1zESERSNbVBt2Jt76h9O1lhnKFWKJpVQN36QTvcspUNeI457BYLhHW6DHzkEUGWVGQsoCVEc0DeUq0UbBAo1PyXV3jM+uX8dv3eQ4/ccYTKUmYQu6OPtDpzAZkTRwxRnxwVXIOOMtJZeVTCmljNNnSLZ5/3ot5y0Xv4e4rK5zgA/u7BbDSpIvtSLdPbBLZbXkdqkFJlO5G0CA3kOEfrZ7avi6YJJqSUZAI0ZN3b5KKIt3BDjZS76i/pmMT64+NWVLFMo8CIQRolySXKNYa7LvGOJSv8YHySp546x/mt+7zTEocPiZcMBJh58JVc96/pAnnPT6EPragWsXHkkNhHXJWuLx9LT/6/l/kU5d/hnvuF85IGU7AWdEvenX1prshz+ItujZRLW47xDeGazt9WZS6mH7qB9zQqMgMvn3CKKXyJY3eSCcP3juiRLyOt2Sa6C6yOXfmTHITkhN8gqiRFQcbknBH2nRW9vGO66/jtvvvwN89/MW0yWim3mwmOCb9XN0mFtd1JkkTwXlEwHkh+ZJUGJLnXHjdhTz53OdzyfVX8JDWXm4pHaJTSpQkhjmPM1flZV0RrtmGX5cAS+26jkRA9TDoaNJbm9AeB1DgvHz73AIdZwQ8yZS2RlwQgjdiUvzc8f6xM/qhn6sniBJRkpZ0ModPQi4t3llejTVuwVsf8nusNdYIgHlBiCQa+ARH/wioNPvFddcxKmTVh2pKeAc+d3zmigt43HnP5tpD3+ORrVX2O2UzeXBGFEPxeHxXdbHLI3OC4BZevzE1P6lPjge0cF2YiHsfZRTnaPPtc/N0LPWNyKE0NaAqHCgDW9n2NNpe5ftoev+F1zMpJkJHlCzPiEVBHpp8Ina4ciPnHx75a9z9hDtQWgASkkqKEColt2Oygx3iujVZFUJW5R0xFUhwFCnjg5d9mB/+wDPJOp4f2XsSebyGjQAqa4RS+97XUXGDKnn26V5/kbkAE8WezQ2wQUePi6PEt1+UHzR0LUvw7QsxvClBIYmyxzXxNPkuRqc7xFkWyEl2YvjT1nOUxNUTgyXzdFKHIA5JSiNb4VsGFx5SXnKvn+Mppz2BTlqveD3iwQt5DLTdsTmpTbWK19mGG2Mq0OBYJ/H/vvNennDOL7JfAo9dy5C0zpY02ZPWKOIGaHV/vCloJJFIAiK+ZgrpOK25xxbuj5WtYd+OGn8yI5lszxa2Lq/56KrBLW84O+HbxyAEKgXkDolkwuVlwUfi9XxW2/RpUwtZ/vic4GUNf9ZG2BKtsH6FvBTWTfjw4at40u0eym/f96kUAiHbD9JBAUsNcNCwRDwWPe1u24t6L5gq6o02yoe/8DH+83nP51aqnJU7GuLZkkDHjLYzVtMa5isViiRK8kYK1oXWq1CvT2s22RW71IETZPD9XK8r37qcEbrkpHlgyHk9/OjurCVhd9mXfRYm2k20tNbY89Kz5avmDZPKa+apYj0GE0JMSFFwOM/Zi+OQKzgnJlblVnw5Kv+ePCsW2NIAXglS0A4RJ4JPNmUZq6dMEF6abvi9GyqIOVysZL9L120OcZ4yVPfA2yamHvVNtnLjvCNHuP3KXXjTw16O0iLvSTnRqJrZQ/fWid+Vbsayl4/2KZbdIXZWNc0wUIeJgEUhF+UfL/7fPPkzT+euJO7YatCyBiklnHbwoRrAIRQ4df2mFTGH7zJWrbuBh3B8Gfb4Og8Uag5T6T9FBVHDmeC6p4CJq0zRFoQpd6qbM6qCVsvLMbf9HHlPgK08ckADFhOlQXCeFBzmPBtExClxxbFmW1yTGrz9+nVOa53M+Y/+a55x28dx7qEr+EZscmKmaDSK1GRP2aAInhTm89gLJbUjnqyNEV1VmEvmcOoIpZCbIN4RyMEZOSWfPFRwWIz/+chXcmIuVTfXUX6EXiRlhqlWjeviMfHghJKIROlSXsDlkT+65O0867zf4gx/kJObazSdI2lZJcu2PWDdxE+g18vY6boMTV6n3o9huk8YMq4FDP+G1rc3D+2igxNPQxwdTbRdZMUFmr5BmQpIJVtuLx9bP8zJrdN52w/+OXdaOYHfvd+v8pVDl3LuNV/gQLPBKqDB0bYVVjauY6MpZLYzxezxnw9vgATk4nsBNeYEp4olI3nDx4wV3+GSTc+3fMmf3PelPPjgXSGF7iIe3UBfrEJzXPDVLC9sqJHGSSBl0LFII5X82dfezq99/GWc0lzj9g2jqYKlSFLFucpBaXcNzA0O6Ha1WrR9as6CYbP27aYGEu1KCg6I7To3z0KI7TxJ7Defzxkbz2oyyRTMO3wW2PKGd3AQT4wdys4mueUkt49/vfow+1q34pwfeh13bt0CU8/e1h7+5sG/xUl7DvKBI7E7pNmRx0NsZivdgRA7QXTcVO8P9HVvKA1vDryj7RNREqZKIHGFZpzXuYafPuOneO7dfwo6SscfZbhqBDLs+UxB8OK2n1adYitivOmb7+Q5H3sxp1rgQSFnX+pAUVQJvRPMh4peE7UPN9rAnK5Jie2sEKf37IU5uoQog5tlsIsa5qzkbrceSRXxgZQSPiUyMTalQwzGQddiQ4xzDm2yunZL/uUH/4zT955S9fJqYMsKbrf3NP7xwa9hPUY+G/cS3BGSM1yWUHMLx/e9I7x3U6u4tVJbG8uBMMQSSao+6oBRUtAh4RFWLGOrGXhvscm99t+Nv7jf82lrhEYDj3Z1Q482CpGqKSzdIpf0IodenpZgNXb426+/i186/8Wc2FzhzNUWZWrTibE6OTKPBkeJUqbYzX3qbUoXpDiM3g+V2bZW92+uF4bMI0uyjOEvCl/O/pztJLTQanrgqjk2KNgUY7+scZnC+w5djcv38b7Hv5577zuZgiaOBgRlhZwywtkn3Zt/eNDvcEE8zHmdgPeCj2W3d3B+NKfXqTTkwUym3rCOU0pX9bviDLWEAzLfJErOx9e38KnJWx75alo+o0EgiuKKzjGq9PYiZiO47nrEhFrEnIEv+LtLP8RTP/RrnOKbPNA8KZTEhkezFqnX962KpkopULyrZOmTjqyVLWRTowPy0tzvIf1nvw4wj7rBMjtzluEvtqnG1RICkJnhm4HDPmEu56Ds5+oNeM/mEVZXbs+Hn/gX3GXlVEh+W+lAy4pfk4PGkh8944m86J5P5+J1+FqZsSKuVlx16sk2w4PVvU8SwasjIP2xo62UUZjnq6nDdw873vSgX+M+e+5Mx+cIVfXa8gw5FsWurlhVfzypGUkS6oV1Cv7vpefwtHN/jb171jhTHC2vbHUKLDq0NKJaFd6lSnHCOTeE5syD4owiOZjrhzn9zcDsewSTx365utDH2WJjO+s8pbPljH7458NGLwPPklh5TQNtl5yYci61kndsXc0pe27LBx//p9y1eTucb1L67vsI4BqIlEBB7jPKpLzqns/hZ277WD56+BouiwHcxtwh3TZ6UXfFk/OFYFUTu0SIpihCZp7ro/G59hH+8w88np8+44lEg7w7msgoMQLHYgdYqpbL94iDZrgQaJP48rcu5invfwEnyx7OVo/zkQ1JrGiDLArqK1pbsApMCOZRVcqUiKpTFB6q1VPRWgeUGA97Jt8nxrz9qJ2OFcJ6b5rmkP4ew+1HNH9Sd8dPNvq6gpIiVj2jlogXxAtqCW9Vn0LCSB7MtfAKWZlYbeVc6BPvuu567rH3Ppz/6Ddzx9ZtMW84MzJyAq7/cdVwhxwjkeGIBq9/xIu470kP5H2Hr2JD95BR0CanqUJUI4VIFAPNqHpNXP85vDaGTIhDh84xjRRsYo02Wkb2WE7Ht/lwp+SO++7BX933JSRtEHy3E1YcjoxgcCzobtF3QEFNKpxfKvLa56+9hEed/xRuLYk7NhIr6ihLh1pGCkZJJJMqHCydUTijkIrg4EXwE6gJmCOZJ5mv6h8j8jlpRBxLZZqDmhYEeQTfbY4x5/qzmEY8+zL69sN6PzpnWFP/aIQMKRWfILhAKZVcdqaeLILRxhormMK3ywbnXHsNj7zVg3jvk9/IwbXWGJIxFoIAkoQYlICy1xq86xEvYd/q7fnQ1mHafpVW6HB9ymhZiaYGsQSCsrWkntHQ+JEQcBZIZZOGD2xph0+1V1DX4S0P+g2899X09NHT5BhV6zNtEV2qmmrKLRyB71xzFU9+18+wFhvcqrnGqjgUJbmKbtIraA5OdZ8r1GGx+cyTi5CzgfUqaEo9JEpdb0jeXJMi1XYFEp1nM7hkFB7asSSYR7yn4xKWV1W8rIBGKrlA1jjn+qv4+dv+JG992Gu5hc+x1Bpi/dU9fFcINlmqJMJj4OTW6bz10S/nSNzHJ9rrWNmiEToYK5gZuQ9IJ1JkthS6NcT5KTqULkdDjqpytba4RDu8+t7P4YG3uDfe+XoVBDk2lFwTCOoRK/BZxuXrV/OoDz6V9c4Wd86VgxbIolJq2c9hdIHIzMyq2L5mDedZ2+2fU2v4IjIXGllbCe5XZ0dCnJ1Uf8eDgOkygFveyKKxsrLCppRY0SZ3sFW0aSZweZMPbxifXL+cF575y/z5Q3+HteCRlM1FBRBxlJpoiKetiSKDLYGz99+bv3jYC/nGEePCQslc4oglGimSkUheaBaLcY3rbqTPciJG0jabLuejmxs8+dYP4nl3eRqRRB2bV7q5wLEgu/XGqKnkHNY2T/zAz3P5oYs5a+0ETvCGxIIYC9RVEi2WKu8v3lXkvDk9/6D3Xwwqn74QM9+n+/lh+8tuV8iW8RazPfyCpXgF8kCn3aYphmRCGZWmz9lo5Lzr8BYbMfGGs3+HZ93xx1BTosuARLA5DTSWkDdpugaRajyoOuNnbvc4vnmf/+DlX3odWdjPXVqJ9hZYKpAsEDqBGIrpazJ0po8PvGurkfuEL43zpM0pzRN57dm/QfQQ9IYXropqOCckEv/5/b/OV676LGetnsJeDlNsObw3ylD1AwSkEtXt2kJEq/7fKUY5FMfbFBx27uLjiMXNWal3/TGpEz96N7z9Ekewpv5FJnF0EgRaXO5W+Lurr6CZncr7Hv96nnX7H4FScQIBJSCsy+YcHwB51gVHOxWxLEqJi0ahjpfe92f5L6f/KB9bP8RlnQoNicFh6ijd7Bh3lgaqjwqFckkIXLXR5g0PfSG3bd2SSDFTs+io92qY4b0gsskzz3sR7/n2u7nP2m1ZcxtsRmFFHClYVeU1QWOqiozeVRSEAXXnSaHMbATnaHzfcTav60t9q43Nqt2Ngtiyj1UC0ZSNVoaaY8Xt4TOa+NCVGzzm4KP57BP+iIeccDa4QJErURzEitC01uX7T73JvnsMJoMGFBiBjJRl5AKinr++/69z/xPvw/nXFaybw7smKQtgnW6LzfYzmQw9ZxX78sxzjcv49KF1XnT3p/H4Ux9NQUkjhe7xd8MZf+UEIy/63J/xN5f8PXdv7ueAbOBTi1VVNjOHqFTiW0kr7++reoZiVW2jm26OIjm95yzjn5kDjDzVbOhZZ/i1leBBTLRn/JMw/F3bDHMkFBsuEUw4ccORXMbbtr7Hl45s8cv3/Ene9cRXc3LrdKK0SS6SV02BpNBlE84heh0xOq66liglDaWajIIjpTZCoNFa5X899vfY1zjAx9pbEDPCkQ5Z7uZGf/rrNMr3j8Yn9DrufsLdeOm9f7Eb0maVitUU9zgrud8N4wf4y0vewWs+/T+5Q2Mfd/WOVCRSiKhWaxcSBITMe5wLJHEUmlBVwoQq+Nzl52XzqyHi2uzeDTNzrg+Bum3Ois5BTejh9WLL6dtn2uqS86rWOBFPciDBiKIEVXKEr0nk/1x/GC+n838e9fv8/n2eR257EYGGa9Fjv3fpVt1i1xwwH0IDwLnu5HOBUIVCPjQrOQ8tOCM7lb97/B9wpTb4dHk9odEkFdWsWq8lGYkYPCRHFhPRadeQE4UmYmgQig4uSYVzk9EoCy5wBUc6e3nLQ15ClrdIqSDXniDIMWpMMohaVuJZQKEJkcR7r/wEzzj/N7hjI3AnW6GTFILi1JAsx5tSiqCpinYSVQTRUE+wQCmuPy+imgZaZx/bEx+3n7aI5VcCbhO88iDxcrT4Rfe+CyG6G0rzcssfIXqPZoEtlJIOPgpuo8Fe8ZT5Ch/tKB9YP8zDb/sgzvvRv+THbvkoiqxDcQwqoQLkrglxg0ccvCd/+8AX8Y12wReKTXLXRE1QcWyYkmvlldutKvl2MVawaQpYucFmc5XMx+rGuMjljZxL1gt+9x4/w5m3uDMxKRpyECGmCMdCvdmq4lZwHhS8QYPA1w79Oz/53hdyyzxwIEBwRnJaFUetMnrRaeiLYqaMDqwYG2Bhy5PfhpLpCa+rp1s4RPzQ5wf67RWuWx7imPif3GXE2CazBk3niTEjE0irbb7TWeV9m9eyWub86dm/zS/e5SfAlUSMXFsU0gZpHl37sFQtlrQoi4L/dvsncfHVl/K733gTe6zkDN9kKx5BXBX7WnDo5iZZ7qvQSh3NkBALlGmTIhnNRuT6TeXjtsmjb/koXnDvnwXLCH4g7w3Hri01pkhGThEUw1EUbX7o/c+gLL7L6Wtr7FMDi5RSafYkE7wZRsXpr81LZN7cZVbBanreI7WvmdQQPRyyimzLbu3qlMhFHqmMqMtpu0RDGjjZ5LDBRVt7+PzGd7j/yY/grx78Qu7VvENVabSMPBVAWXnmo31yiVIURpYF8hDYwnjlWc/iO4e+xd9d/h7WVvdx0GVoUDa2Nll1DXyzQTuW+KQQMtp0aOoKaJtSMrIy8k2fIcUBXnPWswiySlsgp7vZokFwFdXnKLMdkkAmOepKDI/vwKPOfRqXbfwHD1jdwz41nCqx18CCq8YVYai3OcxDRoxXFvb4820QV3sWiPiaKfO9gp32Twl3rAx+bHtlkVwgc2uYbnFNmfHPRzp8Rdu84l4v5FOPfT133XtbOrmQFLwohFB5/mMQtnUskmeh+iwNmJVESfzZ436bsw6cyQfa13I4GaFIrDZyColsFh1y5/Hd6i6WQ9pAWCEPcFloceHGEV5+v6dzvwN3IVmiqeDUEelAEELyx6bQFaucX1VplI5f+sLv8KVvf4Q7hDVWCWhylOqI5qr5At24WjF0jhB02Qr5rJ7q4WZ5mTASyY002ky2bleL3R+DjqNWGWia0e4c5jNbTd55eIv7n/ADnPOYN/KSe/0CEjJCCjQMcsDjKqPySpqhHrMbj4a0gAIpSszBCg4l0qLJ3z3ypZzSuB3nWYd130DaiY6HFTIkwpYDp0YDx6bzBDU2tMG5G4d5+EkP41fu9hNoNPA9ryQ0U6qkiQ2ipKP+/ZwD0y1CavCHl/wv3viVN3C3PadyRkqUyRNxFarsXCUUoPTpx5PozEsntFM3znDeUL9BhhEfpSeBMt470EPS+oWwOszwaAtEmRnqAt+MOe/cEr5WRn7r7F/ig098Mw8/4R5dVMJQcSSplKitjDihWv5jUCmVCCoRco8kSBLIaRBShzvuuT1vecRL2Nha4/OxjeYtWslj3rEpidBJZM7T0QIkJ7mCr5VGK+7j9Y/8VSSu4Lwnpi7yqh1Ijer2+eo7Hv0cOCIu492Xn8evf/JlnJrdmlvIBkUK5LZVUa4dQEJU+6Q3YFhWcoiPM2xksyq1vefQbC+RLt/JUS9/WJdUD4R2NRtk2t8HGmKOwaIPfPD57cR716/izJPuziee+Oe8+G4/Q4hVcSrlEa8Z4gxPB5+q8CBJQGMgHIuWKAcdVioOvi+rJFXBXJMtSTzmpLP5m0c8j+8cKfiSrtOyjCNlm5bPWEEotaT0OSux5Brv+Xr7al52n5/lLqunEb3HSDQkEFQrNYvcd+GIRHYMYiAhcNH6t/mJj72YE6TFfaKRp32UoST6WOnldKUKIwVREkmqBDIkN5GEtjuhTh3qMxPTH/b2NZI6gzIrvab4kFyV4TtzmBTdneh3vCHUGjjXJoniImTeKELO17aUz25u0spuwWsf8DyeddefICeDGBEx2g6aFqjo+93Ztd2w2M9I23d7A1Sk6mpUT9blXglCC08B/NfTfpyv3PM/+P2v/BVrexqcRoPSe1JaJ/gVrCyJWeBjG5vc6xY/wK/d/WcgZXjfncEgVAMrujdWekfALj1KgyAgKZG870+G0QQb/gp+6r0voCiv4qFuD16N6+QwKwiUGalH97CwTTvrxtyxO7xiyNgWTGKHT4hBTy81DtlNdagTLbV7jdshT7VxRQVzHswdnViiCnE28Ca0OuDzFb7XMT7Wvprv0eAnb/1YXn3m8zlj30FKYnXTJRA9lVSeueNCiHcWChF9m1ec9Swu7lzGe775NlYbJ7F/vcC3WmxoYg3ls4XSccZrH/DbBO9oS4F0xayOLsoAwYEUbcgC0tM9Ywv1DX7hva/hG9dcwj33rGDtgvVmoBEdgtDxVEoV0wx6QiJs1lP5n7Z4bqJ3Hv/N6dcxy/i3T5P6Kwq7ZfBjDlQT3lpsOMcFnS0ussA99t6Hv7jPf+UJt30soQh0qBJc2tUfGkUHvEO9dL3/8fvwQNCc6I2/vf8LefChK/jY4c/yBA7QTgmfCdduNrgkXcEv3+lZPPTgXUnSoYk/NkPqtJrBG3KBGJAAMW4RQuD3Lngj//s/3sddGo5TnKC5IKmSlLYuTOhlEMaczcHuhxcTClRDxizbf58+l25J4x+rDQzXAGSgi8/t1PAnHXWZ20ebIxxuZVxNThk3Oevg7XnSrR5BUI/mCVd2kBJiEzrOSDl0PLh4nLv/br2qNAimrOQHeduj/oDV5i35iFyHWAtXNvl62eb01m15yf1+EUKnwp8JyNEHsVAPilKSYwEEJZMW7/ruJ3npp17LrZtweqNJWWySLOJLRU1IQCayXIw/LZ4cm+YuUwdaVCTDbQgzdXH8+TnG4zlDZfjDBTy3m15/8FGk63G6xontwzyqWXI/a/EXX/9X7vH+p/KZ67+KomShAZkS4iYZHZRAo8yIgRvFIwQFq2TBb7N6Cv/84FdwdQpc1Nnie0n5pjvMy+/9m+z1LTppg0waXdm+Y4DzS0XRUARLJQnH1zcu4+kf/FVaDc99UpMjnS1yaSClUWaeDIeWsWt6Cya0UzH7WTUBV+vtpyfLdTWB7eegajQ9nhvbzNEBGHT3IU4zI7JKFiKlCD4JZ685fmTvGv9x+UXc/73/nTd8/p9IUkLh6EiO4MnwJDmmbIDlv6tUInsqVTycYodHnPwg3nzmy7hEt/hU+woecauH85Q7/CDOCd7vJwn4sqTMj/4RINEw8TSsJLqMLSv5yXNfwOH1y7lv2Is6QQio5ISsQTIlOUdwGYWWC536NsPbzypqLV5Am44GTZRdqZE5dIsatk3AWeuOs02rQD1K4RpX0pKSH9t3kHvQ4XkX/D5P/uBv8M10KQ0XKMkAxTtdrLn0BtsBhjOPSMScI5cGMSlPvdtP8Mv3+G9Ie5XX3usFBKeoVhMSDQchqzrRjnqSUlVu21LgRXnex1/FF674MPc6cFv2tje4zhXskQYbnYISJZSJMiXEOZwLE/n2oxG7LeTtt183Gs70ilazwpvB6m/tZzmZ2g88dxK8uPjr8L81ZZO2VRWllGfkpSB0KLPruK9b4UDIeed//CtfeMeFvPGsX+eJpz2CUhyZU+IxRDuX97BCmUUywMeSGDKCVZSGV9/7udzn1Dtzr/136IrgFkBe1TkAF1pH/fqiKIUIK3GVN33z73nzBW/hTntOZE95iCIEWimyJcoen9Mp18lWVpD1RMdBaLh+U9dUrH6hQdVuKTsbNf7R3xs09kl6VTOuSp2IzdmYXE9t3dbA3NbKKQl4QkWd1UgSI0oDiRlJS+7iEj+8/2TK+B1+5P2/wm989vUgG1UhTB0QIVVE9TiIbBwvh0MGVft9gJD1m6sDDRpZzn879UmQhWoWmeRV7tV7+a7AsJGIdWfndj1xUYJBh6qavBKFj1//FZ71iVdz4mrOHVJVv+huW5yVFFIgPicWEXLDB8OS1PPtR4x/aphjYftZQ1Hoy5OI1j9rYvtRT4+TsZ6V2tOh+4JqxIFhycACCdnuCZ5nB9d9YV2CPK3AIfMcLI3Htk7kPmsNXvPFN/KY9/46X7nuYhoS2SCQvANfEjRRAskSXqFNyff7Q1QIlSIsSWJFZA9ZxfEHMnIubV/OT537S6yUcC/ZSwFkGliJnbHIfVG+/Ux7keEBJ2MUhcH5DzXPpcQZbHIr46T3c64HBxlzaLCMzLNiMS2XQex2r3NsJINygx/wkcccPMBHrjmPR5/zPP7lm+9n1QBKKANF16N5JxC2aJJ9328ArNt3QJVfiCnRRaQoKVO10s/9t1dx2eGLuVcmODylS2Sa2PQ9hMS2UZGRmL/e+IVZjS695yAZLc0jhz9qX8LU57QctP8eykylaIc5HZRDqU94x3dk7RjVGYnxYPRyTTqC+E0arklbAyfHLZ7c2keu1/FTH/4NXvjxV1OmCEHwzpOpYpKIZasSrvw+f3Q85MSqMUWrsDOkNu3c0zR49Rf+mrde8lbOapzKCRYopCCmLUoPzmVzNZ0Pozq7XZsZhjCnEdzmBWTGwp4ZtYptbVCq0UI9ncttFV/pPm3iDpz3qNKRi2w0Heb3cp0kfOZINGhqyUNXhHus5vz+xf+Tx773F/jikW/hIpgFJGaErCSK/77fAErF8TGNFJXPpe1XaVrg3Ks/zYu++Mecku3nQGxTaBNim8w18WUgxM5MRYNZfPs6BGcZj790iDNgjKbVCLNZSg0T5wMMXZSMl5AX/QKjeWqq2a2pndOIHRrmONIuUdvCArQ7wn2zVR6xbw+fOPw5Hv6en+Of//1DFRU6FBWZzOz7fgO0lEqJzUHmFVcqXoUr29fw3z7yInLZ5G7ZClvegXSIBIK2CAhbrpiQ401IOGv49jM99EQ7XL4neCm0si5kGtQFGpyZul1IWKwOUOvpu5ruaULc6J3DyGnGknwFJLTINwHX4Ro63NqMH5MT2Fts8FOf+mV+83O/RxkT0UMpxc0pgAGhieIQlEhGJiVP+9jLuezwRZwpJ7CiBSl6tuR6VsIeYqdgPSsr1YuZTerT+faTbKFetqWmg8vJ9Dh/Rpgz/+CSYbBmlILRl0fvNyjsINTTkQ+eqv4lyqZscXVeoslhpbKZBVrSpBG32NCMjIIHt+DujSa/d8E/8qRzf5VvbX2bPDa+7zdA8tVqexU2UsAHePVFb+G9F/5v7tE4mYNF4ipp0HJtGrJKWR4hWxV8p0RiPsWQJsfgaVK8PQkIssXm/M4jiLuQk5DReQDjdQTnEVS1muDRVYjzxgTIc5iINBoDDg8vGPAuMj7r1czIJNDSAKqYKI5IYSVOcnxQOmYUFrhbDDxubZVzLv8QD3nnM/nAte+vPiMalNu4N6na5+WAuoXVJD7cCCKoNJi12fZqm8btv1ul59R0yvuv+DQv/tjvc6uTDnByO3HEGyvaJlqoZp5JIEZFsxy1ONPbz4rrJwl09anH6sbIdMMh+nAteEw/aCC2H47xfY/XPXMT9VgPDhmaPj+IYLq6KfETMdMZcf34edD7cjY3UiS9ARBFrIZtuOq0uIUaT2idiGtfw2Pf/pv80SV/VS1kqKajKxWVOpLw0ffFfvuzbgcrh8c/2bQ7wZw+EzlqBUrgQreRxoMomhKHOtfx1PNfwqpf406HfLd7p76G4wzcENffTXRwi8Tn43N+h93PpAEpk1Cg2XY1zZamT+kc3Lhu0tEx+4iajgNPmuo9Na7T7TjRBU/MHFZGkEhH2uxPHc5uNLnHnoxf+9BredqnXsRW2oTk8doESrLo0TA8hqeOG3LcF7q6uzTGWDWYOF/delOiJpwKpSjOK0/96G9x7aGvc08XaIQWpYsjVVSbEN4MV2iXmeY5bLyLb5qlimwTQ6OBEaldRsIsFNMNH7tdw54hbT0r4zZZ7AsPToTvFWZcVMwLZYz4JJDnrFOQdTa4mxpnHWzxdxe9jQe95+l8eeNbmBOKlIEr8TVlApEbgduvefgQtm+tGl4cwVVNNULgTy75P7zza2/jtmGVvRIpnK8Ea2tDnEE/6oYMf/EkXKbWBuoMbsjAl4Axp9Wp6pgKU/lq3c9wo16/F+sNTufrDSHWSRRW2R5pP7U4Mfg73al/461r1WsLq16TNVuoZLQLKENG2QpEL9yqcDxqdT9fOPRVHvL2n+bdV3wKT0npsv6UU1Xt6vOM7Ybj3/JFSab9wz4VCScOrBIVE4HPXX8xz//oqzl1z15Oc57DbaV0BS3NZhaV5ufoT+Dc76BSu+xpPGRXA7yzvv2NoEhjzm9w8mSVBzjXk60eHFffG14wTd++7sPmok33Rl7Ouv9ZToyRlBJmEJIQyNAkuE5Ck+dAgP8UmmSuzQ+//af546/+DRkdorpu2OCGYv7dHtZ91PMAcRiGpkTIfGX8qcBljnU9xFM+9ALWJHE7ayJlpNFoEtSxMUFXaDDGn238k410ETWHMWfYS2oXDrWMSRpBc02DGYlqKvqDDGoCuWlpQS2KM+24GUSIBue8TkY9Kh2g3lO2SoLzFdpQRloRGmV3yPSKR5pCubHFZurwYDNOWlvlBee/lGd+6EXAFh2NVUgnw2SoShfy+N8E2h00J2b9YXlGJAZhg8ivfO61XHr5F7lL2M9aUaIWMNqsa6Ih2di9GI3xJ4eEbmY8Pul3J3r5wRBnbkjdBp4y5OknEjAHyHTaD/Pc0FyC0fkEblIyMTg5cvRImVWQ0EE4DDfVO2wPOx5e+CKvEumUSoqWcGi1SvryGEmdTY6U16K5USShSIFHWMZd9x3kTV97Jw98189x5MgRioHGk8GbLjeCQ8C5bHt0lUi1IZyjxPjolz7Nmz/3l5y2djJr0uGICxRNoRWhaSXqwrADsskNi3VoyaLFpu3XbZ8xVV2pa/jLrQCTxbEmGP+UjTyOdEo1HyCpOevKDvZqASp0489pMb1ODG9Epbr03tDt2hE5296+jnPie/m7gMaE7xTEVLBFQsyxkpqUna6udygoisg9dJWz9q7w5Ss/yp3e96N85qp/IxFpE5HeyFEDkUr0qbRufqCglFUtIe7kpu3yI0bEedrQlfWG7x2+jB/78nM4LayynwKXPCsKedvY8gELDWLsTDVeEenO5Z3Nt59Ysa17f/OYVmOkpKsluggitM09k2ruxMCQxnG43FXY/kB4MziFpge/j55Yg1SNoROg77VlXnbnyM4c1ZFRmarzOO1INSBqIqXUT2R7ir5mhqrSdlVokIlDnXDElaxTcoDA/fODcN1VPPycX+Dvvv5OGgQ0VSWyUjqV7qXzfelFk2p4djVYRCndDb8DVAx8wBNpGqhTpIg8++Mvp3Ok5GC2wioB6UoWFmIkUyzpTEBy+9RdFu+3+jBnBxz+8cR5RqdXTYFz1MPPc3K4wQ8Y5YHP6gGexbUeNfrBpLq2v8CMpNo3/J6x1/FNonQrfbHyGRocJZE8Kqdo4IFrGadI4Oc+9Ou8+NN/XPUSkJHFatp6KBMNNVyqWjBL6Emz4Y+DhhunQiFAhE7awInjd7/yZs659EPcqbGHvRYIqXISEcO89GNjL25mcjoJ1JhWu1kOTZqnDuDGw68FRnD1ZpFNRY0mXGMYMsZuyCMwh2EPx16jH9Y7sgdbLQfX1QY4/ZMg1GkFuqzrcSKGRKPlHWrQtqJqTUyB+wbhi63Aay/4Ey7Z+hZvefAfsBIaeLTSDKSahJ7hKLvQay4Or/lR1+efGf04yJNShkAg8JnvfZ4Xf+G1nNg6wO3biY6VmGq1vt4NQH2LQY0290CLKe8zh5UOQ5ggQxXoeWMlN0C1sRkFMZ3gjGGQChCmQ06jbyxzXqcMnSKD/9eBgWaj8GldwjoYrw2ukxgQuvyl7rD3SsreYd7REfCbxpnBc/HKft5x4bt4+Pr3eNtjXsupchIx96BKJlW+kSVHBJIHfxxwJQQF59CkSEr87EdfSnAFd9hcYStE4gDMK1UBp9L9xCjH6Owyl82O8e2n3X+xGYY/aj9SP2hxntOj62QrVG8WTDob1h14vbpJyMzUIlbda51gTsaa680SSvVMFjESanFqiOPcNpVi0hpHrBrL6QR1EH019USsgktDp9LfL9Rz5yTcc+0AF3z7Czz6bT/Ll9oX0jRoSjUIwqTSqgwu4qtPveErwACl0vCOn//M73Ph1V/ivrqXPI9Ec0QHyVdGJWYVXKqG60KGMzeYTQ9jZp7IU5pOau1jJOxV6pLo+v7gXnI73QbdUiiSm+fIql+QXlzvSAjalfNJvafF7nMwiU0DT6tts+zFofOAZNX7VkrLiiOJQxWsUDoOLK9aAbfUOJnIA1ZWuXTzWh76//4L77viI2zKITLvKA3U9RVwjg+YtHR0sg3e9p0P8rff/AfuuPck9pQJUSFpxKfK4FV1hKnp+s55Ft++j7LswmMeufNFClW9RD0xH7NgkRBsMJxyzuYz/HqkYnhjpK6hD6I3vVNgktGPHoe9U2F6kQRylf7NEwNXGpIM857UzGiZ4TbBaUaZFWxKh310eJiLRFfw+H/9Ff7vt95Pkk73PT2IBwLR3fAhUJHBtcUmz/7YK1ndMm7VKXAu0MbR8NBQwfWM10k1od0cJEXUFjLUSSNFd8vwF1UOMZuV1Lq5/PikavRgLhHMqhEsIgHrjpHsZeRVHF7xvoeSWUBJI4uWtk9GGT1ix9WFx+I/m11pHNwWHV9hzdVIIcN8dzOkRKLLp+9OXPcx6yeWYDyUJhesrvPUc36Trz3oel5xz5+jKoo7klUV6F6iVgC50mWkGkGzKjzZaZKcIqWrwq+8wnGQMiNmik+O3Eee+dFX8r2Nb3Hv5gGs7TkSSnCGxYxCtjc/PVUHAURquTiTHM7QTVvGkw4Y4eB7Si93k0lOzA2lKKOTJqdvwtlTBLdDLx3y+k6E7SmEQ/MBtHvTQaUnj92r1ILpCERl462UIuPFjzrG4NzJz5jxD/LEl6HMbmfT92aVr+TGaz76cq4/fDmvf8AL0MzjyWijNLsbOo9CzISAo1EkLK8Onh3bP0LWRSvKMpGFBmVWkEwJNHndJf+Pt190Lnfbv59bFJ71oOQx4RFK78Di4olt3b/b4oZv4roD0geMf0Qgt/ZtB4mPu5xnjWoZSd1n9q7RhuYEq0O07+G1O4A1DUKT2ktcul5ex7kkMqLUUJ0IIx8ui9tu/8iS4V+cfwPJiPF3k2hNnOWNCw6cxBsu/HOu06v4q7Nfi2TQrBRviQ4kM4JJVSPwDukUlA3Iq8kGyye5rlJpC6kkk0YFQSM0pMmFm1/nZZ98Pfv2KAejUW51yFeruciS57Cp9D5+RzCmycTXjFZPB738YNhR3VebfoDYMu7CLW38097TdBsBqEYkddGWZJEkCXpUUdWBL6gjxj5qUG4K5tpNahcMq7cZozIUD87fXzp9SbIE14pwx611fH6Qf/zSu7iqHfk/j/gfNKVFEQJBKw5+DNAxZdU78IHcdFe6yjIB8Q0qhUBFfYYk5dnnv4pDnf/ggY0DhNIomhmkRJkJhRU0QoMo83n6RYx/ejGzex9tZFVr3kZknPFrM/rNJ9nOIqGYjSa5UhcSbTcbVQVHVVdq7CevUUtSSttcoK7x93pAe89J8Gkf2lwA0ZmUVNVVjYex7e3n8IjO+QpNjahsNNe4TYKz9xzkw9/6MI99z1P5TrqMPELpqgQzAI0uF1/NsRvqnhV8maBM4JXCeZoF/OGX/5oPf/c87tXah98qMQl4yyjFSAg+GaOaABOpCWP9tIvo5oxKocxBiR7g289Tqa1PbmcntDONf8rrRdx4S6SZEbt4fS9rr7y81r5hdTjU4KyDAwmmYsg1WpCDlGlzQyHYMt5gcsN29fQBNr1Cp4M5ZV/c4m6twOev/jw/+I6f46LOt2hQshk71bwtpRr4Z1DuBkiUutKEPvYwKC449FV+/auv59Z+jRPaHsvzyhFZJMPRKh2ZyygGyG6T+PY7gzDdRAnM2vUdwesnUY+n1ZxmqcFN/Xvd+09r6x1sidQuAj74xcZ7aEdZg/UdYYMnxKREVqlfqNF/m/emTYRoR/QuR/9eFA7nSxrBoJ0TvWOfRB4Q9nDx+oU8+m3/ne8c+SoruaPtumRBqApmu5C/iasWLjlPIhDZ4KmfeiVsXsetUgMLnrIoiD7hGtWcZC8ZRaxUO0ZvwE749sOHw0joIBMcV43hL10HWPD+9tis9GYu9KUbZ3fBjW74LjCYtikK0y7cHIJH8BgO7Y2iGQx9BiDROhRnngRtmFu+s8LM0CIOTAhxoUNe7Me2VojNkgKl1Vmj2VEe5k7me/ESHvDWX+Fz136dpkJHIHT1SSdNSFx4E4hAdHiFV332TXz+0o9x99btaGqiE0vW8pzSR4rOBpY5jojRTL4rabxt+P3ElXnG2U1bLxmr3k5MZM3NqANMVgQxqR+TOkqMrBuk3X+a1dL1Z8KiI693lV24al5tDx5Sq/X25pREpOIfDhvCsOcfp0aLCqKV7tCs4lv1Pr77HI/v53EmtcYw1IgdiK5NygpEHT4JHdfBguJSwYP0JK60b/BD5/w851/zBVpSVipsmlWCT716xHZWQUFJ1ES3rYCquaA7e1krTV+jIt0hkU5yBOf4/LUX8Uef+ytOau7hllsbtENFxW+rkpUZ4ppgRsMSWz4hUWrpx3U6SMMFqUG+vRvpeXWTc+QRKHH05K53PIM9BsP9xWLdiv1E/VEmRhO999ahu7zIKdJdIXOoqHMgIzGUzK3qMB2zH0VzFnvPRauS8+rbz/toWOQsvSXXHVnn/3v/0/n8FZeAE66WnslXwyjyogsOWyBPDu880YxgEKkMy6KhDsQrQqwKaziST7TVeMbHXsiGS5whTa5L0GwX9UntDmjH04pjNgeE2TN8lUXv5eIQqE05ofpDxYUp2336Wo2NSd1+ravd8dNIaXXGb301MZmYGO0kXqz6eifH99P07SfLcY94qSR47XB2M2ejU3Lm+36cj1/5VfZLoEOJJEcURXOHB5IaiEc6sepSs0rMCgs4L1W3mVUVXxUoUVbM8ztffh2f+e6/cZfmKkakpYHYcks5nskGWdNIPo+Cw0B8v7jh10QC81DeJ9yzecWap7//8IxgtX6fIJXqGAxl/osYPoz2n7qlPfiy3r5+Id1MDZu692/nBc0QsGKL+0ej4Ro85gNP4fPf/Sq5wJb3oIaP3YYaL9U9ygOhWyrOXAWXRqka2lEo8Wy6alLLJ7/7eV7zpT/l1OYpnL4F2knoSqRTzqd1P2lNlGEFBWXxTZRMJhLRZq19Fba4hesUOjZHeLc4WVWoNCjz2G+JHFXQnfszRyDMURXo2QsuIzG9zJXGzb9JZMow5vEphaOTCssIMXRw0dPynrOiQ7eMx5zzFD7+vQtoKUTnq+pwKgldrc4kgHjKVHQTrOrbZOarWWE4WhHaxSa//IVXoEm4Q+G5zgTnMrL2epeUx4yWqGHNnqpK26V2MzznYV4oOtmg/tMsKHLyKTpzkmPN59ucAzLmd4LDtpTGpHumBGgmk8ObUSHcwQrf/F5clobFljtFpuvb1/2OCznWVlweKdURVTmruUKnzPixDzyHf7vuAhq2QUGJdxkhQSLi1SikaqqR7lp6TYAnOZBOxBN56UVv4dP/8VnuxAHKEMkI3bGmTZqiR59vP1KoSizS9uqmSqPMo9Mzq86wvB2MO1CdeAq5adWHQY2VbQWBMRVhFpXPmC+Z0xlQWJ3XmRzT1y/GtOvOzIjqEPWkPKIKsYj8QL7CVcVlPPFdz+NbRy6pwqFklQeTCmrN6I0hqiBmNNEbvJg1jPPXv8Dr/u2NnLh2Mrfc2oAIJW2CKoTVITmXeW/8Tvj2iz3mU5qrgy6reynDs+VkNwx/PHKYXGzraoYKqJlzy37wIsoCi7ZU7gT1mfb6tAiyFEsaIaMkoFHQTGmp0IgbPEhP4ir7Ng95x4u5cP0yWkjVShmlqhN0LaDiiAkEocDwClu2wS9/5M8JHOE2HUeZOxraQTOhk4NvrxPcnoUMf6lJnbL4ejpzfambWddXL0js5oo0JtvfqH71ttFPo+YMnlqjNQjXS3isuyjKgDYQ9eHNNGLTtpevi+97+O54vj/KJRqXZXRTEtoRBEf8WGyvC20sIUmg1IRgiDm8BgpRkjiQxIPjAS5PF/PE9z2bq+K1BO1A8igZEjtEB9Glri59hrOIOPjjL/4Ln/veh7iX7CezNmqOdpbjI/gIGjxGp56eIAMzmWegc71YfpIyWs+QJ04HHZvzO6rqkWoLnv3PV2ayBxZ1bCoOxXeJIzWY0WC0Ih4Tj3Zn3/XVLtSwpH07dXWVsuVL17O9vHVJdqPH1aj3GLxB0xElt+PToQ5nnvXITLlPOsCl113KY897Ou0tT2pATEoMnmBK6Pi+Oeaa8cXNi3nxBa/nYBhms/V6KIZ19oe/41iIITMS2xmh0bAqX11UMTznd5Ii826f6FODr6FGffrhdG+sV89ZDgkqzDid3JCAkMrccNdwTD9u+JOqeL3kbFapexRRmBWPLmL0w2xWWcjwe49NSeyJBWd448vf/BI/9pnnIwrmt4BAsgKyylC3tIMY/NL5v0veuY572NogK2OCUW1//5no3IQYf3pftxua4jN6gs7KL8bjexl67qbhbzN+bdjg2eaXTYNd65Sie/93w95vNm97WrY98wYsBO3NGsbnxkKceSecLOrB6oa25aJsNR23UscdswO85+J38NxPvoxmXKVMJc55NiUipjStxeu+/vd89Nsf5TZyItKJM73pzDrMiLcfpSjMSpJn0ZNnOxO3VI1lHsPvteKOxvjTkT0bG8pSd1k6NBTSDRPb6zqnJnuBRRoq5ujUMWM2cWFOodQ5rstEFv6d4QhBWO1kXEvJCdbmjs2D/NlX38ytWqfzm/f+WTZNaUlASXy9fSmv+Owfsy9f49ZlxrV5pJVmFIpmtBSOwpgs2CAzc32WWJPhC59BYa5VqJaB8MbGHN405zTvd+ud/r1HsDmMe9rGmOuDd2zMw8PbloHqFhb3mtBk0geJrEnSNms+owzGCcUmJzVO5WVf/APusP9UfvLWj6MIkEfPMz/7cq6+9grODCdT+utYYZXBtHywY26b2Sljhq/CAlCoW8rwpxn/IJIzXWKdBYy/fnyVzvgOMzeqkyHW5LYaBGjdjLBFhZHqMd7t5/hiOiYNOJhuyG6oADev8S8Dk058fY9vP/gpXrHM05YSiYIkz522IgfKnKd99MV88cgXyCn52++8n3Mveht3WrsdLVepWViZ6meo3UB8+9nG78bypMnGbzPtR0QGmKmjVXhXG9PXhThzbzpzQ43wA9fndpzGT+Lbjxs/C3r8ZWZYuR3q2oxnEZMUrqOtA47cGlXF1wneC6dHzwZX8V8++jt86vpv8Wuf+W32uAOcrEpHA8kCwXx9lZZthb2d8u13elIPAgSL87l04fkCk3KysZRxpOVykojZzDnI3fUN2/zqbcTFm8PcfDRTGVw4G0B+TMaO9WHoz43drIpJKgN/YyKEJVoHo1r/QE1umRh2RHalVtimqzVEC9PUV8oQE9pazSt4QPsAX9Rv8aB3/ATOGXdNLczaFE2lWUB05RArcTC2F+3VYOojtvpQztXCqlNRkalTXtzCOd7kRHZS6CJTQ9JtIuDoqxSRebRNB+6lq+xK2Na7MhJiaFjU28+K74diM5uOrIzt2DnSr9EktrqZu4NDLyIQNapeZyMY9Kkdx3f2GrfcUNaSY6MJeSl4FTp+MPZ0w+ibLGpwSygoiCxt+HOtzZD282IhWo+qLcvY5ST5FTWEytH3k+DeCTB486zrzrs8ienBRpeTz4ShBNNujIgMGZBO8fSTNd53QR9yCcOf1wgPmiOWxsHUDY/UyBIUfc+o/fh+2ibeyTC7eQCJ7XsmS63fKHiiC4EVdY5tvjBuLMafgPcPv75XrPW9z3ChboHmMQWt7ea3GZ6hfzU7gNkWj3OnIzqLIBn1kuN1UFzwxmkbUHhIzmgkKLsSk4LrGsjozARbetMtavyL9kfMMvzB+zvrtKrCkMnGP9c1zKh2T/q3amNsu9wwlIgsZFSTZN7cZMNf6CiUKRjzDrz9Uu81IZ6V8cEf27+TEKmO82DQ6U61WSlhowkhzrpetzOjtHEUZaen5bR7keY4qaZNbFnI+Of42WQKRBXm9T6vthCmO6TI6tIw3fynyex4UJaqAUzTqRnj4thko0skimrsAIYj+mphkgM0LRTaLLyOOzD+2Vz7SZj98idUbUI7VKqWuULgacrjPdn4QWE3xblgZm78mBj+Aos0O+tCBj+KvszPE5/pFYwdebzaJnKZz+CAijVKReYrneG7sE47QCP1VCPmN/55KuvbYZptE+xk9w1/svGP6IVOeYtJ1zfJWS1+Crix99Yu9uQGyIdDIZDWZN+LGPK8xl/JArqh5GS3jue5r7dXq9AlPssc4upZs33BpZ6XEXCpWvjkK/itOmjHDd0NbCSV6d9ntKS/E759/dntpiMtYmPGP7c+j7l+eLhMKFM5gFTrfGxqr2Nvcw6pQzvFAoh2fUfALPYNVEberZddxxlKzY761sM+RjvkGhaXPxw0gGEjlm0Dn2IB44Y/om+vU4hooiTbHrg2lNLLdqI/aswuGTnd725h6P3GjH7GzjQLDLeWylzGP3/oNRxC6tC1DuujVn0BVuMOXX0cLl22gA2eoPN5/PoQdaCmNI+QslRDHEWkXuVVhhZwZIfNI5I6MfFdLsafltBOtJNpxj+25xbXt0eW81yjRj/5d3aYBO/kJBeGpsxo7TXpXIjPvBDm8ie7mxFK1SXCIyHQMKxR9bb2MuZJmfv8MKZj3imFC4c5C9b8+wJRtj2mc1l9+/nmI48bVv8mTENEaiq1i7JxlzH8NFgEn5nr2aJg2mT6xzIojywJBY8wQsPgxWzXYl23qWERSvNsD7As1FW/eItDooPTE2fewJrYUmV3DG0WR6Yq1buFEazlrkf6YmYLoTjLGP6Yt7el13AZavyg8ffIcKEu3hPZjSNWduTtx0ObndQA3Hy88RsF334nvyMD3n6Sfp6b4kAGbGQRSHmZopXstA5ST+kfZfYHcNpXghOHieC76fC83r2uX2ApozdbnsrIZAxfmQGLLsW335lxjqI4o0jOInz76dc2T3I5R81jQhw97rhkYiK9XDI+AX7tK7vNn//VrWmoO2LTLvNPZr7WZOGmm948gqH3le2Edi4cfNTwbbFTZeENPvyCgT+O83FmGf+sddeJhu+m50e1QMj8ieyooS1m+NOvbz4jk4mvr1vTMHhjetqMk8fkuqU84TDffqAO0Dd82xWvOjcOXjc5cAKONWnK5W4M0zZbPq6f94SYZwLL6FpNpkvrsHefAQ5MNn43NndunpNo7NVWFz0M12fq1mjw34IvG0UpJckloiVWtMVWvk5IjZoLGXhzJ8sbqPZYeYuOSh1shJAaVMLqb2BdpXZCErxbfPvahWdw6qXUng2zWbCTvJkshEjV9XVs/1kmVJplqtcf2khTr1/B/NQFnQUS+IE6TZUvjpeanfixsLYXem6Z4YJpKKQMOTkuCPlWA5WIVz9z8ZdJ4hZlYI4fzzLz6B4zjkkJ2NxJ7S7y7YcKPvPVDyYLv44T+4aSTrGJIWvFt1cWHkU6Bxw8qHE0PTRfHgGqTu1uyDrhFJWR1tieTagqgrAmOWVpIYSG07IsKbWkqasUoYPXsIRm5BSvtaRsxrTQy2bspX5TtYzmCIvElstTjq0uvhcW9vSTvHx9jN9dp36/lZuM5EhvleaIuQc27jJN+XUOarHvPYmSIpNh5BE16D4BThXxntQpCNKMoRVWNlUMUlWiVxeR6HdW4FjC6EdRBxmdM7zwfnRzQ267bfzzxPeL5VEyFoGPG9toAi0z0ZxFIOFF0TGbQ3dot+1rG4CqTggngun4pnQu0M5gxTc3w6nNW18axP9FJtkzOq5NpRimU3HYyRz4Zfn29Q0RU7F7m1xpna/IcuPi2ys7o0qM28ssA5e5rvNoUTJq+V2TcsO65pohzy/90V/iHTihkxun7bvlpe7pZz3norzdaqekaLCJCe5Yh/3QwLmaYcwzdv+ocGrF0Nv+t0X07QdlQybJuyyCMx9N419W335Wkj1N1mXsdti4dMzYwIoZ0oqTPsemNKzPpRAxZc7xoDjvqHjymAy6890w0Q2FRCJCSgmOpLc/5C4PPN8BnH3aQz66VW7+aXDV5MC6sTgzN8QOb/yi+vajhj//5+2Ovv2iwx12Xd9+YL7v+KzfOYx/YCh57YaYeZ/d0PWYTNZ9mqdqO23O8bBK+KwN7ybmGT2nsdXp8KhT7n/uk05/zKYD+OOfeOM/JxeD66I/0tWt2Unlc0gf6Cjo2yeWuT43t2Eea3372c5gm/U4wmeZG9s3lakObciwRbc59xPee9phP3D7Z3r7xXpOptnL+BqNfr9kRlHEf376I/7Tm2GADPfcs37jNX/4wVeF5i38z++RPXTKStzI4bHULZA5B11Fh1lFGB2iDo+/No0hM8OIhe8lfgM2otjETH/cgP2MU2VGJVV1IrYE9bpDQwOYbXqcOjojYRSitTGUJ41k2tMr5+pGkDNhiII93mFmfdfSvYHdddQR7Sa3bYrT1tBNaq5xXbvSocsfIxp26wSj+WYfRJ6Ro4ZSaBOIBnscJFdSiNK5Wv/x2T/49L988p1/9HC1UQe+xP/+0j+c8roP/tHzN8Kh/WutE56hGquig+96jgED8DMkpNPC0Gc99XosMe55D+enxsWzNqjojHh0Mcmk+pNtGhKiMwSh3PRajI5WUkeoxbPj7enXPuSABr1rTVo6WbmuBortd5Qpk6sCYKI7g6hTRFtrdDqREDtsyeY/xlLCcx/xjNf/1oOef/72SVXzIb96zrN/7JP/cf5DN9tbzU4qnonvcmYUPB4nnkg586iaBw3o4+EqC6EYTqejFLNgyFJmbQC/YDA16ayoR7SWHQFap249WL1eJCdbCCUzN8IV0pnvOQmtq767Tg0ldYcoXaKgZUDI/smtnqBnn3Tmp/71P/3568ZDtSlvdM6/vyf/7tWX3norbqxINREUS+pIoCGLO4HDJjZyzTMTV0Bn2qCb8dMqxlk6z1Gb8QEyIIM0/tpZ17+k5r4bOAF13tfO8/k2MEui+nrTv9/MpESm08KNbMYOSNNvT547X7p4m323/s5T7vn4qye97v8fAJMFbjkwtuK8AAAAAElFTkSuQmCC' height='120px' width='120px'><h1 class=' mt-5 text-wrap'>User Activated Successfully</h1><p class='mt-3' style='font-size: 19px;' class='text-nowrap bd-highlight'> Please login and complete your profile</p></div></body></html>";
                    }
                }
                else
                {
                    content = "<!DOCTYPE html><html><head><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'></head><body><div class=' w-100 col-lg-6 mt-5 border border-secondary shadow-lg p-3 mb-5 bg-white rounded' style='text-align:center; margin:auto'> <img class='mt-3' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAYAAABS3GwHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAGGdSURBVHja7P15lGxXceeLf2LvfU5m1nDnq6sBEBKTEEPbmMGIGSQxaAADpodfv+e3+vlng7Hb7jbzPIhBA6PNZMDYYLf7Lfu932+9td6vMUhoQBJtjGkza5aYQcMdqyozz9k74vfHOZmVVZVVmVnDrSu3DisXuvfmOXnOPhGxI74R8Q0xM4Ydh9/0hnPnv3fX2e72nz9sqiSkXIqjqRtI6lo+09XOe+CY8DDnhv/D8r/WpX8S1dW+CWBmblsep5YLEVnxd6MOMTbtnkvvYl4UeX7Gg+60Jzzmm3svu+RLw39z2c0deuXrX77wf/39y7Ij8ztS070o5p7kwKshgEMwBOQBBdi4tMgagu9WCP2gIli9/jJEyCZ9NcMEdOitLRXWia437PobFaG1fkcUJGvSPNjmCN0rUzN09r/8or/d/fmPfH5VBbjnCb9xefc7/+Ox6eTWC4LMYGaoGUbCi8M5h6hQpEj2gPhu8HBj/p2u+efe+9sM0znJrj5KeBXblOus956zFInZDGWCPCTKvAM/OXpl9rgnffuUb/1//niFAtzzrN982+Fvf+NxM7v3vFzmheTioioBooaZ4BCcc0Qe2AG2ThkcEFeatMEXr7K62qiBk3UJzjAXZpTgbvYOshnKmgkccpGmCjPJMW/KVMhZOHLsi81/dfo/H/jHq9/YX7sjb7v8me2vff2pzb17Xu7bXWSpjwrmMPHgHOrkf17hF138rGZzl3xnqVCbOEwCJqE+3y27jtafOCxWwFT6n2FnL/6UDBWYwY+u9ogiK4RfbOnHaWXhex+TSuDTwGet89cr/IP3P+qICNPR4/F0gsNngYgRppsvmPv2XY//5Rvecm5/B/jxI8//hJv7+StNBDBEs1X8z//Jj0GhnijGdCus4koh0FX3hI0ADus5V2ztezWzkRZ+M639ep9j+TmOynDEGClnd3z+zJ98/bfCwl/9f08Ot975cDu1hbkc6yygzYCPD8j7MCu8ER9/qSDopr/wzRD8oS6OLLuebL3Qb8bziwjo4o5hAiqCb+SEn/7w1CN/8oWzQ/n1bz7Zpu3cII55NWbI6KYH/PvNC2xHBbXDX7psg9Cv6nYMeQJ3ggr9CgUAfO3WRVUUIziPeTl3/ivXPze0D96zp6TAnBBK0OBwopjJA/K8YWVYHcEZ5uLIcRSc1YS1/9p18xChrb7/YetnvdhjWWxjVDvCgnWYOXxwV0hlkecCKQohOLrdAu/dA/K7Yes/Cr5ME+wam28tJ0Fshj3piWTth99j9VDJquxarPOGDsGS0lBDLBJaeDqSkfmcTmqTe0+hiYB/QKYncnF0rLOPd0A7juAPva5szMIvjyE2+gzLz5MxzxEREoYMuETEhCODWGaBaKF0jtIZeaGId3h169+PHzjG9qXHtf5b4dsrxmpoto64sxWokGyt8q7v+SuoV7zDIahqXckAZRnpAjtFCEnQgKCpQJ0Hc4iLk++L/1MFsqMt/nIfX46j8Pd83iVBoYx/vVEuzqQCv64n0A0CAiIVaGdGJo5CjeTBicd1umREJIkLD9jm9Qj/eqz98XVzlgv/8SxeNLN1exAbBQRWuEBI3xUyQGOxZC0eUIBNEvztwO1HujhrmODNrmWcqI5o2e60Fc/ff0ZZjAW8QSzKJcoVHhD6jbk467VYG33hq8KYveseJw92vRna47UjiQhJleAcliJWxgcUYLte/FZau+Pdn7FRNGerq+lFBGdLXUBJiiVFZXFXfEABJrD6awe1x1dIJylP2K5anO0Q/OW7tNbBtFlCy4gzBef7XuG/YAVwGxL8tYrR5DgIzigXZ6vLjTdVadWOO6reM1KqCsGhpWJFuSI//8AOMGZtzongimzGtWXALdhKF+d4W/uh91EH231FsFRlguV+oQCrtAQur7O3xRZCG5m91BHCsX7sernA6AT70fJ7FYM05AEGemDWjVutJvibiWJthuC7CR3TlaXPVflD5gWKktju1s8vVbn3ia8Aqzz2unu917b0bhNe/EYdsVFC6rbAxdnMgH4z70k34f4ruFWxRf6AFWt7P3aBNlZvL1tcejxK8Nestz+BUZztDGrXvP8hnkFwoNHQmKq+gDopxkDHW7i/C/2kdmMjmcaNCM1aQe39sd5+OwR/1ftf0X66qJyWEhrjEoP3L2QHWFv43TYL/jiNJpt1ra2+/80sT9is+3eD733FPwugqAJRQVfKRrr/KcCkpchaL5xbFxqyJcmqgYI0GfGkJ3q9/bZYe9F+cmusa6SExYSz2v2RlWwX4f4j8JPV26+nHG2rXnq/LmeVGvkTMqA9oay9rli/4efKklXXMoKmqiBuld+4/7pAQxgaVi6eG9sib7aLs+S6srFrb3W9/WYp/0aUem03Z6V/zzJVXSL85kCUlBLetFYAG/oitkEBNqnefgAOHf7ytk74Ycx6+3WWJ9iErAu6nuJrXX92ezNilbUC2srNsVXuSvrvd8mOJT1FSJVxXOgQ1Ej1+gjSv6RsHwq0eb3G212Mtt319rbB87fDxRkNHQ8GtLLGNXph7HIjqH3lVl3E/2WNpw3bJ/g60RkP1Nv/Cwxq++9Z19xdlwq+rJChXr2/WF3yoKn6/57lX+MIx1fo7z8v/v5cb3//EPy1M/OL57mJnDQxJZZx1V1ukC/ohA6CH6i339jzy/HV0yGCmNb1+ysFf7TnIFKRNgtGGROpjBUZ1nIwYUhccZwV4IF6+61WGjFODP9+maUd73y3ykKvLjcq4KzeNWKqKU8MEQdLihs3PRP8QL39A9Z+7UK08a8hEwt+/7drxjetSx5MFY+gYxrR47AD6Ja9+BPRzdnqevsTwdpvbmzihkqKW1P4F10ZkQreVK1wfzHD4Sra9oEguB8ML88E9yJuVyNLIh61Euklmjax3n5wmsl66u11gv0nIrgs4Bc6xJbHBJrzJdbIqoWi+jtlEUGofsMgeJyusyFkzNKKUSOCetfpK9SApd8Max8deFukDFcqIeqta5EZs9HR0RLvAx0q2WgVxoKU5M6v8d6X+9pLMfteomq57LgJPQ7nBFGtLH9ZIinh61+u1mr0Kq2+A0xcd69jCf7xsjRBlXyuSzmVw3wHm2kgWU6Kig8NklVIQTIDM7xUk28qc2Jb7uKMuv6g4G/FEbR3bSP2LKkTKq9CmCqEDhHLckIJuSXavqQQmMozYlyLP1/6b7v3G0v/WSeMDVeXHO0pQNI6EJ4sMRiWamkc2CE2h9/esXkVmJMoUNEARWkmI0xNowsFbUtkec68JJpWlx07tzg1RRbJU0eNGDp+LsLWBNkBQWVlzGNSBbCl8yQ8hRmGkntPwyuqkSKVffLZ1YR19ecfFwiR0XGEVkGvlRFRRcRhE659GLrgtV+1nps/XtZ+lFC0SgjmKGOkmxI7JNBtCix0aeSCWkWb4ZyAc6SUlgRVtg1Cv1E6wInvVUAHFL2XSFIzAgpBaERjSgKdoo13wnTI6cQS87JC8Ida+7HdpKUKNDqGVixVkKfVrpuzChWaZAKlAzcmD+jKmurBj5jVAcjkwr8VREntmGh7R5k5TJSFZqLZjXR2eEJvuzQqXz9q36LZhPDdpty/2kR8/JtxRIwkSzlzJFWCJCKkPJE5wVJiQUoIDskCR7XEsnwTnl/GdnX6169dUzGQpJXwp4hH6vG9k9+HWynS41p+xSyt++VPMvBsPdfO82b1UstEyzk0Jha8cqe2+WkGKfe4PNSWz/rTL3uD37b8/ntCr9uTRLAB4e8jSr04KDhcYdxJm6PTGU0LmBlFJnSdVRQj/ecXRvdqjOPju6EwanVvipgOyFxCY6pcVeshPbUCMJkBC/0fGUPoNzq4bTPpANfknBcoURpRKWYDoasEF7i77DBfGu0yQrNkdz5FyzuIaSm8uIw6Yyug2+2GL0W0pwG4GgQwgdIZbRK/bAiHOol7XZujWYOzUgs52kaanoZ4ipFGYv2BbXWfaQmfUIXUDUy6LCJYVeufaqFXq+BOsfFRRrfqzSwb97l8zOYkYengaM5x7MFmuB6hSKRWxkwXEsZ37Bg/63Z44l9ewtlveCU/OXgPx2KX6KopIoNbbIUGba6bM8m5bosEf/AeXBUt9iFDqRWgrZG5VHAowpP+66U86o/+Fw7de5gf+4L7ZhwhKm2nq6CEg2/Y1rD0rj9+d9Xr6FJZ67lmZopqQjX2p1maQKrr/ns7+tg7QCLhfBXpixhmqX4BbplVXLqYIpO/JrdOa9+z+Etck+hxVhJbHl/UpbFqla9alnQbLUIZKbznR8VR2hb59c99hH0Xv4xwEaR2l39+34c4/cA+HtxokHXadLynQZNjdGlZr05xoMG6Hg/qgq+23zED2kkt/mYYiFIMb5DVW5n1Kiw14UVQSQh5BQV7CFqCBO5ykGLBr//Fn3HghedzygsNbcP3rvgID997MiHLmE6J6CIdVWYkpxSjoyVTPqOrSk5ASau/eXPIwCxk0+GIn9auTK+e36lhpaJFOSDsvfWVda2ff92jn/DEdMcPL7Q86ycPBGUtJRrlY21KRpXh7s4A5kBqZchcm9J7vARKV71oMo/vJmZ8zq3cwz0i/OqffYwH/cbFOImUMbLnvHOYLpRbvvRFmA7sCrtJZiRXMJWalERUagqNuonCifRZxtZag+2qtx/8fdcrBtPeQGtQqeBdh1CqkKuSvMNZgfkmt0vBgnZ40l//OfsuOg8r2kgQ9pz3DMJC4kdXXsPMbI6WhgVHiODNYaUSskCJVVPZXcQvpqSGWrcezrZ6Y8zieU5cNfElVZCn1a7P+gSrRvmsIH/w6df51579hCfqbXddKHkO1vN4dGIvddODWVn8DLt2lIQVJY3mFL7uo3CpmgaiUXGNjFviL7k7wVM+86ecevFFdMURfMA7cCXsPPdZ+BJ+9KWrKKY9u3QKA4rUxoWwRNm1V3Ne3dAKnG4j9Uyb6dv3LVstItYTfAFcdf8C+NAgOkee2pif5laZ57AWPOFzn+TAxS9CTfABUhkJ0mTX+U+jPXcfP7nmRvyOWXYUBpnHDGLmcDhCCQs+MeWzNWL72j0aIS+DBkZEMFVSETFNKyz+RhQgyAApWuU7HR/B32gxmvceR6DodJFGAzodfJaRCZgXbu3ex2FzPOUzf8KBl7wYH6GZKQkhWIAcUoSHXfImFOO297wfOUU4Pc3Ski5trV6SOOknVxKKF9dHi7ZE8TcrttBUvcleC2A9NNpZ5XKU2sVTYvkOflDeS8d5nvr5T7H3ohdhUcmcgWSEkKEdxTcCv3r5JWRlxvc/9mnC3t0cWFBKUwKO0juC92QizGmiOYTUYBIQoHJ7en9QrExV4susKnbbpPULff92Qjhu0ycWTni9UEJXgDxjpkjcMyPMdEtSgh/5gkMxcs7n/oyTXnYBdEq6zUAjRZwXlIwukPsCi45HXvJmnHPc/J5LkQOJk7VZCYtWEJx3Duc8ilXBlrHt9faj1svMqtIGV+1Ykqr77onltDO6THGTHqHrA4//7Ic46cIXgWlfgcx5khrSqqm6YoPHfvhdaCvjh5f/KW7fSezpGAuZMVVA8kJWCHlwQ3iFbOIq3wrRsQrvj2WNCm3uioflVtmOk9BvtN4+1edPl8Ixl5gtYAcZ3/Qd7iuNZ3zmT9h/0fNZUGg0PQ1LmM+rQjwHLQUkgERSSjzsXW8klZFbPvBBZP+pHNCAU7AyQkxI8IiTmmWYTX8Rkwq8G2UDe8IPSJ1v8BIwJyhGJ0XuyjocTZGnfv5TnHThi+hom6a2IIugeaXoviowQ40jouwsA49+35vRcoEffeAvOXryTs7seopY4K1BcB5Jkdh/aeuz1VWlq1VgQ13m3FvzxOYpQtgqSz/WmM4NnO8QyBypMJoq5M7zT1nBsXsjT/vC+9n5ipeS6NKyKk+IVBaQAmiUdJzRjDn4QEpdvLY4+31vJVP49gf+FL9nJztDk4Y6NJWIVgGJGHhxx8312cjvOOsJi6s8IC9EjALlzoZQapcn/sUn2XPRC8EiTWvSySKhMFIOmUa8gEbBOWGneWIGWTfnsVdcgrgpfnb5p7nv5F3sKRzqKteqK4lsnXOmB583pYQWJZbSlpmbsNwqs4VViONa/LEUwyK5ZZQorTzje/EwB48pT/vrD7D7N19M3lViIyAk0MCCBHKB0CgRzWgKFCGSp4CGFjlAWXLmpa/DAnz/0g/w0N0nscc3EecW64PUcJ4lIN92CP6oaiVNhjnBekUeTkgCbUssaEksuzz+c5/g5Be/EIsG4rEQaVpGzKGhEF2o66WqXbN0iouR1MgJZYPHXPYmTDvc/sHPEPfvYV870XVKyypFWxnIrU16vrQA0Eix2p19HdTrIEK0SeLp33j2r/1acesdF0qzWW8zjopNRdaxZQ0HvkzqlrT6H1XGuLr6yudrCAkl02rrrvoWEkXeIisNnwVuKg5yuCg55zMfZtcrXk4jesgFhwOphn5n0lt+378PX5dChT487RA8e5/7TLLC+MGXribMNNkrOVnRYSFA5lqURDJxfSRIRPBS+dpqVg1nHlgPW4fAuyVAmCIVuV/9UbRGpMQEkQBWfUsNxALiFMXjXCBaIrcS5zJ+YJE5KXjo3/0VDzv3ORVtoK8Ce7FqbazSl2q96puQWgid8zig60vymHPyC55HPNbmx1d9lWwqsMtyikLJnCOqkofK7epYSeYdmhLBHCY1dclA7VU/6eUcttBBtMpQs2wNN7QbLEeBdNiL2IT9Zi3akHG4L0UMGoFyvo1kntILJKHjjCzPybslTZfx3XQ393nPkz79SU56yQVAWZc1Z+tYm0U6joe+548RMf7p/ZcS9+/jEY09NMoO4tq0rEkh5aJi19yTvs4gm9om8Nvrmvbfmaspv6uy7yXWUYwyCU2UrihBIsk3uc3aRLo8+a8+y4GnPxWCozTFiauaYXrJTYtVfLTG0dCMGBQrSx57xZsovHL7hz5O2ruT06TBQqqSVb6jlBLJGxmmSgtP26V+xt3M+nkV5xyoEosCZ8cHZQtm5gYTNyrjZWzXou9b66bHLVMtpMC3hcZ0C19CVxWvhldfYdOtJt/u/ox7u3DO5z7G/pdcyJwZ02Q4HzdgIOqxOlE4411vpBTh+5ddjuz2PLzcQcSxYAt4ly1aq57l73VXmVUJuUmEfgW//erRzyBgoVhl9euGFiyRgBByohNC5wg0dnKbm+dg0eHJf/lp9l3wfDCl1Ij4fPCmKuUaI8tvRuXlZw3KwnjCpW9DrOT2D36KbNce9miO5I5uUoyMkBQrlaM+0nRZ5aLVmfUe+C5maFnld44XuBw2Kqhj+6wTXi8XD5lDF0riVI6fK9CWp6WOhUy4pbiPe/A89bMf5cBLXowYtIJVOSoLGyqoERFcyKGAs971JlDj5ve/H04SHppmmY5dOlolUMRXrlC0RDKtXK5J10jGqZVfVm/fq4Pp4SGiOF38nZKCoBFp7OQH6SAd85zzhT9j38UVzh+d4WrhT2UiD75O+CnjFFMKilrl6mWZoB3jVy97NyYZP7zikxT7dvOgY5GOKDmO0gOZo4VnThIz4ola+QmV5TdSWWIx4utu3uOmAIPW202YC9sqoiifhMIpLvM0u5GDM9CIifkU+aEvuK/T5alf+DSn/sYF0C7ptDKamsAppYR1OEBLjy6QhS4ues665M3ghO9eehm6P/EgbeK0hhdj6pcX9OqVnFtGybEufvuVOPoSB0kV84tXcckI/SpWoemhrTm3cIyO8zz+zz/MgYsuqJJJCAz09A4KfC/JN+p1Ri8EhVTfgm/kpAi/eulbCOq464MfQ/bv5bRjxrFGolV6BE80peH8IptbDSxYiqS6s8tX0MXxU4AlzdZjdEMdj+EOyQuiQjBYsIJWKezSjH8MbQ6X8PTP/gknXVjj/C1P0yLJZVWj9yYoYzOB+QxzEYup2gnKxE0f/CD5vlM4SQOSDCmrunQJvl+2sWrl6AT89ksrblc+UG/nURJOq8I3qbH/5CBG405fMIfy5L/4OAcuuoCCLnnMIU+kUvHegwjOV8oQU8SH8YhCFKGQLnlyJJ9hCi5ALOHxl78do81Pr/hLuifPcGbb0y1LxEHDB1KMxNpQOCAWBamMOCpgwjh+2fUhibBK+0fV22+WoK/24oWqUEsiNJOH4PhmXnDsYOLpX7iUna94GWpdGrga7VG8gRbgGyVscA8ofUmWMvCBrnRpaIuz3vdWMhP++QN/gts5y668RW4OKVOfrrViVpDhSauJ+O1lJEhgMtC87wR1QuGMjig/yj2Flvzan3+KvRe/EEzJtUEnV7KSJYKeYsRnAR9C9atjeAF5CWSe6I2gEAGPIlkG0XHW5e/Aa5M7P/gpdp60i11FoMgFi4mO1woJMiPVTG6iVf2ZiqBWsXYcj8MNCt6wsufNTIT1u5DGaDaxuklFVdFWxi06x73zHZ7215ez6xUvoVEajoCvE1yFBZKAa5Sg2YYXJrMMfES6DnyL5IFUcvqlr+WsN7yaHx+9j2OpQIMjYpU/W/cUBLdYUD6ks2DZZ4jwr1Un3/uYVi5YvWuLCBFhwZSjsaQsj/Erf/4JDrz4hUiqmmXVFTTNYVmo3DWLleHI3DKYcfQWWmYlxICSEUkEU8QcIQlFiDSKKR75gTdy+h//r9xy3z3cPSvk0ZhzialUwa4pJWJR4tQIdVOjWjyuWfYwKPh9ZoBxqvVGWbMRu0T/33Up+0Hv751zuJjjGp7byp/zE0089y8/xb6XXARFgrxCpnvGchHLyDano0TqDbIBzd49+4A3OPvdb8XEc9N7P8KZJ+3j5CzDt+fp5oGWa7Jg3YqbUqlxbKE0RQWChGoOsOmqzgWiJF/FGM5kybr01skhlOJwDpIqXpXgcu6yElyHM/72bzjtGedALEnegzic5X2yKAHEheH7zRjyl5FBqNfd+cU190JOgLxL1m3xuMsvR6zFnR/8IHbSAU7rTNGWLloWlJZoqJKRKGol9uoppeIs2hYUaLPyAOO7OsOVJkVhptnlq/ZLGuU053/iQ8z8xvMrffF+S7qmJskTPPLdr2E6Cv906aW0T97Lw/0eXHuOwi+QuxYxFUv6opxzeKmCv5TSKoHKolK4uLg+vXXpDcp2QIGRR6OjniARzXPusHli6vLEv/gcpyzD+VNKeOdrxo/ROP+G3y8ZsVFiER53+TsI3nPLRz5C3KEc6AZmFpQCJWF0qRJjmQqjOoyPCwy6kcHRYwfOq7Av9F52aOZ8r/NzPIHHfu4K9rz05fQT1J5tO3pKkEfHGe96PV1nfO+DH4BdnkfIFNEcbe3QkoDW7ZaqEAy8LO60MiLp5fttitWXE7Y4CkoE74wUMvLuApLPciuHua+Y5ylf+BwnX/yCFTi/1JnqcXH+Da+TOjKFGBJJHWde+jbKtMAPP/xxfOsAe0Vp1A1YpaOK4+rw19vmtc2ORBtf++gnPCneducF0mz2F6iK8beW338t4TczbuUoB6d28YRPfpB9v3FxlfL3hlgk1XTY26kEyXmcOvad90zoRH749/8N3T3Ffpum1V1AyUBcVcRQsy70Ss6rkgtdEQcsCc7MY85hIqRaUAbLI4ooeOngG01+kO5jTh1P+ctPc+AlF1SzcQHnM6TG+UON+PRw/q32s1NNMuZU6Xojx3Pg+c+m3Sn52Tf/B+2G0iwht7pdsw6PeolY27qXt6QUwm0mdMkmBNC9cw4vzHHGM5/Og156IY0y4J1gJEwjaVscoGVBIJBCQUqJR1/yZh79pjfx4/vu5rbsMAsur4i2aq6dTFxVJixCMh2LhilhVXBNj7bElvQh7MgEtYyb9AhtHP/qcx/hlBe/CK+pjgd8XUQGYRnOf7ySTOpKzGW0NODKArGMx73pP7PzwC46GRS+tvS93gtsBQfscUOBeoIvMDEx1EbyBEtXbBHWO23HAX7yf/8/fP8t70UaStTKKoo0yHTb5Z9mCV5ycAkSPPrdb+KR//k/8aN77+WXU0JqBsxXjSEStWfWMWdr9DMNsCY4WRIneK2K0XpVnV0tuUsic5rxpL/4Mx7ykhdT0Kk0M8vQWNGKYPRx/qTpuCHsTrs4y0CgSAlcTtE+xtdf8Tvcfc99nDIvNFO9D8rik/t69ziuMUDPwtgACGDHQ+BZigQNPvdpHcNmG3z/0o/gmoEz3vpHOJ3CGUQP2TYrgGUlkjKcz0l0CKnJ4973NjITvv+BjyE7ZtgZGhXsGEvUrP9ig1VcNqvYoWUGSfrEXYgSqXh7fpQJpZb82l/8GfsuOr8qD4+BTsNolhVzxXKc33k/Ns6/YU/DVSFbSaTpjfs6c9z0kt/hZ9d/kX8lJzPdXiDWRXxikKTKDXg4rjvA0kyw2kQh+GYRwApLKQnFACl5UJrB7/bc+vYrMFUe8fY341TqhsbGtiqAkFH4SugIFVCqMXHm+9+AiHDzBz6IzOyh6XOcOso6/+EV3IAyrLbgLhnaQ3nVUBKqQscl2iRSNH7lLz7OgYuei8UILmChag6KWWVJTWOFPg3g/GZ2fHD2okHKFR8d2pnjjpf/Fseu+yqPCLvZMX+EiCdilYuG0XGQUBrJHe8g+IlPirfefoFrNQfs/3BWiIkEvm4ml+WxhVk/9hvsHVjudjmtuG12kaOzTe788nXQPcaec8/BdRrEEHEVfFCV0DqhAEKE5PS4BMm9fgI3EGB5HHuf90ysKLnl6uthKmNHCkynRAdDkscyXw/5FMRcVfciPSYHIYriLWLiKwJkqbBynOfW4DhGh4f+3Rc483nPrprcva+48ql8if6wIVkMm/u9BSKbYv2TFTgRCqp1zwCKRPTgSKBVqUYoDnL9b/429115LQ+dmmX3sUiSBkqPzrzqcPAmOBNsKwPgIUFw2DJLryuzypM8meaeUCiawakdIdu9hx9f9heQch71vtcSug1iQyufkapmSCSCF3z02zL+b0me4JLX0cLzT5ddhu7axcPKXTSsQBsKhZD36oac4utYQVOV3Q2ilHjyMhK9IxCJocmddOlql1//wp+zf4P1/BtWfgkQHbnVjrsoKReCOlJyWAZF9yDfuuh3+cU11/H41h7csQU6uccXc7ht3sHX3n/ZOPnrkvPXM3Cik2hK3h+AdqAwpvc2ufPSj3Pb298PDcXXg9WCVoGhj0opie42zr7s7WS+EB76jtfzuNe/nl8cPcpt2TGCNvCa0ZF2pSyuqt6UpFhSTBRzVZFY8DllFsi0g/fT3Ok6HLaCX/nzT7DvwudDnlGarqznN44Lzt+pt5qUVf47VMaoSIkyAz36c771klfx8+uv43GtaXa0S6bUYUVkahm79HYe/rWP/rW+C2S9IQM2eUvkCr/eVuUFGzPI9LhUkdwGoGh69ndBZpvcduVXsW7B3uf9Os5CjxULdRmhbuvbTqBURDDvMXXsO/cZ0In89MtXUewM7Cwb7ALmxWqUSCHFqsfXOcxXtUVljIh2CflObtJDLABP/sInOeXFF5KiomLbi/PX8KqhlOKqXTg5fACOHOF//LtX8+Mrr+GsRouZhS6FRkR89Zxq2Ha9oRXMcEsUwNaMASZxkza6/GWoWuW8GZp5GmXV/DFrjtQK/OQr1xFN2f/MJyMEiOCkmnriVTenJnoDRxvIXERLZe/zno0k486rrkRnHc3S1Z2LA+hbr8c3KSSl5SH5BrfLAh3neNznPsypF1+M1L6z827pbIa6AE/6sdfWPr9TEIs4MRBH3ps4076Pr7/i1Rz+0pd5RD7LbCehZuzEca8rafoGTv1xLXleUwFec9YTnpJuveNF0mz0d4BxFGC1gFbYHIQtUyOJ0lSPc47C6rE5WcaO5PGtwE1fvBJnyv7nnlP5vK6i/a5oULZ3a83KXmeZ4syx9zlPh3bkR9d8FZ1pstcyfKqSPylUuU8fq9Ji5wKFKT8MyhGLPOnzn+DUiy+mnRbINEBQUqm4utCuJ/wxxUVF2OIFEIVU18C5stoO2p1DfPOi3+Wea6/kIdPTNEplWj3JIke9cUAz5rWL+WZ/kPb27wBnPeHJiwpgIxWgb+m3WIGjwB7JOSolSY2meYIJZexSOmM6eqb2TnPzF7+CqrLvOU9BoxLFKjdom5PFyUdc6cELKhFS4KRzn05oF9x81XW08pwA+H6dT1UMpl6Yz4Xbc88CHX71c5/gwMUvQjAyy+hkCoURsqzfypVixHlX0bfU72ajycyRKKeLZOopkuEzQRfu459f/CqO3HA9p0212HnMKE1JVk2dCRJIZuQETBLbtQGsUIDXPfrXnhhvvf2CcRTgeA54yMwzR6QhrgpqnYAqKRgNBScZrQKa0zl3/v3VuCic9LxnEmKgCEXNTryNCmBASLjS0/UecYaLiR3nnYPvFHzvxmuZdoEZl1UKoErmA/NB+Inv0E1tfuWzn+TUl1yIRqsCW58IFtDgK2oci4gD56Vf3tAHPbf4BXkUTa4ixj12jBtf+rvce91VnNLI2XskYhaYsoSjqJKFKqhkVNFCFyOcGArwmrOe8JR42x0v8q1mXaFZF2oN1moNcXO2+jCxukVC8KmCVasqSqv9/JLojVnLkOmMO666Bivm2XnuOeS9PEGqQfo0kCdIULpyyxXES81LVCOyDsGcw+HZ/9xnIbHNTV++kWxmhl1dwbyRfM5tvmTO5nnY3/4NZ577bMS05u0RsLpdUGRVnH+zrJNqrIPbARKwaCRnFc4fA2WARvso//1lv82xr3yJh85MseMoOJnCKEgIRqiICjB6+e9tE/4VCvDQ64b2A2yz+zwWe4JmAV8olsMpHU/YU+UJTHMe8Z7XkHcbxEYFUZdS4dUiEfNWtzpub57gMW97C7nl3PzeS8n2HmDngufndpCjyfHUv/zCttfzRxfIi8oTsFwqRQwQ1BEVfIDYPcjXL/49fnbNNfyrqT3YsQ4x87jiKNtfrDLeEVw9RKFfstsnrd1u8tcl9mhl3NGtEIXSIFriQMdxbHfgzvd9jJAlHvWOt+BUMQnkCilUeYLCG+a3r5CipwRmnoe//XW0tc3t7/swzd07KMuc53/2o0xfeD7I9tbz11U6WG6olXh1iHe0rSQLGfHw3Xz3X/8hd3/1Sh4zNcP0giIpp526NEOgHe8X8o9/3VlPeHJ56+0vcq1mPXVPaksr22rtR9XLW/BIggLFYZRNz74uyI6c2668Drol+5/3NMR8hVg5Q10gs3BC5AmiFPhuxr7znkPUkoNXf4Nf/9vPMHvRBZRxAcRvK84PgnhBrRqdVIrDmSMXgSOH+fq//0N+cuWVnN3ImZnvUiYjUHGxllpU0PSJeCx3gXrMcMd7XOdGB86VXrAYcerQEMg6CTM4pRuIs7PcetlHSLnw2Le+BsiQwnBBiA5CUvDbCxN5gABawuPe8TYe9a9fTnjk6QDkWXNJQ9J6eHs2/n5ANOJxRO/ob0ALh7nx372aw1+6mrPzaXYsFJQm7CLwSzrM1AEvxv3iCCvdjeNzuHXU+y0yJwh5VykwpsxQq4qynAiWeU6NDfKd8L13XUGmyqPe8XosVc3yHlBx295SI7jKLasjzMYjzqb0JY4SiVnF9+PcEt6epKn/31uex4A6U10NEs+CY644yrd+4/c4eO2VnDk1TbOTCGR0LPGLEDkQM46mDuJbkMr7jwIM+t09x0M21ZrYEAGY5NyV4lqKsEcaHKSLTwUNqzhtuu0uKYP9ZY475SS+/d4PU4jw2Le/jhQNDYHctr+jLJZGZhBzJaYuWWiRlRlIAg+O7a3nLySSa6BtJS3nYe4g33nZ73P4azfwsNYUu46WzHmYw+EQWuqYI9Egw1LifhICLBYM9Ef+bFICZbOK6VbbnxrqOaglDfGUuSPmoE4hE5oKTgJ7jwkP37mTOy75MLe+/QOEEMgLR+m33zpViax5lJKmtBCMmFUlzx2xVXl7jlc9f07V0J/5DOaOce3LXsl913yZ08XYcbQkIkwnIddISzKCKskLhSSUgvvLESqGYamIhWvnT2Spi6K4MS31CBRn8dt10d3KrqjFTLNfRhqrDEJBySkZFRdnI1b/ngScOZJAKLuUmWd/mqK71/HdSz9IYW3OetdbyDoZqRnxMdQ+kVJ4SDhaJWhWrotefTIXCMima16duoG8Mv00e0mtYbw9m2SgSiIBR6rfdK5AmYgNIaCgFatFNj/Hjb/5Kuau+gqPmGkxfSSSfANJZZ0f8HTr4kmfrHZu3Ylh3deUwIorLCwX4t76TvIQ6+G3Xy78a9OF65jO0+I9l40MV0TI4CHzntauPfz0fZ9DUs4j3vM68m5G2agIZaOr8gRJIgTDldn9BcZev49vAVLtAwfAKalR1fNbdGhe4/wvfiU/vfZqHj+9A5nrUGYBV7bhREV5xpTAXjGeWw7PrdvFEV36WVNIHWYy8FlNgZTxmuNWkhBqUdJwOZlW5cYnzytTezLueO+fcus73wsNxaWIGWTKQD+B0f0XLvzVDlAtbQoVlLyknj8HDh3kWxe/ml/ccCWPmc6YXSiZTg4tEzO+cb973h4l53KJmkiNN5ff3q2iKDqGsI/j3AmpLIkeggjdhuPhHfj5vhluee/HcGXgrEteNzDu1EghrwJT+ZevAEkgy6jGooojJiGYkQehOHqYb/y//pAfX30Vj2412HW0S7SA0MShtFOHE32LHEY9M5jd0kqe3bpbIjfKbz9a+Icr0dj36utp8skoco8vlGjGqaUnTc8szRNIDl3FZ/6EyRNsuQukgFVTIJ1UQwBVBGvfyzf+9R9w6Mtf5uzmNDvmImo5O8j5uUSmQkBLu18Kf//fZNBOiuhy16fq312FsWxM/H4Uv/1oqz+Z8Fe/t3idvKjGgTbwJISibjonZDy4dGQ7hO+98/LFPIHPsDpPYNvKO3d8Dq9goRrkoWWELNDpHOI7F72Sg9ddw5lTDfKukpGxQMkvvbI/BY6VbYJvVFUDJ7DQ64DU6OoYqC5mggcttaxX6Mfnt9+oqyM2qGS64te6DvbQ5D7r4KLSsoCI0Gl30ODYV2TYyfv59ns+tDJPgONf+pF8xGugo4lm5ijb9/DdF7+aYzfewEOnp9hxNNKWxEItEzlGh8gUOTEZ6QQX/LFh0NWsqYwUfFsi6Cv47Ye6OTrk/Mmt/dIdZvg9esu4ly4t8XR9NUjOxYRlQmaKJ2fvEYUdVZ4gT45Hvev1UDpiVhL+hcNAXiAlqsk2R4/yjZe+inu/ejWntVrMHkkUEphSRai6zLopoi5QqpGOK4P/xoV/2C7Q83pCxQRdoTlejKhWNVmMaIy3tbKpfcHXVZQGVGRgXNCyaw0JkFf2HK+t604SuTmSQUhSjRJ14FMFgKXUhkbGyWWD7g74zhUfpuu7nPX2N5K3G8RWSajLJ0iJGDyRihLRsnjiFnv1UZ6SDE/E0QFagO8mUkNwKJICRVCa3UN87RW/w5Grr+Zhs7PMHok4ycDKOptbEXgK1dzmukZ0y+/fDTOZMqal78mOuRXnD46lUCCsLqwypquzPr/ejVAgMd1QB9pyhVn+5zLzhK5izcBDup58egc/efdn8EXW7yfo5wm8J+vxDgVD7gd5goCH6AgKM1n1Pqwh+OQok5BlIJ3D3HjR7/Kzq6/jcdN70PmKtyecALw9OkZgO0r4x9k53HIBc8bQQW4V5i9rCL8yGrsfIH9d89F1U2gX1yrHaJjgEayIWEqc2oZde5v88NJP8IN3VHkCiWWVJ7DFPEESuV/kCaJVvD2WV6NPsSpOKk2xXNAjP+c7F7+au6+7jrOnp5lpR3ZEBycQb8+wkVrjZoaWi2mfel968i7gJIUlEyJtLWsvk+jrsu+78XReN4drdKwgUMCJUnhPA0+RGWfMwS/2THHLpZ+gkRo86t2v7VcH9vIE3kDl+DcMTXqIVBFeiWJOCBrwEbKg2KHDXP9bf8QvrrmaxzRazMx1KNVokNMAilJhm3uqx3Z3Vpzo+uK6UglkefNojxxXJ3BzdA0vbZjD4obf5KCPrxtjhFzPbqGawDm8Kd1MaJTV3x1YEIrpWW5+/4fQhuPRb/pPYBmuFPBQCmTJqijyRA5yE6hEcoFUOUTVsOr2Qf7p3/wBB6+5irOzWWYXEsmEnTjuocu0b5GSwTbgPOsW+pFukqy4dn8SUU9Il7oKMoY/vwEYUwatvR43oR88mqlim3BRMROKEMhwaHA8uGtkszN8/11X4GPkUW9/HZBj0mugOPFhUgGSq/mGCoXc0eke4tsXv5J7r/sKj8yrev6mOY6Z8Ytg7I85R1IHF6bxMW2L4G/WubZKWc/yXsdQh0c4qwaVmcqSUUnri91Xd3OGDcc73sIPcMwZJ0mDQ76LGIQyggqxLKHp2VdkyP49fPs9H6ILPO4dryeWioZAQ/12ewgjj46PNDVQpESeQVq4lx+8+PdYuP4GTp+eYechZUESx8QQE5rmmRejZRkpFceloWsjGP5KwZeRyjQo/NUkGsPpklm1NubtrJxzu0T4zfXH+fTH+mjqk+RWLrStGiQPQ4g22l+w/MhDg1+UbfKyGjidvCCZQCvgi0iQwO5DysN37OSu936Um95yGSEEGsmT/Inf7tGEanp15tH5eW58ye9xzzVXcnLw7D5U0hFHZkbQkpYTsmSYhKoTzTontPCv5uKMu5PYAKlAWAof9XRkrSBvjO1fFNO0jr2rWgaVzbX2w3YPtYKmcyRXz6S1ah5XUMB7ulIQvOe0dpP5nYFvX/FRnHM86m2vrdkmFodUn5gwUCDminYXuOEV/xtHr/kSp02fytSRDuIiuWrl+tKgi1SDwDVWJLdsHAXaEI6/Cpy5JNc6Yt2V4f0AVS6gMvsiE2dzxs/QbtTFGeTQ2UzB769v1H5zyWBQ1B9ckSK5Qsct8LBuYHbnfr77/o9QpoLHXvL27SN3HTsKhnB0jm9c/O84cv03OKN1KjvnDxOAjjkSi0NETLWu2BJE6uTRBtd+3Tj+MjRnI/HBKCVLy8cBr27NVmL3YrrkU7k4afHP6xDQQRdnq/MAvSdSWMEkrRg+NGiHAvGejhOOtQ/SaDWY/tWz++XSJ6z1B4qyIM4Gdj/pV0k+kkIJETouYVZVf/YG8fkqz1sVAtY74Wa5ORvB8W3F+PpqbrKtY937/QD1p2fAQvUSdWJrv1nWeatcnJE7bD2Ttif8VWwyEA/FiDNPCC1uzdt0ypyn/tdPsOeFL0TuBzAoeU5AeeTl78byFt+54jJO2znDGXNNDIdaQTKrRlkNlKUYVbmD32CeY72Q5mLCan2/r2vsODqEMjWM7ddvdhC0TYK/qN+y6G4Z1ZzagUtk3ui6aW5hnmMFPPO/fpap85+DxPuB8Pdc0DKQssSj3vMmkhduu+y9uB37OPVQbx5ZImnNMSrVNEpByPokQMdX6McTcFdLrE70W724cjn4HoY1pq/ODK3LQKftPTZKrtXDsZxV9fG9hYquwtBv9gc5kjV53mf/hMaLnk2jFMiMDonmCV4MlyloCEAkll0e89bXkoJy8zsvx+2c5cC8hyS1y1pnt6XaCb2OnwZbq/lkcqGXsa+pI67dE3hnS0GV5d8Lg0GHWS8FnvrWsCf4so3CulnXGlpcp9YPhs0JXW8Uzvhxrmg38PS/+Si7n38BdAtiwwgayO4H7QJKxFnAxYBlJRSBR7/1zXhv3P7Oj+LynJnkmCo9ThPRrE8bOI713ywYs7qOTPRkgx6LbVAwAzYsrelG1ttvRFgHZwJvh/D3fP7euotVwl+KUYrRFqUTlSf9lz/j5PNeAKURG3lFJaLge61jJ/DhXCClRMw8DWtVZFuaeMyb34SbV+748Ecw16QpvkdTUOHjImiajBt2GNQ5aijiqoK7ai/JMphiAvFZaxcIUhehiSWcGdGo6LdHjUhaBaYcR0DHEf7BgHQ9KqhecMmW+PZLymBFgQw1SM7ISJjz3OkBujz87/6ak59xDlhJyjwBIViAik3kRJf/Cgn19RQEqW7YmcMMznrPW+kGxx2XfgybbnDKUaWFcUQSWRKcqwdu9MpVnPSayHFWsftHZOi7GbbWa0QpQ/5a1wWlribgy31/6V+n1/y6Tss6rN5+M92cSUhRhq5j1L4SmVRzm02qKlATKJMjU0VF8ZR0fcZdGZS0+ZUvfJZH9/j56yFwqgN3YvcX4r/VDc9j3/V6Hv+aP+An8/fxiz1CKdOEBOKNpFVuoBcVmirUf1bvKLzb0LsZx3pvVPhtzN9edyS37SjOiPOd1sk0WbmgCoSQ07FI0wq60uKHWcEhLXni5/+M/Re+AGwpP3/9Ixwvfv6tVAIzI3QdZ77z9cRg3HTZFaTZGR5ytEmRCjou0rQANeYuKeIwFEHxlGJr9gOt5tev119fDdIced4Y/xbur4Lvlp2/sqtY+unu1J9dvBh7FKnAU1KEae4M8xxNxlP/y6fZ+6IXgELEcLXwpzKRB1/HzIrczykjRAQaHqfwiHe+mRgjP7zsg7B7F6cdbrBXjWO9ZJFUM8gyrWBSS1qjwDK2cK8HKVq3soy4pixTnnB/E/xJzrcBmnpfo7hSu0AzQVigyV1Zh2MGT/yrj3PSi15An+dogIZ8O/j5t/poAy1XzVE7+z1vw0S47YoPIntm2X+f4c1RWsIUcI7SWeWeG9UYDNkiAZbJfPxRrs7ymGQwGbaCG3SrhX4zrusmEP7eb7l6IVz94tQJ7aT8tOU4rJGn/vWn2f/C59OlS6PMIKvm8Pp6KkuPkz+miA+BfwlHK9UW0SfEMh59yVtxwG2Xfxi3czcHjpUErSbBm0GSaoSrq9dxGFRqG9gaV8CqoxRsHdcdFP7UK4VYTTBPBNx+GCI0yZWWb7PJQeGh9MYPfUZJmyf/1afZ/cLzETEaZTWHN+sqPl/0/Xv8/D6E48bPv9VH6UuyIoNcSRQEy3nMu99GXgrf+/An8Lln2hyN6HApoWr1zGOQZa2r4wq+jimkm6VE43zPDRPWzazz2ci1xGyJ8E96TbfsoZNA4YwOitk8/+qzH+ekC16A1BVSliWa6qGRr8rPX3m/93/euEBGzCMkRyl51ThfJM689A2c+ZpX8TPpcMQb5gWH4A2CODIcfkABVgiduaGMDNst/CqroECq6qweW5/MEDk+jelr5RHcGFBe6aqUfc+96T2YQyq/zyulesCRRGloifiMHwUo4xxn/u3f8KBnnAOxJHkP4hDL+lojrMLPz2ZZf13SwJ0A1xt+kSJssatV9cxX8xGaVXCD5VVn4OMueQsO5dYPfAqmAjtjZEoTpYFGj2aOXk+QGJgXSle9u5ASTiH58YLkSX37jbhOg7xAhqEO3TQ8b1JrPyyPICOEf/DIBoR/qVgZSYxuFPKkqBkZkcI1apy/y6/99fbj/IZj3sp+/5GnpikXsG2KMwbzBI9+zxv4lf/0e9zZPsTRXQHcDhbU8Lnhyiprrq7Kr6gqErWqp3JVX/U4MKZu0JBMGjcM2422LQ/QO9+t06g6HUB6ZCVXRZZldFKkZV0KWvyoUXAwFTz5rz7N/gufD8a24vySjOAzVBXn6tIT8dseX/R2Zl8EHnLJGykyzy2XXUa5YyenHJmiGwvMRcR5tFYYp+C1QsYSRnIbqy8eN4m1lvCbLDWOg/kgGZCUsN2Cv5EXpVhf+PuKVLtDURNOC8owzZ3ZAnNqnPPXn2b3hS/AEkTR7cX5DXyPhzoaEqodCxzqFL+NyTYRIeVVnuBR73gj3RS584oP4HfsYt9czpQZCwpJFvsJxNVjh6h5WFfR4vXCmeP49yN3CAZdoB414hYJvlvl/M2SrcrVWdT0/keqBFjLwbw0uT20WTD41b/6GPsueCFSD37GbS/Ob0GQVA3FQ6AoCnKX13MN3LbXGnWAaYkkFR73nrcSxHHT5Vfgdu6gcximVFAS0ao1E7dYO+RkMQGzXh9/0phh5A4x9HsjeoKPR7JqI/7fYuBb17HXOH8SmC8jP5nOOBwjT/urT7P/whdSWJe8DJAnUsm24vySqjzDvHbJfIb3AVBUIUfY7nTzdFnV/eATYp6z3/1mVJVbPvgRTtm5k3w+YUlwNX16wlCpuFS9CVE21lAzqW8/bnn2YiA8xAVanORi6yJqs3Xi9es93MCjODMiSmFG4eCHLU+iy1P++jPseMH5gJKnBp08kheGzxarWbYF5zdHAhoukFB8WylajswFJLLtM+gsK5GU4QySdPHa4LGXvBlBueWKTyJZYCp4WhEkxSqx5KTaBJIO7ZqbxM2ZJECeXPiHeCongrWfaCyeDuQJ1FBVoia6KTKfCpIu8Ct//gn2X/CCKnFjDkKkmRyWZ9uP83toEwlRSHcf5K6//L/JcUTSCVFrXZIhPkJ0RBqUUlWEPua9b+DM1/wuv7Q28z20LFWl5yKC1C7kZvr4myH8q53jqgKZKvhyfXx6vHr9wc8g77MbR5B1daaGwYDW6SLiU9GXSAWfS4mVCdqJslNSlG2Cwk+bOT9rGmf8n/+FU19wLiGW+FCrvoX+7C+hahrp/eISytRNkP8uZR2pwELv9XeVVHnOJOsyYwEt5vnn//W3+Nar/j0/eNelZNExL5CsgFQLTipIaDXZsYTucdhf856DkEEDyMRh3mOW8/j3vJ2H/8f/yC9UuXvGkxy0rHKBNAoxc0sYxkWkiguAKEbpbFUiq9Xg0UGGiepjS6Y+6go/39UEWHW7j/TafhYTdUmWbbSbOfVpFIYhfnEw9pJWxcFEV53UclTpd5dSVZeuRkeVBokuDgmJZmxw84xjznd52l98hv09nN8UV+P8fXjTYhUIb+HRwEPpEIGpIJglUgNC8hV4HgLdhXv5+m/8Hgev+WdOOvBgvv/299Mk44w3/0dIOWVuBIwkFSu1SYTM0SjdtswnGExePvz9b6QRMr552WWk3TOcemQa32mTNROp8EQX64Zr6fcSiAhBXCXgQzwGnag5Xta1QyzfkdaNAm3USKYBOLQn9CLSx26D9fgbtWrRU0OSVnX+QO48bVVC6JLbDLfsUuZSyZNOEJw/pgphiKFa7oARcBSaIPP4Y0f41stfxZHrr+ORYTfTv+zidrb41iXvI8bIGe96AxJLxFXCHx0E04qTKHPbFiL0lCCLjjPe+Tqig+9fdjk263jQ0SaxKIi+xJAKURsU9rqOyDF+CcRmllIPC+2CrToNcmvgyzTE2vcE37QKvqsFTiSsGuhQu1lWu0AigmrCU5Axy83TBUdEeNp//QwnveD5dA28bS/On7wQfEW5Ls6hFnAKeabEw4e59t/9LgvX3cipzjHVOUoZHA85kuF2N/mn919O15Sz3v2aRUzClCg5warxr9sZKIhINVsswlnvfCMpJW7/wAewPbs4cDhnWrrM1bGZF8GcI9VUVFLT0KiTiYVYR35/jakwA8omA5XC/o8f8binxltvf75rtHA2EL5sYlA8mGJ3tT/oneB6N6OGpYSpYimiqWKZM6tq8x2VO9RDfdSMZhDEmtzRihzOHb/++Y9z0gsvABLBKmjT9SFSQ+ref3F1o8wWB7reQLUkc+DFV4zwDmLnPv7x5b/H/Je+wqmtBifNlyy4gmkLLJhnZwfcTsfPvnw96hz7n/VkRD0+VUmzBASq4dbbeRQG3pWQYP95zyYVyo++8hVsV8DNa8U1pLXxqqax9PsznMjwCS6Dn2WCb6z1/ZWjeG117a3ksSjwZz74K/6PH/G4p6bb7ni+a7QqoWNzaLKk/qHex1XMk/3/mWr1iUqKlW9vvQUzMKrBzdTx62BwbCIkFX66I+OXvuQ5n/80+y68gNJ1cSlDvJKi1oGX9IU/prioCFusABKrZJcTwQqDTJjrHuTbF/4uh669igft3MmDjxQUlATL6TqHE4ezyFQX8pmcm6+8CjD2P+fXq8I8AXEK4re9HtUnSE7AJxye/c9+BrJQ8rOrryfbOVsN4kZwJkgvsJUBAVwuxKuggqsKfn/+tIwNvVfWv1JGLbpkZzykUgC77Y7nu7zVnyO1Odvk4qc/AaYWck0JUsKS9hWB2lJIT3nqxuvKWltdMyaUXigyxw9bOYeakad97tPsetHzCc7wZERvEK1KaNULnWLEeYc418f5t5rXs3AlmVZjRX0GsX0f37nolczfcAOnTbWYPdRmQQTzGcEcRe7IopKhqPfkhTK9d5qbv/gVVIV9z/l1NCWiCEEd2z3KuHBKFh3iPckKnHn2P+9ZhLkud173NbIA4hxBHJhWeFhP8NVW8LGOcndshaKsLfjLaRD7cYarFNCKolKA1zz8cU9Lt91xPnkTUd3EBNAiya2poTGRUsJSQlNNvGU9Ha7+N6jTvSmRFYRW8dUk52gHaAc42Iic85lPsvfFFxBVCeIRiTgVNPiq9dFiNfLVS7+4ofd7W25CxXDJkTJBjh7jH176Sg5f9xVObjaYOlpA5gkpoHhUElMx0XaVgpuUmDmaC4mZmRY/vep6YjT2Pu9Z5OpIrsDJ9iYLPIJ5RcpqiqaJ4ZKx4/lPIy10+MUNN+C9r+DTVDGOOu9wzkHUVRVg0eq7pT6RDPeRVotgezDpCrhVlinAGx7++HPad952fpY1Kk5/AbGBUWKyRjA7YObd4NZmhisixAQxYbHy76WP/FTCbsu02wY4XFQSDihCXjE00CF3DQ42mtzVKDjrb/8Lp5/7bJwp4h3OVY5Sz8ev0CU3KPKbjPMngjlMoI2RIVAoyScUJRSBdm405w/y31/+/2buqmt40MwsraMlTnKwWO9uWmPSFUOzMxBz5BgWPM0CdLbBndd+lVjOsee555B1GpRZiU++etCYiM5RACFBdLGf1dlSHaeam+brd2pSZZNOet6zoPDcddWVNGdymmUThzI3VSBRyS0jukVqSqlzPrWHhOHWjAmGCb4uyxGsGpf2ZKwoyB92+pX+dQ9//DndO24/P2RNMOsTxVkval/FVQji6uaTOhObKkFPZcTSACdPH0Va/N/yBxj2nLkElAxPSYaCm+aW2ZJugif/H5/hoc96BmSeEqv8Y9XqXmvLv9VQZzAHsXpxwQlYIgWthmurI2XgOoe58eWv5p6rruUhUzO4o23IPVnsYKsAmb0X3xXDpSqhM9OONHfN8PMvfZ3ULdj1gqeQd3PKrAqMSxwZ1RxjcQmfsm1xkQZlZc9znoDXwJ3XfpXQMmZiE9c1gjOSDxUTHUvHl9qg8E9wjJ0/qHcd51ylAGc+5Cq3dPFl6X8714cdBx/OzCBGrCz7H2KEGHGqiCbUNjbrt2vgKYla4i3npzuUY5Jz1t99jAPnvQDyjNJ0Jc5vHB+cv85kx1BRqAiGx9OxRAwQjt3D11/2ag5++UpOn26yp63sIsOVibwuvluZ3Vy8flMygoR6Oruy71CH2Z2BH132Sb7/lncvmWOc11lNXyYUobuN3lGfdkZaPOJdr+Gs1/4BPz82z30zRjNr0kbwYiuUXh2orC38g4O5VNbXVFOV0Q8YslV9vAF3xrTO1urAwAldFPDesIFFP971u+6HP4iNRGEikYb3NJo7uVU6zEXh1/7mozz4hRdWzGVmJx7Ob9AMSnnkMDf8+9/nni9ezcOmp9h1pMMCRiYZDYP2KjHIoAEqxRCLpDwnV2GeyEMOG4f2THHb5Z+iKVM86t2v7cdSmJKyxgkxx1hEaCbAHGe96y2Y5tx6+RV0drbYdyRDNULNSdqfzmPDk1XrNaG2BrIkLB2Qof2ElNU/qYZp6gu72KICDIrwEl9wSfS+tvAPS7otr9dukViwjLtCh6NOeMZffor9F7yIblnQ8Pm21/PnBklLctcLYCB60M5BvvGK3+fgVVfysNYO9h5LdJxjFuGIlkizhe8aSBp4USvvtkwlWchAIwvOMaWB0kpm7+tw2u6Z/hzjs9/8n0EDLlXVHSfKHGNztVsbHWdf8jpM29x5+Sco9+wiHC4qBbCauJbBCe4Dw0smdHdGNsYPrHRP3vzrH/a4pxW333a+czkaI2aKJasCWO1Z+gqmdP0AVgZw3clszWAcsCTjPOD/KpDT4KbZgrnQ4Omf/xj7L7oACkVycCmRkp0AOL/hxGGFoZmw0DnIty78XQ5dezVnTM/SaEe8GvMWKTLHjtDAuh0Kl+HEBpCNlceUC5QoLlaszRo8mJAc7OiCnwnc8uVrCKrse+5TwQVwlZsrbvtZK1RqV7SuZDzpec+mPd/m5mu+Qmu61S95cTZIWLsYCy4vlVhrCqTJ6A1PB+MAEazbJXvY6V/2r33Y2U/v3HbreSJ5BVHWNTfLLXYPRzH6ofrQAH15vDBaIQa2upq0ChF+Np3T2THF0z7zEfZdeCEpRXwmWHJIEJzz24zzd8k0X8T5O/fx7Yteydz1X+NBUw2mj3TpOkO9kKtQeocrEuYDDRk9gKItymyshnl7BCkSHkHNSAFahTC1Z4of/P3VqCkHnvNUYhmJ7sTIE7iUMBxRFB8jahl7zn86Mwg//od/Ius5y/2WvgHsXkYL/SjBV4YP8+3DoN0u+cNOv9L1Cs7QRajSrWGxqxJUq0sTlmVoMdS04pgZ4fYwJOPc94Gd8BN3hNlnPJedF11Ax5SQPF3AO0NrpGA76/m9+KpyPDj06Bz/ePGrOXjdtRxoOFqHS0wyppKRlx18EFoFtPMGyZQu7ZH+Q5DAIReZwlOS0FBVc6cMcq0Gms/eV3Lm7Cx3vfej3PzWy8myjEbyFL7Y9h0AZ3QdZMmDb+C0Q8MCp//Rq2jsOYXCVwV+vRqengeQ3Hh+/yh3Z5gBHHaO/+PTH/WsdNcPn+ek0Z8Q7Nbw4vtb1BANWw51jhLIFefXPqFTI2s0OPa97zI3P8eDznsmSCKYYeKrngDA40i9Mou6dr7KPEdwGzOB3bqCMyF0gMyAMpJ8VS/lS6Xwnnx+juv/7W9x+KorOb25h9ljEckiZlUNurqAUvUweKug2sFOVB2yliYVCO3FUfYa52tD0vOZvSViEJqFw2ZyfnjNV9HuHHvOPYesW024zLTqN67u21ECIUJ0JRvtOjYtEBHaQJZ6tf7UHWQgUhKoOuxSgpgF/MGD3PDi/0D7zu9zUuEIdYWAifWtv7A+q2+DpQejhN8JVpQ0Tz/9mhOW53v/scRev5ObL/1TvvmGN2M+gGZQJ4yq+nitOXy0r51zGPiNFws3SFA4fAlTKCYFMQ/4GJBodLMM6R7k+pf979zz367loa3d5HPzdBtCtxzNK7QRfn2AlHuCQkTZd7TkwTt38qNLP8dNb7qc2OjSKnK6dbIp+YBXCEQIkRA33kwgLkBHaZn0MmGolaARb6m3ghDBZdA5+FNueNn/zqGv3cBubS2ByHUdlIbDklyDH13NhC+Lj05YBXAGpy50ePjuWX76kc9x52vfw3wG0gFSSVcSksCLY54EXnFJmbLNcX9iqqxnzKppKGKBQKSwRAwed+Qw33j5Kznylb/nzJlZds0rLVOs7NLKdm7Muq6BgPQ+rjRCcoQacp4+vIDfAT+67JPc8tb3Qq74Ok/gqVwLH5UEFJvQTFDgoBEoBDqxhJTIpVYGqZikw3xJkSudw7/glt/8I+75h+s4bdcMzaKYCMcfFgv0Yk1bhuszLLk25NxeZap/zRmPfkZ5113Pc5L3vXUH2z4HXbyj0A472h5p5vzgxuuZPdJl94ueAUkJLmNBjEyrrHEhhnc1uljl5DemAE7wXolWgAREpUp2BcUOz/ONf/+H3PPF/8ZDplvsOKYsqCLBCAlK84vwxhpJnaFb+xC2s6HBYObRGNHg8SK0vbKvK4TdLW676jpcJ7LvvKf1XZ2Egg94repk3AbjJJ8WzWdeux2FCKnOBglCCh6bu5dvvux3ufv6r7K/NUPWKen6iDc/2gisEeT23OAlRAwD3x+MAQarT0VqBLNb0HjoQ75ywiqAWSSXKTyJBkarNcVN115LWcxx8nnPo2OJlnnMC6KV4ZmXVOHymwADektoguA8XgwxQZ2j7BzhH3/ztzn8pS9yemuWXfNCV4QZyZgjYc284s0cUo+yxN8fUeuiqyAgvat2JBFCVQISHbTwxJRodhN+R4OfXHkdJrD/WU+pTW2V3ExQ+d6bsEZG3abqhFiXxmRJkFRP2zx2N9958av45VevYXbHFHsWMrxvUFIOrVVaS+hXRXVWiwv6fR9Ld4VeSbR2C/IzHnLlCasAVWmz0rGC6eRpdRWb8fz4+m/SmZvnwec9DZKR8IivAu1chGTVNu83CoNGIKtq2a1UNDjmuwf51kW/w6Frrua0mQaNrtCwJqWWlGK0XI4VBWXu6p6G1V/aWkK/ollkyO0FIJqRJUA8yVfwSXKOWfO4VuCmK68hmLL/OU/FuVAlpzYpT1DWqIVzVqFz4vAJurGEXMh+/nO++W9/n59efyWn7drD3oUG82W3QrPwDOJ2NobQjx0Q93D+gTZMlgl/BYMWNM54yFUnrAIUCJ4OSI6TjEjBSamJb0zx46uvQtvKvvOfhLeAmKPtKmEQJ3jchisBCoGg0DUlhEBsH+TbL/5tjt1wPae1Ztl5JLLgoSDRVK2YEEpBM08eU583c6h1k9HWftT6Jx+YiTAfKpZgXxqZOGIq6VqiVTqm9k5x0xevRlKlBCkmCgeZbnx9PL3RqkpCyNRhDopguHt/wT/8L3/Ez6+9llNnp5D5Ek2Kb+UURLxV7qSNcHHWyimt2lE2wq2S2l3ToqD50BNYAZrME/0sQR2Jbg0GekLRprl3B9//yg34hcT+88+hcI6mVttuMF/BcZuR6k0BCw6OHeEfXvo7HLzuBk5uNJk+1iHYFMEUR0EIDkpjoRnwMZFbInq3poszKQKyQgDVMy+JqSxnwSlW5740CE0TIo58ITEz2+RnV90AhXLS855Bpp62L8g2CINGASdVyYmXgJRQhki4725ufMV/5p4bv8yp07tpdowyJDSruvicwpQp5Rq7UI/echimv6qgy+oJ2b7fP5gIO9EVQMkQq8DCXlClKA6h1Y40d03xk6uuYaFInPrcZ2CWyMxQ50g9xjirXpRQBcd1lx6mkSjVHKxePX9AkEJJ3jASvsiImRLa9/K1l/82x669ngdPTTF9uEvupildUZeBeKIJ5gRvVUNRXCMHoatUf47c5pd9klOcq1y+UPdN68AQOHERDY6pQkizOT+67nqsM8fuc8+h0Vk7T1C6hNO4Ns5PWVFKpQAG3UxpHDzKtS/9Dxz9x+t5cJitSlYwnFVJw14PePJuTRx/MLjtz3mrXbdF9GfYjro8UpAlRqZfyeaE2C2ZOvP0L99vB14dOBopp/dw02UfJWmbJ7zvPSStPMugisNhTuvhnnUVk8A8xrQLVWIrVmveClU9f8yren41h+XQ6d7Hd1/2+9xz9Y080rVIh9vE6SZufg785Fj6Ruj+Js4TaMUk0XHCSUcird2z3H7pnxNp8JhLXkOraNDNEzlCclVOARfBC1n0EKTC+RuhT0BR4fwVoYERSE5wmlDv6R78Jd98+e9w7L9/k/3TM1ipqxIjrIbjj3r+Jd8ZORHGrfoOBv/lhN0BRvroZpyaBNkR+Pn1/4TOtdl13rPIIoiPFbFWql7WAonMCRKNzFWKENVwJsSsonb1VMV1RTLIHHH+l3z3JX/Iz6/+Cg9vtZjuKjs0q3ahYJi5JU3bo7jrbQKW5HHH/ay1W2Q+o5kcqNGWRCMp3R2eQ1d+nU55jJPPezYSK3ZqT1Vk58uS5IXSORTBe0chUMaSYDWpXt1kJ+pxhbKQC8WRn3HTS1/NPTdezyk7W7iFtPog+EEqnBrHt1V89dVigNXzJPX1ZHWGiD4U2i2ZOuMhV95vdwBBOSTznHQ4kGZyvveBj+Cd45HvexOUiSxkdERpJphyGVGUkIHEqgozecH7yqqJBKwerJ1nRjx8mP/xb36fn1x3A49qTdM6skApjhgCZVnScmGJEK5nUsmkgj8uB35/B7BEMiXmGbkF5iRy0pyxsG+K2y77JE1r8ahL/njRx7C4pJ8gS4vtjr0GnqKW6lDvqWVT4cghvvkbv8PRr93ISdN7kYV5iiCE5FcI7yCznA30m4yV6d2EEUu9XWGw2u1+tQMMWjgTpWEtzBk7ouKmmvzwq1/laPsgp51/Hh1TmniSrwIvByxItROYq8Z9mpZkIng8ooI66fP2/PLqq3iM38HMfMl8MHaEjKNakrucMoSql3dZQDappV8rsFstVhgXOuxKwoUAphQIUzjKlGi2lbCjyU+vvA4TYd8znwxaYffOCSWQqdW++Oo4fyck9OhRvnPxf+Ce629kdvcudrYVL9NEOvS46wYteTIbG8fv6yXry5MsXT+3qHAs5gGmTuQ8wFrJEQWc5ESJiJYkPLNdRXY2+NnV/8h8t81Dzn06JKOs8wQOIROhpMoThCR1PT9YVMgcc92DfOuiup5/dgfFQoeggldl3sMe12A+RUw8fiABP+plrEfwJ8mODsPLsxCwZORlRbOiWSBTTxIjT5BN5fzgyq/g6zyB+FAl0KUi3YqyNs4ffvlL/vnf/D5333g1p+3Zy66FjIOdeRAjuLDEfVEqMrNJcPxVrf2Qfx++m7pVIbieAjTPPIEVwIYI/eBLNoRo82jWoqUZBR32pgbM7OCXX/4KqZ3Yd/6TCAScObpiBAXnpOLhcSWZZpQKPhNi+z6+ffErOXbD1zh5pkXjSEGyhA+umodlRoqJVshILvWzubZJE1B0woaPUUmipNBUYb4hNNSRdRN4TxTFnNAohdbeKW7++6sxVU56zlPQqJRSQcle1sb5v/Zv/pBfXPslTp7ZiSx0KcuSrDlLzAocjRqzWyr4a+VC1qztHztPsij0a7lTIkIqClon9A6wTPiXL1STNlGmaaRAx3VAApqMqW6B3zPNLVfdiGvXeQJxNEwoveHVE12FYVcszQJ93p5r2N9qMH20wKXAdPCodusmG09sNChTl8y66DrZpVcv553M9x31fpxktC0y5QNzrioPzxAKSXgzFE/eTuyYmeKnV30V6Rr7z30mIQU6vm5ZXAPnP/Tfr+KUmZMIZUERqoEYvhr5QSjbRFl9UuRQRGgD02GGXWRtBfBo0aV1IgTBNm4rW+/me1T7NPEo0Wm/4MtqUvrdhwts1zTf/sgVUC7w6A+8l7ZW9TJdZzRIaBmwDMLcPdz4ildx5NrrODVv0Doa0ayJk4KuAVI13qszXCrAeeKQJNKkAyB0ZNZyeGO7Ll8IG6AZHAjujETwnkKNBr6OCxRf/zdSUARPo2PM7pziO5d/CPMlD3/nG2i2G8SWgSmx5jX1DdD77uPrL/195v/hak7Nd0JZoHiy5DGzqvzZSsranVoL8hw+G0CXWfPJYc5RwEOlfEqd2ti+QTyTCv545w/w0hw1wvRJ3PSxz9AN8LjL3gul4J1B4dAc4sLdfO03fp+fffWrPCqfRtodXLNJuXAMFxobytZuvCpb1l6TCX5gmHKW4siioB72HlPyHbPc8b7PUHSFs9//eswaEIVAgWUNjh4+xndf+tvc/c83cmD6ZKxYWBXDX1fiUyZ/ho08/3hqdlwzv+OPSFr57mXF9qepZNfRglN3TPHjD32Cm//o7cSsJEQl5Y5w7G6++eI/4L7rr+MR09M0usrulNEtC2Yb+dj16quiGuvE8YfFFWutSQ/lWGr91yjCq48WGTnV4JASZXquIOwN/OiKT3HTmy8hMyNpSZE1aB+8l+9c8K+5+x+/xoFsN63OsUVLvszHH9W8PvT5ZFAce5NcRj/DOO/F1vh9284dQDdsUWXNf28Gx5xXTrkXZPdO7vyzj9NuJJ5w6Tvxx45y47/9A+659loe0mwwfWie6D3zWUYoI/PBr16EtU6rPymOryPs1XpQp8H7SBLxCpZ7vBOOSZeTjzrm981w8wc+TUtyHnzJW7FDh/n2S/83Dn7rHzh5dh/+WIfoqpbPcZR9UmNgG44DRoMNg6/2uCiAbUEJwKjvHbKq1uXYVJNdxzr4ZsZPP/g5cmmwcPsPufv/+XsetmMvM0cLFjLPTtfg3tQmm24SOoJKHFuo1jPlcFLBtwmaa8a5jwKl1cwgRjrB0YoZ3VTSOqjs2TnF997/SeaJHLnpVn557XU86KS9pLmjuKyBxCbqdNXnGkx4rTdxtV4ffy3BHzSbutUxwHpG24y+5viU2vtSYI5AiAUL1uHA3C46uzNu/uif0mo0OH3HDrJOQZk5vMLdvsusBLQdKV0YyzecVPA3U+g3ai1bEkhmuKgVmVhwCA063phdUBp7dnHHBz9EK29x2r4DMD9PLp7CHEF0zedaLvyT+vc6hrLLmjmANXaYZSjccdkBhvVsrjZHWWxYrfdkXPIA7SCoJbwlvGsw5+C0ox38TEaunt3zxn25kiWlIRVW3lWl0cjJO11i8Jvi4qzXGNgmCs2wa7ZTyS7LOJIZmTNcNxFcBk4pAnQ797Fr18k02m1s/hgNpihzILUx3xw5QGg95SCTWP1J465V1kPd8RT+UURHyxG+oQ9mvTGXVYizmiAVYnSzLkbGbDmNWIe5cIx9bc/sUaNjwnQSgpXkCs0ykBpN5ssOkuumoTq2yRRF1VO7Dd9Hi4xjQWngKZwRc1BvJG+EMrInzbDj0DwdFwg2g3Ng2iU3twyu3HxkZiPPNilhbljRMT/iznpLn9Zh1WSgsGMYh7uNQ+BfL76OCIKdQTPmGIlOaKMieG1RYjXJT+8t5LQ9QCQrAQmUusGKzAmDfhti+WyEWdkolUj09cwxEbKaxaU0RZJg4mlLlfNoFQkNiQ5UyUBXTXpcdT3MrfrEtgEff5TF16HBrvRn3VWs0FbPh3aIN5zkk7tAW6v7siVWVmVrLdBGhH891lE3iLys5a9v+LrL44NVUKzNdHUmRYnGokffihe8WvJqnIXVTRD6DT/nBFZ/9Jq4ifuANyL4g7CuriL4k5ZjjG9c3MTvZVKLP/SepSJL6GWepZ7H44cpgNbtboNc7av6WZsgTOsJbMfS7nVmFDdD6De0Hlts8XXi97OxXVU3OKTENqmP2npzl1aLASaxtpsj+MfHKq9X4DfPxx8zUbTOlztxKckYLYfrXedJ3Ekb4uxOksAbtSb9jjPWzkeMdIFsE+tfbMzpdLqJLs56fPzNDWwnvz/dYlLrzRT61VydcRNdk+L34wi+LHs34/BehOUX0ppLcqPW3mT84FY3KKDjCv0kL3qjxXqj789NjOqMLjuQsX37zTUwbtPLFcY6nEwMnbhlK7+uRJgbSwDWsSJiQ89fb4/tZli5E8HHH1f4B4PdQVqRYYGwrRMHs7ERHR3re+u1+qOoHceNQIKIqGJ4J6RoAzwsy0Tc3FIUYc3tVCa2mKuet4kdV6O/v1IBVw8e3QZdHN0Uge8L+3KBH4pM2dhKaL33b26daI4buhYbMUSDJLdjKevyuKcevD45DGpuTOu3eTi+bvOYK+XEO5Za8JUveTJWBV3bTprbMAozbrnCuMJvWyATweq61hUwp7kxrdLaN6abiARtFNkZfY5MsFuNf1/DXrBMSAuyAiQYct6KWc4jr6sr4Mq1hllNFJhu0MAtt/Zb1aIbRtmCzcTxx8KrZXOFfrOSV5sh9MO2ZxtBILUkaTVCqHtb/HpcwY0mrzYrYzupm7NhBdABl6hXrDasInOUtV8vJLhZPv5GXCfdJB9/IvdglV1B13BxYDSU2Q9uRwq+G/oc67XyG30Pg1NbjucRxtkyV7P263EVjmeJwkjlm/D7WyH0vftYzog8yoXpFXeNpeBjdV1tbnHaJDHNdgj+yCB4NVRmvcK/EShz6zuuVrIqTIborMMQrDMeWHSNbIOC79b8joy5DuvF8LcqqN00BVgvjm/rPH8zA9vNRHS2yjKNYkteLY2/ZPLJUPBGhrpO4yvI8HJm2yTB39COYZsrFwChdJlOk9HxRnSeoqvsc475ZQswXjHV+MmrDRelDaBUNqQ/VVcPZNbl40/surnhAqwDK7Vm1nak76/L7sstOW8Yjj/o5rh6JvPgNQb3lo0GtSOJsDbRBV7VrTTDe4+liqpd6nciQFKPuKTOWXSHaVOmgk6Ck7ImR7UY62bsOGVRh5sDXfwME41NvLl18QINFe6BAQ9rCP84ArO088kN2wrGyt/ohCXZ63F1tvPok3DVwzV68dZ0gkKTC+aJs5IRnSdog0NxHsszfLJ1BYrrQXYmrjdfBanazD5b3dRiQBmhGKsrz+qW102we7l1C/5G4cztOlx1E33uIufcEh6jMiRmJGgI0zsX2nQQncGVBWWA1joE/3jg97YWJ/9YvzNp6+HmCP0wwV8Pn84kJcnjojq2Ce940O07kY7lO20a+O8jruCU1kwnhIc+7K7ogJBDOcesNSljAeI2hsJs1Mcf96WM9V03tsCv9z56C66ruEIbIZAav6Vza1gV7m+C31v3wWk0PeF39fy2mTLgHn7GbS684OlfzNLUlaIJyQJJFXWyqp+/Vtf9WoOM17rWKKrA5QK/GoXiIpS59DOKYm8997H8nmyZ8JsZWo8CNRnuHoyiEhx2T8Ofo3rOkc/B6nSBw35z6O/3vt8bSH4CCv8S/98WsS0vQnAOjZFG2bhy17lPv9LtOe+ZKr/y6/+cugdRPGVTiKxP8McR+rF3kAn4Mav+2tX5Mdcj9KMEf7kS9ha7/9kgjr8ewV/L2tuIZ1t+jdUEf63ZvSfq0XOFXD1vNHUKsqc94caTz3tuxQv0oEtf83qjc6WEFqGIBDbGPLxRn3p8AKe28BO/DzfWfYxa1HGEYfn3qrmVtup5KpDGWtXJC9Q2KkTjCv6JUEnrVhlV6wzKbvHFh7/xP3yov4rZ+c/WU9992VsX2j+90sICLuQkDHPSRxwyAp56UgqKiVXcPvXHln2WcxSrLP2MEvxRbNHV3KfxgumVH8Vwa37Eqg8DHxPf/4hK/7PC5RJHlWMMmPn+BwJi9Uf8ogUf+GDVQPDlK7h4767+jGf1bciObEPWhDV2cJOVO5wOeUeD70q3+WPaxrLK98+7SpmB9565e49e+ah3/Md3n3TByw4DyGCUfM/lH3/C/Os+eOkxd5/mbur8FIwUamFLWo/OMUQMUr4mErNap08f6zYdDgGOadF0gk6joVZJ0kTnr0YgtjpbhowVOPaEabhbx9KE33oK/VZBznQMi7+RHXK7D02ObhB2SU47HoOjx/5/ITXcI9/5+ref+qY//PriexyCS99x4Sv+yP7+xueXsR0i5bmVJTEcHieCWaonQK2OUIyqJVHTEe7U2i3NaQLnaz2IyWDX29DaexuhMGajr7/szpb+KRvhWox6fr/qt3oWfS2BH0V/6E7wQdJ5aLFQzuEy+6LM7jy651nPvfaJ/9cXPr5C0dd6UT//P/5un9z8w0e2urEZHdpRdV6dy1Vc9DGuJQBJhhuZ3vdc/YJX+3VxSTdLAYYrxXjNcCqTu7TGGJVQMrxmp2dQBgVM108ls253XG3tNLKI6ImsAC5qsFajs+uhZ9y199+++O7Vvvf/HwA9D+v/tNjSsAAAAABJRU5ErkJggg==' height='120px' width='120px'><h1 class=' mt-5 text-wrap'>Invalid Token</h1><p class='mt-3' style='font-size: 19px;' class='text-nowrap bd-highlight'>Please check your email or contact administrator</p></div></body></html>";
                }
            }
            catch (Exception ex)
            {
                #region Email Template
                content = "<!DOCTYPE html><html><head><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'></head><body><div class=' w-100 col-lg-6 mt-5 border border-secondary shadow-lg p-3 mb-5 bg-white rounded' style='text-align:center; margin:auto'> <img class='mt-3' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAYAAABS3GwHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAGGdSURBVHja7P15lGxXceeLf2LvfU5m1nDnq6sBEBKTEEPbmMGIGSQxaAADpodfv+e3+vlng7Hb7jbzPIhBA6PNZMDYYLf7Lfu932+9td6vMUhoQBJtjGkza5aYQcMdqyozz9k74vfHOZmVVZVVmVnDrSu3DisXuvfmOXnOPhGxI74R8Q0xM4Ydh9/0hnPnv3fX2e72nz9sqiSkXIqjqRtI6lo+09XOe+CY8DDnhv/D8r/WpX8S1dW+CWBmblsep5YLEVnxd6MOMTbtnkvvYl4UeX7Gg+60Jzzmm3svu+RLw39z2c0deuXrX77wf/39y7Ij8ztS070o5p7kwKshgEMwBOQBBdi4tMgagu9WCP2gIli9/jJEyCZ9NcMEdOitLRXWia437PobFaG1fkcUJGvSPNjmCN0rUzN09r/8or/d/fmPfH5VBbjnCb9xefc7/+Ox6eTWC4LMYGaoGUbCi8M5h6hQpEj2gPhu8HBj/p2u+efe+9sM0znJrj5KeBXblOus956zFInZDGWCPCTKvAM/OXpl9rgnffuUb/1//niFAtzzrN982+Fvf+NxM7v3vFzmheTioioBooaZ4BCcc0Qe2AG2ThkcEFeatMEXr7K62qiBk3UJzjAXZpTgbvYOshnKmgkccpGmCjPJMW/KVMhZOHLsi81/dfo/H/jHq9/YX7sjb7v8me2vff2pzb17Xu7bXWSpjwrmMPHgHOrkf17hF138rGZzl3xnqVCbOEwCJqE+3y27jtafOCxWwFT6n2FnL/6UDBWYwY+u9ogiK4RfbOnHaWXhex+TSuDTwGet89cr/IP3P+qICNPR4/F0gsNngYgRppsvmPv2XY//5Rvecm5/B/jxI8//hJv7+StNBDBEs1X8z//Jj0GhnijGdCus4koh0FX3hI0ADus5V2ztezWzkRZ+M639ep9j+TmOynDEGClnd3z+zJ98/bfCwl/9f08Ot975cDu1hbkc6yygzYCPD8j7MCu8ER9/qSDopr/wzRD8oS6OLLuebL3Qb8bziwjo4o5hAiqCb+SEn/7w1CN/8oWzQ/n1bz7Zpu3cII55NWbI6KYH/PvNC2xHBbXDX7psg9Cv6nYMeQJ3ggr9CgUAfO3WRVUUIziPeTl3/ivXPze0D96zp6TAnBBK0OBwopjJA/K8YWVYHcEZ5uLIcRSc1YS1/9p18xChrb7/YetnvdhjWWxjVDvCgnWYOXxwV0hlkecCKQohOLrdAu/dA/K7Yes/Cr5ME+wam28tJ0Fshj3piWTth99j9VDJquxarPOGDsGS0lBDLBJaeDqSkfmcTmqTe0+hiYB/QKYncnF0rLOPd0A7juAPva5szMIvjyE2+gzLz5MxzxEREoYMuETEhCODWGaBaKF0jtIZeaGId3h169+PHzjG9qXHtf5b4dsrxmpoto64sxWokGyt8q7v+SuoV7zDIahqXckAZRnpAjtFCEnQgKCpQJ0Hc4iLk++L/1MFsqMt/nIfX46j8Pd83iVBoYx/vVEuzqQCv64n0A0CAiIVaGdGJo5CjeTBicd1umREJIkLD9jm9Qj/eqz98XVzlgv/8SxeNLN1exAbBQRWuEBI3xUyQGOxZC0eUIBNEvztwO1HujhrmODNrmWcqI5o2e60Fc/ff0ZZjAW8QSzKJcoVHhD6jbk467VYG33hq8KYveseJw92vRna47UjiQhJleAcliJWxgcUYLte/FZau+Pdn7FRNGerq+lFBGdLXUBJiiVFZXFXfEABJrD6awe1x1dIJylP2K5anO0Q/OW7tNbBtFlCy4gzBef7XuG/YAVwGxL8tYrR5DgIzigXZ6vLjTdVadWOO6reM1KqCsGhpWJFuSI//8AOMGZtzongimzGtWXALdhKF+d4W/uh91EH231FsFRlguV+oQCrtAQur7O3xRZCG5m91BHCsX7sernA6AT70fJ7FYM05AEGemDWjVutJvibiWJthuC7CR3TlaXPVflD5gWKktju1s8vVbn3ia8Aqzz2unu917b0bhNe/EYdsVFC6rbAxdnMgH4z70k34f4ruFWxRf6AFWt7P3aBNlZvL1tcejxK8Nestz+BUZztDGrXvP8hnkFwoNHQmKq+gDopxkDHW7i/C/2kdmMjmcaNCM1aQe39sd5+OwR/1ftf0X66qJyWEhrjEoP3L2QHWFv43TYL/jiNJpt1ra2+/80sT9is+3eD733FPwugqAJRQVfKRrr/KcCkpchaL5xbFxqyJcmqgYI0GfGkJ3q9/bZYe9F+cmusa6SExYSz2v2RlWwX4f4j8JPV26+nHG2rXnq/LmeVGvkTMqA9oay9rli/4efKklXXMoKmqiBuld+4/7pAQxgaVi6eG9sib7aLs+S6srFrb3W9/WYp/0aUem03Z6V/zzJVXSL85kCUlBLetFYAG/oitkEBNqnefgAOHf7ytk74Ycx6+3WWJ9iErAu6nuJrXX92ezNilbUC2srNsVXuSvrvd8mOJT1FSJVxXOgQ1Ej1+gjSv6RsHwq0eb3G212Mtt319rbB87fDxRkNHQ8GtLLGNXph7HIjqH3lVl3E/2WNpw3bJ/g60RkP1Nv/Cwxq++9Z19xdlwq+rJChXr2/WF3yoKn6/57lX+MIx1fo7z8v/v5cb3//EPy1M/OL57mJnDQxJZZx1V1ukC/ohA6CH6i339jzy/HV0yGCmNb1+ysFf7TnIFKRNgtGGROpjBUZ1nIwYUhccZwV4IF6+61WGjFODP9+maUd73y3ykKvLjcq4KzeNWKqKU8MEQdLihs3PRP8QL39A9Z+7UK08a8hEwt+/7drxjetSx5MFY+gYxrR47AD6Ja9+BPRzdnqevsTwdpvbmzihkqKW1P4F10ZkQreVK1wfzHD4Sra9oEguB8ML88E9yJuVyNLIh61Euklmjax3n5wmsl66u11gv0nIrgs4Bc6xJbHBJrzJdbIqoWi+jtlEUGofsMgeJyusyFkzNKKUSOCetfpK9SApd8Max8deFukDFcqIeqta5EZs9HR0RLvAx0q2WgVxoKU5M6v8d6X+9pLMfteomq57LgJPQ7nBFGtLH9ZIinh61+u1mr0Kq2+A0xcd69jCf7xsjRBlXyuSzmVw3wHm2kgWU6Kig8NklVIQTIDM7xUk28qc2Jb7uKMuv6g4G/FEbR3bSP2LKkTKq9CmCqEDhHLckIJuSXavqQQmMozYlyLP1/6b7v3G0v/WSeMDVeXHO0pQNI6EJ4sMRiWamkc2CE2h9/esXkVmJMoUNEARWkmI0xNowsFbUtkec68JJpWlx07tzg1RRbJU0eNGDp+LsLWBNkBQWVlzGNSBbCl8yQ8hRmGkntPwyuqkSKVffLZ1YR19ecfFwiR0XGEVkGvlRFRRcRhE659GLrgtV+1nps/XtZ+lFC0SgjmKGOkmxI7JNBtCix0aeSCWkWb4ZyAc6SUlgRVtg1Cv1E6wInvVUAHFL2XSFIzAgpBaERjSgKdoo13wnTI6cQS87JC8Ida+7HdpKUKNDqGVixVkKfVrpuzChWaZAKlAzcmD+jKmurBj5jVAcjkwr8VREntmGh7R5k5TJSFZqLZjXR2eEJvuzQqXz9q36LZhPDdpty/2kR8/JtxRIwkSzlzJFWCJCKkPJE5wVJiQUoIDskCR7XEsnwTnl/GdnX6169dUzGQpJXwp4hH6vG9k9+HWynS41p+xSyt++VPMvBsPdfO82b1UstEyzk0Jha8cqe2+WkGKfe4PNSWz/rTL3uD37b8/ntCr9uTRLAB4e8jSr04KDhcYdxJm6PTGU0LmBlFJnSdVRQj/ecXRvdqjOPju6EwanVvipgOyFxCY6pcVeshPbUCMJkBC/0fGUPoNzq4bTPpANfknBcoURpRKWYDoasEF7i77DBfGu0yQrNkdz5FyzuIaSm8uIw6Yyug2+2GL0W0pwG4GgQwgdIZbRK/bAiHOol7XZujWYOzUgs52kaanoZ4ipFGYv2BbXWfaQmfUIXUDUy6LCJYVeufaqFXq+BOsfFRRrfqzSwb97l8zOYkYengaM5x7MFmuB6hSKRWxkwXEsZ37Bg/63Z44l9ewtlveCU/OXgPx2KX6KopIoNbbIUGba6bM8m5bosEf/AeXBUt9iFDqRWgrZG5VHAowpP+66U86o/+Fw7de5gf+4L7ZhwhKm2nq6CEg2/Y1rD0rj9+d9Xr6FJZ67lmZopqQjX2p1maQKrr/ns7+tg7QCLhfBXpixhmqX4BbplVXLqYIpO/JrdOa9+z+Etck+hxVhJbHl/UpbFqla9alnQbLUIZKbznR8VR2hb59c99hH0Xv4xwEaR2l39+34c4/cA+HtxokHXadLynQZNjdGlZr05xoMG6Hg/qgq+23zED2kkt/mYYiFIMb5DVW5n1Kiw14UVQSQh5BQV7CFqCBO5ykGLBr//Fn3HghedzygsNbcP3rvgID997MiHLmE6J6CIdVWYkpxSjoyVTPqOrSk5ASau/eXPIwCxk0+GIn9auTK+e36lhpaJFOSDsvfWVda2ff92jn/DEdMcPL7Q86ycPBGUtJRrlY21KRpXh7s4A5kBqZchcm9J7vARKV71oMo/vJmZ8zq3cwz0i/OqffYwH/cbFOImUMbLnvHOYLpRbvvRFmA7sCrtJZiRXMJWalERUagqNuonCifRZxtZag+2qtx/8fdcrBtPeQGtQqeBdh1CqkKuSvMNZgfkmt0vBgnZ40l//OfsuOg8r2kgQ9pz3DMJC4kdXXsPMbI6WhgVHiODNYaUSskCJVVPZXcQvpqSGWrcezrZ6Y8zieU5cNfElVZCn1a7P+gSrRvmsIH/w6df51579hCfqbXddKHkO1vN4dGIvddODWVn8DLt2lIQVJY3mFL7uo3CpmgaiUXGNjFviL7k7wVM+86ecevFFdMURfMA7cCXsPPdZ+BJ+9KWrKKY9u3QKA4rUxoWwRNm1V3Ne3dAKnG4j9Uyb6dv3LVstItYTfAFcdf8C+NAgOkee2pif5laZ57AWPOFzn+TAxS9CTfABUhkJ0mTX+U+jPXcfP7nmRvyOWXYUBpnHDGLmcDhCCQs+MeWzNWL72j0aIS+DBkZEMFVSETFNKyz+RhQgyAApWuU7HR/B32gxmvceR6DodJFGAzodfJaRCZgXbu3ex2FzPOUzf8KBl7wYH6GZKQkhWIAcUoSHXfImFOO297wfOUU4Pc3Ski5trV6SOOknVxKKF9dHi7ZE8TcrttBUvcleC2A9NNpZ5XKU2sVTYvkOflDeS8d5nvr5T7H3ohdhUcmcgWSEkKEdxTcCv3r5JWRlxvc/9mnC3t0cWFBKUwKO0juC92QizGmiOYTUYBIQoHJ7en9QrExV4susKnbbpPULff92Qjhu0ycWTni9UEJXgDxjpkjcMyPMdEtSgh/5gkMxcs7n/oyTXnYBdEq6zUAjRZwXlIwukPsCi45HXvJmnHPc/J5LkQOJk7VZCYtWEJx3Duc8ilXBlrHt9faj1svMqtIGV+1Ykqr77onltDO6THGTHqHrA4//7Ic46cIXgWlfgcx5khrSqqm6YoPHfvhdaCvjh5f/KW7fSezpGAuZMVVA8kJWCHlwQ3iFbOIq3wrRsQrvj2WNCm3uioflVtmOk9BvtN4+1edPl8Ixl5gtYAcZ3/Qd7iuNZ3zmT9h/0fNZUGg0PQ1LmM+rQjwHLQUkgERSSjzsXW8klZFbPvBBZP+pHNCAU7AyQkxI8IiTmmWYTX8Rkwq8G2UDe8IPSJ1v8BIwJyhGJ0XuyjocTZGnfv5TnHThi+hom6a2IIugeaXoviowQ40jouwsA49+35vRcoEffeAvOXryTs7seopY4K1BcB5Jkdh/aeuz1VWlq1VgQ13m3FvzxOYpQtgqSz/WmM4NnO8QyBypMJoq5M7zT1nBsXsjT/vC+9n5ipeS6NKyKk+IVBaQAmiUdJzRjDn4QEpdvLY4+31vJVP49gf+FL9nJztDk4Y6NJWIVgGJGHhxx8312cjvOOsJi6s8IC9EjALlzoZQapcn/sUn2XPRC8EiTWvSySKhMFIOmUa8gEbBOWGneWIGWTfnsVdcgrgpfnb5p7nv5F3sKRzqKteqK4lsnXOmB583pYQWJZbSlpmbsNwqs4VViONa/LEUwyK5ZZQorTzje/EwB48pT/vrD7D7N19M3lViIyAk0MCCBHKB0CgRzWgKFCGSp4CGFjlAWXLmpa/DAnz/0g/w0N0nscc3EecW64PUcJ4lIN92CP6oaiVNhjnBekUeTkgCbUssaEksuzz+c5/g5Be/EIsG4rEQaVpGzKGhEF2o66WqXbN0iouR1MgJZYPHXPYmTDvc/sHPEPfvYV870XVKyypFWxnIrU16vrQA0Eix2p19HdTrIEK0SeLp33j2r/1acesdF0qzWW8zjopNRdaxZQ0HvkzqlrT6H1XGuLr6yudrCAkl02rrrvoWEkXeIisNnwVuKg5yuCg55zMfZtcrXk4jesgFhwOphn5n0lt+378PX5dChT487RA8e5/7TLLC+MGXribMNNkrOVnRYSFA5lqURDJxfSRIRPBS+dpqVg1nHlgPW4fAuyVAmCIVuV/9UbRGpMQEkQBWfUsNxALiFMXjXCBaIrcS5zJ+YJE5KXjo3/0VDzv3ORVtoK8Ce7FqbazSl2q96puQWgid8zig60vymHPyC55HPNbmx1d9lWwqsMtyikLJnCOqkofK7epYSeYdmhLBHCY1dclA7VU/6eUcttBBtMpQs2wNN7QbLEeBdNiL2IT9Zi3akHG4L0UMGoFyvo1kntILJKHjjCzPybslTZfx3XQ393nPkz79SU56yQVAWZc1Z+tYm0U6joe+548RMf7p/ZcS9+/jEY09NMoO4tq0rEkh5aJi19yTvs4gm9om8Nvrmvbfmaspv6uy7yXWUYwyCU2UrihBIsk3uc3aRLo8+a8+y4GnPxWCozTFiauaYXrJTYtVfLTG0dCMGBQrSx57xZsovHL7hz5O2ruT06TBQqqSVb6jlBLJGxmmSgtP26V+xt3M+nkV5xyoEosCZ8cHZQtm5gYTNyrjZWzXou9b66bHLVMtpMC3hcZ0C19CVxWvhldfYdOtJt/u/ox7u3DO5z7G/pdcyJwZ02Q4HzdgIOqxOlE4411vpBTh+5ddjuz2PLzcQcSxYAt4ly1aq57l73VXmVUJuUmEfgW//erRzyBgoVhl9euGFiyRgBByohNC5wg0dnKbm+dg0eHJf/lp9l3wfDCl1Ij4fPCmKuUaI8tvRuXlZw3KwnjCpW9DrOT2D36KbNce9miO5I5uUoyMkBQrlaM+0nRZ5aLVmfUe+C5maFnld44XuBw2Kqhj+6wTXi8XD5lDF0riVI6fK9CWp6WOhUy4pbiPe/A89bMf5cBLXowYtIJVOSoLGyqoERFcyKGAs971JlDj5ve/H04SHppmmY5dOlolUMRXrlC0RDKtXK5J10jGqZVfVm/fq4Pp4SGiOF38nZKCoBFp7OQH6SAd85zzhT9j38UVzh+d4WrhT2UiD75O+CnjFFMKilrl6mWZoB3jVy97NyYZP7zikxT7dvOgY5GOKDmO0gOZo4VnThIz4ola+QmV5TdSWWIx4utu3uOmAIPW202YC9sqoiifhMIpLvM0u5GDM9CIifkU+aEvuK/T5alf+DSn/sYF0C7ptDKamsAppYR1OEBLjy6QhS4ues665M3ghO9eehm6P/EgbeK0hhdj6pcX9OqVnFtGybEufvuVOPoSB0kV84tXcckI/SpWoemhrTm3cIyO8zz+zz/MgYsuqJJJCAz09A4KfC/JN+p1Ri8EhVTfgm/kpAi/eulbCOq464MfQ/bv5bRjxrFGolV6BE80peH8IptbDSxYiqS6s8tX0MXxU4AlzdZjdEMdj+EOyQuiQjBYsIJWKezSjH8MbQ6X8PTP/gknXVjj/C1P0yLJZVWj9yYoYzOB+QxzEYup2gnKxE0f/CD5vlM4SQOSDCmrunQJvl+2sWrl6AT89ksrblc+UG/nURJOq8I3qbH/5CBG405fMIfy5L/4OAcuuoCCLnnMIU+kUvHegwjOV8oQU8SH8YhCFKGQLnlyJJ9hCi5ALOHxl78do81Pr/hLuifPcGbb0y1LxEHDB1KMxNpQOCAWBamMOCpgwjh+2fUhibBK+0fV22+WoK/24oWqUEsiNJOH4PhmXnDsYOLpX7iUna94GWpdGrga7VG8gRbgGyVscA8ofUmWMvCBrnRpaIuz3vdWMhP++QN/gts5y668RW4OKVOfrrViVpDhSauJ+O1lJEhgMtC87wR1QuGMjig/yj2Flvzan3+KvRe/EEzJtUEnV7KSJYKeYsRnAR9C9atjeAF5CWSe6I2gEAGPIlkG0XHW5e/Aa5M7P/gpdp60i11FoMgFi4mO1woJMiPVTG6iVf2ZiqBWsXYcj8MNCt6wsufNTIT1u5DGaDaxuklFVdFWxi06x73zHZ7215ez6xUvoVEajoCvE1yFBZKAa5Sg2YYXJrMMfES6DnyL5IFUcvqlr+WsN7yaHx+9j2OpQIMjYpU/W/cUBLdYUD6ks2DZZ4jwr1Un3/uYVi5YvWuLCBFhwZSjsaQsj/Erf/4JDrz4hUiqmmXVFTTNYVmo3DWLleHI3DKYcfQWWmYlxICSEUkEU8QcIQlFiDSKKR75gTdy+h//r9xy3z3cPSvk0ZhzialUwa4pJWJR4tQIdVOjWjyuWfYwKPh9ZoBxqvVGWbMRu0T/33Up+0Hv751zuJjjGp7byp/zE0089y8/xb6XXARFgrxCpnvGchHLyDano0TqDbIBzd49+4A3OPvdb8XEc9N7P8KZJ+3j5CzDt+fp5oGWa7Jg3YqbUqlxbKE0RQWChGoOsOmqzgWiJF/FGM5kybr01skhlOJwDpIqXpXgcu6yElyHM/72bzjtGedALEnegzic5X2yKAHEheH7zRjyl5FBqNfd+cU190JOgLxL1m3xuMsvR6zFnR/8IHbSAU7rTNGWLloWlJZoqJKRKGol9uoppeIs2hYUaLPyAOO7OsOVJkVhptnlq/ZLGuU053/iQ8z8xvMrffF+S7qmJskTPPLdr2E6Cv906aW0T97Lw/0eXHuOwi+QuxYxFUv6opxzeKmCv5TSKoHKolK4uLg+vXXpDcp2QIGRR6OjniARzXPusHli6vLEv/gcpyzD+VNKeOdrxo/ROP+G3y8ZsVFiER53+TsI3nPLRz5C3KEc6AZmFpQCJWF0qRJjmQqjOoyPCwy6kcHRYwfOq7Av9F52aOZ8r/NzPIHHfu4K9rz05fQT1J5tO3pKkEfHGe96PV1nfO+DH4BdnkfIFNEcbe3QkoDW7ZaqEAy8LO60MiLp5fttitWXE7Y4CkoE74wUMvLuApLPciuHua+Y5ylf+BwnX/yCFTi/1JnqcXH+Da+TOjKFGBJJHWde+jbKtMAPP/xxfOsAe0Vp1A1YpaOK4+rw19vmtc2ORBtf++gnPCneducF0mz2F6iK8beW338t4TczbuUoB6d28YRPfpB9v3FxlfL3hlgk1XTY26kEyXmcOvad90zoRH749/8N3T3Ffpum1V1AyUBcVcRQsy70Ss6rkgtdEQcsCc7MY85hIqRaUAbLI4ooeOngG01+kO5jTh1P+ctPc+AlF1SzcQHnM6TG+UON+PRw/q32s1NNMuZU6Xojx3Pg+c+m3Sn52Tf/B+2G0iwht7pdsw6PeolY27qXt6QUwm0mdMkmBNC9cw4vzHHGM5/Og156IY0y4J1gJEwjaVscoGVBIJBCQUqJR1/yZh79pjfx4/vu5rbsMAsur4i2aq6dTFxVJixCMh2LhilhVXBNj7bElvQh7MgEtYyb9AhtHP/qcx/hlBe/CK+pjgd8XUQGYRnOf7ySTOpKzGW0NODKArGMx73pP7PzwC46GRS+tvS93gtsBQfscUOBeoIvMDEx1EbyBEtXbBHWO23HAX7yf/8/fP8t70UaStTKKoo0yHTb5Z9mCV5ycAkSPPrdb+KR//k/8aN77+WXU0JqBsxXjSEStWfWMWdr9DMNsCY4WRIneK2K0XpVnV0tuUsic5rxpL/4Mx7ykhdT0Kk0M8vQWNGKYPRx/qTpuCHsTrs4y0CgSAlcTtE+xtdf8Tvcfc99nDIvNFO9D8rik/t69ziuMUDPwtgACGDHQ+BZigQNPvdpHcNmG3z/0o/gmoEz3vpHOJ3CGUQP2TYrgGUlkjKcz0l0CKnJ4973NjITvv+BjyE7ZtgZGhXsGEvUrP9ig1VcNqvYoWUGSfrEXYgSqXh7fpQJpZb82l/8GfsuOr8qD4+BTsNolhVzxXKc33k/Ns6/YU/DVSFbSaTpjfs6c9z0kt/hZ9d/kX8lJzPdXiDWRXxikKTKDXg4rjvA0kyw2kQh+GYRwApLKQnFACl5UJrB7/bc+vYrMFUe8fY341TqhsbGtiqAkFH4SugIFVCqMXHm+9+AiHDzBz6IzOyh6XOcOso6/+EV3IAyrLbgLhnaQ3nVUBKqQscl2iRSNH7lLz7OgYuei8UILmChag6KWWVJTWOFPg3g/GZ2fHD2okHKFR8d2pnjjpf/Fseu+yqPCLvZMX+EiCdilYuG0XGQUBrJHe8g+IlPirfefoFrNQfs/3BWiIkEvm4ml+WxhVk/9hvsHVjudjmtuG12kaOzTe788nXQPcaec8/BdRrEEHEVfFCV0DqhAEKE5PS4BMm9fgI3EGB5HHuf90ysKLnl6uthKmNHCkynRAdDkscyXw/5FMRcVfciPSYHIYriLWLiKwJkqbBynOfW4DhGh4f+3Rc483nPrprcva+48ql8if6wIVkMm/u9BSKbYv2TFTgRCqp1zwCKRPTgSKBVqUYoDnL9b/429115LQ+dmmX3sUiSBkqPzrzqcPAmOBNsKwPgIUFw2DJLryuzypM8meaeUCiawakdIdu9hx9f9heQch71vtcSug1iQyufkapmSCSCF3z02zL+b0me4JLX0cLzT5ddhu7axcPKXTSsQBsKhZD36oac4utYQVOV3Q2ilHjyMhK9IxCJocmddOlql1//wp+zf4P1/BtWfgkQHbnVjrsoKReCOlJyWAZF9yDfuuh3+cU11/H41h7csQU6uccXc7ht3sHX3n/ZOPnrkvPXM3Cik2hK3h+AdqAwpvc2ufPSj3Pb298PDcXXg9WCVoGhj0opie42zr7s7WS+EB76jtfzuNe/nl8cPcpt2TGCNvCa0ZF2pSyuqt6UpFhSTBRzVZFY8DllFsi0g/fT3Ok6HLaCX/nzT7DvwudDnlGarqznN44Lzt+pt5qUVf47VMaoSIkyAz36c771klfx8+uv43GtaXa0S6bUYUVkahm79HYe/rWP/rW+C2S9IQM2eUvkCr/eVuUFGzPI9LhUkdwGoGh69ndBZpvcduVXsW7B3uf9Os5CjxULdRmhbuvbTqBURDDvMXXsO/cZ0In89MtXUewM7Cwb7ALmxWqUSCHFqsfXOcxXtUVljIh2CflObtJDLABP/sInOeXFF5KiomLbi/PX8KqhlOKqXTg5fACOHOF//LtX8+Mrr+GsRouZhS6FRkR89Zxq2Ha9oRXMcEsUwNaMASZxkza6/GWoWuW8GZp5GmXV/DFrjtQK/OQr1xFN2f/MJyMEiOCkmnriVTenJnoDRxvIXERLZe/zno0k486rrkRnHc3S1Z2LA+hbr8c3KSSl5SH5BrfLAh3neNznPsypF1+M1L6z827pbIa6AE/6sdfWPr9TEIs4MRBH3ps4076Pr7/i1Rz+0pd5RD7LbCehZuzEca8rafoGTv1xLXleUwFec9YTnpJuveNF0mz0d4BxFGC1gFbYHIQtUyOJ0lSPc47C6rE5WcaO5PGtwE1fvBJnyv7nnlP5vK6i/a5oULZ3a83KXmeZ4syx9zlPh3bkR9d8FZ1pstcyfKqSPylUuU8fq9Ji5wKFKT8MyhGLPOnzn+DUiy+mnRbINEBQUqm4utCuJ/wxxUVF2OIFEIVU18C5stoO2p1DfPOi3+Wea6/kIdPTNEplWj3JIke9cUAz5rWL+WZ/kPb27wBnPeHJiwpgIxWgb+m3WIGjwB7JOSolSY2meYIJZexSOmM6eqb2TnPzF7+CqrLvOU9BoxLFKjdom5PFyUdc6cELKhFS4KRzn05oF9x81XW08pwA+H6dT1UMpl6Yz4Xbc88CHX71c5/gwMUvQjAyy+hkCoURsqzfypVixHlX0bfU72ajycyRKKeLZOopkuEzQRfu459f/CqO3HA9p0212HnMKE1JVk2dCRJIZuQETBLbtQGsUIDXPfrXnhhvvf2CcRTgeA54yMwzR6QhrgpqnYAqKRgNBScZrQKa0zl3/v3VuCic9LxnEmKgCEXNTryNCmBASLjS0/UecYaLiR3nnYPvFHzvxmuZdoEZl1UKoErmA/NB+Inv0E1tfuWzn+TUl1yIRqsCW58IFtDgK2oci4gD56Vf3tAHPbf4BXkUTa4ixj12jBtf+rvce91VnNLI2XskYhaYsoSjqJKFKqhkVNFCFyOcGArwmrOe8JR42x0v8q1mXaFZF2oN1moNcXO2+jCxukVC8KmCVasqSqv9/JLojVnLkOmMO666Bivm2XnuOeS9PEGqQfo0kCdIULpyyxXES81LVCOyDsGcw+HZ/9xnIbHNTV++kWxmhl1dwbyRfM5tvmTO5nnY3/4NZ577bMS05u0RsLpdUGRVnH+zrJNqrIPbARKwaCRnFc4fA2WARvso//1lv82xr3yJh85MseMoOJnCKEgIRqiICjB6+e9tE/4VCvDQ64b2A2yz+zwWe4JmAV8olsMpHU/YU+UJTHMe8Z7XkHcbxEYFUZdS4dUiEfNWtzpub57gMW97C7nl3PzeS8n2HmDngufndpCjyfHUv/zCttfzRxfIi8oTsFwqRQwQ1BEVfIDYPcjXL/49fnbNNfyrqT3YsQ4x87jiKNtfrDLeEVw9RKFfstsnrd1u8tcl9mhl3NGtEIXSIFriQMdxbHfgzvd9jJAlHvWOt+BUMQnkCilUeYLCG+a3r5CipwRmnoe//XW0tc3t7/swzd07KMuc53/2o0xfeD7I9tbz11U6WG6olXh1iHe0rSQLGfHw3Xz3X/8hd3/1Sh4zNcP0giIpp526NEOgHe8X8o9/3VlPeHJ56+0vcq1mPXVPaksr22rtR9XLW/BIggLFYZRNz74uyI6c2668Drol+5/3NMR8hVg5Q10gs3BC5AmiFPhuxr7znkPUkoNXf4Nf/9vPMHvRBZRxAcRvK84PgnhBrRqdVIrDmSMXgSOH+fq//0N+cuWVnN3ImZnvUiYjUHGxllpU0PSJeCx3gXrMcMd7XOdGB86VXrAYcerQEMg6CTM4pRuIs7PcetlHSLnw2Le+BsiQwnBBiA5CUvDbCxN5gABawuPe8TYe9a9fTnjk6QDkWXNJQ9J6eHs2/n5ANOJxRO/ob0ALh7nx372aw1+6mrPzaXYsFJQm7CLwSzrM1AEvxv3iCCvdjeNzuHXU+y0yJwh5VykwpsxQq4qynAiWeU6NDfKd8L13XUGmyqPe8XosVc3yHlBx295SI7jKLasjzMYjzqb0JY4SiVnF9+PcEt6epKn/31uex4A6U10NEs+CY644yrd+4/c4eO2VnDk1TbOTCGR0LPGLEDkQM46mDuJbkMr7jwIM+t09x0M21ZrYEAGY5NyV4lqKsEcaHKSLTwUNqzhtuu0uKYP9ZY475SS+/d4PU4jw2Le/jhQNDYHctr+jLJZGZhBzJaYuWWiRlRlIAg+O7a3nLySSa6BtJS3nYe4g33nZ73P4azfwsNYUu46WzHmYw+EQWuqYI9Egw1LifhICLBYM9Ef+bFICZbOK6VbbnxrqOaglDfGUuSPmoE4hE5oKTgJ7jwkP37mTOy75MLe+/QOEEMgLR+m33zpViax5lJKmtBCMmFUlzx2xVXl7jlc9f07V0J/5DOaOce3LXsl913yZ08XYcbQkIkwnIddISzKCKskLhSSUgvvLESqGYamIhWvnT2Spi6K4MS31CBRn8dt10d3KrqjFTLNfRhqrDEJBySkZFRdnI1b/ngScOZJAKLuUmWd/mqK71/HdSz9IYW3OetdbyDoZqRnxMdQ+kVJ4SDhaJWhWrotefTIXCMima16duoG8Mv00e0mtYbw9m2SgSiIBR6rfdK5AmYgNIaCgFatFNj/Hjb/5Kuau+gqPmGkxfSSSfANJZZ0f8HTr4kmfrHZu3Ylh3deUwIorLCwX4t76TvIQ6+G3Xy78a9OF65jO0+I9l40MV0TI4CHzntauPfz0fZ9DUs4j3vM68m5G2agIZaOr8gRJIgTDldn9BcZev49vAVLtAwfAKalR1fNbdGhe4/wvfiU/vfZqHj+9A5nrUGYBV7bhREV5xpTAXjGeWw7PrdvFEV36WVNIHWYy8FlNgZTxmuNWkhBqUdJwOZlW5cYnzytTezLueO+fcus73wsNxaWIGWTKQD+B0f0XLvzVDlAtbQoVlLyknj8HDh3kWxe/ml/ccCWPmc6YXSiZTg4tEzO+cb973h4l53KJmkiNN5ff3q2iKDqGsI/j3AmpLIkeggjdhuPhHfj5vhluee/HcGXgrEteNzDu1EghrwJT+ZevAEkgy6jGooojJiGYkQehOHqYb/y//pAfX30Vj2412HW0S7SA0MShtFOHE32LHEY9M5jd0kqe3bpbIjfKbz9a+Icr0dj36utp8skoco8vlGjGqaUnTc8szRNIDl3FZ/6EyRNsuQukgFVTIJ1UQwBVBGvfyzf+9R9w6Mtf5uzmNDvmImo5O8j5uUSmQkBLu18Kf//fZNBOiuhy16fq312FsWxM/H4Uv/1oqz+Z8Fe/t3idvKjGgTbwJISibjonZDy4dGQ7hO+98/LFPIHPsDpPYNvKO3d8Dq9goRrkoWWELNDpHOI7F72Sg9ddw5lTDfKukpGxQMkvvbI/BY6VbYJvVFUDJ7DQ64DU6OoYqC5mggcttaxX6Mfnt9+oqyM2qGS64te6DvbQ5D7r4KLSsoCI0Gl30ODYV2TYyfv59ns+tDJPgONf+pF8xGugo4lm5ijb9/DdF7+aYzfewEOnp9hxNNKWxEItEzlGh8gUOTEZ6QQX/LFh0NWsqYwUfFsi6Cv47Ye6OTrk/Mmt/dIdZvg9esu4ly4t8XR9NUjOxYRlQmaKJ2fvEYUdVZ4gT45Hvev1UDpiVhL+hcNAXiAlqsk2R4/yjZe+inu/ejWntVrMHkkUEphSRai6zLopoi5QqpGOK4P/xoV/2C7Q83pCxQRdoTlejKhWNVmMaIy3tbKpfcHXVZQGVGRgXNCyaw0JkFf2HK+t604SuTmSQUhSjRJ14FMFgKXUhkbGyWWD7g74zhUfpuu7nPX2N5K3G8RWSajLJ0iJGDyRihLRsnjiFnv1UZ6SDE/E0QFagO8mUkNwKJICRVCa3UN87RW/w5Grr+Zhs7PMHok4ycDKOptbEXgK1dzmukZ0y+/fDTOZMqal78mOuRXnD46lUCCsLqwypquzPr/ejVAgMd1QB9pyhVn+5zLzhK5izcBDup58egc/efdn8EXW7yfo5wm8J+vxDgVD7gd5goCH6AgKM1n1Pqwh+OQok5BlIJ3D3HjR7/Kzq6/jcdN70PmKtyecALw9OkZgO0r4x9k53HIBc8bQQW4V5i9rCL8yGrsfIH9d89F1U2gX1yrHaJjgEayIWEqc2oZde5v88NJP8IN3VHkCiWWVJ7DFPEESuV/kCaJVvD2WV6NPsSpOKk2xXNAjP+c7F7+au6+7jrOnp5lpR3ZEBycQb8+wkVrjZoaWi2mfel968i7gJIUlEyJtLWsvk+jrsu+78XReN4drdKwgUMCJUnhPA0+RGWfMwS/2THHLpZ+gkRo86t2v7VcH9vIE3kDl+DcMTXqIVBFeiWJOCBrwEbKg2KHDXP9bf8QvrrmaxzRazMx1KNVokNMAilJhm3uqx3Z3Vpzo+uK6UglkefNojxxXJ3BzdA0vbZjD4obf5KCPrxtjhFzPbqGawDm8Kd1MaJTV3x1YEIrpWW5+/4fQhuPRb/pPYBmuFPBQCmTJqijyRA5yE6hEcoFUOUTVsOr2Qf7p3/wBB6+5irOzWWYXEsmEnTjuocu0b5GSwTbgPOsW+pFukqy4dn8SUU9Il7oKMoY/vwEYUwatvR43oR88mqlim3BRMROKEMhwaHA8uGtkszN8/11X4GPkUW9/HZBj0mugOPFhUgGSq/mGCoXc0eke4tsXv5J7r/sKj8yrev6mOY6Z8Ytg7I85R1IHF6bxMW2L4G/WubZKWc/yXsdQh0c4qwaVmcqSUUnri91Xd3OGDcc73sIPcMwZJ0mDQ76LGIQyggqxLKHp2VdkyP49fPs9H6ILPO4dryeWioZAQ/12ewgjj46PNDVQpESeQVq4lx+8+PdYuP4GTp+eYechZUESx8QQE5rmmRejZRkpFceloWsjGP5KwZeRyjQo/NUkGsPpklm1NubtrJxzu0T4zfXH+fTH+mjqk+RWLrStGiQPQ4g22l+w/MhDg1+UbfKyGjidvCCZQCvgi0iQwO5DysN37OSu936Um95yGSEEGsmT/Inf7tGEanp15tH5eW58ye9xzzVXcnLw7D5U0hFHZkbQkpYTsmSYhKoTzTontPCv5uKMu5PYAKlAWAof9XRkrSBvjO1fFNO0jr2rWgaVzbX2w3YPtYKmcyRXz6S1ah5XUMB7ulIQvOe0dpP5nYFvX/FRnHM86m2vrdkmFodUn5gwUCDminYXuOEV/xtHr/kSp02fytSRDuIiuWrl+tKgi1SDwDVWJLdsHAXaEI6/Cpy5JNc6Yt2V4f0AVS6gMvsiE2dzxs/QbtTFGeTQ2UzB769v1H5zyWBQ1B9ckSK5Qsct8LBuYHbnfr77/o9QpoLHXvL27SN3HTsKhnB0jm9c/O84cv03OKN1KjvnDxOAjjkSi0NETLWu2BJE6uTRBtd+3Tj+MjRnI/HBKCVLy8cBr27NVmL3YrrkU7k4afHP6xDQQRdnq/MAvSdSWMEkrRg+NGiHAvGejhOOtQ/SaDWY/tWz++XSJ6z1B4qyIM4Gdj/pV0k+kkIJETouYVZVf/YG8fkqz1sVAtY74Wa5ORvB8W3F+PpqbrKtY937/QD1p2fAQvUSdWJrv1nWeatcnJE7bD2Ttif8VWwyEA/FiDNPCC1uzdt0ypyn/tdPsOeFL0TuBzAoeU5AeeTl78byFt+54jJO2znDGXNNDIdaQTKrRlkNlKUYVbmD32CeY72Q5mLCan2/r2vsODqEMjWM7ddvdhC0TYK/qN+y6G4Z1ZzagUtk3ui6aW5hnmMFPPO/fpap85+DxPuB8Pdc0DKQssSj3vMmkhduu+y9uB37OPVQbx5ZImnNMSrVNEpByPokQMdX6McTcFdLrE70W724cjn4HoY1pq/ODK3LQKftPTZKrtXDsZxV9fG9hYquwtBv9gc5kjV53mf/hMaLnk2jFMiMDonmCV4MlyloCEAkll0e89bXkoJy8zsvx+2c5cC8hyS1y1pnt6XaCb2OnwZbq/lkcqGXsa+pI67dE3hnS0GV5d8Lg0GHWS8FnvrWsCf4so3CulnXGlpcp9YPhs0JXW8Uzvhxrmg38PS/+Si7n38BdAtiwwgayO4H7QJKxFnAxYBlJRSBR7/1zXhv3P7Oj+LynJnkmCo9ThPRrE8bOI713ywYs7qOTPRkgx6LbVAwAzYsrelG1ttvRFgHZwJvh/D3fP7euotVwl+KUYrRFqUTlSf9lz/j5PNeAKURG3lFJaLge61jJ/DhXCClRMw8DWtVZFuaeMyb34SbV+748Ecw16QpvkdTUOHjImiajBt2GNQ5aijiqoK7ai/JMphiAvFZaxcIUhehiSWcGdGo6LdHjUhaBaYcR0DHEf7BgHQ9KqhecMmW+PZLymBFgQw1SM7ISJjz3OkBujz87/6ak59xDlhJyjwBIViAik3kRJf/Cgn19RQEqW7YmcMMznrPW+kGxx2XfgybbnDKUaWFcUQSWRKcqwdu9MpVnPSayHFWsftHZOi7GbbWa0QpQ/5a1wWlribgy31/6V+n1/y6Tss6rN5+M92cSUhRhq5j1L4SmVRzm02qKlATKJMjU0VF8ZR0fcZdGZS0+ZUvfJZH9/j56yFwqgN3YvcX4r/VDc9j3/V6Hv+aP+An8/fxiz1CKdOEBOKNpFVuoBcVmirUf1bvKLzb0LsZx3pvVPhtzN9edyS37SjOiPOd1sk0WbmgCoSQ07FI0wq60uKHWcEhLXni5/+M/Re+AGwpP3/9Ixwvfv6tVAIzI3QdZ77z9cRg3HTZFaTZGR5ytEmRCjou0rQANeYuKeIwFEHxlGJr9gOt5tev119fDdIced4Y/xbur4Lvlp2/sqtY+unu1J9dvBh7FKnAU1KEae4M8xxNxlP/y6fZ+6IXgELEcLXwpzKRB1/HzIrczykjRAQaHqfwiHe+mRgjP7zsg7B7F6cdbrBXjWO9ZJFUM8gyrWBSS1qjwDK2cK8HKVq3soy4pixTnnB/E/xJzrcBmnpfo7hSu0AzQVigyV1Zh2MGT/yrj3PSi15An+dogIZ8O/j5t/poAy1XzVE7+z1vw0S47YoPIntm2X+f4c1RWsIUcI7SWeWeG9UYDNkiAZbJfPxRrs7ymGQwGbaCG3SrhX4zrusmEP7eb7l6IVz94tQJ7aT8tOU4rJGn/vWn2f/C59OlS6PMIKvm8Pp6KkuPkz+miA+BfwlHK9UW0SfEMh59yVtxwG2Xfxi3czcHjpUErSbBm0GSaoSrq9dxGFRqG9gaV8CqoxRsHdcdFP7UK4VYTTBPBNx+GCI0yZWWb7PJQeGh9MYPfUZJmyf/1afZ/cLzETEaZTWHN+sqPl/0/Xv8/D6E48bPv9VH6UuyIoNcSRQEy3nMu99GXgrf+/An8Lln2hyN6HApoWr1zGOQZa2r4wq+jimkm6VE43zPDRPWzazz2ci1xGyJ8E96TbfsoZNA4YwOitk8/+qzH+ekC16A1BVSliWa6qGRr8rPX3m/93/euEBGzCMkRyl51ThfJM689A2c+ZpX8TPpcMQb5gWH4A2CODIcfkABVgiduaGMDNst/CqroECq6qweW5/MEDk+jelr5RHcGFBe6aqUfc+96T2YQyq/zyulesCRRGloifiMHwUo4xxn/u3f8KBnnAOxJHkP4hDL+lojrMLPz2ZZf13SwJ0A1xt+kSJssatV9cxX8xGaVXCD5VVn4OMueQsO5dYPfAqmAjtjZEoTpYFGj2aOXk+QGJgXSle9u5ASTiH58YLkSX37jbhOg7xAhqEO3TQ8b1JrPyyPICOEf/DIBoR/qVgZSYxuFPKkqBkZkcI1apy/y6/99fbj/IZj3sp+/5GnpikXsG2KMwbzBI9+zxv4lf/0e9zZPsTRXQHcDhbU8Lnhyiprrq7Kr6gqErWqp3JVX/U4MKZu0JBMGjcM2422LQ/QO9+t06g6HUB6ZCVXRZZldFKkZV0KWvyoUXAwFTz5rz7N/gufD8a24vySjOAzVBXn6tIT8dseX/R2Zl8EHnLJGykyzy2XXUa5YyenHJmiGwvMRcR5tFYYp+C1QsYSRnIbqy8eN4m1lvCbLDWOg/kgGZCUsN2Cv5EXpVhf+PuKVLtDURNOC8owzZ3ZAnNqnPPXn2b3hS/AEkTR7cX5DXyPhzoaEqodCxzqFL+NyTYRIeVVnuBR73gj3RS584oP4HfsYt9czpQZCwpJFvsJxNVjh6h5WFfR4vXCmeP49yN3CAZdoB414hYJvlvl/M2SrcrVWdT0/keqBFjLwbw0uT20WTD41b/6GPsueCFSD37GbS/Ob0GQVA3FQ6AoCnKX13MN3LbXGnWAaYkkFR73nrcSxHHT5Vfgdu6gcximVFAS0ao1E7dYO+RkMQGzXh9/0phh5A4x9HsjeoKPR7JqI/7fYuBb17HXOH8SmC8jP5nOOBwjT/urT7P/whdSWJe8DJAnUsm24vySqjzDvHbJfIb3AVBUIUfY7nTzdFnV/eATYp6z3/1mVJVbPvgRTtm5k3w+YUlwNX16wlCpuFS9CVE21lAzqW8/bnn2YiA8xAVanORi6yJqs3Xi9es93MCjODMiSmFG4eCHLU+iy1P++jPseMH5gJKnBp08kheGzxarWbYF5zdHAhoukFB8WylajswFJLLtM+gsK5GU4QySdPHa4LGXvBlBueWKTyJZYCp4WhEkxSqx5KTaBJIO7ZqbxM2ZJECeXPiHeCongrWfaCyeDuQJ1FBVoia6KTKfCpIu8Ct//gn2X/CCKnFjDkKkmRyWZ9uP83toEwlRSHcf5K6//L/JcUTSCVFrXZIhPkJ0RBqUUlWEPua9b+DM1/wuv7Q28z20LFWl5yKC1C7kZvr4myH8q53jqgKZKvhyfXx6vHr9wc8g77MbR5B1daaGwYDW6SLiU9GXSAWfS4mVCdqJslNSlG2Cwk+bOT9rGmf8n/+FU19wLiGW+FCrvoX+7C+hahrp/eISytRNkP8uZR2pwELv9XeVVHnOJOsyYwEt5vnn//W3+Nar/j0/eNelZNExL5CsgFQLTipIaDXZsYTucdhf856DkEEDyMRh3mOW8/j3vJ2H/8f/yC9UuXvGkxy0rHKBNAoxc0sYxkWkiguAKEbpbFUiq9Xg0UGGiepjS6Y+6go/39UEWHW7j/TafhYTdUmWbbSbOfVpFIYhfnEw9pJWxcFEV53UclTpd5dSVZeuRkeVBokuDgmJZmxw84xjznd52l98hv09nN8UV+P8fXjTYhUIb+HRwEPpEIGpIJglUgNC8hV4HgLdhXv5+m/8Hgev+WdOOvBgvv/299Mk44w3/0dIOWVuBIwkFSu1SYTM0SjdtswnGExePvz9b6QRMr552WWk3TOcemQa32mTNROp8EQX64Zr6fcSiAhBXCXgQzwGnag5Xta1QyzfkdaNAm3USKYBOLQn9CLSx26D9fgbtWrRU0OSVnX+QO48bVVC6JLbDLfsUuZSyZNOEJw/pgphiKFa7oARcBSaIPP4Y0f41stfxZHrr+ORYTfTv+zidrb41iXvI8bIGe96AxJLxFXCHx0E04qTKHPbFiL0lCCLjjPe+Tqig+9fdjk263jQ0SaxKIi+xJAKURsU9rqOyDF+CcRmllIPC+2CrToNcmvgyzTE2vcE37QKvqsFTiSsGuhQu1lWu0AigmrCU5Axy83TBUdEeNp//QwnveD5dA28bS/On7wQfEW5Ls6hFnAKeabEw4e59t/9LgvX3cipzjHVOUoZHA85kuF2N/mn919O15Sz3v2aRUzClCg5warxr9sZKIhINVsswlnvfCMpJW7/wAewPbs4cDhnWrrM1bGZF8GcI9VUVFLT0KiTiYVYR35/jakwA8omA5XC/o8f8binxltvf75rtHA2EL5sYlA8mGJ3tT/oneB6N6OGpYSpYimiqWKZM6tq8x2VO9RDfdSMZhDEmtzRihzOHb/++Y9z0gsvABLBKmjT9SFSQ+ref3F1o8wWB7reQLUkc+DFV4zwDmLnPv7x5b/H/Je+wqmtBifNlyy4gmkLLJhnZwfcTsfPvnw96hz7n/VkRD0+VUmzBASq4dbbeRQG3pWQYP95zyYVyo++8hVsV8DNa8U1pLXxqqax9PsznMjwCS6Dn2WCb6z1/ZWjeG117a3ksSjwZz74K/6PH/G4p6bb7ni+a7QqoWNzaLKk/qHex1XMk/3/mWr1iUqKlW9vvQUzMKrBzdTx62BwbCIkFX66I+OXvuQ5n/80+y68gNJ1cSlDvJKi1oGX9IU/prioCFusABKrZJcTwQqDTJjrHuTbF/4uh669igft3MmDjxQUlATL6TqHE4ezyFQX8pmcm6+8CjD2P+fXq8I8AXEK4re9HtUnSE7AJxye/c9+BrJQ8rOrryfbOVsN4kZwJkgvsJUBAVwuxKuggqsKfn/+tIwNvVfWv1JGLbpkZzykUgC77Y7nu7zVnyO1Odvk4qc/AaYWck0JUsKS9hWB2lJIT3nqxuvKWltdMyaUXigyxw9bOYeakad97tPsetHzCc7wZERvEK1KaNULnWLEeYc418f5t5rXs3AlmVZjRX0GsX0f37nolczfcAOnTbWYPdRmQQTzGcEcRe7IopKhqPfkhTK9d5qbv/gVVIV9z/l1NCWiCEEd2z3KuHBKFh3iPckKnHn2P+9ZhLkud173NbIA4hxBHJhWeFhP8NVW8LGOcndshaKsLfjLaRD7cYarFNCKolKA1zz8cU9Lt91xPnkTUd3EBNAiya2poTGRUsJSQlNNvGU9Ha7+N6jTvSmRFYRW8dUk52gHaAc42Iic85lPsvfFFxBVCeIRiTgVNPiq9dFiNfLVS7+4ofd7W25CxXDJkTJBjh7jH176Sg5f9xVObjaYOlpA5gkpoHhUElMx0XaVgpuUmDmaC4mZmRY/vep6YjT2Pu9Z5OpIrsDJ9iYLPIJ5RcpqiqaJ4ZKx4/lPIy10+MUNN+C9r+DTVDGOOu9wzkHUVRVg0eq7pT6RDPeRVotgezDpCrhVlinAGx7++HPad952fpY1Kk5/AbGBUWKyRjA7YObd4NZmhisixAQxYbHy76WP/FTCbsu02wY4XFQSDihCXjE00CF3DQ42mtzVKDjrb/8Lp5/7bJwp4h3OVY5Sz8ev0CU3KPKbjPMngjlMoI2RIVAoyScUJRSBdm405w/y31/+/2buqmt40MwsraMlTnKwWO9uWmPSFUOzMxBz5BgWPM0CdLbBndd+lVjOsee555B1GpRZiU++etCYiM5RACFBdLGf1dlSHaeam+brd2pSZZNOet6zoPDcddWVNGdymmUThzI3VSBRyS0jukVqSqlzPrWHhOHWjAmGCb4uyxGsGpf2ZKwoyB92+pX+dQ9//DndO24/P2RNMOsTxVkval/FVQji6uaTOhObKkFPZcTSACdPH0Va/N/yBxj2nLkElAxPSYaCm+aW2ZJugif/H5/hoc96BmSeEqv8Y9XqXmvLv9VQZzAHsXpxwQlYIgWthmurI2XgOoe58eWv5p6rruUhUzO4o23IPVnsYKsAmb0X3xXDpSqhM9OONHfN8PMvfZ3ULdj1gqeQd3PKrAqMSxwZ1RxjcQmfsm1xkQZlZc9znoDXwJ3XfpXQMmZiE9c1gjOSDxUTHUvHl9qg8E9wjJ0/qHcd51ylAGc+5Cq3dPFl6X8714cdBx/OzCBGrCz7H2KEGHGqiCbUNjbrt2vgKYla4i3npzuUY5Jz1t99jAPnvQDyjNJ0Jc5vHB+cv85kx1BRqAiGx9OxRAwQjt3D11/2ag5++UpOn26yp63sIsOVibwuvluZ3Vy8flMygoR6Oruy71CH2Z2BH132Sb7/lncvmWOc11lNXyYUobuN3lGfdkZaPOJdr+Gs1/4BPz82z30zRjNr0kbwYiuUXh2orC38g4O5VNbXVFOV0Q8YslV9vAF3xrTO1urAwAldFPDesIFFP971u+6HP4iNRGEikYb3NJo7uVU6zEXh1/7mozz4hRdWzGVmJx7Ob9AMSnnkMDf8+9/nni9ezcOmp9h1pMMCRiYZDYP2KjHIoAEqxRCLpDwnV2GeyEMOG4f2THHb5Z+iKVM86t2v7cdSmJKyxgkxx1hEaCbAHGe96y2Y5tx6+RV0drbYdyRDNULNSdqfzmPDk1XrNaG2BrIkLB2Qof2ElNU/qYZp6gu72KICDIrwEl9wSfS+tvAPS7otr9dukViwjLtCh6NOeMZffor9F7yIblnQ8Pm21/PnBklLctcLYCB60M5BvvGK3+fgVVfysNYO9h5LdJxjFuGIlkizhe8aSBp4USvvtkwlWchAIwvOMaWB0kpm7+tw2u6Z/hzjs9/8n0EDLlXVHSfKHGNztVsbHWdf8jpM29x5+Sco9+wiHC4qBbCauJbBCe4Dw0smdHdGNsYPrHRP3vzrH/a4pxW333a+czkaI2aKJasCWO1Z+gqmdP0AVgZw3clszWAcsCTjPOD/KpDT4KbZgrnQ4Omf/xj7L7oACkVycCmRkp0AOL/hxGGFoZmw0DnIty78XQ5dezVnTM/SaEe8GvMWKTLHjtDAuh0Kl+HEBpCNlceUC5QoLlaszRo8mJAc7OiCnwnc8uVrCKrse+5TwQVwlZsrbvtZK1RqV7SuZDzpec+mPd/m5mu+Qmu61S95cTZIWLsYCy4vlVhrCqTJ6A1PB+MAEazbJXvY6V/2r33Y2U/v3HbreSJ5BVHWNTfLLXYPRzH6ofrQAH15vDBaIQa2upq0ChF+Np3T2THF0z7zEfZdeCEpRXwmWHJIEJzz24zzd8k0X8T5O/fx7Yteydz1X+NBUw2mj3TpOkO9kKtQeocrEuYDDRk9gKItymyshnl7BCkSHkHNSAFahTC1Z4of/P3VqCkHnvNUYhmJ7sTIE7iUMBxRFB8jahl7zn86Mwg//od/Ius5y/2WvgHsXkYL/SjBV4YP8+3DoN0u+cNOv9L1Cs7QRajSrWGxqxJUq0sTlmVoMdS04pgZ4fYwJOPc94Gd8BN3hNlnPJedF11Ax5SQPF3AO0NrpGA76/m9+KpyPDj06Bz/ePGrOXjdtRxoOFqHS0wyppKRlx18EFoFtPMGyZQu7ZH+Q5DAIReZwlOS0FBVc6cMcq0Gms/eV3Lm7Cx3vfej3PzWy8myjEbyFL7Y9h0AZ3QdZMmDb+C0Q8MCp//Rq2jsOYXCVwV+vRqengeQ3Hh+/yh3Z5gBHHaO/+PTH/WsdNcPn+ek0Z8Q7Nbw4vtb1BANWw51jhLIFefXPqFTI2s0OPa97zI3P8eDznsmSCKYYeKrngDA40i9Mou6dr7KPEdwGzOB3bqCMyF0gMyAMpJ8VS/lS6Xwnnx+juv/7W9x+KorOb25h9ljEckiZlUNurqAUvUweKug2sFOVB2yliYVCO3FUfYa52tD0vOZvSViEJqFw2ZyfnjNV9HuHHvOPYesW024zLTqN67u21ECIUJ0JRvtOjYtEBHaQJZ6tf7UHWQgUhKoOuxSgpgF/MGD3PDi/0D7zu9zUuEIdYWAifWtv7A+q2+DpQejhN8JVpQ0Tz/9mhOW53v/scRev5ObL/1TvvmGN2M+gGZQJ4yq+nitOXy0r51zGPiNFws3SFA4fAlTKCYFMQ/4GJBodLMM6R7k+pf979zz367loa3d5HPzdBtCtxzNK7QRfn2AlHuCQkTZd7TkwTt38qNLP8dNb7qc2OjSKnK6dbIp+YBXCEQIkRA33kwgLkBHaZn0MmGolaARb6m3ghDBZdA5+FNueNn/zqGv3cBubS2ByHUdlIbDklyDH13NhC+Lj05YBXAGpy50ePjuWX76kc9x52vfw3wG0gFSSVcSksCLY54EXnFJmbLNcX9iqqxnzKppKGKBQKSwRAwed+Qw33j5Kznylb/nzJlZds0rLVOs7NLKdm7Muq6BgPQ+rjRCcoQacp4+vIDfAT+67JPc8tb3Qq74Ok/gqVwLH5UEFJvQTFDgoBEoBDqxhJTIpVYGqZikw3xJkSudw7/glt/8I+75h+s4bdcMzaKYCMcfFgv0Yk1bhuszLLk25NxeZap/zRmPfkZ5113Pc5L3vXUH2z4HXbyj0A472h5p5vzgxuuZPdJl94ueAUkJLmNBjEyrrHEhhnc1uljl5DemAE7wXolWgAREpUp2BcUOz/ONf/+H3PPF/8ZDplvsOKYsqCLBCAlK84vwxhpJnaFb+xC2s6HBYObRGNHg8SK0vbKvK4TdLW676jpcJ7LvvKf1XZ2Egg94repk3AbjJJ8WzWdeux2FCKnOBglCCh6bu5dvvux3ufv6r7K/NUPWKen6iDc/2gisEeT23OAlRAwD3x+MAQarT0VqBLNb0HjoQ75ywiqAWSSXKTyJBkarNcVN115LWcxx8nnPo2OJlnnMC6KV4ZmXVOHymwADektoguA8XgwxQZ2j7BzhH3/ztzn8pS9yemuWXfNCV4QZyZgjYc284s0cUo+yxN8fUeuiqyAgvat2JBFCVQISHbTwxJRodhN+R4OfXHkdJrD/WU+pTW2V3ExQ+d6bsEZG3abqhFiXxmRJkFRP2zx2N9958av45VevYXbHFHsWMrxvUFIOrVVaS+hXRXVWiwv6fR9Ld4VeSbR2C/IzHnLlCasAVWmz0rGC6eRpdRWb8fz4+m/SmZvnwec9DZKR8IivAu1chGTVNu83CoNGIKtq2a1UNDjmuwf51kW/w6Frrua0mQaNrtCwJqWWlGK0XI4VBWXu6p6G1V/aWkK/ollkyO0FIJqRJUA8yVfwSXKOWfO4VuCmK68hmLL/OU/FuVAlpzYpT1DWqIVzVqFz4vAJurGEXMh+/nO++W9/n59efyWn7drD3oUG82W3QrPwDOJ2NobQjx0Q93D+gTZMlgl/BYMWNM54yFUnrAIUCJ4OSI6TjEjBSamJb0zx46uvQtvKvvOfhLeAmKPtKmEQJ3jchisBCoGg0DUlhEBsH+TbL/5tjt1wPae1Ztl5JLLgoSDRVK2YEEpBM08eU583c6h1k9HWftT6Jx+YiTAfKpZgXxqZOGIq6VqiVTqm9k5x0xevRlKlBCkmCgeZbnx9PL3RqkpCyNRhDopguHt/wT/8L3/Ez6+9llNnp5D5Ek2Kb+UURLxV7qSNcHHWyimt2lE2wq2S2l3ToqD50BNYAZrME/0sQR2Jbg0GekLRprl3B9//yg34hcT+88+hcI6mVttuMF/BcZuR6k0BCw6OHeEfXvo7HLzuBk5uNJk+1iHYFMEUR0EIDkpjoRnwMZFbInq3poszKQKyQgDVMy+JqSxnwSlW5740CE0TIo58ITEz2+RnV90AhXLS855Bpp62L8g2CINGASdVyYmXgJRQhki4725ufMV/5p4bv8yp07tpdowyJDSruvicwpQp5Rq7UI/echimv6qgy+oJ2b7fP5gIO9EVQMkQq8DCXlClKA6h1Y40d03xk6uuYaFInPrcZ2CWyMxQ50g9xjirXpRQBcd1lx6mkSjVHKxePX9AkEJJ3jASvsiImRLa9/K1l/82x669ngdPTTF9uEvupildUZeBeKIJ5gRvVUNRXCMHoatUf47c5pd9klOcq1y+UPdN68AQOHERDY6pQkizOT+67nqsM8fuc8+h0Vk7T1C6hNO4Ns5PWVFKpQAG3UxpHDzKtS/9Dxz9x+t5cJitSlYwnFVJw14PePJuTRx/MLjtz3mrXbdF9GfYjro8UpAlRqZfyeaE2C2ZOvP0L99vB14dOBopp/dw02UfJWmbJ7zvPSStPMugisNhTuvhnnUVk8A8xrQLVWIrVmveClU9f8yren41h+XQ6d7Hd1/2+9xz9Y080rVIh9vE6SZufg785Fj6Ruj+Js4TaMUk0XHCSUcird2z3H7pnxNp8JhLXkOraNDNEzlCclVOARfBC1n0EKTC+RuhT0BR4fwVoYERSE5wmlDv6R78Jd98+e9w7L9/k/3TM1ipqxIjrIbjj3r+Jd8ZORHGrfoOBv/lhN0BRvroZpyaBNkR+Pn1/4TOtdl13rPIIoiPFbFWql7WAonMCRKNzFWKENVwJsSsonb1VMV1RTLIHHH+l3z3JX/Iz6/+Cg9vtZjuKjs0q3ahYJi5JU3bo7jrbQKW5HHH/ay1W2Q+o5kcqNGWRCMp3R2eQ1d+nU55jJPPezYSK3ZqT1Vk58uS5IXSORTBe0chUMaSYDWpXt1kJ+pxhbKQC8WRn3HTS1/NPTdezyk7W7iFtPog+EEqnBrHt1V89dVigNXzJPX1ZHWGiD4U2i2ZOuMhV95vdwBBOSTznHQ4kGZyvveBj+Cd45HvexOUiSxkdERpJphyGVGUkIHEqgozecH7yqqJBKwerJ1nRjx8mP/xb36fn1x3A49qTdM6skApjhgCZVnScmGJEK5nUsmkgj8uB35/B7BEMiXmGbkF5iRy0pyxsG+K2y77JE1r8ahL/njRx7C4pJ8gS4vtjr0GnqKW6lDvqWVT4cghvvkbv8PRr93ISdN7kYV5iiCE5FcI7yCznA30m4yV6d2EEUu9XWGw2u1+tQMMWjgTpWEtzBk7ouKmmvzwq1/laPsgp51/Hh1TmniSrwIvByxItROYq8Z9mpZkIng8ooI66fP2/PLqq3iM38HMfMl8MHaEjKNakrucMoSql3dZQDappV8rsFstVhgXOuxKwoUAphQIUzjKlGi2lbCjyU+vvA4TYd8znwxaYffOCSWQqdW++Oo4fyck9OhRvnPxf+Ce629kdvcudrYVL9NEOvS46wYteTIbG8fv6yXry5MsXT+3qHAs5gGmTuQ8wFrJEQWc5ESJiJYkPLNdRXY2+NnV/8h8t81Dzn06JKOs8wQOIROhpMoThCR1PT9YVMgcc92DfOuiup5/dgfFQoeggldl3sMe12A+RUw8fiABP+plrEfwJ8mODsPLsxCwZORlRbOiWSBTTxIjT5BN5fzgyq/g6zyB+FAl0KUi3YqyNs4ffvlL/vnf/D5333g1p+3Zy66FjIOdeRAjuLDEfVEqMrNJcPxVrf2Qfx++m7pVIbieAjTPPIEVwIYI/eBLNoRo82jWoqUZBR32pgbM7OCXX/4KqZ3Yd/6TCAScObpiBAXnpOLhcSWZZpQKPhNi+z6+ffErOXbD1zh5pkXjSEGyhA+umodlRoqJVshILvWzubZJE1B0woaPUUmipNBUYb4hNNSRdRN4TxTFnNAohdbeKW7++6sxVU56zlPQqJRSQcle1sb5v/Zv/pBfXPslTp7ZiSx0KcuSrDlLzAocjRqzWyr4a+VC1qztHztPsij0a7lTIkIqClon9A6wTPiXL1STNlGmaaRAx3VAApqMqW6B3zPNLVfdiGvXeQJxNEwoveHVE12FYVcszQJ93p5r2N9qMH20wKXAdPCodusmG09sNChTl8y66DrZpVcv553M9x31fpxktC0y5QNzrioPzxAKSXgzFE/eTuyYmeKnV30V6Rr7z30mIQU6vm5ZXAPnP/Tfr+KUmZMIZUERqoEYvhr5QSjbRFl9UuRQRGgD02GGXWRtBfBo0aV1IgTBNm4rW+/me1T7NPEo0Wm/4MtqUvrdhwts1zTf/sgVUC7w6A+8l7ZW9TJdZzRIaBmwDMLcPdz4ildx5NrrODVv0Doa0ayJk4KuAVI13qszXCrAeeKQJNKkAyB0ZNZyeGO7Ll8IG6AZHAjujETwnkKNBr6OCxRf/zdSUARPo2PM7pziO5d/CPMlD3/nG2i2G8SWgSmx5jX1DdD77uPrL/195v/hak7Nd0JZoHiy5DGzqvzZSsranVoL8hw+G0CXWfPJYc5RwEOlfEqd2ti+QTyTCv545w/w0hw1wvRJ3PSxz9AN8LjL3gul4J1B4dAc4sLdfO03fp+fffWrPCqfRtodXLNJuXAMFxobytZuvCpb1l6TCX5gmHKW4siioB72HlPyHbPc8b7PUHSFs9//eswaEIVAgWUNjh4+xndf+tvc/c83cmD6ZKxYWBXDX1fiUyZ/ho08/3hqdlwzv+OPSFr57mXF9qepZNfRglN3TPHjD32Cm//o7cSsJEQl5Y5w7G6++eI/4L7rr+MR09M0usrulNEtC2Yb+dj16quiGuvE8YfFFWutSQ/lWGr91yjCq48WGTnV4JASZXquIOwN/OiKT3HTmy8hMyNpSZE1aB+8l+9c8K+5+x+/xoFsN63OsUVLvszHH9W8PvT5ZFAce5NcRj/DOO/F1vh9284dQDdsUWXNf28Gx5xXTrkXZPdO7vyzj9NuJJ5w6Tvxx45y47/9A+659loe0mwwfWie6D3zWUYoI/PBr16EtU6rPymOryPs1XpQp8H7SBLxCpZ7vBOOSZeTjzrm981w8wc+TUtyHnzJW7FDh/n2S/83Dn7rHzh5dh/+WIfoqpbPcZR9UmNgG44DRoMNg6/2uCiAbUEJwKjvHbKq1uXYVJNdxzr4ZsZPP/g5cmmwcPsPufv/+XsetmMvM0cLFjLPTtfg3tQmm24SOoJKHFuo1jPlcFLBtwmaa8a5jwKl1cwgRjrB0YoZ3VTSOqjs2TnF997/SeaJHLnpVn557XU86KS9pLmjuKyBxCbqdNXnGkx4rTdxtV4ffy3BHzSbutUxwHpG24y+5viU2vtSYI5AiAUL1uHA3C46uzNu/uif0mo0OH3HDrJOQZk5vMLdvsusBLQdKV0YyzecVPA3U+g3ai1bEkhmuKgVmVhwCA063phdUBp7dnHHBz9EK29x2r4DMD9PLp7CHEF0zedaLvyT+vc6hrLLmjmANXaYZSjccdkBhvVsrjZHWWxYrfdkXPIA7SCoJbwlvGsw5+C0ox38TEaunt3zxn25kiWlIRVW3lWl0cjJO11i8Jvi4qzXGNgmCs2wa7ZTyS7LOJIZmTNcNxFcBk4pAnQ797Fr18k02m1s/hgNpihzILUx3xw5QGg95SCTWP1J465V1kPd8RT+UURHyxG+oQ9mvTGXVYizmiAVYnSzLkbGbDmNWIe5cIx9bc/sUaNjwnQSgpXkCs0ykBpN5ssOkuumoTq2yRRF1VO7Dd9Hi4xjQWngKZwRc1BvJG+EMrInzbDj0DwdFwg2g3Ng2iU3twyu3HxkZiPPNilhbljRMT/iznpLn9Zh1WSgsGMYh7uNQ+BfL76OCIKdQTPmGIlOaKMieG1RYjXJT+8t5LQ9QCQrAQmUusGKzAmDfhti+WyEWdkolUj09cwxEbKaxaU0RZJg4mlLlfNoFQkNiQ5UyUBXTXpcdT3MrfrEtgEff5TF16HBrvRn3VWs0FbPh3aIN5zkk7tAW6v7siVWVmVrLdBGhH891lE3iLys5a9v+LrL44NVUKzNdHUmRYnGokffihe8WvJqnIXVTRD6DT/nBFZ/9Jq4ifuANyL4g7CuriL4k5ZjjG9c3MTvZVKLP/SepSJL6GWepZ7H44cpgNbtboNc7av6WZsgTOsJbMfS7nVmFDdD6De0Hlts8XXi97OxXVU3OKTENqmP2npzl1aLASaxtpsj+MfHKq9X4DfPxx8zUbTOlztxKckYLYfrXedJ3Ekb4uxOksAbtSb9jjPWzkeMdIFsE+tfbMzpdLqJLs56fPzNDWwnvz/dYlLrzRT61VydcRNdk+L34wi+LHs34/BehOUX0ppLcqPW3mT84FY3KKDjCv0kL3qjxXqj789NjOqMLjuQsX37zTUwbtPLFcY6nEwMnbhlK7+uRJgbSwDWsSJiQ89fb4/tZli5E8HHH1f4B4PdQVqRYYGwrRMHs7ERHR3re+u1+qOoHceNQIKIqGJ4J6RoAzwsy0Tc3FIUYc3tVCa2mKuet4kdV6O/v1IBVw8e3QZdHN0Uge8L+3KBH4pM2dhKaL33b26daI4buhYbMUSDJLdjKevyuKcevD45DGpuTOu3eTi+bvOYK+XEO5Za8JUveTJWBV3bTprbMAozbrnCuMJvWyATweq61hUwp7kxrdLaN6abiARtFNkZfY5MsFuNf1/DXrBMSAuyAiQYct6KWc4jr6sr4Mq1hllNFJhu0MAtt/Zb1aIbRtmCzcTxx8KrZXOFfrOSV5sh9MO2ZxtBILUkaTVCqHtb/HpcwY0mrzYrYzupm7NhBdABl6hXrDasInOUtV8vJLhZPv5GXCfdJB9/IvdglV1B13BxYDSU2Q9uRwq+G/oc67XyG30Pg1NbjucRxtkyV7P263EVjmeJwkjlm/D7WyH0vftYzog8yoXpFXeNpeBjdV1tbnHaJDHNdgj+yCB4NVRmvcK/EShz6zuuVrIqTIborMMQrDMeWHSNbIOC79b8joy5DuvF8LcqqN00BVgvjm/rPH8zA9vNRHS2yjKNYkteLY2/ZPLJUPBGhrpO4yvI8HJm2yTB39COYZsrFwChdJlOk9HxRnSeoqvsc475ZQswXjHV+MmrDRelDaBUNqQ/VVcPZNbl40/surnhAqwDK7Vm1nak76/L7sstOW8Yjj/o5rh6JvPgNQb3lo0GtSOJsDbRBV7VrTTDe4+liqpd6nciQFKPuKTOWXSHaVOmgk6Ck7ImR7UY62bsOGVRh5sDXfwME41NvLl18QINFe6BAQ9rCP84ArO088kN2wrGyt/ohCXZ63F1tvPok3DVwzV68dZ0gkKTC+aJs5IRnSdog0NxHsszfLJ1BYrrQXYmrjdfBanazD5b3dRiQBmhGKsrz+qW102we7l1C/5G4cztOlx1E33uIufcEh6jMiRmJGgI0zsX2nQQncGVBWWA1joE/3jg97YWJ/9YvzNp6+HmCP0wwV8Pn84kJcnjojq2Ce940O07kY7lO20a+O8jruCU1kwnhIc+7K7ogJBDOcesNSljAeI2hsJs1Mcf96WM9V03tsCv9z56C66ruEIbIZAav6Vza1gV7m+C31v3wWk0PeF39fy2mTLgHn7GbS684OlfzNLUlaIJyQJJFXWyqp+/Vtf9WoOM17rWKKrA5QK/GoXiIpS59DOKYm8997H8nmyZ8JsZWo8CNRnuHoyiEhx2T8Ofo3rOkc/B6nSBw35z6O/3vt8bSH4CCv8S/98WsS0vQnAOjZFG2bhy17lPv9LtOe+ZKr/y6/+cugdRPGVTiKxP8McR+rF3kAn4Mav+2tX5Mdcj9KMEf7kS9ha7/9kgjr8ewV/L2tuIZ1t+jdUEf63ZvSfq0XOFXD1vNHUKsqc94caTz3tuxQv0oEtf83qjc6WEFqGIBDbGPLxRn3p8AKe28BO/DzfWfYxa1HGEYfn3qrmVtup5KpDGWtXJC9Q2KkTjCv6JUEnrVhlV6wzKbvHFh7/xP3yov4rZ+c/WU9992VsX2j+90sICLuQkDHPSRxwyAp56UgqKiVXcPvXHln2WcxSrLP2MEvxRbNHV3KfxgumVH8Vwa37Eqg8DHxPf/4hK/7PC5RJHlWMMmPn+BwJi9Uf8ogUf+GDVQPDlK7h4767+jGf1bciObEPWhDV2cJOVO5wOeUeD70q3+WPaxrLK98+7SpmB9565e49e+ah3/Md3n3TByw4DyGCUfM/lH3/C/Os+eOkxd5/mbur8FIwUamFLWo/OMUQMUr4mErNap08f6zYdDgGOadF0gk6joVZJ0kTnr0YgtjpbhowVOPaEabhbx9KE33oK/VZBznQMi7+RHXK7D02ObhB2SU47HoOjx/5/ITXcI9/5+ref+qY//PriexyCS99x4Sv+yP7+xueXsR0i5bmVJTEcHieCWaonQK2OUIyqJVHTEe7U2i3NaQLnaz2IyWDX29DaexuhMGajr7/szpb+KRvhWox6fr/qt3oWfS2BH0V/6E7wQdJ5aLFQzuEy+6LM7jy651nPvfaJ/9cXPr5C0dd6UT//P/5un9z8w0e2urEZHdpRdV6dy1Vc9DGuJQBJhhuZ3vdc/YJX+3VxSTdLAYYrxXjNcCqTu7TGGJVQMrxmp2dQBgVM108ls253XG3tNLKI6ImsAC5qsFajs+uhZ9y199+++O7Vvvf/HwA9D+v/tNjSsAAAAABJRU5ErkJggg==' height='120px' width='120px'><h1 class=' mt-5 text-wrap'>Invalid Token</h1><p class='mt-3' style='font-size: 19px;' class='text-nowrap bd-highlight'>Please check your email or contact administrator</p></div></body></html>";

                #endregion
            }
            return new ContentResult()
            {
                Content = content,
                ContentType = "text/html",
            };
        }


        [HttpPost]
        [Route("en/job_seeker/ajax_job_seeker_detail")]
        public async Task<object> ajax_job_seeker_detail([FromBody] JobSeekerDetailRequestDTO model)
        {
            long job_seeker_id_enc = model.job_seeker_id_enc; string token = model.token;
            string lan = "en";
            JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
            bool isSuccess = true;
            try
            {

                ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode = 200 };
                JobSeekerDetailsResponseDTO JobSeekerDetailsResponseDTOObj = new JobSeekerDetailsResponseDTO();

                JobSeekerSubDetailsResponseDTO JobSeekerSubDetailsResponseDTOObj = new JobSeekerSubDetailsResponseDTO();

                JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
                Session sessionObj = await sessionService.GetByToken(model.token);

                string hostUrl = config["Utility:APIBaseURL"];

                JobSeeker jobSeekerObj = await jobSeekerService.GetById(job_seeker_id_enc);
                JobSeekerPersonalInformation jobSeekerPersonalInformationObj = await jobSeekerPersonalInformationService.GetByJobSeekerId(job_seeker_id_enc);
                JobSeekerQualification jobSeekerQualificationObj = await jobSeekerQualificationService.GetByJobSeekerId(job_seeker_id_enc);
                JobSeekerWorkExperience jobSeekerWorkExperienceObj = await jobSeekerWorkExperienceService.GetByJobSeekerId(job_seeker_id_enc);
                List<JobSeekerWorkExperienceDetailsArrResponseDTO> work_experienceList = new List<JobSeekerWorkExperienceDetailsArrResponseDTO>();
                AttachmentJobSeekerResponseDTO attachmentJobSeekerResponseDTO = new AttachmentJobSeekerResponseDTO();

                string country_code = jobSeekerObj.CountryCode;
                CountryDetails countryObj = countryDetailsService.GetByCode(country_code);
                if (countryObj != null)
                {
                    JobSeekerDetailsResponseDTOObj.country_name = countryObj.Name;
                }

                //JobSeekerWorkExperienceDetailsArrResponseDTO jobSeekerWorkExperienceDtoObj = mapper.Map<JobSeekerWorkExperienceDetailsArrResponseDTO>(jobSeekerWorkExperienceObj);
                #region job detail
                if (jobSeekerWorkExperienceObj != null)
                {
                    JobSeekerWorkExperienceDetailsArrResponseDTO jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();

                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj.Company1;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj.JobTitle1;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj.CountryId1;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj.City1;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj.TimePeriodFrom1;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj.TimePeriodTo1;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj.ReasonForLeaving1;
                    work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);

                    jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj.Company2;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj.JobTitle2;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj.CountryId2;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj.City2;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj.TimePeriodFrom2;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj.TimePeriodTo2;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj.ReasonForLeaving2;

                    work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj.Company3;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj.JobTitle3;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj.CountryId3;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj.City3;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj.TimePeriodFrom3;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj.TimePeriodTo3;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj.ReasonForLeaving3;
                    work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);

                    JobSeekerDetailsResponseDTOObj.work_experience = work_experienceList;
                }
                #endregion
                JobSeekerSubDetailsResponseDTOObj = mapper.Map<JobSeekerSubDetailsResponseDTO>(jobSeekerObj);
                if (JobSeekerSubDetailsResponseDTOObj != null)
                {
                    JobSeekerSubDetailsResponseDTOObj.last_login = Convert.ToString(sessionObj.CreatedOn);
                    JobSeekerDetailsResponseDTOObj.basic_information = JobSeekerSubDetailsResponseDTOObj;
                    JobSeekerDetailsResponseDTOObj.personal_information = jobSeekerPersonalInformationObj;
                    JobSeekerDetailsResponseDTOObj.qualification = jobSeekerQualificationObj;
                    JobSeekerDetailsResponseDTOObj.attachmentJobseeker = attachmentJobSeekerResponseDTO;
                }
                #region attachment

                #region resume Url
                if (jobSeekerPersonalInformationObj != null)
                {
                    string UploadedFrom = string.Empty;
                    JobSeekerAttachment RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj.JobSeekerId, "resume");
                    string fileNameUpdated = "", Path = "";
                    if (RecruiterAttachmentObj != null)
                    {
                        fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated;
                        if (RecruiterAttachmentObj != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
                    }
                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.resume = Path;
                    #endregion

                    #region id_card Url
                    UploadedFrom = string.Empty;
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj.JobSeekerId, "id_card");
                    fileNameUpdated = "";
                    Path = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
                    if (RecruiterAttachmentObj != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.id_card = Path;
                    #endregion

                    #region passport Url
                    UploadedFrom = string.Empty;
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj.JobSeekerId, "passport");
                    fileNameUpdated = ""; Path = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
                    if (RecruiterAttachmentObj != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.passport = Path;
                    #endregion

                    #region video_resume Url
                    UploadedFrom = string.Empty;
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj.JobSeekerId, "video_resume");
                    fileNameUpdated = ""; Path = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
                    if (RecruiterAttachmentObj != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.video_resume = Path;
                    #endregion


                    #region picture Url
                    UploadedFrom = string.Empty;
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj.JobSeekerId, "picture");
                    fileNameUpdated = "";
                    Path = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
                    if (RecruiterAttachmentObj != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.picture = Path;
                    #endregion

                }

                #region resume picture Url
                var encryptedJobSeekerId = UtilityHelper.HashEncode(job_seeker_id_enc);
                attachmentJobSeekerResponseDTO.profile_url = config["Utility:ClientBaseURL"].ToString() + "JobSeekerResume/" + encryptedJobSeekerId;
                attachmentJobSeekerResponseDTO.isActivePicture = jobSeekerObj.IsPictureApproved;
                attachmentJobSeekerResponseDTO.isActiveVideo = jobSeekerObj.IsVideoApproved;
                #endregion

                #endregion


                JsonArrLoginResponseDTOObj.code = "1";
                JsonArrLoginResponseDTOObj.MSG = "success";
                JsonArrLoginResponseDTOObj.data = JobSeekerDetailsResponseDTOObj;
            }
            catch (Exception ex)
            {

                JsonArrLoginResponseDTOObj.code = "0";
                JsonArrLoginResponseDTOObj.MSG = "Unsuccessfull";
                JsonArrLoginResponseDTOObj.data = ex.Message;
            }
            return JsonArrLoginResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/ajax_my_job_applications_mobile")]
        public async Task<object> ajax_my_job_applications_mobile([FromBody] MyJobApplicationsRequestDTO model)
        {
            string lan = "en";

            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode = 200 };
            List<RecruiterPostedJobsAndApplicantsResponse2DTO> RecruiterPostedJobsAndApplicantsResponse2DTOList = new List<RecruiterPostedJobsAndApplicantsResponse2DTO>();

            //int offset = Convert.ToInt32(model.offset);
            //int paging = Convert.ToInt32(model.pagination);
            string orderByColumn = "CreatedOn";
            string orderBy = "DESC";

            JobSeeker jobSeekerObj = new JobSeeker();
            jobSeekerObj.IsActive = true;
            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            string hostUrl = config["Utility:APIBaseURL"];
            bool isSuccess = true;

            Session sessionObj = null;

            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(model.token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(model.token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }

            RecruiterPostedJobsAndApplicants recruiterPostedJobsAndApplicantsObj = new RecruiterPostedJobsAndApplicants();
            recruiterPostedJobsAndApplicantsObj.JobSeekerId = sessionObj.UserId;
            recruiterPostedJobsAndApplicantsObj.IsActive = true;

            IEnumerable<RecruiterPostedJobsAndApplicants> RecruiterPostedJobsAndApplicantsList = await recruiterPostedJobsAndApplicantsService.All(orderByColumn, orderBy, "", recruiterPostedJobsAndApplicantsObj);
            if (!RecruiterPostedJobsAndApplicantsList.Any())
            {

            }
            else
            {
                foreach (RecruiterPostedJobsAndApplicants RecruiterPostedJobsAndApplicantsObj in RecruiterPostedJobsAndApplicantsList)
                {
                    RecruiterPostedJobsAndApplicantsResponse2DTO RecruiterPostedJobsAndApplicantsResponse2DTOObj = new RecruiterPostedJobsAndApplicantsResponse2DTO();
                    RecruiterPostedJobsAndApplicantsResponse2DTOObj = mapper.Map<RecruiterPostedJobsAndApplicantsResponse2DTO>(RecruiterPostedJobsAndApplicantsObj);

                    RecruiterPostedJobs RecruiterPostedJobsObj = await recruiterPostedJobsService.GetById(RecruiterPostedJobsAndApplicantsResponse2DTOObj.recruiter_posted_jobs_id_enc);

                    if (RecruiterPostedJobsObj != null)
                    {
                        JobSeeker JobSeekerObj = await jobSeekerService.GetById(RecruiterPostedJobsAndApplicantsObj.JobSeekerId);
                        RecruiterPostedJobsAndApplicantsResponse2DTOObj.recruiter_posted_jobs_and_applicants_id_enc = RecruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsAndApplicantsId;

                        RecruiterPostedJobsAndApplicantsResponse2DTOObj.reference_no = RecruiterPostedJobsObj.ReferenceNo;
                        RecruiterPostedJobsAndApplicantsResponse2DTOObj.title = RecruiterPostedJobsObj.Title;

                        CountryDetails countryDetailsObj = countryDetailsService.GetById(RecruiterPostedJobsObj.CountryId);
                        RecruiterPostedJobsAndApplicantsResponse2DTOObj.country_name = "";
                        if (countryDetailsObj != null)
                        {
                            RecruiterPostedJobsAndApplicantsResponse2DTOObj.country_name = countryDetailsObj.Name;
                        }
                        RecruiterPostedJobsAndApplicantsResponse2DTOObj.city = RecruiterPostedJobsObj.City;
                        RecruiterPostedJobsAndApplicantsResponse2DTOObj.date = RecruiterPostedJobsObj.CreatedOn;

                        RecruiterAttachment RecruiterAttachmentObj = await recruiterAttachmentService.GetByDocumentType(RecruiterPostedJobsObj.RecruiterId, "logo");
                        string UploadedFrom = String.Empty;
                        if (RecruiterAttachmentObj?.UploadedFrom != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
                        if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                        else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                        string fileNameUpdated = "", imgPath = "";
                        if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
                        imgPath = hostUrl + "/assets/icons/icon_document.png";
                        if (!string.IsNullOrEmpty(fileNameUpdated)) { imgPath = hostUrl + "/Uploads/" + fileNameUpdated; }
                        RecruiterPostedJobsAndApplicantsResponse2DTOObj.logo_url = imgPath;
                        if (RecruiterPostedJobsObj.IsActive == true) { RecruiterPostedJobsAndApplicantsResponse2DTOObj.is_active = "Y"; } else { RecruiterPostedJobsAndApplicantsResponse2DTOObj.is_active = "N"; }

                        // total_message_against_job
                        // total_message_against_job_unread
                        int totalMessageAgainstJob = messageAgainstJobService.GetTotalMessagesAgainstJob(RecruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsAndApplicantsId);
                        int totalMessageAgainstJobUnRead = messageAgainstJobService.GetTotalMessagesAgainstJobUnRead(RecruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsAndApplicantsId, "recruiter");

                        RecruiterPostedJobsAndApplicantsResponse2DTOObj.total_message_against_job = Convert.ToString(totalMessageAgainstJob);
                        RecruiterPostedJobsAndApplicantsResponse2DTOObj.total_message_against_job_unread = Convert.ToString(totalMessageAgainstJobUnRead);

                        RecruiterPostedJobsAndApplicantsResponse2DTOList.Add(RecruiterPostedJobsAndApplicantsResponse2DTOObj);
                    }
                }

            }
            JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
            JsonArrLoginResponseDTOObj.code = "1";
            JsonArrLoginResponseDTOObj.MSG = "success";
            JsonArrLoginResponseDTOObj.data = RecruiterPostedJobsAndApplicantsResponse2DTOList;
            return JsonArrLoginResponseDTOObj;
        }

        [HttpPost]
        [Route("en/job_seeker/personal_information_mobile")]
        public async Task<Object> personal_information_mobile([FromBody] PersonalInformationMobileRequestDTO model)
        {
            string token = model.token;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeekerPersonalInformationResponseDTO jobSeekerPersonalInformationResponseDTOObj = new JobSeekerPersonalInformationResponseDTO();

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    try
                    {
                        JobSeekerPersonalInformation jobSeekerPersonalInformationObj = await jobSeekerPersonalInformationService.GetById(sessionObj.UserId);

                        if (jobSeekerPersonalInformationObj != null)
                        {
                            jobSeekerPersonalInformationResponseDTOObj.tbl_job_seeker_personal_information_id = jobSeekerPersonalInformationObj.JobSeekerPersonalInformationId;
                            jobSeekerPersonalInformationResponseDTOObj.tbl_job_seeker_id = jobSeekerPersonalInformationObj.JobSeekerId;
                            jobSeekerPersonalInformationResponseDTOObj.nationality = jobSeekerPersonalInformationObj.CountryId;
                            jobSeekerPersonalInformationResponseDTOObj.tbl_marital_status_id = jobSeekerPersonalInformationObj.MaritalStatusId;
                            jobSeekerPersonalInformationResponseDTOObj.home_phone = jobSeekerPersonalInformationObj.HomePhone;
                            jobSeekerPersonalInformationResponseDTOObj.mobileCode = jobSeekerPersonalInformationObj.MobileCode;
                            jobSeekerPersonalInformationResponseDTOObj.is_driving_license = jobSeekerPersonalInformationObj.IsDrivingLicense;
                            jobSeekerPersonalInformationResponseDTOObj.current_location = jobSeekerPersonalInformationObj.CurrentLocation;
                            jobSeekerPersonalInformationResponseDTOObj.visa_status = jobSeekerPersonalInformationObj.VisaStatus;
                            jobSeekerPersonalInformationResponseDTOObj.added_date = jobSeekerPersonalInformationObj.CreatedOn;
                            jobSeekerPersonalInformationResponseDTOObj.is_active = jobSeekerPersonalInformationObj.IsActive == true ? "Y" : "N";
                        }

                        JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
                        JsonArrLoginResponseDTOObj.code = "1";
                        JsonArrLoginResponseDTOObj.MSG = "success";
                        JsonArrLoginResponseDTOObj.data = jobSeekerPersonalInformationResponseDTOObj;
                        return JsonArrLoginResponseDTOObj;
                    }
                    catch (Exception ex)
                    {
                        JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
                        JsonArrLoginResponseDTOObj.code = "0";
                        JsonArrLoginResponseDTOObj.MSG = "critical error: " + ex.Message;
                        JsonArrLoginResponseDTOObj.data = null;
                        return JsonArrLoginResponseDTOObj;
                    }
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;   
        }



        [HttpPost]
        [Route("en/job_seeker/qualification_mobile")]
        public async Task<Object> qualification_mobile([FromBody] QualificationMobileRequestDTO model)
        {
            string token = model.token;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeekerQualificationResponseDTO jobSeekerQualificationResponseDTOObj = new JobSeekerQualificationResponseDTO();

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    JobSeekerQualification jobSeekerQualificationObj = await jobSeekerQualificationService.GetByJobSeekerId(sessionObj.UserId);

                    jobSeekerQualificationResponseDTOObj.tbl_job_seeker_qualification_id = jobSeekerQualificationObj.JobSeekerQualificationId;
                    jobSeekerQualificationResponseDTOObj.tbl_job_seeker_id = jobSeekerQualificationObj.JobSeekerId;
                    jobSeekerQualificationResponseDTOObj.tbl_education_level_id = jobSeekerQualificationObj.EducationLevelId;
                    jobSeekerQualificationResponseDTOObj.tbl_specialization_id = jobSeekerQualificationObj.SpecializationId;
                    jobSeekerQualificationResponseDTOObj.school_university = jobSeekerQualificationObj.SchoolUniversity;
                    jobSeekerQualificationResponseDTOObj.graduation_date = jobSeekerQualificationObj.GraduationDate;
                    jobSeekerQualificationResponseDTOObj.name_of_degree = jobSeekerQualificationObj.NameOfDegree;
                    jobSeekerQualificationResponseDTOObj.added_date = jobSeekerQualificationObj.CreatedOn;
                    jobSeekerQualificationResponseDTOObj.is_active = jobSeekerQualificationObj.IsActive == true ? "Y" : "N";

                    JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
                    JsonArrLoginResponseDTOObj.code = "1";
                    JsonArrLoginResponseDTOObj.MSG = "success";
                    JsonArrLoginResponseDTOObj.data = jobSeekerQualificationResponseDTOObj;
                    return JsonArrLoginResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/work_experience_mobile")]
        public async Task<Object> work_experience_mobile([FromBody] WorkExperienceMobileRequestDTO model)
        {
            string token = model.token;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeekerWorkExperienceDetailsResponseDTO jobSeekerWorkExperienceDetailsResponseDTOObj = new JobSeekerWorkExperienceDetailsResponseDTO();

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                List<JobSeekerWorkExperienceDetailsArrResponseDTO> work_experienceList = new List<JobSeekerWorkExperienceDetailsArrResponseDTO>();

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    JobSeekerWorkExperience jobSeekerWorkExperienceObj = await jobSeekerWorkExperienceService.GetByJobSeekerId(sessionObj.UserId);

                    JobSeekerWorkExperienceDetailsArrResponseDTO jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();

                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj?.Company1;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj?.JobTitle1;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj?.CountryId1 ?? 0;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj?.City1;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj?.TimePeriodFrom1;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj?.TimePeriodTo1;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj?.ReasonForLeaving1;
                    work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);

                    jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj?.Company2;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj?.JobTitle2;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj?.CountryId2 ?? 0;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj?.City2;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj?.TimePeriodFrom2;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj?.TimePeriodTo2;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj?.ReasonForLeaving2;

                    work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj?.Company3;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj?.JobTitle3;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj?.CountryId3 ?? 0;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj?.City3;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj?.TimePeriodFrom3;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj?.TimePeriodTo3;
                    jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj?.ReasonForLeaving3;
                    work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);

                    jobSeekerWorkExperienceDetailsResponseDTOObj.work_experience = work_experienceList;

                    JsonArrWorkExperienceResponseDTO jsonArrWorkExperienceResponseDTOObj = new JsonArrWorkExperienceResponseDTO();
                    jsonArrWorkExperienceResponseDTOObj.code = "1";
                    jsonArrWorkExperienceResponseDTOObj.MSG = "success";
                    jsonArrWorkExperienceResponseDTOObj.experience_level = jobSeekerWorkExperienceObj?.ExperienceLevel;
                    jsonArrWorkExperienceResponseDTOObj.data = jobSeekerWorkExperienceDetailsResponseDTOObj;
                    return jsonArrWorkExperienceResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be retrieved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/ajax_registration_form_mobile")]
        public async Task<object> ajax_registration_form_mobile([FromBody] RegistrationFromMobileRequestDTO model)
        {
            string token = model.token;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeekerDetailResponseDTO jobSeekerDetailResponseDTOObj = new JobSeekerDetailResponseDTO();
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            string hostUrl = config["Utility:APIBaseURL"];

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    JobSeeker jobSeekerObj = await jobSeekerService.GetById(sessionObj.UserId);

                    jobSeekerDetailResponseDTOObj.tbl_job_seeker_id = jobSeekerObj.JobSeekerId;
                    jobSeekerDetailResponseDTOObj.title = jobSeekerObj.Title;
                    jobSeekerDetailResponseDTOObj.first_name = jobSeekerObj.FirstName;
                    jobSeekerDetailResponseDTOObj.last_name = jobSeekerObj.LastName;
                    jobSeekerDetailResponseDTOObj.dob = jobSeekerObj.Dob.ToString("dd-MM-yyyy");
                    jobSeekerDetailResponseDTOObj.gender = jobSeekerObj.Gender;
                    jobSeekerDetailResponseDTOObj.mobile = jobSeekerObj.Mobile;
                    jobSeekerDetailResponseDTOObj.country_no = jobSeekerObj.CountryNo;
                    jobSeekerDetailResponseDTOObj.country_code = jobSeekerObj.CountryCode;
                    jobSeekerDetailResponseDTOObj.email = jobSeekerObj.Email;
                    jobSeekerDetailResponseDTOObj.last_login = Convert.ToString(sessionObj.CreatedOn);
                    jobSeekerDetailResponseDTOObj.added_date = jobSeekerObj.CreatedOn;
                    jobSeekerDetailResponseDTOObj.apple_device_token = jobSeekerObj.AppleDeviceToken;
                    jobSeekerDetailResponseDTOObj.android_device_token = jobSeekerObj.AndroidDeviceToken;

                    JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
                    JsonArrLoginResponseDTOObj.code = "1";
                    JsonArrLoginResponseDTOObj.MSG = "Success";
                    JsonArrLoginResponseDTOObj.data = jobSeekerDetailResponseDTOObj;
                    return JsonArrLoginResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be retrieved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/job_seeker/approve_job_seeker_files")]
        public async Task<object> approve_job_seeker_files([FromBody] ApproveJobSeekerFilesRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            bool isSuccess = true;
            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            string hostUrl = config["Utility:APIBaseURL"];
            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (model.jobSeekerId <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Invalid Data";
                        return JsonArrResponseDTOObj;
                    }
                }
                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    JobSeeker jobSeekerObj = await jobSeekerService.GetById(model.jobSeekerId);
                    if (model.ApprovedType == "picture")
                    {
                        jobSeekerObj.IsPictureApproved = model.isApproved;
                    }
                    if (model.ApprovedType == "video")
                    {
                        jobSeekerObj.IsVideoApproved = model.isApproved;
                    }
                    if (jobSeekerObj.IsPictureApproved == true && jobSeekerObj.IsVideoApproved == true)
                    {
                        jobSeekerObj.IsApproved = true;
                    }
                    //Update All Data
                    jobSeekerService.Save(jobSeekerObj);
                    JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
                    JsonArrLoginResponseDTOObj.code = "1";
                    JsonArrLoginResponseDTOObj.MSG = "Success";
                    JsonArrLoginResponseDTOObj.data = jobSeekerService;
                    return JsonArrLoginResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be retrieved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/job_seeker/forgot_password")]
        public async Task<JsonArrResponseDTO> forgot_password([FromBody] JobSeekerForgotPasswordRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeeker jobSeekerObjOld = null;
            bool isSuccess = true;
            bool isExist = false;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                if (string.IsNullOrWhiteSpace(model.email))
                {
                    JsonArrResponseDTOObj.code = "0";
                    JsonArrResponseDTOObj.MSG = "Email is blank. Please enter Email.";
                    return JsonArrResponseDTOObj;
                }
                else if (!UtilityHelper.IsValidEmail(model.email))
                {
                    JsonArrResponseDTOObj.code = "0";
                    JsonArrResponseDTOObj.MSG = "Invalid Email.";
                    return JsonArrResponseDTOObj;
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    JobSeeker jobSeekerObj = await jobSeekerService.GetByEmail(model.email);
                    if (jobSeekerObj == null || jobSeekerObj.IsActive == false)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Invalid Email.";
                        return JsonArrResponseDTOObj;
                    }

                    string token = UtilityHelper.rand();

                    ForgotPassword forgotPasswordObjOld = await forgotPasswordService.GetByUserId(jobSeekerObj.JobSeekerId);
                    if (forgotPasswordObjOld != null)
                    {
                        forgotPasswordObjOld.IsActive = false;
                        forgotPasswordService.Save(forgotPasswordObjOld);
                    }

                    DateTime todayDateTime = DateTime.Now;
                    ForgotPassword forgotPasswordObj = new ForgotPassword();
                    forgotPasswordObj.ForgotPasswordId = 0;
                    forgotPasswordObj.UserId = jobSeekerObj.JobSeekerId;
                    forgotPasswordObj.ForgotPasswordToken = token;
                    forgotPasswordObj.TokenGenerationDate = todayDateTime;
                    forgotPasswordObj.IsPasswordReset = false;
                    forgotPasswordObj.IsActive = true;
                    forgotPasswordService.Save(forgotPasswordObj);

                    long jobSeekerId = jobSeekerService.Save(jobSeekerObj);

                    // Email Code Goes Here

                    string hostUrl = config["Utility:APIBaseURL"];
                    string resetPasswordLink = hostUrl + "/en/job_seeker/reset_password?token=" + token;
                    string projectName = config["Utility:ProjectName"];
                    string emailText = "<style type='text/css'> .ReadMsgBody { width: 100% !important;} .ExternalClass {width: 100% !important;} #like { color:#000000; } #follow { color:#000000; } </style> <table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' style='height:100% !important; margin:0; padding:0; width:100% !important; background-color:#FFFFFF'> <tr> <td align='center' valign='top'><table width='600' border='0' align='left' cellpadding='0' cellspacing='0'> <tr> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='580' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='580' align='left' valign='top' style='border-collapse:collapse;'><font face='Verdana, Arial, Helvetica, sans-serif' size='2'><font size='5'>D</font><strong>ear " + jobSeekerObj.FirstName + " " + jobSeekerObj.LastName + ",</strong></font></td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td height='12' width='10'></td> <td height='12' width='580'></td> <td height='12' width='10'></td> </tr> <tr> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>Please click on the link below to reset your password. You will get another email with system generated password. You can change the password once logged in.</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'></td> <td width='10' height='12'></td> </tr> <tr> <td height='12'></td> <td height='12' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#237DAF;'>" + resetPasswordLink + "</td> <td height='12'></td> </tr> <tr> <td height='12'></td> <td height='12'></td> <td height='12'></td> </tr> <tr> <td height='12'></td> <td height='12' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>If clicking the link does not work, just copy and paste the entire link into your browser.</td> <td height='12'></td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'></td> <td width='10' height='12'></td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>We assure you our best services.&nbsp;</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'></td> <td width='10' height='12'></td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:22px; font-weight:normal; color:#000000;'><font face='Verdana, Arial, Helvetica, sans-serif' size='2'><font size='5'>T</font><strong>hanks</strong></font></td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'><a href='" + hostUrl + "' target='_blank' alias='" + hostUrl + "' style='color:#237DAF; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-decoration:none'><span style='color:#237DAF;' title='" + hostUrl + "'>" + hostUrl + "</span></a></td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'></td> <td width='10' height='12'></td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'><hr ></td> <td width='10' height='12'></td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='center' valign='top' style='border-collapse:collapse; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; font-weight:normal; color:#000000;'>This mail is sent to you because you registered at " + hostUrl + ". Its not a part of spam mails.</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>&nbsp;</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> </table></td> </tr> </table>";
                    string emailSubject = "Forgot Password at Jobs Portal";

                    string emailFromName = "Jobs Portal";
                    string emailFromEmail = config["EmailConfig:EmailFromEmail"];
                    string emailFromPassword = config["EmailConfig:EmailFromPassword"];
                    string emailToName = jobSeekerObj.FirstName + " " + jobSeekerObj.LastName;
                    string emailToEmail = jobSeekerObj.Email;

                    MimeMessage message = new MimeKit.MimeMessage();
                    var bodyBuilder = new BodyBuilder();
                    MailboxAddress from = new MailboxAddress(emailFromEmail, emailFromEmail);
                    message.From.Add(from);
                    MailboxAddress to = null;
                    to = new MailboxAddress(emailToName, emailToEmail);
                    message.To.Add(to);
                    message.Subject = emailSubject;
                    bodyBuilder.HtmlBody = emailText;
                    message.Body = bodyBuilder.ToMessageBody();
                    SmtpClient client = new SmtpClient();
                    int port = Convert.ToInt32(config["EmailConfig:EmailPort"]);
                    bool EmailEnableSSL = Convert.ToBoolean(config["EmailConfig:EmailEnableSSL"]);
                    string SMTP = config["EmailConfig:EmailSMTP"];
                    client.Connect(SMTP, port, EmailEnableSSL);

                    client.Authenticate(emailFromEmail, emailFromPassword);
                    client.Send(message);
                    client.Disconnect(true);
                    client.Dispose();

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Please follow the link in email with instructions on how to reset your password.";
                    return JsonArrResponseDTOObj;

                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpGet]
        [Route("en/job_seeker/reset_password")]
        public async Task<object> reset_password(string token)
        {
            //string token = token;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeeker jobSeekerObjOld = null;
            bool isSuccess = true;
            bool isExist = false;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                if (string.IsNullOrWhiteSpace(token))
                {
                    return "<div style='border:1px solid #333; padding:10px; width:500px; text-align:center; margin:auto'>Invalid token.</div>";
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    ForgotPassword forgotPasswordObj = await forgotPasswordService.GetByUserToken(token);
                    if (forgotPasswordObj == null || forgotPasswordObj.IsActive == false)
                    {
                        return "<div style='border:1px solid #333; padding:10px; width:500px; text-align:center; margin:auto'>Invalid token.</div>";
                    }

                    DateTime todayDateTime = DateTime.Now;
                    forgotPasswordObj.TokenUseDate = todayDateTime;
                    forgotPasswordObj.IsPasswordReset = true;
                    forgotPasswordService.Save(forgotPasswordObj);

                    long forgotPasswordId = forgotPasswordService.Save(forgotPasswordObj);
                    JobSeeker jobSeekerObj = await jobSeekerService.GetById(forgotPasswordObj.UserId);

                    string password = UtilityHelper.rand();
                    string accActivationToken = UtilityHelper.rand();
                    jobSeekerObj.Password = UtilityHelper.sha1(password);
                    jobSeekerService.Save(jobSeekerObj);

                    // Email Code Goes Here
                    string hostUrl = config["Utility:APIBaseURL"];
                    string emailText = "<style type='text/css'> .ReadMsgBody { width: 100% !important;} .ExternalClass {width: 100% !important;} #like { color:#000000; } #follow { color:#000000; } </style> <table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' style='height:100% !important; margin:0; padding:0; width:100% !important; background-color:#FFFFFF'> <tr> <td align='center' valign='top'><table width='600' border='0' align='left' cellpadding='0' cellspacing='0'> <tr> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='580' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='580' align='left' valign='top' style='border-collapse:collapse;'><font face='Verdana, Arial, Helvetica, sans-serif' size='2'><font size='5'>D</font><strong>ear " + jobSeekerObj.FirstName + " " + jobSeekerObj.LastName + ",</strong></font></td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td height='12' width='10'></td> <td height='12' width='580'></td> <td height='12' width='10'></td> </tr> <tr> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>Your Password is changed successfully.</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'></td> <td width='10' height='12'></td> </tr> <tr> <td height='12'></td> <td height='12' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal;'>New Password: <span style='color:#237DAF;'>" + password + "</span></td> <td height='12'></td> </tr> <tr> <td height='12'></td> <td height='12'></td> <td height='12'></td> </tr> <tr> <td height='12'></td> <td height='12' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>Please note that it is a system generated password. You can change the password after login with system generated password.</td> <td height='12'></td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'></td> <td width='10' height='12'></td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'></td> <td width='10' height='12'></td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:22px; font-weight:normal; color:#000000;'><font face='Verdana, Arial, Helvetica, sans-serif' size='2'><font size='5'>T</font><strong>hanks</strong></font></td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'><a href='" + hostUrl + "' target='_blank' alias='" + hostUrl + "' style='color:#237DAF; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-decoration:none'><span style='color:#237DAF;' title='" + hostUrl + "'>" + hostUrl + "</span></a></td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'></td> <td width='10' height='12'></td> </tr> <tr> <td width='10' height='12'></td> <td width='580' height='12'><hr ></td> <td width='10' height='12'></td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='center' valign='top' style='border-collapse:collapse; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; font-weight:normal; color:#000000;'>This mail is sent to you because you registered at " + hostUrl + ". Its not a part of spam mails.</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> <tr> <td width='10' align='left' valign='top'></td> <td width='580' align='left' valign='top' style='border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000;'>&nbsp;</td> <td width='10' align='left' valign='top' style='border-collapse:collapse;'>&nbsp;</td> </tr> </table></td> </tr> </table>";
                    string emailSubject = "New Password at Jobs Portal";

                    string emailFromName = "Jobs Portal";
                    string emailFromEmail = config["EmailConfig:EmailFromEmail"];
                    string emailFromPassword = config["EmailConfig:EmailFromPassword"];
                    string emailToName = jobSeekerObj.FirstName + " " + jobSeekerObj.LastName;
                    string emailToEmail = jobSeekerObj.Email;

                    MimeMessage message = new MimeKit.MimeMessage();
                    var bodyBuilder = new BodyBuilder();
                    MailboxAddress from = new MailboxAddress(emailFromEmail, emailFromEmail);
                    message.From.Add(from);
                    MailboxAddress to = null;
                    to = new MailboxAddress(emailToName, emailToEmail);
                    message.To.Add(to);
                    message.Subject = emailSubject;
                    bodyBuilder.HtmlBody = emailText;
                    message.Body = bodyBuilder.ToMessageBody();
                    SmtpClient client = new SmtpClient();
                    int port = Convert.ToInt32(config["EmailConfig:EmailPort"]);
                    bool EmailEnableSSL = Convert.ToBoolean(config["EmailConfig:EmailEnableSSL"]);
                    string SMTP = config["EmailConfig:EmailSMTP"];
                    client.Connect(SMTP, port, EmailEnableSSL);

                    client.Authenticate(emailFromEmail, emailFromPassword);
                    client.Send(message);
                    client.Disconnect(true);
                    client.Dispose();

                    return "<div style='border:1px solid #333; padding:10px; width:500px; text-align:center; margin:auto'>Password changed successfully. Please check your email for new password.</div>";

                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/job_seeker/ajax_attachments_mobile")]
        public async Task<Object> ajax_attachments_mobile([FromBody] AttachemntsJobSeekerRequestDTO model)
        {
            string token = model.token;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";
            string hostUrl = config["Utility:APIBaseURL"];
            AttachmentJobSeekerResponseDTO attachmentJobSeekerResponseDTO = new AttachmentJobSeekerResponseDTO();

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    JobSeekerPersonalInformation jobSeekerPersonalInformationObj = await jobSeekerPersonalInformationService.GetById(sessionObj.UserId);
                    if (jobSeekerPersonalInformationObj == null && sessionObj != null)
                    {
                        jobSeekerPersonalInformationObj = new JobSeekerPersonalInformation();
                        jobSeekerPersonalInformationObj.JobSeekerId = sessionObj.UserId;
                    }
                    #region resume Url
                    JobSeekerAttachment RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj?.JobSeekerId ?? 0, "resume");
                    string fileNameUpdated = "", Path = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
                    string UploadedFrom = String.Empty;
                    if (RecruiterAttachmentObj != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.resume = Path;
                    #endregion

                    #region id_card Url
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj?.JobSeekerId ?? 0, "id_card");
                    fileNameUpdated = "";
                    Path = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
                    UploadedFrom = String.Empty;
                    if (RecruiterAttachmentObj != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.id_card = Path;
                    #endregion

                    #region passport Url
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj?.JobSeekerId ?? 0, "passport");
                    fileNameUpdated = ""; Path = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
                    UploadedFrom = String.Empty;
                    if (RecruiterAttachmentObj != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.passport = Path;
                    #endregion

                    #region video_resume Url
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj?.JobSeekerId ?? 0, "video_resume");
                    fileNameUpdated = ""; Path = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }

                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.video_resume = Path;
                    #endregion

                    #region picture Url
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj?.JobSeekerId ?? 0, "picture");
                    fileNameUpdated = "";
                    Path = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
                    UploadedFrom = String.Empty;
                    if (RecruiterAttachmentObj != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.picture = Path;
                    #endregion
                    if (sessionObj.UserId > 0)
                    {
                        JobSeeker jobSeekerObj = await jobSeekerService.GetById(sessionObj.UserId);
                        attachmentJobSeekerResponseDTO.isActivePicture = jobSeekerObj.IsPictureApproved;
                        attachmentJobSeekerResponseDTO.isActiveVideo = jobSeekerObj.IsVideoApproved;
                    }
                    JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
                    JsonArrLoginResponseDTOObj.code = "1";
                    JsonArrLoginResponseDTOObj.MSG = "success";
                    JsonArrLoginResponseDTOObj.data = attachmentJobSeekerResponseDTO;
                    return JsonArrLoginResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }



        [HttpPost]
        [Route("en/job_seeker/job_seeker_profile")]
        public async Task<object> job_seeker_profile([FromBody] JobSeekerProfileRequestDTO model)
        {

            long jobSeekerId = model.gegowtztuz;
            //string displayBlack = model.display_back;
            //string displayPrinter = model.display_printer;
            //if (string.IsNullOrEmpty(displayPrinter)) { displayPrinter = "N"; }

            //JobSeeker jobSeekerObj = await jobSeekerService.GetById(jobSeekerId);
            //JobSeekerPersonalInformation jobSeekerPersonalInformationObj = await jobSeekerPersonalInformationService.GetById(jobSeekerId);
            //JobSeekerQualification jobSeekerQualificationObj = await jobSeekerQualificationService.GetByJobSeekerId(jobSeekerId);
            //JobSeekerWorkExperience jobSeekerWorkExperienceObj = await jobSeekerWorkExperienceService.GetByJobSeekerId(jobSeekerId);
            //string email = (!String.IsNullOrEmpty(jobSeekerObj.Email) ? jobSeekerObj.Email : "info@brainstormhouse.com");
            //string mobileNo = (!String.IsNullOrEmpty(jobSeekerObj.Mobile) ? jobSeekerObj.Mobile : "0504214571");
            //mobileNo = "+" + jobSeekerObj.CountryNo + mobileNo;
            //#region picture Url
            //var UploadedFrom = string.Empty;
            //var hostUrl = string.Empty;
            //var RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj?.JobSeekerId??0, "picture");
            //var fileNameUpdated = "";
            //string Path = "";
            //if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
            //if (RecruiterAttachmentObj?.UploadedFrom != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
            //if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
            //else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
            //if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; } else { Path = "http://ofwpro.com/assets/images/default_male.png"; }
            //string ImageUrl = Path;
            //#endregion

            //StringBuilder profile_str = new StringBuilder();

            //string company1 = jobSeekerWorkExperienceObj?.Company1;
            //string job_title1 = jobSeekerWorkExperienceObj?.JobTitle1;
            //long country_id_enc1 = jobSeekerWorkExperienceObj?.CountryId1 ?? 0;
            //string city1 = jobSeekerWorkExperienceObj?.City1;
            //string time_period_from1 = jobSeekerWorkExperienceObj?.TimePeriodFrom1;
            //string time_period_to1 = jobSeekerWorkExperienceObj?.TimePeriodTo1;
            //string reason_for_leaving1 = jobSeekerWorkExperienceObj?.ReasonForLeaving1;

            //string company2 = jobSeekerWorkExperienceObj?.Company2;
            //string job_title2 = jobSeekerWorkExperienceObj?.JobTitle2;
            //long country_id_enc2 = jobSeekerWorkExperienceObj?.CountryId2 ?? 0;
            //string city2 = jobSeekerWorkExperienceObj?.City2;
            //string time_period_from2 = jobSeekerWorkExperienceObj?.TimePeriodFrom2;
            //string time_period_to2 = jobSeekerWorkExperienceObj?.TimePeriodTo2;
            //string reason_for_leaving2 = jobSeekerWorkExperienceObj?.ReasonForLeaving2;

            //string company3 = jobSeekerWorkExperienceObj?.Company3;
            //string job_title3 = jobSeekerWorkExperienceObj?.JobTitle3;
            //long country_id_enc3 = jobSeekerWorkExperienceObj?.CountryId3 ?? 0;
            //string city3 = jobSeekerWorkExperienceObj?.City3;
            //string time_period_from3 = jobSeekerWorkExperienceObj?.TimePeriodFrom3;
            //string time_period_to3 = jobSeekerWorkExperienceObj?.TimePeriodTo3;
            //string reason_for_leaving3 = jobSeekerWorkExperienceObj?.ReasonForLeaving3;

            //string country_name1 = "";
            //if (country_id_enc1 > 0)
            //{
            //    CountryDetails country_obj = countryDetailsService.GetById(country_id_enc1);
            //    country_name1 = country_obj.Name;
            //}

            //string country_name2 = "";
            //if (country_id_enc2 > 0)
            //{
            //    CountryDetails country_obj = countryDetailsService.GetById(country_id_enc2);
            //    country_name2 = country_obj.Name;
            //}

            //string country_name3 = "";
            //if (country_id_enc3 > 0)
            //{
            //    CountryDetails country_obj = countryDetailsService.GetById(country_id_enc3);
            //    country_name3 = country_obj.Name;
            //}

            //// Personal Information
            //long tbl_job_seeker_personal_information_id = jobSeekerPersonalInformationObj?.JobSeekerPersonalInformationId ?? 0;
            //long nationality = jobSeekerPersonalInformationObj?.CountryId ?? 0;
            //long tbl_marital_status_id = jobSeekerPersonalInformationObj?.MaritalStatusId ?? 0;
            //string home_phone = jobSeekerPersonalInformationObj?.HomePhone;
            //string is_driving_license = jobSeekerPersonalInformationObj?.IsDrivingLicense;
            //string current_location = jobSeekerPersonalInformationObj?.CurrentLocation;
            //string visa_status = jobSeekerPersonalInformationObj?.VisaStatus;

            //string country_name = "";
            //if (nationality > 0)
            //{
            //    CountryDetails country_obj = countryDetailsService.GetById(nationality);
            //    country_name = country_obj.Name;
            //}

            //string marital_status_name = "Not Specified";
            //if (tbl_marital_status_id > 0)
            //{
            //    MaritalStatus marital_status_obj = maritalStatusService.GetById(tbl_marital_status_id);
            //    marital_status_name = marital_status_obj.Name;
            //}

            //if (string.IsNullOrEmpty(country_name)) { country_name = "Not Specified"; }
            //if (string.IsNullOrEmpty(home_phone)) { home_phone = "Not Specified"; }
            //if (string.IsNullOrEmpty(current_location)) { current_location = "Not Specified"; }
            //if (string.IsNullOrEmpty(visa_status)) { visa_status = "Not Specified"; }
            //else
            //{
            //    visa_status = visa_status.Replace("_", " ");
            //    //TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            //    //visa_status = ucwords($visa_status);
            //}

            //if (is_driving_license == "Y")
            //{
            //    is_driving_license = "Yes";
            //}
            //else
            //{
            //    is_driving_license = "No";
            //}

            //var is_arr1 = "N";
            //var is_arr2 = "N";
            //var is_arr3 = "N";

            //if ((company1) != "" || (job_title1) != "" || (country_name1) != "" || (city1) != "" || (time_period_from1) != "" || (time_period_to1) != "" || (reason_for_leaving1) != "")
            //{
            //    is_arr1 = "Y";
            //}
            //if ((company2) != "" || (job_title2) != "" || (country_name2) != "" || (city2) != "" || (time_period_from2) != "" || (time_period_to2) != "" || (reason_for_leaving2) != "")
            //{
            //    is_arr2 = "Y";
            //}
            //if ((company3) != "" || (job_title3) != "" || (country_name3) != "" || (city3) != "" || (time_period_from3) != "" || (time_period_to3) != "" || (reason_for_leaving3) != "")
            //{
            //    is_arr3 = "Y";
            //}
            //var educationalLevel = ""; var specialization = "";
            /////// Qualifications
            //if (jobSeekerQualificationObj?.EducationLevelId > 0) { educationalLevel = educationalLevelService.GetById(jobSeekerQualificationObj.EducationLevelId)?.Name; } else { educationalLevel = "Not Specified"; }
            //if (jobSeekerQualificationObj?.SpecializationId > 0) { specialization = specializationService.GetById(jobSeekerQualificationObj.EducationLevelId)?.Result?.SpecializationNameEn; } else { educationalLevel = "Not Specified"; }

            //profile_str.Append("<!DOCTYPE html>");
            //profile_str.Append("<html lang='en'>");
            //profile_str.Append("<head>");
            //profile_str.Append("  <meta charset='utf-8'>  ");
            //profile_str.Append("  <meta name='viewport' content='width=device-width, initial-scale=0.52'>");
            //profile_str.Append("  <title>:: " + jobSeekerObj.FirstName + " " + jobSeekerObj.LastName + "</title>  ");
            //profile_str.Append("  <script src='https://code.jquery.com/jquery-2.2.4.min.js' integrity='sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=' crossorigin='anonymous'></script> ");
            //profile_str.Append("  <script language='javascript'>");
            //profile_str.Append("	$(document).ready(function(e) {");
            //profile_str.Append("		var h = $(document).height();");
            //profile_str.Append("		$('#container').css('height', (h-20)+'px');	");
            //profile_str.Append("		if(isiPhone()){			");
            //profile_str.Append("		}	");
            //profile_str.Append("	});	");
            //profile_str.Append("	function isiPhone(){");
            //profile_str.Append("		return (");
            //profile_str.Append("			(navigator.platform.indexOf('iPhone') != -1) ||	(navigator.platform.indexOf('iPod') != -1)");
            //profile_str.Append("		);");
            //profile_str.Append("	}	");
            //profile_str.Append("	function print_me() {");
            //profile_str.Append("		$('#img_print').hide();");
            //profile_str.Append("		window.print();");
            //profile_str.Append("	}");
            //profile_str.Append("  </script>    ");
            //profile_str.Append("<style>");
            //profile_str.Append("body {");
            //profile_str.Append("	margin:0px;");
            //profile_str.Append("	padding:0px;");
            //profile_str.Append("	background-color:#C9C9C9;");
            //profile_str.Append("	color:#666;");
            //profile_str.Append("	font-family:Arial, Helvetica, sans-serif;");
            //profile_str.Append("	font-size:15px;");
            //profile_str.Append("}");
            //profile_str.Append("#container {");
            //profile_str.Append("	background-image: url('http://ofwpro.com/assets/images/profile_bg.png');");
            //profile_str.Append("	background-repeat: no-repeat;");
            //profile_str.Append("	background-position: top; ");
            //profile_str.Append("	width:100%;");
            //profile_str.Append("	max-width:795px;");
            //profile_str.Append("	min-height:950px;");
            //profile_str.Append("	overflow-X:hidden; 	");
            //profile_str.Append("}");
            //profile_str.Append(".head_contact {");
            //profile_str.Append("	font-size:14px;");
            //profile_str.Append("	font-weight:bold;	");
            //profile_str.Append("}");
            //profile_str.Append(".circle-img { border-radius:50%;  }	");
            //profile_str.Append(".user-img { border-radius:50%; max-height:180px; max-width:180px; }	");
            //profile_str.Append(".hello { font-size:60px; padding-left:300px; word-spacing: 3px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; }");
            //profile_str.Append(".there { font-size:60px; color: #F37740; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; }");
            //profile_str.Append(".i_am { font-size:30px; padding-left:300px; word-spacing: 3px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; }");
            //profile_str.Append(".first_name { font-size:30px; color: #F37740; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; }");
            //profile_str.Append(".last_name { font-size:30px; color: #F37740; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; }");
            //profile_str.Append(".designation { font-size:30px; color: #F37740; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; }");
            //profile_str.Append(".panel_head { font-size:16px; color:#F37740; border-bottom:1px solid #666; width:90%; }");
            //profile_str.Append(".col_orange { color: #F37740; }");
            //profile_str.Append(".icon { width:20px; }");
            //profile_str.Append(".timeline { padding: 100px 0px !important; }");
            //profile_str.Append("</style>");
            //profile_str.Append("</head>");
            //profile_str.Append("<body>");
            //profile_str.Append("<div id='container' style='margin:auto; background-color:#F3F2F0; position:relative'>");
            //profile_str.Append("  	<div style='height:21px;'></div>	");
            //profile_str.Append("	<div style='width:90%; margin:auto; position:relative; background-color:#FFF'>     ");
            //profile_str.Append("        <table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>");
            //profile_str.Append("          <tr>");
            //profile_str.Append("            <td> ");
            //profile_str.Append("				<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>");
            //profile_str.Append("				  <tr>");
            //profile_str.Append("					<td width='12%' height='45' align='center' valign='middle'><img src='http://ofwpro.com/assets/images/icons/icon_profile_mobile.png'></td>");
            //profile_str.Append("					<td width='20%' align='left' valign='middle' class='head_contact'>" + mobileNo + "</td>");
            //profile_str.Append("					<td width='12%' align='center' valign='middle'><img src='http://ofwpro.com/assets/images/icons/icon_profile_phone.png' alt=''></td>");
            //profile_str.Append("					<td width='20%' align='left' valign='middle' class='head_contact'>"+ mobileNo + "</td>");
            //profile_str.Append("					<td width='12%' align='center' valign='middle'><img src='http://ofwpro.com/assets/images/icons/icon_profile_email.png' alt=''></td>");
            //profile_str.Append("					<td width='24%' align='left' valign='middle' class='head_contact'>" + email + "</td>");
            //profile_str.Append("				  </tr>");
            //profile_str.Append("				</table>");
            //profile_str.Append("				<div style='position:absolute; left:30px; top:71px'><img src='" + Path + "' class='user-img'></div>            ");
            //profile_str.Append("            </td>");
            //profile_str.Append("          </tr>");
            //profile_str.Append("        </table>");
            //profile_str.Append("    </div>");
            //profile_str.Append("    <div>    ");
            //profile_str.Append("        <br>");
            //profile_str.Append("        <br>");
            //profile_str.Append("        <div style='padding-top:20px'><span class='hello'>HELLO</span> <span class='there'> THERE</span></div>	");
            //profile_str.Append("        <div style='padding-top:13px'><span class='i_am'>I AM </span> <span class='first_name'>" + jobSeekerObj.FirstName + "</span> <span class='last_name'>" + jobSeekerObj.LastName + "</span></div>	");
            //profile_str.Append("        <div style='padding-top:13px'><span class='i_am'>I AM </span> <span title='" + jobSeekerObj.Title + "' class='designation'>" + jobSeekerObj.Title + "</span> </div>	");
            //profile_str.Append("    </div>");
            //profile_str.Append("	<br>");
            //profile_str.Append("	<br>");
            //profile_str.Append("	<div>");
            //profile_str.Append("        <table width='90%' border='0' cellspacing='0' cellpadding='0' align='center'>");
            //profile_str.Append("          <tr>");
            //profile_str.Append("            <td width='55%' align='left' valign='top'>");
            //profile_str.Append("                <div class='panel_head' style='border-bottom:none'>WORK EXPERIENCE</div>                    ");
            //profile_str.Append("                <table width='100%' border='0' cellspacing='0' cellpadding='0' style='background-color:#F4F4F4'>");
            //profile_str.Append("                  <tr>");
            //profile_str.Append("                    <td width='35%' height='50' style='border-right:4px solid #DDDDDD'>&nbsp;</td>");
            //profile_str.Append("                    <td>&nbsp;</td>");
            //profile_str.Append("                  </tr>");
            //profile_str.Append("                </table>");

            //if (is_arr1.Equals("Y"))
            //{
            //    profile_str.Append("                <table width='100%' border='0' cellspacing='0' cellpadding='0' style='background-color:#F4F4F4'>");
            //    profile_str.Append("                  <tr>");
            //    profile_str.Append("                    <td width='35%' height='50' align='center' valign='top' style='border-right:4px solid #DDDDDD'>" + time_period_from1 + "<br />" + time_period_to1 + "</td>");
            //    profile_str.Append("                    <td align='left' valign='top'>");
            //    profile_str.Append("                      <div style='padding:0px 5px 0px 5px'>");
            //    profile_str.Append("                         " + job_title1 + ", " + company1 + "<br />");
            //    profile_str.Append("                        <p style='font-size:12px'><em>" + country_name1 + ", " + city1 + "</em></p>");
            //    profile_str.Append("                        <ul>");
            //    profile_str.Append("                          <li><strong>Duties and Responsibilities:</strong> <br />" + reason_for_leaving1 + "</li>");
            //    profile_str.Append("                        </ul>");
            //    profile_str.Append("                      </div>");
            //    profile_str.Append("                    </td>");
            //    profile_str.Append("                  </tr>");
            //    profile_str.Append("                </table>");
            //    profile_str.Append("                <table width='100%' border='0' cellspacing='0' cellpadding='0' style='background-color:#F4F4F4'>");
            //    profile_str.Append("                  <tr>");
            //    profile_str.Append("                    <td width='35%' height='50' style='border-right:4px solid #DDDDDD'>&nbsp;</td>");
            //    profile_str.Append("                    <td>&nbsp;</td>");
            //    profile_str.Append("                  </tr>");
            //    profile_str.Append("                </table>");
            //}

            //if (is_arr2.Equals("Y"))
            //{
            //    profile_str.Append("                <table width='100%' border='0' cellspacing='0' cellpadding='0' style='background-color:#F4F4F4'>");
            //    profile_str.Append("                  <tr>");
            //    profile_str.Append("                    <td width='35%' height='50' align='center' valign='top' style='border-right:4px solid #DDDDDD'>" + time_period_from2 + "<br />" + time_period_to2 + "</td>");
            //    profile_str.Append("                    <td align='left' valign='top'>");
            //    profile_str.Append("                      <div style='padding:0px 5px 0px 5px'>");
            //    profile_str.Append("                         " + job_title2 + ", " + company2 + "<br />");
            //    profile_str.Append("                        <p style='font-size:12px'><em>" + country_name2 + ", " + city2 + "</em></p>");
            //    profile_str.Append("                        <ul>");
            //    profile_str.Append("                          <li><strong>Duties and Responsibilities:</strong> <br />" + reason_for_leaving2 + "</li>");
            //    profile_str.Append("                        </ul>");
            //    profile_str.Append("                      </div>");
            //    profile_str.Append("                    </td>");
            //    profile_str.Append("                  </tr>");
            //    profile_str.Append("                </table>");
            //    profile_str.Append("                <table width='100%' border='0' cellspacing='0' cellpadding='0' style='background-color:#F4F4F4'>");
            //    profile_str.Append("                  <tr>");
            //    profile_str.Append("                    <td width='35%' height='50' style='border-right:4px solid #DDDDDD'>&nbsp;</td>");
            //    profile_str.Append("                    <td>&nbsp;</td>");
            //    profile_str.Append("                  </tr>");
            //    profile_str.Append("                </table>");
            //}

            //if (is_arr3.Equals("Y"))
            //{
            //    profile_str.Append("                <table width='100%' border='0' cellspacing='0' cellpadding='0' style='background-color:#F4F4F4'>");
            //    profile_str.Append("                  <tr>");
            //    profile_str.Append("                    <td width='35%' height='50' align='center' valign='top' style='border-right:4px solid #DDDDDD'>" + time_period_from3 + "<br />" + time_period_to3 + "</td>");
            //    profile_str.Append("                    <td align='left' valign='top'>");
            //    profile_str.Append("                      <div style='padding:0px 5px 0px 5px'>");
            //    profile_str.Append("                         " + job_title3 + ", " + company3 + "<br />");
            //    profile_str.Append("                        <p style='font-size:12px'><em>" + country_name3 + ", " + city3 + "</em></p>");
            //    profile_str.Append("                        <ul>");
            //    profile_str.Append("                          <li><strong>Duties and Responsibilities:</strong> <br />" + reason_for_leaving3 + "</li>");
            //    profile_str.Append("                        </ul>");
            //    profile_str.Append("                      </div>");
            //    profile_str.Append("                    </td>");
            //    profile_str.Append("                  </tr>");
            //    profile_str.Append("                </table>");
            //    profile_str.Append("                <table width='100%' border='0' cellspacing='0' cellpadding='0' style='background-color:#F4F4F4'>");
            //    profile_str.Append("                  <tr>");
            //    profile_str.Append("                    <td width='35%' height='50' style='border-right:4px solid #DDDDDD'>&nbsp;</td>");
            //    profile_str.Append("                    <td>&nbsp;</td>");
            //    profile_str.Append("                  </tr>");
            //    profile_str.Append("                </table>");
            //}

            //profile_str.Append("			</td>");
            //profile_str.Append("			<td align='left' valign='top'>");
            //profile_str.Append("			    <table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            //profile_str.Append("				  <tr>");
            //profile_str.Append("					<td>");
            //profile_str.Append("					  <div class='panel_head'>PERSONAL INFORMATION</div>");
            //profile_str.Append("							<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td width='14%' height='35' align='center' valign='middle' class='col_orange'>&nbsp;</td>");
            //profile_str.Append("									<td width='40%' align='left' valign='middle'>&nbsp;</td>");
            //profile_str.Append("									<td align='left' valign='middle'>&nbsp;</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'><img class='icon' src='http://ofwpro.com/assets/images/icons/nationality.png' /></td>");
            //profile_str.Append("									<td align='left' valign='middle'>Nationality</td>");
            //profile_str.Append("									<td align='left' valign='middle'>" + country_name + "</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'><img class='icon' src='http://ofwpro.com/assets/images/icons/driving.png' /></td>");
            //profile_str.Append("									<td align='left' valign='middle'>Driving License</td>");
            //profile_str.Append("									<td align='left' valign='middle'>" + is_driving_license + "</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'><img class='icon' src='http://ofwpro.com/assets/images/icons/location.png' /></td>");
            //profile_str.Append("									<td align='left' valign='middle'>Current Location</td>");
            //profile_str.Append("									<td align='left' valign='middle'>" + current_location + "</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'><img class='icon' src='http://ofwpro.com/assets/images/icons/visa.png' /></td>");
            //profile_str.Append("									<td align='left' valign='middle'> Visa Status</td>");
            //profile_str.Append("									<td align='left' valign='middle'>" + visa_status + "</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'><img class='icon' src='http://ofwpro.com/assets/images/icons/marital.png' /></td>");
            //profile_str.Append("									<td align='left' valign='middle'>Social Status</td>");
            //profile_str.Append("									<td align='left' valign='middle'>" + marital_status_name + "</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'><img class='icon' src='http://ofwpro.com/assets/images/icons/d.o.b.png' /></td>");
            //profile_str.Append("									<td align='left' valign='middle'>DOB</td>");
            //profile_str.Append("									<td align='left' valign='middle'>" + jobSeekerObj?.Dob.ToShortDateString() + "</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'>&nbsp;</td>");
            //profile_str.Append("									<td align='left' valign='middle'>&nbsp;</td>");
            //profile_str.Append("									<td align='left' valign='middle'>&nbsp;</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("						  </table>    ");
            //profile_str.Append("					</td>");
            //profile_str.Append("				  </tr>");
            //profile_str.Append("				  <tr>");
            //profile_str.Append("					<td>");
            //profile_str.Append("					  <div class='panel_head'>QUALIFICATION</div>");
            //profile_str.Append("							<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td width='14%' height='35' align='center' valign='middle' class='col_orange'>&nbsp;</td>");
            //profile_str.Append("									<td width='40%' align='left' valign='middle'>&nbsp;</td>");
            //profile_str.Append("									<td align='left' valign='middle'>&nbsp;</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'><img class='icon' src='http://ofwpro.com/assets/images/icons/education.png' /></td>");
            //profile_str.Append("									<td align='left' valign='middle'>Educational Degree</td>");
            //profile_str.Append("									<td align='left' valign='middle'>" + educationalLevel + "</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'><img class='icon' src='http://ofwpro.com/assets/images/icons/spelization.png' /></td>");
            //profile_str.Append("									<td align='left' valign='middle'>Specialization</td>");
            //profile_str.Append("									<td align='left' valign='middle'>" + specialization + "</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'><img class='icon' src='http://ofwpro.com/assets/images/icons/school.png' /></td>");
            //profile_str.Append("									<td align='left' valign='middle'>School/University</td>");
            //profile_str.Append("									<td align='left' valign='middle'>" + jobSeekerQualificationObj?.SchoolUniversity + "</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'><img class='icon' src='http://ofwpro.com/assets/images/icons/d.o.b.png' /></td>");
            //profile_str.Append("									<td align='left' valign='middle'>Graduation Date</td>");
            //profile_str.Append("									<td align='left' valign='middle'>" + jobSeekerQualificationObj?.GraduationDate.ToShortDateString() + "</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("								  <tr>");
            //profile_str.Append("									<td height='35' align='center' valign='middle'>&nbsp;</td>");
            //profile_str.Append("									<td align='left' valign='middle'>&nbsp;</td>");
            //profile_str.Append("									<td align='left' valign='middle'>&nbsp;</td>");
            //profile_str.Append("							  </tr>");
            //profile_str.Append("						  </table>");
            //profile_str.Append("					</td>");
            //profile_str.Append("				  </tr>");
            //profile_str.Append("				</table>");
            //profile_str.Append("            </td>");
            //profile_str.Append("          </tr>");
            //profile_str.Append("        </table>");
            //profile_str.Append("    </div>");
            //profile_str.Append("</div>    ");
            //profile_str.Append("<div style='width:795px; height:25px; background-color:#F37740; margin:auto; text-align:center; '><a href='http://ofwpro.com' style='color:#FFF; text-decoration:none; font-size:12px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif'>www.ofwpro.com</a></div>");
            //profile_str.Append("</body>");
            //profile_str.Append("</html>");
            var encryptedJobSeekerId = UtilityHelper.HashEncode(jobSeekerId);
            var BaseURL = config["Utility:ClientBaseURL"].ToString();
            var URI = BaseURL + "JobSeekerResume/" + encryptedJobSeekerId + "/";
            JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
            JsonArrLoginResponseDTOObj.code = "1";
            JsonArrLoginResponseDTOObj.MSG = "success";
            JsonArrLoginResponseDTOObj.data = new { html = "", url = URI };
            return JsonArrLoginResponseDTOObj;

        }



        //[HttpPost]
        //[Route("en/job_seeker/pusher_code")]
        //public async Task<object> pusher_code(string token)
        //{
        //    var options = new PusherOptions();
        //    options.Cluster = "eu";

        //    var pusher = new Pusher("b696c420f9702d42cb44", "9f75c505be6189722330", "627281", options);
        //    Task<ITriggerResult> resultTask = pusher.TriggerAsync("my-channel", "my-event", new { message = "hello world" });
        //    return "ok";
        //}
    }
}