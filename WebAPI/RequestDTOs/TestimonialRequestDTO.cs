﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class TestimonialRequestDTO
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string Designation { get; set; }
        public string TestimonialText { get; set; }

        public bool IsActive { get; set; }
    }
}
