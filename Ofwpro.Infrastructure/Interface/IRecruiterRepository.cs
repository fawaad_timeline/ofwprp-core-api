﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IRecruiterRepository
    {
        Task<IEnumerable<Recruiter>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Recruiter recruiterObj);
        Task<Recruiter> GetById(long recruiterId);
        long Save(Recruiter recruiterObj);
        bool Activate(long recruiterId, bool isActive);
        bool IsUserActivated(string email, string password);
        bool Delete(long recruiterId);
        bool IsExist(string email, long recruiterId);
        bool Validate(string email, string password);
        Task<Recruiter> GetByEmail(string email);
        Task<Recruiter> GetByRecruiterIdToken(string recruiterIdToken);
        Task<Recruiter> GetByActivationToken(string accActivationToken);
        long NoOfCompanies();

    }
}
