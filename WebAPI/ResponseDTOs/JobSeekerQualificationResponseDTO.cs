﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JobSeekerQualificationResponseDTO
    {
        public long tbl_job_seeker_qualification_id { get; set; }
        public long tbl_job_seeker_id { get; set; }
        public long tbl_education_level_id { get; set; }
        public long tbl_specialization_id { get; set; }

        public JobSeekerQualification jobSeekerQualification { get; set; }
        public EducationLevels educationLevels { get; set; }
        public Specialization specialization { get; set; }


        public string school_university { get; set; }
        public string name_of_degree { get; set; }
        public DateTime? graduation_date { get; set; }
        public DateTime? added_date { get; set; }
        public string is_active { get; set; }

    }
}
