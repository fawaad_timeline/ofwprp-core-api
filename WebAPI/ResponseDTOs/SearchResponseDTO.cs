﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class SearchResponseDTO
    {
       
        public int offset { get; set; }
        public int paging { get; set; }
        public int totalRecords { get; set; }
        public IEnumerable<dynamic> Records { get; set; }
    }
}
