﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Services.DTOs;
using Ofwpro.Services.Interfaces;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using MimeKit;
using MailKit.Net.Smtp;
using PusherServer;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class DeviceTokenController : ControllerBase
    {
        private readonly IFileUploadService fileUploadService;
        private readonly IJobSeekerService jobSeekerService;
        private readonly IAdministratorService administratorService;
        private readonly ISessionService sessionService;
        private readonly IJobSeekerPersonalInformationService jobSeekerPersonalInformationService;
        private readonly IJobSeekerQualificationService jobSeekerQualificationService;
        private readonly IJobSeekerWorkExperienceService jobSeekerWorkExperienceService;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IJobSeekerAttachmentService jobSeekerAttachmentService;
        private readonly IRecruiterPostedJobsService recruiterPostedJobsService;
        private readonly IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService;
        private readonly ICountryDetailsService countryDetailsService;
        private readonly IRecruiterAttachmentService recruiterAttachmentService;
        private readonly IForgotPasswordService forgotPasswordService;
        private readonly IMessageGeneralService messageGeneralService;
        private readonly IRecruiterService recruiterService;
        private readonly IRecruiterCompanyService recruiterCompanyService;
        private readonly IMessageAgainstJobService messageAgainstJobService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public DeviceTokenController(IFileUploadService fileUploadService, IJobSeekerService jobSeekerService, IAdministratorService administratorService, ISessionService sessionService, IJobSeekerPersonalInformationService jobSeekerPersonalInformationService, IJobSeekerQualificationService jobSeekerQualificationService, IJobSeekerWorkExperienceService jobSeekerWorkExperienceService, IWebHostEnvironment hostingEnvironment, IJobSeekerAttachmentService jobSeekerAttachmentService, IRecruiterPostedJobsService recruiterPostedJobsService, IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService, ICountryDetailsService countryDetailsService, IRecruiterAttachmentService recruiterAttachmentService, IForgotPasswordService forgotPasswordService, IMessageGeneralService messageGeneralService, IRecruiterService recruiterService, IRecruiterCompanyService recruiterCompanyService, IMessageAgainstJobService messageAgainstJobService, IMapper mapper, IConfiguration config)
        {
            this.fileUploadService = fileUploadService;
            this.jobSeekerService = jobSeekerService;
            this.administratorService = administratorService;
            this.sessionService = sessionService;
            this.jobSeekerPersonalInformationService = jobSeekerPersonalInformationService;
            this.jobSeekerQualificationService = jobSeekerQualificationService;
            this.jobSeekerWorkExperienceService = jobSeekerWorkExperienceService;
            this.hostingEnvironment = hostingEnvironment;
            this.jobSeekerAttachmentService = jobSeekerAttachmentService;
            this.recruiterPostedJobsService = recruiterPostedJobsService;
            this.recruiterPostedJobsAndApplicantsService = recruiterPostedJobsAndApplicantsService;
            this.countryDetailsService = countryDetailsService;
            this.recruiterAttachmentService = recruiterAttachmentService;
            this.forgotPasswordService = forgotPasswordService;
            this.messageGeneralService = messageGeneralService;
            this.recruiterService = recruiterService;
            this.recruiterCompanyService = recruiterCompanyService;
            this.messageAgainstJobService = messageAgainstJobService;
            this.mapper = mapper;
            this.config = config;
        }


        [HttpPost]
        [Route("en/device_token/save_device_token_apple")]
        public async Task<object> save_device_token_apple([FromBody] SaveDeviceTokenAppleRequestDTO model)
        {
            string token = model.token; string apple_device_token = model.apple_device_token;
            ResponseDTO<object> response = new ResponseDTO<object>();

            bool isSuccess = true;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(apple_device_token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid apple device token";
                        return JsonArrResponseDTOObj;
                    }                    
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    if (sessionObj.UserType.Equals("job_seeker"))
                    {
                        JobSeeker jobSeekerObj = await jobSeekerService.GetById(sessionObj.UserId);
                        jobSeekerObj.AppleDeviceToken = apple_device_token;
                        jobSeekerService.Save(jobSeekerObj);
                    }

                    if (sessionObj.UserType.Equals("recruiter"))
                    {
                        Recruiter recruiterObj = await recruiterService.GetById(sessionObj.UserId);
                        recruiterObj.AppleDeviceToken = apple_device_token;
                        recruiterService.Save(recruiterObj);
                    }

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Success";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/device_token/save_device_token_android")]
        public async Task<object> save_device_token_android([FromBody] SaveDeviceTokenAndroidRequestDTO model)
        {
            string token = model.token;  string android_device_token = model.android_device_token;
            ResponseDTO<object> response = new ResponseDTO<object>();

            bool isSuccess = true;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(android_device_token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid apple device token";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    if (sessionObj.UserType.Equals("job_seeker"))
                    {
                        JobSeeker jobSeekerObj = await jobSeekerService.GetById(sessionObj.UserId);
                        jobSeekerObj.AndroidDeviceToken = android_device_token;
                        jobSeekerService.Save(jobSeekerObj);
                    }

                    if (sessionObj.UserType.Equals("recruiter"))
                    {
                        Recruiter recruiterObj = await recruiterService.GetById(sessionObj.UserId);
                        recruiterObj.AndroidDeviceToken = android_device_token;
                        recruiterService.Save(recruiterObj);
                    }

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Success";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }
    }
}