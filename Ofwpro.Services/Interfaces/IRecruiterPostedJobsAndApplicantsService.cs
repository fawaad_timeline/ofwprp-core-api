﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface IRecruiterPostedJobsAndApplicantsService
    {
        Task<IEnumerable<RecruiterPostedJobsAndApplicants>> All(string orderByColumn, string orderBy, string searchTerm, RecruiterPostedJobsAndApplicants recruiterPostedJobsAndApplicantsObj);
        Task<RecruiterPostedJobsAndApplicants> GetById(long recruiterPostedJobsAndApplicantsId);
        long Save(RecruiterPostedJobsAndApplicants recruiterPostedJobsAndApplicantsObj);
        bool Activate(long recruiterPostedJobsAndApplicantsId, bool isActive);
        bool Delete(long recruiterPostedJobsAndApplicantsId);
        Task<RecruiterPostedJobsAndApplicants> GetByRecruiterPostedJobsId(long recruiterPostedJobsId);
        Task<List<RecruiterPostedJobsAndApplicants>> GetAllByRecruiterPostedJobsId(long recruiterPostedJobsId);
        bool IsExist(long jobSeekerId, long recruiterPostedJobsId);
        int totalApplicants(long recruiterPostedJobsId);
        Task<IEnumerable<RecruiterPostedJobsAndApplicants>> GetByJobSeeker(long jobSeekerId);
    }
}
