﻿
using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JobSeekerWorkExperienceDetailsArrResponseDTO
    {
        public string company { get; set; }
        public string job_title { get; set; }
        public long country_id_enc { get; set; }
        public string city { get; set; }
        public string time_period_from { get; set; }
        public string time_period_to { get; set; }
        public string reason_for_leaving { get; set; }
        public CountryDetails countryDetails { get; set; }
    }
}
