﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class DashboardResponseDTO
    {
        public int newsCount { get; set; }
        public int jobseekersCount { get; set; }
        public int registeredCompaniesCount { get; set; }
        public int registerRequestsCount { get; set; }
        public int employmentApplicationsCount { get; set; }
    }
}
