﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using LinqKit;

namespace Ofwpro.Infrastructure.Repository
{
    public class AdministratorRepository : IAdministratorRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public AdministratorRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Administrator>> GetAll()
        {
            return await unitOfWork.Context.Administrator.ToListAsync();
        }

        public async Task<IEnumerable<Administrator>> Get(long administratorId)
        {
            return await unitOfWork.Context.Administrator.Where(x => !x.IsDeleted && x.AdministratorId == (administratorId == 0 ? x.AdministratorId : administratorId)).ToListAsync();
        }
        
        public long Save(Administrator administrator)
        {
            if (administrator.AdministratorId == 0)
            {
                administrator.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<Administrator>().Add(administrator);
            }
            else
            {
                administrator.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(administrator).State = EntityState.Modified;
                unitOfWork.Context.Administrator.Update(administrator);
            }

            unitOfWork.Commit();
            return administrator.AdministratorId;
        }

        public bool Activate(long administratorId, bool isActive)
        {
            //Administrator administrator = unitOfWork.Context.Administrator.Where(x => x.IsDeleted == false && x.AdministratorId == administratorId).LastOrDefault();

            Administrator administrator = unitOfWork.Context.Administrator.Where(x => x.AdministratorId == administratorId).FirstOrDefault();
   

            if (administrator != null)
            {
                administrator.UpdatedOn = DateTime.Now;
                administrator.IsActive = isActive;
                unitOfWork.Context.Entry(administrator).State = EntityState.Modified;
                unitOfWork.Context.Administrator.Update(administrator);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool Delete (long administratorId)
        {
            Administrator administrator = unitOfWork.Context.Administrator.Where(x=> x.AdministratorId == administratorId).FirstOrDefault();

            if (administrator != null)
            {
                administrator.UpdatedOn = DateTime.Now;
                administrator.IsDeleted = true;
                unitOfWork.Context.Entry(administrator).State = EntityState.Modified;
                unitOfWork.Context.Administrator.Update(administrator);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }




        public bool IsExist(string email, long administratorId)
        {
            var predicate = PredicateBuilder.New<Administrator>(true);

            predicate = predicate.And(i => i.Email == email);
            if (administratorId > 0)
            {
                predicate = predicate.And(i => i.AdministratorId != administratorId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.Administrator.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public bool Validate(string email, string password)
        {
            var predicate = PredicateBuilder.New<Administrator>(true);

            predicate = predicate.And(i => i.Email == email);
            predicate = predicate.And(i => i.Password == password);
            predicate = predicate.And(i => i.IsActive == true);
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.Administrator.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public Task<Administrator> GetByEmail(string email)
        {
            return unitOfWork.Context.Administrator.Where(x => !x.IsDeleted && x.Email == email).FirstOrDefaultAsync();
        }

        public Task<Administrator> GetByAdministratorIdToken(string administratorIdToken)
        {
            return unitOfWork.Context.Administrator.Where(x => !x.IsDeleted && x.AdministratorIdToken == administratorIdToken).FirstOrDefaultAsync();
        }

    }
}
