﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class SpecializationRepository: ISpecializationRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public SpecializationRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<Tuple<IEnumerable<Specialization>,int>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Specialization specializationObj)
        {
            var predicate = PredicateBuilder.New<Specialization>(true);
            var predicateInner = PredicateBuilder.New<Specialization>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                predicateInner = predicateInner.Or(i => i.SpecializationNameEn.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.SpecializationNameAr.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (specializationObj.SpecializationId > 0)
            {
                predicate = predicate.And(i => i.SpecializationId == specializationObj.SpecializationId);
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(specializationObj.SpecializationNameEn))
            {
                predicate = predicate.And(i => i.SpecializationNameEn.Contains(specializationObj.SpecializationNameEn));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(specializationObj.SpecializationNameAr))
            {
                predicate = predicate.And(i => i.SpecializationNameAr.Contains(specializationObj.SpecializationNameAr));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(specializationObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == specializationObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            IEnumerable<Specialization> list = new List<Specialization>();
            int recordsCount = await unitOfWork.Context.Specialization.Where(predicate).CountAsync();
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    list = await this.unitOfWork.Context.Specialization.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).Skip(paging * offset).Take(paging).ToListAsync();
                }
                else
                {
                    list = await this.unitOfWork.Context.Specialization.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).Skip(paging * offset).Take(paging).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    list = await this.unitOfWork.Context.Specialization.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).Skip(paging * offset).Take(paging).ToListAsync();
                }
                else
                {
                    //recruiterList.Skip(paging * offset).Take(paging).ToList();
                    list = await this.unitOfWork.Context.Specialization.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).Skip(paging * offset).Take(paging).ToListAsync();
                }
            }
            return new Tuple<IEnumerable<Specialization>, int>(list, recordsCount);
        }

        public Task<Specialization> GetById(long specializationId)
        {
            return unitOfWork.Context.Specialization.Where(x => !x.IsDeleted && x.SpecializationId == specializationId).FirstOrDefaultAsync();
        }

        public long Save(Specialization specializationObj)
        {
            if (specializationObj.SpecializationId <= 0)
            {
                specializationObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<Specialization>().Add(specializationObj);
            }
            else
            {
                specializationObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(specializationObj).State = EntityState.Modified;
                unitOfWork.Context.Specialization.Update(specializationObj);
            }

            unitOfWork.Commit();
            return specializationObj.SpecializationId;
        }


        public bool Activate(long specializationId, bool isActive)
        {
            Specialization specializationObj = unitOfWork.Context.Specialization.Where(x => x.SpecializationId == specializationId).ToList().LastOrDefault();
            if (specializationObj != null)
            {
                specializationObj.IsActive = isActive;
                specializationObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(specializationObj).State = EntityState.Modified;
                unitOfWork.Context.Specialization.Update(specializationObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long specializationId)
        {
            var specializationObj = unitOfWork.Context.Specialization.Where(x => !x.IsDeleted && x.SpecializationId == specializationId).ToList().LastOrDefault();

            if (specializationObj != null)
            {
                specializationObj.UpdatedOn = DateTime.Now;
                specializationObj.IsDeleted = true;
                unitOfWork.Context.Entry(specializationObj).State = EntityState.Modified;
                unitOfWork.Context.Specialization.Update(specializationObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool IsExist(string SpecializationNameEn, long specializationId)
        {
            var predicate = PredicateBuilder.New<Specialization>(true);

            predicate = predicate.And(i => i.SpecializationNameEn == SpecializationNameEn);
            if (specializationId > 0)
            {
                predicate = predicate.And(i => i.SpecializationId != specializationId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.Specialization.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public async Task<IEnumerable<Specialization>> GetAll()
        {
            var result = unitOfWork.Context.Specialization.Where(x=>!x.IsDeleted).OrderBy(x => x.SpecializationNameEn);
            return await result.ToListAsync();
        }
    }
}
