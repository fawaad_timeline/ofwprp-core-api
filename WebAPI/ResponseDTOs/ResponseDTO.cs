﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTO
{
    public class ResponseDTO<T>
    {
        public int StatusCode { get; set; }
        public int totalRecords { get; set; }
        public bool IsSuccess { get; set; }
        public T Data { get; set; }
        public string Message { get; set; }
        public string ExceptionMessage { get; set; }
    }
}
