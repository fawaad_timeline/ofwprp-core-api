﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class MaritalStatusService : IMaritalStatusService
	{
		private readonly IMaritalStatusRepository _IMaritalStatusRepository;
		public MaritalStatusService(IMaritalStatusRepository iMaritalStatusRepository)
		{
			_IMaritalStatusRepository = iMaritalStatusRepository;
		}

		public async Task<IEnumerable<MaritalStatus>> GetAll()
		{
			return await _IMaritalStatusRepository.GetAll();
		}

		public MaritalStatus GetById(long id)
		{
			return _IMaritalStatusRepository.GetById(id);
		}
		public long Save(MaritalStatus data)
		{
			return _IMaritalStatusRepository.Save(data);
		}
		public bool Delete(long id)
		{
			return _IMaritalStatusRepository.Delete(id);
		}
	}
}
