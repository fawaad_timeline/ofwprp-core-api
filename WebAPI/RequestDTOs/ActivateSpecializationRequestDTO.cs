﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class ActivateSpecializationRequestDTO
    {
        public long id { get; set; }
        public bool active { get; set; }
    }
}
