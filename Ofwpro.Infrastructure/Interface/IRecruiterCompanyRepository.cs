﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IRecruiterCompanyRepository
    {
        Task<IEnumerable<RecruiterCompany>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, RecruiterCompany recruiterCompanyObj);
        Task<RecruiterCompany> GetById(long recruiterCompanyId);
        long Save(RecruiterCompany recruiterCompanyObj);
        bool Activate(long recruiterCompanyId, bool isActive);
        bool Delete(long recruiterCompanyId);
        Task<RecruiterCompany> GetByRecruiterId(long recruiterId);
    }
}
