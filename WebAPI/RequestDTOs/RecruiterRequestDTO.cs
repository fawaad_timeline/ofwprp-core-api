﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class RecruiterRequestDTO
    {
        //public string national_number { get; set; }
        //public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        //public string father_name { get; set; }
        //public string mother_name { get; set; }
        //public DateTime dob { get; set; }
        public string gender { get; set; }
        public string mobile { get; set; }
        public string country_no { get; set; }
        public string country_code { get; set; }
        public string company_name { get; set; }
        public string current_designation { get; set; }
        //public string user_type { get; set; }        
        public string email { get; set; }
        public string password { get; set; }
    }
}
