﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class MaritalStatusResponseDTO
    {
		public string Id
		{
			get;
			set;
		}
		public string Name
		{
			get;
			set;
		}
	}
}
