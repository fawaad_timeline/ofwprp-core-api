﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class JobSeekerQualificationService: IJobSeekerQualificationService
    {
        private readonly IJobSeekerQualificationRepository jobSeekerQualificationRepository;

        public JobSeekerQualificationService(IJobSeekerQualificationRepository jobSeekerQualificationRepository)
        {
            this.jobSeekerQualificationRepository = jobSeekerQualificationRepository;
        }

        public async Task<IEnumerable<JobSeekerQualification>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerQualification jobSeekerQualificationObj)
        {
            return await jobSeekerQualificationRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, jobSeekerQualificationObj);
        }

        public async Task<JobSeekerQualification> GetById(long jobSeekerQualificationId)
        {
            return await jobSeekerQualificationRepository.GetById(jobSeekerQualificationId);
        }

        public long Save(JobSeekerQualification jobSeekerQualificationObj)
        {
            return jobSeekerQualificationRepository.Save(jobSeekerQualificationObj);
        }

        public bool Activate(long jobSeekerQualificationId, bool isActive)
        {
            return jobSeekerQualificationRepository.Activate(jobSeekerQualificationId, isActive);
        }

        public bool Delete(long jobSeekerQualificationId)
        {
            return jobSeekerQualificationRepository.Delete(jobSeekerQualificationId);
        }

        public Task<JobSeekerQualification> GetByJobSeekerId(long jobSeekerId)
        {
            return jobSeekerQualificationRepository.GetByJobSeekerId(jobSeekerId);
        }

    }
}
