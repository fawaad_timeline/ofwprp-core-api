﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class SessionRepository: ISessionRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public SessionRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Session>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Session sessionObj)
        {
            var predicate = PredicateBuilder.New<Session>(true);
            var predicateInner = PredicateBuilder.New<Session>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                //predicateInner = predicateInner.Or(i => i.SessionNameEn.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.SessionNameAr.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (sessionObj.SessionId > 0)
            {
                predicate = predicate.And(i => i.SessionId == sessionObj.SessionId);
                isPradicate = true;
            }
            //if (!string.IsNullOrEmpty(sessionObj.SessionNameEn))
            //{
            //    predicate = predicate.And(i => i.SessionNameEn.Contains(sessionObj.SessionNameEn));
            //    isPradicate = true;
            //}
            //if (!string.IsNullOrEmpty(sessionObj.SessionNameAr))
            //{
            //    predicate = predicate.And(i => i.SessionNameAr.Contains(sessionObj.SessionNameAr));
            //    isPradicate = true;
            //}
            if (!string.IsNullOrEmpty(sessionObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == sessionObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.Session.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.Session.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.Session.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.Session.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<Session> GetById(long sessionId)
        {
            return unitOfWork.Context.Session.Where(x => !x.IsDeleted && x.SessionId == sessionId).FirstOrDefaultAsync();
        }

        public long Save(Session sessionObj)
        {
            if (sessionObj.SessionId <= 0)
            {
                sessionObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<Session>().Add(sessionObj);
            }
            else
            {
                sessionObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(sessionObj).State = EntityState.Modified;
                unitOfWork.Context.Session.Update(sessionObj);
            }

            unitOfWork.Commit();
            return sessionObj.SessionId;
        }


        public bool Activate(long sessionId, bool isActive)
        {
            Session sessionObj = unitOfWork.Context.Session.Where(x => x.SessionId == sessionId).ToList().LastOrDefault();
            if (sessionObj != null)
            {
                sessionObj.IsActive = isActive;
                sessionObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(sessionObj).State = EntityState.Modified;
                unitOfWork.Context.Session.Update(sessionObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long sessionId)
        {
            var sessionObj = unitOfWork.Context.Session.Where(x => !x.IsDeleted && x.SessionId == sessionId).ToList().LastOrDefault();

            if (sessionObj != null)
            {
                sessionObj.UpdatedOn = DateTime.Now;
                sessionObj.IsDeleted = true;
                unitOfWork.Context.Entry(sessionObj).State = EntityState.Modified;
                unitOfWork.Context.Session.Update(sessionObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool IsExist(string SessionNameEn, long sessionId)
        {
            var predicate = PredicateBuilder.New<Session>(true);

            //predicate = predicate.And(i => i.SessionNameEn == SessionNameEn);
            if (sessionId > 0)
            {
                predicate = predicate.And(i => i.SessionId != sessionId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.Session.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public bool DeleteSessionBasedOnUser(long jobSeekerId, string userType)
        {
            var sessionObj = unitOfWork.Context.Session.Where(x => !x.IsDeleted && x.UserId == jobSeekerId && x.UserType == userType).ToList().LastOrDefault();

            if (sessionObj != null)
            {
                sessionObj.UpdatedOn = DateTime.Now;
                sessionObj.IsDeleted = true;
                unitOfWork.Context.Entry(sessionObj).State = EntityState.Modified;
                unitOfWork.Context.Session.Update(sessionObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public Task<Session> GetByToken(string token)
        {
            return unitOfWork.Context.Session.Where(x => !x.IsDeleted && x.Token == token).FirstOrDefaultAsync();
        }

        public bool DeleteByToken(string token)
        {
            var sessionObj = unitOfWork.Context.Session.Where(x => !x.IsDeleted && x.Token == token).ToList().LastOrDefault();

            if (sessionObj != null)
            {
                sessionObj.UpdatedOn = DateTime.Now;
                sessionObj.IsDeleted = true;
                unitOfWork.Context.Entry(sessionObj).State = EntityState.Modified;
                unitOfWork.Context.Session.Update(sessionObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }


        public Task<Session> GetByUserId(long userId)
        {
            return unitOfWork.Context.Session.Where(x => !x.IsDeleted && x.UserId == userId).FirstOrDefaultAsync();
        }
    }
}
