﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class RecruiterPostedJobsSaveRequestDTO
    {
        public string token { get; set; }
        public long recruiter_posted_jobs_id_enc { get; set; }
        //public string reference_no { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public long country_id_enc { get; set; }
        public string city { get; set; }
        public long employment_type { get; set; }
        public long company_type { get; set; }
        public long salary_range_id_enc { get; set; }
        public long experience_level { get; set; }
        public long roles_job_id_enc { get; set; }
        //public string roles_job_other { get; set; }
    }
}
