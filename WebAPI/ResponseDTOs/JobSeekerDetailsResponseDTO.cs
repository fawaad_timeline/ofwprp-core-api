﻿
using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JobSeekerDetailsResponseDTO
    {     
        public object basic_information { get; set; }
        public string country_name{ get; set; }
        public JobSeekerPersonalInformation personal_information { get; set; }
        public JobSeekerQualification qualification { get; set; }

        //public JobSeekerWorkExperience workExperience { get; set; }
        public List<JobSeekerWorkExperienceDetailsArrResponseDTO> work_experience { get; set; }
        public AttachmentJobSeekerResponseDTO attachmentJobseeker { get; set; }

    }
}
