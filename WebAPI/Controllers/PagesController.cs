﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Ofwpro.Core;
using Ofwpro.Services.Interfaces;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class PagesController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IUserService userService;
        private readonly IFileUploadService fileUploadService;
        private readonly IUserResolverService userResolverService;
        private readonly IPagesService pagesService;
        private readonly IAdministratorService administratorService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public PagesController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IUserService userService, IFileUploadService fileUploadService, IUserResolverService userResolverService, IPagesService pagesService, IAdministratorService administratorService, IMapper mapper, IConfiguration config)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.userService = userService;
            this.fileUploadService = fileUploadService;
            this.userResolverService = userResolverService;
            this.pagesService = pagesService;
            this.administratorService = administratorService;
            this.mapper = mapper;
            this.config = config;
        }


        [HttpPost("en/pages/SavePages")]
        public async Task<IActionResult> SavePages([FromBody] PagesRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            Pages pagesObjOld = null;
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been saved successfully";
            string ExceptionMessage = "";

            string lan = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.PagesManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                if (isSuccess == true && model.Id > 0)
                {
                    pagesObjOld = await pagesService.GetById(model.Id);
                    if (pagesObjOld == null)
                    {
                        StatusCode = 404;
                        isSuccess = false;
                        Message = "No data found";
                        ExceptionMessage = "";
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(model.ContactUs))
                    {
                        StatusCode = 400;
                        isSuccess = false;
                        Message = "Please provide contact us data.";
                        ExceptionMessage = "";
                    } else if (string.IsNullOrEmpty(model.AboutUs))
                    {
                        StatusCode = 400;
                        isSuccess = false;
                        Message = "Please provide about us data.";
                        ExceptionMessage = "";
                    }
                }
                              

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    Pages pagesObj = mapper.Map<Pages>(model);
                    if (model.Id > 0)
                    {
                        if (pagesObjOld != null)
                        {
                            pagesObj.CreatedOn = pagesObjOld.CreatedOn;
                            pagesObj.CreatedBy = pagesObjOld.CreatedBy;
                            pagesObj.IsDeleted = pagesObjOld.IsDeleted;
                            pagesObj.UpdatedBy = currentUser.Id;
                        }
                    }
                    else
                    {
                        pagesObj.CreatedOn = DateTime.Now;
                        pagesObj.IsDeleted = false;
                        if (string.IsNullOrEmpty(pagesObj.CreatedBy))
                        {
                            if (currentUser != null)
                            {
                                pagesObj.CreatedBy = currentUser.Id;
                            }
                        }
                    }

                    // Save pages                   
                    long sessionId = pagesService.Save(pagesObj);

                    if (sessionId < 0)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be saved.";
                        ExceptionMessage = "";
                    }
                    else
                    {
                        // Retrieve pages
                        pagesObj = await pagesService.GetById(sessionId);
                        PagesResponseDTO PagesResponseDTOObj = mapper.Map<PagesResponseDTO>(pagesObj);
                        Data = PagesResponseDTOObj;
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while saving data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        
    }
}