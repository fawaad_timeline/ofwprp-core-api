﻿
jQuery(function ($) {
    if ($('#userpic').length > 0) {
        var cropCoordinates;
        $('#userpic').fileapi({
            url: '/',
            accept: 'image/*',
            autoUpload: false,
            imageSize: {
                minWidth: 100,
                minHeight: 100,
            },
            elements: {
                active: { show: '.js-upload', hide: '.js-browse' },
                preview: {
                    el: '.js-preview',
                    width: 200,
                    height: 200
                },
                progress: '.js-progress'
            },
            onSelect: function (evt, ui) {
                debugger;
                var file = ui.files[0];
                if (!FileAPI.support.transform) {
                    alert('Your browser does not support Flash :(');
                }
                else if (file) {
                    var maxPopupHeight = $(window).height() - 200 - 100;
                    var maxPopupWidth = $(window).width() - 200;

                    //if (maxPopupHeight > 350) maxPopupHeight = 350;
                    //if (maxPopupWidth > 350) maxPopupWidth = 350;

                   // $('#popup').attr('style', 'max-width:' + maxPopupWidth + 'px; max-height:' + maxPopupHeight + 'px');

                    $('#popup').modal({
                        closeOnEsc: true,
                        closeOnOverlayClick: false,
                        onOpen: function (overlay) {
                            $(overlay).on('click', '.js-upload', function () {
                                $.modal().close();
                                $('#userpic').fileapi('upload');
                            });
                            $('.js-img', overlay).cropper({
                                file: file,
                                bgColor: '#fff',
                                maxSize: [maxPopupWidth, maxPopupHeight],
                                minSize: [50, 50],
                                selection: '90%',
                                onSelect: function (coords) {
                                    $('#userpic').fileapi('crop', file, coords);
                                    cropCoordinates = coords;
                                }
                            });
                        }
                    }).open();
                }
            },
            onFileUpload: function (evnt, uiEvt) {
                if (cropCoordinates != undefined) {

                    var fileList = Array.prototype.map.call(uiEvt.files, function (file) {
                        var result = {
                            lastModified: new Date(file.lastModified).toISOString(),
                            name: file.name,
                            size: file.size,
                            type: file.type,
                            relativePath: file.webkitRelativePath
                        };

                        // Attach the blob data itself as a non-enumerable property so it doesn't appear in the JSON
                        Object.defineProperty(result, 'blob', { value: file });

                        return result;
                    });
                    var files = FileListItems(uiEvt.files);

                    var file = uiEvt.file;

                    BlobToBase64(file, function (base64String) {
                        
                        GetCroppedImage(cropCoordinates, base64String, function (fileDataURI) {
                            var croppedFile = DataURIToFile(fileDataURI, 'cropped-' + file.name);

                            var croppedFiles = [];
                            croppedFiles.push(croppedFile);

                            files = FileListItems(croppedFiles);

                            $("#uploadPicture").prop("files", files);
                            $("#uploadPicture").trigger("change");
                            var event = new Event('change');
                            document.getElementById('uploadPicture').dispatchEvent(event);
                        });

                    });
                }
            }
        });
    }
});

var GetCroppedImage = function (cropCoordinates, dataURI, callback) {
    debugger;
    var w = cropCoordinates.w;
    var h = cropCoordinates.h;
    var lh = cropCoordinates.lh;
    var lw = cropCoordinates.lw;
    var x = cropCoordinates.x;
    var y = cropCoordinates.y;

    var dataURL;

    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');

    var img = new Image();
    img.onload = function () {
        canvas.height = h;
        canvas.width = w;
        context.drawImage(img, x, y, w, h, 0, 0, w, h);
        dataURL = canvas.toDataURL();

        callback(dataURL);
    };
    img.src = dataURI;
};

var BlobToBase64 = function (blob, callback) {
    var reader = new FileReader();
    reader.onload = function () {
        var dataUrl = reader.result;
        var base64 = dataUrl.split(',')[1];
        callback(dataUrl);
    };
    reader.readAsDataURL(blob);
};

function DataURIToFile(dataurl, filename) {

    var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);

    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
}


function Base64toBlob(dataURI,filename) {
    debugger;
    var block = dataURI.split(";");
    // Get the content type
    var contentType = block[0].split(":")[1];// In this case "image/gif"
    // get the real base64 content of the file
    var realData = block[1].split(",")[1];


    contentType = contentType || '';
    sliceSize = 512;

    var byteCharacters = atob(realData);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    debugger;

    var blob = new File(byteArrays, filename, { type: contentType });
    return blob;
}

function FileListItems(files) {
    var b = new ClipboardEvent("").clipboardData || new DataTransfer()
    for (var i = 0, len = files.length; i < len; i++) b.items.add(files[i])
    return b.files
}




function wrapInput(elementId) {
    document.getElementById(elementId).click();
}

function createAlert() {
    alert("Hello");
}


function moveToPersonalInfoTab(nextTab) {
    document.getElementById(nextTab).click();
}

function moveToQualificationTab(nextTab) {
    document.getElementById(nextTab).click();
}

function moveToPersonalInfoTab(nextTab) {
    document.getElementById(nextTab).click();
}

function moveToWorkExperienceTab(nextTab) {
    document.getElementById(nextTab).click();
}

function moveToAttachmentsTab(nextTab) {
    document.getElementById(nextTab).click();
}

function moveToNextTab(nextTab) {
    document.getElementById(nextTab).click();
}

function moveToPostedJobsTab(nextTab) {
    document.getElementById(nextTab).click();
    window.location.reload();
}

function moveToMessagesTab(nextTab) {
    document.getElementById(nextTab).click();
    window.location.reload();
}
function refreshOwlTestimonial(testimonialCarousal) {
    var caraousel = document.getElementsByClassName(testimonialCarousal);
}

function show_hide(divId1, divId2) {
    document.getElementById(divId1).style.display = "none";
    document.getElementById(divId2).style.display = "block";
}

function openPopup(elementClass1, elementClass2, elementClass3) {
    const shareButton = document.querySelector('.' + elementClass1);
    const shareDialog = document.querySelector('.' + elementClass2);
    const closeButton = document.querySelector('.' + elementClass3);

    shareButton.addEventListener('click', event => {
        shareDialog.classList.add('is-open');
    });

    closeButton.addEventListener('click', event => {
        shareDialog.classList.remove('is-open');
    });

    let url = "http://localhost:54965/JobSeeker/VLYp8W1V36TMmFaUoKGT3A==";

    //navigator.share({ url: url });

    if (navigator.share) {
        navigator.share({
            title: 'web.dev',
            text: 'Check out web.dev.',
            url: url,
        })
            .then(() => console.log('Successful share'))
            .catch((error) => console.log('Error sharing', error));
    }
}

function shareFiles(URL) {
    debugger;
    let url = URL;

    if (navigator.share) {
        navigator.share({
            title: 'web.dev',
            text: 'Check out web.dev.',
            url: url,
        })
            .then(() => console.log('Successful share'))
            .catch((error) => console.log('Error sharing', error));
    }
}

//window.onunload = () => {
//    // Clear the local storage
//    //localStorage.clear();
//}

//window.onbeforeunload = function (e) {

//    window.localStorage.unloadTime = JSON.stringify(new Date());

//};

//window.onload = function () {

//    let loadTime = new Date();
//    let unloadTime = new Date(JSON.parse(window.localStorage.unloadTime));
//    let refreshTime = loadTime.getTime() - unloadTime.getTime();

//    if (refreshTime > 3000)//3000 milliseconds
//    {
//        window.localStorage.clear();
//    }

//};
