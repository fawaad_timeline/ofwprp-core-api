﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IPagesRepository
    {
        Task<IEnumerable<Pages>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Pages pagesObj);
        Task<Pages> GetById(long pagesId);
        long Save(Pages pagesObj);
        bool Activate(long pagesId, bool isActive);
        bool Delete(long pagesId);
        bool IsExist(string pagesName, long PagesId);
        Task<IEnumerable<Pages>> GetAll();
    }
}
