﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class MessageGeneral
    {
        protected long messageGeneralId, jobSeekerId, userId;
        protected string userType, message, createdBy, updatedBy;
        protected DateTime createdOn, updatedOn;
        protected bool isRead, isActive, isDeleted, isDeletedJobSeeker, isDeletedRecruiter;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long MessageGeneralId { get => messageGeneralId; set => messageGeneralId = value; }
        public long JobSeekerId { get => jobSeekerId; set => jobSeekerId = value; }
        public long UserId { get => userId; set => userId = value; }
        public string UserType { get => userType; set => userType = value; }
        public string Message { get => message; set => message = value; }
        public bool IsRead { get => isRead; set => isRead = value; }

        public bool IsActive { get => isActive; set => isActive = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public bool IsDeletedJobSeeker { get => isDeletedJobSeeker; set => isDeletedJobSeeker = value; }
        public bool IsDeletedRecruiter { get => isDeletedRecruiter; set => isDeletedRecruiter = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }
    }
}
