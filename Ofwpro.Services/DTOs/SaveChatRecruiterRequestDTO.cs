﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ofwpro.Services.DTOs
{
    public class SaveChatRecruiterRequestDTO
    {
        public long recruiter_id_enc { get; set; }

        public string message_from { get; set; }
        public string sender_name { get; set; }
        public long job_seeker_id_enc { get; set; }
        public long tbl_recruiter_id { get; set; }
        public int total_messages_unread { get; set; }
        public string is_new { get; set; }
        public string message { get; set; }
        public DateTime added_date { get; set; }

    }
}
