﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using MimeKit;
using MailKit.Net.Smtp;
using PusherServer;
using Ofwpro.Services.Interfaces;
using Ofwpro.Core;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class MessageAgainstJobController : ControllerBase
    {
        private readonly IFileUploadService fileUploadService;
        private readonly IJobSeekerService jobSeekerService;
        private readonly IAdministratorService administratorService;
        private readonly ISessionService sessionService;
        private readonly IJobSeekerPersonalInformationService jobSeekerPersonalInformationService;
        private readonly IJobSeekerQualificationService jobSeekerQualificationService;
        private readonly IJobSeekerWorkExperienceService jobSeekerWorkExperienceService;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IJobSeekerAttachmentService jobSeekerAttachmentService;
        private readonly IRecruiterPostedJobsService recruiterPostedJobsService;
        private readonly IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService;
        private readonly ICountryDetailsService countryDetailsService;
        private readonly IRecruiterAttachmentService recruiterAttachmentService;
        private readonly IForgotPasswordService forgotPasswordService;
        private readonly IMessageGeneralService messageGeneralService;
        private readonly IRecruiterService recruiterService;
        private readonly IRecruiterCompanyService recruiterCompanyService;
        private readonly IMessageAgainstJobService messageAgainstJobService;
        private readonly INotificationService notificationService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public MessageAgainstJobController(IFileUploadService fileUploadService, IJobSeekerService jobSeekerService, IAdministratorService administratorService, ISessionService sessionService, IJobSeekerPersonalInformationService jobSeekerPersonalInformationService, IJobSeekerQualificationService jobSeekerQualificationService, IJobSeekerWorkExperienceService jobSeekerWorkExperienceService, IWebHostEnvironment hostingEnvironment, IJobSeekerAttachmentService jobSeekerAttachmentService, IRecruiterPostedJobsService recruiterPostedJobsService, IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService, ICountryDetailsService countryDetailsService, IRecruiterAttachmentService recruiterAttachmentService, IForgotPasswordService forgotPasswordService, IMessageGeneralService messageGeneralService, IRecruiterService recruiterService, IRecruiterCompanyService recruiterCompanyService, IMessageAgainstJobService messageAgainstJobService, INotificationService notificationService, IMapper mapper, IConfiguration config)
        {
            this.fileUploadService = fileUploadService;
            this.jobSeekerService = jobSeekerService;
            this.administratorService = administratorService;
            this.sessionService = sessionService;
            this.jobSeekerPersonalInformationService = jobSeekerPersonalInformationService;
            this.jobSeekerQualificationService = jobSeekerQualificationService;
            this.jobSeekerWorkExperienceService = jobSeekerWorkExperienceService;
            this.hostingEnvironment = hostingEnvironment;
            this.jobSeekerAttachmentService = jobSeekerAttachmentService;
            this.recruiterPostedJobsService = recruiterPostedJobsService;
            this.recruiterPostedJobsAndApplicantsService = recruiterPostedJobsAndApplicantsService;
            this.countryDetailsService = countryDetailsService;
            this.recruiterAttachmentService = recruiterAttachmentService;
            this.forgotPasswordService = forgotPasswordService;
            this.messageGeneralService = messageGeneralService;
            this.recruiterService = recruiterService;
            this.recruiterCompanyService = recruiterCompanyService;
            this.messageAgainstJobService = messageAgainstJobService;
            this.notificationService = notificationService;
            this.mapper = mapper;
            this.config = config;
        }

        [HttpPost]
        [Route("en/message_against_job/ajax_save_message_mobile")]
        public async Task<JsonObjResponseDTO> ajax_save_message_mobile([FromBody] SaveMessageMobileRequestDTO model)
        {
            string token = model.token;
            long recruiter_posted_jobs_and_applicants_id_enc = model.recruiter_posted_jobs_and_applicants_id_enc;
            string user_type = model.user_type;
            string message = model.message;
            string os = model.os;
            JsonResponseObjectApi _response = new JsonResponseObjectApi();

            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            Recruiter recruiterObj = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            Dictionary<object, object> paramsDict = new Dictionary<object, object>();
            Dictionary<object, object> ArrDict = new Dictionary<object, object>();

            JsonObjResponseDTO JsonObjResponseDTOObj = new JsonObjResponseDTO();
            DateTime todayDateTime = DateTime.Now;
            
            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonObjResponseDTOObj.code = "2";
                        JsonObjResponseDTOObj.MSG = "Invalid token";
                        return JsonObjResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonObjResponseDTOObj.code = "2";
                            JsonObjResponseDTOObj.MSG = "Invalid token";
                            return JsonObjResponseDTOObj;
                        }
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(Convert.ToString(recruiter_posted_jobs_and_applicants_id_enc)) || recruiter_posted_jobs_and_applicants_id_enc<=0)
                    {
                        JsonObjResponseDTOObj.code = "0";
                        JsonObjResponseDTOObj.MSG = "[recruiter_posted_jobs_and_applicants_id_enc] is blank. Please enter [recruiter_posted_jobs_and_applicants_id_enc].";
                        return JsonObjResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(user_type))
                    {
                        JsonObjResponseDTOObj.code = "0";
                        JsonObjResponseDTOObj.MSG = "[user_type] is blank. Please enter [user_type].";
                        return JsonObjResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(message))
                    {
                        JsonObjResponseDTOObj.code = "0";
                        JsonObjResponseDTOObj.MSG = "Message is blank. Please enter Message.";
                        return JsonObjResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    if (user_type.Equals("job_seeker"))
                    {
                        JobSeeker jobSeekerObj = await jobSeekerService.GetById(sessionObj.UserId);
                        RecruiterPostedJobs recruiterPostedJobsObj = new RecruiterPostedJobs();

                        RecruiterPostedJobsAndApplicants recruiterPostedJobsAndApplicantsObj = await recruiterPostedJobsAndApplicantsService.GetById(recruiter_posted_jobs_and_applicants_id_enc);
                        if (recruiterPostedJobsAndApplicantsObj != null)
                        {
                            recruiterPostedJobsObj = await recruiterPostedJobsService.GetById(recruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsId);
                        }

                        //int totalMessagesUnread = messageAgainstJobService.GetTotalMessagesAgainstJobUnread(recruiter_posted_jobs_and_applicants_id_enc, "recruiter");
                        int totalMessagesUnread = messageAgainstJobService.GetTotalMessagesAgainstJobUnRead(recruiter_posted_jobs_and_applicants_id_enc, "recruiter");

                        paramsDict = new Dictionary<object, object>();
                        paramsDict.Add("unread_count", totalMessagesUnread);
                        paramsDict.Add("message", message);
                        paramsDict.Add("job_seeker_id_enc", sessionObj.UserId);
                        paramsDict.Add("recruiter_id_enc", recruiterPostedJobsObj.RecruiterId);
                        object data = new object();
                        Recruiter recruiterObj_ = await recruiterService.GetById(recruiterPostedJobsObj.RecruiterId);
                        
                        _response.added_date = todayDateTime;
                        _response.id = 0;
                        _response.message = message;
                        _response.total_messages_unread = totalMessagesUnread;
                        _response.message_from = jobSeekerObj.FirstName + " " + jobSeekerObj.LastName;
                        _response.user_type = model.user_type;
                        _response.job_seeker_id_enc = jobSeekerObj.JobSeekerId;
                        _response.recruiter_id_enc = recruiterPostedJobsObj.RecruiterId;
                        _response.sender_name = jobSeekerObj.FirstName + " " + jobSeekerObj.LastName;

                        Session sessionRecruiterObj = await sessionService.GetByUserId(recruiterPostedJobsObj.RecruiterId);
                        _response.recruiter_posted_jobs_and_applicants_id_enc  = recruiterPostedJobsObj.RecruiterPostedJobsId;
                        if (model.os == "IOS")
                        { 
                            await notificationService.SendNotificationToRecruiterAsync(recruiterObj_.AppleDeviceToken,"", _response, totalMessagesUnread, message, os, sessionRecruiterObj, jobSeekerObj, recruiterObj_);
                        }
                        else if (model.os == "android")
                        { 
                            await notificationService.SendNotificationToRecruiterAsync(recruiterObj_.AndroidDeviceToken,"", _response, totalMessagesUnread, message, os, sessionRecruiterObj, jobSeekerObj, recruiterObj_);
                        }

                        recruiterObj = recruiterObj_;
                        // CURL CODE
                        //$ch = curl_init(HOST_URL."/".LAN_SEL."/notifications/send_notification_to_recruiter");
                        //curl_setopt($ch, CURLOPT_POST, count($params));
                        //curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
                        //$response = curl_exec($ch);

                        ArrDict = new Dictionary<object, object>();
                        ArrDict.Add("title", recruiterPostedJobsObj.Title);
                        ArrDict.Add("message_from", "job_seeker");
                        ArrDict.Add("sender_name", jobSeekerObj.FirstName+" "+ jobSeekerObj.LastName);
                        ArrDict.Add("job_seeker_id_enc", sessionObj.UserId);
                        ArrDict.Add("recruiter_id_enc", recruiterPostedJobsObj.RecruiterId);
                        ArrDict.Add("recruiter_posted_jobs_id_enc", recruiterPostedJobsObj.RecruiterPostedJobsId);
                        ArrDict.Add("recruiter_posted_jobs_and_applicants_id_enc", recruiter_posted_jobs_and_applicants_id_enc);
                        ArrDict.Add("total_messages_unread", totalMessagesUnread);
                        ArrDict.Add("message", message);
                        ArrDict.Add("added_date", todayDateTime);

                    } 
                    else if (user_type.Equals("recruiter"))
                    {
                        long recruiterId = sessionObj.UserId;
                        long jobSeekerId = 0;
                        Recruiter RecruiterObj = await recruiterService.GetById(recruiterId);
                        RecruiterPostedJobs RecruiterPostedJobsObj = new RecruiterPostedJobs();

                        RecruiterPostedJobsAndApplicants RecruiterPostedJobsAndApplicantsObj = await recruiterPostedJobsAndApplicantsService.GetById(recruiter_posted_jobs_and_applicants_id_enc);
                        if (RecruiterPostedJobsAndApplicantsObj != null)
                        {
                            RecruiterPostedJobsObj = await recruiterPostedJobsService.GetById(RecruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsId);
                            jobSeekerId = RecruiterPostedJobsAndApplicantsObj.JobSeekerId;
                            recruiterObj = await recruiterService.GetById(sessionObj.UserId);
                        }

                        string title = "";
                        if (RecruiterPostedJobsObj != null)
                        {
                            title = RecruiterPostedJobsObj.Title;
                        }

                        int totalMessagesUnread =  messageAgainstJobService.GetTotalMessagesAgainstJobUnRead(recruiter_posted_jobs_and_applicants_id_enc, "job_seeker");

                        paramsDict = new Dictionary<object, object>();
                        paramsDict.Add("unread_count", totalMessagesUnread);
                        paramsDict.Add("message", message);
                        paramsDict.Add("job_seeker_id_enc", jobSeekerId);
                        paramsDict.Add("recruiter_id_enc", recruiterId);
                        
                        JobSeeker jobSeekerObj_ = await jobSeekerService.GetById(jobSeekerId);
                        Session sessionJobSeekerObj = await sessionService.GetByUserId(jobSeekerId);
                        object data = new object();
                        
                        _response.added_date = todayDateTime;
                        _response.id = 0;
                        _response.message = message;
                        _response.message_from = recruiterObj.FirstName + " " + recruiterObj.LastName;
                        _response.total_messages_unread = totalMessagesUnread;
                        _response.user_type = model.user_type;
                        _response.job_seeker_id_enc = jobSeekerId;
                        _response.recruiter_id_enc = recruiterObj.RecruiterId;
                        _response.sender_name = recruiterObj.FirstName + " " + recruiterObj.LastName;

                        Session sessionRecruiterObj = await sessionService.GetByUserId(RecruiterPostedJobsObj.RecruiterId);
                        _response.recruiter_posted_jobs_and_applicants_id_enc = RecruiterPostedJobsObj.RecruiterPostedJobsId;
                        await notificationService.SendNotificationToJobSeekerAsync(jobSeekerObj_.AppleDeviceToken, "", _response, totalMessagesUnread, message, os, sessionJobSeekerObj, jobSeekerObj_, RecruiterObj);

                        //$ch = curl_init(HOST_URL."/".LAN_SEL."/notifications/send_notification_to_job_seeker");
                        //curl_setopt($ch, CURLOPT_POST, count($params));
                        //curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
                        //$response = curl_exec($ch);
                        //curl_close($ch);

                        ArrDict = new Dictionary<object, object>();
                        ArrDict.Add("title", title);
                        ArrDict.Add("message_from", "recruiter");
                        ArrDict.Add("sender_name", RecruiterObj.CompanyName);
                        ArrDict.Add("job_seeker_id_enc", jobSeekerId);
                        ArrDict.Add("recruiter_id_enc", recruiterId);
                        ArrDict.Add("recruiter_posted_jobs_id_enc", RecruiterPostedJobsObj.RecruiterPostedJobsId);
                        ArrDict.Add("recruiter_posted_jobs_and_applicants_id_enc", recruiter_posted_jobs_and_applicants_id_enc);
                        ArrDict.Add("total_messages_unread", totalMessagesUnread);
                        ArrDict.Add("message", message);
                        ArrDict.Add("added_date", DateTime.Today);
                    }
                    
                    MessageAgainstJob messageAgainstJobObj = new MessageAgainstJob();
                    messageAgainstJobObj.RecruiterPostedJobsAndApplicantsId = recruiter_posted_jobs_and_applicants_id_enc;
                    messageAgainstJobObj.UserType = user_type;
                    messageAgainstJobObj.Message = message;
                    messageAgainstJobObj.IsActive = true;
                    messageAgainstJobObj.IsDeleted = false;
                    messageAgainstJobObj.CreatedOn = todayDateTime;
                    long  massgeId = messageAgainstJobService.Save(messageAgainstJobObj);

                    //var options = new PusherOptions();
                    //options.Cluster = "eu";

                    //var pusher = new Pusher("b696c420f9702d42cb44", "9f75c505be6189722330", "627281", options);
                    //Task<ITriggerResult> resultTask = pusher.TriggerAsync("my-channel", "my-event", ArrDict);


                    JsonObjResponseDTOObj.code = "1";
                    JsonObjResponseDTOObj.MSG = "Chat messages sent successfully";
                    JsonObjResponseDTOObj.data = _response;
                    return JsonObjResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonObjResponseDTOObj;
            }

            return JsonObjResponseDTOObj;
        }


        [HttpPost]
        [Route("en/message_against_job/all_messages_against_job_mobile")]
        public async Task<object> all_messages_against_job_mobile([FromBody] MessageAgainstJobMobileRequestDTO model)
        {
            string token = model.token;  long recruiter_posted_jobs_and_applicants_id_enc = model.recruiter_posted_jobs_and_applicants_id_enc;  int offset = model.offset; int pagination = model.pagination;

            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            Recruiter recruiterObj = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            Dictionary<object, object> paramsDict = new Dictionary<object, object>();
            Dictionary<object, object> ArrDict = new Dictionary<object, object>();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }
                }

                if (offset < 0) { offset = 0; }
                if (pagination <= 0) { pagination = 50; }
                
                string hostUrl = config["Utility:APIBaseURL"];
                
                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    IEnumerable<MessageAgainstJob> messageAgainstJobList = await messageAgainstJobService.GetAllMessagesAgainstJob("CreatedOn", "desc", "", true, recruiter_posted_jobs_and_applicants_id_enc);
                    messageAgainstJobList = messageAgainstJobList.Skip(pagination * offset).Take(pagination).ToList();

                    messageAgainstJobList = messageAgainstJobList.Reverse();

                    if (sessionObj.UserType.Equals("job_seeker"))
                    {
                        messageAgainstJobService.MarkMessageRead(recruiter_posted_jobs_and_applicants_id_enc, "recruiter");
                    }else
                    {
                        messageAgainstJobService.MarkMessageRead(recruiter_posted_jobs_and_applicants_id_enc, "job_seeker");
                    }

                    string userNameJobSeeker = "";
                    string userNameRecruiter = "";
                    string userName = "";
                    List<Dictionary<string, string>> arr = new List<Dictionary<string, string>>();

                    foreach (MessageAgainstJob messageAgainstJobObj in messageAgainstJobList)
                    {
                        Recruiter recruiterObj_;
                        JobSeeker jobSeekerObj_;
                        RecruiterPostedJobsAndApplicants RecruiterPostedJobsAndApplicants_ = await recruiterPostedJobsAndApplicantsService.GetById(messageAgainstJobObj.RecruiterPostedJobsAndApplicantsId);
                        long jobSeekerId = RecruiterPostedJobsAndApplicants_.JobSeekerId;
                        long recruiterPostedJobsId = RecruiterPostedJobsAndApplicants_.RecruiterPostedJobsId;
                        if (string.IsNullOrEmpty(userNameJobSeeker) && messageAgainstJobObj.UserType.Equals("job_seeker"))
                        {
                            jobSeekerObj_ = await jobSeekerService.GetById(jobSeekerId);
                            userNameJobSeeker = jobSeekerObj_.FirstName + " " + jobSeekerObj_.LastName;
                        }

                        RecruiterPostedJobs RecruiterPostedJobsObj = null;
                        if (string.IsNullOrEmpty(userNameRecruiter) && messageAgainstJobObj.UserType.Equals("recruiter"))
                        {
                            RecruiterPostedJobsObj = await recruiterPostedJobsService.GetById(recruiterPostedJobsId);
                            long recruiterId_ = RecruiterPostedJobsObj.RecruiterId;
                            Recruiter RecruiterObj_ = await recruiterService.GetById(recruiterId_);
                            userNameRecruiter = RecruiterObj_.CompanyName;
                        }

                        string icon = "";
                        if (messageAgainstJobObj.UserType.Equals("job_seeker"))
                        {
                            icon = hostUrl+ "/images/icons/icon_job_seeker.png";
                            userName = userNameJobSeeker;
                        } else
                        {
                            icon = hostUrl + "/images/icons/icon_recruiter.png";
                            userName = userNameRecruiter;
                        }
                        Dictionary<string, string> dictData = new Dictionary<string, string>();
                        dictData.Add("id",Convert.ToString(messageAgainstJobObj.MessageAgainstJobId));
                        dictData.Add("user_type", messageAgainstJobObj.UserType);
                        dictData.Add("message", messageAgainstJobObj.Message);
                        dictData.Add("added_date", Convert.ToString(messageAgainstJobObj.CreatedOn));
                        dictData.Add("message_from", messageAgainstJobObj.UserType);
                        if (RecruiterPostedJobsObj != null)
                        {
                            dictData.Add("recruiter_posted_jobs_id_enc", Convert.ToString(RecruiterPostedJobsObj.RecruiterPostedJobsId));
                        } else
                        {
                            dictData.Add("recruiter_posted_jobs_id_enc", "");
                        }
                        arr.Add(dictData);
                    }

                    MessageAgainstJobResponseDTO messageAgainstJobResponseDTOObj = new MessageAgainstJobResponseDTO();

                    messageAgainstJobResponseDTOObj.code = "1";
                    messageAgainstJobResponseDTOObj.MSG = "Success";
                    messageAgainstJobResponseDTOObj.messages = arr;
                    return messageAgainstJobResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/message_against_job/mark_message_read")]
        public async Task<object> mark_message_read([FromBody] MarkMessageReadRequestDTO model)
        {
            string token = model.token; long recruiter_posted_jobs_and_applicants_id_enc = model.recruiter_posted_jobs_and_applicants_id_enc;
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            Recruiter recruiterObj = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            Dictionary<object, object> paramsDict = new Dictionary<object, object>();
            Dictionary<object, object> ArrDict = new Dictionary<object, object>();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(Convert.ToString(recruiter_posted_jobs_and_applicants_id_enc)) || recruiter_posted_jobs_and_applicants_id_enc <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "[recruiter_posted_jobs_and_applicants_id_enc] is blank. Please enter [recruiter_posted_jobs_and_applicants_id_enc].";
                        return JsonArrResponseDTOObj;
                    }                    
                }
                
                string hostUrl = config["Utility:APIBaseURL"];

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    if (sessionObj.UserType.Equals("job_seeker"))
                    {
                        messageAgainstJobService.MarkMessageRead(recruiter_posted_jobs_and_applicants_id_enc, "recruiter");
                    }
                    else
                    {
                        messageAgainstJobService.MarkMessageRead(recruiter_posted_jobs_and_applicants_id_enc, "job_seeker");
                    }

                    MessageAgainstJobResponseDTO messageAgainstJobResponseDTOObj = new MessageAgainstJobResponseDTO();

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Success";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }
    }
}