﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface IJobSeekerQualificationService
    {
        Task<IEnumerable<JobSeekerQualification>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerQualification jobSeekerQualificationObj);
        Task<JobSeekerQualification> GetById(long jobSeekerQualificationId);
        long Save(JobSeekerQualification jobSeekerQualificationObj);
        bool Activate(long jobSeekerQualificationId, bool isActive);
        bool Delete(long jobSeekerQualificationId);        
        Task<JobSeekerQualification> GetByJobSeekerId(long jobSeekerId);
    }
}
