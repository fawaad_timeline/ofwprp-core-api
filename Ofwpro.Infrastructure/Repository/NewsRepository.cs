﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class NewsRepository : INewsRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public NewsRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<News>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, bool searchNewsDate, News newsObj)
        {
            var predicate = PredicateBuilder.New<News>(true);
            var predicateInner = PredicateBuilder.New<News>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                predicateInner = predicateInner.Or(i => i.NewsTitle.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.NewsDescription.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (newsObj.NewsId > 0)
            {
                predicate = predicate.And(i => i.NewsId == newsObj.NewsId);
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(newsObj.NewsTitle))
            {
                predicate = predicate.And(i => i.NewsTitle.Contains(newsObj.NewsTitle));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(newsObj.NewsDescription))
            {
                predicate = predicate.And(i => i.NewsDescription.Contains(newsObj.NewsDescription));
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(Convert.ToString(newsObj.NewsDate)) && searchNewsDate == true)
            {
                predicate = predicate.And(i => i.NewsDate == newsObj.NewsDate);
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(newsObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == newsObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.News.Where(predicate).OrderByDynamic(orderByColumn, UtilityHelper.QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.News.Where(predicate).OrderByDynamic(orderByColumn, UtilityHelper.QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.News.OrderByDynamic(orderByColumn, UtilityHelper.QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.News.OrderByDynamic(orderByColumn, UtilityHelper.QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }
        public async Task<IEnumerable<News>> GetAll()
        {
            return await unitOfWork.Context.News.Where(x => !x.IsDeleted).ToListAsync();
        }

        public async Task<IEnumerable<News>> GetCurrentNews()
        {
            return await unitOfWork.Context.News.Where(x => !x.IsDeleted && x.NewsDate <= DateTime.Now).ToListAsync();
        }
        

        public Task<News> GetById(long newsId)
        {
            return unitOfWork.Context.News.Where(x => !x.IsDeleted && x.NewsId == newsId).FirstOrDefaultAsync();
        }

        public long Save(News newsObj)
        {
            if (newsObj.NewsId <= 0)
            {
                newsObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<News>().Add(newsObj);
            }
            else
            {
                newsObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(newsObj).State = EntityState.Modified;
                unitOfWork.Context.News.Update(newsObj);
            }

            unitOfWork.Commit();
            return newsObj.NewsId;
        }


        public bool Activate(long newsId, bool isActive)
        {
            News newsObj = unitOfWork.Context.News.Where(x => x.NewsId == newsId).ToList().LastOrDefault();
            if (newsObj != null)
            {
                newsObj.IsActive = isActive;
                newsObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(newsObj).State = EntityState.Modified;
                unitOfWork.Context.News.Update(newsObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long newsId)
        {
            var newsObj = unitOfWork.Context.News.Where(x => !x.IsDeleted && x.NewsId == newsId).ToList().LastOrDefault();

            if (newsObj != null)
            {
                newsObj.UpdatedOn = DateTime.Now;
                newsObj.IsDeleted = true;
                unitOfWork.Context.Entry(newsObj).State = EntityState.Modified;
                unitOfWork.Context.News.Update(newsObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool IsExist(string newsTitle, long newsId)
        {
            var predicate = PredicateBuilder.New<News>(true);

            predicate = predicate.And(i => i.NewsTitle == newsTitle);
            if (newsId > 0)
            {
                predicate = predicate.And(i => i.NewsId != newsId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.News.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }
    }
}
