﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;

namespace Ofwpro.Services.Services
{
    public class FileUploadService : IFileUploadService
    {
        private readonly IFileUploadRepository fileUploadRepository;

        public FileUploadService(IFileUploadRepository fileUploadRepository)
        {
            this.fileUploadRepository = fileUploadRepository;
        }

        public async Task<IEnumerable<FileUpload>> Get(long fileId) => await fileUploadRepository.Get(fileId);
        public async Task<IEnumerable<FileUpload>> GetByModule(long masterId, string module) => await fileUploadRepository.GetByModule(masterId, module);
        public long Save(FileUpload fileUpload) => fileUploadRepository.Save(fileUpload);
        public bool Delete(long fileId) => fileUploadRepository.Delete(fileId);
        public bool DeleteByModule(long masterId, string module) => fileUploadRepository.DeleteByModule(masterId, module);
    }
}
