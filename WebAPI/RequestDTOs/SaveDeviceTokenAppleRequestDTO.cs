﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class SaveDeviceTokenAppleRequestDTO
    {
        public string token { get; set; }
        public string apple_device_token { get; set; }
    }
}
