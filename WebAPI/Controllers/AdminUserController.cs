﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using System.Linq.Expressions;
using Ofwpro.Core;
using Ofwpro.Services.Interfaces;
using Microsoft.AspNetCore.Http;

namespace WebAPI.Controllers
{
    [EnableCors("AllowOrigin")]
    public class AdminUserController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUserService userService;
        private readonly IAdministratorService administratorService;
        private readonly IUserResolverService userResolverService;
        private readonly IFileUploadService fileUploadService;
        private readonly IJobSeekerService jobSeekerService;
        private readonly INewsService newsService;
        private readonly IRecruiterService recruiterService;
        private readonly IRecruiterCompanyService recruiterCompanyService;
        private readonly IRecruiterPostedJobsService recruiterPostedJobsService;
        private readonly IRecruiterAttachmentService recruiterAttachmentService;
        private readonly ICountryDetailsService countryDetailsService;
        private readonly ISalaryRangeService salaryRangeService;
        private readonly IMaritalStatusService maritalStatusService;
        private readonly IEducationLevelService educationLevelService;
        private readonly IExperienceLevelService experienceLevelService;
        private readonly IEmployeeTypeService employeeTypeService;
        private readonly IJobRoleService jobRoleService;
        private readonly IMapper mapper;
        private readonly ISessionService sessionService;
        private readonly IConfiguration config;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService;

        public AdminUserController(UserManager<ApplicationUser> userManager, IUserService userService,
                                   IAdministratorService administratorService, IUserResolverService userResolverService,
                                   IFileUploadService fileUploadService, IJobSeekerService jobSeekerService,
                                   INewsService newsService, IRecruiterService recruiterService,
                                   IRecruiterCompanyService recruiterCompanyService,
                                   IRecruiterPostedJobsService recruiterPostedJobsService,
                                   IRecruiterAttachmentService recruiterAttachmentService,
                                   ICountryDetailsService countryDetailsService, ISalaryRangeService salaryRangeService,
                                   IMaritalStatusService maritalStatusService,
                                   IEducationLevelService educationLevelService,
                                   IExperienceLevelService experienceLevelService,
                                   IEmployeeTypeService employeeTypeService, IJobRoleService jobRoleService,
                                   IMapper mapper, ISessionService sessionService, IConfiguration config,
                                   IWebHostEnvironment hostingEnvironment,
                                   IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService)
        {
            this.userManager = userManager;
            this.userService = userService;
            this.administratorService = administratorService;
            this.userResolverService = userResolverService;
            this.fileUploadService = fileUploadService;
            this.jobSeekerService = jobSeekerService;
            this.newsService = newsService;
            this.recruiterService = recruiterService;
            this.recruiterCompanyService = recruiterCompanyService;
            this.recruiterPostedJobsService = recruiterPostedJobsService;
            this.recruiterAttachmentService = recruiterAttachmentService;
            this.countryDetailsService = countryDetailsService;
            this.salaryRangeService = salaryRangeService;
            this.maritalStatusService = maritalStatusService;
            this.educationLevelService = educationLevelService;
            this.experienceLevelService = experienceLevelService;
            this.employeeTypeService = employeeTypeService;
            this.jobRoleService = jobRoleService;
            this.mapper = mapper;
            this.sessionService = sessionService;
            this.config = config;
            this.hostingEnvironment = hostingEnvironment;
            this.recruiterPostedJobsAndApplicantsService = recruiterPostedJobsAndApplicantsService;
        }

        [HttpPost]
        [Route("en/job_seeker/ajax_all_admin_users")]
        public async Task<object> ajax_all_admin_users([FromBody] AdminUserListRequestDTO model)
        {
            string lan = "en";
            bool isSuccess = true;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            Session sessionObj = null;

            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(model.Token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(model.Token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && !sessionObj.UserType.Equals("admin"))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }

            if (isSuccess == true)
            {
                if (isSuccess == true)
                {
                    Administrator adminiObj = null;
                    IEnumerable<Administrator> adminiObjList = await administratorService.Get(sessionObj.UserId);
                    if (adminiObjList.Any())
                    {
                        adminiObj = adminiObjList.FirstOrDefault();
                        if (!adminiObj.Role.Equals("super-admin"))
                        {
                            JsonArrResponseDTOObj.code = "0";
                            JsonArrResponseDTOObj.MSG = "Not authorized";
                            return JsonArrResponseDTOObj;
                        }
                    }
                    else
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Not authorized";
                        return JsonArrResponseDTOObj;
                    }
                }
            }


            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode=200 };
            List<JobSeekerListingResponseDTO> JobSeekerListingResponseDTOList = null;

            int offset = Convert.ToInt32(model.offset);
            int paging = Convert.ToInt32(model.pagination);
            string orderByColumn = "CreatedOn";
            string orderBy = "DESC";

            IEnumerable<Administrator> administratorList = await administratorService.GetAll();
            if (administratorList.Any())
            {
                administratorList = administratorList.Skip(paging * offset).Take(paging).ToList();
            }

            JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
            JsonArrLoginResponseDTOObj.code = "1";
            JsonArrLoginResponseDTOObj.MSG = "success";
            JsonArrLoginResponseDTOObj.data = administratorList;
            return JsonArrLoginResponseDTOObj;
        }

        [HttpPost]
        [Route("en/admin_user/ajax_register_admin_user")]

        public async Task<JsonArrResponseDTO> ajax_register_admin_user([FromBody] AdminUserRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            Administrator administratorObjOld = null;
            bool isSuccess = true;
            bool isExist = false;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            Session sessionObj = null;

            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(model.Token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(model.Token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && !sessionObj.UserType.Equals("admin"))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }



            try
            {
                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(model.FirstName))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "First name is blank. Please enter first name.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.LastName))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Last Name is blank. Please enter last name.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.Email))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Email is blank. Please enter email.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(model.Password)))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Password is blank. Please enter password.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.Role))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Role is blank. Please provide role.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (!model.Role.Equals("admin") && !model.Role.Equals("super-admin"))
                    {

                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Role is not valid. It could be either admin or super-admin";
                        return JsonArrResponseDTOObj;
                    }

                }

                // Validate Duplidate
                if (isSuccess == true)
                {
                    isExist = administratorService.IsExist(model.Email, 0);
                    if (isExist == true)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Email already exists. Please use a different email.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    //string token = UtilityHelper.rand();

                    string accActivationToken = UtilityHelper.rand();
                    model.Password = UtilityHelper.sha1(model.Password);

                    Administrator AdministratorObj = mapper.Map<Administrator>(model);

                    //AdministratorObj.AdministratorIdToken = token;
                    AdministratorObj.CreatedOn = DateTime.Now;
                    AdministratorObj.IsDeleted = false;
                    AdministratorObj.IsActive = true;
                    //AdministratorObj.AccActivationToken = accActivationToken;

                    if (string.IsNullOrEmpty(AdministratorObj.CreatedBy))
                    {
                        if (loggedInUserId != null)
                        {
                            AdministratorObj.CreatedBy = loggedInUserId;
                        }
                    }

                    long AdministratorId = administratorService.Save(AdministratorObj);

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "User registered successfully. ";
                    return JsonArrResponseDTOObj;

                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/admin_user/ajax_save_admin_user")]
        public async Task<JsonArrResponseDTO> ajax_save_admin_user([FromBody] AdminUserRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            Administrator administratorObjOld = null;
            bool isSuccess = true;
            bool isExist = false;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            Session sessionObj = null;


            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(model.Token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(model.Token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && !sessionObj.UserType.Equals("admin"))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }

            if (isSuccess == true)
            {
                IEnumerable<Administrator> administratorObjOldList = await administratorService.Get(model.Id);
                if (administratorObjOldList.Any())
                {
                    administratorObjOld = administratorObjOldList.FirstOrDefault();
                }
            }

            if (isSuccess == true)
            {
                if (administratorObjOld == null)
                {
                    JsonArrResponseDTOObj.code = "0";
                    JsonArrResponseDTOObj.MSG = "User does not exist";
                    return JsonArrResponseDTOObj;
                }
            }


            try
            {
                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(model.FirstName))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "First name is blank. Please enter first name.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.LastName))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Last Name is blank. Please enter last name.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.Email))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Email is blank. Please enter email.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(model.Password)))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Password is blank. Please enter password.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.Role))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Role is blank. Please provide role.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (!model.Role.Equals("admin") && !model.Role.Equals("super-admin"))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Role is not valid. It could be either admin or super-admin";
                        return JsonArrResponseDTOObj;
                    }

                }




                // Validate Duplidate
                if (isSuccess == true)
                {
                    isExist = administratorService.IsExist(model.Email, administratorObjOld.AdministratorId);
                    if (isExist == true)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Email already exists. Please use a different email.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    //string token = UtilityHelper.rand();

                    string accActivationToken = UtilityHelper.rand();
                    model.Password = UtilityHelper.sha1(model.Password);

                    Administrator AdministratorObj = mapper.Map<Administrator>(model);

                    //AdministratorObj.AdministratorIdToken = token;
                    AdministratorObj.CreatedOn = administratorObjOld.CreatedOn;
                    AdministratorObj.IsDeleted = administratorObjOld.IsDeleted;
                    AdministratorObj.IsActive = administratorObjOld.IsActive;
                    AdministratorObj.UpdatedOn = DateTime.Now;

                    if (string.IsNullOrEmpty(model.Password))
                    {
                        AdministratorObj.Password = administratorObjOld.Password;
                    }

                    long AdministratorId = administratorService.Save(AdministratorObj);

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "User registered details saved successfully. ";
                    return JsonArrResponseDTOObj;

                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/admin_user/ajax_validate_and_login_admin")]
        public async Task<object> ajax_validate_and_login_mobile([FromBody] AdministratorLoginRequestDTO model)
        {
            string loggedInUserId = "";
            bool isSuccess = true;
            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.email) || string.IsNullOrWhiteSpace(model.password))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Parameters Missing";
                        return JsonArrResponseDTOObj;
                    }
                }

                string password = "";
                if (isSuccess == true)
                {
                    password = UtilityHelper.Encrypt(model.password);
                    bool validate = administratorService.Validate(model.email, password);
                    if (validate == false)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Invalid Credentials";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    Administrator administratorObj = await administratorService.GetByEmail(model.email);
                    HttpContext.Session.SetString("adminIdSess", Convert.ToString(administratorObj.AdministratorId));
                    HttpContext.Session.SetString("adminFirstNameSess", administratorObj.FirstName);
                    HttpContext.Session.SetString("adminLastNameSess", administratorObj.LastName);
                    HttpContext.Session.SetString("userTypeSess", "admin");

                    sessionService.DeleteSessionBasedOnUser(administratorObj.AdministratorId, "admin");

                    string token = UtilityHelper.rand();
                    Session sessionObj = new Session();
                    sessionObj.IsActive = true;
                    sessionObj.SessionId = 0;
                    sessionObj.Token = token;
                    sessionObj.UserId = administratorObj.AdministratorId;
                    sessionObj.UserType = "admin";
                    sessionService.Save(sessionObj);

                    // Get picture
                    string picture = "";

                    LoginResponseDTO loginResponseDTOObj = new LoginResponseDTO();
                    loginResponseDTOObj.token = token;
                    loginResponseDTOObj.id = administratorObj.AdministratorId;
                    loginResponseDTOObj.first_name = administratorObj.FirstName;
                    loginResponseDTOObj.last_name = administratorObj.LastName;
                    loginResponseDTOObj.logo_url = picture;

                    JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
                    JsonArrLoginResponseDTOObj.code = "1";
                    JsonArrLoginResponseDTOObj.MSG = "Login Successful";
                    JsonArrLoginResponseDTOObj.data = loginResponseDTOObj;
                    return JsonArrLoginResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "There was an error in processing the request.";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/admin_user/ajax_activate_admin_user")]
        public async Task<JsonArrResponseDTO> ajax_activate_admin_user(AdminActionRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            Administrator administratorObjOld = null;
            bool isSuccess = true;
            bool isExist = false;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            Session sessionObj = null;


            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(model.Token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(model.Token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && !sessionObj.UserType.Equals("admin"))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }

            if (isSuccess == true)
            {
                IEnumerable<Administrator> administratorObjOldList = await administratorService.Get(model.Id);
                if (administratorObjOldList.Any())
                {
                    administratorObjOld = administratorObjOldList.FirstOrDefault();
                }
            }

            if (isSuccess == true)
            {
                if (isSuccess == true)
                {
                    Administrator adminiObj = null;
                    IEnumerable<Administrator> adminiObjList = await administratorService.Get(sessionObj.UserId);
                    if (adminiObjList.Any())
                    {
                        adminiObj = adminiObjList.FirstOrDefault();
                        if (!adminiObj.Role.Equals("super-admin"))
                        {
                            JsonArrResponseDTOObj.code = "0";
                            JsonArrResponseDTOObj.MSG = "Not authorized";
                            return JsonArrResponseDTOObj;
                        }
                    }
                    else
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Not authorized";
                        return JsonArrResponseDTOObj;
                    }
                }
            }


            if (isSuccess == true)
            {
                if (administratorObjOld == null)
                {
                    JsonArrResponseDTOObj.code = "0";
                    JsonArrResponseDTOObj.MSG = "User does not exist";
                    return JsonArrResponseDTOObj;
                }
            }


            try
            {
                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    administratorService.Activate(model.Id, true);

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Information saved successfully. ";
                    return JsonArrResponseDTOObj;

                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/admin_user/ajax_deactivate_admin_user")]

        public async Task<JsonArrResponseDTO> ajax_deactivate_admin_user(AdminActionRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            Administrator administratorObjOld = null;
            bool isSuccess = true;
            bool isExist = false;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            Session sessionObj = null;


            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(model.Token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(model.Token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && !sessionObj.UserType.Equals("admin"))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }

            if (isSuccess == true)
            {
                IEnumerable<Administrator> administratorObjOldList = await administratorService.Get(model.Id);
                if (administratorObjOldList.Any())
                {
                    administratorObjOld = administratorObjOldList.FirstOrDefault();
                }
            }

            if (isSuccess == true)
            {
                if (isSuccess == true)
                {
                    Administrator adminiObj = null;
                    IEnumerable<Administrator> adminiObjList = await administratorService.Get(sessionObj.UserId);
                    if (adminiObjList.Any())
                    {
                        adminiObj = adminiObjList.FirstOrDefault();
                        if (!adminiObj.Role.Equals("super-admin"))
                        {
                            JsonArrResponseDTOObj.code = "0";
                            JsonArrResponseDTOObj.MSG = "Not authorized";
                            return JsonArrResponseDTOObj;
                        }
                    }
                    else
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Not authorized";
                        return JsonArrResponseDTOObj;
                    }
                }
            }


            if (isSuccess == true)
            {
                if (administratorObjOld == null)
                {
                    JsonArrResponseDTOObj.code = "0";
                    JsonArrResponseDTOObj.MSG = "User does not exist";
                    return JsonArrResponseDTOObj;
                }
            }


            try
            {
                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    administratorService.Activate(model.Id, false);

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Information saved successfully. ";
                    return JsonArrResponseDTOObj;

                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/admin_user/ajax_delete_admin_user")]

        public async Task<JsonArrResponseDTO> ajax_delete_admin_user(AdminActionRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            Administrator administratorObjOld = null;
            bool isSuccess = true;
            bool isExist = false;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            Session sessionObj = null;


            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(model.Token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(model.Token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && !sessionObj.UserType.Equals("admin"))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }

            if (isSuccess == true)
            {
                IEnumerable<Administrator> administratorObjOldList = await administratorService.Get(model.Id);
                if (administratorObjOldList.Any())
                {
                    administratorObjOld = administratorObjOldList.FirstOrDefault();
                }
            }

            if (isSuccess == true)
            {
                if (isSuccess == true)
                {
                    Administrator adminiObj = null;
                    IEnumerable<Administrator> adminiObjList = await administratorService.Get(sessionObj.UserId);
                    if (adminiObjList.Any())
                    {
                        adminiObj = adminiObjList.FirstOrDefault();
                        if (!adminiObj.Role.Equals("super-admin"))
                        {
                            JsonArrResponseDTOObj.code = "0";
                            JsonArrResponseDTOObj.MSG = "Not authorized";
                            return JsonArrResponseDTOObj;
                        }
                    }
                    else
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Not authorized";
                        return JsonArrResponseDTOObj;
                    }
                }
            }


            if (isSuccess == true)
            {
                if (administratorObjOld == null)
                {
                    JsonArrResponseDTOObj.code = "0";
                    JsonArrResponseDTOObj.MSG = "User does not exist";
                    return JsonArrResponseDTOObj;
                }
            }


            try
            {
                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    administratorService.Delete(model.Id);

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Information deleted successfully. ";
                    return JsonArrResponseDTOObj;

                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/admin_user/ajax_all_recruiters")]
        public async Task<ResponseDTO<List<RecruiterListingResponseDTO>>> All([FromBody] RecruiterAllRequestDTO model)
        {
            string lan = "en";
            string hostUrl = config["Utility:APIBaseURL"];
            int StatusCode = 200;
            bool isSuccess = true;
            string Message = "Information retrieved successfully";
            string ExceptionMessage = "";
            int totalRecords = 0;
            ResponseDTO<List<RecruiterListingResponseDTO>> response = new ResponseDTO<List<RecruiterListingResponseDTO>>();
            List<RecruiterListingResponseDTO> RecruiterListingResponseDTOList = null;

            int offset = Convert.ToInt32(model.offset);
            int paging = Convert.ToInt32(model.pagination);
            string orderByColumn = "";
            string orderBy = "ASC";

            if (string.IsNullOrEmpty(orderByColumn) || orderByColumn.ToLower() == "string") { orderByColumn = "CreatedOn"; }
            if (string.IsNullOrEmpty(orderBy) || orderBy.ToLower() == "string") { orderBy = "Desc"; }

            Recruiter recruiterObj = new Recruiter();
            IEnumerable<Recruiter> recruiterList = await recruiterService.All(offset, paging, orderByColumn, orderBy, "", recruiterObj);
            if (!recruiterList.Any())
            {
                StatusCode = 404;
                isSuccess = true;
                Message = "No records found";
                ExceptionMessage = "";
            }
            else
            {
                totalRecords = recruiterList.Count();
                IEnumerable<Recruiter> RecruiterList = recruiterList.Skip(paging * offset).Take(paging).ToList();
                RecruiterListingResponseDTOList = mapper.Map<List<RecruiterListingResponseDTO>>(RecruiterList);
                foreach (var _recObj in RecruiterListingResponseDTOList)
                {
                    RecruiterPostedJobs recruiterPostedJobs = new RecruiterPostedJobs();
                    recruiterPostedJobs.RecruiterId = _recObj.RecruiterId;
                    #region jOb
                    IEnumerable<RecruiterPostedJobs> postedjobList = await recruiterPostedJobsService.All(orderByColumn, orderBy, "", recruiterPostedJobs);
                    List<RecruiterPostedJobs> listPosted = mapper.Map<List<RecruiterPostedJobs>>(postedjobList);
                    _recObj.recruiterPostedJobs = listPosted;
                    #endregion
                    #region logo

                    RecruiterAttachment RecruiterAttachmentObj = await recruiterAttachmentService.GetByDocumentType(recruiterPostedJobs.RecruiterId, "logo");
                    string UploadedFrom = "";
                    if (RecruiterAttachmentObj?.UploadedFrom != null) UploadedFrom = RecruiterAttachmentObj?.UploadedFrom;

                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    string fileNameUpdated = "", imgPath = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
                    imgPath = hostUrl + "/assets/images/default_logo.png";
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { imgPath = hostUrl + "/Uploads/" + fileNameUpdated; }
                    _recObj.logo = imgPath;
                    _recObj.logo_url = imgPath;
                    #endregion

                    var recruiterCompany = await recruiterCompanyService.GetByRecruiterId(_recObj.RecruiterId);

                    if (recruiterCompany != null)
                    {
                        _recObj.CompanyName = recruiterCompany.CompanyName;
                        _recObj.CompanyAddress = recruiterCompany.Address;
                        _recObj.AboutCompany = recruiterCompany.AboutCompany;
                        _recObj.CompanyDescription = recruiterCompany.Description;
                        _recObj.CompanyNoOfEmployees = recruiterCompany.NoOfEmployees;
                        _recObj.CompanyPhoneNumber = recruiterCompany.PhoneNumber;
                        _recObj.CompanyWebsiteURL = recruiterCompany.WebsiteUrl;
                    }
                }
            }
            response.totalRecords = totalRecords;
            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = RecruiterListingResponseDTOList;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;
            return response;
        }


        [HttpPost]
        [Route("en/admin_user/ajax_load_dashboard_admin")]

        public async Task<JsonArrResponseDTO> ajax_load_dashboard_admin([FromBody]AdminActionRequestDTO model)
        {
            DashboardResponseDTO data = new DashboardResponseDTO();
            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            string loggedInUserId = "";

            Administrator administratorObjOld = null;
            bool isSuccess = true;
            Session sessionObj = null;


            // Validate Token
            if (isSuccess == true)
            {
                if (string.IsNullOrWhiteSpace(model.Token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(model.Token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }
            }
            try
            {
                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    var jobSeekersList = await jobSeekerService.All("CreatedOn", "DESC", "", new JobSeeker(), null);
                    var registerRequests = await jobSeekerService.All("CreatedOn", "DESC", "", new JobSeeker(), true);
                    IEnumerable<News> newsList = await newsService.All(0, 99999, "CreatedOn", "DESC", "", false , new News());
                    var resruiterList = await recruiterService.All(0, 99999,"CreatedOn", "DESC", "", new Recruiter());
                    data.jobseekersCount = jobSeekersList.Count();
                    data.newsCount = newsList.Count();
                    data.registerRequestsCount = registerRequests.Count();
                    data.registeredCompaniesCount = resruiterList.Count();

                    JsonArrResponseDTOObj.data = data;
                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "dashboard data";
                    return JsonArrResponseDTOObj;

                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "error while getting dashboard data";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

        [HttpPost]
        [Route("en/admin_user/ajax_all_jobs")]
        public async Task<object> ajax_all_jobs([FromBody] RecruiterAllRequestDTO model)
        {
            string lan = "en";
            Boolean isSuccess = true;
            int recordsCount = 0;
            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode = 200 };
            List<RecruiterPostedJobsResponseDTO> RecruiterPostedJobsResponseDTOList = new List<RecruiterPostedJobsResponseDTO>();

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
            Session sessionObj = null;

            int offset = Convert.ToInt32(model.offset);
            int paging = Convert.ToInt32(model.pagination);
            string orderByColumn = "CreatedOn";
            string orderBy = "DESC";

            if (paging <= 0) { paging = 10; }

            if (isSuccess == true && !string.IsNullOrEmpty(model.token))
            {
                if (string.IsNullOrWhiteSpace(model.token))
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }

                if (isSuccess == true)
                {
                    sessionObj = await sessionService.GetByToken(model.token);
                    if (sessionObj == null)
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true && sessionObj.UserType != "administrator")
                {
                    JsonArrResponseDTOObj.code = "2";
                    JsonArrResponseDTOObj.MSG = "Invalid token";
                    return JsonArrResponseDTOObj;
                }
            }
            long jobSeekerId = 0;
            if (sessionObj != null)
            {
                jobSeekerId = sessionObj.UserId;
            }
            RecruiterPostedJobs recruiterPostedJobsObj = new RecruiterPostedJobs();
            recruiterPostedJobsObj.IsActive = true;

            string hostUrl = config["Utility:APIBaseURL"];

            //recruiterPostedJobsObj.Title = model.job_title_p;
            //recruiterPostedJobsObj.City = model.city_p;
            //recruiterPostedJobsObj.SalaryRangeId = model.salary_range_id_enc_p;
            //recruiterPostedJobsObj.CompanyType = model.company_type_p;
            //recruiterPostedJobsObj.EmploymentType = model.employment_type_p;

            IEnumerable<RecruiterPostedJobs> recruiterPostedJobsList = await recruiterPostedJobsService.All(orderByColumn, orderBy, model.q, recruiterPostedJobsObj);
            recordsCount = recruiterPostedJobsList.Count();
            if (!recruiterPostedJobsList.Any())
            {

            }
            else
            {
                int totalRecords = recruiterPostedJobsList.Count();
                IEnumerable<RecruiterPostedJobs> RecruiterPostedJobsList = recruiterPostedJobsList.Skip(paging * offset).Take(paging).ToList();

                foreach (RecruiterPostedJobs RecruiterPostedJobsObj in RecruiterPostedJobsList)
                {
                    bool hasApplied = false;
                    if (sessionObj != null)
                    {
                        hasApplied = recruiterPostedJobsAndApplicantsService.IsExist(sessionObj.UserId, RecruiterPostedJobsObj.RecruiterPostedJobsId);
                    }
                    RecruiterPostedJobsResponseDTO RecruiterPostedJobsResponseDTOObj = new RecruiterPostedJobsResponseDTO();
                    RecruiterPostedJobsResponseDTOObj = mapper.Map<RecruiterPostedJobsResponseDTO>(RecruiterPostedJobsObj);

                    Recruiter recruiterObj = await recruiterService.GetById(RecruiterPostedJobsObj.RecruiterId);
                    string companyName = "";
                    if (recruiterObj != null)
                    {

                        RecruiterPostedJobsResponseDTOObj.recruiter_number = recruiterObj.CountryCode + recruiterObj.Mobile;
                        RecruiterPostedJobsResponseDTOObj.recruiter_email = recruiterObj.Email;

                        RecruiterCompany recruiterCompany = await recruiterCompanyService.GetByRecruiterId(recruiterObj.RecruiterId);

                        if (recruiterCompany != null)
                        {
                            RecruiterPostedJobsResponseDTOObj.company_name = recruiterCompany.CompanyName;
                            RecruiterPostedJobsResponseDTOObj.company_number = recruiterCompany.PhoneNumber;
                            RecruiterPostedJobsResponseDTOObj.company_website = recruiterCompany.WebsiteUrl;
                            RecruiterPostedJobsResponseDTOObj.company_address = recruiterCompany.Address;

                        }
                    }

                  
                    

                    RecruiterAttachment RecruiterAttachmentObj = await recruiterAttachmentService.GetByDocumentType(RecruiterPostedJobsObj.RecruiterId, "logo");
                    string fileNameUpdated = "", imgPath = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }
                    string UploadedFrom = string.Empty;
                    if (RecruiterAttachmentObj?.UploadedFrom != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;
                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    imgPath = hostUrl + "/assets/images/default_logo.png";
                    if (!string.IsNullOrEmpty(fileNameUpdated)) { imgPath = hostUrl + "/Uploads/" + fileNameUpdated; }
                    RecruiterPostedJobsResponseDTOObj.logo = imgPath;
                    RecruiterPostedJobsResponseDTOObj.logo_url = imgPath;
                    RecruiterPostedJobsResponseDTOObj.has_applied = hasApplied;
                    //if (RecruiterPostedJobsResponseDTOObj?.description.Length > 50) { RecruiterPostedJobsResponseDTOObj.description = RecruiterPostedJobsResponseDTOObj.description.Substring(0, 50) + "..."; }
                    CountryDetails countryDetailsObj = countryDetailsService.GetById(RecruiterPostedJobsObj.CountryId);

                    if (countryDetailsObj != null) { RecruiterPostedJobsResponseDTOObj.country_name = countryDetailsObj.Name; }
                    if (RecruiterPostedJobsResponseDTOObj.city != "") { RecruiterPostedJobsResponseDTOObj.city = RecruiterPostedJobsResponseDTOObj.city + ", "; }
                    SalaryRange salaryRangeObj = salaryRangeService.GetById(RecruiterPostedJobsObj.SalaryRangeId);
                    if (salaryRangeObj != null) { RecruiterPostedJobsResponseDTOObj.salary_range = salaryRangeObj.Name; }

                    ExperienceLevel exprienceLevelObj = experienceLevelService.GetById(Convert.ToInt32(RecruiterPostedJobsObj.ExperienceLevel));
                    if (exprienceLevelObj != null) { RecruiterPostedJobsResponseDTOObj.experience_level = exprienceLevelObj.Name; }

                    EmployeeType employeeTypeObj = employeeTypeService.GetById(Convert.ToInt32(RecruiterPostedJobsObj.EmploymentType));
                    if (employeeTypeObj != null) { RecruiterPostedJobsResponseDTOObj.employment_type= employeeTypeObj.Name; }

                    if (jobSeekerId > 0)
                    {
                        bool isExist = recruiterPostedJobsAndApplicantsService.IsExist(jobSeekerId, RecruiterPostedJobsObj.RecruiterPostedJobsId);
                        if (isExist == true)
                        {
                            RecruiterPostedJobsObj.has_applied = "1";
                        }
                        else
                        {
                            RecruiterPostedJobsObj.has_applied = "0";
                        }
                    }

                    var applicant = await recruiterPostedJobsAndApplicantsService.GetAllByRecruiterPostedJobsId(RecruiterPostedJobsResponseDTOObj.recruiter_posted_jobs_id_enc);

                    RecruiterPostedJobsResponseDTOObj.total_applicant = applicant.Count();

                    // Get Roles Job - Entitiy is not available right now
                    RecruiterPostedJobsResponseDTOList.Add(RecruiterPostedJobsResponseDTOObj);

                }
            }

            JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
            JsonArrLoginResponseDTOObj.code = "1";
            JsonArrLoginResponseDTOObj.MSG = "success";
            JsonArrLoginResponseDTOObj.totalRecords = recordsCount;
            JsonArrLoginResponseDTOObj.data = RecruiterPostedJobsResponseDTOList;
            return JsonArrLoginResponseDTOObj;
        }

        [HttpPost]
        [Route("en/admin_user/delete_posted_job")]
        public async Task<JsonArrResponseDTO> delete_posted_jobs_mobile([FromBody] DeletePostedJobsRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && sessionObj.UserType != "administrator")
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(model.recruiter_posted_jobs_id_enc)) || model.recruiter_posted_jobs_id_enc <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Job data is blank.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Save
                if (isSuccess == true)
                {
                    recruiterPostedJobsService.Delete(model.recruiter_posted_jobs_id_enc);

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Job deleted successfully.";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be deleted";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }
    }
}
