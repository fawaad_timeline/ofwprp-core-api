﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class RecruiterService: IRecruiterService
    {
        private readonly IRecruiterRepository recruiterRepository;

        public RecruiterService(IRecruiterRepository recruiterRepository)
        {
            this.recruiterRepository = recruiterRepository;
        }

        public async Task<IEnumerable<Recruiter>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Recruiter recruiterObj)
        {
            return await recruiterRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, recruiterObj);
        }

        public async Task<Recruiter> GetById(long recruiterId)
        {
            return await recruiterRepository.GetById(recruiterId);
        }

        public long Save(Recruiter recruiterObj)
        {
            return recruiterRepository.Save(recruiterObj);
        }

        public bool Activate(long recruiterId, bool isActive)
        {
            return recruiterRepository.Activate(recruiterId, isActive);
        }

        public bool Delete(long recruiterId)
        {
            return recruiterRepository.Delete(recruiterId);
        }

        public bool IsExist(string email, long recruiterId)
        {
            return recruiterRepository.IsExist(email, recruiterId);
        }

        public bool Validate(string email, string password)
        {
            return recruiterRepository.Validate(email, password);
        }
        public bool IsUserActivated(string email, string password)
        {
            return recruiterRepository.IsUserActivated(email, password);
        }

        public Task<Recruiter> GetByEmail(string email)
        {
            return recruiterRepository.GetByEmail(email);
        }

        public Task<Recruiter> GetByRecruiterIdToken(string recruiterIdToken)
        {
            return recruiterRepository.GetByRecruiterIdToken(recruiterIdToken);
        }

        public Task<Recruiter> GetByActivationToken(string accActivationToken)
        {
            return recruiterRepository.GetByActivationToken(accActivationToken);
        }

        public long NoOfCompanies()
        {
            return recruiterRepository.NoOfCompanies();
        }
    }
}
