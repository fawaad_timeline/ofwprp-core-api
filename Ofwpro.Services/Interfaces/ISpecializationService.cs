﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface ISpecializationService
    {
        Task<Tuple<IEnumerable<Specialization>, int>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Specialization specializationObj);
        Task<Specialization> GetById(long specializationId);
        long Save(Specialization specializationObj);
        bool Activate(long specializationId, bool isActive);
        bool Delete(long specializationId);
        bool IsExist(string specializationName, long SpecializationId);
        Task<IEnumerable<Specialization>> GetAll();
    }
}
