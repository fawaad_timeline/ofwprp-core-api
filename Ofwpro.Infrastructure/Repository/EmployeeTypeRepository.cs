﻿using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class EmployeeTypeRepository : IEmployeeTypeRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public EmployeeTypeRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<Tuple<IEnumerable<EmployeeType>, int>> GetAll(int offset,int paging)
        {
            var recordsCount = _unitOfWork.Context.EmployeeType.Count();
            var dataList = await _unitOfWork.Context.EmployeeType.OrderBy(x => x.Id).Skip(offset * paging).Take(paging).ToListAsync();
            return new Tuple<IEnumerable<EmployeeType>, int>(dataList,recordsCount);
        }
        public EmployeeType GetById(int id)
        {
            var employeeTypeList = _unitOfWork.Context.EmployeeType.Where(x => x.Id == id).FirstOrDefault();
            return employeeTypeList;
        }
        public int Add(EmployeeType employeeType)
        {
            _unitOfWork.Context.Set<EmployeeType>().Add(employeeType);
            _unitOfWork.Commit();
            return employeeType.Id;
        }

        public void Delete(int id)
        {
            var employeeType = _unitOfWork.Context.EmployeeType.Where(x => x.Id == id).FirstOrDefault();
            if (employeeType != null)
            {
                _unitOfWork.Context.EmployeeType.Remove(employeeType);
                _unitOfWork.Commit();
            }
        }
        public bool IsExists(string name)
        {
            bool IsExists = false;
            var employeeTypes = _unitOfWork.Context.EmployeeType.Where(x => x.Name == name);
            if (employeeTypes.Any())
                IsExists = true;
            else
                IsExists = false;
            return IsExists;
        }

        public int Update(EmployeeType employeeType)
        {
            _unitOfWork.Context.Entry(employeeType).State = EntityState.Modified;
            _unitOfWork.Context.EmployeeType.Update(employeeType);
            _unitOfWork.Commit();
            return employeeType.Id;
        }
    }
}
