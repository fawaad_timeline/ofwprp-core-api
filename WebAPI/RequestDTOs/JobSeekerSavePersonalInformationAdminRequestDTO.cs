﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class JobSeekerSavePersonalInformationAdminRequestDTO
    {
        public long Id { get; set; }
        public string Token { get; set; }
        public long country_id_enc { get; set; }
        public long marital_status_id_enc { get; set; }
        public string home_phone { get; set; }
        public string mobileCode { get; set; }
        public string is_driving_license { get; set; }
        public string current_location { get; set; }
        public string visa_status { get; set; }



        //public long CurrentCountry { get; set; }
        //public string ObigationService { get; set; }
        //public string NoOfChildren { get; set; }
        //public string MartyrsFamily { get; set; }
        //public string PeopleOfDisability { get; set; }
        //public string DisabilityDescription { get; set; }
        //public string NoOfDisabilityCard { get; set; }
        //public string AddressOfLiving { get; set; }
        //public string PlaceOfBirth { get; set; }
        //public string IdentificationPapers { get; set; }
        //public string OriginalAddress { get; set; }
        //public string ExpectedAddress { get; set; }
        //public bool NeedCompassion { get; set; }
        //public DateTime DateOfDisabilityCard { get; set; }
        //public DateTime ExpectedDate { get; set; }

    }
}
