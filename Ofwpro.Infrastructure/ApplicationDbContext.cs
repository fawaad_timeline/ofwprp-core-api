﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using Ofwpro.Core;

namespace Ofwpro.Infrastructure
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
           : base(options)
        {
            //ChangeTracker.LazyLoadingEnabled = false;
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

        }

        #region Entity
        public DbSet<Administrator> Administrator { get; set; }
        public DbSet<FileUpload> FileUploads { get; set; }
        public DbSet<Specialization> Specialization { get; set; }
        public DbSet<JobSeeker> JobSeeker { get; set; }
        public DbSet<Session> Session { get; set; }
        public DbSet<Recruiter> Recruiter { get; set; }
        public DbSet<RecruiterCompany> RecruiterCompany { get; set; }
        public DbSet<JobSeekerPersonalInformation> JobSeekerPersonalInformation { get; set; }
        public DbSet<JobSeekerQualification> JobSeekerQualification { get; set; }
        public DbSet<JobSeekerWorkExperience> JobSeekerWorkExperience { get; set; }
        public DbSet<RecruiterPostedJobs> RecruiterPostedJobs { get; set; }
        public DbSet<JobSeekerAttachment> JobSeekerAttachment { get; set; }
        public DbSet<RecruiterPostedJobsAndApplicants> RecruiterPostedJobsAndApplicants { get; set; }
        public DbSet<RecruiterAttachment> RecruiterAttachment { get; set; }
        public DbSet<CountryDetails> CountryDetails { get; set; }
        public DbSet<SalaryRange> SalaryRange { get; set; }
        public DbSet<WebSettings> WebSettings { get; set; }
        public DbSet<CompanyType> CompanyType { get; set; }
        public DbSet<EmployeeType> EmployeeType { get; set; }
        public DbSet<MaritalStatus> MaritalStatus { get; set; }
        public DbSet<EducationLevels> EducationLevels { get; set; }
        public DbSet<JobRoles> JobRoles { get; set; }
        public DbSet<ExperienceLevel> ExperienceLevel { get; set; }
        public DbSet<ForgotPassword> ForgotPassword { get; set; }
        public DbSet<MessageGeneral> MessageGeneral { get; set; }
        public DbSet<MessageAgainstJob> MessageAgainstJob { get; set; }
        public DbSet<HtmlPages> HtmlPages { get; set; }
        public DbSet<Faq> Faq { get; set; }
        public DbSet<Testimonial> Testimonial { get; set; }
        public DbSet<Pages> Pages { get; set; }
        public DbSet<EmailSubscription>  EmailSubscription {get; set;}
        public DbSet<ContactUs> ContactUs { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<WebsiteVisitorCounter> WebsiteVisitorCounter { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);			

            //builder.Entity<ApplicationUser>(b =>
            //{
            //    // Each User can have many entries in the UserRole join table
            //    b.HasMany(e => e.UserRoles)
            //    .WithOne(e => e.User)
            //        .HasForeignKey(ur => ur.UserId)
            //        .IsRequired()
            //        .OnDelete(DeleteBehavior.Cascade);
            //});

            //builder.Entity<IdentityRole>().HasData(new IdentityRole
            //{
            //    Id = "1",
            //    Name = "Super Admin",
            //    NormalizedName = "Super Admin"
            //});

            //builder.Entity<IdentityRole>().HasData(new IdentityRole
            //{
            //    Id = "2",
            //    Name = "Admin",
            //    NormalizedName = "Admin"
            //});

            //var hasher = new PasswordHasher<ApplicationUser>();
            //builder.Entity<ApplicationUser>().HasData(new ApplicationUser
            //{
            //    Id = "1",
            //    UserName = "vasim",
            //    NormalizedUserName = "Vasim",
            //    FirstName = "Vasim",
            //    LastName = "Sunni",
            //    IsActive = true,
            //    Email = "vasimsunni@timeline.ae",
            //    NormalizedEmail = "vasimsunni@timeline.ae",
            //    EmailConfirmed = true,
            //    PhoneNumberConfirmed = true,
            //    PasswordHash = hasher.HashPassword(null, "P@ssword@1"),
            //    SecurityStamp = Guid.NewGuid().ToString("D")
            //});

            //builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            //{
            //    RoleId = "1",
            //    UserId = "1"
            //});

            builder.Entity<Administrator>().HasData(new Administrator
            {
                AdministratorId = 1,
                FirstName = "Vasim",
                LastName = "Sunni",
                IsActive = true,
                Email = "admin@timeline.ae",
                Password = "mpJrT3VcW7zbjl+aUXQQjQ=="
            });


            #region adding the Matrial status
            builder.Entity<MaritalStatus>().HasData(new MaritalStatus
            {
                Id = 1,
                Name = "Single"
            });
            builder.Entity<MaritalStatus>().HasData(new MaritalStatus
            {
                Id = 2,
                Name = "Married"
            });
            builder.Entity<MaritalStatus>().HasData(new MaritalStatus
            {
                Id = 3,
                Name = "Widow"
            });
            builder.Entity<MaritalStatus>().HasData(new MaritalStatus
            {
                Id = 4,
                Name = "Divoced"
            });
            #endregion

            #region Html Pages
            builder.Entity<HtmlPages>().HasData(new HtmlPages
            {
                Id= 1,
                Name = "faqs",
                HtmlCode = ""
            });
            builder.Entity<HtmlPages>().HasData(new HtmlPages
            {
                Id=2,
                Name = "contact us",
                HtmlCode = ""
            });
            builder.Entity<HtmlPages>().HasData(new HtmlPages
            {
                Id=3,
                Name = "",
                HtmlCode = ""
            });
            builder.Entity<HtmlPages>().HasData(new HtmlPages
            {
                Id=4,
                Name = "",
                HtmlCode = ""
            });
            #endregion

            builder.Entity<Pages>().HasData(new Pages
            {
                PagesId = 1,
                ContactUs = "",
                AboutUs = ""
            });
        }
    }
}
