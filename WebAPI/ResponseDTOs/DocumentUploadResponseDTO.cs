﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class DocumentUploadResponseDTO
    {
        public long fileId { get; set; }
        public string fileNameOriginal { get; set; }
        public string fileNameUpdated { get; set; }
        public string fileType { get; set; }
        public string fileSize { get; set; }
    }
}
