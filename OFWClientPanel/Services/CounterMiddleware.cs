﻿using Microsoft.AspNetCore.Http;
using Ofwpro.Core;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Services
{
    public class CounterMiddleware
    {
        private readonly RequestDelegate _requestDelegate;
        private readonly IWebsiteVisitorCounterService websiteVisitorCounterService;

        public CounterMiddleware(RequestDelegate requestDelegate)
        {
            _requestDelegate = requestDelegate;
        }

        public async Task Invoke(HttpContext context, IWebsiteVisitorCounterService websiteVisitorCounterService)
        {
            string visitorId = context.Request.Cookies["VisitorId"];

            if(visitorId != null)
            {
                var websiteVisitorCounterObj = await websiteVisitorCounterService.GetByVisitorId(visitorId);

                if (websiteVisitorCounterObj!=null && websiteVisitorCounterObj.VisitorId == visitorId)
                {
                    //WebsiteVisitorCounter counter = new WebsiteVisitorCounter();

                    //counter.WebsiteVisitorCounterId = websiteVisitorCounterObj.WebsiteVisitorCounterId;
                    //counter.VisitorId = visitorId;
                    //counter.VisitorCount = websiteVisitorCounterObj.VisitorCount + 1;
                    //counter.VisitedDate = DateTime.Now;

                    //long savedId = websiteVisitorCounterService.Save(counter);
                }
            }
            else if (visitorId == null)
            {
                //don the necessary staffs here to save the count by one
                string Id = Guid.NewGuid().ToString();
                context.Response.Cookies.Append("VisitorId", Id, new CookieOptions()
                {
                    Path = "/",
                    HttpOnly = true,
                    Secure = false,
                });

                WebsiteVisitorCounter counter = new WebsiteVisitorCounter();

                counter.WebsiteVisitorCounterId = 0;
                counter.VisitorId = Id;
                counter.VisitorCount = 1;
                counter.VisitedDate = DateTime.Now;

                long savedId = websiteVisitorCounterService.Save(counter);
            }

            await _requestDelegate(context);
        }
    }
}
