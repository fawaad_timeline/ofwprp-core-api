﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ofwpro.Core;
using Ofwpro.Services.Interfaces;

namespace Ofwpro.Services.Services
{
    public class UserResolverService: IUserResolverService
    {
		private readonly IHttpContextAccessor context;
		private readonly UserManager<ApplicationUser> userManager;

		public UserResolverService(IHttpContextAccessor context, UserManager<ApplicationUser> userManager)
		{
			this.context = context;
			this.userManager = userManager;
		}

		public async Task<ApplicationUser> GetCurrentUser()
		{
			return await userManager.GetUserAsync(context.HttpContext.User);
		}
	}
}
