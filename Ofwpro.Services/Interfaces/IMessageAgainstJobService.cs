﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface IMessageAgainstJobService
    {
        Task<IEnumerable<MessageAgainstJob>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, MessageAgainstJob messageGeneralObj);
        Task<MessageAgainstJob> GetById(long messageGeneralId);
        long Save(MessageAgainstJob messageGeneralObj);
        bool Activate(long messageGeneralId, bool isActive);
        bool Delete(long messageGeneralId);
        int GetTotalMessagesAgainstJobUnRead(long recruiterPostedJobsAndApplicantsId, string userType);
        Task<IEnumerable<MessageAgainstJob>> GetAllMessagesAgainstJob(string orderByColumn, string orderBy, string searchTerm, bool isActive, long recruiterPostedJobsAndApplicantsId);
        bool MarkMessageRead(long recruiterPostedJobsAndApplicantsId, string userType);
        int GetTotalMessagesAgainstJob(long recruiterPostedJobsAndApplicantsId);
    }
}
