﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface IWebsiteVisitorCounterService
    {
        Task<IEnumerable<WebsiteVisitorCounter>> GetAll();
        Task<WebsiteVisitorCounter> GetByVisitorId(string visitorId);
        long Save(WebsiteVisitorCounter websiteVisitorCounter);
    }
}
