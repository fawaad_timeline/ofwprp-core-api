﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Ofwpro.Core
{
    public class JobRoles
    {
		protected long _Id;
		protected string _Name;

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id
		{
			get => _Id;
			set => _Id = value;
		}
		public string Name
		{
			get => _Name;
			set => _Name = value;
		}
	}
}
