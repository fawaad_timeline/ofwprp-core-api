﻿using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class MaritalStatusRepository : IMaritalStatusRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public MaritalStatusRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<MaritalStatus>> GetAll()
        {
            var result = _unitOfWork.Context.MaritalStatus.OrderBy(x => x.Id);
            return await result.ToListAsync();
        }
        public MaritalStatus GetById(long id)
        {
            var maritalStatusList = _unitOfWork.Context.MaritalStatus.Where(x => x.Id == id).FirstOrDefault();
            return maritalStatusList;
        }
        public long Save(MaritalStatus data)
        {
            if (data.Id <= 0)
            {
                _unitOfWork.Context.Set<MaritalStatus>().Add(data);
            }
            else
            {
                _unitOfWork.Context.Entry(data).State = EntityState.Modified;
                _unitOfWork.Context.MaritalStatus.Update(data);
            }
            _unitOfWork.Commit();
            return data.Id;
        }
        public bool Delete(long id)
        {
            var maritalStatusList = _unitOfWork.Context.MaritalStatus.Where(x => x.Id == id).FirstOrDefault();
            _unitOfWork.Context.MaritalStatus.Remove(maritalStatusList);
            _unitOfWork.Commit();
            return true;
        }
    }
}
