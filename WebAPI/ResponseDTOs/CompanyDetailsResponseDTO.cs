﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class CompanyDetailsResponseDTO
    {
        public long tbl_recruiter_id { get; set; }
        public string no_of_employees { get; set; }
        public string about_company { get; set; }
        public string website_url { get; set; }
        public string company_name { get; set; }
        public string added_date { get; set; }
        public string is_active { get; set; }
        public string logo_url { get; set; }
    }
}
