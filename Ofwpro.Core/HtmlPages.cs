﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ofwpro.Core
{
    public class HtmlPages
	{
        protected long _Id;
        protected string _Name, _HtmlCode;

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id
		{
			get => _Id;
			set => _Id = value;
		}
		public string Name
		{
			get => _Name;
			set => _Name = value;
		}
		public string HtmlCode
		{
			get => _HtmlCode;
			set => _HtmlCode = value;
		}
	}
}
