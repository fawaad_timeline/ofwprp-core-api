using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Blazored.LocalStorage;
using Blazored.Modal;
using CurrieTechnologies.Razor.SweetAlert2;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Server;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OFWClientPanel.Data;
using OFWClientPanel.Services;
using Ofwpro.Core;
using Ofwpro.Infrastructure;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.Repository;
using Ofwpro.Services.Interfaces;
using Ofwpro.Services.Services;
using FileUpload = OFWClientPanel.Services.FileUpload;

namespace OFWClientPanel
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<WeatherForecastService>();

            services.AddSingleton<IUserTwoFactorTokenProvider<ApplicationUser>, DataProtectorTokenProvider<ApplicationUser>>();
            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 8;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                //Lockout Settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(Convert.ToInt64(Configuration["AccountLockOutInfo:LockoutTimeSpan"]));
                options.Lockout.MaxFailedAccessAttempts = Convert.ToInt32(Configuration["AccountLockOutInfo:MaxFailedAccessAttempts"]);
                options.Lockout.AllowedForNewUsers = false;
                //User Settings
                options.User.RequireUniqueEmail = false;
                //Default Signin Settings
                options.SignIn.RequireConfirmedEmail = false;
                options.SignIn.RequireConfirmedPhoneNumber = false;
                options.Tokens.ProviderMap.Add("Default", new TokenProviderDescriptor(typeof(IUserTwoFactorTokenProvider<ApplicationUser>)));
            })
            .AddEntityFrameworkStores<ApplicationDbContext>();
            services.Configure<DataProtectionTokenProviderOptions>(o =>
            {
                o.Name = "Default";
                o.TokenLifespan = TimeSpan.FromHours(1);
            });

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(1);
            });

            services.AddDbContext<ApplicationDbContext>(opts => opts.UseSqlServer(Configuration["ConnectionString:RectaMasterDb"]));

            services.AddScoped<AuthenticationStateProvider, CustomAuthenticationStateProvider>();
            services.AddHttpContextAccessor();

            #region repostories
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IJobSeekerRepository, JobSeekerRepository>();
            services.AddScoped<IRecruiterRepository, RecruiterRepository>();
            services.AddScoped<ICountryDetailRepository, CountryDetailRepository>();
            services.AddScoped<IMaritalStatusRepository, MaritalStatusRepository>();
            services.AddScoped<IJobSeekerPersonalInformationRepository, JobSeekerPersonalInformationRepository>();
            services.AddScoped<IJobSeekerQualificationRepository, JobSeekerQualificationRepository>();
            services.AddScoped<IJobSeekerWorkExperienceRepository, JobSeekerWorkExperienceRepository>();
            services.AddScoped<ISpecializationRepository, SpecializationRepository>();
            services.AddScoped<IEducationLevelRepository, EducationLevelRepository>();
            services.AddScoped<IExperienceLevelRepository, ExperienceLevelRepository>();
            services.AddScoped<IJobSeekerAttachmentRepository, JobSeekerAttachmentRepository>();
            services.AddScoped<IRecruiterRepository, RecruiterRepository>();
            services.AddScoped<IRecruiterCompanyRepository, RecruiterCompanyRepository>();
            services.AddScoped<IRecruiterPostedJobsRepository, RecruiterPostedJobsRepository>();
            services.AddScoped<IRecruiterAttachmentRepository, RecruiterAttachmentRepository>();
            services.AddScoped<IRecruiterPostedJobsAndApplicantsRepository, RecruiterPostedJobsAndApplicantsRepository>();
            services.AddScoped<IEmployeeTypeRepository, EmployeeTypeRepository>();
            services.AddScoped<ISalaryRangeRepository, SalaryRangeRepository>();
            services.AddScoped<ICompanyTypeRepository, CompanyTypeRepository>();
            services.AddScoped<IFileUploadRepository, FileUploadRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IMessageGeneralRepository, MessageGeneralRepository>();
            services.AddScoped<ISessionRepository, SessionRepository>();
            services.AddScoped<ITestimonialRepository, TestimonialRepository>();
            services.AddScoped<IPagesRepository, PagesRepository>();
            services.AddScoped<IFaqRepository, FaqRepository>();
            services.AddScoped<IWebSettingsRepository, WebSettingsRepository>();
            services.AddScoped<IEmailSubscriptionRepository, EmailSubscriptionRepository>();
            services.AddScoped<IContactUsRepository, ContactUsRepository>();
            services.AddScoped<INewsRepository, NewsRepository>();
            services.AddScoped<IWebsiteVisitorCounterRepository, WebsiteVisitorCounterRepository>();
            services.AddScoped<IForgotPasswordRepository, ForgotPasswordRepository>();
            #endregion

            #region services
            services.AddTransient<IJobSeekerService, JobSeekerService>();
            services.AddScoped<IRecruiterService, RecruiterService>();
            services.AddScoped<ICountryDetailsService, CountryDetailsService>();
            services.AddScoped<IMaritalStatusService, MaritalStatusService>();
            services.AddScoped<IJobSeekerPersonalInformationService, JobSeekerPersonalInformationService>();
            services.AddScoped<IJobSeekerQualificationService, JobSeekerQualificationService>();
            services.AddScoped<IJobSeekerWorkExperienceService, JobSeekerWorkExperienceService>();
            services.AddScoped<IEducationLevelService, EducationLevelService>();
            services.AddScoped<ISpecializationService, SpecializationService>();
            services.AddScoped<IJobSeekerAttachmentService, JobSeekerAttachmentService>();
            services.AddScoped<IExperienceLevelService, ExperienceLevelService>();
            services.AddScoped<IRecruiterService, RecruiterService>();
            services.AddScoped<IRecruiterCompanyService, RecruiterCompanyService>();
            services.AddScoped<IRecruiterPostedJobsService, RecruiterPostedJobsService>();
            services.AddScoped<IRecruiterAttachmentService, RecruiterAttachmentService>();
            services.AddScoped<IEmployeeTypeService, EmployeeTypeService>();
            services.AddScoped<ISalaryRangeService, SalaryRangeService>();
            services.AddScoped<ICompanyTypeService, CompanyTypeService>();
            services.AddScoped<IFileUploadService, FileUploadService>();
            services.AddScoped<IFileUpload, FileUpload>();
            services.AddScoped<IJobSeekerDashboardService, JobSeekerDashboardService>();
            services.AddScoped<IRecruiterDashboardService, RecruiterDashboardService>();
            services.AddScoped<IRecruiterPostedJobsAndApplicantsService, RecruiterPostedJobsAndApplicantsService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IMessageGeneralService, MessageGeneralService>();
            services.AddScoped<ISessionService, SessionService>();
            services.AddScoped<Ofwpro.Services.Interfaces.INotificationService, Ofwpro.Services.Services.NotificationService>();
            services.AddScoped<ITestimonialService, TestimonialService>();
            services.AddScoped<IPagesService, PagesService>();
            services.AddScoped<IFaqService, FaqService>();
            services.AddScoped<IWebSettingsService, WebSettingsService>();
            services.AddScoped<IEmailSubscriptionService, EmailSubscriptionService>();
            services.AddScoped<IContactUsService, ContactUsService>();
            services.AddScoped<INewsService, NewsService>();
            services.AddScoped<IWebsiteVisitorCounterService, WebsiteVisitorCounterService>();
            services.AddScoped<IForgotPasswordService, ForgotPasswordService>();
            #endregion

            //Blazor Service
            services.AddBlazoredLocalStorage();

            //services.AddSweetAlert2();
            services.AddSweetAlert2(options => {
                options.Theme = SweetAlertTheme.Minimal;
                options.SetThemeForColorSchemePreference(ColorScheme.Light, SweetAlertTheme.Default);
                options.SetThemeForColorSchemePreference(ColorScheme.Dark, SweetAlertTheme.Dark);
            });

            //Website Visitors
            
            services.AddBlazoredModal();

            services.AddSingleton<HttpClient>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
            app.UseMiddleware(typeof(CounterMiddleware));
            
        }
    }
}
