﻿using LinqKit;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class JobSeekerRepository: IJobSeekerRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public JobSeekerRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<JobSeeker>> All(string orderByColumn, string orderBy, string searchTerm, JobSeeker jobSeekerObj,bool? isApproved=null)
        {
            var predicate = PredicateBuilder.New<JobSeeker>(true);
            var predicateInner = PredicateBuilder.New<JobSeeker>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                predicateInner = predicateInner.Or(i => i.NationalNumber.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.Title.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.Title.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.FirstName.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.LastName.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.FatherName.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.MotherName.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.Gender.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.Mobile.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.CountryCode.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.Email.Contains(searchTerm));
                predicateInner = predicateInner.Or(i => i.Title.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (jobSeekerObj.JobSeekerId > 0)
            {
                predicate = predicate.And(i => i.JobSeekerId == jobSeekerObj.JobSeekerId);
                isPradicate = true;
            }
            //if (!string.IsNullOrEmpty(jobSeekerObj.JobSeekerNameEn))
            //{
            //    predicate = predicate.And(i => i.JobSeekerNameEn.Contains(jobSeekerObj.JobSeekerNameEn));
            //    isPradicate = true;
            //}
            //if (!string.IsNullOrEmpty(jobSeekerObj.JobSeekerNameAr))
            //{
            //    predicate = predicate.And(i => i.JobSeekerNameAr.Contains(jobSeekerObj.JobSeekerNameAr));
            //    isPradicate = true;
            //}
            if (!string.IsNullOrEmpty(jobSeekerObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == jobSeekerObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }
            if (isApproved != null) 
            {
                predicate = predicate.And(x => x.IsApproved == isApproved);
            }

            // Return Results
            try
            {
                if (isPradicate)
                {
                    if (orderBy.ToLower().Equals("asc"))
                    {
                        return await this.unitOfWork.Context.JobSeeker.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                    }
                    else
                    {
                        return await this.unitOfWork.Context.JobSeeker.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                    }
                }
                else
                {
                    if (orderBy.ToLower().Equals("asc"))
                    {
                        return await this.unitOfWork.Context.JobSeeker.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                    }
                    else
                    {
                        return await this.unitOfWork.Context.JobSeeker.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Task<JobSeeker> GetById(long jobSeekerId)
        {
            return unitOfWork.Context.JobSeeker.Where(x => !x.IsDeleted && x.JobSeekerId == jobSeekerId).FirstOrDefaultAsync();
        }

        public long Save(JobSeeker jobSeekerObj)
        {
            if (jobSeekerObj.JobSeekerId <= 0)
            {
                jobSeekerObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<JobSeeker>().Add(jobSeekerObj);
            }
            else
            {
                jobSeekerObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(jobSeekerObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeeker.Update(jobSeekerObj);
            }

            unitOfWork.Commit();
            unitOfWork.Context.Entry(jobSeekerObj).State = EntityState.Detached;
            return jobSeekerObj.JobSeekerId;
        }


        public bool Activate(long jobSeekerId, bool isActive)
        {
            JobSeeker jobSeekerObj = unitOfWork.Context.JobSeeker.Where(x => x.JobSeekerId == jobSeekerId).ToList().LastOrDefault();
            if (jobSeekerObj != null)
            {
                jobSeekerObj.IsActive = isActive;
                jobSeekerObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(jobSeekerObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeeker.Update(jobSeekerObj);
                unitOfWork.Commit();
                unitOfWork.Context.Entry(jobSeekerObj).State = EntityState.Detached;
                return true;
            }
            return false;
        }

        public bool Delete(long jobSeekerId)
        {
            var jobSeekerObj = unitOfWork.Context.JobSeeker.Where(x => !x.IsDeleted && x.JobSeekerId == jobSeekerId).ToList().LastOrDefault();

            if (jobSeekerObj != null)
            {
                jobSeekerObj.UpdatedOn = DateTime.Now;
                jobSeekerObj.IsDeleted = true;
                unitOfWork.Context.Entry(jobSeekerObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeeker.Update(jobSeekerObj);
                unitOfWork.Context.Entry(jobSeekerObj).State = EntityState.Detached;
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool IsExist(string email, long jobSeekerId)
        {
            var predicate = PredicateBuilder.New<JobSeeker>(true);

            predicate = predicate.And(i => i.Email == email);
            if (jobSeekerId > 0)
            {
                predicate = predicate.And(i => i.JobSeekerId != jobSeekerId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.JobSeeker.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }
        
        public bool Validate(string email, string password)
        {
            var predicate = PredicateBuilder.New<JobSeeker>(true);

            predicate = predicate.And(i => i.Email == email);
            predicate = predicate.And(i => i.Password == password);
            predicate = predicate.And(i => i.IsActive == true);
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.JobSeeker.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public Task<JobSeeker> GetByEmail(string email)
        {
            return unitOfWork.Context.JobSeeker.Where(x => !x.IsDeleted && x.Email == email).FirstOrDefaultAsync();
        }
   
        public Task<JobSeeker> GetByJobSeekerIdToken(string jobSeekerIdToken)
        {
            return unitOfWork.Context.JobSeeker.Where(x => !x.IsDeleted && x.JobSeekerIdToken == jobSeekerIdToken).FirstOrDefaultAsync();
        }

        public Task<JobSeeker> GetByActivationToken(string accActivationToken)
        {
            return unitOfWork.Context.JobSeeker.Where(x => !x.IsDeleted && x.AccActivationToken == accActivationToken).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<JobSeeker>> GetAll()
        {
            return await unitOfWork.Context.JobSeeker.Where(x => !x.IsDeleted).OrderBy(x => x.FirstName).ToListAsync();
        }
        public async Task<IEnumerable<JobSeeker>> GetTopEightLatestJobSeekers()
        {
            return await unitOfWork.Context.JobSeeker.Where(x =>x.IsActive && !x.IsDeleted).OrderByDescending(x => (x.UpdatedOn.Date > x.CreatedOn.Date ? x.UpdatedOn : x.CreatedOn)).Take(8).ToListAsync();
        }
        public async Task<IEnumerable<JobSeeker>> Filter(string searchText, string searchLocation, string experienceLevel)
        {
            var jobSeekers = await unitOfWork.Context.JobSeeker.Where(x =>x.IsActive && !x.IsDeleted).ToListAsync();

            var countryDetails = unitOfWork.Context.CountryDetails.ToList();

            if (searchText.Trim().ToLower() != "")
            {
                jobSeekers = jobSeekers.Where(x => x.Title.Trim().ToLower().Contains(searchText) || (x.FirstName + " " + x.LastName).Trim().ToLower().Contains(searchText) || x.Email.Trim().ToLower().Contains(searchText)).ToList();
            }


            if (searchLocation.ToLower().Trim() != "")
            {
                var countries = countryDetails.Where(c => c.Name.Trim().ToLower().Contains(searchLocation.ToLower().Trim())).ToList();

                jobSeekers = jobSeekers.Where(x => (countries.Any(r => r.Code == x.CountryCode))).ToList();
            }

            if (experienceLevel.Trim().ToLower() != "")
            {
                var jobSeekerExperience = unitOfWork.Context.JobSeekerWorkExperience.Where(x => x.ExperienceLevel.Trim().ToLower() == experienceLevel.Trim().ToLower()).ToList();


                jobSeekers = jobSeekers.Where(x => jobSeekerExperience.Any(je => je.JobSeekerId == x.JobSeekerId)).ToList();
            }



            return jobSeekers.OrderByDescending(x => (x.UpdatedOn.Date > x.CreatedOn.Date ? x.UpdatedOn : x.CreatedOn)).ToList();
        }

        public async Task<IEnumerable<JobSeeker>> GetByPostedJob(long postedJobId)
        {
            var postedJob = unitOfWork.Context.RecruiterPostedJobsAndApplicants.Where(x => x.RecruiterPostedJobsId == postedJobId).ToList();

            var jobSeekers = await unitOfWork.Context.JobSeeker.Where(x => !x.IsDeleted).ToListAsync();

            jobSeekers = jobSeekers.Where(x => postedJob.Any(p => p.JobSeekerId == x.JobSeekerId)).ToList();

            return jobSeekers;
        }

        public long NoOfCVs()
        {
            return unitOfWork.Context.JobSeeker.Where(x =>x.IsActive && !x.IsDeleted).Count();
        }
    }
}
