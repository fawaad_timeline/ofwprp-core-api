﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class RecruiterAllRequestDTO
    {
        public string token { get; set; }
        public string job_title_p { get; set; }
        public string city_p { get; set; }
        public long salary_range_id_enc_p { get; set; }
        public long company_type_p { get; set; }
        public long employment_type_p { get; set; }
        public string q { get; set; }
        public string offset { get; set; }
        public string pagination { get; set; }
    }
}
