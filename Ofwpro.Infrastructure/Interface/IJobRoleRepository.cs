﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IJobRoleRepository
    {
        Task<Tuple<IEnumerable<JobRoles>, int>> GetAll(int offset, int paging);
        JobRoles GetById(long id);
        long Add(JobRoles jobRole);
        long Update(JobRoles jobRole);
        void Delete(long id);
        bool IsExists(string name);
    }
}
