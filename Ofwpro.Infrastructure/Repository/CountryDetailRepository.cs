﻿using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class CountryDetailRepository : ICountryDetailRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public CountryDetailRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<CountryDetails>> GetAll()
        {
            var result = _unitOfWork.Context.CountryDetails.OrderBy(x => x.Name);
            return await result.ToListAsync();
        }
        public CountryDetails GetById(long id)
        {
            var countryDetail = _unitOfWork.Context.CountryDetails.Where(x => x.Id == id).FirstOrDefault();
            return countryDetail;
        }

        public CountryDetails GetByCode(string code)
        {
            var countryDetail = _unitOfWork.Context.CountryDetails.Where(x => x.Code == code).FirstOrDefault();
            return countryDetail;
        }
    }
}
