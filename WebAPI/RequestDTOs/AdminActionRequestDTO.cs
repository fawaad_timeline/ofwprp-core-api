﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class AdminActionRequestDTO
    {
        public long Id { get; set; }
        public string Token { get; set; }
    }
}
