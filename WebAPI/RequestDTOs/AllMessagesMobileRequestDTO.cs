﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class AllMessagesMobileRequestDTO
    {
        public string token { get; set; }
        public long job_seeker_id_enc { get; set; }
        public long recruiter_id_enc { get; set; }
    }
}
