﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ofwpro.Services.DTOs
{
    public class SpecializationSearchDTO
    {
         public int offset { get; set; }
        public int paging { get; set; }
        public string orderByColumn { get; set; }
        public string orderBy { get; set; }
        public string searchTerm { get; set; }
        public string SpecializationNameEn { get; set; }
        public string SpecializationNameAr { get; set; }
        public string Lan { get; set; }
    }
}
