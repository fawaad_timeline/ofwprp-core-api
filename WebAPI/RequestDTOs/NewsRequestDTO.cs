﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class NewsRequestDTO
    {
        public long Id { get; set; }
        public string NewsTitle { get; set; }
        public DateTime NewsDate { get; set; }
        public string NewsDescription { get; set; }
        public string Icon { get; set; }
        public string Lan { get; set; }

        public bool IsActive { get; set; }

        //public List<NewsFilesRequestDTO> NewsFilesList { get; set; }
    }
}
