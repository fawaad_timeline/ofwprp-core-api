﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JsonArrResponseDTO
    {
        public string code { get; set; }
        public string MSG { get; set; }
        public dynamic data { get; set; }
    }
}
