﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class JobSeekerSaveRequestDTO
    {
        public string token { get; set; }
        public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public DateTime dob { get; set; }
        public string gender { get; set; }
        public string mobile { get; set; }
        public string country_no { get; set; }
        public string country_code { get; set; }        
        public string password { get; set; }
    }
}
