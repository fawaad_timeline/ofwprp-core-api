﻿using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models.JobSeeker
{
    public class Dashboard
    {
        private readonly IJobSeekerWorkExperienceService jobSeekerWorkExperienceService;

        public Dashboard(IJobSeekerWorkExperienceService jobSeekerWorkExperienceService)
        {
            this.jobSeekerWorkExperienceService = jobSeekerWorkExperienceService;
        }

        public async Task<WorkExperience> GetJobSeekerWorkExperience(long JobSeekerId)
        {
            WorkExperience workExperience = null;

            try 
            {
                var workExperienceObj = await jobSeekerWorkExperienceService.GetByJobSeekerId(JobSeekerId);


                if(workExperienceObj != null)
                {
                    workExperience = new WorkExperience()
                    {
                        ExperienceLevel = workExperienceObj.ExperienceLevel,

                        Company1 = workExperienceObj.Company1,
                        JobTitle1 = workExperienceObj.JobTitle1,
                        City1 = workExperienceObj.City1,
                        CountryId1 = workExperienceObj.CountryId1.ToString(),
                        TimePeriodFrom1 = Convert.ToDateTime(workExperienceObj.TimePeriodFrom1),
                        TimePeriodTo1 = Convert.ToDateTime(workExperienceObj.TimePeriodTo1),
                        ReasonForLeaving1 = workExperienceObj.ReasonForLeaving1,

                        Company2 = workExperienceObj.Company2,
                        JobTitle2 = workExperienceObj.JobTitle2,
                        City2 = workExperienceObj.City2,
                        CountryId2 = workExperienceObj.CountryId2.ToString(),
                        TimePeriodFrom2 = Convert.ToDateTime(workExperienceObj.TimePeriodFrom2),
                        TimePeriodTo2 = Convert.ToDateTime(workExperienceObj.TimePeriodTo2),
                        ReasonForLeaving2 = workExperienceObj.ReasonForLeaving2,

                        Company3 = workExperienceObj.Company3,
                        JobTitle3 = workExperienceObj.JobTitle3,
                        City3 = workExperienceObj.City3,
                        CountryId3 = workExperienceObj.CountryId3.ToString(),
                        TimePeriodFrom3 = Convert.ToDateTime(workExperienceObj.TimePeriodFrom3),
                        TimePeriodTo3 = Convert.ToDateTime(workExperienceObj.TimePeriodTo3),
                        ReasonForLeaving3 = workExperienceObj.ReasonForLeaving3,
                    };
                }
            }
            catch(Exception ex) { }

            return workExperience;
        }
    }
}
