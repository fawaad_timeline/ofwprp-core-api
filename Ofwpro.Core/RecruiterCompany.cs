﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class RecruiterCompany
    {
        protected long recruiterCompanyId, recruiterId;
        protected string companyName, noOfEmployees, aboutCompany, websiteUrl, createdBy, updatedBy;
        protected DateTime createdOn, updatedOn;
        protected bool isActive, isDeleted;

        protected string phoneNumber, address, description;


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long RecruiterCompanyId { get => recruiterCompanyId; set => recruiterCompanyId = value; }
        public long RecruiterId { get => recruiterId; set => recruiterId = value; }
        public string NoOfEmployees { get => noOfEmployees; set => noOfEmployees = value; }
        public string CompanyName { get => companyName; set => companyName = value; }
        public string AboutCompany { get => aboutCompany; set => aboutCompany = value; }
        public string WebsiteUrl { get => websiteUrl; set => websiteUrl = value; }
        public bool IsActive { get => isActive; set => isActive = value; }


        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string Address { get => address; set => address = value; }
        public string Description { get => description; set => description = value; }

        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }
    }
}
