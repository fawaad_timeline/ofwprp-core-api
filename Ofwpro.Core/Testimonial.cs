﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class Testimonial
    {
        protected long testimonialId;
        protected string fullName, designation, testimonialText, createdBy, updatedBy;
        protected DateTime createdOn, updatedOn;
        protected bool isActive, isDeleted;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TestimonialId { get => testimonialId; set => testimonialId = value; }
        public string FullName { get => fullName; set => fullName = value; }
        public string Designation { get => designation; set => designation = value; }
        public string TestimonialText { get => testimonialText; set => testimonialText = value; }

        public bool IsActive { get => isActive; set => isActive = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }
    }
}
