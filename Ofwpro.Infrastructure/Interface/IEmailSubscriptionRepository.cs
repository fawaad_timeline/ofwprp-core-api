﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IEmailSubscriptionRepository
    {
        long Save(EmailSubscription emailSubscription);
    }
}
