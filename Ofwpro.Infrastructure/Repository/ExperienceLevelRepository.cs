﻿using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class ExperienceLevelRepository : IExperienceLevelRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public ExperienceLevelRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public int Add(ExperienceLevel experienceLevel)
        {
            //experienceLevel.Id = Guid.NewGuid().ToString();
            _unitOfWork.Context.Set<ExperienceLevel>().Add(experienceLevel);
            _unitOfWork.Commit();
            return experienceLevel.Id;
        }

        public void Delete(int id)
        {
            var experienceLevel = _unitOfWork.Context.ExperienceLevel.Where(x => x.Id == id).FirstOrDefault();
            if (experienceLevel != null)
            {
                _unitOfWork.Context.ExperienceLevel.Remove(experienceLevel);
                _unitOfWork.Commit();
            }
        }

        public async Task<int> GetAllCount()
        {
            return await _unitOfWork.Context.ExperienceLevel.CountAsync();
        }
        public async Task<IEnumerable<ExperienceLevel>> GetAll()
        {
            return await _unitOfWork.Context.ExperienceLevel.OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<Tuple<IEnumerable<ExperienceLevel>, int>> GetAll(int offset, int paging)
        {
            var recordsCount = _unitOfWork.Context.ExperienceLevel.Count();
            var dataList = await _unitOfWork.Context.ExperienceLevel.OrderBy(x => x.Name).Skip(offset * paging).Take(paging).ToListAsync();
            return new Tuple<IEnumerable<ExperienceLevel>, int>(dataList, recordsCount);
        }
        public ExperienceLevel GetById(int id)
        {
            var expList = _unitOfWork.Context.ExperienceLevel.Where(x => x.Id == id).FirstOrDefault();
            return expList;
        }

        public bool IsExists(string name)
        {
            bool IsExists = false;
            var experienceLevel = _unitOfWork.Context.ExperienceLevel.Where(x => x.Name == name);
            if (experienceLevel.Any())
                IsExists = true;
            else
                IsExists = false;
            return IsExists;
        }

        public int Update(ExperienceLevel experienceLevel)
        {
            _unitOfWork.Context.Entry(experienceLevel).State = EntityState.Modified;
            _unitOfWork.Context.ExperienceLevel.Update(experienceLevel);
            _unitOfWork.Commit();
            return experienceLevel.Id;
        }
    }
}
