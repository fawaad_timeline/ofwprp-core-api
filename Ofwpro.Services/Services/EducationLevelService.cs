﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class EducationLevelService: IEducationLevelService
    {
		private readonly IEducationLevelRepository _IEducationLevelRepository;
		public EducationLevelService(IEducationLevelRepository iEducationLevelRepository)
		{
			_IEducationLevelRepository = iEducationLevelRepository;
		}

        public long Add(EducationLevels educationLevel)
        {
            return _IEducationLevelRepository.Add(educationLevel);
        }

        public void Delete(long id)
        {
            _IEducationLevelRepository.Delete(id);
        }

        public async Task<Tuple<IEnumerable<EducationLevels>, int>> GetAll(int offset, int paging)
        {
			return await _IEducationLevelRepository.GetAll(offset, paging);
		}

		public EducationLevels GetById(long id)
		{
			return _IEducationLevelRepository.GetById(id);
		}

        public bool IsExists(string name)
        {
            return _IEducationLevelRepository.IsExists(name);
        }

        public long Update(EducationLevels educationLevel)
        {
            return _IEducationLevelRepository.Update(educationLevel);
        }
    }
}
