﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class ForgotPasswordRepository: IForgotPasswordRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public ForgotPasswordRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<ForgotPassword>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, ForgotPassword forgotPasswordObj)
        {
            var predicate = PredicateBuilder.New<ForgotPassword>(true);
            var predicateInner = PredicateBuilder.New<ForgotPassword>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                //predicateInner = predicateInner.Or(i => i.ForgotPasswordNameEn.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.ForgotPasswordNameAr.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (forgotPasswordObj.ForgotPasswordId > 0)
            {
                predicate = predicate.And(i => i.ForgotPasswordId == forgotPasswordObj.ForgotPasswordId);
                isPradicate = true;
            }
            //if (!string.IsNullOrEmpty(forgotPasswordObj.ForgotPasswordNameEn))
            //{
            //    predicate = predicate.And(i => i.ForgotPasswordNameEn.Contains(forgotPasswordObj.ForgotPasswordNameEn));
            //    isPradicate = true;
            //}
            //if (!string.IsNullOrEmpty(forgotPasswordObj.ForgotPasswordNameAr))
            //{
            //    predicate = predicate.And(i => i.ForgotPasswordNameAr.Contains(forgotPasswordObj.ForgotPasswordNameAr));
            //    isPradicate = true;
            //}
            if (!string.IsNullOrEmpty(forgotPasswordObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == forgotPasswordObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.ForgotPassword.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.ForgotPassword.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.ForgotPassword.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.ForgotPassword.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<ForgotPassword> GetById(long forgotPasswordId)
        {
            return unitOfWork.Context.ForgotPassword.Where(x => !x.IsDeleted && x.ForgotPasswordId == forgotPasswordId).FirstOrDefaultAsync();
        }

        public long Save(ForgotPassword forgotPasswordObj)
        {
            if (forgotPasswordObj.ForgotPasswordId <= 0)
            {
                forgotPasswordObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<ForgotPassword>().Add(forgotPasswordObj);
            }
            else
            {
                forgotPasswordObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(forgotPasswordObj).State = EntityState.Modified;
                unitOfWork.Context.ForgotPassword.Update(forgotPasswordObj);
            }

            unitOfWork.Commit();
            return forgotPasswordObj.ForgotPasswordId;
        }


        public bool Activate(long forgotPasswordId, bool isActive)
        {
            ForgotPassword forgotPasswordObj = unitOfWork.Context.ForgotPassword.Where(x => x.ForgotPasswordId == forgotPasswordId).ToList().LastOrDefault();
            if (forgotPasswordObj != null)
            {
                forgotPasswordObj.IsActive = isActive;
                forgotPasswordObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(forgotPasswordObj).State = EntityState.Modified;
                unitOfWork.Context.ForgotPassword.Update(forgotPasswordObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long forgotPasswordId)
        {
            var forgotPasswordObj = unitOfWork.Context.ForgotPassword.Where(x => !x.IsDeleted && x.ForgotPasswordId == forgotPasswordId).ToList().LastOrDefault();

            if (forgotPasswordObj != null)
            {
                forgotPasswordObj.UpdatedOn = DateTime.Now;
                forgotPasswordObj.IsDeleted = true;
                unitOfWork.Context.Entry(forgotPasswordObj).State = EntityState.Modified;
                unitOfWork.Context.ForgotPassword.Update(forgotPasswordObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool IsExist(string email, long forgotPasswordId)
        {
            var predicate = PredicateBuilder.New<ForgotPassword>(true);

            //predicate = predicate.And(i => i.Email == email);
            if (forgotPasswordId > 0)
            {
                predicate = predicate.And(i => i.ForgotPasswordId != forgotPasswordId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.ForgotPassword.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }
        
        public bool Validate(string email, string password)
        {
            var predicate = PredicateBuilder.New<ForgotPassword>(true);

            //predicate = predicate.And(i => i.Email == email);
            //predicate = predicate.And(i => i.Password == password);

            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.ForgotPassword.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public Task<ForgotPassword> GetByUserId(long userId)
        {
            return unitOfWork.Context.ForgotPassword.Where(x => !x.IsDeleted && x.UserId == userId).FirstOrDefaultAsync();
        }

        public Task<ForgotPassword> GetByUserToken(string token)
        {
            return unitOfWork.Context.ForgotPassword.Where(x => !x.IsDeleted && x.ForgotPasswordToken == token).FirstOrDefaultAsync();
        }
    }
}
