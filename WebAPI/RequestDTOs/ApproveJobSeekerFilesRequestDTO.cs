﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class ApproveJobSeekerFilesRequestDTO
    {
        public long jobSeekerId { get; set; }
        public bool isApproved { get; set; }
        public string ApprovedType { get; set; }
    }
}
