﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models.Recruiter
{
    public class CompanyDetails
    {
        public long RecruiterCompanyId { get; set; }
        [Required]
        public string CompanyName { get; set; }

        [Required]
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string OtherInformation { get; set; }

        public string CompanyLogoURL { get; set; }
    }
}
