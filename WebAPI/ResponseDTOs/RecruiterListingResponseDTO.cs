﻿
using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class RecruiterListingResponseDTO
    {

        public long RecruiterId { get; set; }
        public string RecruiterIdToken { get; set; }
        public string NationalNumber { get; set; }
        //public string Title { get => title; set => title = value; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        //public DateTime Dob { get => dob; set => dob = value; }
        public string Gender { get; set; }
        public string Mobile { get; set; }
        public string CountryNo { get; set; }
        public string CountryCode { get; set; }
        public string CompanyName { get; set; }
        public string CurrentDesignation { get; set; }
        public string UserType { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string AppleDeviceToken { get; set; }
        public string AndroidDeviceToken { get; set; }
        public string AccActivationToken { get; set; }
        //
        public List<RecruiterPostedJobs> recruiterPostedJobs { get; set; }
        //
        public string logo { get; set; }
        public string logo_url { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string CompanyAddress { get; internal set; }
        public string AboutCompany { get; internal set; }
        public string CompanyDescription { get; internal set; }
        public string CompanyNoOfEmployees { get; internal set; }
        public string CompanyPhoneNumber { get; internal set; }
        public string CompanyWebsiteURL { get; internal set; }
    }
}

