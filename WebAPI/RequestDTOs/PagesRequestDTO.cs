﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class PagesRequestDTO
    {
        public long Id { get; set; }
        public string ContactUs { get; set; }
        public string AboutUs { get; set; }
       
        public bool IsActive { get; set; }
    }
}
