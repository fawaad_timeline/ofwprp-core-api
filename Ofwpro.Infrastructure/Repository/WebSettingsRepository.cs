﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class WebSettingsRepository : IWebSettingsRepository
    { 
        private readonly IUnitOfWork unitOfWork;

        public WebSettingsRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<WebSettings> All(int settingId)
        {
            if (settingId > 0)
            {
                 return await this.unitOfWork.Context.WebSettings.Where(x => x.Id == settingId).SingleOrDefaultAsync();
            }
            return null;
        }

        public Task<WebSettings> GetById(long WebSettingsId)
        {
            return unitOfWork.Context.WebSettings.Where(x => x.Id == WebSettingsId).FirstOrDefaultAsync();
        }

        public long AddUpdate(WebSettings WebSettingsObj)
        {
            if (WebSettingsObj.Id <= 0)
            {
                unitOfWork.Context.Set<WebSettings>().Add(WebSettingsObj);
            }
            else
            {
                unitOfWork.Context.Entry(WebSettingsObj).State = EntityState.Modified;
                unitOfWork.Context.WebSettings.Update(WebSettingsObj);
            }

            unitOfWork.Commit();
            return WebSettingsObj.Id;
        }

        public bool Delete(long WebSettingsId)
        {
            var WebSettingsObj = unitOfWork.Context.WebSettings.Where(x => x.Id == WebSettingsId).ToList().LastOrDefault();

            if (WebSettingsObj != null)
            {
                unitOfWork.Context.WebSettings.Remove(WebSettingsObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public async Task<IEnumerable<WebSettings>> GetAll()
        {
            return await unitOfWork.Context.WebSettings.ToListAsync();
        }
        public async Task<WebSettings> GetFirstRecord()
        {
            return await unitOfWork.Context.WebSettings.FirstOrDefaultAsync();
        }
    }
}
