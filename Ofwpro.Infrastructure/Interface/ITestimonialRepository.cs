﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface ITestimonialRepository
    {
        Task<IEnumerable<Testimonial>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, Testimonial testimonialObj);
        Task<Testimonial> GetById(long testimonialId);
        long Save(Testimonial testimonialObj);
        bool Activate(long testimonialId, bool isActive);
        bool Delete(long testimonialId);
        bool IsExist(string testimonialName, long TestimonialId);

        Task<IEnumerable<Testimonial>> GetAll();
    }
}
