﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class LoginResponseDTO
    {
        public string token { get; set; }
        public long id { get; set; }
        public string first_name { get; set; }
        public string last_name  { get; set; }
        public string title { get; set; }
        public string logo_url { get; set; }
    }
}
