﻿using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class WebsiteVisitorCounterRepository : IWebsiteVisitorCounterRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public WebsiteVisitorCounterRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<WebsiteVisitorCounter>> GetAll()
        {
            return await unitOfWork.Context.WebsiteVisitorCounter.ToListAsync();
        }
        public Task<WebsiteVisitorCounter> GetByVisitorId(string visitorId)
        {
            return unitOfWork.Context.WebsiteVisitorCounter.Where(x => x.VisitorId == visitorId).FirstOrDefaultAsync();
        }
        public long Save(WebsiteVisitorCounter websiteVisitorCounter)
        {
            if(websiteVisitorCounter.WebsiteVisitorCounterId <= 0)
            {
                websiteVisitorCounter.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<WebsiteVisitorCounter>().Add(websiteVisitorCounter);
            }
            else
            {
                websiteVisitorCounter.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(websiteVisitorCounter).State = EntityState.Modified;
                unitOfWork.Context.WebsiteVisitorCounter.Update(websiteVisitorCounter);
            }

            unitOfWork.Commit();
            unitOfWork.Context.Entry(websiteVisitorCounter).State = EntityState.Detached;
            return websiteVisitorCounter.WebsiteVisitorCounterId;
        }
    }
}
