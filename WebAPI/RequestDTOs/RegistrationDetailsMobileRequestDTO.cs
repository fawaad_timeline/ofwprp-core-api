﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class RegistrationDetailsMobileRequestDTO
    {
        public string token { get; set; }
    }
}
