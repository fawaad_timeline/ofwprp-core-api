﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class SaveDeviceTokenAndroidRequestDTO
    {
        public string token { get; set; }
        public string android_device_token { get; set; }
    }
}
