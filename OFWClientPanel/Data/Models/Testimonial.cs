﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models
{
    public class Testimonial
    {
        public long TestimonialId { get; set; }
        public string FullName { get; set; }
        public string Designation { get; set; }
        public string TestimonialText { get; set; }
    }
}
