﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class JobSeekerForgotPasswordRequestDTO
    {
        public string email { get; set; }
        public string device_uid { get; set; }
        public string device_type { get; set; }
        public string is_ajax { get; set; }
    }
}
