﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class JobSeekerDetailRequestDTO
    {
        public long job_seeker_id_enc { get; set; }
        public string token { get; set; }
    }
}
