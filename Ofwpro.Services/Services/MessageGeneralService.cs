﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class MessageGeneralService: IMessageGeneralService
    {
        private readonly IMessageGeneralRepository messageGeneralRepository;

        public MessageGeneralService(IMessageGeneralRepository messageGeneralRepository)
        {
            this.messageGeneralRepository = messageGeneralRepository;
        }

        public async Task<IEnumerable<MessageGeneral>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, MessageGeneral messageGeneralObj)
        {
            return await messageGeneralRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, messageGeneralObj);
        }

        public async Task<MessageGeneral> GetById(long messageGeneralId)
        {
            return await messageGeneralRepository.GetById(messageGeneralId);
        }

        public long Save(MessageGeneral messageGeneralObj)
        {
            return messageGeneralRepository.Save(messageGeneralObj);
        }

        public bool Activate(long messageGeneralId, bool isActive)
        {
            return messageGeneralRepository.Activate(messageGeneralId, isActive);
        }

        public bool Delete(long messageGeneralId)
        {
            return messageGeneralRepository.Delete(messageGeneralId);
        }

        public int TotalMessagesUnreadRecruiter(long jobSeekerId, long recruiterId)
        {
            return messageGeneralRepository.TotalMessagesUnreadRecruiter(jobSeekerId, recruiterId);
        }

        public int TotalMessagesUnreadJobSeeker(long jobSeekerId, long recruiterId)
        {
            return messageGeneralRepository.TotalMessagesUnreadJobSeeker(jobSeekerId, recruiterId);
        }

        public bool MarkMessageReadJobSeeker(long jobSeekerId, long recruiterId)
        {
            return messageGeneralRepository.MarkMessageReadJobSeeker(jobSeekerId, recruiterId);
        }

        public bool MarkMessageReadRecruiter(long jobSeekerId, long recruiterId)
        {
            return messageGeneralRepository.MarkMessageReadRecruiter(jobSeekerId, recruiterId);
        }

        public IEnumerable<long> GetChatGroupsJobSeeker(long jobSeekerId)
        {
            return messageGeneralRepository.GetChatGroupsJobSeeker(jobSeekerId);
        }

        public IEnumerable<long> GetChatGroupsRecruiter(long recruiterId)
        {
            return messageGeneralRepository.GetChatGroupsRecruiter(recruiterId);
        }
       

        public int GetTotalMessages(long jobSeekerId, long recruiterId)
        {
            return messageGeneralRepository.GetTotalMessages(jobSeekerId, recruiterId);
        }

        public MessageGeneral GetLastMessages(long jobSeekerId, long recruiterId)
        {
            return messageGeneralRepository.GetLastMessages(jobSeekerId, recruiterId);
        }

        public List<MessageGeneral> GetAllMessages(long jobSeekerId, long recruiterId, int offset, int paging)
        {
            return messageGeneralRepository.GetAllMessages(jobSeekerId, recruiterId, offset, paging);
        }

        public Task<IEnumerable<MessageGeneral>> GetAllMessagesGeneral(string orderByColumn, string orderBy, string searchTerm, bool isActive, long jobSeekerId, long recruiterId)
        {
            return messageGeneralRepository.GetAllMessagesGeneral(orderByColumn, orderBy, searchTerm, isActive, jobSeekerId, recruiterId);
        }
        public bool DeleteMessagesOfJobSeeker(long jobSeekerId, long recruiterId, string userType)
        {
            return messageGeneralRepository.DeleteMessagesOfJobSeeker(jobSeekerId, recruiterId, userType);
        }
        public bool DeleteMessagesOfRecruiter(long jobSeekerId, long recruiterId, string userType)
        {
            return messageGeneralRepository.DeleteMessagesOfRecruiter(jobSeekerId, recruiterId, userType);
        }
    }
}
