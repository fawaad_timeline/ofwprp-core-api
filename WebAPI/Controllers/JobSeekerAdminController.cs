﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using MimeKit;
using MailKit.Net.Smtp;
using PusherServer;
using Ofwpro.Services.Interfaces;
using Ofwpro.Core;
using System.Linq.Expressions;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class JobSeekerAdmin : ControllerBase
    {
        private readonly IFileUploadService fileUploadService;
        private readonly IJobSeekerService jobSeekerService;
        private readonly IAdministratorService administratorService;
        private readonly ISessionService sessionService;
        private readonly IJobSeekerPersonalInformationService jobSeekerPersonalInformationService;
        private readonly IJobSeekerQualificationService jobSeekerQualificationService;
        private readonly IJobSeekerWorkExperienceService jobSeekerWorkExperienceService;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IJobSeekerAttachmentService jobSeekerAttachmentService;
        private readonly IRecruiterPostedJobsService recruiterPostedJobsService;
        private readonly IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService;
        private readonly ICountryDetailsService countryDetailsService;
        private readonly IRecruiterAttachmentService recruiterAttachmentService;
        private readonly IEducationLevelService educationLevelService;
        private readonly ISpecializationService specializationService;
        private readonly IMessageAgainstJobService messageAgainstJobService;
        private readonly IMaritalStatusService maritalStatusService;
        private readonly IForgotPasswordService forgotPasswordService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public JobSeekerAdmin(IFileUploadService fileUploadService, IJobSeekerService jobSeekerService, IAdministratorService administratorService, ISessionService sessionService, IJobSeekerPersonalInformationService jobSeekerPersonalInformationService, IJobSeekerQualificationService jobSeekerQualificationService, IJobSeekerWorkExperienceService jobSeekerWorkExperienceService, IWebHostEnvironment hostingEnvironment, IJobSeekerAttachmentService jobSeekerAttachmentService, IRecruiterPostedJobsService recruiterPostedJobsService, IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService, ICountryDetailsService countryDetailsService, IRecruiterAttachmentService recruiterAttachmentService, IEducationLevelService educationLevelService, ISpecializationService specializationService, IMessageAgainstJobService messageAgainstJobService, IMaritalStatusService maritalStatusService, IForgotPasswordService forgotPasswordService, IMapper mapper, IConfiguration config)
        {
            this.fileUploadService = fileUploadService;
            this.jobSeekerService = jobSeekerService;
            this.administratorService = administratorService;
            this.sessionService = sessionService;
            this.jobSeekerPersonalInformationService = jobSeekerPersonalInformationService;
            this.jobSeekerQualificationService = jobSeekerQualificationService;
            this.jobSeekerWorkExperienceService = jobSeekerWorkExperienceService;
            this.hostingEnvironment = hostingEnvironment;
            this.jobSeekerAttachmentService = jobSeekerAttachmentService;
            this.recruiterPostedJobsService = recruiterPostedJobsService;
            this.recruiterPostedJobsAndApplicantsService = recruiterPostedJobsAndApplicantsService;
            this.countryDetailsService = countryDetailsService;
            this.recruiterAttachmentService = recruiterAttachmentService;
            this.educationLevelService = educationLevelService;
            this.specializationService = specializationService;
            this.messageAgainstJobService = messageAgainstJobService;
            this.maritalStatusService = maritalStatusService;
            this.forgotPasswordService = forgotPasswordService;
            this.mapper = mapper;
            this.config = config;
        }

        [HttpPost]
        [Route("en/job_seeker/ajax_all_job_seekers_admin")]
        public async Task<object> ajax_all_job_seekers_admin([FromBody] JobSeekerAllRequestAdminDTO model)
        {
            string lan = "en";
            int totalRecords = 0;
            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode=200 };
            List<JobSeekerListingAdminResponseDTO> JobSeekerListingResponseDTOList = null;
            List<JobSeekerListingAdminResponseDTO> JobSeekerResponseList = new List<JobSeekerListingAdminResponseDTO>();

            int offset = Convert.ToInt32(model.offset);
            int paging = Convert.ToInt32(model.pagination);
            string orderByColumn = "CreatedOn";
            string orderBy = "DESC";

            JobSeeker jobSeekerObj = new JobSeeker();
            jobSeekerObj.IsActive = true;

            string hostUrl = config["Utility:APIBaseURL"];
            IEnumerable<JobSeeker> jobSeekerList = null;
            try
            {
                jobSeekerList = await jobSeekerService.All(orderByColumn, orderBy, model.q, jobSeekerObj, model.isApproved);
            }
            catch (Exception ex)
            {
                return "Database Issue: " + ex.StackTrace;
            }            
            if (!jobSeekerList.Any())
            {

            }
            else
            {
                if (!string.IsNullOrWhiteSpace(model.has_video))
                {
                    if (model.has_video.Equals("Y"))
                    {
                        JobSeekerAttachment jobSeekerAttachmentObj = new JobSeekerAttachment();
                        jobSeekerAttachmentObj.IsActive = true;
                        jobSeekerAttachmentObj.DocumentType = "video_resume";
                        IEnumerable<JobSeekerAttachment> jobSeekerAttachmentList = await jobSeekerAttachmentService.All(0, 9999999, "CreatedOn", "ASC", "", jobSeekerAttachmentObj);
                        List<long> jobSeekerIdExemptList = jobSeekerAttachmentList.Select(x => x.JobSeekerId).ToList();
                        if (!jobSeekerIdExemptList.Any()) { jobSeekerIdExemptList.Add(9999999999999); }
                        jobSeekerList = jobSeekerList.Where(x => jobSeekerIdExemptList.Contains(x.JobSeekerId));
                    }
                }
                if (model.isAttachment != true)
                {
                    totalRecords = jobSeekerList.Count();
                    jobSeekerList = jobSeekerList.Skip(paging * offset).Take(paging).ToList();
                }
                JobSeekerListingResponseDTOList = mapper.Map<List<JobSeekerListingAdminResponseDTO>>(jobSeekerList);
                foreach (JobSeekerListingAdminResponseDTO JobSeekerListingAdminResponseDTOObj in JobSeekerListingResponseDTOList)
                {
                    bool isExist = jobSeekerAttachmentService.IsExistDocumentType(JobSeekerListingAdminResponseDTOObj.job_seeker_id_enc, "video_resume");
                    bool isPictureExist = jobSeekerAttachmentService.IsExistDocumentType(JobSeekerListingAdminResponseDTOObj.job_seeker_id_enc, "picture");
                    //JobSeekerListingAdminResponseDTOObj.IsActive = ;
                    JobSeekerListingAdminResponseDTOObj.has_video = "No";
                    if (isExist == true)
                    {
                        JobSeekerListingAdminResponseDTOObj.has_video = "Yes";
                    }

                    JobSeekerAttachment jobSeekerAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType("picture");
                    string UploadedFrom = "";
                    if (jobSeekerAttachmentObj?.UploadedFrom != null) UploadedFrom = jobSeekerAttachmentObj.UploadedFrom;

                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (jobSeekerAttachmentObj != null)
                    {
                        JobSeekerListingAdminResponseDTOObj.picture = hostUrl + "/Uploads/" + jobSeekerAttachmentObj.FileNameUpdated;
                    }
                    else
                    {
                        if (JobSeekerListingAdminResponseDTOObj.gender.Equals("male"))
                        {
                            JobSeekerListingAdminResponseDTOObj.picture = hostUrl + "/assets/images/default_male.png";
                        }
                        else
                        {
                            JobSeekerListingAdminResponseDTOObj.picture = hostUrl + "/assets/images/default_female.png";
                        }
                    }

                    // Personal Information
                    JobSeekerPersonalInformationResponseDTO jobSeekerPersonalInformationResponseDTOObj = new JobSeekerPersonalInformationResponseDTO();
                    JobSeekerPersonalInformation jobSeekerPersonalInformationObj = await jobSeekerPersonalInformationService.GetByJobSeekerId(JobSeekerListingAdminResponseDTOObj.job_seeker_id_enc);
                    if (jobSeekerPersonalInformationObj != null)
                    {
                        jobSeekerPersonalInformationResponseDTOObj.tbl_job_seeker_personal_information_id = jobSeekerPersonalInformationObj.JobSeekerPersonalInformationId;
                        jobSeekerPersonalInformationResponseDTOObj.tbl_job_seeker_id = jobSeekerPersonalInformationObj.JobSeekerId;
                        jobSeekerPersonalInformationResponseDTOObj.nationality = jobSeekerPersonalInformationObj.CountryId;
                        jobSeekerPersonalInformationResponseDTOObj.tbl_marital_status_id = jobSeekerPersonalInformationObj.MaritalStatusId;
                        jobSeekerPersonalInformationResponseDTOObj.home_phone = jobSeekerPersonalInformationObj.HomePhone;
                        jobSeekerPersonalInformationResponseDTOObj.mobileCode = jobSeekerPersonalInformationObj.MobileCode;
                        jobSeekerPersonalInformationResponseDTOObj.is_driving_license = (jobSeekerPersonalInformationObj?.IsDrivingLicense == "Y") ? "Yes" : "No";
                        jobSeekerPersonalInformationResponseDTOObj.current_location = jobSeekerPersonalInformationObj.CurrentLocation;
                        jobSeekerPersonalInformationResponseDTOObj.visa_status = jobSeekerPersonalInformationObj.VisaStatus;
                        jobSeekerPersonalInformationResponseDTOObj.added_date = jobSeekerPersonalInformationObj.CreatedOn;
                        jobSeekerPersonalInformationResponseDTOObj.is_active = jobSeekerPersonalInformationObj.IsActive == true ? "Y" : "N";

                        jobSeekerPersonalInformationResponseDTOObj.countryDetails = countryDetailsService.GetById(jobSeekerPersonalInformationObj.CountryId);
                        jobSeekerPersonalInformationResponseDTOObj.maritalStatus = maritalStatusService.GetById(jobSeekerPersonalInformationObj.MaritalStatusId);
                    }

                    JobSeekerListingAdminResponseDTOObj.jobSeekerPersonalInformation = jobSeekerPersonalInformationResponseDTOObj;

                    // Qualification
                    JobSeekerQualificationResponseDTO jobSeekerQualificationResponseDTOObj = new JobSeekerQualificationResponseDTO();
                    JobSeekerQualification jobSeekerQualificationObj = await jobSeekerQualificationService.GetByJobSeekerId(JobSeekerListingAdminResponseDTOObj.job_seeker_id_enc);
                    if (jobSeekerQualificationObj != null)
                    {
                        jobSeekerQualificationResponseDTOObj.tbl_job_seeker_qualification_id = jobSeekerQualificationObj.JobSeekerQualificationId;
                        jobSeekerQualificationResponseDTOObj.tbl_job_seeker_id = jobSeekerQualificationObj.JobSeekerId;
                        jobSeekerQualificationResponseDTOObj.tbl_education_level_id = jobSeekerQualificationObj.EducationLevelId;
                        jobSeekerQualificationResponseDTOObj.tbl_specialization_id = jobSeekerQualificationObj.SpecializationId;
                        jobSeekerQualificationResponseDTOObj.school_university = jobSeekerQualificationObj.SchoolUniversity;
                        jobSeekerQualificationResponseDTOObj.graduation_date = jobSeekerQualificationObj.GraduationDate;
                        jobSeekerQualificationResponseDTOObj.added_date = jobSeekerQualificationObj.CreatedOn;
                        jobSeekerQualificationResponseDTOObj.is_active = jobSeekerQualificationObj.IsActive == true ? "Y" : "N";

                        jobSeekerQualificationResponseDTOObj.educationLevels = educationLevelService.GetById(jobSeekerQualificationObj.EducationLevelId);
                        jobSeekerQualificationResponseDTOObj.jobSeekerQualification = await jobSeekerQualificationService.GetById(jobSeekerQualificationObj.JobSeekerQualificationId);
                        jobSeekerQualificationResponseDTOObj.specialization = await specializationService.GetById(jobSeekerQualificationObj.SpecializationId);
                    }
                    JobSeekerListingAdminResponseDTOObj.jobSeekerQualification = jobSeekerQualificationResponseDTOObj;

                    // Work Experience
                    List<JobSeekerWorkExperienceDetailsArrResponseDTO> work_experienceList = new List<JobSeekerWorkExperienceDetailsArrResponseDTO>();
                    JobSeekerWorkExperience jobSeekerWorkExperienceObj = await jobSeekerWorkExperienceService.GetByJobSeekerId(JobSeekerListingAdminResponseDTOObj.job_seeker_id_enc);

                    JobSeekerWorkExperienceDetailsArrResponseDTO jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();
                    if (jobSeekerWorkExperienceObj != null)
                    {
                        if (jobSeekerWorkExperienceObj.CountryId1 != 0)
                        {
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj.Company1;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj.JobTitle1;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj.CountryId1;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj.City1;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj.TimePeriodFrom1;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj.TimePeriodTo1;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj.ReasonForLeaving1;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.countryDetails = countryDetailsService.GetById(jobSeekerWorkExperienceObj.CountryId1);
                            work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);
                        }
                        if (jobSeekerWorkExperienceObj.CountryId2 != 0)
                        {

                            jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj.Company2;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj.JobTitle2;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj.CountryId2;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj.City2;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj.TimePeriodFrom2;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj.TimePeriodTo2;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj.ReasonForLeaving2;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.countryDetails = countryDetailsService.GetById(jobSeekerWorkExperienceObj.CountryId2);
                            work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);
                        }
                        if (jobSeekerWorkExperienceObj.CountryId3 != 0)
                        {
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj.Company3;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj.JobTitle3;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj.CountryId3;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj.City3;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj.TimePeriodFrom3;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj.TimePeriodTo3;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj.ReasonForLeaving3;
                            jobSeekerWorkExperienceDetailsArrResponseDTOObj.countryDetails = countryDetailsService.GetById(jobSeekerWorkExperienceObj.CountryId3);
                            work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);
                        }
                    }
                    JobSeekerListingAdminResponseDTOObj.work_experience = work_experienceList;
                    JobSeekerListingAdminResponseDTOObj.experience_level = jobSeekerWorkExperienceObj?.ExperienceLevel;

                    // Attachments
                    AttachmentJobSeekerResponseDTO attachmentJobSeekerResponseDTO = new AttachmentJobSeekerResponseDTO();

                    #region resume Url
                    JobSeekerAttachment RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(JobSeekerListingAdminResponseDTOObj.job_seeker_id_enc, "resume");
                    string fileNameUpdated = "", Path = "";
                    if (RecruiterAttachmentObj?.UploadedFrom != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;

                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }

                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.resume = Path;
                    #endregion

                    #region id_card Url
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(JobSeekerListingAdminResponseDTOObj.job_seeker_id_enc, "id_card");
                    fileNameUpdated = "";
                    Path = "";
                    if (RecruiterAttachmentObj?.UploadedFrom != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;

                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }

                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.id_card = Path;
                    #endregion

                    #region passport Url
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(JobSeekerListingAdminResponseDTOObj.job_seeker_id_enc, "passport");
                    if (RecruiterAttachmentObj?.UploadedFrom != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;

                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    fileNameUpdated = ""; Path = "";
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }

                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.passport = Path;
                    #endregion

                    #region video_resume Url
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(JobSeekerListingAdminResponseDTOObj.job_seeker_id_enc, "video_resume");
                    fileNameUpdated = ""; Path = "";
                    if (RecruiterAttachmentObj?.UploadedFrom != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;

                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }

                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.video_resume = Path;
                    #endregion

                    #region picture Url
                    RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(JobSeekerListingAdminResponseDTOObj.job_seeker_id_enc, "picture");
                    fileNameUpdated = "";
                    Path = "";
                    if (RecruiterAttachmentObj != null) UploadedFrom = RecruiterAttachmentObj.UploadedFrom;

                    if (UploadedFrom == "Client") hostUrl = config["Utility:ClientBaseURL"].ToString();
                    else if (UploadedFrom == "Admin") hostUrl = config["Utility:APIBaseURL"].ToString();
                    if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }

                    if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
                    attachmentJobSeekerResponseDTO.picture = Path;
                    #endregion

                    JobSeekerListingAdminResponseDTOObj.attachmentJobSeeker = attachmentJobSeekerResponseDTO;
                    if (model.isAttachment == true && (isPictureExist || isExist))
                    {
                        JobSeekerResponseList.Add(JobSeekerListingAdminResponseDTOObj);
                        totalRecords++;
                    }

                    var appliedJobs = await recruiterPostedJobsAndApplicantsService.GetByJobSeeker(JobSeekerListingAdminResponseDTOObj.job_seeker_id_enc);

                    JobSeekerListingAdminResponseDTOObj.applied_jobs = appliedJobs.Count();
                }
            }
            if (model.isAttachment?? false)
            {
                JobSeekerResponseList = JobSeekerResponseList.Skip(paging * offset).Take(paging).ToList();
            }
            JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
            JsonArrLoginResponseDTOObj.code = "1";
            JsonArrLoginResponseDTOObj.MSG = "success";
            JsonArrLoginResponseDTOObj.totalRecords = totalRecords;
            JsonArrLoginResponseDTOObj.data = (model.isAttachment == true) ? JobSeekerResponseList : JobSeekerListingResponseDTOList;
            return JsonArrLoginResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/ajax_register_save_changes_admin")]
        public async Task<JsonArrResponseDTO> ajax_register_save_changes_admin([FromBody] JobSeekerSaveRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeeker jobSeekerObjOld = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }
                }

                if (isSuccess == true)
                {
                    long administratorId = sessionObj.UserId;
                    IEnumerable<Administrator> administratorList = await administratorService.Get(administratorId);
                    if (administratorList.Any())
                    {
                        Administrator administratorObj = administratorList.FirstOrDefault();
                        string role = administratorObj.Role;
                        if (!role.Equals("super-admin"))
                        {
                            JsonArrResponseDTOObj.code = "0";
                            JsonArrResponseDTOObj.MSG = "Not authorized.";
                            return JsonArrResponseDTOObj;
                        }
                    }
                    else
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Invalid user";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(model.title))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Title is blank. Please enter title.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.first_name))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "First Name is blank. Please enter First Name.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.last_name))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Last Name is blank. Please enter Last Name.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(model.dob)))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Date of Birth is blank. Please enter Date of Birth.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.gender))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Gender is blank. Please enter Gender.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (!model.gender.Equals("male") && !model.gender.Equals("female"))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Gender can only be male or female.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(model.mobile))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Mobile is blank. Please enter Mobile.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    jobSeekerObjOld = await jobSeekerService.GetById(sessionObj.UserId);

                    JobSeeker jobSeekerObj = null;
                    jobSeekerObj = jobSeekerObjOld;
                    jobSeekerObj.Title = model.title;
                    jobSeekerObj.FirstName = model.first_name;
                    jobSeekerObj.LastName = model.last_name;
                    jobSeekerObj.Dob = model.dob;
                    jobSeekerObj.Gender = model.gender;
                    jobSeekerObj.Mobile = model.mobile;
                    jobSeekerObj.CountryNo = model.country_no;
                    jobSeekerObj.CountryCode = model.country_code;

                    if (!string.IsNullOrEmpty(model.password))
                    {
                        jobSeekerObj.Password = UtilityHelper.sha1(model.password);
                    }
                    jobSeekerObjOld.UpdatedOn = DateTime.Now;
                    jobSeekerObjOld.UpdatedBy = loggedInUserId;

                    long jobSeekerId = jobSeekerService.Save(jobSeekerObj);
                    // Email Code Goes Here

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Changes saved successfully.";
                    return JsonArrResponseDTOObj;

                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/ajax_save_personal_information_admin")]
        public async Task<JsonArrResponseDTO> ajax_save_personal_information_admin([FromBody] JobSeekerSavePersonalInformationAdminRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeekerPersonalInformation jobSeekerPersonalInformationObjOld = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.Token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.Token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && !sessionObj.UserType.Equals("admin"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(Convert.ToString(model.country_id_enc)) || model.country_id_enc <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Nationality is blank. Please select Nationality.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(model.marital_status_id_enc)) || model.marital_status_id_enc <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Marital Status is blank. Please select Marital Status.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    jobSeekerPersonalInformationObjOld = await jobSeekerPersonalInformationService.GetById(model.Id);
                    if (jobSeekerPersonalInformationObjOld != null)
                    {
                        JobSeekerPersonalInformation jobSeekerPersonalInformationObj = mapper.Map<JobSeekerPersonalInformation>(model);

                        jobSeekerPersonalInformationObj.CountryId = model.country_id_enc;
                        jobSeekerPersonalInformationObj.MaritalStatusId = model.marital_status_id_enc;
                        jobSeekerPersonalInformationObj.HomePhone = model.home_phone;
                        jobSeekerPersonalInformationObj.MobileCode = model.mobileCode;
                        jobSeekerPersonalInformationObj.IsDrivingLicense = model.is_driving_license;
                        jobSeekerPersonalInformationObj.CurrentLocation = model.current_location;
                        jobSeekerPersonalInformationObj.VisaStatus = model.visa_status;

                        jobSeekerPersonalInformationObj.UpdatedOn = DateTime.Now;
                        jobSeekerPersonalInformationObj.UpdatedBy = loggedInUserId;

                        jobSeekerPersonalInformationService.Save(jobSeekerPersonalInformationObj);
                    }
                    else
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "No data found.";
                        return JsonArrResponseDTOObj;
                    }

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Changes saved successfully.";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/ajax_save_qualification_admin")]
        public async Task<JsonArrResponseDTO> ajax_save_qualification_admin([FromBody] JobSeekerSaveQualificationAdminRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            JobSeekerQualification jobSeekerQualificationObjOld = null;
            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.Token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.Token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && !sessionObj.UserType.Equals("admin"))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(Convert.ToString(model.education_level_id_enc)) || model.education_level_id_enc <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Education Level is blank. Please select Education Level.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(model.school_university)))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "School/University is blank. Please enter School/University.";
                        return JsonArrResponseDTOObj;
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(model.graduation_date)))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Graduation Date is blank. Please enter Graduation Date.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    jobSeekerQualificationObjOld = await jobSeekerQualificationService.GetById(model.Id);
                    if (jobSeekerQualificationObjOld != null)
                    {
                        JobSeekerQualification jobSeekerQualificationObj = mapper.Map<JobSeekerQualification>(model);
                        jobSeekerQualificationObj.EducationLevelId = model.education_level_id_enc;
                        jobSeekerQualificationObj.SpecializationId = model.specialization_id_enc;
                        jobSeekerQualificationObj.SchoolUniversity = model.school_university;
                        jobSeekerQualificationObj.GraduationDate = model.graduation_date;
                        jobSeekerQualificationObj.NameOfDegree = model.name_of_degree;

                        jobSeekerQualificationObj.UpdatedOn = DateTime.Now;
                        jobSeekerQualificationObj.UpdatedBy = loggedInUserId;

                        jobSeekerQualificationService.Save(jobSeekerQualificationObj);
                    }
                    else
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "No data found.";
                        return JsonArrResponseDTOObj;
                    }

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Changes saved successfully.";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be saved";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }



        //[HttpPost]
        //[Route("en/job_seeker/ajax_save_work_experience_admin")]
        //public async Task<JsonArrResponseDTO> ajax_save_qualification_admin([FromBody] JobSeekerSaveWorkExperienceRequestDTO model)
        //{
        //    ResponseDTO<object> response = new ResponseDTO<object>();
        //    string loggedInUserId = "";

        //    JobSeekerWorkExperience jobSeekerWorkExperienceObjOld = null;
        //    bool isSuccess = true;
        //    bool isExist = false;
        //    Session sessionObj = null;

        //    JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

        //    try
        //    {
        //        // Validate Token
        //        if (isSuccess == true)
        //        {
        //            if (string.IsNullOrWhiteSpace(model.token))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }

        //            if (isSuccess == true)
        //            {
        //                sessionObj = await sessionService.GetByToken(model.token);
        //                if (sessionObj == null)
        //                {
        //                    JsonArrResponseDTOObj.code = "2";
        //                    JsonArrResponseDTOObj.MSG = "Invalid token";
        //                    return JsonArrResponseDTOObj;
        //                }
        //            }

        //            if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }
        //        }

        //        // Create/Edit(Save)
        //        if (isSuccess == true)
        //        {
        //            jobSeekerWorkExperienceObjOld = await jobSeekerWorkExperienceService.GetByJobSeekerId(sessionObj.UserId);
        //            if (jobSeekerWorkExperienceObjOld != null)
        //            {
        //                jobSeekerWorkExperienceObjOld.ExperienceLevel = model.experience_level;
        //                jobSeekerWorkExperienceObjOld.Company1 = model.company1;
        //                jobSeekerWorkExperienceObjOld.JobTitle1 = model.job_title1;
        //                jobSeekerWorkExperienceObjOld.CountryId1 = model.country_id_enc1;
        //                jobSeekerWorkExperienceObjOld.City1 = model.city1;
        //                jobSeekerWorkExperienceObjOld.TimePeriodFrom1 = model.time_period_from1;
        //                jobSeekerWorkExperienceObjOld.TimePeriodTo1 = model.time_period_to1;
        //                jobSeekerWorkExperienceObjOld.ReasonForLeaving1 = model.reason_for_leaving1;

        //                jobSeekerWorkExperienceObjOld.Company2 = model.company2;
        //                jobSeekerWorkExperienceObjOld.JobTitle2 = model.job_title2;
        //                jobSeekerWorkExperienceObjOld.CountryId2 = model.country_id_enc2;
        //                jobSeekerWorkExperienceObjOld.City2 = model.city2;
        //                jobSeekerWorkExperienceObjOld.TimePeriodFrom2 = model.time_period_from2;
        //                jobSeekerWorkExperienceObjOld.TimePeriodTo2 = model.time_period_to2;
        //                jobSeekerWorkExperienceObjOld.ReasonForLeaving2 = model.reason_for_leaving2;

        //                jobSeekerWorkExperienceObjOld.Company3 = model.company3;
        //                jobSeekerWorkExperienceObjOld.JobTitle3 = model.job_title3;
        //                jobSeekerWorkExperienceObjOld.CountryId3 = model.country_id_enc3;
        //                jobSeekerWorkExperienceObjOld.City3 = model.city3;
        //                jobSeekerWorkExperienceObjOld.TimePeriodFrom3 = model.time_period_from3;
        //                jobSeekerWorkExperienceObjOld.TimePeriodTo3 = model.time_period_to3;
        //                jobSeekerWorkExperienceObjOld.ReasonForLeaving3 = model.reason_for_leaving3;

        //                jobSeekerWorkExperienceObjOld.UpdatedOn = DateTime.Now;
        //                jobSeekerWorkExperienceObjOld.UpdatedBy = loggedInUserId;

        //                jobSeekerWorkExperienceService.Save(jobSeekerWorkExperienceObjOld);
        //            }
        //            else
        //            {
        //                JobSeekerWorkExperience jobSeekerWorkExperienceObj = mapper.Map<JobSeekerWorkExperience>(model);
        //                jobSeekerWorkExperienceObj.JobSeekerWorkExperienceId = 0;
        //                jobSeekerWorkExperienceObj.JobSeekerId = sessionObj.UserId;
        //                jobSeekerWorkExperienceObj.CreatedOn = DateTime.Now;
        //                jobSeekerWorkExperienceObj.CreatedBy = loggedInUserId;
        //                jobSeekerWorkExperienceObj.IsDeleted = false;
        //                jobSeekerWorkExperienceObj.IsActive = true;
        //                jobSeekerWorkExperienceService.Save(jobSeekerWorkExperienceObj);
        //            }

        //            JsonArrResponseDTOObj.code = "1";
        //            JsonArrResponseDTOObj.MSG = "Changes saved successfully.";
        //            return JsonArrResponseDTOObj;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        JsonArrResponseDTOObj.code = "0";
        //        JsonArrResponseDTOObj.MSG = "Information could not be saved";
        //        return JsonArrResponseDTOObj;
        //    }

        //    return JsonArrResponseDTOObj;
        //}


        //[HttpPost, DisableRequestSizeLimit]
        //[Consumes("multipart/form-data")]
        //[Route("en/job_seeker/upload_file_admin")]
        //public async Task<object> upload_file_admin([FromForm] AppsUploadRequestDTO model)
        //{
        //    ResponseDTO<object> response = new ResponseDTO<object>();
        //    string loggedInUserId = "";
        //    Session sessionObj = null;

        //    int StatusCode = 0;
        //    bool isSuccess = true;
        //    string Message = "";
        //    string ExceptionMessage = "";

        //    string FileTypeConfig = "";
        //    string FileSizeConfig = "";

        //    FileTypeConfig = config["FileConfig:FileTypeConfig"];
        //    FileSizeConfig = config["FileConfig:FileSizeConfig"];
        //    string hostUrl = config["Utility:APIBaseURL"];

        //    JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

        //    //ApplicationUser currentUser = await userResolverService.GetCurrentUser();
        //    string userId = "";

        //    // Validate Token
        //    if (isSuccess == true)
        //    {
        //        if (string.IsNullOrWhiteSpace(model.token))
        //        {
        //            JsonArrResponseDTOObj.code = "2";
        //            JsonArrResponseDTOObj.MSG = "Invalid token";
        //            return JsonArrResponseDTOObj;
        //        }

        //        if (isSuccess == true)
        //        {
        //            sessionObj = await sessionService.GetByToken(model.token);
        //            if (sessionObj == null)
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }
        //        }

        //        if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
        //        {
        //            JsonArrResponseDTOObj.code = "2";
        //            JsonArrResponseDTOObj.MSG = "Invalid token";
        //            return JsonArrResponseDTOObj;
        //        }
        //    }

        //    if (isSuccess == true)
        //    {
        //        if (string.IsNullOrWhiteSpace(model.document_type))
        //        {
        //            JsonArrResponseDTOObj.code = "0";
        //            JsonArrResponseDTOObj.MSG = "[document_type] is blank. Please enter [document_type].";
        //            return JsonArrResponseDTOObj;
        //        }
        //        else if (!model.document_type.Equals("resume") && !model.document_type.Equals("video_resume") && !model.document_type.Equals("passport") && !model.document_type.Equals("id_card") && !model.document_type.Equals("picture"))
        //        {
        //            JsonArrResponseDTOObj.code = "0";
        //            JsonArrResponseDTOObj.MSG = "Invalid document type.";
        //            return JsonArrResponseDTOObj;
        //        }
        //    }

        //    try
        //    {
        //        var file = model.myfile;

        //        string webRootPath = hostingEnvironment.WebRootPath;
        //        string contentRootPath = hostingEnvironment.ContentRootPath;

        //        string newPath = Path.Combine(webRootPath, "assets");
        //        newPath = Path.Combine(newPath, "uploads");

        //        if (!Directory.Exists(newPath))
        //        {
        //            Directory.CreateDirectory(newPath);
        //        }

        //        if (isSuccess == true)
        //        {
        //            if (file.Length > 0)
        //            {
        //                string fileNameUpdated = "";
        //                string fileNameOriginal = "";
        //                string fileType = "";
        //                int fileSize = 0;

        //                string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
        //                string fullPath = Path.Combine(newPath, string.Empty);
        //                fileNameOriginal = Path.GetFileName(file.FileName);
        //                string fileExt = fileNameOriginal.Substring(fileNameOriginal.LastIndexOf('.'), fileNameOriginal.Length - fileNameOriginal.LastIndexOf('.'));
        //                string fileExtension = Path.GetExtension(fileNameOriginal);

        //                fileNameUpdated = Guid.NewGuid().ToString() + fileExt;
        //                if (file.ContentType == null)
        //                {
        //                    fileType = UtilityHelper.GetContentType(file.FileName);
        //                }
        //                else
        //                {
        //                    fileType = file.ContentType.Split('/')[0].ToString();
        //                }
        //                if (fileType == "application") fileType = "document";
        //                fileSize = Int32.Parse(file.Length.ToString());

        //                if (isSuccess == true && fileSize > Convert.ToInt32(FileSizeConfig))
        //                {
        //                    JsonArrResponseDTOObj.code = "0";
        //                    JsonArrResponseDTOObj.MSG = "File size cannot be greater than " + Convert.ToInt32(FileSizeConfig) + " KB";
        //                    return JsonArrResponseDTOObj;
        //                }

        //                if (model.document_type.Equals("video_resume") && !fileExtension.Contains("mp4"))
        //                {
        //                    JsonArrResponseDTOObj.code = "0";
        //                    JsonArrResponseDTOObj.MSG = "Only .mp4 files are allowed.";
        //                    return JsonArrResponseDTOObj;
        //                }

        //                if (isSuccess == true)
        //                {
        //                    string savedFileName = Path.Combine(fullPath, fileNameUpdated);
        //                    using (var stream = new FileStream(savedFileName, FileMode.Create))
        //                    {
        //                        file.CopyTo(stream);
        //                    }

        //                    jobSeekerAttachmentService.Delete(sessionObj.UserId, model.document_type);

        //                    JobSeekerAttachment jobSeekerAttachmentObj = new JobSeekerAttachment();
        //                    jobSeekerAttachmentObj.JobSeekerAttachmentId = 0;
        //                    jobSeekerAttachmentObj.JobSeekerId = sessionObj.UserId;
        //                    jobSeekerAttachmentObj.DocumentType = model.document_type;
        //                    jobSeekerAttachmentObj.FileNameOriginal = fileNameOriginal;
        //                    jobSeekerAttachmentObj.FileNameUpdated = fileNameUpdated;
        //                    jobSeekerAttachmentObj.FileType = fileType;
        //                    jobSeekerAttachmentObj.FileSize = Convert.ToString(fileSize);
        //                    jobSeekerAttachmentObj.IsActive = true;
        //                    jobSeekerAttachmentObj.IsDeleted = false;
        //                    jobSeekerAttachmentObj.CreatedOn = DateTime.Now;
        //                    jobSeekerAttachmentObj.CreatedBy = loggedInUserId;
        //                    jobSeekerAttachmentService.Save(jobSeekerAttachmentObj);

        //                    JsonArrUploadResponseDTO jsonArrUploadResponseDTOObj = new JsonArrUploadResponseDTO();
        //                    jsonArrUploadResponseDTOObj.code = "1";
        //                    jsonArrUploadResponseDTOObj.MSG = "File uploaded successfully.";
        //                    jsonArrUploadResponseDTOObj.path = hostUrl + "/Uploads/" + fileNameUpdated;
        //                    return jsonArrUploadResponseDTOObj;
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        JsonArrResponseDTOObj.code = "0";
        //        JsonArrResponseDTOObj.MSG = "There was an error while uploading the file.";
        //        return JsonArrResponseDTOObj;
        //    }

        //    JsonArrResponseDTOObj.code = "0";
        //    JsonArrResponseDTOObj.MSG = "There was an error while uploading the file.";
        //    return JsonArrResponseDTOObj;
        //}




        //[HttpPost]
        //[Route("en/job_seeker/personal_information_admin")]
        //public async Task<Object> personal_information_admin([FromBody] PersonalInformationMobileRequestDTO model)
        //{
        //    string token = model.token;
        //    ResponseDTO<object> response = new ResponseDTO<object>();
        //    string loggedInUserId = "";

        //    JobSeekerPersonalInformationResponseDTO jobSeekerPersonalInformationResponseDTOObj = new JobSeekerPersonalInformationResponseDTO();

        //    bool isSuccess = true;
        //    bool isExist = false;
        //    Session sessionObj = null;

        //    JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

        //    try
        //    {
        //        // Validate Token
        //        if (isSuccess == true)
        //        {
        //            if (string.IsNullOrWhiteSpace(token))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }

        //            if (isSuccess == true)
        //            {
        //                sessionObj = await sessionService.GetByToken(token);
        //                if (sessionObj == null)
        //                {
        //                    JsonArrResponseDTOObj.code = "2";
        //                    JsonArrResponseDTOObj.MSG = "Invalid token";
        //                    return JsonArrResponseDTOObj;
        //                }
        //            }

        //            if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }
        //        }

        //        // Create/Edit(Save)
        //        if (isSuccess == true)
        //        {
        //            JobSeekerPersonalInformation jobSeekerPersonalInformationObj = await jobSeekerPersonalInformationService.GetById(sessionObj.UserId);

        //            jobSeekerPersonalInformationResponseDTOObj.tbl_job_seeker_personal_information_id = jobSeekerPersonalInformationObj.JobSeekerPersonalInformationId;
        //            jobSeekerPersonalInformationResponseDTOObj.tbl_job_seeker_id = jobSeekerPersonalInformationObj.JobSeekerId;
        //            jobSeekerPersonalInformationResponseDTOObj.nationality = jobSeekerPersonalInformationObj.CountryId;
        //            jobSeekerPersonalInformationResponseDTOObj.tbl_marital_status_id = jobSeekerPersonalInformationObj.MaritalStatusId;
        //            jobSeekerPersonalInformationResponseDTOObj.home_phone = jobSeekerPersonalInformationObj.HomePhone;
        //            jobSeekerPersonalInformationResponseDTOObj.mobileCode = jobSeekerPersonalInformationObj.MobileCode;
        //            jobSeekerPersonalInformationResponseDTOObj.is_driving_license = jobSeekerPersonalInformationObj.IsDrivingLicense;
        //            jobSeekerPersonalInformationResponseDTOObj.current_location = jobSeekerPersonalInformationObj.CurrentLocation;
        //            jobSeekerPersonalInformationResponseDTOObj.visa_status = jobSeekerPersonalInformationObj.VisaStatus;
        //            jobSeekerPersonalInformationResponseDTOObj.added_date = jobSeekerPersonalInformationObj.CreatedOn;
        //            jobSeekerPersonalInformationResponseDTOObj.is_active = jobSeekerPersonalInformationObj.IsActive == true ? "Y" : "N";

        //            JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
        //            JsonArrLoginResponseDTOObj.code = "1";
        //            JsonArrLoginResponseDTOObj.MSG = "success";
        //            JsonArrLoginResponseDTOObj.data = jobSeekerPersonalInformationResponseDTOObj;
        //            return JsonArrLoginResponseDTOObj;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        JsonArrResponseDTOObj.code = "0";
        //        JsonArrResponseDTOObj.MSG = "Information could not be saved";
        //        return JsonArrResponseDTOObj;
        //    }

        //    return JsonArrResponseDTOObj;
        //}



        //[HttpPost]
        //[Route("en/job_seeker/qualification_admin")]
        //public async Task<Object> qualification_admin([FromBody] QualificationMobileRequestDTO model)
        //{
        //    string token = model.token;
        //    ResponseDTO<object> response = new ResponseDTO<object>();
        //    string loggedInUserId = "";

        //    JobSeekerQualificationResponseDTO jobSeekerQualificationResponseDTOObj = new JobSeekerQualificationResponseDTO();

        //    bool isSuccess = true;
        //    bool isExist = false;
        //    Session sessionObj = null;

        //    JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

        //    try
        //    {
        //        // Validate Token
        //        if (isSuccess == true)
        //        {
        //            if (string.IsNullOrWhiteSpace(token))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }

        //            if (isSuccess == true)
        //            {
        //                sessionObj = await sessionService.GetByToken(token);
        //                if (sessionObj == null)
        //                {
        //                    JsonArrResponseDTOObj.code = "2";
        //                    JsonArrResponseDTOObj.MSG = "Invalid token";
        //                    return JsonArrResponseDTOObj;
        //                }
        //            }

        //            if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }
        //        }

        //        // Create/Edit(Save)
        //        if (isSuccess == true)
        //        {
        //            JobSeekerQualification jobSeekerQualificationObj = await jobSeekerQualificationService.GetByJobSeekerId(sessionObj.UserId);

        //            jobSeekerQualificationResponseDTOObj.tbl_job_seeker_qualification_id = jobSeekerQualificationObj.JobSeekerQualificationId;
        //            jobSeekerQualificationResponseDTOObj.tbl_job_seeker_id = jobSeekerQualificationObj.JobSeekerId;
        //            jobSeekerQualificationResponseDTOObj.tbl_education_level_id = jobSeekerQualificationObj.EducationLevelId;
        //            jobSeekerQualificationResponseDTOObj.tbl_specialization_id = jobSeekerQualificationObj.SpecializationId;
        //            jobSeekerQualificationResponseDTOObj.school_university = jobSeekerQualificationObj.SchoolUniversity;
        //            jobSeekerQualificationResponseDTOObj.graduation_date = jobSeekerQualificationObj.GraduationDate;
        //            jobSeekerQualificationResponseDTOObj.added_date = jobSeekerQualificationObj.CreatedOn;
        //            jobSeekerQualificationResponseDTOObj.is_active = jobSeekerQualificationObj.IsActive == true ? "Y" : "N";

        //            JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
        //            JsonArrLoginResponseDTOObj.code = "1";
        //            JsonArrLoginResponseDTOObj.MSG = "success";
        //            JsonArrLoginResponseDTOObj.data = jobSeekerQualificationResponseDTOObj;
        //            return JsonArrLoginResponseDTOObj;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        JsonArrResponseDTOObj.code = "0";
        //        JsonArrResponseDTOObj.MSG = "Information could not be saved";
        //        return JsonArrResponseDTOObj;
        //    }

        //    return JsonArrResponseDTOObj;
        //}


        //[HttpPost]
        //[Route("en/job_seeker/work_experience_admin")]
        //public async Task<Object> work_experience_admin([FromBody] WorkExperienceMobileRequestDTO model)
        //{
        //    string token = model.token;
        //    ResponseDTO<object> response = new ResponseDTO<object>();
        //    string loggedInUserId = "";

        //    JobSeekerWorkExperienceDetailsResponseDTO jobSeekerWorkExperienceDetailsResponseDTOObj = new JobSeekerWorkExperienceDetailsResponseDTO();

        //    bool isSuccess = true;
        //    bool isExist = false;
        //    Session sessionObj = null;

        //    JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

        //    try
        //    {
        //        // Validate Token
        //        if (isSuccess == true)
        //        {
        //            if (string.IsNullOrWhiteSpace(token))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }

        //            if (isSuccess == true)
        //            {
        //                sessionObj = await sessionService.GetByToken(token);
        //                if (sessionObj == null)
        //                {
        //                    JsonArrResponseDTOObj.code = "2";
        //                    JsonArrResponseDTOObj.MSG = "Invalid token";
        //                    return JsonArrResponseDTOObj;
        //                }
        //            }

        //            if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }
        //        }

        //        List<JobSeekerWorkExperienceDetailsArrResponseDTO> work_experienceList = new List<JobSeekerWorkExperienceDetailsArrResponseDTO>();

        //        // Create/Edit(Save)
        //        if (isSuccess == true)
        //        {
        //            JobSeekerWorkExperience jobSeekerWorkExperienceObj = await jobSeekerWorkExperienceService.GetByJobSeekerId(sessionObj.UserId);

        //            JobSeekerWorkExperienceDetailsArrResponseDTO jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();

        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj.Company1;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj.JobTitle1;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj.CountryId1;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj.City1;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj.TimePeriodFrom1;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj.TimePeriodTo1;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj.ReasonForLeaving1;
        //            work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);

        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj.Company2;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj.JobTitle2;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj.CountryId2;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj.City2;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj.TimePeriodFrom2;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj.TimePeriodTo2;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj.ReasonForLeaving2;

        //            work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj = new JobSeekerWorkExperienceDetailsArrResponseDTO();
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.company = jobSeekerWorkExperienceObj.Company3;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.job_title = jobSeekerWorkExperienceObj.JobTitle3;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.country_id_enc = jobSeekerWorkExperienceObj.CountryId3;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.city = jobSeekerWorkExperienceObj.City3;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_from = jobSeekerWorkExperienceObj.TimePeriodFrom3;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.time_period_to = jobSeekerWorkExperienceObj.TimePeriodTo3;
        //            jobSeekerWorkExperienceDetailsArrResponseDTOObj.reason_for_leaving = jobSeekerWorkExperienceObj.ReasonForLeaving3;
        //            work_experienceList.Add(jobSeekerWorkExperienceDetailsArrResponseDTOObj);

        //            jobSeekerWorkExperienceDetailsResponseDTOObj.work_experience = work_experienceList;

        //            JsonArrWorkExperienceResponseDTO jsonArrWorkExperienceResponseDTOObj = new JsonArrWorkExperienceResponseDTO();
        //            jsonArrWorkExperienceResponseDTOObj.code = "1";
        //            jsonArrWorkExperienceResponseDTOObj.MSG = "success";
        //            jsonArrWorkExperienceResponseDTOObj.experience_level = jobSeekerWorkExperienceObj.ExperienceLevel;
        //            jsonArrWorkExperienceResponseDTOObj.data = jobSeekerWorkExperienceDetailsResponseDTOObj;
        //            return jsonArrWorkExperienceResponseDTOObj;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        JsonArrResponseDTOObj.code = "0";
        //        JsonArrResponseDTOObj.MSG = "Information could not be retrieved";
        //        return JsonArrResponseDTOObj;
        //    }

        //    return JsonArrResponseDTOObj;
        //}


        //[HttpPost]
        //[Route("en/job_seeker/ajax_registration_form_admin")]
        //public async Task<object> ajax_registration_form_admin([FromBody] RegistrationFromMobileRequestDTO model)
        //{
        //    string token = model.token;
        //    ResponseDTO<object> response = new ResponseDTO<object>();
        //    string loggedInUserId = "";

        //    JobSeekerDetailResponseDTO jobSeekerDetailResponseDTOObj = new JobSeekerDetailResponseDTO();
        //    bool isSuccess = true;
        //    bool isExist = false;
        //    Session sessionObj = null;

        //    JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();
        //    string hostUrl = config["Utility:APIBaseURL"];

        //    try
        //    {
        //        // Validate Token
        //        if (isSuccess == true)
        //        {
        //            if (string.IsNullOrWhiteSpace(token))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }

        //            if (isSuccess == true)
        //            {
        //                sessionObj = await sessionService.GetByToken(token);
        //                if (sessionObj == null)
        //                {
        //                    JsonArrResponseDTOObj.code = "2";
        //                    JsonArrResponseDTOObj.MSG = "Invalid token";
        //                    return JsonArrResponseDTOObj;
        //                }
        //            }

        //            if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }
        //        }

        //        // Create/Edit(Save)
        //        if (isSuccess == true)
        //        {
        //            JobSeeker jobSeekerObj = await jobSeekerService.GetById(sessionObj.UserId);

        //            jobSeekerDetailResponseDTOObj.tbl_job_seeker_id = jobSeekerObj.JobSeekerId;
        //            jobSeekerDetailResponseDTOObj.title = jobSeekerObj.Title;
        //            jobSeekerDetailResponseDTOObj.first_name = jobSeekerObj.FirstName;
        //            jobSeekerDetailResponseDTOObj.last_name = jobSeekerObj.LastName;
        //            jobSeekerDetailResponseDTOObj.dob = Convert.ToString(jobSeekerObj.Dob);
        //            jobSeekerDetailResponseDTOObj.gender = jobSeekerObj.Gender;
        //            jobSeekerDetailResponseDTOObj.mobile = jobSeekerObj.Mobile;
        //            jobSeekerDetailResponseDTOObj.country_no = jobSeekerObj.CountryNo;
        //            jobSeekerDetailResponseDTOObj.country_code = jobSeekerObj.CountryCode;
        //            jobSeekerDetailResponseDTOObj.email = jobSeekerObj.Email;
        //            jobSeekerDetailResponseDTOObj.last_login = Convert.ToString(sessionObj.CreatedOn);
        //            jobSeekerDetailResponseDTOObj.added_date = jobSeekerObj.CreatedOn;
        //            jobSeekerDetailResponseDTOObj.apple_device_token = jobSeekerObj.AppleDeviceToken;
        //            jobSeekerDetailResponseDTOObj.android_device_token = jobSeekerObj.AndroidDeviceToken;

        //            JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
        //            JsonArrLoginResponseDTOObj.code = "1";
        //            JsonArrLoginResponseDTOObj.MSG = "Success";
        //            JsonArrLoginResponseDTOObj.data = jobSeekerDetailResponseDTOObj;
        //            return JsonArrLoginResponseDTOObj;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        JsonArrResponseDTOObj.code = "0";
        //        JsonArrResponseDTOObj.MSG = "Information could not be retrieved";
        //        return JsonArrResponseDTOObj;
        //    }

        //    return JsonArrResponseDTOObj;
        //}


        //[HttpPost]
        //[Route("en/job_seeker/ajax_attachments_admin")]
        //public async Task<Object> ajax_attachments_admin([FromBody] AttachemntsJobSeekerRequestDTO model)
        //{
        //    string token = model.token;
        //    ResponseDTO<object> response = new ResponseDTO<object>();
        //    string loggedInUserId = "";
        //    string hostUrl = config["Utility:APIBaseURL"];
        //    AttachmentJobSeekerResponseDTO attachmentJobSeekerResponseDTO = new AttachmentJobSeekerResponseDTO();

        //    bool isSuccess = true;
        //    bool isExist = false;
        //    Session sessionObj = null;

        //    JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

        //    try
        //    {
        //        // Validate Token
        //        if (isSuccess == true)
        //        {
        //            if (string.IsNullOrWhiteSpace(token))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }

        //            if (isSuccess == true)
        //            {
        //                sessionObj = await sessionService.GetByToken(token);
        //                if (sessionObj == null)
        //                {
        //                    JsonArrResponseDTOObj.code = "2";
        //                    JsonArrResponseDTOObj.MSG = "Invalid token";
        //                    return JsonArrResponseDTOObj;
        //                }
        //            }

        //            if (isSuccess == true && sessionObj.UserType.Equals("recruiter"))
        //            {
        //                JsonArrResponseDTOObj.code = "2";
        //                JsonArrResponseDTOObj.MSG = "Invalid token";
        //                return JsonArrResponseDTOObj;
        //            }
        //        }

        //        // Create/Edit(Save)
        //        if (isSuccess == true)
        //        {
        //            JobSeekerPersonalInformation jobSeekerPersonalInformationObj = await jobSeekerPersonalInformationService.GetById(sessionObj.UserId);
        //            #region resume Url
        //            JobSeekerAttachment RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj.JobSeekerId, "resume");
        //            string fileNameUpdated = "", Path = "";
        //            if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }

        //            if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
        //            attachmentJobSeekerResponseDTO.resume = Path;
        //            #endregion

        //            #region id_card Url
        //            RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj.JobSeekerId, "id_card");
        //            fileNameUpdated = "";
        //            Path = "";
        //            if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }

        //            if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
        //            attachmentJobSeekerResponseDTO.id_card = Path;
        //            #endregion

        //            #region passport Url
        //            RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj.JobSeekerId, "passport");
        //            fileNameUpdated = ""; Path = "";
        //            if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }

        //            if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
        //            attachmentJobSeekerResponseDTO.passport = Path;
        //            #endregion

        //            #region video_resume Url
        //            RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj.JobSeekerId, "video_resume");
        //            fileNameUpdated = ""; Path = "";
        //            if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }

        //            if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
        //            attachmentJobSeekerResponseDTO.video_resume = Path;
        //            #endregion

        //            #region picture Url
        //            RecruiterAttachmentObj = await jobSeekerAttachmentService.GetByDocumentType(jobSeekerPersonalInformationObj.JobSeekerId, "picture");
        //            fileNameUpdated = "";
        //            Path = "";
        //            if (RecruiterAttachmentObj != null) { fileNameUpdated = RecruiterAttachmentObj.FileNameUpdated; }

        //            if (!string.IsNullOrEmpty(fileNameUpdated)) { Path = hostUrl + "/Uploads/" + fileNameUpdated; }
        //            attachmentJobSeekerResponseDTO.picture = Path;
        //            #endregion

        //            JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
        //            JsonArrLoginResponseDTOObj.code = "1";
        //            JsonArrLoginResponseDTOObj.MSG = "success";
        //            JsonArrLoginResponseDTOObj.data = attachmentJobSeekerResponseDTO;
        //            return JsonArrLoginResponseDTOObj;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        JsonArrResponseDTOObj.code = "0";
        //        JsonArrResponseDTOObj.MSG = "Information could not be saved";
        //        return JsonArrResponseDTOObj;
        //    }

        //    return JsonArrResponseDTOObj;
        //}


        [HttpPost]
        [Route("en/job_seeker/activedeactive_job_seeker")]
        public async Task<JsonArrResponseDTO> activedeactive_job_seeker([FromBody] DeleteJob_SeekerRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && (sessionObj.UserType.Equals("job_seeker") || (sessionObj.UserType.Equals("recruiter"))))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(model.id)) || model.id <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "[Job Seeker id] is blank.";
                        return JsonArrResponseDTOObj;
                    }
                }
                //get the object 

                jobSeekerService.Activate(model.id,model.isActive);
                if (model.isActive)
                {
                    JsonArrResponseDTOObj.MSG = "JobSeeker Active successfully.";
                }
                else
                {
                    var jobSeekerData = await jobSeekerService.GetById(model.id);
                    jobSeekerData.AndroidDeviceToken = "";
                    jobSeekerData.AppleDeviceToken = "";
                    var Id = jobSeekerService.Save(jobSeekerData);
                    var sessionToDelete = await sessionService.GetByUserId(model.id);
                    if (sessionToDelete != null)
                    {
                        sessionService.DeleteByToken(sessionToDelete.Token);
                    }
                    JsonArrResponseDTOObj.MSG = "JobSeeker Deactive successfully.";
                }
                JsonArrResponseDTOObj.code = "1";
                return JsonArrResponseDTOObj;
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be deleted";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


        [HttpPost]
        [Route("en/job_seeker/delete_job_seeker")]
        public async Task<JsonArrResponseDTO> delete_job_seeker([FromBody] DeleteJob_SeekerRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }

                    if (isSuccess == true && (sessionObj.UserType.Equals("job_seeker") || (sessionObj.UserType.Equals("recruiter"))))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(model.id)) || model.id <= 0)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Job Seeker Id  is blank.";
                        return JsonArrResponseDTOObj;
                    }
                }

                // Save
                if (isSuccess == true)
                {
                    var jobSeekerData = await jobSeekerService.GetById(model.id);
                    jobSeekerData.AndroidDeviceToken = "";
                    jobSeekerData.AppleDeviceToken = "";
                    jobSeekerData.IsDeleted = true;
                    var id = jobSeekerService.Save(jobSeekerData);
                    var jobSeekerToken = await sessionService.GetByUserId(model.id);
                    if (jobSeekerToken != null)
                    {
                        sessionService.DeleteByToken(jobSeekerToken.Token);
                    }
                    //jobSeekerService.Delete(model.id);

                    JsonArrResponseDTOObj.code = "1";
                    JsonArrResponseDTOObj.MSG = "Job Seeker deleted successfully.";
                    return JsonArrResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "Information could not be deleted";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }

    }
}