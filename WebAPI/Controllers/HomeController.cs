﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using Ofwpro.Services.Interfaces;
using Ofwpro.Core;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class HomeController : ControllerBase
    {
        private readonly IFileUploadService fileUploadService;

        private readonly IAdministratorService administratorService;
        private readonly IContactUsService contactUsService;
        private readonly ISessionService sessionService;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IRecruiterPostedJobsService recruiterPostedJobsService;
        private readonly IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService;
        private readonly ICompanyTypeService _ICompanyTypeService;
        private readonly IEmployeeTypeService _IEmployeeTypeService;
        private readonly IExperienceLevelService _IExperienceLevelService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public HomeController(IContactUsService contactUsService, IFileUploadService fileUploadService, IAdministratorService administratorService, ISessionService sessionService,
            IWebHostEnvironment hostingEnvironment, IRecruiterPostedJobsService recruiterPostedJobsService,
            IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService,
            ICompanyTypeService ICompanyTypeService, IEmployeeTypeService IEmployeeTypeService,
            IExperienceLevelService IExperienceLevelService,
            IMapper mapper, IConfiguration config)
        {
            this.fileUploadService = fileUploadService;
            this.administratorService = administratorService;
            this.sessionService = sessionService;
            this.hostingEnvironment = hostingEnvironment;
            this.contactUsService = contactUsService;
            this.recruiterPostedJobsService = recruiterPostedJobsService;
            this.recruiterPostedJobsAndApplicantsService = recruiterPostedJobsAndApplicantsService;
            _ICompanyTypeService = ICompanyTypeService;
            _IEmployeeTypeService = IEmployeeTypeService;
            _IExperienceLevelService = IExperienceLevelService;
            this.mapper = mapper;
            this.config = config;
        }

        [HttpPost]
        [Route("en/home/ajax_validate_token")]
        public async Task<JsonArrResponseDTO> ajax_validate_token([FromBody] ValidateTokenRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }
                }

                JsonArrResponseDTOObj.code = "1";
                JsonArrResponseDTOObj.MSG = "Valid token";
                return JsonArrResponseDTOObj;
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "2";
                JsonArrResponseDTOObj.MSG = "Invalid token";
                return JsonArrResponseDTOObj;
            }
        }
        //Get: api/GetCompanyTypeList
        [HttpGet]
        [Route("en/home/ajax_all_company_type")]
        public async Task<object> GetCompanyTypeList()
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var companyTypes = await _ICompanyTypeService.GetAll();
                var companyTypeList = mapper.Map<IEnumerable<CompanyTypeResponseDTO>>(companyTypes);
                if (companyTypeList.Any())
                {
                    return companyTypeList;
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }
            return "No data found.";
        }


        //Post: api/CompanyType
        [HttpPost]
        [Route("en/home/ajax_add_company_type")]
        public IActionResult AddCompanyType([FromBody] CompanyTypeResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            // map dto to entity
            var companyType = mapper.Map<CompanyType>(model);
            try
            {
                if (!_ICompanyTypeService.IsExists(model.Name))
                {
                    long newCompanyType = _ICompanyTypeService.Add(companyType);
                    Data = null;
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Company type created successfully";
                }
                else
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Company type already exists.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while adding data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        //PUT: api/CompanyType/5
        [HttpPut]
        [Route("en/home/ajax_update_company_type")]
        public IActionResult UpdateCompany([FromBody] long id, [FromBody] CompanyTypeResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            // map dto to entity
            var companyType = mapper.Map<CompanyType>(model);

            try
            {
                companyType.Id = id;
                if (id != model.Id)
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Invalid company type.";
                }
                else
                {
                    long newId = _ICompanyTypeService.Update(companyType);
                    Data = null;
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Company type updated successfully";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while updating data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        //Get: api/CompanyType/5
        [HttpGet]
        [Route("en/home/ajax_get_company_type")]
        public IActionResult GetSalaryRange(GetCompanyTypeRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var result = _ICompanyTypeService.GetById(id);
                var companyType = mapper.Map<CompanyType>(result);
                if (companyType != null)
                {
                    isSuccess = true;
                    StatusCode = 200;
                    Data = companyType;
                    Message = "Success";
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //Get: api/GetExperienceLevelList
        [HttpGet]
        [Route("en/home/ajax_all_experience_level")]
        public async Task<IActionResult> GetExperienceLevelList()
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var experienceLevels = await _IExperienceLevelService.GetAll();
                var experienceList = mapper.Map<IEnumerable<ExperienceLevelResponseDTO>>(experienceLevels);
                if (experienceList.Any())
                {
                    isSuccess = true;
                    StatusCode = 200;
                    Data = experienceList;
                    Message = "Success";
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }
        //Get: api/GetExperienceLevelList
        [HttpPost]
        [Route("en/home/ajax_all_experience_level_admin")]
        public async Task<IActionResult> GetExperienceLevelList([FromBody] ExperienceLevelRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            int offset = model.offset;
            int paging = model.paging == 0 ? Convert.ToInt32(config["GeneralConfig:Paging"]) : model.paging;
            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                response.totalRecords = await _IExperienceLevelService.GetAllCount();
                var experienceLevels = await _IExperienceLevelService.GetAll(offset, paging);
                var experienceList = mapper.Map<IEnumerable<ExperienceLevelResponseDTO>>(experienceLevels);
                if (experienceList.Any())
                {
                    isSuccess = true;
                    StatusCode = 200;
                    Data = experienceList;
                    Message = "Success";
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        //Get: api/GetEmployeeTypeList
        [HttpGet]
        [Route("en/home/ajax_all_employment_type")]
        public async Task<object> GetEmployeeTypeList()
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var employeeTypes = await _IEmployeeTypeService.GetAll(0, 9999);
                var employeeTypeList = mapper.Map<IEnumerable<EmployeeTypeResponseDTO>>(employeeTypes.Item1);
                if (employeeTypeList.Any())
                {
                    return employeeTypeList;
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }
            return "No data found.";
        }


        //Get: api/GetEmployeeTypeList
        [HttpPost]
        [Route("en/home/ajax_all_experience_level_admin")]
        public async Task<object> GetEmployeeTypeListAdmin([FromBody] GetExperienceLevelRequestDTO model)
        {
            int recordsCount = 0;
            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode = 200 };
            response.StatusCode = 200;
            int offset = model.offset;
            int paging = model.paging == 0 ? Convert.ToInt32(config["GeneralConfig:Paging"]) : model.paging;
            try
            {
                var experienceLevels = await _IExperienceLevelService.GetAll(offset, paging);
                recordsCount = experienceLevels.Item2;
                var employeeTypeList = mapper.Map<IEnumerable<ExperienceLevelResponseDTO>>(experienceLevels.Item1);
                if (!employeeTypeList.Any())
                {
                    response.StatusCode = 404;
                    response.Data = null;
                    response.Message = "no record found.";
                }
                else
                {
                    SearchResponseDTO SearchResponseDTOObj = new SearchResponseDTO();
                    SearchResponseDTOObj.Records = employeeTypeList;
                    SearchResponseDTOObj.offset = offset;
                    SearchResponseDTOObj.paging = paging;
                    SearchResponseDTOObj.totalRecords = recordsCount;

                    response.Data = SearchResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = 500;
                response.Data = null;
                response.Message = "Failed while fetching data.";
                response.ExceptionMessage = ex.Message.ToString();
            }
            return ResponseHelper<SearchResponseDTO>.GenerateResponse(response);
        }

        //Get: api/GetEmployeeTypeList
        [HttpPost]
        [Route("en/home/ajax_all_employment_type_admin")]
        public async Task<object> ajax_all_employment_type_admin([FromBody] EmployeeTypeRequestDTO model)
        {
            int recordsCount = 0;
            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode = 200 };
            response.StatusCode = 200;
            int offset = model.offset;
            int paging = model.paging == 0 ? Convert.ToInt32(config["GeneralConfig:Paging"]) : model.paging;
            try
            {
                var employeeTypes = await _IEmployeeTypeService.GetAll(offset, paging);
                recordsCount = employeeTypes.Item2;
                var employeeTypeList = mapper.Map<IEnumerable<EmployeeTypeResponseDTO>>(employeeTypes.Item1);
                if (!employeeTypeList.Any())
                {
                    response.StatusCode = 404;
                    response.Data = null;
                    response.Message = "no record found.";
                }
                else
                {
                    SearchResponseDTO SearchResponseDTOObj = new SearchResponseDTO();
                    SearchResponseDTOObj.Records = employeeTypeList;
                    SearchResponseDTOObj.offset = offset;
                    SearchResponseDTOObj.paging = paging;
                    SearchResponseDTOObj.totalRecords = recordsCount;

                    response.Data = SearchResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = 500;
                response.Data = null;
                response.Message = "Failed while fetching data.";
                response.ExceptionMessage = ex.Message.ToString();
            }
            return ResponseHelper<SearchResponseDTO>.GenerateResponse(response);
        }

        //Post: api/EmployeeType
        [HttpPost]
        [Route("en/home/ajax_add_employment_type")]
        public IActionResult AddEmployeeType([FromBody] EmployeeTypeResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            // map dto to entity
            var employeeType = mapper.Map<EmployeeType>(model);
            try
            {
                if (!_IEmployeeTypeService.IsExists(model.Name))
                {
                    long newEmployeeType = _IEmployeeTypeService.Add(employeeType);
                    Data = null;
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Employee type created successfully";
                }
                else
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Employee type already exists.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while adding data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //PUT: api/EmployeeType/5
        [HttpPost]
        [Route("en/home/ajax_update_employment_type")]
        public IActionResult UpdateEmployeeType([FromBody] EmployeeTypeResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            // map dto to entity
            var employeeType = mapper.Map<EmployeeType>(model);

            try
            {
                if (model.Id <= 0)
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Invalid employee type.";
                }
                else
                {
                    long newId = _IEmployeeTypeService.Update(employeeType);
                    Data = null;
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Employee type updated successfully";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while updating data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //Get: api/EmployeeType/5
        [HttpGet]
        [Route("en/home/ajax_get_employment_type")]
        public IActionResult GetEmployeeType(GetEmployeeTypeRequestDTO model)
        {
            int id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var result = _IEmployeeTypeService.GetById(id);
                var employeeType = mapper.Map<EmployeeTypeResponseDTO>(result);
                if (employeeType != null)
                {
                    isSuccess = true;
                    StatusCode = 200;
                    Data = employeeType;
                    Message = "Success";
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        // DELETE: api/EmployeeType/5
        [HttpDelete]
        [Route("en/home/ajax_delete_employment_type")]
        public IActionResult DeleteEmployeeType(int id)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            try
            {
                _IEmployeeTypeService.Delete(id);

                isSuccess = true;
                StatusCode = 200;
                Message = "Deleted successfully.";
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while deleting data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        // DELETE: api/CompanyType/5
        [HttpDelete]
        [Route("en/home/ajax_delete_company_type")]
        public IActionResult DeleteCompanyType([FromBody] GetEmployeeTypeRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            try
            {
                _ICompanyTypeService.Delete(id);

                isSuccess = true;
                StatusCode = 200;
                Message = "Deleted successfully.";
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while deleting data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        //Post: api/ExperienceLevel
        [HttpPost]
        [Route("en/home/ajax_add_experience_level")]
        public IActionResult AddExperienceLevel([FromBody] ExperienceLevelResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            // map dto to entity
            var experienceLevel = mapper.Map<ExperienceLevel>(model);
            try
            {
                if (!_IExperienceLevelService.IsExists(model.Name))
                {
                    int newSalaryRange = _IExperienceLevelService.Add(experienceLevel);
                    Data = null;
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Experience level created successfully";
                }
                else
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Experience level already exists.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while adding data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //PUT: api/ExperienceLevel/5
        [HttpPost]
        [Route("en/home/ajax_update_experience_level")]
        public IActionResult UpdateExperienceLevel([FromBody] ExperienceLevelResponseDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            var experienceLevel = mapper.Map<ExperienceLevel>(model);

            try
            {
                if (experienceLevel.Id <= 0)
                {
                    Data = null;
                    StatusCode = 400;
                    isSuccess = false;
                    Message = "Invalid experience level range.";
                }
                else
                {
                    Data = _IExperienceLevelService.Update(experienceLevel);
                    StatusCode = 200;
                    isSuccess = true;
                    Message = "Experience level updated successfully";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while updating data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        //Get: api/ExperienceLevel/5
        [HttpGet]
        [Route("en/home/ajax_get_experience_level")]
        public IActionResult GetExperienceLevel([FromBody] GetEmployeeTypeRequestDTO model)
        {
            //int id = model.id;

            int id = 1;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            try
            {
                var result = _IExperienceLevelService.GetById(id);
                var experienceLevelResponse = mapper.Map<ExperienceLevelResponseDTO>(result);
                if (experienceLevelResponse != null)
                {
                    isSuccess = true;
                    StatusCode = 200;
                    Data = experienceLevelResponse;
                    Message = "Success";
                }
                else
                {
                    StatusCode = 404;
                    Message = "No data found.";
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }


            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        // DELETE: api/SalaryRange/5
        [HttpDelete]
        [Route("en/home/ajax_delete_experience_level")]
        public IActionResult DeleteExperienceLevel([FromBody] GetEmployeeTypeRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            try
            {
                //_IExperienceLevelService.Delete(id);

                isSuccess = true;
                StatusCode = 200;
                Message = "Deleted successfully.";
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while deleting data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        [HttpDelete]
        [Route("en/home/ajax_delete_experience_level/{id}")]
        public IActionResult DeleteExperienceLevelById(int id)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";
            try
            {
                _IExperienceLevelService.Delete(id);

                isSuccess = true;
                StatusCode = 200;
                Message = "Deleted successfully.";
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while deleting data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        [HttpPost]
        [Route("en/home/ajax_get_cv")]
        public async Task<JsonArrResponseDTO> ajax_get_cv([FromBody] ValidateTokenRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                        else
                        {
                            string html = "<!doctype html> <html lang='en'> <head> <meta charset='utf-8'> <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'> <title>CV</title> <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css' integrity='sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk' crossorigin='anonymous'> <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css'> <style> @font-face { font-family: 'mouse-300'; src: url('../fonts/RobotoSlab-Regular.ttf') format('truetype'); } @font-face { font-family: 'mouse-500'; src: url('../fonts/RobotoSlab-Bold.ttf') format('truetype'); } * { margin: 0px; padding: 0px; list-style: none; } img { max-width: 100%; } a { text-decoration: none; outline: none; color: #444; } a:hover { color: #444; } ul { margin-bottom: 0; padding-left: 0; } a:hover, a:focus, input, textarea { text-decoration: none; outline: none; } .center { text-align: center; } .left { text-align: left; } .right { text-align: right; } .cp { cursor: pointer; } html, body { height: 100%; } p { margin-bottom: 0px; width: 100%; } .no-padding { padding: 0px; } .no-margin { margin: 0px; } .hid { display: none; } .top-mar { margin-top: 15px; } .h-100 { height: 100%; } ::placeholder { color: #747f8a !important; font-size: 13px; opacity: .5 !important; } .container-fluid { padding: 0px; } h1, h2, h3, h4, h5, h6 { font-family: 'mouse-500', Arial, Helvetica, sans-serif; } strong { font-family: 'mouse-500', Arial, Helvetica, sans-serif; } body { background-color: #f7f7ff !important; font-family: 'mouse-300', Arial, Helvetica, sans-serif; color: #6A6A6A; } .session-title { padding: 30px; margin: 0px; } .session-title h2 { width: 100%; text-align: center; } .session-title p { max-width: 850px; text-align: center; float: none; margin: auto; } .session-title span { float: right; font-style: italic; } .inner-title { padding: 20px; padding-left: 0px; margin-bottom: 30px; } .inner-title h2 { width: 100%; text-align: center; font-size: 2rem; font-family: 'slab', Arial, Helvetica, sans-serif; } .inner-title p { width: 100%; text-align: center; } .page-nav { padding: 40px; text-align: center; padding-top: 160px; } .page-nav ul { float: none; margin: auto; } .page-nav h2 { font-size: 36px; width: 100%; color: #444; } @media screen and (max-width: 600px) { .page-nav h2 { font-size: 26px; } } .page-nav ul li { float: left; margin-right: 10px; margin-top: 10px; font-size: 16px; } .page-nav ul li i { width: 30px; text-align: center; color: #444; } .page-nav ul li a { color: #444; } .btn-success { background-color: #00ab9f; border-color: #00ab9f; } .btn-success:hover { background-color: #00ab9f !important; border-color: #00ab9f !important; } .btn-success:active { background-color: #00ab9f !important; border-color: #00ab9f !important; } .btn-success:focus { background-color: #00ab9f !important; border-color: #00ab9f !important; box-shadow: none !important; } .btn-info { background-color: #4f6dcd; border-color: #4f6dcd; } .btn-info:hover { background-color: #4f6dcd !important; border-color: #4f6dcd !important; } .btn-info:active { background-color: #4f6dcd !important; border-color: #4f6dcd !important; } .btn-info:focus { background-color: #4f6dcd !important; border-color: #4f6dcd !important; box-shadow: none !important; } .btn { box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12); border-radius: 2px; } .form-control:focus { box-shadow: none !important; border: 2px solid  #00a8df; } .btn-light { background-color: #FFF; color: #3F3F3F; } .collapse.show { display: block !important; } .form-control:focus { box-shadow: none; border: 2px solid #0d7a40 !important; } .form-control { background-color: #F8F8F8; margin-bottom: 20px; } .form-control:focus { background-color: #FFF; border-color: #CCC; } .container { max-width: 1100px; } @media screen and (max-width: 1060px) { .container { max-width: 100%; overflow: hidden; } } /* ===================================== Header CSS ================================== */ .profile-box { width: 1050px; margin: 30px auto; box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12); background-color: #FFF; padding-left: 40px; padding-right: 20px; } @media screen and (max-width: 1060px) { .profile-box { margin: 0px; } } @media screen and (max-width: 924px) { .profile-box { padding: 0px; padding-right: 10px; } } .profile-box .left-side { background-color: #00ab9f; color: #FFF; padding: 40px 20px; } .profile-box .left-side .profile-info { padding: 5px; box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12); background-color: #FFF; width: auto; max-width: 200px; margin: auto; margin-bottom: 20px; border-radius: 10px; color: #444; text-align: center; } .profile-box .left-side .profile-info img { width: 100%; } .profile-box .left-side .profile-info h3 { font-size: 1rem; margin-bottom: 0px; padding: 5px; } .profile-box .left-side .profile-info span { font-size: .9rem; } .profile-box .left-side .ltitle { padding: 10px; font-size: 1.1rem; border-bottom: 1px solid #fff; } .profile-box .left-side .contact-box { display: flex; padding: 10px 0px; } .profile-box .left-side .contact-box .icon { padding: 10px; } .profile-box .left-side .contact-box .icon i { padding: 10px; border: 1px solid #FFF; border-radius: 50px; width: 40px; height: 40px; text-align: center; } .profile-box .left-side .contact-box .detail { font-size: .9rem; margin: auto 0; } .profile-box .left-side .pb0 { padding-bottom: 0px; } .profile-box .left-side .social-link li { padding: 10px 15px; } .profile-box .left-side .refer-cov { padding: 8px; } .profile-box .left-side .refer-cov b { font-size: .9rem; } .profile-box .left-side .refer-cov p { font-size: .8rem; } .profile-box .left-side .refer-cov span { font-size: .9rem; } .profile-box .left-side .hoby li { float: left; font-size: .9rem; padding: 15px; text-align: center; width: 33.33%; } .profile-box .left-side .hoby li i { font-size: 22px; margin-bottom: 15px; } @media screen and (max-width: 924px) { .overcover { padding: 8px; } .rt-div { padding-left: 0px; } } @media screen and (max-width: 767px) { .left-co { padding-right: 0px; } .rt-div { padding-left: 30px; } } .rit-cover .hotkey { text-align: right; padding: 30px 20px; } .rit-cover .hotkey h1 { width: 100%; text-align: right; color: #c7c2c2; } .rit-cover .hotkey small { font-size: 1rem; color: #9c9898; margin-top: -30px; font-weight: 600; } .rit-cover .rit-titl { padding: 10px; border-bottom: 2px solid #CCC; font-size: 1.1rem; } .rit-cover .rit-titl i { font-size: 18px; margin-right: 5px; } .rit-cover .about { font-size: .9rem; text-align: justify; text-indent: 15px; padding: 5px 0px; } .rit-cover .about .btn-link li { float: left; margin-top: 15px; padding: 5px 20px; border-radius: 15px; border: 1px solid #888; margin-right: 10px; margin-bottom: 10px; color: #888; } @media screen and (max-width: 483px) { .rit-cover .about .btn-link li { margin-right: 5px !important; width: 100%; margin-bottom: 10px; } } .rit-cover .about .btn-link li i { margin-top: 5px; margin-left: -28px; } .rit-cover .about .btn-link li a { color: #888; } .rit-cover .work-exp { padding: 5px 0px; } .rit-cover .work-exp h6 { font-size: .9rem; margin-bottom: 0px; } .rit-cover .work-exp h6 span { float: right; font-size: .8rem; } .rit-cover .work-exp i { font-size: .9rem; } .rit-cover .work-exp ul { padding: 5px 0px; } .rit-cover .work-exp ul li { font-size: .9rem; padding-left: 5px; } .rit-cover .work-exp ul li i { margin-right: 5px; } .rit-cover .education { padding: 20px 0px; } .rit-cover .education ul li { box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12); border-radius: 2px; background-color: #ecebeb3d; padding: 10px 15px; font-size: .9rem; } .rit-cover .education ul li span { font-size: .8rem; } .rit-cover .profess-cover { padding: 20px 0px; } .rit-cover .profess-cover .prog-row { width: 100%; text-align: left; padding: 8px 2px; font-size: .9rem; } .rit-cover .profess-cover .prog-row div { padding: 0px; } .rit-cover .profess-cover .progress-bar { background-color: #00ab9f; } </style> </head> <body> <div class='container-fluid overcover'> <div class='container profile-box'> <div class='row'> <div class='col-md-4 left-co'> <div class='left-side'> <div class='profile-info'> <img src='#cvimageurl' alt=''> <h3>#name</h3> <span>#jobtitle</span> </div> <h2 class='ltitle text-center' style='font-size: 22px'>Personal Information</h2> <div class='data_user mt-3'> <h6>Date of Birth</h6> <p class='ml-3'>#DOB</p> </div> <div class='data_user'> <h6>Nationality</h6> <p class='ml-3'>#Nationality</p> </div> <div class='data_user'> <h6>Social Status</h6> <p class='ml-3'>#SocialStatus</p> </div> <div class='data_user'> <h6>Visa Status</h6> <p class='ml-3'>#VisaStatus</p> </div> <div class='data_user'> <h6>Driving License</h6> <p class='ml-3'>#DrivingLicense</p> </div> <h4 class='ltitle text-center mt-2 '  style='font-size: 22px'>Specialization</h4> <p class='ml-3'>#specialization</p> <h4 class='ltitle text-center mt-3 '  style='font-size: 22px'>Contact</h4> <div class='contact-box pb0'> <div class='icon'> <i class='fas fa-phone'></i> </div> <div class='detail'> #phone </div> </div> <div class='contact-box pb0'> <div class='icon'> <i class='fas fa fa-envelope'></i> </div> <div class='detail'> #email </div> </div> <div class='contact-box'> <div class='icon'> <i class='fas fa-map-marker-alt'></i> </div> <div class='detail'> #currentlocation </div> </div> </div> </div> <div class='col-md-8 rt-div'> <div class='rit-cover'> <div class='hotkey'> <h1 class=''>#name</h1> <small>#jobtitle</small> </div> <h2 class='rit-titl'><i class='far fa-user'></i> Profile</h2> <div class='about'> <p>#about</p> </div> <h2 class='rit-titl'><i class='fas fa-briefcase'></i> Work Experiance</h2> <div class='work-exp'> <h6>#jobdesignation <span>#jobstart-#jobend</span></h6> <i>#jobaddress</i> <p>#jobdetail</p> </div> <h2 class='rit-titl'><i class='fas fa-graduation-cap'></i> Qualification</h2> <div class='education'> <ul class='row no-margin'> <li class='col-md-6'><span>#edustart-#eduend</span> <br> #institutename</li> </ul> </div> </div> </div> </div> </div> </div> </body> <script src='https://code.jquery.com/jquery-3.5.1.slim.min.js' integrity='sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj' crossorigin='anonymous'></script> <script src='https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js' integrity='sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo' crossorigin='anonymous'></script> <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js' integrity='sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI' crossorigin='anonymous'></script> <script> $( document ).ready(function() { var arr = ['bg_1.jpg','bg_2.jpg','bg_3.jpg']; var i = 0; setInterval(function(){ if(i == arr.length - 1){ i = 0; }else{ i++; } var img = 'url(../assets/images/'+arr[i]+')'; $('.full-bg').css('background-image',img); }, 4000) }); </script> </html>";
                            html.Replace("", "a");
                        }
                    }
                }

                JsonArrResponseDTOObj.code = "1";
                JsonArrResponseDTOObj.MSG = "";
                return JsonArrResponseDTOObj;
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "2";
                JsonArrResponseDTOObj.MSG = "Invalid token";
                return JsonArrResponseDTOObj;
            }
        }

        [HttpPost]
        [Route("en/home/ajax_get_contact_us")]
        public async Task<IActionResult> ajax_get_contact_us([FromBody] ValidateTokenRequestDTO model)
        {
            int recordsCount = 0;
            ResponseDTO<SearchResponseDTO> response = new ResponseDTO<SearchResponseDTO>() { StatusCode = 200 };
            int offset = model.offset;
            int paging = model.paging == 0 ? Convert.ToInt32(config["GeneralConfig:Paging"]) : model.paging;
            Session sessionObj = null;
            try
            {
                // Validate Token

                if (string.IsNullOrWhiteSpace(model.token))
                {
                    response.StatusCode = 500;
                    response.Data = null;
                    response.Message = "Failed while fetching data.";
                }

                sessionObj = await sessionService.GetByToken(model.token);
                if (sessionObj == null)
                {
                    response.StatusCode = 500;
                    response.Data = null;
                    response.Message = "Failed while fetching data.";
                }
                else
                {

                    var contactList = contactUsService.GetAll().ToList();
                    recordsCount = contactList.Count;
                    contactList = contactList.Skip(model.offset * model.paging).Take(model.paging).ToList();
                    if (!contactList.Any())
                    {
                        response.StatusCode = 404;
                        response.Data = null;
                        response.Message = "no record found.";
                    }
                    else
                    {
                        SearchResponseDTO SearchResponseDTOObj = new SearchResponseDTO();
                        SearchResponseDTOObj.Records = contactList;
                        SearchResponseDTOObj.offset = offset;
                        SearchResponseDTOObj.paging = paging;
                        SearchResponseDTOObj.totalRecords = recordsCount;
                        response.Data = SearchResponseDTOObj;
                    }
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = 500;
                response.Data = null;
                response.Message = "Failed while fetching data.";
                response.ExceptionMessage = ex.Message.ToString();
            }
            return ResponseHelper<SearchResponseDTO>.GenerateResponse(response);
        }
    }
}