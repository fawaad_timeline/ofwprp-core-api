﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models
{
    public class JobSeekerMessage
    {
        public long JobSeekerId { get; set; }
        public long RecruiterId { get; set; }
        public string CompanyName { get; set; }
        public long TotalMessages { get; set; }
        public long TotalUnreadMessages { get; set; }
        public bool Checked { get; set; }
        public string CompanyLogo { get; set; }
        public string JobSeekerProfile { get; set; }
    }
}
