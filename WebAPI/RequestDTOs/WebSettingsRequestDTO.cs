﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class WebSettingsRequestDTO
    {
        public int Id { get; set; }
        public string AboutHtml { get; set; }
        public string PrivacyPolicyHtml { get; set; }
        public string TermsAndConditionsHtml { get; set; }
    }
}
