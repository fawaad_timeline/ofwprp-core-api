﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models
{
    public class JobApplications
    {
        public string JobApplicationId { get; set; }
        public string PostedJobId { get; set; }
        public long JobSeekerId { get; set; }
        public long RecruiterId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public long ExperienceLevel { get; set; }
        public long EmploymentType { get; set; }
        public long CompanyType { get; set; }
        public string SalaryRange { get; set; }
        public string CompanyName { get; set; }
        public long CountryId { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public DateTime AppliedDate { get; set; }
        public string CompanyLogoURL { get; set; }
        public string ProfilePictureURL { get; set; }

        public bool Checked { get; set; }

        public string EmploymentTypeName { get; set; }
        public string CompanyTypeName { get; set; }
        public string ExperienceLevelName { get; set; }
    }
}
