﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class RecruiterPostedJobs
    {
        protected long recruiterPostedJobsId, recruiterId, countryId, salaryRangeId, employmentType, companyType, experienceLevel;
        protected string referenceNo, title, description, city, rolesJobId, rolesJobOther, has_applied_, createdBy, updatedBy;
        protected DateTime createdOn, updatedOn;
        protected bool isActive, isDeleted;

        protected long requiredNumber;
        protected bool isTrainingAvailable;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long RecruiterPostedJobsId { get => recruiterPostedJobsId; set => recruiterPostedJobsId = value; }
        public long RecruiterId { get => recruiterId; set => recruiterId = value; }
        public string ReferenceNo { get => referenceNo; set => referenceNo = value; }
        public string Title { get => title; set => title = value; }
        public string Description { get => description; set => description = value; }
        public long CountryId { get => countryId; set => countryId = value; }
        public string City { get => city; set => city = value; }
        public long EmploymentType { get => employmentType; set => employmentType = value; }
        public long CompanyType { get => companyType; set => companyType = value; }
        public long SalaryRangeId { get => salaryRangeId; set => salaryRangeId = value; }
        public long ExperienceLevel { get => experienceLevel; set => experienceLevel = value; }
        public string RolesJobId { get => rolesJobId; set => rolesJobId = value; }
        public string RolesJobOther { get => rolesJobOther; set => rolesJobOther = value; }
        public bool IsActive { get => isActive; set => isActive = value; }


        public long RequiredNumber { get => requiredNumber; set => requiredNumber = value; }
        public bool IsTrainingAvailable { get => isTrainingAvailable; set => isTrainingAvailable = value; }


        public string has_applied { get => has_applied_; set => has_applied_ = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }
    }
}
