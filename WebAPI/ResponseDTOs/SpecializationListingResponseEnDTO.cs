﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class SpecializationListingResponseEnDTO
    {
        public long id { get; set; }
        public string name { get; set; }
    }
}
