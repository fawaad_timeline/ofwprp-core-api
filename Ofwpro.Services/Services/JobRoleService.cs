﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class JobRoleService : IJobRoleService
    {
		private readonly IJobRoleRepository _IJobRoleRepository;
		public JobRoleService(IJobRoleRepository iJobRoleRepository)
		{
			_IJobRoleRepository = iJobRoleRepository;
		}

        public long Add(JobRoles jobRole)
        {
            return _IJobRoleRepository.Add(jobRole);
        }

        public void Delete(long id)
        {
            _IJobRoleRepository.Delete(id);
        }

        public async Task<Tuple<IEnumerable<JobRoles>, int>> GetAll(int offset, int paging)
		{
			return await _IJobRoleRepository.GetAll(offset, paging);
		}

		public JobRoles GetById(long id)
		{
			return _IJobRoleRepository.GetById(id);
		}

        public bool IsExists(string name)
        {
            return _IJobRoleRepository.IsExists(name);
        }

        public long Update(JobRoles jobRole)
        {
            return _IJobRoleRepository.Update(jobRole);
        }
    }
}
