﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface IEmployeeTypeService
    {
        Task<Tuple<IEnumerable<EmployeeType>, int>> GetAll(int offset,int paging);
        EmployeeType GetById(int id);
        int Add(EmployeeType employeeType);
        int Update(EmployeeType employeeType);
        void Delete(int id);
        bool IsExists(string name);
    }
}
