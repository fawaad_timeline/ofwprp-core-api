﻿using System;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IUnitOfWork:IDisposable
    {
        ApplicationDbContext Context { get;}
        void Commit();
    }
}
