using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using Ofwpro.Core;
using Ofwpro.Infrastructure;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.Repository;
using Ofwpro.Services.Interfaces;
using Ofwpro.Services.Services;
using WebAPI.Helpers;
using WebAPI.Mappings;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IUserTwoFactorTokenProvider<ApplicationUser>, DataProtectorTokenProvider<ApplicationUser>>();
            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 8;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                //Lockout Settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(Convert.ToInt64(Configuration["AccountLockOutInfo:LockoutTimeSpan"]));
                options.Lockout.MaxFailedAccessAttempts = Convert.ToInt32(Configuration["AccountLockOutInfo:MaxFailedAccessAttempts"]);
                options.Lockout.AllowedForNewUsers = false;
                //User Settings
                options.User.RequireUniqueEmail = false;
                //Default Signin Settings
                options.SignIn.RequireConfirmedEmail = false;
                options.SignIn.RequireConfirmedPhoneNumber = false;
                options.Tokens.ProviderMap.Add("Default", new TokenProviderDescriptor(typeof(IUserTwoFactorTokenProvider<ApplicationUser>)));
            })
            .AddEntityFrameworkStores<ApplicationDbContext>();
            services.Configure<DataProtectionTokenProviderOptions>(o =>
            {
                o.Name = "Default";
                o.TokenLifespan = TimeSpan.FromHours(1);
            });

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(1);
            });

            services.AddDbContext<ApplicationDbContext>(opts => opts.UseSqlServer(Configuration["ConnectionString:OfwproDb"]));

            #region Repository Scope
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAdministratorRepository, AdministratorRepository>();
            services.AddScoped<IFileUploadRepository, FileUploadRepository>();
            services.AddScoped<ISpecializationRepository, SpecializationRepository>();
            services.AddScoped<IJobSeekerRepository, JobSeekerRepository>();
            services.AddScoped<ISessionRepository, SessionRepository>();
            services.AddScoped<IRecruiterRepository, RecruiterRepository>();
            services.AddScoped<IRecruiterCompanyRepository, RecruiterCompanyRepository>();
            services.AddScoped<IJobSeekerPersonalInformationRepository, JobSeekerPersonalInformationRepository>();
            services.AddScoped<IJobSeekerQualificationRepository, JobSeekerQualificationRepository>();
            services.AddScoped<IJobSeekerWorkExperienceRepository, JobSeekerWorkExperienceRepository>();
            services.AddScoped<IRecruiterPostedJobsRepository, RecruiterPostedJobsRepository>();
            services.AddScoped<IJobSeekerAttachmentRepository, JobSeekerAttachmentRepository>();
            services.AddScoped<IRecruiterPostedJobsAndApplicantsRepository, RecruiterPostedJobsAndApplicantsRepository>();
            services.AddScoped<IRecruiterAttachmentRepository, RecruiterAttachmentRepository>();
            services.AddScoped<ICountryDetailRepository, CountryDetailRepository>();
            services.AddScoped<ISalaryRangeRepository, SalaryRangeRepository>();
            services.AddScoped<ICompanyTypeRepository, CompanyTypeRepository>();
            services.AddScoped<IEmployeeTypeRepository, EmployeeTypeRepository>();
            services.AddScoped<IMaritalStatusRepository, MaritalStatusRepository>();
            services.AddScoped<IEducationLevelRepository, EducationLevelRepository>();
            services.AddScoped<IJobRoleRepository, JobRoleRepository>();
            services.AddScoped<IExperienceLevelRepository, ExperienceLevelRepository>();
            services.AddScoped<IForgotPasswordRepository, ForgotPasswordRepository>();
            services.AddScoped<IMessageGeneralRepository, MessageGeneralRepository>();
            services.AddScoped<IMessageAgainstJobRepository, MessageAgainstJobRepository>();
            services.AddScoped<IFaqRepository, FaqRepository>();
            services.AddScoped<ITestimonialRepository, TestimonialRepository>();
            services.AddScoped<IPagesRepository, PagesRepository>();
            services.AddScoped<IWebSettingsRepository, WebSettingsRepository>();
            services.AddScoped<INewsRepository, NewsRepository>();
            services.AddScoped<IContactUsRepository, ContactUsRepository>();
            #endregion

            #region Service Scope
            services.AddScoped<IUserResolverService, UserResolverService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAdministratorService, AdministratorService>();
            services.AddScoped<IFileUploadService, FileUploadService>();
            services.AddScoped<ISpecializationService, SpecializationService>();
            services.AddScoped<IJobSeekerService, JobSeekerService>();
            services.AddScoped<ISessionService, SessionService>();
            services.AddScoped<IRecruiterService, RecruiterService>();
            services.AddScoped<IRecruiterCompanyService, RecruiterCompanyService>();
            services.AddScoped<IJobSeekerPersonalInformationService, JobSeekerPersonalInformationService>();
            services.AddScoped<IJobSeekerQualificationService, JobSeekerQualificationService>();
            services.AddScoped<IJobSeekerWorkExperienceService, JobSeekerWorkExperienceService>();
            services.AddScoped<IRecruiterPostedJobsService, RecruiterPostedJobsService>();
            services.AddScoped<IJobSeekerAttachmentService, JobSeekerAttachmentService>();
            services.AddScoped<IRecruiterPostedJobsAndApplicantsService, RecruiterPostedJobsAndApplicantsService>();
            services.AddScoped<IRecruiterAttachmentService, RecruiterAttachmentService>();
            services.AddScoped<ICountryDetailsService, CountryDetailsService>();
            services.AddScoped<ISalaryRangeService, SalaryRangeService>();
            services.AddScoped<ICompanyTypeService, CompanyTypeService>();
            services.AddScoped<IEmployeeTypeService, EmployeeTypeService>();
            services.AddScoped<IMaritalStatusService, MaritalStatusService>();
            services.AddScoped<IEducationLevelService, EducationLevelService>();
            services.AddScoped<IJobRoleService, JobRoleService>();
            services.AddScoped<IExperienceLevelService, ExperienceLevelService>();
            services.AddScoped<IForgotPasswordService, ForgotPasswordService>();
            services.AddScoped<IMessageGeneralService, MessageGeneralService>();
            services.AddScoped<IMessageAgainstJobService, MessageAgainstJobService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<IFaqService, FaqService>();
            services.AddScoped<ITestimonialService, TestimonialService>();
            services.AddScoped<IPagesService, PagesService>();
            services.AddScoped<IWebSettingsService, WebSettingsService>();
            services.AddScoped<INewsService, NewsService>();
            services.AddScoped<IContactUsService, ContactUsService>();
            #endregion


            //Automapper
            //Registering and Initializing AutoMapper
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });

            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            #region CORS
            //services.AddCors(options =>
            //{
            //    string corsUrls = Configuration["CORSUrl:Url"];
            //    string[] Orgins = corsUrls.Split(",");

            //    options.AddPolicy("CorsPolicy", p => p.WithOrigins(Orgins)
            //            .AllowAnyMethod()
            //            .AllowAnyHeader()
            //            .AllowCredentials());
            //});

            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });

            #endregion
            services.AddMemoryCache();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddMvc(options => options.EnableEndpointRouting = false );

            //Extend Model Validation with Response Helper
            services.AddMvc().ConfigureApiBehaviorOptions(options =>
            {
                //InvalidModelStateResponseFactory is a Func delegate  
                // and used to customize the error response.    
                //It is exposed as property of ApiBehaviorOptions class  
                // that is used to configure api behaviour.    
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    List<string> Errors = actionContext.ModelState.Where(modelError => modelError.Value.Errors.Count > 0).Select(modelError => modelError.Value.Errors.FirstOrDefault().ErrorMessage).ToList();

                    ObjectResult objectResult = new ObjectResult(new ModelStateErrorHelper(Errors).ErrorResponse);
                    objectResult.StatusCode = StatusCodes.Status400BadRequest;

                    return objectResult;
                };
            });

            //Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "OfwPro Software Architecture",
                    Description = "OfwPro Web API",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "OfwPro Software",
                        Email = string.Empty,
                        Url = new Uri("https://timeline.ae"),
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under LICX",
                        Url = new Uri("https://example.com/license"),
                    }
                });



                c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                    }
                });
                c.OperationFilter<SecurityRequirementsOperationFilter>();

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseCors("AllowOrigin");
            app.UseHttpsRedirection();
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                //c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                string swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "v1");
                c.RoutePrefix = string.Empty;
            });
            app.UseSession();
            app.UseStaticFiles();
            app.UseRouting();
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllers()
            //         .RequireCors("AllowOrigin");
            //    endpoints.MapControllers();
            //});
            app.UseMvc();

            //app.UseHttpsRedirection();
            //app.UseStaticFiles();
            //app.UseCookiePolicy();
           
            //app.UseAuthentication();
            //app.UseAuthorization();

            
        }
    }
}
