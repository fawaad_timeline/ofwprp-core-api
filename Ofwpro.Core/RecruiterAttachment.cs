﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class RecruiterAttachment
    {
        protected long recruiterAttachmentId, recruiterId;
        protected string documentType, fileNameOriginal, fileNameUpdated, fileType, fileSize, createdBy, updatedBy;
        protected DateTime createdOn, updatedOn;
        protected bool isActive, isDeleted;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long RecruiterAttachmentId { get => recruiterAttachmentId; set => recruiterAttachmentId = value; }
        public long RecruiterId { get => recruiterId; set => recruiterId = value; }
        public string DocumentType { get => documentType; set => documentType = value; }
        public string FileNameOriginal { get => fileNameOriginal; set => fileNameOriginal = value; }
        public string FileNameUpdated { get => fileNameUpdated; set => fileNameUpdated = value; }
        public string FileType { get => fileType; set => fileType = value; }
        public string FileSize { get => fileSize; set => fileSize = value; }

        public bool IsActive { get => isActive; set => isActive = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string UploadedFrom { get; set; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }
    }
}
