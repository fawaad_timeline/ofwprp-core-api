﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Ofwpro.Core;
using Ofwpro.Services.Interfaces;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class FaqController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IUserService userService;
        private readonly IFileUploadService fileUploadService;
        private readonly IUserResolverService userResolverService;
        private readonly IFaqService faqService;
        private readonly IAdministratorService administratorService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public FaqController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IUserService userService, IFileUploadService fileUploadService, IUserResolverService userResolverService, IFaqService faqService, IAdministratorService administratorService, IMapper mapper, IConfiguration config)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.userService = userService;
            this.fileUploadService = fileUploadService;
            this.userResolverService = userResolverService;
            this.faqService = faqService;
            this.administratorService = administratorService;
            this.mapper = mapper;
            this.config = config;
        }

        [HttpGet]
        [Route("en/faq/AllFaq")]
        public async Task<object> All()
        {
            Faq FaqObj = new Faq();
            var faqList = await faqService.All(0, 9999, "CreatedOn", "ASC", "", FaqObj);
            var FaqList = mapper.Map<IEnumerable<FaqResponseDTO>>(faqList);
            if (FaqList.Any())
            {
                return FaqList;
            }
            else
            {
            }
            return "No data found.";
        }

        [HttpGet("en/faq/GetFaq")]
        public async Task<IActionResult> GetFaq(GetRequestDTO model)
        {
            long id = model.Id;
            ResponseDTO<object> response = new ResponseDTO<object>();

            int StatusCode = 0;
            bool isSuccess = false;
            object Data = null;
            string Message = "";
            string ExceptionMessage = "";

            isSuccess = true;
            StatusCode = 200;
            Message = "Success";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.FaqManagement.ToString());
                bool hasRight = true;

                // Check rights
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Get details
                if (isSuccess == true)
                {
                    var faqObj = await faqService.GetById(id);
                    long sessionId = id;

                    if (faqObj != null)
                    {
                        FaqResponseDTO FaqResponseDTOObj = mapper.Map<FaqResponseDTO>(faqObj);
                        response.Data = FaqResponseDTOObj;
                    }
                    else
                    {
                        StatusCode = 404;
                        isSuccess = false;
                        Message = "No data found.";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while fetching data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        [HttpPost("en/faq/SaveFaq")]
        public async Task<IActionResult> SaveFaq([FromBody] FaqRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            Faq faqObjOld = null;
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been saved successfully";
            string ExceptionMessage = "";

            string lan = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.FaqManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                if (isSuccess == true && model.Id > 0)
                {
                    faqObjOld = await faqService.GetById(model.Id);
                    if (faqObjOld == null)
                    {
                        StatusCode = 404;
                        isSuccess = false;
                        Message = "No data found";
                        ExceptionMessage = "";
                    }
                }

                if (isSuccess == true)
                {
                    // Validation
                    if (string.IsNullOrEmpty(model.Question))
                    {
                        StatusCode = 400;
                        isSuccess = false;
                        Message = "Please provide question.";
                        ExceptionMessage = "";
                    }
                }

                // Check duplication
                if (isSuccess == true)
                {
                    isExist = faqService.IsExist(model.Question, model.Id);

                    if (isExist)
                    {
                        StatusCode = 409;
                        isSuccess = false;
                        Message = "Question already exists.";
                        ExceptionMessage = "";
                    }
                }

                // Create/Edit(Save)
                if (isSuccess == true)
                {
                    Faq faqObj = mapper.Map<Faq>(model);
                    if (model.Id > 0)
                    {
                        if (faqObjOld != null)
                        {
                            faqObj.CreatedOn = faqObjOld.CreatedOn;
                            faqObj.CreatedBy = faqObjOld.CreatedBy;
                            faqObj.IsDeleted = faqObjOld.IsDeleted;
                            faqObj.UpdatedBy = currentUser.Id;
                        }
                    }
                    else
                    {
                        faqObj.CreatedOn = DateTime.Now;
                        faqObj.IsDeleted = false;
                        if (string.IsNullOrEmpty(faqObj.CreatedBy))
                        {
                            if (currentUser != null)
                            {
                                faqObj.CreatedBy = currentUser.Id;
                            }
                        }
                    }

                    // Save faq                   
                    long sessionId = faqService.Save(faqObj);

                    if (sessionId < 0)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be saved.";
                        ExceptionMessage = "";
                    }
                    else
                    {
                        // Retrieve faq
                        faqObj = await faqService.GetById(sessionId);
                        FaqResponseDTO FaqResponseDTOObj = mapper.Map<FaqResponseDTO>(faqObj);
                        Data = FaqResponseDTOObj;
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Failed while saving data.";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }

        [HttpPut("en/faq/ActivateFaq")]
        public async Task<IActionResult> ActivateFaq(ActivateRequestDTO model)
        {
            long id = model.id; bool active = model.active;
            ResponseDTO<object> response = new ResponseDTO<object>();
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been updated successfully";
            string ExceptionMessage = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.FaqManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                Faq faqObj = await faqService.GetById(id);
                if (faqObj == null)
                {
                    StatusCode = 404;
                    isSuccess = false;
                    Message = "No data found";
                    ExceptionMessage = "";
                }

                // Activate/Deactivate
                if (isSuccess == true)
                {
                    bool result = faqService.Activate(id, active);
                    if (result == false)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be updated";
                        ExceptionMessage = "";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Information could not be updated";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }


        [HttpDelete("en/faq/DeleteFaq")]
        public async Task<IActionResult> DeleteFaq(DeleteRequestDTO model)
        {
            long id = model.id;
            ResponseDTO<object> response = new ResponseDTO<object>();
            bool isExist = false;

            int StatusCode = 200;
            bool isSuccess = true;
            object Data = null;
            string Message = "Information has been deleted successfully";
            string ExceptionMessage = "";

            try
            {
                ApplicationUser currentUser = await userResolverService.GetCurrentUser();

                //var hasRight = await userRightService.ValidateUserRight(currentUser.Id, Helpers.UserRightEnum.FaqManagement.ToString());
                bool hasRight = true;

                // Check righs
                if (!hasRight)
                {
                    StatusCode = 403;
                    isSuccess = false;
                    Message = "Forbidden! You are not authorized to perform this task.";
                }

                // Check existance
                Faq faqObj = await faqService.GetById(id);
                if (faqObj == null)
                {
                    StatusCode = 404;
                    isSuccess = false;
                    Message = "No data found";
                    ExceptionMessage = "";
                }

                // Delete
                if (isSuccess == true)
                {
                    bool result = faqService.Delete(id);
                    if (result == false)
                    {
                        StatusCode = 500;
                        isSuccess = false;
                        Message = "Information could not be deleted";
                        ExceptionMessage = "";
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = 500;
                Message = "Information could not be deleted";
                ExceptionMessage = ex.Message.ToString();
            }

            response.StatusCode = StatusCode;
            response.IsSuccess = isSuccess;
            response.Data = Data;
            response.Message = Message;
            response.ExceptionMessage = ExceptionMessage;

            return ResponseHelper<object>.GenerateResponse(response);
        }
    }
}