﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTOs;
using Ofwpro.Core;
using Ofwpro.Services.DTOs;

namespace WebAPI.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Administrator, AdministratorResponseDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.AdministratorId))
           .ReverseMap();

            CreateMap<Administrator, AdministratorRequestDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.AdministratorId))
           .ReverseMap();

            CreateMap<Administrator, AdminUserRequestDTO>()
            .ForMember(dest => dest.Id, source => source.MapFrom(src => src.AdministratorId))
            .ReverseMap();


            CreateMap<FileUpload, FileUploadRequestDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.FileId))
           .ReverseMap();

            // Specialization
            CreateMap<Specialization, SpecializationRequestDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.SpecializationId))
           .ReverseMap();

            
            CreateMap<JobSeekerAttachment, JobSeekerAttachment>()
           .ReverseMap();

            CreateMap<Specialization, SpecializationListingResponseEnDTO>()
           .ForMember(dest => dest.id, source => source.MapFrom(src => src.SpecializationId))
           .ForMember(dest => dest.name, source => source.MapFrom(src => src.SpecializationNameEn))
           .ReverseMap();

            CreateMap<Specialization, SpecializationListingResponseArDTO>()
           .ForMember(dest => dest.id, source => source.MapFrom(src => src.SpecializationId))
           .ForMember(dest => dest.name, source => source.MapFrom(src => src.SpecializationNameAr))
           .ReverseMap();


            // JobSeeker
            CreateMap<JobSeeker, JobSeekerRequestDTO>()
            .ForMember(dest => dest.title, source => source.MapFrom(src => src.Title))
            .ForMember(dest => dest.first_name, source => source.MapFrom(src => src.FirstName))
            .ForMember(dest => dest.last_name, source => source.MapFrom(src => src.LastName))
            .ForMember(dest => dest.gender, source => source.MapFrom(src => src.Gender))
            .ForMember(dest => dest.country_no, source => source.MapFrom(src => src.CountryNo))
            .ForMember(dest => dest.country_code, source => source.MapFrom(src => src.CountryCode))
            .ForMember(dest => dest.mobile, source => source.MapFrom(src => src.Mobile))
            .ForMember(dest => dest.email, source => source.MapFrom(src => src.Email))
            .ForMember(dest => dest.password, source => source.MapFrom(src => src.Password))
            .ReverseMap();

            CreateMap<JobSeeker, JobSeekerSaveRequestDTO>()
           .ForMember(dest => dest.title, source => source.MapFrom(src => src.Title))
           .ForMember(dest => dest.first_name, source => source.MapFrom(src => src.FirstName))
           .ForMember(dest => dest.last_name, source => source.MapFrom(src => src.LastName))
           .ForMember(dest => dest.gender, source => source.MapFrom(src => src.Gender))
           .ForMember(dest => dest.country_no, source => source.MapFrom(src => src.CountryNo))
           .ForMember(dest => dest.country_code, source => source.MapFrom(src => src.CountryCode))
           .ForMember(dest => dest.mobile, source => source.MapFrom(src => src.Mobile))
           .ForMember(dest => dest.password, source => source.MapFrom(src => src.Password))
           .ReverseMap();

            CreateMap<JobSeeker, JobSeekerResponseDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.JobSeekerId))
           .ReverseMap();

            CreateMap<JobSeeker, JobSeekerListingResponseDTO>()
            .ForMember(dest => dest.job_seeker_id_enc, source => source.MapFrom(src => src.JobSeekerId))
            .ForMember(dest => dest.title, source => source.MapFrom(src => src.Title))
            .ForMember(dest => dest.first_name, source => source.MapFrom(src => src.FirstName))
            .ForMember(dest => dest.last_name, source => source.MapFrom(src => src.LastName))
            .ForMember(dest => dest.dob, source => source.MapFrom(src => src.Dob))
            .ForMember(dest => dest.gender, source => source.MapFrom(src => src.Gender))
            .ForMember(dest => dest.country_no, source => source.MapFrom(src => src.CountryNo))
            .ForMember(dest => dest.country_code, source => source.MapFrom(src => src.CountryCode))
            .ForMember(dest => dest.email, source => source.MapFrom(src => src.Email))
            .ReverseMap();

            CreateMap<JobSeeker, JobSeekerListingAdminResponseDTO>()
            .ForMember(dest => dest.job_seeker_id_enc, source => source.MapFrom(src => src.JobSeekerId))
            .ForMember(dest => dest.title, source => source.MapFrom(src => src.Title))
            .ForMember(dest => dest.first_name, source => source.MapFrom(src => src.FirstName))
            .ForMember(dest => dest.last_name, source => source.MapFrom(src => src.LastName))
            .ForMember(dest => dest.dob, source => source.MapFrom(src => src.Dob))
            .ForMember(dest => dest.gender, source => source.MapFrom(src => src.Gender))
            .ForMember(dest => dest.country_no, source => source.MapFrom(src => src.CountryNo))
            .ForMember(dest => dest.country_code, source => source.MapFrom(src => src.CountryCode))
            .ForMember(dest => dest.email, source => source.MapFrom(src => src.Email))
            .ForMember(dest => dest.isActive, source => source.MapFrom(src => src.IsActive))
            .ForMember(dest => dest.isPictureApproved, source => source.MapFrom(src => src.IsPictureApproved))
            .ForMember(dest => dest.isVideoApproved, source => source.MapFrom(src => src.IsVideoApproved))
            .ReverseMap();

            CreateMap<JobSeeker, JobSeekerSubDetailsResponseDTO>()
           .ForMember(dest => dest.job_seeker_id_enc, source => source.MapFrom(src => src.JobSeekerId))
           .ForMember(dest => dest.title, source => source.MapFrom(src => src.Title))
           .ForMember(dest => dest.first_name, source => source.MapFrom(src => src.FirstName))
           .ForMember(dest => dest.last_name, source => source.MapFrom(src => src.LastName))
           .ForMember(dest => dest.dob, source => source.MapFrom(src => src.Dob))
           .ForMember(dest => dest.gender, source => source.MapFrom(src => src.Gender))
           .ForMember(dest => dest.country_no, source => source.MapFrom(src => src.CountryNo))
           .ForMember(dest => dest.country_code, source => source.MapFrom(src => src.CountryCode))
           .ForMember(dest => dest.email, source => source.MapFrom(src => src.Email))
           .ReverseMap();

            // Recruiter
            CreateMap<Recruiter, RecruiterRequestDTO>()
            .ForMember(dest => dest.first_name, source => source.MapFrom(src => src.FirstName))
            .ForMember(dest => dest.last_name, source => source.MapFrom(src => src.LastName))
            .ForMember(dest => dest.gender, source => source.MapFrom(src => src.Gender))
            .ForMember(dest => dest.country_no, source => source.MapFrom(src => src.CountryNo))
            .ForMember(dest => dest.country_code, source => source.MapFrom(src => src.CountryCode))
            .ForMember(dest => dest.company_name, source => source.MapFrom(src => src.CompanyName))
            .ForMember(dest => dest.current_designation, source => source.MapFrom(src => src.CurrentDesignation))
            .ForMember(dest => dest.mobile, source => source.MapFrom(src => src.Mobile))
            .ForMember(dest => dest.email, source => source.MapFrom(src => src.Email))
            .ForMember(dest => dest.password, source => source.MapFrom(src => src.Password))
            .ReverseMap();

            CreateMap<Recruiter, RecruiterSaveRequestDTO>()
           .ForMember(dest => dest.first_name, source => source.MapFrom(src => src.FirstName))
           .ForMember(dest => dest.last_name, source => source.MapFrom(src => src.LastName))
           .ForMember(dest => dest.gender, source => source.MapFrom(src => src.Gender))
           .ForMember(dest => dest.mobile, source => source.MapFrom(src => src.Mobile))
           .ForMember(dest => dest.country_no, source => source.MapFrom(src => src.CountryNo))
           .ForMember(dest => dest.country_code, source => source.MapFrom(src => src.CountryCode))
           .ForMember(dest => dest.company_name, source => source.MapFrom(src => src.CompanyName))
           .ForMember(dest => dest.current_designation, source => source.MapFrom(src => src.CurrentDesignation))
           .ForMember(dest => dest.password, source => source.MapFrom(src => src.Password))
           .ReverseMap();

            CreateMap<Recruiter, RecruiterResponseDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.RecruiterId))
           .ReverseMap();

            CreateMap<Recruiter, RecruiterListingResponseDTO>()
           .ForMember(dest => dest.RecruiterId, source => source.MapFrom(src => src.RecruiterId))
           .ReverseMap();

            //Recruiter Company
            CreateMap<RecruiterCompany, RecruiterCompanySaveRequestDTO>()
           .ForMember(dest => dest.no_of_employees, source => source.MapFrom(src => src.NoOfEmployees))
           .ForMember(dest => dest.about_company, source => source.MapFrom(src => src.AboutCompany))
           .ForMember(dest => dest.website_url, source => source.MapFrom(src => src.WebsiteUrl))
           .ReverseMap();

            //Personal Information
            CreateMap<JobSeekerPersonalInformation, JobSeekerSavePersonalInformationRequestDTO>()
           .ForMember(dest => dest.country_id_enc, source => source.MapFrom(src => src.CountryId))
           .ForMember(dest => dest.marital_status_id_enc, source => source.MapFrom(src => src.MaritalStatusId))
           .ForMember(dest => dest.home_phone, source => source.MapFrom(src => src.HomePhone))
           .ForMember(dest => dest.mobileCode, source => source.MapFrom(src => src.MobileCode))
           .ForMember(dest => dest.is_driving_license, source => source.MapFrom(src => src.IsDrivingLicense))
           .ForMember(dest => dest.current_location, source => source.MapFrom(src => src.CurrentLocation))
           .ForMember(dest => dest.visa_status, source => source.MapFrom(src => src.VisaStatus))
           .ReverseMap();

            //Personal Information
            CreateMap<JobSeekerPersonalInformation, JobSeekerSavePersonalInformationAdminRequestDTO>()
            .ForMember(dest => dest.Id, source => source.MapFrom(src => src.JobSeekerPersonalInformationId))
           .ForMember(dest => dest.country_id_enc, source => source.MapFrom(src => src.CountryId))
           .ForMember(dest => dest.marital_status_id_enc, source => source.MapFrom(src => src.MaritalStatusId))
           .ForMember(dest => dest.home_phone, source => source.MapFrom(src => src.HomePhone))
           .ForMember(dest => dest.mobileCode, source => source.MapFrom(src => src.MobileCode))
           .ForMember(dest => dest.is_driving_license, source => source.MapFrom(src => src.IsDrivingLicense))
           .ForMember(dest => dest.current_location, source => source.MapFrom(src => src.CurrentLocation))
           .ForMember(dest => dest.visa_status, source => source.MapFrom(src => src.VisaStatus))
           .ReverseMap();

            //Qualification
            CreateMap<JobSeekerQualification, JobSeekerSaveQualificationRequestDTO>()
           .ForMember(dest => dest.education_level_id_enc, source => source.MapFrom(src => src.EducationLevelId))
           .ForMember(dest => dest.specialization_id_enc, source => source.MapFrom(src => src.SpecializationId))
           .ForMember(dest => dest.school_university, source => source.MapFrom(src => src.SchoolUniversity))
           .ForMember(dest => dest.graduation_date, source => source.MapFrom(src => src.GraduationDate))
           .ForMember(dest => dest.name_of_degree, source => source.MapFrom(src => src.NameOfDegree))
           .ReverseMap();

            CreateMap<JobSeekerQualification, JobSeekerSaveQualificationAdminRequestDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.EducationLevelId))
           .ForMember(dest => dest.education_level_id_enc, source => source.MapFrom(src => src.EducationLevelId))
           .ForMember(dest => dest.specialization_id_enc, source => source.MapFrom(src => src.SpecializationId))
           .ForMember(dest => dest.school_university, source => source.MapFrom(src => src.SchoolUniversity))
           .ForMember(dest => dest.graduation_date, source => source.MapFrom(src => src.GraduationDate))
           .ForMember(dest => dest.name_of_degree, source => source.MapFrom(src => src.NameOfDegree))
           .ReverseMap();

            //Work Experience
            CreateMap<JobSeekerWorkExperience, JobSeekerSaveWorkExperienceRequestDTO>()
           .ForMember(dest => dest.experience_level, source => source.MapFrom(src => src.ExperienceLevel))
           .ForMember(dest => dest.company1, source => source.MapFrom(src => src.Company1))
           .ForMember(dest => dest.job_title1, source => source.MapFrom(src => src.JobTitle1))
           .ForMember(dest => dest.country_id_enc1, source => source.MapFrom(src => src.CountryId1))
           .ForMember(dest => dest.city1, source => source.MapFrom(src => src.City1))
           .ForMember(dest => dest.time_period_from1, source => source.MapFrom(src => src.TimePeriodFrom1))
           .ForMember(dest => dest.time_period_to1, source => source.MapFrom(src => src.TimePeriodTo1))
           .ForMember(dest => dest.reason_for_leaving1, source => source.MapFrom(src => src.ReasonForLeaving1))
           .ForMember(dest => dest.company2, source => source.MapFrom(src => src.Company2))
           .ForMember(dest => dest.job_title2, source => source.MapFrom(src => src.JobTitle2))
           .ForMember(dest => dest.country_id_enc2, source => source.MapFrom(src => src.CountryId2))
           .ForMember(dest => dest.city2, source => source.MapFrom(src => src.City2))
           .ForMember(dest => dest.time_period_from2, source => source.MapFrom(src => src.TimePeriodFrom2))
           .ForMember(dest => dest.time_period_to2, source => source.MapFrom(src => src.TimePeriodTo2))
           .ForMember(dest => dest.reason_for_leaving2, source => source.MapFrom(src => src.ReasonForLeaving2))
           .ForMember(dest => dest.company3, source => source.MapFrom(src => src.Company3))
           .ForMember(dest => dest.job_title3, source => source.MapFrom(src => src.JobTitle3))
           .ForMember(dest => dest.country_id_enc3, source => source.MapFrom(src => src.CountryId3))
           .ForMember(dest => dest.city3, source => source.MapFrom(src => src.City3))
           .ForMember(dest => dest.time_period_from3, source => source.MapFrom(src => src.TimePeriodFrom3))
           .ForMember(dest => dest.time_period_to3, source => source.MapFrom(src => src.TimePeriodTo3))
           .ForMember(dest => dest.reason_for_leaving3, source => source.MapFrom(src => src.ReasonForLeaving3))
           .ReverseMap();

            //Recruiter Posted Jobs Create
            CreateMap<RecruiterPostedJobs, RecruiterPostedJobsCreateRequestDTO>()
           .ForMember(dest => dest.reference_no, source => source.MapFrom(src => src.ReferenceNo))
           .ForMember(dest => dest.title, source => source.MapFrom(src => src.Title))
           .ForMember(dest => dest.description, source => source.MapFrom(src => src.Description))
           .ForMember(dest => dest.country_id_enc, source => source.MapFrom(src => src.CountryId))
           .ForMember(dest => dest.city, source => source.MapFrom(src => src.City))
           .ForMember(dest => dest.employment_type, source => source.MapFrom(src => src.EmploymentType))
           .ForMember(dest => dest.company_type, source => source.MapFrom(src => src.CompanyType))
           .ForMember(dest => dest.salary_range_id_enc, source => source.MapFrom(src => src.SalaryRangeId))
           .ForMember(dest => dest.experience_level, source => source.MapFrom(src => src.ExperienceLevel))
           .ForMember(dest => dest.roles_job_id_enc, source => source.MapFrom(src => src.RolesJobId))
           .ForMember(dest => dest.roles_job_other, source => source.MapFrom(src => src.RolesJobOther))
           .ForMember(dest => dest.required_number, source => source.MapFrom(src => src.RequiredNumber))
           .ForMember(dest => dest.is_training_available, source => source.MapFrom(src => src.IsTrainingAvailable))
           .ReverseMap();

            CreateMap<CountryDetails, CountryResponseDTO>()
             .ReverseMap();

            CreateMap<SalaryRange, SalaryRangeResponseDTO>()
             .ReverseMap();

            CreateMap<CompanyType, CompanyTypeResponseDTO>()
             .ReverseMap();

            CreateMap<EmployeeType, EmployeeTypeResponseDTO>()
             .ReverseMap();

            CreateMap<MaritalStatus, MaritalStatusResponseDTO>()
             .ReverseMap();

            CreateMap<EducationLevels, EducationLevelResponseDTO>()
             .ReverseMap();

            CreateMap<ExperienceLevel, ExperienceLevelResponseDTO>()
           .ReverseMap();


            //Recruiter Posted Jobs Save
            CreateMap<RecruiterPostedJobs, RecruiterPostedJobsSaveRequestDTO>()
            .ForMember(dest => dest.recruiter_posted_jobs_id_enc, source => source.MapFrom(src => src.RecruiterPostedJobsId))
           .ForMember(dest => dest.title, source => source.MapFrom(src => src.Title))
           .ForMember(dest => dest.description, source => source.MapFrom(src => src.Description))
           .ForMember(dest => dest.country_id_enc, source => source.MapFrom(src => src.CountryId))
           .ForMember(dest => dest.city, source => source.MapFrom(src => src.City))
           .ForMember(dest => dest.employment_type, source => source.MapFrom(src => src.EmploymentType))
           .ForMember(dest => dest.company_type, source => source.MapFrom(src => src.CompanyType))
           .ForMember(dest => dest.salary_range_id_enc, source => source.MapFrom(src => src.SalaryRangeId))
           .ForMember(dest => dest.experience_level, source => source.MapFrom(src => src.ExperienceLevel))    // Mismatch mapping. Need to check with developer
           .ForMember(dest => dest.roles_job_id_enc, source => source.MapFrom(src => src.RolesJobId))
           .ReverseMap();

            //Recruiter Posted Jobs Response
            CreateMap<RecruiterPostedJobs, RecruiterPostedJobsResponseDTO>()
            .ForMember(dest => dest.recruiter_posted_jobs_id_enc, source => source.MapFrom(src => src.RecruiterPostedJobsId))
            .ForMember(dest => dest.recruiter_id_enc, source => source.MapFrom(src => src.RecruiterId))
            .ForMember(dest => dest.reference_no, source => source.MapFrom(src => src.ReferenceNo))
            .ForMember(dest => dest.title, source => source.MapFrom(src => src.Title))
            .ForMember(dest => dest.description, source => source.MapFrom(src => src.Description))
            .ForMember(dest => dest.city, source => source.MapFrom(src => src.City))
            .ForMember(dest => dest.employment_type, source => source.MapFrom(src => src.EmploymentType))
            .ForMember(dest => dest.experience_level, source => source.MapFrom(src => src.ExperienceLevel))
            .ForMember(dest => dest.added_date, source => source.MapFrom(src => src.CreatedOn))
            .ReverseMap();

            CreateMap<RecruiterPostedJobs, RecruiterPostedJobsDetailsResponseDTO>()
            .ForMember(dest => dest.recruiter_posted_jobs_id_enc, source => source.MapFrom(src => src.RecruiterPostedJobsId))
            .ForMember(dest => dest.recruiter_id_enc, source => source.MapFrom(src => src.RecruiterId))
            .ForMember(dest => dest.reference_no, source => source.MapFrom(src => src.ReferenceNo))
            .ForMember(dest => dest.title, source => source.MapFrom(src => src.Title))
            .ForMember(dest => dest.description, source => source.MapFrom(src => src.Description))
            .ForMember(dest => dest.city, source => source.MapFrom(src => src.City))
            .ForMember(dest => dest.employment_type, source => source.MapFrom(src => src.EmploymentType))
            .ForMember(dest => dest.experience_level, source => source.MapFrom(src => src.ExperienceLevel))
            .ReverseMap();

            CreateMap<RecruiterPostedJobs, RecruiterPostedJobsDetailsIDResponseDTO>()
          .ForMember(dest => dest.recruiter_posted_jobs_id_enc, source => source.MapFrom(src => src.RecruiterPostedJobsId))
          .ForMember(dest => dest.recruiter_id_enc, source => source.MapFrom(src => src.RecruiterId))
          .ForMember(dest => dest.reference_no, source => source.MapFrom(src => src.ReferenceNo))
          .ForMember(dest => dest.title, source => source.MapFrom(src => src.Title))
          .ForMember(dest => dest.description, source => source.MapFrom(src => src.Description))
          .ForMember(dest => dest.city, source => source.MapFrom(src => src.City))
          .ForMember(dest => dest.employment_type, source => source.MapFrom(src => src.EmploymentType))
          .ForMember(dest => dest.experience_level, source => source.MapFrom(src => src.ExperienceLevel))
          .ReverseMap();

            CreateMap<RecruiterPostedJobs, RecruiterPostedJobsMobileResponseDTO>()
            .ForMember(dest => dest.recruiter_posted_jobs_id_enc, source => source.MapFrom(src => src.RecruiterPostedJobsId))
            .ForMember(dest => dest.reference_no, source => source.MapFrom(src => src.ReferenceNo))
            .ForMember(dest => dest.title, source => source.MapFrom(src => src.Title))
            .ForMember(dest => dest.description, source => source.MapFrom(src => src.Description))
            .ForMember(dest => dest.city, source => source.MapFrom(src => src.City))
            .ForMember(dest => dest.date_posted, source => source.MapFrom(src => src.CreatedOn))
            .ReverseMap();

            CreateMap<RecruiterPostedJobsAndApplicants, RecruiterPostedJobsAndApplicantsResponseDTO>()
           .ForMember(dest => dest.recruiter_posted_jobs_and_applicants_id_enc, source => source.MapFrom(src => src.RecruiterPostedJobsAndApplicantsId))
           .ForMember(dest => dest.recruiter_posted_jobs_id_enc, source => source.MapFrom(src => src.RecruiterPostedJobsId))
           .ReverseMap();

            CreateMap<RecruiterPostedJobsAndApplicants, RecruiterPostedJobsAndApplicantsResponse2DTO>()
            .ForMember(dest => dest.recruiter_posted_jobs_and_applicants_id_enc, source => source.MapFrom(src => src.RecruiterPostedJobsAndApplicantsId))
            .ForMember(dest => dest.recruiter_posted_jobs_id_enc, source => source.MapFrom(src => src.RecruiterPostedJobsId))
            .ReverseMap();

            CreateMap<MessageGeneral, AllMessagesResponseDTO>()
            .ForMember(dest => dest.id, source => source.MapFrom(src => src.MessageGeneralId))
            .ForMember(dest => dest.message, source => source.MapFrom(src => src.Message))
            .ForMember(dest => dest.added_date, source => source.MapFrom(src => src.CreatedOn))
            .ReverseMap();

            CreateMap<JobRoles, JobRoleResponseDTO>()
           .ReverseMap();

            CreateMap<Specialization, SpecializationResponseDTO>()
            .ForMember(dest => dest.id, source => source.MapFrom(src => src.SpecializationId))
            .ForMember(dest => dest.name, source => source.MapFrom(src => src.SpecializationNameEn))
           .ReverseMap();

            CreateMap<JobSeekerAttachment, JobSeekerAttachmentResponseDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.JobSeekerAttachmentId))
          .ReverseMap();

            // Faq
            CreateMap<Faq, FaqRequestDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.FaqId))
           .ReverseMap();

            CreateMap<Faq, FaqResponseDTO>()
            .ForMember(dest => dest.Id, source => source.MapFrom(src => src.FaqId))
           .ReverseMap();
            // News 
            CreateMap<News, NewsRequestDTO>()
              .ForMember(dest => dest.Id, source => source.MapFrom(src => src.NewsId))
              .ReverseMap();

            CreateMap<News, NewsResponseDTO>()
            .ForMember(dest => dest.Id, source => source.MapFrom(src => src.NewsId))
            .ReverseMap();

            CreateMap<News, NewsSearchDTO>()
            .ReverseMap();
            // Testimonial
            CreateMap<Testimonial, TestimonialRequestDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.TestimonialId))
           .ReverseMap();

            CreateMap<Testimonial, TestimonialResponseDTO>()
            .ForMember(dest => dest.Id, source => source.MapFrom(src => src.TestimonialId))
           .ReverseMap();

            // Pages
            CreateMap<Pages, PagesRequestDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.PagesId))
           .ReverseMap();

            CreateMap<Pages, PagesResponseDTO>()
            .ForMember(dest => dest.Id, source => source.MapFrom(src => src.PagesId))
           .ReverseMap();
            // Contact Us
            CreateMap<ContactUs, ContactUsResponseDTO>()
           .ForMember(dest => dest.Id, source => source.MapFrom(src => src.ContactUsId))
           .ReverseMap();

           // CreateMap<Pages, PagesResponseDTO>()
           // .ForMember(dest => dest.Id, source => source.MapFrom(src => src.PagesId))
           //.ReverseMap();

        }
    }
}
