﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class RecruiterUploadRequestDTO
    {
        public string token { get; set; }
        public IFormFile myfile { get; set; }
    }
}
