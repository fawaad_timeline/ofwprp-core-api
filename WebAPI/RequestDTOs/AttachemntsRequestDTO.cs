﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class AttachmentsRequestDTO
    {
        public long Id { get; set; }
        public long FileId { get; set; }
        public string AttachmentTitle { get; set; }
        public string AttachmentDescription { get; set; }
        public string FileNameOriginal { get; set; }
        public string FileNameUpdated { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
    }
}
