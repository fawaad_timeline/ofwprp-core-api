﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class SalaryRangeRequestDTO
    {
        public long id { get; set; }
    }
    public class SalaryRangePaggingRequestDTO
    {
        public int paging { get; set; }
        public int offset { get; set; }
    }
}
