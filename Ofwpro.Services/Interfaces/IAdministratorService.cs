﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ofwpro.Core;

namespace Ofwpro.Services.Interfaces
{
    public interface IAdministratorService
    {
        Task<IEnumerable<Administrator>> Get(long administratorId);
        long Save(Administrator administrator);
        bool Activate(long administratorId, bool isActive);
        bool Delete(long administratorId);

        bool IsExist(string email, long administratorId);
        bool Validate(string email, string password);
        Task<Administrator> GetByEmail(string email);
        Task<Administrator> GetByAdministratorIdToken(string administratorIdToken);
        Task<IEnumerable<Administrator>> GetAll();
    }
}
