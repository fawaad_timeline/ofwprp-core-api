﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IRecruiterAttachmentRepository
    {
        Task<IEnumerable<RecruiterAttachment>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, RecruiterAttachment recruiterAttachmentObj);
        Task<RecruiterAttachment> GetById(long recruiterAttachmentId);
        long Save(RecruiterAttachment recruiterAttachmentObj);
        bool Activate(long recruiterAttachmentId, bool isActive);
        bool Delete(long recruiterId, string documentType);
        bool IsExist(string recruiterAttachmentName, long RecruiterAttachmentId);
        Task<RecruiterAttachment> GetByDocumentType(long recruiterId, string documentType);
    }
}
