﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class FaqResponseDTO
    {
        public long Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string IsActive { get; set; }
    }
}
