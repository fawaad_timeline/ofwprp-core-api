﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class ExperienceLevelRequestDTO
    {
        public int offset { get; set; }
        public int paging { get; set; }
    }
}
