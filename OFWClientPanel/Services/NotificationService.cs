﻿using Microsoft.Extensions.Configuration;
using OFWClientPanel.Data.Models.Recruiter;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ofwpro.Core;
using Nancy.Json;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Ofwpro.Services.DTOs;


namespace OFWClientPanel.Services
{
    public class NotificationService:INotificationService
    {
        public async Task<object> SendNotificationToJobSeekerAsync(string deviceToken, string _title, object _data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj)
        {
            string webAPIKey = "AIzaSyBtK_0psD-S6AVeGAV7o9d73HMFfn2LxnY";
            string serverKey = "AAAAApG0x3U:APA91bGj-XH6x09jh-DzpQ1ObzJDhlHXtLgrIMPyTaQuD6whCYIIK4Uutu1SgwpV2gncFYALM-DPk51GVdJ2xW9w89sfiHAcue_HZfFFqZQrhDiu1R2Rd91U0YC5JZ9PNco3UeFyDwzO";
            string senderId = "11034478453";
            string webAddress = "https://fcm.googleapis.com/fcm/send";

            #region comment 
            //string androidFCMkey = "";
            //object data_ = null;

            //if (string.IsNullOrEmpty(message)) { return false; }
            //if (jobSeekerObj == null) { return false; }

            //// If job seeker is logged out no need to send notification
            //if (sessionObj == null) { return false; }

            //// Receiver
            //string iosDeviceToken = jobSeekerObj.AppleDeviceToken;
            //string androidDeviceToken = jobSeekerObj.AndroidDeviceToken;

            //// Sender
            //string senderName = recruiterObj.CompanyName;
            //if (os.ToLower().Equals("android"))
            //{
            //    try
            //    {
            //        var result = "-1";
            //        var webAddr = "https://fcm.googleapis.com/fcm/send";
            //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);

            //        httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            //        httpWebRequest.ContentType = "application/json";
            //        httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, androidFCMkey);
            //        httpWebRequest.Method = "POST";
            //        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //        {
            //            var notification = new
            //            {
            //                to = androidDeviceToken,
            //                notification = new
            //                {
            //                    title = "OFWPRO",
            //                    message = message,
            //                    sound = "default"
            //                },
            //                data = data_
            //            };

            //            string strNJson = JsonConvert.SerializeObject(notification);
            //            streamWriter.Write(strNJson);
            //            streamWriter.Flush();
            //        }

            //        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //        {
            //            result = streamReader.ReadToEnd();
            //            return true;
            //        }
            //    }
            //    catch (Exception ex) { }
            //}
            //else
            //{
            //    try
            //    {
            //        var result = "-1";
            //        var webAddr = "https://fcm.googleapis.com/fcm/send";
            //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);

            //        httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            //        httpWebRequest.ContentType = "application/json";
            //        httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, androidFCMkey);
            //        httpWebRequest.Method = "POST";
            //        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //        {
            //            var notification = new
            //            {
            //                to = iosDeviceToken,
            //                notification = new
            //                {
            //                    title = "OFWPRO",
            //                    message = message,
            //                    sound = "default"
            //                },
            //                data = data_
            //            };

            //            string strNJson = JsonConvert.SerializeObject(notification);
            //            streamWriter.Write(strNJson);
            //            streamWriter.Flush();
            //        }

            //        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //        {
            //            result = streamReader.ReadToEnd();
            //            return true;
            //        }
            //    }
            //    catch (Exception ex) { }
            //}

            //return true;
            #endregion
            try
            {

                var result = "-1";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddress);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
                httpWebRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                httpWebRequest.Method = "POST";

                var payload = new
                {
                    to = deviceToken,
                    notification = new
                    {
                        title = _title,
                        body = message,
                        sound = "default",
                        badge = 0,
                        data = _data
                    },
                    data = new
                    {
                        body = _data,
                        title = _title,
                    }
                };

                var serializer = new JavaScriptSerializer();
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(payload);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)(await httpWebRequest.GetResponseAsync());
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                return result;
            }
            catch (Exception ex)
            {
                var mes = ex;
            }

            return false;
        }


        public async Task<object> SendNotificationToRecruiterAsync(string deviceToken, string _title, object _data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj)
        {

            string webAPIKey = "AIzaSyBtK_0psD-S6AVeGAV7o9d73HMFfn2LxnY";
            string serverKey = "AAAAApG0x3U:APA91bGj-XH6x09jh-DzpQ1ObzJDhlHXtLgrIMPyTaQuD6whCYIIK4Uutu1SgwpV2gncFYALM-DPk51GVdJ2xW9w89sfiHAcue_HZfFFqZQrhDiu1R2Rd91U0YC5JZ9PNco3UeFyDwzO";
            string senderId = "11034478453";
            string webAddress = "https://fcm.googleapis.com/fcm/send";
            #region comment
            //string androidFCMkey = "";
            //object data_ = null;

            //if (string.IsNullOrEmpty(message)) { return false; }
            //if (jobSeekerObj == null) { return false; }

            //// If recruiter is logged out no need to send notification
            //if (sessionObj == null) { return false; }

            //// Receiver
            //string iosDeviceToken = jobSeekerObj.AppleDeviceToken;
            //string androidDeviceToken = jobSeekerObj.AndroidDeviceToken;

            //// Sender
            //string senderName = recruiterObj.CompanyName;
            //if (os.ToLower().Equals("android"))
            //{
            //    try
            //    {
            //        var result = "-1";
            //        var webAddr = "https://fcm.googleapis.com/fcm/send";
            //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);

            //        httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            //        httpWebRequest.ContentType = "application/json";
            //        httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, androidFCMkey);
            //        httpWebRequest.Method = "POST";
            //        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //        {
            //            var notification = new
            //            {
            //                to = androidDeviceToken,
            //                notification = new
            //                {
            //                    title = "OFWPRO",
            //                    message = message,
            //                    sound = "default"
            //                },
            //                data = data_
            //            };

            //            string strNJson = JsonConvert.SerializeObject(notification);
            //            streamWriter.Write(strNJson);
            //            streamWriter.Flush();
            //        }

            //        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //        {
            //            result = streamReader.ReadToEnd();
            //            return true;
            //        }
            //    }
            //    catch (Exception ex) { }
            //}
            //else
            //{
            //    try
            //    {
            //        var result = "-1";
            //        var webAddr = "https://fcm.googleapis.com/fcm/send";
            //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);

            //        httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            //        httpWebRequest.ContentType = "application/json";
            //        httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, androidFCMkey);
            //        httpWebRequest.Method = "POST";
            //        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //        {
            //            var notification = new
            //            {
            //                to = iosDeviceToken,
            //                notification = new
            //                {
            //                    title = "OFWPRO",
            //                    message = message,
            //                    sound = "default"
            //                },
            //                data = data_
            //            };

            //            string strNJson = JsonConvert.SerializeObject(notification);
            //            streamWriter.Write(strNJson);
            //            streamWriter.Flush();
            //        }

            //        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //        {
            //            result = streamReader.ReadToEnd();
            //            return true;
            //        }
            //    }
            //    catch (Exception ex) { }
            //}

            //return true;

            #endregion
            try
            {

                var result = "-1";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddress);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
                httpWebRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                httpWebRequest.Method = "POST";


                var payload = new
                {
                    to = deviceToken,
                    notification = new
                    {
                        title = _title,
                        body = message,
                        sound = "default",
                        badge = 0,
                        data = _data
                    },
                    data = new
                    {
                        body = _data,
                        title = _title,
                    }
                };

                var serializer = new JavaScriptSerializer();
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(payload);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)(await httpWebRequest.GetResponseAsync());
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                return result;
            }
            catch (Exception ex)
            {
                var mes = ex;
            }

            return false;
        }

        public async Task<object> SendPusher(string deviceToken, string _title, object _data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj, SaveChatJobSeekerRequestDTO data)
        {

            string webAPIKey = "AIzaSyBtK_0psD-S6AVeGAV7o9d73HMFfn2LxnY";
            string serverKey = "AAAAApG0x3U:APA91bGj-XH6x09jh-DzpQ1ObzJDhlHXtLgrIMPyTaQuD6whCYIIK4Uutu1SgwpV2gncFYALM-DPk51GVdJ2xW9w89sfiHAcue_HZfFFqZQrhDiu1R2Rd91U0YC5JZ9PNco3UeFyDwzO";
            string senderId = "11034478453";
            string webAddress = "https://fcm.googleapis.com/fcm/send";

            try
            {

                var result = "-1";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddress);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
                httpWebRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                httpWebRequest.Method = "POST";

                var payload = new
                {
                    to = deviceToken,
                    notification = new
                    {
                        title = _title,
                        body = message,
                        sound = "default",
                        badge = 0,
                        data = data
                    },
                    data = new
                    {
                        body = data,
                        title = _title,
                    }
                };

                var serializer = new JavaScriptSerializer();
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(payload);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)(await httpWebRequest.GetResponseAsync());
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                return result;
            }
            catch (Exception ex)
            {
                var mes = ex;
            }

            return false;


            //try {
            //    var options = new PusherOptions();
            //    options.Cluster = "eu";

            //    var pusher = new Pusher("b696c420f9702d42cb44", "9f75c505be6189722330", "627281", options);
            //    Task<ITriggerResult> resultTask = pusher.TriggerAsync("my-channel", "my-event", data);
            //    return true;
            //}
            //catch(Exception ex)
            //{
            //    return false;
            //}
        }


        public async Task<object> SendPusherJs(string deviceToken, string _title, object _data, int unReadCount, string message, string os, Session sessionObj, JobSeeker jobSeekerObj, Recruiter recruiterObj, SaveChatRecruiterRequestDTO data)
        {

            string webAPIKey = "AIzaSyBtK_0psD-S6AVeGAV7o9d73HMFfn2LxnY";
            string serverKey = "AAAAApG0x3U:APA91bGj-XH6x09jh-DzpQ1ObzJDhlHXtLgrIMPyTaQuD6whCYIIK4Uutu1SgwpV2gncFYALM-DPk51GVdJ2xW9w89sfiHAcue_HZfFFqZQrhDiu1R2Rd91U0YC5JZ9PNco3UeFyDwzO";
            string senderId = "11034478453";
            string webAddress = "https://fcm.googleapis.com/fcm/send";

            try
            {

                var result = "-1";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddress);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
                httpWebRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                httpWebRequest.Method = "POST";

                var payload = new
                {
                    to = deviceToken,
                    notification = new
                    {
                        title = _title,
                        body = message,
                        sound = "default",
                        badge = 0,
                        data = data
                    },
                    data = new
                    {
                        body = data,
                        title = _title,
                    }
                };

                var serializer = new JavaScriptSerializer();
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(payload);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)(await httpWebRequest.GetResponseAsync());
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                return result;
            }
            catch (Exception ex)
            {
                var mes = ex;
            }

            return false;


            //try {
            //    var options = new PusherOptions();
            //    options.Cluster = "eu";

            //    var pusher = new Pusher("b696c420f9702d42cb44", "9f75c505be6189722330", "627281", options);
            //    Task<ITriggerResult> resultTask = pusher.TriggerAsync("my-channel", "my-event", data);
            //    return true;
            //}
            //catch(Exception ex)
            //{
            //    return false;
            //}
        }
    }
}
