﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class JobSeeker
    {
        protected long jobSeekerId;
        protected string jobSeekerIdToken, nationalNumber, title, firstName, lastName, fatherName, motherName, gender, mobile, countryNo, countryCode, email, password, accActivationToken, appleDeviceToken, androidDeviceToken, userType, createdBy, updatedBy;
        protected DateTime dob, createdOn, updatedOn;
        protected bool isActive, isDeleted, isApproved, isPictureApproved, isVideoApproved;

       
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long JobSeekerId { get => jobSeekerId; set => jobSeekerId = value; }
        public string NationalNumber { get => nationalNumber; set => nationalNumber = value; }
        public string JobSeekerIdToken { get => jobSeekerIdToken; set => jobSeekerIdToken = value; }
        public string Title { get => title; set => title = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string FatherName { get => fatherName; set => fatherName = value; }
        public string MotherName { get => motherName; set => motherName = value; }
        public DateTime Dob { get => dob; set => dob = value; }
        public string Gender { get => gender; set => gender = value; }
        public string Mobile { get => mobile; set => mobile = value; }
        public string CountryNo { get => countryNo; set => countryNo = value; }
        public string CountryCode { get => countryCode; set => countryCode = value; }
        public string UserType { get => userType; set => userType = value; }
        public string Email { get => email; set => email = value; }
        public bool IsPictureApproved { get => isPictureApproved; set => isPictureApproved = value; }
        public bool IsVideoApproved { get => isVideoApproved; set => isVideoApproved = value; }
        public bool IsApproved { get => isApproved; set => isApproved = value; }
        public string Password { get => password; set => password = value; }
        public string AppleDeviceToken { get => appleDeviceToken; set => appleDeviceToken = value; }
        public string AndroidDeviceToken { get => androidDeviceToken; set => androidDeviceToken = value; }
        public string AccActivationToken { get => accActivationToken; set => accActivationToken = value; }

        public bool IsActive { get => isActive; set => isActive = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }




    }
}
