﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class RecruiterAttachmentRepository: IRecruiterAttachmentRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public RecruiterAttachmentRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<RecruiterAttachment>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, RecruiterAttachment recruiterAttachmentObj)
        {
            var predicate = PredicateBuilder.New<RecruiterAttachment>(true);
            var predicateInner = PredicateBuilder.New<RecruiterAttachment>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                //predicateInner = predicateInner.Or(i => i.RecruiterAttachmentNameEn.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.RecruiterAttachmentNameAr.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (recruiterAttachmentObj.RecruiterAttachmentId > 0)
            {
                predicate = predicate.And(i => i.RecruiterAttachmentId == recruiterAttachmentObj.RecruiterAttachmentId);
                isPradicate = true;
            }
            if (recruiterAttachmentObj.RecruiterId > 0)
            {
                predicate = predicate.And(i => i.RecruiterId == recruiterAttachmentObj.RecruiterId);
                isPradicate = true;
            }
            //if (!string.IsNullOrEmpty(recruiterAttachmentObj.RecruiterAttachmentNameAr))
            //{
            //    predicate = predicate.And(i => i.RecruiterAttachmentNameAr.Contains(recruiterAttachmentObj.RecruiterAttachmentNameAr));
            //    isPradicate = true;
            //}
            if (!string.IsNullOrEmpty(recruiterAttachmentObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == recruiterAttachmentObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.RecruiterAttachment.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.RecruiterAttachment.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.RecruiterAttachment.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.RecruiterAttachment.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<RecruiterAttachment> GetById(long recruiterAttachmentId)
        {
            return unitOfWork.Context.RecruiterAttachment.Where(x => !x.IsDeleted && x.RecruiterAttachmentId == recruiterAttachmentId).FirstOrDefaultAsync();
        }

        public long Save(RecruiterAttachment recruiterAttachmentObj)
        {
            if (recruiterAttachmentObj.RecruiterAttachmentId <= 0)
            {
                recruiterAttachmentObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<RecruiterAttachment>().Add(recruiterAttachmentObj);
            }
            else
            {
                recruiterAttachmentObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(recruiterAttachmentObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterAttachment.Update(recruiterAttachmentObj);
            }

            unitOfWork.Commit();
            return recruiterAttachmentObj.RecruiterAttachmentId;
        }


        public bool Activate(long recruiterAttachmentId, bool isActive)
        {
            RecruiterAttachment recruiterAttachmentObj = unitOfWork.Context.RecruiterAttachment.Where(x => x.RecruiterAttachmentId == recruiterAttachmentId).ToList().LastOrDefault();
            if (recruiterAttachmentObj != null)
            {
                recruiterAttachmentObj.IsActive = isActive;
                recruiterAttachmentObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(recruiterAttachmentObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterAttachment.Update(recruiterAttachmentObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long recruiterId, string documentType)
        {
            var recruiterAttachmentObj = unitOfWork.Context.RecruiterAttachment.Where(x => !x.IsDeleted && x.RecruiterId == recruiterId && x.DocumentType == documentType).ToList().LastOrDefault();

            if (recruiterAttachmentObj != null)
            {
                recruiterAttachmentObj.UpdatedOn = DateTime.Now;
                recruiterAttachmentObj.IsDeleted = true;
                unitOfWork.Context.Entry(recruiterAttachmentObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterAttachment.Update(recruiterAttachmentObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool IsExist(string RecruiterAttachmentNameEn, long recruiterAttachmentId)
        {
            var predicate = PredicateBuilder.New<RecruiterAttachment>(true);

            //predicate = predicate.And(i => i.RecruiterAttachmentNameEn == RecruiterAttachmentNameEn);
            if (recruiterAttachmentId > 0)
            {
                predicate = predicate.And(i => i.RecruiterAttachmentId != recruiterAttachmentId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.RecruiterAttachment.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }
        
        public Task<RecruiterAttachment> GetByDocumentType(long recruiterId, string documentType)
        {
            return unitOfWork.Context.RecruiterAttachment.Where(x => !x.IsDeleted && x.RecruiterId == recruiterId && x.DocumentType == documentType).FirstOrDefaultAsync();
        }

    }
}
