﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models.JobSeeker
{
    public class PersonalInformation
    {
        public long PersonalInformationId { get; set; }
        [Required]
        public string CountryId { get;  set; }

        [Required]
        public string MaritalStatusId { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string HasDrivingLicense  { get; set; }
        public string CurrentLocation { get; set; }
        public string VisaStatus { get; set; }

        public string PlaceOfBirthId { get; set; }
        public string NumberOfChildren { get; set; }
        public string CurrentCountryId { get; set; }
        public string HomePhone { get; set; }
        public string IsDrivingLicense { get; set; }        
        public string PeopleOfDisability { get; set; }
        public string NeedCompanion { get; set; }
        public string OriginalAddress { get; set; }
        public string ExpectedHomeAddress { get; set; }
        public string MartyrFamily { get; set; }
        public string DisabilityDescription { get; set; }
        public string DisabilityCardNumber { get; set; }
        public string City { get; set; }
        public string IdentificationPapers { get; set; }

        public DateTime DateOfHavingDisabilityCard { get; set; }
        public DateTime ReturningHomeExpectedDate { get; set; }

















    }
}
