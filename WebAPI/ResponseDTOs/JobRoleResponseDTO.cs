﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JobRoleResponseDTO
    {
		public long Id
		{
			get;
			set;
		}
		public string Name
		{
			get;
			set;
		}
	}
	public class JobRolePagingResponseDTO { 
		public int offset { get; set; }
		public int paging { get; set; }
	}
}
