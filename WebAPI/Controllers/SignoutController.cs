﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using Ofwpro.Services.Interfaces;
using Ofwpro.Core;

namespace WebAPI.Controllers
{

    [EnableCors("AllowOrigin")]
    public class SignoutController : ControllerBase
    {
        private readonly IFileUploadService fileUploadService;

        private readonly IAdministratorService administratorService;
        private readonly ISessionService sessionService;
        private readonly IJobSeekerService jobSeekerService;
        private readonly IRecruiterService recruiterService;
        private readonly IWebHostEnvironment hostingEnvironment;
        private readonly IRecruiterPostedJobsService recruiterPostedJobsService;
        private readonly IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService;
        private readonly IMapper mapper;
        private readonly IConfiguration config;

        public SignoutController(IRecruiterService recruiterService, IJobSeekerService jobSeekerService, IFileUploadService fileUploadService, IAdministratorService administratorService, ISessionService sessionService, IWebHostEnvironment hostingEnvironment, IRecruiterPostedJobsService recruiterPostedJobsService, IRecruiterPostedJobsAndApplicantsService recruiterPostedJobsAndApplicantsService, IMapper mapper, IConfiguration config)
        {
            this.fileUploadService = fileUploadService;
            this.administratorService = administratorService;
            this.sessionService = sessionService;
            this.recruiterService = recruiterService;
            this.hostingEnvironment = hostingEnvironment;
            this.recruiterPostedJobsService = recruiterPostedJobsService;
            this.recruiterPostedJobsAndApplicantsService = recruiterPostedJobsAndApplicantsService;
            this.mapper = mapper;
            this.config = config;
            this.jobSeekerService = jobSeekerService;
        }

        [HttpPost]
        [Route("en/signout/destroy_token")]
        public async Task<JsonArrResponseDTO> destroy_token([FromBody] DestroyTokenRequestDTO model)
        {
            ResponseDTO<object> response = new ResponseDTO<object>();
            string loggedInUserId = "";

            bool isSuccess = true;
            bool isExist = false;
            Session sessionObj = null;

            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                // Validate Token
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.token))
                    {
                        JsonArrResponseDTOObj.code = "2";
                        JsonArrResponseDTOObj.MSG = "Invalid token";
                        return JsonArrResponseDTOObj;
                    }

                    if (isSuccess == true)
                    {
                        sessionObj = await sessionService.GetByToken(model.token);
                        if (sessionObj == null)
                        {
                            JsonArrResponseDTOObj.code = "2";
                            JsonArrResponseDTOObj.MSG = "Invalid token";
                            return JsonArrResponseDTOObj;
                        }
                    }
                }
                long Id = 0;
                if (sessionObj.UserType == "job_seeker")
                {
                    var jobSeekerData = await jobSeekerService.GetById(sessionObj.UserId);
                    jobSeekerData.AndroidDeviceToken = "";
                    jobSeekerData.AppleDeviceToken = "";
                    Id = jobSeekerService.Save(jobSeekerData);
                }
                else if (sessionObj.UserType == "recruiter")
                {
                    var recruiterData = await recruiterService.GetById(sessionObj.UserId);
                    recruiterData.AndroidDeviceToken = "";
                    recruiterData.AppleDeviceToken = "";
                    Id = recruiterService.Save(recruiterData);
                }
                sessionService.DeleteByToken(model.token);

                JsonArrResponseDTOObj.code = "1";
                JsonArrResponseDTOObj.MSG = "Token destroyed";
                return JsonArrResponseDTOObj;
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "2";
                JsonArrResponseDTOObj.MSG = "Invalid token";
                return JsonArrResponseDTOObj;
            }
        }
    }
}