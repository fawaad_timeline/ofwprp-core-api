﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class JobSeekerWorkExperience
    {
        protected long jobSeekerWorkExperienceId, jobSeekerId, countryId1, countryId2, countryId3;
        protected string experienceLevel, company1, jobTitle1, city1, timePeriodFrom1, timePeriodTo1, reasonForLeaving1, createdBy, updatedBy;
        protected string company2, jobTitle2, city2, timePeriodFrom2, timePeriodTo2, reasonForLeaving2;
        protected string company3, jobTitle3, city3, timePeriodFrom3, timePeriodTo3, reasonForLeaving3;
        protected DateTime graduationDate, createdOn, updatedOn;
        protected bool isActive, isDeleted;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long JobSeekerWorkExperienceId { get => jobSeekerWorkExperienceId; set => jobSeekerWorkExperienceId = value; }
        public long JobSeekerId { get => jobSeekerId; set => jobSeekerId = value; }
        public string ExperienceLevel { get => experienceLevel; set => experienceLevel = value; }
        
        public string Company1 { get => company1; set => company1 = value; }
        public string JobTitle1 { get => jobTitle1; set => jobTitle1 = value; }
        public long CountryId1 { get => countryId1; set => countryId1 = value; }
        public string City1 { get => city1; set => city1 = value; }
        public string TimePeriodFrom1 { get => timePeriodFrom1; set => timePeriodFrom1 = value; }
        public string TimePeriodTo1 { get => timePeriodTo1; set => timePeriodTo1 = value; }
        public string ReasonForLeaving1 { get => reasonForLeaving1; set => reasonForLeaving1 = value; }

        public string Company2 { get => company2; set => company2 = value; }
        public string JobTitle2 { get => jobTitle2; set => jobTitle2 = value; }
        public long CountryId2 { get => countryId2; set => countryId2 = value; }
        public string City2 { get => city2; set => city2 = value; }
        public string TimePeriodFrom2 { get => timePeriodFrom2; set => timePeriodFrom2 = value; }
        public string TimePeriodTo2 { get => timePeriodTo2; set => timePeriodTo2 = value; }
        public string ReasonForLeaving2 { get => reasonForLeaving2; set => reasonForLeaving2 = value; }

        public string Company3 { get => company3; set => company3 = value; }
        public string JobTitle3 { get => jobTitle3; set => jobTitle3 = value; }
        public long CountryId3 { get => countryId3; set => countryId3 = value; }
        public string City3 { get => city3; set => city3 = value; }
        public string TimePeriodFrom3 { get => timePeriodFrom3; set => timePeriodFrom3 = value; }
        public string TimePeriodTo3 { get => timePeriodTo3; set => timePeriodTo3 = value; }
        public string ReasonForLeaving3 { get => reasonForLeaving3; set => reasonForLeaving3 = value; }

        public bool IsActive { get => isActive; set => isActive = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }
    }
}
