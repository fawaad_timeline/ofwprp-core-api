﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ofwpro.Core
{
    public class ForgotPassword
    {
        protected long forgotPasswordId, userId;
        protected string userType, forgotPasswordToken, createdBy, updatedBy;
        protected DateTime tokenGenerationDate, tokenUseDate, createdOn, updatedOn;
        protected bool isPasswordReset, isActive, isDeleted;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ForgotPasswordId { get => forgotPasswordId; set => forgotPasswordId = value; }
        public long UserId { get => userId; set => userId = value; }
        public string UserType { get => userType; set => userType = value; }
        public string ForgotPasswordToken { get => forgotPasswordToken; set => forgotPasswordToken = value; }
        public DateTime TokenGenerationDate { get => tokenGenerationDate; set => tokenGenerationDate = value; }
        public DateTime TokenUseDate { get => tokenUseDate; set => tokenUseDate = value; }
        public bool IsPasswordReset { get => isPasswordReset; set => isPasswordReset = value; }


        public bool IsActive { get => isActive; set => isActive = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string UpdatedBy { get => updatedBy; set => updatedBy = value; }
        public DateTime CreatedOn { get => createdOn; set => createdOn = value; }
        public DateTime UpdatedOn { get => updatedOn; set => updatedOn = value; }
    }
}
