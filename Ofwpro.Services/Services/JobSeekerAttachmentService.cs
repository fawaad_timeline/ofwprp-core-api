﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class JobSeekerAttachmentService: IJobSeekerAttachmentService
    {
        private readonly IJobSeekerAttachmentRepository jobSeekerAttachmentRepository;

        public JobSeekerAttachmentService(IJobSeekerAttachmentRepository jobSeekerAttachmentRepository)
        {
            this.jobSeekerAttachmentRepository = jobSeekerAttachmentRepository;
        }

        public async Task<IEnumerable<JobSeekerAttachment>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerAttachment jobSeekerAttachmentObj)
        {
            return await jobSeekerAttachmentRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, jobSeekerAttachmentObj);
        }

        public async Task<JobSeekerAttachment> GetById(long jobSeekerAttachmentId)
        {
            return await jobSeekerAttachmentRepository.GetById(jobSeekerAttachmentId);
        }

        public long Save(JobSeekerAttachment jobSeekerAttachmentObj)
        {
            return jobSeekerAttachmentRepository.Save(jobSeekerAttachmentObj);
        }

        public bool Activate(long jobSeekerAttachmentId, bool isActive)
        {
            return jobSeekerAttachmentRepository.Activate(jobSeekerAttachmentId, isActive);
        }

        public bool Delete(long jobSeekerId, string documentType)
        {
            return jobSeekerAttachmentRepository.Delete(jobSeekerId, documentType);
        }

        public bool IsExist(string jobSeekerAttachmentName, long JobSeekerAttachmentId)
        {
            return jobSeekerAttachmentRepository.IsExist(jobSeekerAttachmentName, JobSeekerAttachmentId);
        }

        public bool IsExistDocumentType(long jobSeekerId, string documentType)
        {
            return jobSeekerAttachmentRepository.IsExistDocumentType(jobSeekerId, documentType);
        }
        
        public Task<JobSeekerAttachment> GetByDocumentType(string documentType)
        {
            return jobSeekerAttachmentRepository.GetByDocumentType(documentType);
        }
        public Task<JobSeekerAttachment> GetByDocumentType(long jobSeekerId, string documentType)
        {
            return jobSeekerAttachmentRepository.GetByDocumentType( jobSeekerId, documentType);
        }

        public async Task<IEnumerable<JobSeekerAttachment>> GetByJobSeekerId(long jobSeekerId)
        {
            return await jobSeekerAttachmentRepository.GetByJobSeekerId(jobSeekerId);
        }
        public Task<JobSeekerAttachment> GetByJobSeekerAndDocumentType(long jobSeekerId, string documentType)
        {
            return jobSeekerAttachmentRepository.GetByJobSeekerAndDocumentType(jobSeekerId, documentType);
        }
        public bool DeleteById(long jobSeekerAttachmentId)
        {
            return jobSeekerAttachmentRepository.DeleteById(jobSeekerAttachmentId);
        }
    }
}
