﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JobSeekerWorkExperienceResponseDTO
    {
        public long tbl_job_seeker_qualification_id { get; set; }
        public long tbl_job_seeker_id { get; set; }
        public long tbl_education_level_id { get; set; }
        public long tbl_specialization_id { get; set; }

        public string school_university { get; set; }
        public DateTime? graduation_date { get; set; }
        public DateTime? added_date { get; set; }
        public string is_active { get; set; }
    }
}
