﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class AppsUploadRequestDTO
    {
        public string token { get; set; }
        public string document_type { get; set; }       //  'resume','video_resume','passport','id_card','picture'
        public IFormFile myfile { get; set; }
    }
}
