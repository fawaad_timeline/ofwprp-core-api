﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class JobSeekerListingAdminResponseDTO
    {
        

        public long job_seeker_id_enc { get; set; }
        public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public string mobile { get; set; }
        public string country_no { get; set; }
        public string country_code { get; set; }
        public string email { get; set; }
        public string picture { get; set; }
        public string has_video { get; set; }
        public bool isActive { get; set; }
        public bool isPictureApproved { get; set; }
        public bool isVideoApproved { get; set; }
        public string experience_level { get; set; }

        public int applied_jobs { get; set; }

        public JobSeekerPersonalInformationResponseDTO jobSeekerPersonalInformation { get; set; }
        public JobSeekerQualificationResponseDTO jobSeekerQualification { get; set; }
        public List<JobSeekerWorkExperienceDetailsArrResponseDTO> work_experience  { get; set; }
        public AttachmentJobSeekerResponseDTO attachmentJobSeeker { get; set; }
    }
}
