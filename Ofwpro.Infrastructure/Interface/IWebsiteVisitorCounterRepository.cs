﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface IWebsiteVisitorCounterRepository
    {
        Task<IEnumerable<WebsiteVisitorCounter>> GetAll();
        Task<WebsiteVisitorCounter> GetByVisitorId(string visitorId);
        long Save(WebsiteVisitorCounter websiteVisitorCounter);
    }
}
