﻿using OFWClientPanel.Data.Models.Recruiter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Services
{
    public interface IRecruiterDashboardService
    {
        Task<CompanyDetails> GetCompanyDetails(long RecruiterId);
    }
}
