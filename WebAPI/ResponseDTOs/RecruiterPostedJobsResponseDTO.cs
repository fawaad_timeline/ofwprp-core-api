﻿
using System;

namespace WebAPI.ResponseDTOs
{
    public class RecruiterPostedJobsResponseDTO
    {
        public string recruiter_email { get; set; }

        public string recruiter_number { get; set; }
        public string company_number { get; set; }

        public long recruiter_posted_jobs_id_enc { get; set; }
        public long recruiter_id_enc { get; set; }
        public string company_name { get; set; }
        public string company_address { get; set; }
        public string company_website { get; set; }
        public string logo_url { get; set; }
        public string reference_no { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string role_name { get; set; }
        public string country_name { get; set; }
        public string city { get; set; }
        public string employment_type { get; set; }
        public string company_type { get; set; }
        public string salary_range { get; set; }
        public string experience_level { get; set; }
        public DateTime added_date { get; set; }
        public string logo { get; set; }
        public bool has_applied { get; set; }
        public int total_applicant { get; set; }
    }
}
