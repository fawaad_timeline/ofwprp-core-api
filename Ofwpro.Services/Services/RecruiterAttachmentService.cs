﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class RecruiterAttachmentService: IRecruiterAttachmentService
    {
        private readonly IRecruiterAttachmentRepository recruiterAttachmentRepository;

        public RecruiterAttachmentService(IRecruiterAttachmentRepository recruiterAttachmentRepository)
        {
            this.recruiterAttachmentRepository = recruiterAttachmentRepository;
        }

        public async Task<IEnumerable<RecruiterAttachment>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, RecruiterAttachment recruiterAttachmentObj)
        {
            return await recruiterAttachmentRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, recruiterAttachmentObj);
        }

        public async Task<RecruiterAttachment> GetById(long recruiterAttachmentId)
        {
            return await recruiterAttachmentRepository.GetById(recruiterAttachmentId);
        }

        public long Save(RecruiterAttachment recruiterAttachmentObj)
        {
            return recruiterAttachmentRepository.Save(recruiterAttachmentObj);
        }

        public bool Activate(long recruiterAttachmentId, bool isActive)
        {
            return recruiterAttachmentRepository.Activate(recruiterAttachmentId, isActive);
        }

        public bool Delete(long jobSeekerId, string documentType)
        {
            return recruiterAttachmentRepository.Delete(jobSeekerId, documentType);
        }

        public bool IsExist(string recruiterAttachmentName, long RecruiterAttachmentId)
        {
            return recruiterAttachmentRepository.IsExist(recruiterAttachmentName, RecruiterAttachmentId);
        }

        public Task<RecruiterAttachment> GetByDocumentType(long recruiterId, string documentType)
        {
            return recruiterAttachmentRepository.GetByDocumentType(recruiterId, documentType);
        }
    }
}
