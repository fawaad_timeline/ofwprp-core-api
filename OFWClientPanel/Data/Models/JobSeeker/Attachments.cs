﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models.JobSeeker
{
    public class Attachments
    {
        public long ResumeDocumentId { get; set; }
        public long PassportDocumentId { get; set; }
        public long IDCardDocumentId { get; set; }
        public long PictureId { get; set; }
        public long VideoId { get; set; }
        public string DocumentType { get; set; }
        public string FileNameOriginal { get; set; }
        public string FileNameUpdated { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }

        public string ResumeURL { get; set; }
        public string ResumeFileType { get; set; }
        public string PassportURL { get; set; }
        public string PassportFileType { get; set; }
        public string IDCardURL { get; set; }
        public string IDCardFileType { get; set; }
        public string PictureURL { get; set; }
        public string PictureFileType { get; set; }
        public string VideoURL { get; set; }
        public string VideoFileType { get; set; }
    }
}
