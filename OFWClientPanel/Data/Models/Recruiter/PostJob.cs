﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models.Recruiter
{
    public class PostJob
    {
        public string PostedJobId { get; set; }
        public string ReferenceNo { get; set; }

        public string RecruiterId { get; set; }

        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public string CountryId { get; set; }
        public string City { get; set; }

        [Required]
        public string EmploymentType { get; set; }
        public string CompanyType { get; set; }
        public string SalaryRangeId { get; set; }
        public string ExperienceLevel { get; set; }
        public string RolesJobId { get; set; }
        public string RolesJobOther { get; set; }
        public string RequiredNumber { get; set; }
        public string IsTrainingAvailable { get; set; }

        public string Country { get; set; }
        public string SalaryRange { get; set; }
        public DateTime CreatedOn { get; set; }

        public string CompanyName { get; set; }
        public string CompanyLogoURL { get; set; }
        public string CompanyDescription { get; set; }

        public string JobSeekerId { get; set; }

        public bool Checked { get; set; }
        public DateTime UpdatedOn { get; set; }

        public long TotalApplicants { get; set; }
    }
}
