﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models.JobSeeker
{
    public class Profile
    {
        public long JobseekerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string JobTitle { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string DrivingLicense { get; set; }
        public string SocialStatus { get; set; }
        public DateTime DOB { get; set; }
        public string CurrentCountry { get; set; }
        public string Gender { get; set; }
        public string Mobile { get; set; }

        public DateTime LastUpdated { get; set; }

        public Qualification Education { get; set; }
        public WorkExperience WorkExperience { get; set; }
        public string TotalExperience { get; set; }
        public string ProfilePictureURL { get; set; }
        public DateTime AppliedDate { get; set; }

        public List<string> Designations { get; set; }
    }
}
