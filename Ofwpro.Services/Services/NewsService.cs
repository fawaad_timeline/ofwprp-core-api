﻿using AutoMapper;
using AutoMapper.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class NewsService : INewsService
    {
        private readonly INewsRepository newsRepository;

        public NewsService(INewsRepository newsRepository)
        {
            this.newsRepository = newsRepository;
        }

        public async Task<IEnumerable<News>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, bool searchNewsDate, News newsObj)
        {
            return await newsRepository.All(offset, paging, orderByColumn, orderBy, searchTerm, searchNewsDate, newsObj);
        }
        public async Task<IEnumerable<News>> GetAll()
        {
            return await newsRepository.GetAll();
        }

        public async Task<IEnumerable<News>> GetCurrentNews()
        {
            return await newsRepository.GetAll();
        }
        
        public async Task<News> GetById(long newsId)
        {
            return await newsRepository.GetById(newsId);
        }

        public long Save(News newsObj)
        {
            return newsRepository.Save(newsObj);
        }

        public bool Activate(long newsId, bool isActive)
        {
            return newsRepository.Activate(newsId, isActive);
        }

        public bool Delete(long newsId)
        {
            return newsRepository.Delete(newsId);
        }

        public bool IsExist(string newsTitle, long NewsId)
        {
            return newsRepository.IsExist(newsTitle, NewsId);
        }
    }
}
