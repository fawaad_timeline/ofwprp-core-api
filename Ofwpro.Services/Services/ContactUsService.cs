﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ofwpro.Services.Services
{
    public class ContactUsService:IContactUsService
    {
        private readonly IContactUsRepository contactUsRepository;

        public ContactUsService(IContactUsRepository contactUsRepository)
        {
            this.contactUsRepository = contactUsRepository;
        }
        public List<ContactUs> GetAll()
        {
            return contactUsRepository.GetAll();
        }

        public long Save(ContactUs contactUs)
        {
            return contactUsRepository.Save(contactUs);
        }
    }
}
