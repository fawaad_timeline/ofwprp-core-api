﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Services
{
    public class RecruiterPostedJobsService: IRecruiterPostedJobsService
    {
        private readonly IRecruiterPostedJobsRepository recruiterPostedJobsRepository;

        public RecruiterPostedJobsService(IRecruiterPostedJobsRepository recruiterPostedJobsRepository)
        {
            this.recruiterPostedJobsRepository = recruiterPostedJobsRepository;
        }

        public async Task<IEnumerable<RecruiterPostedJobs>> All(string orderByColumn, string orderBy, string searchTerm, RecruiterPostedJobs recruiterPostedJobsObj)
        {
            return await recruiterPostedJobsRepository.All(orderByColumn, orderBy, searchTerm, recruiterPostedJobsObj);
        }

        public async Task<RecruiterPostedJobs> GetById(long recruiterPostedJobsId)
        {
            return await recruiterPostedJobsRepository.GetById(recruiterPostedJobsId);
        }

        public long Save(RecruiterPostedJobs recruiterPostedJobsObj)
        {
            return recruiterPostedJobsRepository.Save(recruiterPostedJobsObj);
        }

        public bool Activate(long recruiterPostedJobsId, bool isActive)
        {
            return recruiterPostedJobsRepository.Activate(recruiterPostedJobsId, isActive);
        }

        public bool Delete(long recruiterPostedJobsId)
        {
            return recruiterPostedJobsRepository.Delete(recruiterPostedJobsId);
        }       

        public Task<RecruiterPostedJobs> GetByRecruiterId(long recruiterId)
        {
            return recruiterPostedJobsRepository.GetByRecruiterId(recruiterId);
        }

        public bool IsExist(string referenceNo)
        {
            return recruiterPostedJobsRepository.IsExist(referenceNo);
        }

        public bool IsExistRecruiterPostedJobsId(long recruiterPostedJobsId)
        {
            return recruiterPostedJobsRepository.IsExistRecruiterPostedJobsId(recruiterPostedJobsId);
        }

        public bool RemoveRange(List<RecruiterPostedJobs> recruiterPostedJobs)
        {
            return recruiterPostedJobsRepository.RemoveRange(recruiterPostedJobs);
        }
        public async Task<IEnumerable<RecruiterPostedJobs>> Filter(string searchText, string searchLocation, List<string> employmentType)
        {
            return await recruiterPostedJobsRepository.Filter(searchText, searchLocation, employmentType);
        }
        public async Task<IEnumerable<RecruiterPostedJobs>> GetTopEightLatestPostedJobs()
        {
            return await recruiterPostedJobsRepository.GetTopEightLatestPostedJobs();
        }
        public long NoOfJobs()
        {
            return recruiterPostedJobsRepository.NoOfJobs();
        }
        
        public async Task<IEnumerable<RecruiterPostedJobs>> PostedJobsByRecruiterId(long recruiterId)
        {
            return await recruiterPostedJobsRepository.PostedJobsByRecruiterId(recruiterId);
        }

        public async Task<bool> DeleteJobsByRecruiterId(long recruiterId)
        {
            return await recruiterPostedJobsRepository.DeleteJobsByRecruiterId(recruiterId);
        }
    }
}
