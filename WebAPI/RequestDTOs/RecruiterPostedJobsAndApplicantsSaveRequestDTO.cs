﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class RecruiterPostedJobsAndApplicantsSaveRequestDTO
    {
        public string token { get; set; }
        public long tbl_recruiter_posted_jobs_id { get; set; }
    }
}
