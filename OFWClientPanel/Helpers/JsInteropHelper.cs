﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Threading.Tasks;

namespace OFWClientPanel.Helpers
{
    public class JsInteropHelper
    {
        [Inject]
        public IJSRuntime JsRuntime { get; set; }
        public static async Task LoadJSAsync(IJSRuntime jsRuntime)
        {
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jqueryJS', src: '/theme/js/jquery-1.8.0.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'smoothWheelJS', src: '/theme/js/smoothWheel.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'bootstrapJS', src: '/theme/js/bootstrap.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'carouselJS', src: '/theme/js/owl.carousel.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'prettyPhotoJS', src: '/theme/js/jquery.prettyPhoto.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'imagesLoadedJS', src: '/theme/js/images-loaded.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'countJS', src: '/theme/js/jquery.count.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'knobifyJS', src: '/theme/js/knobify.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'isotopeJS', src: '/theme/js/isotope.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'mainJS', src: '/theme/js/main.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'select2JS', src: '/theme/assets/select2/select2.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'fileAPIJS', src: '/js/FileAPI/FileAPI.min.js',type: 'text/javascript' }));");
            //await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'fileAPIExisJS', src: '/js/FileAPI/FileAPI.exif.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jsFileAPIJS', src: '/js/FileAPI/jquery.fileapi.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jCropJS', src: '/js/FileAPI/jcrop/jquery.Jcrop.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jModalJS', src: '/js/jquery.modal.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'fileUploadJS', src: '/js/fileUpload.js',type: 'text/javascript' }));");
        }

        public static async Task UnloadJSAsync(IJSRuntime jsRuntime)
        {
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/theme/js/jquery-1.8.0.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/theme/js/smoothWheel.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/theme/js/bootstrap.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/theme/js/owl.carousel.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/theme/js/jquery.prettyPhoto.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/theme/js/images-loaded.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/theme/js/jquery.count.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/theme/js/knobify.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/theme/js/isotope.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/theme/js/main.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/theme/assets/select2/select2.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/js/FileAPI/FileAPI.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/js/FileAPI/FileAPI.exif.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/js/FileAPI/jquery.fileapi.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/js/FileAPI/jcrop/jquery.Jcrop.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/js/jquery.modal.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.createElement('script'),{ src: '/js/fileUpload.js',type: 'text/javascript' }));");
        }


        public static async Task RefreshJS(IJSRuntime jsRuntime)
        {
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('jqueryJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('smoothWheelJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('bootstrapJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('carouselJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('prettyPhotoJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('imagesLoadedJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('countJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('knobifyJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('isotopeJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('mainJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('select2JS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('fileAPIJS')));");
            //await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('fileAPIExisJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('jsFileAPIJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('jCropJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('jModalJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('fileUploadJS')));");


            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jqueryJS', src: '/theme/js/jquery-1.8.0.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'smoothWheelJS', src: '/theme/js/smoothWheel.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'bootstrapJS', src: '/theme/js/bootstrap.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'carouselJS', src: '/theme/js/owl.carousel.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'prettyPhotoJS', src: '/theme/js/jquery.prettyPhoto.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'imagesLoadedJS', src: '/theme/js/images-loaded.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'countJS', src: '/theme/js/jquery.count.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'knobifyJS', src: '/theme/js/knobify.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'isotopeJS', src: '/theme/js/isotope.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'mainJS', src: '/theme/js/main.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'select2JS', src: '/theme/assets/select2/select2.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'fileAPIJS', src: '/js/FileAPI/FileAPI.min.js',type: 'text/javascript' }));");
            //await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'fileAPIExisJS', src: '/js/FileAPI/FileAPI.exif.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jsFileAPIJS', src: '/js/FileAPI/jquery.fileapi.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jCropJS', src: '/js/FileAPI/jcrop/jquery.Jcrop.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jModalJS', src: '/js/jquery.modal.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'fileUploadJS', src: '/js/fileUpload.js',type: 'text/javascript' }));");
        }

        public static async Task<bool> RefreshJSResult(IJSRuntime jsRuntime)
        {
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('jqueryJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('smoothWheelJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('bootstrapJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('carouselJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('prettyPhotoJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('imagesLoadedJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('countJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('knobifyJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('isotopeJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('mainJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('select2JS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('fileAPIJS')));");
            //await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('fileAPIExisJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('jsFileAPIJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('jCropJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('jModalJS')));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.removeChild(Object.assign(document.getElementById('fileUploadJS')));");


            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jqueryJS', src: '/theme/js/jquery-1.8.0.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'smoothWheelJS', src: '/theme/js/smoothWheel.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'bootstrapJS', src: '/theme/js/bootstrap.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'carouselJS', src: '/theme/js/owl.carousel.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'prettyPhotoJS', src: '/theme/js/jquery.prettyPhoto.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'imagesLoadedJS', src: '/theme/js/images-loaded.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'countJS', src: '/theme/js/jquery.count.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'knobifyJS', src: '/theme/js/knobify.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'isotopeJS', src: '/theme/js/isotope.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'mainJS', src: '/theme/js/main.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'select2JS', src: '/theme/assets/select2/select2.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'fileAPIJS', src: '/js/FileAPI/FileAPI.min.js',type: 'text/javascript' }));");
            //await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'fileAPIExisJS', src: '/js/FileAPI/FileAPI.exif.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jsFileAPIJS', src: '/js/FileAPI/jquery.fileapi.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jCropJS', src: '/js/FileAPI/jcrop/jquery.Jcrop.min.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'jModalJS', src: '/js/jquery.modal.js',type: 'text/javascript' }));");
            await jsRuntime.InvokeVoidAsync("eval", "document.body.appendChild(Object.assign(document.createElement('script'),{id:'fileUploadJS', src: '/js/fileUpload.js',type: 'text/javascript' }));");

            return true;
        }

    }
}
