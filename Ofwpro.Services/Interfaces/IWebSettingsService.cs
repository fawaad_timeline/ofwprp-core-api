﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface IWebSettingsService
    {
        Task<WebSettings> All(int settingId);
        Task<WebSettings> GetById(long WebSettingsId);
        long AddUpdate(WebSettings WebSettingsObj);
        bool Delete(long WebSettingsId);
        Task<IEnumerable<WebSettings>> GetAll();
        Task<WebSettings> GetFirstRecord();
    }
}
