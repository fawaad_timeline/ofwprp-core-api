﻿namespace WebAPI.RequestDTOs
{
    public class GetExperienceLevelRequestDTO
    {
        public int offset { get; set; }
        public int paging { get; set; }
    }

}
