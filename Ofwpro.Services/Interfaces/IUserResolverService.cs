﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ofwpro.Core;

namespace Ofwpro.Services.Interfaces
{
    public interface IUserResolverService
    {
        Task<ApplicationUser> GetCurrentUser();
    }
}
