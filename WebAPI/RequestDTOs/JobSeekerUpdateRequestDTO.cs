﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class JobSeekerUpdateRequestDTO
    {
        public string token { get; set; }
        public long recruiter_posted_jobs_id_enc { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public long country_id_enc { get; set; }
        public string city { get; set; }
        public string employment_type { get; set; }
        public string company_type { get; set; }
        public long salary_range_id_enc { get; set; }
        public string experience_level { get; set; }
        public string roles_job_id_enc { get; set; }
        //public string roles_job_other { get; set; }
    }
}
