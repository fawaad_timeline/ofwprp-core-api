﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class MyJobApplicationsRequestDTO
    {
        public string offset { get; set; }
        public string pagination { get; set; }
        public string token { get; set; }
    }
}
