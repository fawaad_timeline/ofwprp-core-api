﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class MessageAgainstJobRepository: IMessageAgainstJobRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public MessageAgainstJobRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<MessageAgainstJob>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, MessageAgainstJob messageGeneralObj)
        {
            var predicate = PredicateBuilder.New<MessageAgainstJob>(true);
            var predicateInner = PredicateBuilder.New<MessageAgainstJob>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                
                predicateInner = predicateInner.Or(i => i.UserType.Contains(searchTerm));
            }

            // Dynamic Conditions
            //if (messageGeneralObj.MessageAgainstJobId > 0)
            //{
            //    predicate = predicate.And(i => i.MessageAgainstJobId == messageGeneralObj.MessageAgainstJobId);
            //    isPradicate = true;
            //}
            //if (!string.IsNullOrEmpty(messageGeneralObj.MessageAgainstJobNameEn))
            //{
            //    predicate = predicate.And(i => i.MessageAgainstJobNameEn.Contains(messageGeneralObj.MessageAgainstJobNameEn));
            //    isPradicate = true;
            //}
            //if (!string.IsNullOrEmpty(messageGeneralObj.MessageAgainstJobNameAr))
            //{
            //    predicate = predicate.And(i => i.MessageAgainstJobNameAr.Contains(messageGeneralObj.MessageAgainstJobNameAr));
            //    isPradicate = true;
            //}
            //if (!string.IsNullOrEmpty(messageGeneralObj.CreatedBy))
            //{
            //    predicate = predicate.And(i => i.CreatedBy == messageGeneralObj.CreatedBy);
            //    isPradicate = true;
            //}

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.MessageAgainstJob.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.MessageAgainstJob.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.MessageAgainstJob.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.MessageAgainstJob.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<MessageAgainstJob> GetById(long messageGeneralId)
        {
            return unitOfWork.Context.MessageAgainstJob.Where(x => !x.IsDeleted && x.MessageAgainstJobId == messageGeneralId).FirstOrDefaultAsync();
        }

        public long Save(MessageAgainstJob messageGeneralObj)
        {
            if (messageGeneralObj.MessageAgainstJobId <= 0)
            {
                messageGeneralObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<MessageAgainstJob>().Add(messageGeneralObj);
            }
            else
            {
                messageGeneralObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(messageGeneralObj).State = EntityState.Modified;
                unitOfWork.Context.MessageAgainstJob.Update(messageGeneralObj);
            }

            unitOfWork.Commit();
            return messageGeneralObj.MessageAgainstJobId;
        }


        public bool Activate(long messageGeneralId, bool isActive)
        {
            MessageAgainstJob messageGeneralObj = unitOfWork.Context.MessageAgainstJob.Where(x => x.MessageAgainstJobId == messageGeneralId).ToList().LastOrDefault();
            if (messageGeneralObj != null)
            {
                messageGeneralObj.IsActive = isActive;
                messageGeneralObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(messageGeneralObj).State = EntityState.Modified;
                unitOfWork.Context.MessageAgainstJob.Update(messageGeneralObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long messageGeneralId)
        {
            var messageGeneralObj = unitOfWork.Context.MessageAgainstJob.Where(x => !x.IsDeleted && x.MessageAgainstJobId == messageGeneralId).ToList().LastOrDefault();

            if (messageGeneralObj != null)
            {
                messageGeneralObj.UpdatedOn = DateTime.Now;
                messageGeneralObj.IsDeleted = true;
                unitOfWork.Context.Entry(messageGeneralObj).State = EntityState.Modified;
                unitOfWork.Context.MessageAgainstJob.Update(messageGeneralObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }


        public int GetTotalMessagesAgainstJobUnRead(long recruiterPostedJobsAndApplicantsId, string userType)
        {
            return unitOfWork.Context.MessageAgainstJob.Where(x => !x.IsDeleted && x.RecruiterPostedJobsAndApplicantsId == recruiterPostedJobsAndApplicantsId && x.UserType.Equals(userType)).ToList().Count();
        }


        public async Task<IEnumerable<MessageAgainstJob>> GetAllMessagesAgainstJob(string orderByColumn, string orderBy, string searchTerm, bool isActive, long recruiterPostedJobsAndApplicantsId)
        {
            var predicate = PredicateBuilder.New<MessageAgainstJob>(true);
            var predicateInner = PredicateBuilder.New<MessageAgainstJob>(false);
            bool isPradicate = false;

            // Dynamic Conditions
            if (recruiterPostedJobsAndApplicantsId > 0)
            {
                predicate = predicate.And(i => i.RecruiterPostedJobsAndApplicantsId == recruiterPostedJobsAndApplicantsId);
                isPradicate = true;
            }
            
            if (isActive == true)
            {
                predicate = predicate.And(i => i.IsActive == true);
            }
            else
            {
                predicate = predicate.And(i => i.IsActive == false);
            }

            if (!string.IsNullOrEmpty(searchTerm))
            {
                predicate = predicate.And(i => i.Message.Contains(searchTerm));

                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;


            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.MessageAgainstJob.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.MessageAgainstJob.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.MessageAgainstJob.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.MessageAgainstJob.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }


        public bool MarkMessageRead(long recruiterPostedJobsAndApplicantsId, string userType)
        {
            List<MessageAgainstJob> list = unitOfWork.Context.MessageAgainstJob.Where(x => x.UserType.Equals("recruiter") && x.UserType == userType && x.RecruiterPostedJobsAndApplicantsId == recruiterPostedJobsAndApplicantsId).ToList();
            list.ForEach(x => x.IsRead = true);
            unitOfWork.Context.MessageAgainstJob.UpdateRange(list);
            unitOfWork.Commit();
            return true;
        }

        public int GetTotalMessagesAgainstJob(long recruiterPostedJobsAndApplicantsId)
        {
            return unitOfWork.Context.MessageAgainstJob.Where(x => !x.IsDeleted && x.RecruiterPostedJobsAndApplicantsId == recruiterPostedJobsAndApplicantsId).ToList().Count();
        }
    }
}
