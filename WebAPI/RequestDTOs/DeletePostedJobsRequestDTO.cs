﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class DeletePostedJobsRequestDTO
    {
        public string token { get; set; }
        public long recruiter_posted_jobs_id_enc { get; set; }
    }
}
