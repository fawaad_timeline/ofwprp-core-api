﻿using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class CompanyTypeRepository : ICompanyTypeRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        public CompanyTypeRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public long Add(CompanyType companyType)
        {
            _unitOfWork.Context.Set<CompanyType>().Add(companyType);
            _unitOfWork.Commit();
            return companyType.Id;
        }

        public void Delete(long id)
        {
            var companyType = _unitOfWork.Context.CompanyType.Where(x => x.Id == id).FirstOrDefault();
            if (companyType != null)
            {
                _unitOfWork.Context.CompanyType.Remove(companyType);
                _unitOfWork.Commit();
            }
        }

        public async Task<IEnumerable<CompanyType>> GetAll()
        {
            var result = _unitOfWork.Context.CompanyType.OrderBy(x => x.Name);
            return await result.ToListAsync();
        }
        public CompanyType GetById(long id)
        {
            var companyType = _unitOfWork.Context.CompanyType.Where(x => x.Id == id).FirstOrDefault();
            return companyType;
        }

        public bool IsExists(string name)
        {
            bool IsExists = false;
            var companyTypes = _unitOfWork.Context.CompanyType.Where(x => x.Name == name);
            if (companyTypes.Any())
                IsExists = true;
            else
                IsExists = false;
            return IsExists;
        }

        public long Update(CompanyType companyType)
        {
            _unitOfWork.Context.Entry(companyType).State = EntityState.Modified;
            _unitOfWork.Context.CompanyType.Update(companyType);
            _unitOfWork.Commit();
            return companyType.Id;
        }
    }
}
