﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Interface
{
    public interface ICountryDetailRepository
    {
        Task<IEnumerable<CountryDetails>> GetAll();
        CountryDetails GetById(long id);
        CountryDetails GetByCode(string code);
    }
}
