﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class JobSeekerPersonalInformationRepository: IJobSeekerPersonalInformationRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public JobSeekerPersonalInformationRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<JobSeekerPersonalInformation>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerPersonalInformation jobSeekerPersonalInformationObj)
        {
            var predicate = PredicateBuilder.New<JobSeekerPersonalInformation>(true);
            var predicateInner = PredicateBuilder.New<JobSeekerPersonalInformation>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                //predicateInner = predicateInner.Or(i => i.JobSeekerPersonalInformationNameEn.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.JobSeekerPersonalInformationNameAr.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (jobSeekerPersonalInformationObj.JobSeekerPersonalInformationId > 0)
            {
                predicate = predicate.And(i => i.JobSeekerPersonalInformationId == jobSeekerPersonalInformationObj.JobSeekerPersonalInformationId);
                isPradicate = true;
            }
            //if (!string.IsNullOrEmpty(jobSeekerPersonalInformationObj.JobSeekerPersonalInformationNameEn))
            //{
            //    predicate = predicate.And(i => i.JobSeekerPersonalInformationNameEn.Contains(jobSeekerPersonalInformationObj.JobSeekerPersonalInformationNameEn));
            //    isPradicate = true;
            //}
            //if (!string.IsNullOrEmpty(jobSeekerPersonalInformationObj.JobSeekerPersonalInformationNameAr))
            //{
            //    predicate = predicate.And(i => i.JobSeekerPersonalInformationNameAr.Contains(jobSeekerPersonalInformationObj.JobSeekerPersonalInformationNameAr));
            //    isPradicate = true;
            //}
            if (!string.IsNullOrEmpty(jobSeekerPersonalInformationObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == jobSeekerPersonalInformationObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.JobSeekerPersonalInformation.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.JobSeekerPersonalInformation.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.JobSeekerPersonalInformation.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.JobSeekerPersonalInformation.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public async Task<JobSeekerPersonalInformation> GetById(long jobSeekerPersonalInformationId)
        {
            var data = await unitOfWork.Context.JobSeekerPersonalInformation.Where(x => !x.IsDeleted && x.JobSeekerId == jobSeekerPersonalInformationId).SingleOrDefaultAsync();
            return data;
        }

        public long Save(JobSeekerPersonalInformation jobSeekerPersonalInformationObj)
        {
            if (jobSeekerPersonalInformationObj.JobSeekerPersonalInformationId <= 0)
            {
                jobSeekerPersonalInformationObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<JobSeekerPersonalInformation>().Add(jobSeekerPersonalInformationObj);
            }
            else
            {
                jobSeekerPersonalInformationObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(jobSeekerPersonalInformationObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerPersonalInformation.Update(jobSeekerPersonalInformationObj);
            }

            unitOfWork.Commit();
            unitOfWork.Context.Entry(jobSeekerPersonalInformationObj).State = EntityState.Detached;
            return jobSeekerPersonalInformationObj.JobSeekerPersonalInformationId;
        }


        public bool Activate(long jobSeekerPersonalInformationId, bool isActive)
        {
            JobSeekerPersonalInformation jobSeekerPersonalInformationObj = unitOfWork.Context.JobSeekerPersonalInformation.Where(x => x.JobSeekerPersonalInformationId == jobSeekerPersonalInformationId).ToList().LastOrDefault();
            if (jobSeekerPersonalInformationObj != null)
            {
                jobSeekerPersonalInformationObj.IsActive = isActive;
                jobSeekerPersonalInformationObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(jobSeekerPersonalInformationObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerPersonalInformation.Update(jobSeekerPersonalInformationObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long jobSeekerPersonalInformationId)
        {
            var jobSeekerPersonalInformationObj = unitOfWork.Context.JobSeekerPersonalInformation.Where(x => !x.IsDeleted && x.JobSeekerPersonalInformationId == jobSeekerPersonalInformationId).ToList().LastOrDefault();

            if (jobSeekerPersonalInformationObj != null)
            {
                jobSeekerPersonalInformationObj.UpdatedOn = DateTime.Now;
                jobSeekerPersonalInformationObj.IsDeleted = true;
                unitOfWork.Context.Entry(jobSeekerPersonalInformationObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerPersonalInformation.Update(jobSeekerPersonalInformationObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }


        public Task<JobSeekerPersonalInformation> GetByJobSeekerId(long jobSeekerId)
        {
            return unitOfWork.Context.JobSeekerPersonalInformation.Where(x => !x.IsDeleted && x.JobSeekerId == jobSeekerId).OrderByDescending(x => x.JobSeekerPersonalInformationId).FirstOrDefaultAsync();
        }

    }
}
