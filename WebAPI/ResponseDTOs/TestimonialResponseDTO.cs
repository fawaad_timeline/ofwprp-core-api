﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class TestimonialResponseDTO
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string Designation { get; set; }
        public string TestimonialText { get; set; }
        public string IsActive { get; set; }
    }
}
