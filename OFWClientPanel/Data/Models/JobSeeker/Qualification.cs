﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OFWClientPanel.Data.Models.JobSeeker
{
    public class Qualification
    {
        public long QualificationId { get; set; }

        [Required (ErrorMessage ="Name of degree is required")]
        public string NameOfDegree { get; set; }
        [Required(ErrorMessage = "Education  level is required")]
        public string EducationLevelId { get;  set; }

        
        public string SpecializationId { get; set; }

        [Required(ErrorMessage = "School/University is required")]
        public string SchoolOrUniversity { get; set; }

        [Required(ErrorMessage = "Graduation Date is required")]
        public DateTime GraduationDate  { get; set; }

        public string EducationLevel { get; set; }
        public string Specialization { get; set; }
    }
}
