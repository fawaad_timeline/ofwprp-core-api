﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class FileResponseDTO
    {
        public long Id { get; set; }
        public string FileTitle { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
        public string Type { get; set; }
        public string OriginalName { get; set; }
    }
}
