﻿using Ofwpro.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface ICompanyTypeService
    {
        Task<IEnumerable<CompanyType>> GetAll();
        CompanyType GetById(long id);
        long Add(CompanyType companyType);
        long Update(CompanyType companyType);
        void Delete(long id);
        bool IsExists(string name);
    }
}
