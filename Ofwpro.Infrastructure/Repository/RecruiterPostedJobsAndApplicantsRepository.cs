﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class RecruiterPostedJobsAndApplicantsRepository: IRecruiterPostedJobsAndApplicantsRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public RecruiterPostedJobsAndApplicantsRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<RecruiterPostedJobsAndApplicants>> All(string orderByColumn, string orderBy, string searchTerm, RecruiterPostedJobsAndApplicants recruiterPostedJobsAndApplicantsObj)
        {
            var predicate = PredicateBuilder.New<RecruiterPostedJobsAndApplicants>(true);
            var predicateInner = PredicateBuilder.New<RecruiterPostedJobsAndApplicants>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                //predicateInner = predicateInner.Or(i => i.RecruiterPostedJobsAndApplicantsNameEn.Contains(searchTerm));
                //predicateInner = predicateInner.Or(i => i.RecruiterPostedJobsAndApplicantsNameAr.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (recruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsId > 0)
            {
                predicate = predicate.And(i => i.RecruiterPostedJobsId == recruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsId);
                isPradicate = true;
            }
            if (recruiterPostedJobsAndApplicantsObj.JobSeekerId > 0)
            {
                predicate = predicate.And(i => i.JobSeekerId == recruiterPostedJobsAndApplicantsObj.JobSeekerId);
                isPradicate = true;
            }
            if (recruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsAndApplicantsId > 0)
            {
                predicate = predicate.And(i => i.RecruiterPostedJobsAndApplicantsId == recruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsAndApplicantsId);
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(recruiterPostedJobsAndApplicantsObj.Comments))
            {
                predicate = predicate.And(i => i.Comments.Contains(recruiterPostedJobsAndApplicantsObj.Comments));
                isPradicate = true;
            }
            //if (!string.IsNullOrEmpty(recruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsAndApplicantsNameAr))
            //{
            //    predicate = predicate.And(i => i.RecruiterPostedJobsAndApplicantsNameAr.Contains(recruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsAndApplicantsNameAr));
            //    isPradicate = true;
            //}
            if (!string.IsNullOrEmpty(recruiterPostedJobsAndApplicantsObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == recruiterPostedJobsAndApplicantsObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            //predicate = predicate.And(i => i.IsActive == true);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.RecruiterPostedJobsAndApplicants.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.RecruiterPostedJobsAndApplicants.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.RecruiterPostedJobsAndApplicants.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.RecruiterPostedJobsAndApplicants.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<RecruiterPostedJobsAndApplicants> GetById(long recruiterPostedJobsAndApplicantsId)
        {
            return unitOfWork.Context.RecruiterPostedJobsAndApplicants.Where(x => !x.IsDeleted && x.RecruiterPostedJobsAndApplicantsId == recruiterPostedJobsAndApplicantsId).FirstOrDefaultAsync();
        }

        public long Save(RecruiterPostedJobsAndApplicants recruiterPostedJobsAndApplicantsObj)
        {
            if (recruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsAndApplicantsId <= 0)
            {
                recruiterPostedJobsAndApplicantsObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<RecruiterPostedJobsAndApplicants>().Add(recruiterPostedJobsAndApplicantsObj);
            }
            else
            {
                recruiterPostedJobsAndApplicantsObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(recruiterPostedJobsAndApplicantsObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterPostedJobsAndApplicants.Update(recruiterPostedJobsAndApplicantsObj);
            }

            unitOfWork.Commit();
            return recruiterPostedJobsAndApplicantsObj.RecruiterPostedJobsAndApplicantsId;
        }


        public bool Activate(long recruiterPostedJobsAndApplicantsId, bool isActive)
        {
            RecruiterPostedJobsAndApplicants recruiterPostedJobsAndApplicantsObj = unitOfWork.Context.RecruiterPostedJobsAndApplicants.Where(x => x.RecruiterPostedJobsAndApplicantsId == recruiterPostedJobsAndApplicantsId).ToList().LastOrDefault();
            if (recruiterPostedJobsAndApplicantsObj != null)
            {
                recruiterPostedJobsAndApplicantsObj.IsActive = isActive;
                recruiterPostedJobsAndApplicantsObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(recruiterPostedJobsAndApplicantsObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterPostedJobsAndApplicants.Update(recruiterPostedJobsAndApplicantsObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long recruiterPostedJobsAndApplicantsId)
        {
            var recruiterPostedJobsAndApplicantsObj = unitOfWork.Context.RecruiterPostedJobsAndApplicants.Where(x => !x.IsDeleted && x.RecruiterPostedJobsAndApplicantsId == recruiterPostedJobsAndApplicantsId).ToList().LastOrDefault();

            if (recruiterPostedJobsAndApplicantsObj != null)
            {
                recruiterPostedJobsAndApplicantsObj.UpdatedOn = DateTime.Now;
                recruiterPostedJobsAndApplicantsObj.IsDeleted = true;
                unitOfWork.Context.Entry(recruiterPostedJobsAndApplicantsObj).State = EntityState.Modified;
                unitOfWork.Context.RecruiterPostedJobsAndApplicants.Update(recruiterPostedJobsAndApplicantsObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public Task<RecruiterPostedJobsAndApplicants> GetByRecruiterPostedJobsId(long recruiterPostedJobsId)
        {
            return unitOfWork.Context.RecruiterPostedJobsAndApplicants.Where(x => !x.IsDeleted && x.RecruiterPostedJobsId == recruiterPostedJobsId).FirstOrDefaultAsync();
        }

        public async Task<List<RecruiterPostedJobsAndApplicants>> GetAllByRecruiterPostedJobsId(long recruiterPostedJobsId)
        {
            return await unitOfWork.Context.RecruiterPostedJobsAndApplicants.Where(x => !x.IsDeleted && x.RecruiterPostedJobsId == recruiterPostedJobsId).ToListAsync();
        }

        public bool IsExist(long jobSeekerId, long recruiterPostedJobsId)
        {
            var predicate = PredicateBuilder.New<RecruiterPostedJobsAndApplicants>(true);

            predicate = predicate.And(i => i.JobSeekerId == jobSeekerId);
            predicate = predicate.And(i => i.RecruiterPostedJobsId == recruiterPostedJobsId);
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.RecruiterPostedJobsAndApplicants.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public int totalApplicants(long recruiterPostedJobsId)
        {
            var jobSeekers = unitOfWork.Context.JobSeeker.Where(x => !x.IsDeleted).ToList();

            var recruiterPostedJobApplicants = unitOfWork.Context.RecruiterPostedJobsAndApplicants.Where(x => x.RecruiterPostedJobsId == recruiterPostedJobsId).ToList();

            var result = recruiterPostedJobApplicants.Where(x => jobSeekers.Any(y => x.JobSeekerId == y.JobSeekerId)).Count();

            return result;
        }

        public async Task<IEnumerable<RecruiterPostedJobsAndApplicants>> GetByJobSeeker(long jobSeekerId)
        {
            return await unitOfWork.Context.RecruiterPostedJobsAndApplicants.Where(x => !x.IsDeleted && x.JobSeekerId == jobSeekerId).ToListAsync();
        }

    }
}
