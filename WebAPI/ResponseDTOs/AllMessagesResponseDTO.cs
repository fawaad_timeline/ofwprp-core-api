﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class AllMessagesResponseDTO
	{
		public long id { get; set; }
		public string user_type { get; set; }
		public string message { get; set; }
		public DateTime added_date { get; set; }
		public string message_from { get; set; }
		
	}
}
