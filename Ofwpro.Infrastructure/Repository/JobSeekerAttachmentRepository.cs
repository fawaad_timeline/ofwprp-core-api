﻿using LinqKit;
using Microsoft.EntityFrameworkCore;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using Ofwpro.Infrastructure.UtilityHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Infrastructure.Repository
{
    public class JobSeekerAttachmentRepository: IJobSeekerAttachmentRepository
    {
        private readonly IUnitOfWork unitOfWork;

        public JobSeekerAttachmentRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<JobSeekerAttachment>> All(int offset, int paging, string orderByColumn, string orderBy, string searchTerm, JobSeekerAttachment jobSeekerAttachmentObj)
        {
            var predicate = PredicateBuilder.New<JobSeekerAttachment>(true);
            var predicateInner = PredicateBuilder.New<JobSeekerAttachment>(false);
            bool isPradicate = false;
            bool isPradicateInner = false;

            if (!string.IsNullOrEmpty(searchTerm))
            {
                isPradicateInner = true;
                predicateInner = predicateInner.Or(i => i.DocumentType.Contains(searchTerm));
            }

            // Dynamic Conditions
            if (jobSeekerAttachmentObj.JobSeekerAttachmentId > 0)
            {
                predicate = predicate.And(i => i.JobSeekerAttachmentId == jobSeekerAttachmentObj.JobSeekerAttachmentId);
                isPradicate = true;
            }
            if (jobSeekerAttachmentObj.JobSeekerId > 0)
            {
                predicate = predicate.And(i => i.JobSeekerId == jobSeekerAttachmentObj.JobSeekerId);
                isPradicate = true;
            }
            if (!string.IsNullOrEmpty(jobSeekerAttachmentObj.DocumentType))
            {
                predicate = predicate.And(i => i.DocumentType.Contains(jobSeekerAttachmentObj.DocumentType));
                isPradicate = true;
            }
                       if (!string.IsNullOrEmpty(jobSeekerAttachmentObj.CreatedBy))
            {
                predicate = predicate.And(i => i.CreatedBy == jobSeekerAttachmentObj.CreatedBy);
                isPradicate = true;
            }

            predicate = predicate.And(i => i.IsDeleted == false);
            isPradicate = true;

            if (isPradicateInner == true)
            {
                predicate = predicate.And(predicateInner);
            }

            // Return Results
            if (isPradicate)
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.JobSeekerAttachment.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.JobSeekerAttachment.Where(predicate).OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
            else
            {
                if (orderBy.ToLower().Equals("asc"))
                {
                    return await this.unitOfWork.Context.JobSeekerAttachment.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Asc).ToListAsync();
                }
                else
                {
                    return await this.unitOfWork.Context.JobSeekerAttachment.OrderByDynamic(orderByColumn, QueryableExtensions.Order.Desc).ToListAsync();
                }
            }
        }

        public Task<JobSeekerAttachment> GetById(long jobSeekerAttachmentId)
        {
            return unitOfWork.Context.JobSeekerAttachment.Where(x => !x.IsDeleted && x.JobSeekerAttachmentId == jobSeekerAttachmentId).FirstOrDefaultAsync();
        }

        public long Save(JobSeekerAttachment jobSeekerAttachmentObj)
        {
            if (jobSeekerAttachmentObj.JobSeekerAttachmentId <= 0)
            {
                jobSeekerAttachmentObj.CreatedOn = DateTime.Now;
                unitOfWork.Context.Set<JobSeekerAttachment>().Add(jobSeekerAttachmentObj);
            }
            else
            {
                jobSeekerAttachmentObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(jobSeekerAttachmentObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerAttachment.Update(jobSeekerAttachmentObj);
            }

            unitOfWork.Commit();
            unitOfWork.Context.Entry(jobSeekerAttachmentObj).State = EntityState.Detached;
            return jobSeekerAttachmentObj.JobSeekerAttachmentId;
        }


        public bool Activate(long jobSeekerAttachmentId, bool isActive)
        {
            JobSeekerAttachment jobSeekerAttachmentObj = unitOfWork.Context.JobSeekerAttachment.Where(x => x.JobSeekerAttachmentId == jobSeekerAttachmentId).ToList().LastOrDefault();
            if (jobSeekerAttachmentObj != null)
            {
                jobSeekerAttachmentObj.IsActive = isActive;
                jobSeekerAttachmentObj.UpdatedOn = DateTime.Now;
                unitOfWork.Context.Entry(jobSeekerAttachmentObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerAttachment.Update(jobSeekerAttachmentObj);
                unitOfWork.Commit();
                return true;
            }
            return false;
        }


        public bool Delete(long jobSeekerId, string documentType)
        {
            var jobSeekerAttachmentObj = unitOfWork.Context.JobSeekerAttachment.Where(x => !x.IsDeleted && x.JobSeekerId == jobSeekerId && x.DocumentType == documentType).ToList().LastOrDefault();

            if (jobSeekerAttachmentObj != null)
            {
                jobSeekerAttachmentObj.UpdatedOn = DateTime.Now;
                jobSeekerAttachmentObj.IsDeleted = true;
                unitOfWork.Context.Entry(jobSeekerAttachmentObj).State = EntityState.Modified;
                unitOfWork.Context.JobSeekerAttachment.Update(jobSeekerAttachmentObj);
                unitOfWork.Commit();
                return true;
            }

            return false;
        }

        public bool IsExist(string JobSeekerAttachmentNameEn, long jobSeekerAttachmentId)
        {
            var predicate = PredicateBuilder.New<JobSeekerAttachment>(true);

            //predicate = predicate.And(i => i.JobSeekerAttachmentNameEn == JobSeekerAttachmentNameEn);
            if (jobSeekerAttachmentId > 0)
            {
                predicate = predicate.And(i => i.JobSeekerAttachmentId != jobSeekerAttachmentId);
            }
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.JobSeekerAttachment.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public bool IsExistDocumentType(long jobSeekerId, string documentType)
        {
            var predicate = PredicateBuilder.New<JobSeekerAttachment>(true);

            predicate = predicate.And(i => i.JobSeekerId == jobSeekerId);
            predicate = predicate.And(i => i.DocumentType.Equals(documentType));
            predicate = predicate.And(i => i.IsDeleted == false);

            var result = this.unitOfWork.Context.JobSeekerAttachment.Where(predicate).ToList();
            if (result.Any()) { return true; }

            return false;
        }

        public Task<JobSeekerAttachment> GetByDocumentType(string documentType)
        {
            return unitOfWork.Context.JobSeekerAttachment.Where(x => !x.IsDeleted && x.DocumentType == documentType && x.JobSeekerId == 0).OrderByDescending(x => x.JobSeekerAttachmentId).FirstOrDefaultAsync();
        }

        public Task<JobSeekerAttachment> GetByDocumentType(long jobSeekerId,string documentType)
        {
            return unitOfWork.Context.JobSeekerAttachment.Where(x => !x.IsDeleted && x.DocumentType == documentType && !string.IsNullOrEmpty(documentType) && x.JobSeekerId == jobSeekerId).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<JobSeekerAttachment>> GetByJobSeekerId(long jobSeekerId)
        {
            return await unitOfWork.Context.JobSeekerAttachment.Where(x => !x.IsDeleted && x.JobSeekerId == jobSeekerId).ToListAsync();
        }

        public Task<JobSeekerAttachment> GetByJobSeekerAndDocumentType(long jobSeekerId, string documentType)
        {
            return unitOfWork.Context.JobSeekerAttachment.Where(x => !x.IsDeleted && x.DocumentType == documentType && x.JobSeekerId == jobSeekerId).OrderByDescending(x => x.JobSeekerAttachmentId).FirstOrDefaultAsync();
        }
        public bool DeleteById(long jobSeekerAttachmentId)
        {
            var jobSeekerAttachment = unitOfWork.Context.JobSeekerAttachment.Where(x => x.JobSeekerAttachmentId == jobSeekerAttachmentId).ToList().LastOrDefault();

            if(jobSeekerAttachment != null)
            {
                jobSeekerAttachment.IsDeleted = true;
                jobSeekerAttachment.UpdatedOn = DateTime.Now;

                unitOfWork.Context.Entry(jobSeekerAttachment).State = EntityState.Modified;
                unitOfWork.Context.Set<JobSeekerAttachment>().Update(jobSeekerAttachment);
                unitOfWork.Commit();

                return true;
            }
            return false;
        }
    }
}
