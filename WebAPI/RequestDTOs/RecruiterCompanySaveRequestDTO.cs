﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class RecruiterCompanySaveRequestDTO
    {
        public string token { get; set; }
        public string no_of_employees { get; set; }
        public string about_company { get; set; }
        public string website_url { get; set; }       
    }
}
