﻿using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ofwpro.Services.Interfaces
{
    public interface IExperienceLevelService
    {
        Task<IEnumerable<ExperienceLevel>> GetAll();

        Task<Tuple<IEnumerable<ExperienceLevel>, int>> GetAll(int offset, int paging);
        ExperienceLevel GetById(int id);
        int Add(ExperienceLevel experienceLevel);
        int Update(ExperienceLevel experienceLevel);
        void Delete(int id);
        bool IsExists(string name);
        Task<int> GetAllCount();
    }
}
