﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.RequestDTOs
{
    public class GetAllApplicationsMobileRequestDTO
    {
        public string token { get; set; }
        public long recruiter_posted_jobs_id_enc { get; set; }
        public int offse { get; set; }
        public int pagination { get; set; }
    }
}
