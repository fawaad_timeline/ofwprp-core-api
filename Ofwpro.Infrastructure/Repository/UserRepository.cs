﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ofwpro.Core;
using Ofwpro.Infrastructure.Interface;

namespace Ofwpro.Infrastructure.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;

        public UserRepository(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public async Task<SignInResult> Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return SignInResult.Failed;

            var user = await userManager.FindByNameAsync(username);
            if (user == null)
                return SignInResult.Failed;

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, set lockoutOnFailure: true
            var result = await signInManager.PasswordSignInAsync(user, password, false, lockoutOnFailure: true);
            return result;
        }
        public async Task<IEnumerable<ApplicationUser>> GetAll()
        {
            return await unitOfWork.Context.Users.ToListAsync();
        }
        public async Task<ApplicationUser> Get(string userId)
        {
            return await userManager.FindByIdAsync(userId);
        }

        public async Task<ApplicationUser> GetByEmail(string email)
        {
            return await userManager.FindByEmailAsync(email);
        }

        public async Task<ApplicationUser> GetByUsername(string username)
        {
            return await userManager.FindByNameAsync(username);
        }

        public async Task<IdentityResult> Add(ApplicationUser obUser, string password, string role)
        {
            try
            {
                var user = new ApplicationUser
                {
                    UserName = obUser.UserName,
                    Email = obUser.Email,
                    FirstName = obUser.FirstName,
                    LastName = obUser.LastName,
                    NormalizedUserName = obUser.NormalizedUserName,
                    NormalizedEmail = obUser.NormalizedEmail,
                    EmailConfirmed = obUser.EmailConfirmed,
                    PhoneNumber = obUser.PhoneNumber,
                    PhoneNumberConfirmed = obUser.PhoneNumberConfirmed,
                    TwoFactorEnabled = obUser.TwoFactorEnabled,
                    LockoutEnd = obUser.LockoutEnd,
                    LockoutEnabled = false,
                    AccessFailedCount = obUser.AccessFailedCount,
                    IsActive = obUser.IsActive,
                    IsDeleted = false,
                    CreatedOn = DateTime.Now,
                    CreatedBy = obUser.CreatedBy
                };

                var result = await userManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    result = await userManager.AddToRoleAsync(user, role);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IdentityResult> Update(ApplicationUser applicationUser)
        {
            applicationUser.UpdatedOn = DateTime.Now;
            var result = await userManager.UpdateAsync(applicationUser);
            return result;
        }

        public async Task<bool> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null) return false;

            var user = await userManager.FindByIdAsync(userId);
            if (user == null) return false;

            var result = await userManager.ConfirmEmailAsync(user, code);
            return result.Succeeded ? result.Succeeded : false;
        }

        public async Task<bool> ResetPassword(ApplicationUser applicationUser, string code, string password)
        {
            var user = await userManager.FindByEmailAsync(applicationUser.Email);
            if (user == null) return false;

            var result = await userManager.ResetPasswordAsync(user, code, password);
            if (result.Succeeded) return result.Succeeded;

            return false;
        }
        public async Task<bool> ChangePassword(string email, string oldPassword, string newPassword)
        {
            var user = await userManager.FindByEmailAsync(email);

            if (user != null)
            {
                var result = await userManager.ChangePasswordAsync(user, oldPassword, newPassword);
                return result.Succeeded;
            }

            return false;
        }

        public async Task<IList<string>> GetRole(ApplicationUser applicationUser)
        {
            var role = await userManager.GetRolesAsync(applicationUser);
            return role;
        }
        public async Task<bool> Delete(string userId, string deletedBy)
        {
            var user = await userManager.FindByIdAsync(userId);
            if (user == null) return false;

            user.IsDeleted = true;
            user.UpdatedBy = deletedBy;
            user.UpdatedOn = DateTime.Now;

            var result = await userManager.UpdateAsync(user);
            return result.Succeeded ? result.Succeeded : false;
        }
        public async Task<bool> IsEmailExists(string email)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user != null) return true;

            return false;
        }
        public async Task<string> GetFullName(string userId)
        {
            var user = await userManager.FindByIdAsync(userId);
            if (user != null) return user.FirstName + " " + user.LastName;

            return string.Empty;
        }

        public async Task<ApplicationUser> Activate(string userId, bool active)
        {
            var user = await userManager.FindByIdAsync(userId);
            if (user != null)
            {
                user.IsActive = active;
                user.UpdatedOn = DateTime.Now;
            }
            return user;
        }
    }
}
