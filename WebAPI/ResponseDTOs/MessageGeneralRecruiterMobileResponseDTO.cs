﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.ResponseDTOs
{
    public class MessageGeneralRecruiterMobileResponseDTO
    {
        public string from_icon { get; set; }
        public string from { get; set; }
        public string to_icon { get; set; }
        public string to { get; set; }
        public string date { get; set; }
        public string message { get; set; }        
    }
}
