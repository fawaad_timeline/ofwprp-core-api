﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Ofwpro.Core;
using Ofwpro.Services.Interfaces;
using WebAPI.Helpers;
using WebAPI.RequestDTOs;
using WebAPI.ResponseDTO;
using WebAPI.ResponseDTOs;

namespace WebAPI.Controllers
{
    [EnableCors("AllowOrigin")]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration config;
        private readonly IFileUploadService fileUploadService;
        private readonly IUserResolverService userResolverService;
        private readonly IAdministratorService administratorService;
        private readonly ISessionService sessionService;
        public LoginController(IConfiguration config, IFileUploadService fileUploadService, IUserResolverService userResolverService, IAdministratorService administratorService, ISessionService sessionService)
        {
            this.config = config;
            this.fileUploadService = fileUploadService;
            this.userResolverService = userResolverService;
            this.administratorService = administratorService;
            this.sessionService = sessionService;
        }
        //[DisableCors]
        [HttpPost]
        [Route("en/administrator/ajax_validate_and_login_admin")]
        public async Task<object> ajax_validate_and_login_admin([FromBody] AdministratorLoginRequestDTO model)
        {
            string loggedInUserId = "";
            bool isSuccess = true;
            JsonArrResponseDTO JsonArrResponseDTOObj = new JsonArrResponseDTO();

            try
            {
                if (isSuccess == true)
                {
                    if (string.IsNullOrWhiteSpace(model.email) || string.IsNullOrWhiteSpace(model.password))
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Parameters Missing";
                        return JsonArrResponseDTOObj;
                    }
                }

                string password = "";
                if (isSuccess == true)
                {
                    password = UtilityHelper.Encrypt(model.password);
                    bool validate = administratorService.Validate(model.email, password);
                    if (validate == false)
                    {
                        JsonArrResponseDTOObj.code = "0";
                        JsonArrResponseDTOObj.MSG = "Invalid Credentials";
                        return JsonArrResponseDTOObj;
                    }
                }

                if (isSuccess == true)
                {
                    Administrator administratorObj = await administratorService.GetByEmail(model.email);
                    HttpContext.Session.SetString("administratorIdSess", Convert.ToString(administratorObj.AdministratorId));
                    HttpContext.Session.SetString("firstNameSess", administratorObj.FirstName);
                    HttpContext.Session.SetString("lastNameSess", administratorObj.LastName);
                    HttpContext.Session.SetString("userTypeSess", "administrator");

                    sessionService.DeleteSessionBasedOnUser(administratorObj.AdministratorId, "administrator");

                    string token = UtilityHelper.rand();
                    Session sessionObj = new Session();
                    sessionObj.IsActive = true;
                    sessionObj.SessionId = 0;
                    sessionObj.Token = token;
                    sessionObj.UserId = administratorObj.AdministratorId;
                    sessionObj.UserType = "administrator";
                    sessionService.Save(sessionObj);

                    // Get picture
                    string picture = "";

                    LoginResponseDTO loginResponseDTOObj = new LoginResponseDTO();
                    loginResponseDTOObj.token = token;
                    loginResponseDTOObj.id = administratorObj.AdministratorId;
                    loginResponseDTOObj.first_name = administratorObj.FirstName;
                    loginResponseDTOObj.last_name = administratorObj.LastName;
                    loginResponseDTOObj.logo_url = picture;

                    JsonArrLoginResponseDTO JsonArrLoginResponseDTOObj = new JsonArrLoginResponseDTO();
                    JsonArrLoginResponseDTOObj.code = "1";
                    JsonArrLoginResponseDTOObj.MSG = "Login Successful";
                    JsonArrLoginResponseDTOObj.data = loginResponseDTOObj;
                    return JsonArrLoginResponseDTOObj;
                }
            }
            catch (Exception ex)
            {
                JsonArrResponseDTOObj.code = "0";
                JsonArrResponseDTOObj.MSG = "There was an error in processing the request.";
                return JsonArrResponseDTOObj;
            }

            return JsonArrResponseDTOObj;
        }


    }
}